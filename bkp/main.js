(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["main"],{

/***/ 83696:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _change_plan_change_plan_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./change-plan/change-plan.component */ 22298);
/* harmony import */ var _components_add_update_customer_add_update_customer_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/add-update-customer/add-update-customer.component */ 93546);





const routes = [
    {
        path: '',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_tabs_tabs_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./tabs/tabs.module */ 9483)).then(m => m.TabsPageModule)
    },
    // {
    //   path: '',
    //   loadChildren: () => import('./consume-v/consume-v.module').then( m => m.ConsumeVPageModule)
    // },
    {
        path: 'search-lead',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_search-lead_search-lead_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./search-lead/search-lead.module */ 56183)).then(m => m.SearchLeadPageModule)
    },
    {
        path: 'consume',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_consume-v_consume-v_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./consume-v/consume-v.module */ 42268)).then(m => m.ConsumeVPageModule)
    },
    {
        path: "options",
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_options_options_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./options/options.module */ 55383)).then(m => m.OptionsPageModule),
    },
    {
        path: 'food-detail',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_food-detail_food-detail_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./food-detail/food-detail.module */ 29972)).then(m => m.FoodDetailPageModule)
    },
    {
        path: 'summary-lead',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_summary-lead_summary-lead_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./summary-lead/summary-lead.module */ 18690)).then(m => m.SummaryLeadPageModule)
    },
    {
        path: 'summary-update',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_modal_summary-update_summary-update_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./modal/summary-update/summary-update.module */ 3573)).then(m => m.SummaryUpdatePageModule)
    },
    {
        path: 'login',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_login_login_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./login/login.module */ 69549)).then(m => m.LoginPageModule)
    },
    {
        path: 'select-diet',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_diet-plan_select-diet_select-diet_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./diet-plan/select-diet/select-diet.module */ 70478)).then(m => m.SelectDietPageModule)
    },
    {
        path: 'confirm-diet',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_diet-plan_confirm-diet_confirm-diet_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./diet-plan/confirm-diet/confirm-diet.module */ 13247)).then(m => m.ConfirmDietPageModule)
    },
    {
        path: 'update-slot',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_diet-plan_update-slot_update-slot_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./diet-plan/update-slot/update-slot.module */ 47588)).then(m => m.UpdateSlotPageModule)
    },
    {
        path: 'update-detail-slot',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_diet-plan_update-detail-slot_update-detail-slot_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./diet-plan/update-detail-slot/update-detail-slot.module */ 61406)).then(m => m.UpdateDetailSlotPageModule)
    },
    {
        path: 'users',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_users_users_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./users/users.module */ 96538)).then(m => m.UsersPageModule)
    },
    {
        path: 'users-list',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_users-list_users-list_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./users-list/users-list.module */ 83615)).then(m => m.UsersListPageModule)
    },
    {
        path: 'consume-v',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_consume-v_consume-v_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./consume-v/consume-v.module */ 42268)).then(m => m.ConsumeVPageModule)
    },
    {
        path: 'analysis',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_analysis_analysis_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./analysis/analysis.module */ 49902)).then(m => m.AnalysisPageModule)
    },
    {
        path: 'options',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_options_options_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./options/options.module */ 55383)).then(m => m.OptionsPageModule)
    },
    {
        path: 'options-new',
        loadChildren: () => __webpack_require__.e(/*! import() */ "default-src_app_diet-plan_update-slot-new_update-slot-new-routing_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./diet-plan/update-slot-new/update-slot-new-routing.module */ 96490)).then(m => m.UpdateSlotNewPageRoutingModule)
    },
    {
        path: 'addupdate',
        component: _components_add_update_customer_add_update_customer_component__WEBPACK_IMPORTED_MODULE_1__.AddUpdateCustomerComponent
    },
    {
        path: 'change-plan',
        component: _change_plan_change_plan_component__WEBPACK_IMPORTED_MODULE_0__.ChangePlanComponent
    },
    {
        path: 'update-slot-new',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_diet-plan_update-slot-new_update-slot-new-routing_module_ts"), __webpack_require__.e("src_app_diet-plan_update-slot-new_update-slot-new_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./diet-plan/update-slot-new/update-slot-new.module */ 41150)).then(m => m.UpdateSlotNewPageModule)
    },
    {
        path: 'deitician-search',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_deitician-search_deitician-search_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./deitician-search/deitician-search.module */ 10415)).then(m => m.DeiticianSearchPageModule)
    },
    {
        path: 'preference-diet',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_preference-diet_preference-diet_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./preference-diet/preference-diet.module */ 97470)).then(m => m.PreferenceDietPageModule)
    },
    {
        path: 'personalise',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_personalise_personalise_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./personalise/personalise.module */ 52572)).then(m => m.PersonalisePageModule)
    },
    {
        path: 'new-profile',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_new-profile_new-profile_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./new-profile/new-profile.module */ 34719)).then(m => m.NewProfilePageModule)
    },
    {
        path: 'update-slot-remarks',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_update-slot-remarks_update-slot-remarks_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./update-slot-remarks/update-slot-remarks.module */ 4938)).then(m => m.UpdateSlotRemarksPageModule)
    },
    {
        path: 'food-item-choice',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_food-item-choice_food-item-choice_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./food-item-choice/food-item-choice.module */ 73331)).then(m => m.FoodItemChoicePageModule)
    },
    // {
    //   path: 'admin',
    //   loadChildren: () => import('./admin/admin.module').then( m => m.AdminPageModule)
    // },
    // {
    //   path: 'home',
    //   loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
    // },
    // {
    //   path: 'customers',
    //   loadChildren: () => import('./customers/customers.module').then( m => m.CustomersPageModule)
    // },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_4__.PreloadAllModules, onSameUrlNavigation: 'reload' })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__.RouterModule]
    })
], AppRoutingModule);



/***/ }),

/***/ 2050:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_app_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./app.component.html */ 75158);
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss */ 30836);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 13252);





let AppComponent = class AppComponent {
    constructor(router) {
        this.router = router;
        if (!localStorage.getItem("acess_token") &&
            localStorage.getItem("acess_token") == null) {
            this.router.navigate(["./tabs/home"]);
        }
    }
};
AppComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__.Router }
];
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-root',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_app_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], AppComponent);



/***/ }),

/***/ 34750:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/platform-browser */ 86219);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _shared_interceptors_jwt_interceptor__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./shared/interceptors/jwt.interceptor */ 45821);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/common/http */ 83981);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app-routing.module */ 83696);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ 2050);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic/storage */ 78713);
/* harmony import */ var _modal_summary_update_summary_update_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./modal/summary-update/summary-update.page */ 7217);
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ 3242);
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/components.module */ 57693);
/* harmony import */ var _ionic_native_native_storage_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/native-storage/ngx */ 44991);
/* harmony import */ var _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/keyboard/ngx */ 95689);
/* harmony import */ var _change_plan_change_plan_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./change-plan/change-plan.component */ 22298);
/* harmony import */ var _select_plan_popup_select_plan_popup_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./select-plan-popup/select-plan-popup.component */ 98522);

















let AppModule = class AppModule {
};
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.NgModule)({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__.AppComponent, _modal_summary_update_summary_update_page__WEBPACK_IMPORTED_MODULE_3__.SummaryUpdatePage, _change_plan_change_plan_component__WEBPACK_IMPORTED_MODULE_8__.ChangePlanComponent, _select_plan_popup_select_plan_popup_component__WEBPACK_IMPORTED_MODULE_9__.SelectPlanPopupComponent],
        entryComponents: [_modal_summary_update_summary_update_page__WEBPACK_IMPORTED_MODULE_3__.SummaryUpdatePage],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_12__.BrowserModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.IonicModule.forRoot(),
            // IonicStorageModule.forRoot(),
            _ionic_storage__WEBPACK_IMPORTED_MODULE_14__.IonicStorageModule.forRoot({
                name: "__mydbSDP",
                driverOrder: ["indexeddb", "sqlite", "websql", "localstorage"]
            }),
            // IonicStorageModule.forRoot(),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_1__.AppRoutingModule,
            _angular_common_http__WEBPACK_IMPORTED_MODULE_15__.HttpClientModule,
            _components_components_module__WEBPACK_IMPORTED_MODULE_5__.ComponentsModule,
            // ConsumeVPageModule
        ],
        providers: [
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_16__.RouteReuseStrategy, useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_13__.IonicRouteStrategy },
            { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_15__.HTTP_INTERCEPTORS, useClass: _shared_interceptors_jwt_interceptor__WEBPACK_IMPORTED_MODULE_0__.InterceptorProvider, multi: true },
            _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_4__.InAppBrowser,
            _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_7__.Keyboard,
            _ionic_native_native_storage_ngx__WEBPACK_IMPORTED_MODULE_6__.NativeStorage,
            _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_7__.Keyboard,
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__.AppComponent],
    })
], AppModule);



/***/ }),

/***/ 38198:
/*!********************************!*\
  !*** ./src/app/app.service.ts ***!
  \********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppService": () => (/* binding */ AppService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 83981);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./core/utility/utilities */ 53533);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/storage */ 78713);
/* harmony import */ var _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./core/constants/constants */ 92133);
/* harmony import */ var _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./shared/constants/constants */ 66239);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ 88377);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ 64008);









let AppService = class AppService {
    constructor(utilities, storage, httpClient, httpBackend) {
        this.utilities = utilities;
        this.storage = storage;
        this.httpClient = httpClient;
        this.setTerms = false;
        this.offerIcon = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.paymentDone = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.expDateUpdated = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.showRateUS = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.changeplanToggle = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.changeTodaysCalToggle = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.snoozeWater = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.extendWaterNotification = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.extendFastingNotification = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.nutriScorePayment = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.isPremiumUser = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.mainPageScrollTop = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.navigateToLunchSlot = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.goToDetoxPlan = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.goToAnalysis = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.goToPersonalDiet = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.downloadDietPlan = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.goToDoSection = new rxjs__WEBPACK_IMPORTED_MODULE_3__.Subject();
        this.offerIcon$ = this.offerIcon.asObservable();
        this.toggleSwitch$ = this.changeplanToggle.asObservable();
        this.toggleSwitchTodaysCal$ = this.changeTodaysCalToggle.asObservable();
        this.paymentDone$ = this.paymentDone.asObservable();
        this.expDateUpdated$ = this.expDateUpdated.asObservable();
        this.extendWaterNotification$ = this.extendWaterNotification.asObservable();
        this.extendFastingNotification$ = this.extendFastingNotification.asObservable();
        this.showRateUS$ = this.showRateUS.asObservable();
        this.snoozeWater$ = this.snoozeWater.asObservable();
        this.nutriScorePayment$ = this.nutriScorePayment.asObservable();
        this.isPremiumUser$ = this.isPremiumUser.asObservable();
        this.mainPageScrollTop$ = this.mainPageScrollTop.asObservable();
        this.navigateToLunchSlot$ = this.navigateToLunchSlot.asObservable();
        this.goToDetoxPlan$ = this.goToDetoxPlan.asObservable();
        this.goToAnalysis$ = this.goToAnalysis.asObservable();
        this.goToPersonalDiet$ = this.goToPersonalDiet.asObservable();
        this.downloadDietPlan$ = this.downloadDietPlan.asObservable();
        this.goToDoSection$ = this.goToDoSection.asObservable();
        this.isNew = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.isNewAPIs;
        this.url = "";
        this.httpClient1 = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient(httpBackend);
    }
    getLocalData(url) {
        return this.httpClient.get(url, {}).toPromise();
    }
    getNotifyMessage(selectedFoodItems) {
        localStorage.setItem("foodNotificationMsg", 'Diet Plan Updated Successfully.');
    }
    offerCountDown() {
        let deadline = new Date(localStorage.getItem("countDownTimer")).getTime();
        let self = this;
        let x = setInterval(function () {
            let now = new Date().getTime();
            let t = deadline - now;
            if (t < 0) {
                clearInterval(x);
                console.log("In service");
                localStorage.setItem("offerTimeExpired", "true");
                self.offerIcon.next();
                return false;
            }
        }, 1000);
    }
    externalRegistration(key, payload) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.authenticateExternal;
        return this.httpClient.post(url + `?key=${key}`, payload, { headers: {} });
    }
    paymentDoneFunc() {
        this.paymentDone.next();
    }
    expDateUpdatedFunc() {
        this.expDateUpdated.next();
    }
    extendWaterNotificationFunc() {
        this.extendWaterNotification.next();
    }
    extendFastingNotificationFunc() {
        this.extendFastingNotification.next();
    }
    showRateUSFunc() {
        this.showRateUS.next();
    }
    toogleSwitchFunc(data) {
        this.changeplanToggle.next(data);
    }
    toogleSwitchTodaysCalFunc(data) {
        this.changeTodaysCalToggle.next(data);
    }
    snoozeWaterFunc() {
        this.snoozeWater.next();
    }
    nutriScorePaymentFunc() {
        this.nutriScorePayment.next();
    }
    isPremiumUserFunc() {
        this.isPremiumUser.next();
    }
    mainPageScrollTopFunc() {
        this.mainPageScrollTop.next();
    }
    navigateToLunchSlotFunc() {
        this.navigateToLunchSlot.next();
    }
    goToDetoxPlanFunc() {
        this.goToDetoxPlan.next();
    }
    goToAnalysisFunc() {
        this.goToAnalysis.next();
    }
    goToPersonalDietFunc() {
        this.goToPersonalDiet.next();
    }
    downloadDietPlanFunc() {
        this.downloadDietPlan.next();
    }
    goToDoSectionFunc() {
        this.goToDoSection.next();
    }
    searchProfile(email) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "customer-info/" + email;
        return this.httpClient.get(url, {}).toPromise();
    }
    getBeatoData(userid) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.beatoAPI;
        const header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders().set("Authorization", "Bearer eebdc61a834a99b856b99e0754b4383120b92f118a1e18a8225e0b8772b80f520138d93913fc30df").set("Content-Length", "<calculated when request is sent>").set("Host", "<calculated when request is sent>");
        return this.httpClient1.post(url, { user_id: `${userid}` }, { headers: header }).toPromise();
    }
    updateTargetCal(payload, userid, token) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL8445 + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.updateTargetCal + `${payload}&userId=${userid}`;
        const header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders().set("Authorization", `Bearer ${token}`);
        return this.httpClient1.post(url, payload, { headers: header }).toPromise();
    }
    updateSlotRemarks(payload, userId, token) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL8444 + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.updateSlotRemarks + userId;
        const header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders().set("Authorization", `Bearer ${token}`);
        return this.httpClient.post(url, payload).toPromise();
    }
    getSlotRemarks(payload, userId, token) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL8444 + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.getSlotRemarks + `${userId}`;
        const header = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders().set("Authorization", `Bearer ${token}`);
        return this.httpClient.post(url, payload).toPromise();
    }
    getProfile() {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "customer-info/" + localStorage.getItem('email');
        return this.httpClient.get(url, {}).toPromise();
    }
    getFBToken(user, config) {
        let userData = { authToken: "" };
        if (user != null) {
            userData = user;
        }
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.authUrl + "" + userData.authToken + "&email=" + user.email + "&name=" + user.name + "&firstName=" + user.firstName + "&lastName=" + user.lastName + "&provider=" + user.provider + "&appSource=" + config.app_source;
        console.log("simba", url);
        return this.httpClient.get(url, {}).toPromise();
    }
    getToken(user, config) {
        let userData = { authToken: "" };
        if (user != null) {
            userData = user;
        }
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.authUrl + "" + userData.authToken + "&email=" + user.email +
            "&appSource=" + config.app_source + "&device=" + config.device + "&os=" + config.os + "&country=" + config.country + "&region=" + config.region + "&provider=" + user.provider;
        console.log("simba", url);
        return this.httpClient.get(url, {}).toPromise();
    }
    getAppleToken(user, config) {
        let userData = { authToken: "" };
        if (user != null) {
            userData = user;
        }
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.authUrl + "na&email=" + user.email + "&name='" + user.firstName + " " + user.lastName + "'&firstName=" + user.firstName + "&lastName=" + user.lastName + "&provider=" + user.provider + "&appSource=" + config.app_source;
        console.log("simba", url);
        return this.httpClient.get(url, {}).toPromise();
    }
    getPhoneNumberToken(user, config) {
        let userData = { authToken: "" };
        if (user != null) {
            userData = user;
        }
        // const url = APIS.BASEURL + "" + APIS.authUrl + "na&email=" + user.email + "&name='" + user.firstName + " " + user.lastName + "'&firstName=" + user.firstName + "&lastName=" + user.lastName + "&provider=" + user.provider + "&appSource=" + config.app_source;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.authUrl + "na&email=" + user.email + "&name='" + user.firstName + " " + user.lastName + "'&firstName=" + user.firstName + "&lastName=" + user.lastName + "&provider=mobile&appSource=" + config.app_source;
        console.log("simba", url);
        return this.httpClient.get(url, {}).toPromise();
    }
    getCorporateToken(user, config) {
        let userData = { authToken: "" };
        if (user != null) {
            userData = user;
        }
        // const url = APIS.BASEURL + "" + APIS.authUrl + "na&email=" + user.email + "&name='" + user.firstName + " " + user.lastName + "'&firstName=" + user.firstName + "&lastName=" + user.lastName + "&provider=" + user.provider + "&appSource=" + config.app_source;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.authUrl + "na&email=" + user.email + "&name='" + user.firstName + " " + user.lastName + "'&firstName=" + user.firstName + "&lastName=" + user.lastName + "&provider=EMAIL&appSource=" + config.app_source;
        console.log("simba", url);
        return this.httpClient.get(url, {}).toPromise();
    }
    getEmailToken(user, config) {
        let userData = { authToken: "" };
        if (user != null) {
            userData = user;
        }
        // const url = APIS.BASEURL + "" + APIS.authUrl + "na&email=" + user.email + "&name='" + user.firstName + " " + user.lastName + "'&firstName=" + user.firstName + "&lastName=" + user.lastName + "&provider=" + user.provider + "&appSource=" + config.app_source;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.authUrl + "na&email=" + user.email + "&name='" + user.firstName + " " + user.lastName + "'&firstName=" + user.firstName + "&lastName=" + user.lastName + "&provider=mobile&appSource=" + config.app_source;
        console.log("simba", url);
        return this.httpClient.get(url, {}).toPromise();
    }
    postOptionFoodList(foodCodeList, email) {
        let url;
        if (this.isNew) {
            foodCodeList["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email,
                url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.optionSelection + `?userId=${_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email}`;
        }
        else {
            url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.optionSelection + `?userId=${_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email}`;
        }
        return this.httpClient.post(url, foodCodeList, {}).toPromise();
    }
    getDefaultData(token) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.defaultDetail;
        console.log("manual header", token);
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders().set("authorization", `bearer ${token}`);
        return this.httpClient1.get(url, { headers: headers }).toPromise();
    }
    getDefaultDataDiet(id) {
        id = id ? id : "IND";
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.defaultDetail + "?country=" + id;
        return this.httpClient.get(url, {}).toPromise();
    }
    postDemographic(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateDemographic;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    dietPlanSlotTimingUpdate(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.dietPlanTiming;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    postLifeStyle(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateLifeStyle;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    postDiet(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateDiet;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getDietPlans(isDetox, date, country, recommended, isfetch = false) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.dietPlansDirect + `?date=${date}&userId=${_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email}&refresh=false`;
        return this.httpClient.get(url, {}).toPromise();
        // }
    }
    getDietPlansManagement(date, email) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.dietPlansDirect + `?date=${_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.dietDate}&userId=${email}&refresh=false`;
        return this.httpClient.get(url, {}).toPromise();
    }
    getOptionsManagement(slot, email) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "options" + `?slot=${slot}&userId=${email}`;
        return this.httpClient.get(url, {}).toPromise();
    }
    getOptionsManagementNew(slot, email, category, date, dietplanname) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "custom/dietPlan/fetchFoodOptions" + `?slot=${slot}&email=${email}&category=${category}&date=${date}&dietPlanName=${dietplanname}`;
        return this.httpClient.get(url, {}).toPromise();
    }
    updateOptionsManagementNew(slot, email, category, date, dietplanname, payload) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "custom/dietPlan/updateFoodOptions" + `?slot=${slot}&email=${email}&category=${category}&date=${date}&dietPlanName=${dietplanname}`;
        return this.httpClient.post(url, payload).toPromise();
    }
    deleteCustomerHabit(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.deleteHabits;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getHabitsForUpdate() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.getHabitsForUpdate;
        return this.httpClient.get(url, {}).toPromise();
    }
    fetchHabitsList() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.habitMaster;
        return this.httpClient.get(url, {}).toPromise();
    }
    fetchCustomerHabitList() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.customerHabit;
        return this.httpClient.get(url, {}).toPromise();
    }
    addCustomerHabit(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.createHabit;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    payment(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.payment;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    paymentConfirm(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.paymentConfirm;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    updateCustomerHabit(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateHabit;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    updateWeight(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateWeight;
        console.log(url + ":-update weight:-", reqBody);
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    unsubscribe(email) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.removePlan + "?email=" + email;
        return this.httpClient.get(url, {}).toPromise();
    }
    getWeightGraph() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.gerWeightGraph;
        return this.httpClient.get(url, {}).toPromise();
    }
    sendMail(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.sendMail;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getCouponList() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.getCouponList;
        // return this.httpClient.get(url, {}).toPromise();
        return this.httpClient.get(url).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    subscribePlanByCoupon(reqData) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.subscribePlanByCoupon;
        return this.httpClient.post(url, reqData).toPromise();
    }
    getOptions(slot, isDetox, country, email) {
        // country = country ? country : "IND";
        let url;
        // if(this.isNew){
        //   let reqBody = {
        //     "slot": slot,
        //     "customerId": CONSTANTS.email
        //   }
        //   url = APP.baseurl + "" + APIS.optionsData+ `?slot=${slot}&userId=${email}`;
        //   return this.httpClient.post(url, reqBody, {}).toPromise();
        // }else{
        url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.optionsData + `?slot=${slot}&userId=${email}`
            // + "&detox=" + isDetox
            + "&country=" + country;
        return this.httpClient.get(url, {}).toPromise();
        // }
    }
    getOptionsNew(slot, isDetox, country, email) {
        let url;
        url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "api/" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.optionsData + `?slot=${slot}&userId=${email}`;
        return this.httpClient.get(url, {}).toPromise();
        // }
    }
    terms(reqData) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.addTnC;
        return this.httpClient.post(url, reqData).toPromise();
    }
    getOnePlan1(token) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.getOnePlan;
        console.log("manual header", token);
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders().set("authorization", `bearer ${token}`);
        return this.httpClient1.get(url, { headers: headers })?.toPromise();
    }
    getOnePlan() {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.getOnePlan;
        return this.httpClient.get(url, {})?.toPromise();
    }
    copyDietPlan(userId, fromDate, toDate) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.copyDietPlanWeekly + `?userId=${userId}&fromDate=${fromDate}&toDate=${toDate}&range=7`;
        return this.httpClient.post(url, {})?.toPromise();
    }
    googoleFit(userid) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.googleFit + `${userid}/dataset:aggregate`;
        return this.httpClient.post(url, {}, {})?.toPromise();
        //
    }
    waterDrank(data) {
        return this.httpClient.post(_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + 'customer/water/drank', data, {})
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    remindertoggle() {
        return this.httpClient.get(_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + 'fetch/water/reminder', {})
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    reminderupdate(data) {
        return this.httpClient.post(_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + 'customer/water/reminder', data, {})
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    tips() {
        return this.httpClient.get(_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + 'water/tips ', {})
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    user() {
        return this.httpClient.get(_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + 'customer/water/recommendation')
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    calories() {
        return this.httpClient.get(_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + 'fetch/target/calories').pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    refresh(reqBody, email) {
        _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.country = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.country ? _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.country : "IND";
        let url;
        if (this.isNew) {
            reqBody["userId"] = email;
            url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshInternational + `?userId=${email}`;
        }
        else {
            if (_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.country != "IND") {
                reqBody["userId"] = email;
            }
            else {
                delete reqBody.country;
            }
            url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.country != "IND" ? _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshInternational + `?userId=${email}` : _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refresh + `?userId=${email}`;
        }
        // let url = CONSTANTS.country != "IND" ? "https://test.fightitaway.com:8443/api/dietPlans/refresh/options" : APIS.BASEURL + APIS.refresh;
        //
        return this.httpClient.post(url, reqBody, {}).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    fetchFood(reqBody, email) {
        let url;
        if (this.isNew) {
            reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
            reqBody["date"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.dietDate;
            reqBody["country"] = "IND";
            url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.fetchFood + `?userId=${email}`;
        }
        else {
            url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.fetchFood;
        }
        return this.httpClient.post(url, reqBody, {}).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    searchFoodItem(name) {
        let url;
        url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurlApi + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.searchFoodItem + `?foodName=${name}&page=1`;
        return this.httpClient.post(url, {}, {}).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    getWordPressCategory(categoryId) {
        return this.httpClient.get(`${_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.WP_BASEURL}?categories=${categoryId}&page=1&per_page=100&_embed`)
            .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    getCurrentLocation() {
        return this.httpClient.get('https://geolocation-db.com/json/', {}).toPromise();
    }
    fetchDietPlan() {
        return this.httpClient.get(_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.fetchDietPlans, {}).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    getFAQ() {
        return this.httpClient.get(_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.fetchHelp, {}).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    getByPostId(id) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.WP_BASEURL + "?include[]=" + id;
        return this.httpClient.get(url, {}).toPromise();
    }
    updateDetoxStatus(reqBody) {
        return this.httpClient.post(_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.detoxStatus, reqBody, {}).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_5__.map)(results => results));
    }
    updateExpiryDate(reqBody) {
        return this.httpClient.post(_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateExpiryDate, reqBody, {}).toPromise();
    }
    updateCurrentWeight(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateCurrentWeigt;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    updateTargetWeight(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateTargetWeight;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getDietTimings() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.timings;
        return this.httpClient.get(url, {}).toPromise();
    }
    getCouponListOffered() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.getCouponListOffered;
        return this.httpClient.get(url, {}).toPromise();
    }
    getRecipeOfTheDay() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.getRecipeOfTheDay;
        return this.httpClient.get(url, {}).toPromise();
    }
    getSubFoodItems(reqBody) {
        reqBody.customerId = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        // const url = APIS.BASEURL + "" + APIS.lessThan100SlotsFoodItem;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.lessThan100SlotsFoodItemNew;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getLessThan100CaloriesFoodItem() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.lessThan100CaloriesFoodItem;
        return this.httpClient.get(url, {}).toPromise();
    }
    getHighProteinFoodItem() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.highProteinFoodItem;
        return this.httpClient.get(url, {}).toPromise();
    }
    getHealthyChociesFoodItem() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.healthyChoicesFoodItem;
        return this.httpClient.get(url, {}).toPromise();
    }
    doUpdateCustDietPlan(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateCustDietPlan;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getReferralCode(referralCode) {
        let reqBody = {};
        reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.referralCode + "?referralCode=" + referralCode;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    doReferralUser(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.referralUser;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    updateReferralUser(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateReferralUser;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getReferralUserTransactions(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.referralUserTransactions;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    postRatings(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.ratings;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getDietPreference(payload) {
        const baseurl = 'https://app.smartdietplanner.com:8444/';
        const url = baseurl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.getDietPreference + `${payload.userId}`;
        return this.httpClient.get(url, {}).toPromise();
    }
    updateDietPref(userid, reqBody) {
        const baseurl = 'https://app.smartdietplanner.com:8444/';
        const url = baseurl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateDietPref + "?userId=" + userid;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    fetchTodoList() {
        let reqBody = {
            "customerId": _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email //CONSTANTS.email//CONSTANTS.email
        };
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.fetchTodoList;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    saveOrUpdateCustDailyTodo(reqBody) {
        reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.saveOrUpdateCustDailyTodo;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    saveHotLeads(reqBody) {
        reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.saveHotLeads;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    deleteHotLeads(reqBody) {
        reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.deleteHotLeads;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    updateProfile(reqBody) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.BASEURL + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateProfile;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getFoodPrefHistory(slot) {
        let reqBody = {};
        reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        reqBody["slot"] = slot;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.getFoodPrefHistory;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    searchFoodItemByName(searchText, token) {
        let reqBody = {};
        reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + `searchFoodItem?foodName=${searchText}&page=1`;
        const headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders().set("authorization", `bearer ${token}`);
        return this.httpClient1.post(url, {}, { headers: headers }).toPromise();
    }
    updateDietPlan(reqBody) {
        console.log(reqBody);
        reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateDietPlan;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    updateEatenFoodItems(reqBody) {
        reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.updateEatenFoodItems;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    // async saveEncrypt(_data) {
    // 	return await CryptoJS.AES.encrypt(JSON.stringify(_data), CONSTANTS.encryptKey).toString();
    // }
    // async getDecrypt(data) {
    //   let _data = data.toString().replace(/p1L2u3S/g, '+' ).replace(/s1L2a3S4h/g, '/').replace(/e1Q2u3A4l/g, '=');
    // 	const bytes = CryptoJS.AES.decrypt(_data, CONSTANTS.encryptKey);
    // 	if (bytes.toString()) {
    // 		return await JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    // 	}
    // }
    postSaveFastingDetails(reqBody) {
        reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.postSaveFastingDetails;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getFacts() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.postFacts;
        return this.httpClient.post(url, { customerId: _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email, currentLot: 1, lotSize: 100 }, {}).toPromise();
    }
    postFavoriteFacts(factId) {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.postFavorite;
        return this.httpClient.post(url, { customerId: _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email, factId: factId }, {}).toPromise();
    }
    getRecipies(reqBody) {
        reqBody["customerId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        // reqBody["date"] = "06122021";
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.getRecipies;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getAffiliate() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.fetchAffiliate + `?userId=${_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email}`;
        return this.httpClient.get(url, {}).toPromise();
    }
    sendOTP(reqBody) {
        // reqBody["customerId"] = reqBody.email;
        console.log("reqBody", reqBody);
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.sendOTP;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    verifyOTP(reqBody) {
        console.log("reqBody", reqBody);
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.verifyOTP;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    fetchOrder(reqBody) {
        reqBody["userId"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.fetchOrder;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    createOrder(reqBody) {
        // reqBody["customerId"] = CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.createOrder;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getSurvey() {
        const url = './assets/stub/survey_master.json';
        return this.httpClient.get(url, {}).toPromise();
    }
    getExercise() {
        const url = './assets/stub/exercise.json';
        return this.httpClient.get(url, {}).toPromise();
    }
    postSurvey(reqBody) {
        reqBody["_id"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.storeSurveyResponse;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    saveFasting(reqBody) {
        reqBody["_id"] = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email;
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.saveFasting;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getFasting() {
        const url = _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.refreshBaseUrl + "" + _core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.APIS.getFasting;
        return this.httpClient.get(url, {}).toPromise();
    }
    postCopyDietPlan(reqBody, fromDate, toDate, range) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + `copyDietPlan?userId=${_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email}&fromDate=${fromDate}&toDate=${toDate}&range=${range}`;
        return this.httpClient.post(url, reqBody, {}).toPromise();
    }
    getRefreshDietPlan(date) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + `fetchDietPlan?userId=${_core_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.email}&date=${date}&refresh=true`;
        return this.httpClient.get(url, {}).toPromise();
    }
    getDietRecall(userId) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + `${_shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.SUBAPIS.getDietRecall}${userId}`;
        return this.httpClient.get(url, {}).toPromise();
    }
    saveDietRecall(userId, payload) {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.APIS.baseurl + `${_shared_constants_constants__WEBPACK_IMPORTED_MODULE_2__.SUBAPIS.saveDietRecall}${userId}`;
        return this.httpClient.post(url, payload, {}).toPromise();
    }
};
AppService.ctorParameters = () => [
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_0__.UTILITIES },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_6__.Storage },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpBackend }
];
AppService = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Injectable)({
        providedIn: "root"
    })
], AppService);



/***/ }),

/***/ 22298:
/*!******************************************************!*\
  !*** ./src/app/change-plan/change-plan.component.ts ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ChangePlanComponent": () => (/* binding */ ChangePlanComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_change_plan_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./change-plan.component.html */ 78962);
/* harmony import */ var _change_plan_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./change-plan.component.scss */ 64651);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core/utility/utilities */ 53533);
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app.service */ 38198);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/storage */ 78713);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../core/constants/constants */ 92133);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ 29243);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _select_plan_popup_select_plan_popup_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../select-plan-popup/select-plan-popup.component */ 98522);













let ChangePlanComponent = class ChangePlanComponent {
    constructor(utilities, appService, storage, router, modalController) {
        this.utilities = utilities;
        this.appService = appService;
        this.storage = storage;
        this.router = router;
        this.modalController = modalController;
        this.muscleMaleImg = "../../assets/img/muscle-male.png";
        this.muscleFemaleImg = "../../assets/img/muscle-female.png";
        this.fatMaleImg = "../../assets/img/fat-male.png";
        this.fatFemaleImg = "../../assets/img/fat-female.png";
        this.isIosDevice = this.utilities.isDeviceiOS();
        this.containPlanTypes = [];
        this.selectedPlan = "weightLoss";
        this.isPlanLoaded = false;
        this.isPlanActiveForDiet = _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.isPlanActiveParent;
        this.activitySlider = [];
        // upgradePlan() {
        //   this.router.navigate(["upgrade-plan"]);
        // }
        this.selectPlanFromDiseases = false;
        this.userName = "";
        this.suggestedClories = 0;
        this.diseases = [];
        this.expandPlanCards = false;
    }
    ngOnInit() {
        let activityArr = [];
        let selectedDietPlan = _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.selectedDietPlan;
        if (['post_covid', 'diabetes', 'hypertension'].includes(selectedDietPlan)) {
            activityArr = _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.constantsJson.premiumPlanInculdes.filter((ele) => {
                return ele.title != "Detox Plan";
            });
            this.activitySlider = activityArr;
        }
        else {
            this.activitySlider = _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.constantsJson.premiumPlanInculdes;
        }
        this.dataInit();
    }
    gotoEditPlanSelection(event, isType, plan, note) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            event.stopPropagation();
            if (isType == this.selectedPlan)
                return;
            let me = this;
            const modal = yield me.modalController.create({
                component: _select_plan_popup_select_plan_popup_component__WEBPACK_IMPORTED_MODULE_6__.SelectPlanPopupComponent,
                backdropDismiss: true,
                cssClass: 'app-offer-popup',
                componentProps: { planType: isType, plan: plan, isPremium: this.isPlanActiveForDiet, note: note }
            });
            modal.onDidDismiss().then((data) => {
                if (data && data.data && data.data.isActivate) {
                    this.selectePlan(data.data.planId);
                }
            });
            return yield modal.present();
        });
    }
    dataInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            let planTypes = _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.constantsJson.diet_plans;
            planTypes = planTypes.filter((ele) => {
                return ele.isVisible;
            });
            let tempPlanTypes;
            let self = this;
            this.storage.get("profileData").then((res) => {
                let profileData = JSON.parse(res);
                if (profileData.demographic.gender.value.toLowerCase() == 'male') {
                    tempPlanTypes = planTypes.filter((ele) => {
                        if (ele.id.toLowerCase() != "pcos") {
                            return ele;
                        }
                    });
                }
                else {
                    tempPlanTypes = planTypes;
                }
                this.userName = profileData["profile"]["name"];
                this.suggestedClories = profileData["lifeStyle"]["calories"];
                if (profileData && profileData["lifeStyle"] && profileData["lifeStyle"]["dietPlanName"]) {
                    self.selectedPlan = profileData["lifeStyle"]["dietPlanName"];
                }
                else {
                    self.selectedPlan = "weightLoss";
                }
                if (this.diseases.length > 0) {
                    let isDiabetes, isPCOD, isCholestrol, isBloodPressure;
                    this.diseases.forEach((ele) => {
                        if (ele.code == "D")
                            isDiabetes = true;
                        else if (ele.code == "P")
                            isPCOD = true;
                        else if (ele.code == "C")
                            isCholestrol = true;
                        else if (ele.code == "B")
                            isBloodPressure = true;
                    });
                    tempPlanTypes.unshift(tempPlanTypes.splice(tempPlanTypes.findIndex(item => item.id === "weightLoss"), 1)[0]);
                    tempPlanTypes.unshift(tempPlanTypes.splice(tempPlanTypes.findIndex(item => item.id === "weightLossPlus"), 1)[0]);
                    if (isBloodPressure)
                        tempPlanTypes.unshift(tempPlanTypes.splice(tempPlanTypes.findIndex(item => item.id === "hypertension"), 1)[0]); //.sort((a, b) =>{ return (a.id != 'hypertension' ? 1 : -1)});
                    if (isCholestrol)
                        tempPlanTypes.unshift(tempPlanTypes.splice(tempPlanTypes.findIndex(item => item.id === "cholesterol"), 1)[0]); //sort((a, b) =>{ return (a.id != 'cholesterol' ? 1 : -1) });
                    if (isPCOD)
                        tempPlanTypes.unshift(tempPlanTypes.splice(tempPlanTypes.findIndex(item => item.id === "pcos"), 1)[0]); //tempPlanTypes.sort((a, b) =>{ return (a.id != 'pcos' ? 1 : -1) });
                    if (isDiabetes)
                        tempPlanTypes.unshift(tempPlanTypes.splice(tempPlanTypes.findIndex(item => item.id === "diabetes"), 1)[0]); //tempPlanTypes.sort((a, b) =>{ return (a.id != 'diabetes' ? 1 : -1) });
                    if (this.selectedPlan == 'hypertension' || this.selectedPlan == 'cholesterol' ||
                        this.selectedPlan == 'pcos' || this.selectedPlan == 'diabetes')
                        this.selectPlanFromDiseases = true;
                    this.planTypes = tempPlanTypes;
                }
                else {
                    tempPlanTypes.unshift(tempPlanTypes.splice(tempPlanTypes.findIndex(item => item.id === "weightLoss"), 1)[0]);
                    tempPlanTypes.unshift(tempPlanTypes.splice(tempPlanTypes.findIndex(item => item.id === "weightLossPlus"), 1)[0]);
                    this.planTypes = tempPlanTypes;
                }
                this.planTypes.unshift(this.planTypes.splice(this.planTypes.findIndex(item => item.id === self.selectedPlan), 1)[0]);
                this.planTypes.filter((ele) => {
                    if (ele.isGenderCheck) {
                        if (ele.id == 'fatShredding_morning') {
                            if (profileData.demographic.gender.value.toLowerCase() == 'male') {
                                ele['img'] = this.fatMaleImg;
                                return ele;
                            }
                            else {
                                ele['img'] = this.fatFemaleImg;
                                return ele;
                            }
                        }
                        else if (ele.id == 'muscleGain_morning') {
                            if (profileData.demographic.gender.value.toLowerCase() == 'male') {
                                ele['img'] = this.muscleMaleImg;
                                return ele;
                            }
                            else {
                                ele['img'] = this.muscleFemaleImg;
                                return ele;
                            }
                        }
                    }
                    else
                        return ele;
                });
                this.containPlanTypes = this.planTypes;
            });
        });
    }
    ionViewWillEnter() {
        // this.getPlanStatus();
        // this.dataInit();
        // this.storage.get("localData").then(val => {
        //   if (val != "") {
        //     let data = JSON.parse(val);
        //     this.diseases = data.otherMaster.diseases.filter(item=>{
        //       return item.isSelected==true;
        //     });
        //     this.localData = data;
        //     this.dataInit();
        //   }else{
        this.dataInit();
        // }
        // })
    }
    toggelExpandCards(flag) {
        if (!this.expandPlanCards || flag)
            this.expandPlanCards = !this.expandPlanCards;
    }
    selectePlan(isType) {
        //  this.utilities.showLoading();
        let req = {
            "dietPlanName": isType
        };
        this.appService.doUpdateCustDietPlan(req).then(success => {
            this.appService.getProfile().then(profileData => {
                console.log("profileData,", profileData);
                //  this.storage.get("localData").then((defaultData)=>{
                //   defaultData = JSON.parse(defaultData);
                if (profileData && profileData["profile"] && profileData["profile"].dietPlanType) {
                    //   defaultData.otherMaster.type.filter((ele) => {
                    //     if(profileData["profile"].dietPlanType == ele.name) return ele.isSelected = true;
                    //   })
                }
                if (profileData && profileData["lifeStyle"] && profileData["lifeStyle"].prefWorkOutTime) {
                    //   defaultData.otherMaster.workoutTime.filter((ele) => {
                    //    if(profileData["lifeStyle"].prefWorkOutTime == ele.name) return ele.isSelected = true;
                    //   })
                }
                //    this.storage.set("localData", this.utilities.parseString(defaultData)).then(()=>{
                this.storage.set("profileData", this.utilities.parseString(profileData)).then(() => {
                    //   this.storage.set("dietData", null).then(()=>{
                    //       this.storage.set("optionsData", null).then(()=>{
                    _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.dietDate = moment__WEBPACK_IMPORTED_MODULE_5___default()().format("DDMMYYYY");
                    localStorage.setItem("profileData", JSON.stringify(profileData));
                    this.router.navigate(["dietplan"]);
                });
                //   })
                //   })
                //  });
            });
            // })
        }, error => {
            this.utilities.hideLoader();
            console.log("DietPlan Error:", error);
            this.utilities.presentAlert("Errror...");
        });
    }
};
ChangePlanComponent.ctorParameters = () => [
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__.UTILITIES },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_3__.AppService },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_8__.Storage },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.ModalController }
];
ChangePlanComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-change-plan',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_change_plan_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_change_plan_component_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ChangePlanComponent);



/***/ }),

/***/ 93546:
/*!*********************************************************************************!*\
  !*** ./src/app/components/add-update-customer/add-update-customer.component.ts ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddUpdateCustomerComponent": () => (/* binding */ AddUpdateCustomerComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_add_update_customer_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./add-update-customer.component.html */ 51292);
/* harmony import */ var _add_update_customer_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-update-customer.component.scss */ 98664);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/app.service */ 38198);
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ 3242);






let AddUpdateCustomerComponent = class AddUpdateCustomerComponent {
    constructor(appService, iab) {
        this.appService = appService;
        this.iab = iab;
        this.currentCountryCode = "in";
        this.isMobile = false;
    }
    getValue(event) {
        if (event.detail.value == "Mobile") {
            this.isMobile = true;
        }
        else {
            this.isMobile = false;
        }
    }
    ngOnInit() { }
    getToken() {
        let payload = {
            "profile": {
                "name": this.name,
                "email": this.emailDetail
            }
        };
        let payload1 = {
            "profile": {
                "name": this.name,
                "mobile": this.phoneNumber == undefined ? "" : this.phoneNumber.internationalNumber,
            }
        };
        console.log("payload", this.phoneNumber);
        this.appService.externalRegistration("HLTH22531", this.isMobile ? payload1 : payload).subscribe(res => {
            console.log("tee:", localStorage.getItem('tkn'));
            this.iab.create(`https://onboarding.smartdietplanner.com/read?token=${localStorage.getItem('tkn')}`, '_system');
            //  window.open(`https://app.smartdietplanner.com?token=${localStorage.getItem('tkn')}`,'_blank');
        });
    }
};
AddUpdateCustomerComponent.ctorParameters = () => [
    { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__.InAppBrowser }
];
AddUpdateCustomerComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-add-update-customer',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_add_update_customer_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_add_update_customer_component_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], AddUpdateCustomerComponent);



/***/ }),

/***/ 57693:
/*!*************************************************!*\
  !*** ./src/app/components/components.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ComponentsModule": () => (/* binding */ ComponentsModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _components_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./components.routing.module */ 81375);
/* harmony import */ var _home_vertical_home_vertical_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home-vertical/home-vertical.component */ 79645);
/* harmony import */ var _home_kcounter_home_kcounter_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home-kcounter/home-kcounter.component */ 28373);
/* harmony import */ var _home_dietplan_home_dietplan_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home-dietplan/home-dietplan.component */ 92406);
/* harmony import */ var _home_water_home_water_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home-water/home-water.component */ 99737);
/* harmony import */ var ng_circle_progress__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ng-circle-progress */ 91756);
/* harmony import */ var _add_update_customer_add_update_customer_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./add-update-customer/add-update-customer.component */ 93546);
/* harmony import */ var ion_intl_tel_input__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ion-intl-tel-input */ 39458);








// import {HomeStepsComponent} from './home-steps/home-steps.component';





let ComponentsModule = class ComponentsModule {
};
ComponentsModule = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_8__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__.ReactiveFormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.IonicModule,
            ion_intl_tel_input__WEBPACK_IMPORTED_MODULE_11__.IonIntlTelInputModule,
            _components_routing_module__WEBPACK_IMPORTED_MODULE_0__.ComponentsRoutingModule,
            ng_circle_progress__WEBPACK_IMPORTED_MODULE_12__.NgCircleProgressModule.forRoot({
                animation: false,
                radius: 49,
                innerStrokeWidth: 6,
                outerStrokeWidth: 6,
                space: -6,
                responsive: false,
                showTitle: true,
                titleFontSize: "15",
                subtitleFontSize: "15",
                unitsFontSize: "15",
                renderOnClick: false
            })
        ],
        exports: [
            _home_vertical_home_vertical_component__WEBPACK_IMPORTED_MODULE_1__.HomeVerticalComponent,
            // HomeStepsComponent,
            _home_kcounter_home_kcounter_component__WEBPACK_IMPORTED_MODULE_2__.HomeKcounterComponent,
            _home_dietplan_home_dietplan_component__WEBPACK_IMPORTED_MODULE_3__.HomeDietplanComponent,
            _home_water_home_water_component__WEBPACK_IMPORTED_MODULE_4__.HomeWaterComponent,
            _add_update_customer_add_update_customer_component__WEBPACK_IMPORTED_MODULE_5__.AddUpdateCustomerComponent
        ],
        declarations: [
            _home_vertical_home_vertical_component__WEBPACK_IMPORTED_MODULE_1__.HomeVerticalComponent,
            // HomeStepsComponent,
            _home_kcounter_home_kcounter_component__WEBPACK_IMPORTED_MODULE_2__.HomeKcounterComponent,
            _home_dietplan_home_dietplan_component__WEBPACK_IMPORTED_MODULE_3__.HomeDietplanComponent,
            _home_water_home_water_component__WEBPACK_IMPORTED_MODULE_4__.HomeWaterComponent,
            _add_update_customer_add_update_customer_component__WEBPACK_IMPORTED_MODULE_5__.AddUpdateCustomerComponent
        ],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_7__.CUSTOM_ELEMENTS_SCHEMA]
    })
], ComponentsModule);



/***/ }),

/***/ 81375:
/*!*********************************************************!*\
  !*** ./src/app/components/components.routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ComponentsRoutingModule": () => (/* binding */ ComponentsRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 13252);



const routes = [];
let ComponentsRoutingModule = class ComponentsRoutingModule {
};
ComponentsRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule],
    })
], ComponentsRoutingModule);



/***/ }),

/***/ 92406:
/*!*********************************************************************!*\
  !*** ./src/app/components/home-dietplan/home-dietplan.component.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomeDietplanComponent": () => (/* binding */ HomeDietplanComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_dietplan_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./home-dietplan.component.html */ 274);
/* harmony import */ var _home_dietplan_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home-dietplan.component.scss */ 95425);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);




let HomeDietplanComponent = class HomeDietplanComponent {
    constructor() {
        this.isDetox = false;
        this.suggestedCalories = {
            totalCalories: 0,
            recomended: 0,
            totalCarbs: 0,
            totalCarbsPer: 0,
            totalFat: 0,
            totalFatPer: 0,
            totalFiber: 0,
            totalFiberPer: 0,
            totalProtien: 0,
            totalProtienPer: 0,
            minus10: 0,
            plus10: 0,
            minus1010: 0
        };
        this.isAnalysisPageVisited = false;
        this.selectedThemeColor = '';
        this.detoxThemeColor = this.selectedThemeColor;
    }
    ngOnInit() { }
    roundingVal(val) {
        if (isNaN(val)) {
            return 0;
        }
        return Math.round(val);
    }
};
HomeDietplanComponent.ctorParameters = () => [];
HomeDietplanComponent.propDecorators = {
    isDetox: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    suggestedCalories: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    selectedDietPlan: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    isAnalysisPageVisited: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    detoxMaxValue: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    selectedThemeColor: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    detoxThemeColor: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }]
};
HomeDietplanComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-home-dietplan',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_dietplan_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_home_dietplan_component_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], HomeDietplanComponent);



/***/ }),

/***/ 28373:
/*!*********************************************************************!*\
  !*** ./src/app/components/home-kcounter/home-kcounter.component.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomeKcounterComponent": () => (/* binding */ HomeKcounterComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_kcounter_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./home-kcounter.component.html */ 65754);
/* harmony import */ var _home_kcounter_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home-kcounter.component.scss */ 20051);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);




let HomeKcounterComponent = class HomeKcounterComponent {
    constructor() {
        this.currentDateIndex = 0;
        this.totalTodaysCaloriesPerc = 0;
        this.tolalCalories = 0;
        this.habitList = [];
        this.totalTodaysCalories = 0;
        console.log("currentDateIndex", this.currentDateIndex);
    }
    ngOnInit() { }
    roundingVal(val) {
        if (isNaN(val)) {
            return 0;
        }
        return Math.round(val);
    }
};
HomeKcounterComponent.ctorParameters = () => [];
HomeKcounterComponent.propDecorators = {
    currentDateIndex: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['currentDateIndex',] }],
    totalTodaysCaloriesPerc: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['totalTodaysCaloriesPerc',] }],
    tolalCalories: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['tolalCalories',] }],
    habitList: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['habitList',] }],
    totalTodaysCalories: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }]
};
HomeKcounterComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-home-kcounter',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_kcounter_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_home_kcounter_component_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], HomeKcounterComponent);



/***/ }),

/***/ 79645:
/*!*********************************************************************!*\
  !*** ./src/app/components/home-vertical/home-vertical.component.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomeVerticalComponent": () => (/* binding */ HomeVerticalComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_vertical_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./home-vertical.component.html */ 90848);
/* harmony import */ var _home_vertical_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home-vertical.component.scss */ 42685);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../core/utility/utilities */ 53533);
/* harmony import */ var _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/constants/constants */ 92133);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/storage */ 78713);
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app.service */ 38198);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ 29243);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ 3242);
/* harmony import */ var _recipe_day_recipe_day_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../recipe-day/recipe-day.component */ 95642);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/platform-browser */ 86219);














let counterSwipe = 0;
let HomeVerticalComponent = class HomeVerticalComponent {
    constructor(platform, router, storage, utilities, appService, cdr, loadingController, toastController, el, _sanitizer, iab, appServices, route) {
        this.platform = platform;
        this.router = router;
        this.storage = storage;
        this.utilities = utilities;
        this.appService = appService;
        this.cdr = cdr;
        this.loadingController = loadingController;
        this.toastController = toastController;
        this.el = el;
        this._sanitizer = _sanitizer;
        this.iab = iab;
        this.appServices = appServices;
        this.route = route;
        this.refreshDiet = new _angular_core__WEBPACK_IMPORTED_MODULE_8__.EventEmitter();
        this.removeItem = new _angular_core__WEBPACK_IMPORTED_MODULE_8__.EventEmitter();
        this.kcounter = new _angular_core__WEBPACK_IMPORTED_MODULE_8__.EventEmitter(true);
        this.isChecked = true;
        this.toggleButtons = [];
        this.toggleButtonsNormal = [
            { "text": "Normal Plan", "isChecked": true, "color": "#01A3A4", plan: "weightLoss" },
            { "text": "Detox Plan", "isChecked": false, "color": "#4CB271", plan: "detox" }
        ];
        this.toggleButtonsFat = [
            { "text": "Tone Up", "isChecked": true, "color": "#FD980F", plan: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedDietPlan },
            { "text": "Detox Plan", "isChecked": false, "color": "#4CB271", plan: "detox" }
        ];
        this.toggleButtonsMuscle = [
            { "text": "Muscle Building", "isChecked": true, "color": "#0B94C1", plan: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedDietPlan },
            { "text": "Detox Plan", "isChecked": false, "color": "#4CB271", plan: "detox" }
        ];
        this.toggleButtonsImmunity = [
            { "text": "Immunity Plan", "isChecked": true, "color": "#FD9F33", plan: "immunity_booster" },
            { "text": "Detox Plan", "isChecked": false, "color": "#4CB271", plan: "detox" }
        ];
        this.toggleButtonsNormalDetox = [
            { "text": "Normal Plan", "isChecked": false, "color": "#01A3A4", plan: "weightLoss" },
            { "text": "Detox Plan", "isChecked": true, "color": "#4CB271", plan: "detox" }
        ];
        this.toggleButtonsImmunityDetox = [
            { "text": "Immunity Plan", "isChecked": false, "color": "#FD9F33", plan: "immunity_booster" },
            { "text": "Detox Plan", "isChecked": true, "color": "#4CB271", plan: "detox" }
        ];
        this.toggleButtonsCholesterol = [
            { "text": "Cholesterol Plan", "isChecked": true, "color": "#A31E79", plan: "cholesterol" },
            { "text": "Detox Plan", "isChecked": false, "color": "#4CB271", plan: "detox" }
        ];
        this.toggleButtonsCholesterolDetox = [
            { "text": "Cholesterol Plan", "isChecked": false, "color": "#A31E79", plan: "cholesterol" },
            { "text": "Detox Plan", "isChecked": true, "color": "#4CB271", plan: "detox" }
        ];
        this.streamVideoM = false;
        this.months = [
            { ind: 1, month: "JAN" },
            { ind: 2, month: "FEB" },
            { ind: 3, month: "MAR" },
            { ind: 4, month: "APR" },
            { ind: 5, month: "MAY" },
            { ind: 6, month: "JUN" },
            { ind: 7, month: "JUL" },
            { ind: 8, month: "AUG" },
            { ind: 9, month: "SEP" },
            { ind: 10, month: "OCT" },
            { ind: 11, month: "NOV" },
            { ind: 12, month: "DEC" }
        ];
        this.isNotCordova = true;
        this.isPlanActiveForDiet = false; // lock and unlock icon
        this.isRandomLock = false;
        this.appleHealthKitPermission = false;
        this.isIosDevice = this.utilities.isDeviceiOS();
        this.isAndroidDevice = this.utilities.isDeviceAndroid();
        this.isDetox = _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox;
        this.detoxToggle = false;
        this.healthProblem = false;
        this.deficit = 0;
        this.firstCofficient = 0;
        this.secondCofficient = 0;
        this.showDeficitGraph = true;
        this.weeks = [];
        this.dayValue = "";
        this.showSourceInfo = false;
        this.googleFitConfigure = true;
        this.googleFitConfigureCalories = false;
        this.isToday = true;
        this.waterPercentage = 0;
        this.consmedAtCurrentTime = 0;
        this.deficitToday = 0;
        this.deficitTodayPer = 0;
        this.deficitTotal = 500;
        this.dietTimings = [];
        this.noNextDate = false;
        this.noPrevDate = true;
        this.defaultCircleFillColor = true;
        this.hasAnimation = 0;
        this.currentDateIndex = 0;
        this.selectedCountry = "IND";
        this.isPullReferesh = false;
        // isTypeCovid = false;
        this.selectedDietPlan = "weightLoss";
        this.streamVideo = false;
        this.isTodayDietPlan = true;
        this.tolalCalories = 0;
        this.countdownMins = "00";
        this.countdownSec = "00";
        this.countdownHours = "00";
        this.loadingMsgImgTime = 1000;
        this.loadingMsgTime = 4000;
        this.detoxMaxValue = 1000;
        this.checkSurveyInterval = 10 * 60 * 1000; // 10 mins
        this.toggleTodayCalCount = [
            { "text": "Counter", "isChecked": false, "color": "#01A3A4" },
            { "text": "Plan", "isChecked": true, "color": "#01A3A4" }
        ];
        this.slideOptsVideo = {
            initialSlide: 0,
            slidesPerView: 1.2,
            loop: false,
            centeredSlides: false,
            spaceBetween: 1
        };
        this.slideOptsTransformation = {
            initialSlide: 0,
            slidesPerView: 1.2,
            loop: true,
            centeredSlides: false,
            spaceBetween: 1
        };
        this.whySDPIndex = 0;
        this.whySDPData = [];
        this.videoLandscape = false;
        this.isAnalysisPageVisited = false;
        this.slideOptsBottom = {
            initialSlide: 0,
            slidesPerView: 2.50,
            loop: false,
            centeredSlides: false,
            slidesOffsetBefore: 20,
            slidesOffsetAfter: 50,
            spaceBetween: 5,
            observer: true,
            observeParents: true,
            on: {
                beforeInit() {
                    this.swiper = this;
                    this.swiper.on('slideChangeTransitionStart', function () {
                        let $wrapperEl = this.swiper.$wrapperEl;
                        let params = this.swiper.params;
                        $wrapperEl.children(('.' + (params.slideClass) + '.' + (params.slideDuplicateClass)))
                            .each(function () {
                            let idx = this.getAttribute('data-swiper-slide-index');
                            this.innerHTML = $wrapperEl.children('.' + params.slideClass + '[data-swiper-slide-index="' + idx + '"]:not(.' + params.slideDuplicateClass + ')').html();
                        });
                    });
                    this.swiper.on('slideChangeTransitionEnd', function () {
                        this.swiper.slideToLoop(this.swiper.realIndex, 0, false);
                    });
                }
            }
        };
        this.isGoogleFitCaloryZero = false;
        this.fixScale = 1;
        this.isPinkText = false;
        this.loadAgain = false;
        this.lessThan100FoodItems = [];
        this.lessThan100FoodItemsAtTime = [];
        this.lessThan100FoodItemsCounter = 5;
        this.lessThan100FoodItemSlotSelected = 2;
        this.filteredLess100Food = {};
        this.highProteinFoodItems = [];
        this.highProteinFoodItemsAtTime = [];
        this.highProteinFoodItemsCounter = 5;
        this.highProteinFoodItemSlotSelected = 2;
        this.filteredHighProtein = {};
        this.healthyChoicesFoodItems = [];
        this.healthyChoicesFoodItemsAtTime = [];
        this.healthyChoicesFoodItemsCounter = 5;
        this.healthyChoicesFoodItemSlotSelected = 2;
        this.filteredHealthyChoices = {};
        // IOS inapp purchase keys
        this.productIDs = ['smart_diet_planner_monthly_app', 'smart_diet_planner_quarterly_app', 'smart_diet_planner_half_yearly_app', 'smart_diet_planner_yearly_app'];
        // refferProductIDs = ['6_Months_offer', 'one_year_referral', 'smart_diet_planner_monthly_app', 'smart_diet_planner_quarterly_app', 'smart_diet_planner_half_yearly_app', 'smart_diet_planner_yearly_app'];
        this.refferProductIDs = ['referral_6_months', 'one_year_referral', 'smart_diet_planner_monthly_app', 'smart_diet_planner_quarterly_app', 'smart_diet_planner_half_yearly_app', 'smart_diet_planner_yearly_app'];
        // Android inapp purchase keys
        this.androidProductIDs = ['smart_diet_planner_monthly', 'smart_diet_planner_quarterly', 'smart_diet_planner_half_yearly', 'smart_diet_planner_yearly'];
        this.refferAndroidProductIDs = ['referral_6_months', 'yearly_offer_referral', 'smart_diet_planner_monthly', 'smart_diet_planner_quarterly', 'smart_diet_planner_half_yearly', 'smart_diet_planner_yearly'];
        // Gauge meter options
        this.canvasWidth = 150;
        this.needleValue = 50;
        this.centralLabel = '';
        this.readyForChange = true;
        this.options = {
            hasNeedle: true,
            needleColor: 'black',
            needleUpdateSpeed: 1000,
            arcColors: ["#0BB852", "#FFF", "#8BC73C", "#FFF", "#FFAE00", "#FFF", "#FF791F", "#FFF", "#FF2441"],
            arcDelimiters: [14, 15, 29, 30, 49, 50, 74, 75],
            needleStartValue: 0,
        };
        this.onFasting = false;
        this.fastingCountdown = '00:00:00';
        this.isTimerRed = false;
        this.isWaterNotificationEnable = false;
        this.isFastingNotificationEnable = false;
        this.insertItemInArrayOnSpecificIndex = (arr, index, newItem) => [
            // part of the array before the specified index
            ...arr.slice(0, index),
            // inserted item
            newItem,
            // part of the array after the specified index
            ...arr.slice(index)
        ];
        this.tipMessage = [
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<span class="two-lines-noti-msg">Hi, I am your personal health coach !</span>',
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<span class="two-lines-noti-msg">Today\'s  personalised  <b>Diet plan</b>  is ready</span>',
            // '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            // '<span class="two-lines-noti-msg"> <span class="custom-link-color" clickId="Activate">Activate</span> your Google Fit to keep track of your steps and calories burnt.</span>',
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<span class="two-lines-noti-msg">I suggest 12 glasses of water for you!  <span class="custom-link-color" clickId="Track Water">Start tracking.</span></span>',
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<span class="two-lines-noti-msg">Fast for 14 hrs to improve metabolism. <span class="custom-link-color" clickId="Track Fast">Use tracker !</span></span>',
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<span class="single-line-noti-msg-center"><span class="custom-link-color" clickId="CheckAnalysis">Click to view </span>Diet plan analysis</span>',
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<ng-container><span>Click tick in the diet plan <ion-icon name="checkmark-circle-outline" class="get-remove-icon-msg"></ion-icon> to add the calories in calorie counter</span></ng-container>'
        ];
        this.tipMessageafterReg = [
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<span class="two-lines-noti-msg">Improve health, focus on food items marked <span style="color: #38A534">Green</span> in diet options.</span>',
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<span class="two-lines-noti-msg">Don’t forget to keep track of your 12 glasses of water intake. <span class="custom-link-color" clickId="Add Water">Add here.</span></span>',
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<span class="two-lines-noti-msg">Make sure you fast for 14 hours today! <span class="custom-link-color" clickId="Track Fast">Keep track</span> of it!</span>',
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<span class="single-line-noti-msg-center"><span class="custom-link-color" clickId="CheckAnalysis">Click to view </span> Diet plan analysis</span>',
            '<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>',
            '<ng-container><span>Click tick in the diet plan <ion-icon name="checkmark-circle-outline" class="get-remove-icon-msg"></ion-icon> to add the calories in calorie counter</span></ng-container>'
        ];
        this.isrefresh = false;
        this.weightGraphData = [{ startWeight: 0, currentWeight: 0 }];
        this.drankwater = 0;
        this.localalarmstatus = "";
        this.isMessageRed = false;
        ///
        this.isStrait = false;
        this.isExpired = false;
        this.isOpenMantraInfo = false;
        this.caloriesDistribution = [
            {
                min: 35,
                max: 60
            },
            {
                min: 25,
                max: 50
            },
            {
                min: 29,
                max: 30
            }
        ];
        this.calory = "0";
        this.barPercent = [0, 0, 0];
        this.barPercent2 = [0, 0, 0];
        this.calories = [];
        this.startEnd = [
            { date: "", weight: 0 },
            { date: "", weight: 0 }
        ];
        this.profileName = "";
        this.profilePic = "";
        this.habitList = [];
        this.refreshCounter = [];
        this.isUp = [];
        this.myPercent = "";
        this.habitsForUpdate = [];
        this.caloryAsPerPlan = [0, 0, 0];
        this.timerStorage = { dietPlan: [] };
        this.caloryDistri = [0, 0, 0, 0];
        this.showCouponIcon = false;
        this.showFreeTrialOfferIcon = false;
        this.amount = 0;
        this.couponCode = 0;
        this.poundValue = 0.453592;
        this.weightUnit = "kg";
        this.caloriesCheck = {
            caloriesPer: 0,
            isValid: false,
            points: 0,
            strokeColor: "#f70808",
            progressPer: "0",
            totalProgressPer: 0
        };
        this.nutrientsCheck = {
            carbsIsValid: false,
            carbsStrokeColor: "#f70808",
            proteinIsValid: false,
            proteinStrokeColor: "#f70808",
            fatIsValid: false,
            fiberIsValid: false,
            fatStrokeColor: "#f70808",
            points: 0,
            message: '',
            isValid: false,
            progressPer: 0,
            totalProgressPer: 0
        };
        this.minFats = 40;
        this.maxFats = 90;
        this.minProtein = 35;
        this.maxProtein = 90;
        this.minFiber = 25;
        this.maxFiber = 60;
        this.recommendedCheck = {
            points: 0,
            recommended: 0,
            maxRecommended: 2,
            isValid: false,
            message: '',
            strokeColor: "#f70808",
            recommendedItems: [],
            progressPer: 0,
            totalProgressPer: 0
        };
        this.distributionCheck = {
            till6PMIsValid: false,
            till6PMStrokeColor: "#f70808",
            after6PMIsValid: false,
            after6PMStrokeColor: "#f70808",
            points: 0,
            message: '',
            isValid: false
        };
        this.meal = {
            morning: '',
            night: '',
            isValid: false,
            difference: '',
            message: '',
            points: 0,
            progressPer: 0,
            totalProgressPer: 0
        };
        this.scoreColor = "#0F8E8E";
        this.totalScore = 0;
        this.timingPer = 0;
        this.isPlanActiveUser = false;
        this.type = [{ index: 0, type: "When you wake-up" }];
        this.foodType = [];
        this.localData = { otherMaster: { suggestedPlan: [], dietPlan: [] } };
        this.dataPlan = { dishes: [], drinks: [] };
        this.diets = [];
        this.slotIndex = 0;
        this.currentDayDiets = [];
        this.suggestedCalories = {
            totalCalories: 0,
            recomended: 0,
            totalCarbs: 0,
            totalCarbsPer: 0,
            totalFat: 0,
            totalFatPer: 0,
            totalFiber: 0,
            totalFiberPer: 0,
            totalProtien: 0,
            totalProtienPer: 0,
            minus10: 0,
            plus10: 0,
            minus1010: 0
        };
        this.isWaterAnimation = 0;
        //Fasting -- alam
        this.fastingMessage = "";
        this.fastingShortMessage = "";
        this.fastingStartTime = "";
        this.fastingEndTime = "";
        this.differnceTime = "";
        this.percentageFastingMeal = 0;
        this.time = 15;
        this.isOpenMealInfo = false;
        this.sumrizeList = [];
        this.habitListAll = [];
        this.noofdays = 0;
        this.tempExpiryDay = 0;
        this.tempCurrentDay = 0;
        this.isPlanActive = false;
        this.habitCheck = [];
        this.isActiveButton = 0;
        this.isDistributionActiveButton = 0;
        this.weightCollaps = true;
        this.value = "";
        this.fastingHours = 0;
        // scheduleNotification(breakfastTime, dinnerTime, wakeuptime) {
        //   const year = new Date().getFullYear();
        //   const month = new Date().getMonth();
        //   const date = new Date().getDate();
        //   this.localNotifications.schedule([
        //     {
        //       id: 4,
        //       title: "Eat Salad before lunch !",
        //       text: "Low calories density & fiber content",
        //       smallIcon: 'res://sm_icon.png',
        //       //  icon: "https://test.fightitaway.com/images/salad.png",
        //       trigger: {
        //         at: new Date(
        //           new Date(year, month, date, breakfastTime - 1, 30, 0).getTime()
        //         )
        //       },
        //       actions: [
        //         { id: "d4", title: "Done" },
        //         { id: "s4", title: "Snooze" }
        //       ]
        //     },
        //     {
        //       id: 4,
        //       title: "Eat Salad before Dinner !",
        //       text: "Low calories density & fiber content",
        //       smallIcon: 'res://sm_icon.png',
        //       //  icon: "https://test.fightitaway.com/images/salad.png",
        //       trigger: {
        //         at: new Date(
        //           new Date(year, month, date, dinnerTime - 1, 30, 0).getTime()
        //         )
        //       },
        //       actions: [
        //         { id: "d4", title: "Done" },
        //         { id: "s4", title: "Snooze" }
        //       ]
        //     },
        //     {
        //       id: 6,
        //       title: "Start with fruit not tea",
        //       text: "Kick start your metabolism high",
        //       smallIcon: 'res://sm_icon.png',
        //       //  icon: "http://test.fightitaway.com/images/apple.png",
        //       trigger: {
        //         at: new Date(
        //           new Date(year, month, date, wakeuptime - 1, 30, 0).getTime()
        //         )
        //       },
        //       actions: [
        //         { id: "d6", title: "Done" },
        //         { id: "s6", title: "Snooze" }
        //       ]
        //     }
        //   ]);
        // }
        this.totalTodaysCalories = 0;
        this.totalTodaysCaloriesPerc = 0;
        this.overCalories = false;
        this.tempVar = 0;
        // goToProtienTracker(){
        //   if(this.currentDateIndex == 0){
        //     this.router.navigate(['protien-tracker']);
        //   } else{
        //     this.utilities.showErrorToast("You can't check Protein Tracker for future date");
        //   }
        // }
        this.totalProtien = 0;
        this.protienConsumed = 0;
        this.showProtienTracker = false;
        // checkingProtinTracker = true;
        this.protienConsumedPer = 0;
        this.todaySuggestedCalories = 0;
        this.recipeData = {};
        this.totalCalForActivity = 0;
        this.activityCalPer = 0;
        this.activityStepsPer = 0;
        this.total_steps = 0;
        this.total_cal = 0;
        this.time_options = [];
        this.selectedTime = "yesterday";
        this.changedTime = "Today";
        this.monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        this.calories_data = [];
        this.consumed_cal = 0;
        this.recommended_cal = 0;
        this.apiCaloriesFit = {};
        this.isGraphView = false;
        this.isRecordingSubscribed = false;
        this.isFitStarted = false;
        this.userFirstName = "";
        this.healthData = [];
        this.healthProblemCounter = 0;
        this.bindPartContent = "";
        this.bindContent = "";
        this.pageNumber = 1;
        this.perPagePost = 1;
        this.isMoreQnADetails = true;
        this.bindNewsContent = "";
        this.bindNewsTitle = "";
        this.image_URL = '';
        this.selectedThemeColor = '';
        this.weightLossThemeColor = '#01A3A4';
        this.immunityThemeColor = '#FD9F33';
        this.pcosThemeColor = '#FF5A7D';
        this.fatThemeColor = '#FD980F';
        this.muscleThemeColor = '#0B94C1';
        this.weightLossPlusThemeColor = '#0B94C1';
        this.postCovidThemeColor = '#754B29';
        this.detoxThemeColor = '#4CB271';
        this.diabetesThemeColor = '#D14322';
        this.hypertensionTheme = '#FF5D56';
        this.cholesterolTheme = '#A31E79';
        this.weightLossBottomRound = './assets/images/header-circle.svg';
        this.immunityBottomRound = './assets/images/header-circle-immunity.svg';
        this.postCovidBottomRound = './assets/images/header-circle-post-covid.svg';
        this.detoxBottomRound = './assets/images/header-circle-detox.svg';
        this.pcosBottomRound = './assets/images/pcos.svg';
        this.diabetesThemeColorRound = './assets/images/header-circle-diabetes.svg'; //'#D14322';
        this.hypertensionThemeRound = './assets/images/header-circle-hypertension.svg'; //'#FF5D56';
        this.cholesterolThemeRound = './assets/images/header-circle-cholesterol.svg'; //'#A31E79';
        this.planStatusChecked = false;
        this.radiousValue = true;
        this.resultData = [
            // {
            //   "resultImage": "./assets/images/test03.png",
            //   "resultName": "Nitya Arora",
            //   "resultAge": "28 yrs",
            //   "resultTitle": "Lost 52 Kg  in 14 months",
            //   "resultDetails": '"I had tried all sorts of diets such as GM, keto, intermittent fasting, did lots of exercises also but the results were temporary. This app taught me the right way."'
            // },
            {
                "isPlantype": false,
                "resultImage": "./assets/images/trnsform_new.jpeg",
                "resultName": "Viraj Sethi",
                "resultAge": "26 yrs",
                "resultTitle": "Transformed  in 15 months",
                "resultDetails": '"Smart Diet Planner helped me a lot to align my diet as per macro nutrient & my taste. Features like Protein Tracker was something I used to follow everyday."'
            },
            {
                "isPlantype": false,
                "resultImage": "./assets/images/tejas.png",
                "resultName": "Tejas",
                "resultAge": "25 yrs",
                "resultTitle": "Transformed  in 1.7 year",
                "resultDetails": '"I was using excel earlier to plan my diet and now I use "Smart Diet Planner" I highly recommend it to my followers and to my students."'
            },
            {
                "isPlantype": false,
                "resultImage": "./assets/images/transform_musel.jpg",
                "resultName": "Fakhare Alam",
                "resultAge": "35 yrs",
                "resultTitle": "Transformed  in 1 year",
                "resultDetails": '"For Me following diet had been tough as I am very choosy of my food. This app is helping me and I really like the protein tracker"'
            }
            // ,
            // {
            //   "resultImage": "./assets/images/test01.png",
            //   "resultName": "Disha Jain",
            //   "resultAge": "31 yrs",
            //   "resultTitle": "Lost 14 Kg in 4 months",
            //   "resultDetails": '"Being a foodie my journey was difficult but this app gives me a plethora of healthy options to eat and as per my liking, made the journey much easier."'
            // }
            ,
            {
                "isPlantype": true,
                "resultImage": "./assets/images/test02.png",
                "resultName": "Shalloo Chawdhary",
                "resultAge": "45 yrs",
                "resultTitle": "Lost 45 Kg in 10 months",
                "resultDetails": '"Smart diet planner taught me the right way of losing fat based on changes in lifestyle with my choice of meals and the concept of mindful eating."'
            }, {
                "isPlantype": true,
                "resultImage": "./assets/images/test04.png",
                "resultName": "Poonam Bedi",
                "resultAge": "56 yrs",
                "resultTitle": "Lost 10 Kg in 3 months",
                "resultDetails": '"Smart Diet plan taught me right way of eating and I have been able to change my lifestyle with ease with help of its trackers."'
            }, {
                "isPlantype": true,
                "resultImage": "./assets/images/test05.png",
                "resultName": "Puneet Manchanda",
                "resultAge": "51 yrs",
                "resultTitle": "87 Kg to 72 Kg in 5 months",
                "resultDetails": '"I am a transformed person and much healthier version of myself. I have learnt what to eat and how much to eat at what time with this app."'
            }, {
                "isPlantype": true,
                "resultImage": "./assets/images/test06.png",
                "resultName": "Sakshi Sharma",
                "resultAge": "26 yrs",
                "resultTitle": "Lost 6 kgs in 2 months",
                "resultDetails": '"Got healthy personalized diet plan based on my preferences. I could see the results from the 1st week itself. It had certain key mantras that are cherry on the cake."'
            }, {
                "isPlantype": true,
                "resultImage": "./assets/images/test07.png",
                "resultName": "Saurabh Kochhar",
                "resultAge": "42 yrs",
                "resultTitle": "Reduced 16 Kg in 5 months",
                "resultDetails": '"Best part of this app is NO MOR CALORIES COUNTING. I used to hate data entry to count calories. With this app  I know what to eat and when to eat."'
            }
        ];
        this.resultDataUpdated = [];
        this.isManjhariNew = "manjhari2.png";
        // restMessages(){
        //   if(this.isIosDevice){
        //     this.tipMessage = this.insertItemInArrayOnSpecificIndex(this.tipMessage,4,'<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>');
        //     this.tipMessage = this.insertItemInArrayOnSpecificIndex(this.tipMessage,5,'<span class="two-lines-noti-msg">Give permission in health kit to "Smart diet planner" to keep track of your steps.</span>');
        //     this.tipMessageafterReg = this.insertItemInArrayOnSpecificIndex(this.tipMessageafterReg,2,'<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>');
        //     this.tipMessageafterReg = this.insertItemInArrayOnSpecificIndex(this.tipMessageafterReg,3,'<span class="two-lines-noti-msg">Give permission in health kit to "Smart diet planner" to keep track of your steps.</span>');
        //   }else if(this.isAndroidDevice){
        //     this.tipMessage = this.insertItemInArrayOnSpecificIndex(this.tipMessage,4,'<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>');
        //     this.tipMessage = this.insertItemInArrayOnSpecificIndex(this.tipMessage,5,'<span class="two-lines-noti-msg"> <span class="custom-link-color" clickId="Activate">Activate</span> your Google Fit to keep track of your steps and calories burnt.</span>');
        //     this.tipMessageafterReg = this.insertItemInArrayOnSpecificIndex(this.tipMessageafterReg,2,'<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>');
        //     this.tipMessageafterReg = this.insertItemInArrayOnSpecificIndex(this.tipMessageafterReg,3,'<span class="two-lines-noti-msg"> <span class="custom-link-color" clickId="Activate">Activate</span> your Google Fit to keep track of your steps and calories burnt.</span>');
        //   }
        //   if(localStorage.getItem("startActivity")) this.removeHealthKitPerMessage();
        //   this.storage.get("localData").then((val) => {
        //     let data = JSON.parse(val);
        //     let dieasesArray = data["otherMaster"]["diseases"].filter((ele) =>{
        //       return ele.isSelected;
        //     })
        //     if(dieasesArray.length){
        //       let diesValues = "";
        //       dieasesArray.forEach((ele) =>{
        //         console.log(ele.value)
        //         diesValues = diesValues ? diesValues + ", "+ ele.value : ele.value;
        //       });
        //       this.tipMessage.push('<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>');
        //       this.tipMessage.push('<ng-container><span class="two-lines-noti-msg">Items marked with heart icon <img src="./assets/img/health1.svg" class="pic-food-heart-msg"/> are best for your '+diesValues+'</span></ng-container>');
        //       this.tipMessageafterReg.push('<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>');
        //       this.tipMessageafterReg.push('<ng-container><span class="two-lines-noti-msg">Items marked with heart icon <img src="./assets/img/health1.svg" class="pic-food-heart-msg"/> are best for your '+diesValues+'</span></ng-container>');
        //     //  this.loadMessages();
        //     }else{
        //    //   this.loadMessages();
        //     }
        //   })
        // }
        this.isManjhari = false;
        this.videoManjhari = "https://www.youtube.com/embed/vXBq6YkkSN0";
        // changeTime() {
        //   this.showLoader();
        //   let searchDates = [];
        //   let newtodate = "";
        // }
        this.dataSlot = "";
        this.isMore = new Array(50);
        let me = this;
        this.recipeDayComponent = new _recipe_day_recipe_day_component__WEBPACK_IMPORTED_MODULE_7__.RecipeDayComponent(storage, appService, router, route, _sanitizer, utilities);
        this.profilePic = _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.ProfileInfo.profilePic;
        this.image_URL = _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.image_URL;
        this.platform.ready().then(() => {
            if (this.platform.is('cordova')) {
                this.isNotCordova = false;
            }
            else {
                this.isNotCordova = true;
            }
        });
        this.appService.offerIcon$.subscribe(() => {
            this.manageCouponCounter();
        });
        this.appService.nutriScorePayment$.subscribe(() => {
            this.paymentSubscribeModel('nutri diet plan');
        });
        this.appService.mainPageScrollTop$.subscribe(() => {
            let self = this;
            setTimeout(() => {
                //   self.ionContent.scrollToTop(500);
            }, 100);
        });
        this.appService.paymentDone$.subscribe(() => {
            this.startCelebration();
            //  this.dataOnInIt();
        });
        this.appService.goToPersonalDiet$.subscribe(() => {
            let self = this;
            setTimeout(() => {
                self.gotoEditPersonalPlan();
            }, 100);
        });
        if (_core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isTestEnv)
            alert("You are in test environment");
        setInterval(() => {
            this.needleValue = Math.floor(Math.random() * 100);
        }, 2000);
    }
    // ngOnChanges(){
    //  console.log("ngOnChange");
    // // this.ngOnInit();
    // }
    slidePrev() {
        this.resultSlide.slidePrev();
    }
    streamVidM() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            this.streamVideoM = true;
            console.log("tttt", this.streamVideoM);
        });
    }
    closeVideoM() {
        this.streamVideoM = false;
    }
    gotoLink() {
        let url = 'https://manjariwellness.com/about-us/';
        this.iab.create(url, "_blank", "location=yes");
    }
    slideNext() {
        this.resultSlide.slideNext();
    }
    updateSlotItems(customPortionSlotItem, ind, xindex) {
        if (customPortionSlotItem !== null && !isNaN(customPortionSlotItem)) {
            this.diets[ind].data[xindex].portion = customPortionSlotItem;
            const itemCode = [...new Map(this.diets[ind].data.map(item => [item['code'], item])).values()];
            const reqData = {
                slot: ind,
                foodCodeList: itemCode,
                detox: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox,
                date: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate,
                country: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.country,
                isUpdateDiet: true
            };
            console.log("reqData update Portion:-", reqData);
            if (reqData.foodCodeList.length > 0) {
                this.utilities.presentLoading();
                this.appService.postOptionFoodList(reqData, _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.email).then(success => {
                    this.fetchDiet(_core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox, _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate);
                }, err => {
                    this.utilities.hideLoader();
                    console.log("post", err);
                });
            }
            else {
                this.utilities.presentAlert("Please select item with portion.");
            }
        }
    }
    ngOnChange() {
        this.cdr.detectChanges();
    }
    videoClickWhySDP(id, data) {
        this.whoWeAreVideo = data;
        if (this.whySDPData[id]["mode"] == "l")
            this.videoLandscape = true;
        else
            this.videoLandscape = false;
        this.streamVideo = true;
    }
    whySDPSlideChanged(ev) {
        this.videoSlideWhySDP.getActiveIndex().then((index) => {
            if (ev.target.swiper.isEnd) { // Added this code because getActiveIndex returns wrong index for last slide
                this.whySDPIndex = this.whySDPData.length - 1;
            }
            else {
                this.whySDPIndex = index;
            }
        });
    }
    videoClick() {
        this.streamVideo = true;
    }
    closeVideo() {
        this.streamVideo = false;
    }
    manageCouponCounter() {
        let count = localStorage.getItem("couponCounter");
        let coutDownTimer = localStorage.getItem("offerTimeExpired");
        if (coutDownTimer && coutDownTimer == "true") {
            this.showCouponIcon = false;
        }
        else if (Number(count) >= 1 && coutDownTimer == "false") {
            this.showCouponIcon = true;
            //  this.countDown();
        }
        else {
            this.showCouponIcon = false;
        }
    }
    converDateForLead(date_ob) {
        // adjust 0 before single digit date
        let date = ("0" + date_ob.getDate()).slice(-2);
        // current month
        let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
        // current year
        let year = date_ob.getFullYear();
        // current hours
        let hours = date_ob.getHours().toString().length == 1 ? "0" + date_ob.getHours() : date_ob.getHours();
        // current minutes
        let minutes = date_ob.getMinutes().toString().length == 1 ? "0" + date_ob.getMinutes() : date_ob.getMinutes();
        // current seconds
        let seconds = date_ob.getSeconds().toString().length == 1 ? "0" + date_ob.getSeconds() : date_ob.getSeconds();
        // prints date & time in YYYY-MM-DD HH:MM:SS format
        console.log(year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds);
        return year + "-" + month + "-" + date + " " + hours + ":" + minutes + ":" + seconds;
    }
    paymentSubscribeModel(content) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            let self = this;
            self.utilities.showLoading();
            let count = localStorage.getItem("couponCounter");
            let coutDownTimer = localStorage.getItem("offerTimeExpired");
            if (Number(count) >= 1 && coutDownTimer == "false") {
                this.storage.get('profileData').then(val => {
                    if (val != '') {
                        this.appService.getCouponListOffered().then(success => {
                            let couponList = JSON.parse(JSON.stringify(success)).couponList.filter((ele) => {
                                return ele.couponCode.indexOf("REF") == -1;
                            });
                            let data;
                            if (self.isIosDevice) {
                                data = couponList.filter((ele) => {
                                    return ele.couponCode == "special_offer_ios";
                                });
                            }
                            else {
                                data = couponList.filter((ele) => {
                                    return ele.couponCode == "special_offer_android";
                                });
                            }
                            // self.isIosDevice ? couponList[0] : couponList[1];
                            self.utilities.hideLoader();
                            data[0]["productId"] = self.isIosDevice ? "smart_diet_planner_yearly_offer_app" : self.isAndroidDevice ? "offer_sdp_6months" : "";
                            self.showPaymentSubscribe(data[0], content);
                        }, err => {
                            self.utilities.hideLoader();
                            console.log("errr", err);
                        });
                    }
                });
            }
            else {
                this.storage.get("puchase_plan").then(success => {
                    if (success) {
                        let plans = JSON.parse(JSON.stringify(success)).couponList;
                        console.log("Plan", plans);
                        let plan = plans.filter(ele => {
                            return ele.perid == 6;
                        })[0];
                        plan["productId"] = self.isIosDevice ? "smart_diet_planner_half_yearly_app" : self.isAndroidDevice ? "smart_diet_planner_half_yearly" : "";
                        self.showPaymentSubscribe(plan, content);
                    }
                }, err => {
                    // this.utilities.hideLoader();
                    console.log('errr', err);
                });
            }
        });
    }
    resetAllValues(cb) {
        this.caloriesCheck = {
            caloriesPer: 0,
            isValid: false,
            points: 0,
            strokeColor: "#f70808",
            progressPer: "0",
            totalProgressPer: 0
        };
        this.nutrientsCheck = {
            carbsIsValid: false,
            carbsStrokeColor: "#f70808",
            proteinIsValid: false,
            proteinStrokeColor: "#f70808",
            fatIsValid: false,
            fiberIsValid: false,
            fatStrokeColor: "#f70808",
            points: 0,
            message: '',
            isValid: false,
            progressPer: 0,
            totalProgressPer: 0
        };
        this.minCarbs = 0;
        this.maxCarbs = 0;
        this.minFats = 40;
        this.maxFats = 65;
        this.minProtein = 35;
        this.maxProtein = 60;
        this.minFiber = 25;
        this.maxFiber = 40;
        this.recommendedCheck = {
            points: 0,
            recommended: 0,
            maxRecommended: 2,
            isValid: false,
            message: '',
            strokeColor: "#f70808",
            recommendedItems: [],
            progressPer: 0,
            totalProgressPer: 0
        };
        this.distributionCheck = {
            till6PMIsValid: false,
            till6PMStrokeColor: "#f70808",
            after6PMIsValid: false,
            after6PMStrokeColor: "#f70808",
            points: 0,
            message: '',
            isValid: false
        };
        this.meal = {
            morning: '',
            night: '',
            isValid: false,
            difference: '',
            message: '',
            points: 0,
            progressPer: 0,
            totalProgressPer: 0
        };
        cb();
    }
    getDietPlanScore(cb) {
        this.resetAllValues(() => {
            this.timingPer = this.getPercentageCal((this.caloryAsPerPlan[1] + this.caloryAsPerPlan[0]), this.suggestedCalories.totalCalories, (this.caloriesDistribution[1].min + this.caloriesDistribution[0].min));
            this.minCarbs = Math.round((0.35 * this.suggestedCalories.totalCalories / 4));
            this.maxCarbs = Math.round((0.65 * this.suggestedCalories.totalCalories / 4));
            if (parseFloat(this.suggestedCalories.totalCalories) <= this.suggestedCalories.plus10) {
                this.caloriesCheck.points = 40;
            }
            else {
                let extraCalories = Math.round(((this.suggestedCalories.totalCalories - this.suggestedCalories.plus10) / this.suggestedCalories.plus10) * 100 / 5);
                if (extraCalories >= 4) {
                    this.caloriesCheck.points = 0;
                }
                else if (extraCalories == 3) {
                    this.caloriesCheck.points = 10;
                }
                else if (extraCalories == 2) {
                    this.caloriesCheck.points = 20;
                }
                else if (extraCalories == 1) {
                    this.caloriesCheck.points = 30;
                }
            }
            this.caloriesCheck.caloriesPer = (parseFloat(this.suggestedCalories.totalCalories) / parseFloat(this.suggestedCalories.recomended)) * 100;
            this.caloriesCheck.progressPer = Math.round((Math.PI * 2 * 22 * this.caloriesCheck.points) / 40) + "";
            this.caloriesCheck.totalProgressPer = Math.round((Math.PI * 2 * 22 * 1)) - Math.round((Math.PI * 2 * 22 * this.caloriesCheck.points) / 40);
            let numOfMultiFaliure = 0;
            // if (this.suggestedCalories.totalCarbsPer >= 35 && this.suggestedCalories.totalCarbsPer <= 70) {
            if (this.suggestedCalories.totalCarbs >= this.minCarbs && this.suggestedCalories.totalCarbs <= this.maxCarbs) {
                this.nutrientsCheck.points = this.nutrientsCheck.points + 5;
            }
            // if (this.suggestedCalories.totalProtienPer >= 12 && this.suggestedCalories.totalProtienPer <= 30) {
            if (this.suggestedCalories.totalProtien >= this.minProtein && this.suggestedCalories.totalProtien <= this.maxProtein) {
                this.nutrientsCheck.points = this.nutrientsCheck.points + 5;
            }
            // if (this.suggestedCalories.totalFatPer >= 10 && this.suggestedCalories.totalFatPer <= 35) {
            // if (this.suggestedCalories.totalFat >= this.minFats && this.suggestedCalories.totalFat <= this.maxFats) {
            if (this.suggestedCalories.totalFat <= this.maxFats) {
                this.nutrientsCheck.points = this.nutrientsCheck.points + 5;
            }
            if (this.suggestedCalories.totalFiber >= this.minFiber && this.suggestedCalories.totalFiber <= this.maxFiber) {
                this.nutrientsCheck.points = this.nutrientsCheck.points + 5;
            }
            this.nutrientsCheck.progressPer = Math.round((Math.PI * 2 * 22 * this.nutrientsCheck.points) / 20);
            this.nutrientsCheck.totalProgressPer = Math.round((Math.PI * 2 * 22 * 1)) - Math.round((Math.PI * 2 * 22 * this.nutrientsCheck.points) / 20);
            let numOfDistributionFaliure = 0;
            if ((this.caloryDistri[3] >= (this.caloriesDistribution[1].min + this.caloriesDistribution[0].min)) &&
                (this.caloryDistri[3] <= (this.caloriesDistribution[1].max + this.caloriesDistribution[0].max))) {
                this.distributionCheck.points = this.distributionCheck.points + 10;
            }
            if (this.caloryDistri[2] <= this.caloriesDistribution[2].max) {
                this.distributionCheck.points = this.distributionCheck.points + 10;
            }
            this.storage.get("diets").then(diets => {
                var _a, _b, _c, _d, _e, _f, _g, _h;
                this.diets = diets;
                let totalRecommended = 0;
                this.diets.forEach(el => {
                    el.data.forEach(element => {
                        if (element.RecommendedIn && element.RecommendedIn.length > 0 && element.RecommendedIn[0] != "") {
                            totalRecommended = totalRecommended + 1;
                            this.recommendedCheck.recommendedItems.push(element);
                        }
                    });
                });
                if (totalRecommended == 1) {
                    this.recommendedCheck.points = 10;
                }
                else if (totalRecommended >= 2) { //this.recommendedCheck.maxRecommended) {
                    this.recommendedCheck.points = 20;
                }
                this.recommendedCheck.progressPer = Math.round((Math.PI * 2 * 22 * this.recommendedCheck.points) / 20);
                this.recommendedCheck.totalProgressPer = Math.round((Math.PI * 2 * 22 * 1)) - Math.round((Math.PI * 2 * 22 * this.recommendedCheck.points) / 20);
                this.meal.morning = this.tConvert((_a = this.diets.find(o => o.slot == 1)) === null || _a === void 0 ? void 0 : _a.time);
                this.meal.night = ((_b = this.diets.find(o => o.slot == 8)) === null || _b === void 0 ? void 0 : _b.time) ?
                    this.tConvert((_c = this.diets.find(o => o.slot == 8)) === null || _c === void 0 ? void 0 : _c.time) :
                    this.tConvert((_d = this.diets.find(o => o.slot == 7)) === null || _d === void 0 ? void 0 : _d.time);
                this.timeDiffernce((_e = diets[1]) === null || _e === void 0 ? void 0 : _e.time, ((_f = diets[8]) === null || _f === void 0 ? void 0 : _f.time) ? (_g = diets[8]) === null || _g === void 0 ? void 0 : _g.time : (_h = diets[7]) === null || _h === void 0 ? void 0 : _h.time);
                let totalScore;
                if (!_core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox) {
                    totalScore = ((this.caloriesCheck.points + this.nutrientsCheck.points +
                        this.distributionCheck.points + this.recommendedCheck.points + this.meal.points) / 110 * 100);
                }
                else {
                    totalScore = ((this.caloriesCheck.points +
                        this.distributionCheck.points + this.recommendedCheck.points + this.meal.points) / 80 * 100);
                }
                this.totalScore = Math.floor(totalScore);
                console.log("Total Score :::  ", this.totalScore);
            });
        });
    }
    // timingPer = 0;
    showPaymentSubscribe(data, content) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            let me = this;
            let obj = { 'content': content, "plan": data };
            if (content == 'diet plan analysis') {
                this.getDietPlanScore((tScore) => {
                    obj["totalScore"] = tScore;
                    //   this.openPurchaseModal(obj);
                });
            }
            else if (content == 'unlock your diet plan') {
                obj["totalCalories"] = this.roundUpvalue(this.suggestedCalories.totalCalories);
                obj["carbs"] = this.roundUpvalue(this.suggestedCalories.totalCarbs);
                obj["protein"] = this.roundUpvalue(this.suggestedCalories.totalProtien);
                obj["fat"] = this.roundUpvalue(this.suggestedCalories.totalFat);
                //   this.openPurchaseModal(obj);
            }
            else {
                //    this.openPurchaseModal(obj);
            }
        });
    }
    tConvert(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
        if (time.length > 1) { // If time format correct
            time = time.slice(1); // Remove full string match value
            time[5] = +time[0] < 12 ? ' AM' : ' PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }
    customProgressBar(current, total) {
        // border-top-right-radius: 50% 100%; border-bottom-right-radius: 50% 100%; border-top-left-radius: 50% 100%; border-bottom-left-radius: 50% 100%;
        let progressBarContainer = "<div><div style='display: inline-block; height: 15px; width: 15px; background: #01A3A4; border-radius: 100%;'></div>";
        let spaceSpan = "<div style='display: inline-block; height: 10px; width: 10px; background: #F6F7FC;'></div>";
        let yetToNavigateBullet = "<div style='display: inline-block; height: 15px; width: 15px; background: #DDDDDD; border-radius: 100%;'></div>";
        let navigatedBullet = "<div style='display: inline-block; height: 15px; width: 15px; background: #01A3A4; border-radius: 100%;'></div>";
        // let progressBarContainer: string = "<div><div class='custom-bullet active-custom-bullet'></div>";
        // let spaceSpan = "<div class='custom-bullet-space'></div>";
        // let yetToNavigateBullet = "<div class='custom-bullet'></div>"
        // let navigatedBullet = "<div class='custom-bullet active-custom-bullet'></div>";
        for (let i = 1; i < total; i++) {
            if (current > i) {
                progressBarContainer += spaceSpan + navigatedBullet;
            }
            else
                progressBarContainer += spaceSpan + yetToNavigateBullet;
        }
        progressBarContainer += '</div>';
        return progressBarContainer;
    }
    waterClick() {
        if (this.currentDateIndex == 0) {
            this.router.navigate(["water-guage"], {
                queryParams: {
                    drankwater: this.drankwater,
                    recommendedwater: this.recommendedwater,
                    waterPercentage: this.waterPercentage
                }
            });
        }
        else {
            //  this.utilities.showErrorToast("You can't check Water Tracker for future date");
        }
    }
    fastingClick() {
        // if (!this.isPlanActiveForDiet && !this.isRandomLock) {
        //   this.paymentSubscribeModel('fasting');
        // }else{
        if (this.currentDateIndex == 0) {
            this.router.navigate(["meal-guage"]);
        }
        else {
            //  this.utilities.showErrorToast("You can't check Fasting Tracker for future date");
        }
        // }
    }
    weightClick() {
        if (!this.isPlanActiveForDiet && !this.isRandomLock) {
            this.paymentSubscribeModel('weightloss');
        }
        else {
            this.router.navigate(["weight-guage"]);
        }
    }
    // openStore() {
    //   this.platform.ready().then(() => {
    //     if (this.platform.is('android')) {
    //       console.log('android platform');
    //       this.market.open('com.google.android.apps.fitness');
    //     } else if (this.platform.is('ios')) {
    //       this.market.open('com.google.ios.apps.fitness');
    //       console.log('ios platform');
    //     }
    //     else {
    //       this.showBtnsBlk = false;
    //     }
    //   })
    // }
    getMealHour() {
        // if (parseInt(this.meal.difference) >= 14) {
        if (parseFloat(this.meal.difference) > 13.5) {
            this.meal.message = `Your fasting duration is ${this.meal.difference} Hrs which is awesome`;
            this.meal.isValid = true;
            this.meal.points += 10;
        }
        else if (parseFloat(this.meal.difference) <= 13.5 && parseFloat(this.meal.difference) >= 12) {
            this.meal.message = `Your fasting duration is ${this.meal.difference} Hrs which is good, recommended 14 hours`;
            this.meal.isValid = true;
            this.meal.points += 5;
        }
        else if (parseFloat(this.meal.difference) < 12) {
            this.meal.message = `Your fasting duration is ${this.meal.difference} Hrs which is not good, recommended 14 hours`;
            this.meal.isValid = false;
            this.meal.points = 0;
        }
        this.meal.progressPer = Math.round((Math.PI * 2 * 22 * this.meal.points) / 20);
        this.meal.totalProgressPer = Math.round((Math.PI * 2 * 22 * 1)) - Math.round((Math.PI * 2 * 22 * this.meal.points) / 20);
    }
    timeDiffernce(firstDateTime, SecondDateTime) {
        console.log("breakFast", firstDateTime);
        console.log("dinner", SecondDateTime);
        this.percentageFastingMeal = 0;
        let currentTime = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate(), new Date().getHours(), new Date().getMinutes());
        this.fastingStartTime = firstDateTime;
        let hourMinut = firstDateTime.split(":");
        let hourMinut2;
        hourMinut2 = SecondDateTime.split(":");
        var startDate = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate(), parseInt(hourMinut[0]), parseInt(hourMinut[1]));
        var startDateForward = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() + 1, parseInt(hourMinut[0]), parseInt(hourMinut[1]));
        var startDateBackward = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 1, parseInt(hourMinut[0]), parseInt(hourMinut[1]));
        // Do your operations
        var endDate = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate(), parseInt(hourMinut2[0]), parseInt(hourMinut2[1]));
        // Do your operations
        var endDatePrevious = new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate() - 1, parseInt(hourMinut2[0]), parseInt(hourMinut2[1]));
        clearInterval(this.intervalPercent);
        if (new Date().getHours() > 12) {
            let sec_num = ((startDateForward.getTime() - endDate.getTime()) / 1000);
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - hours * 3600) / 60);
            this.meal.difference = hours + (minutes == 0 ? '' : ':' + minutes);
            this.getMealHour();
        }
        else {
            let sec_num = ((startDate.getTime() - endDatePrevious.getTime()) / 1000);
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - hours * 3600) / 60);
            this.meal.difference = hours + (minutes == 0 ? '' : ':' + minutes);
            this.getMealHour();
        }
        console.log("difference:-", startDate.getTime() - currentTime.getTime());
        if (currentTime.getTime() > startDate.getTime() && currentTime.getTime() < endDate.getTime()) {
            this.fastingMessage = "FASTING"; //"MEAL TIME";
            this.fastingShortMessage = "FASTING"; //"MEAL TIME";
            this.minTime = "6:00";
            this.maxTime = "11:00";
            this.hourValues = ['6', '7', '8', '9', '10', '11'];
            this.fastingEndTime = SecondDateTime;
            this.fastingStartTime = firstDateTime;
            let totalDurationMeal = (endDate.getTime() - startDate.getTime());
            // tslint:disable-next-line: max-line-length
            // this.intervalPercent = setInterval(() => {
            //   // this.percentageFastingMeal = Math.floor(100 - ((totalDurationMeal - (currentTime.getTime() - startDate.getTime())) * 100 / totalDurationMeal));
            //   this.percentageFastingMeal = Math.floor(100 - ((this.fastingDuration - (currentTime.getTime() - startDate.getTime())) * 100 / this.fastingDuration));
            // }, 1000);
            console.log("this.percentageFastingMeal", this.percentageFastingMeal);
            let sec_num = ((endDate.getTime() - startDate.getTime()) / 1000);
            var hours = Math.floor(sec_num / 3600);
            var minutes = Math.floor((sec_num - hours * 3600) / 60);
            this.differnceTime = hours + (minutes == 0 ? '' : ':' + minutes);
            this.value = this.differnceTime;
            this.mealType = "mealtime";
            this.differenceHours = this.differnceTime;
            localStorage.setItem("differenceHours", this.differenceHours);
            localStorage.setItem("mealType", this.mealType);
            this.startTimer((totalDurationMeal - (endDate.getTime() - currentTime.getTime())) / 1000);
        }
        else {
            this.fastingEndTime = firstDateTime;
            this.fastingStartTime = SecondDateTime;
            this.fastingMessage = "FASTING";
            this.fastingShortMessage = "FASTING";
            this.minTime = "18";
            this.maxTime = "23";
            this.hourValues = ['18', '19', '20', '21', '22', '23'];
            let totalDuration1 = (startDateForward.getTime() - endDate.getTime());
            let totalDuration2 = (startDate.getTime() - endDatePrevious.getTime());
            // this.intervalPercent = setInterval(() => {
            //   if (new Date().getHours() > 12) {
            //     this.percentageFastingMeal = Math.floor((totalDuration1 - (startDateForward.getTime() - currentTime.getTime())) * 100 / totalDuration1);
            //   }
            //   else {
            //     this.percentageFastingMeal = Math.floor((totalDuration2 - (startDate.getTime() - currentTime.getTime())) * 100 / totalDuration2);
            //   }
            // }, 1000);
            if (new Date().getHours() > 12) {
                let sec_num = ((startDateForward.getTime() - endDate.getTime()) / 1000);
                var hours = Math.floor(sec_num / 3600);
                var minutes = Math.floor((sec_num - hours * 3600) / 60);
                this.differnceTime = hours + (minutes == 0 ? '' : ':' + minutes);
                this.mealType = "fastinngtime";
                this.differenceHours = this.differnceTime;
                localStorage.setItem("differenceHours", this.differenceHours);
                localStorage.setItem("mealType", this.mealType);
                this.value = this.differnceTime;
                this.startTimer((totalDuration1 - (startDateForward.getTime() - currentTime.getTime())) / 1000);
            }
            else {
                let sec_num = ((startDate.getTime() - endDatePrevious.getTime()) / 1000);
                var hours = Math.floor(sec_num / 3600);
                var minutes = Math.floor((sec_num - hours * 3600) / 60);
                this.differnceTime = hours + (minutes == 0 ? '' : ':' + minutes);
                this.value = this.differnceTime;
                this.startTimer((totalDuration2 - (startDate.getTime() - currentTime.getTime())) / 1000);
            }
        }
        /* this.storage.get("fastingTime").then((fasting) => {
           let data = JSON.parse(fasting);
           if (fasting != null && fasting != undefined) {
             this.fastingStartTime = data.startTime;
             this.value = data.hours;
             this.fillDefault(this.fastingStartTime, this.value);
     
           }
         });
         */
    }
    updateFastingGauge() {
        this.onFasting = (localStorage.getItem("onFast") === 'true');
        this.fastingDuration = Number(localStorage.getItem("fastDuration")) || 12;
        if (this.onFasting) {
            this.fastingData = localStorage.getItem("fastStartTime");
            this.setFastingHours();
        }
    }
    setFastingHours() {
        this.fastingStartTme = this.fastingData ? moment__WEBPACK_IMPORTED_MODULE_5___default()(this.fastingData) : moment__WEBPACK_IMPORTED_MODULE_5___default()(this.meal.night, 'hh:mm a');
        this.fastingEndTme = moment__WEBPACK_IMPORTED_MODULE_5___default()(this.fastingStartTme).add(this.fastingDuration, "hours");
        this.updateFastingCountDown();
    }
    updateFastingCountDown() {
        const currentTme = new Date().getTime();
        const startTime = new Date(this.fastingStartTme).getTime();
        let timeDiff = Math.floor((currentTme - startTime) / 1000);
        clearInterval(this.fastingCountdownIntrvl);
        this.fastingCountdownIntrvl = setInterval(() => {
            timeDiff++;
            this.fastingCountdown = this.transform(timeDiff);
            this.percentageFastingMeal = (Math.floor((100 * timeDiff) / (3600 * this.fastingDuration)));
            if (this.percentageFastingMeal > 100) {
                this.isTimerRed = true;
            }
            if (!this.onFasting) {
                this.resetFastingGauge();
            }
        }, 1000);
    }
    resetFastingGauge() {
        this.isTimerRed = false;
        this.percentageFastingMeal = 0;
        this.fastingCountdown = "00:00:00";
        clearInterval(this.fastingCountdownIntrvl);
    }
    // async openFastingPopup() {
    //   // if (CONSTANTS.isPlanActiveParent) {
    //     const modal = await this.modalController.create({
    //       component: FastingAlarmPopupComponent,
    //       backdropDismiss: true,
    //       cssClass: 'app-offer-popup',
    //       componentProps: { "onFast": this.onFasting, "fastDuration": this.fastingDuration }
    //     });
    //     modal.onDidDismiss().then((data: any) => {
    //       if (data.data) {
    //         this.onFasting = data.data.onFast;
    //         if (this.onFasting) {
    //           this.fastingData = new Date(data.data.startTime).toISOString();
    //           this.setFastingHours();
    //         } else {
    //           this.resetFastingGauge();
    //         }
    //       }
    //     })
    //     return await modal.present();
    //   // }else{
    //   //   this.fastingClick()
    //   // }
    // }
    fillDefault(startTime, hours) {
        this.fastingStartTime = startTime;
        let startTimeFirstPart = parseInt(startTime.split(':')[0]);
        let startTimeSecondPart = parseInt(startTime.split(':')[1]);
        let hrsFirstPart = parseInt(hours.split(':')[0]);
        let hrsSeondPart = isNaN(parseInt(hours.split(':')[1])) ? '' : parseInt(hours.split(':')[1]);
        this.fastingEndTime = (startTimeFirstPart + hrsFirstPart) + ":" + startTimeSecondPart + hrsSeondPart;
        if (parseInt(this.fastingEndTime.split(':')[0]) > 24) {
            this.fastingEndTime = parseInt(this.fastingEndTime.split(':')[0]) - 24 + ':' + this.fastingEndTime.split(':')[1];
        }
    }
    changeStarttime(event) {
        this.fastingStartTime = event.detail.value;
        let startTimeFirstPart = parseInt(event.detail.value.split(':')[0]);
        let startTimeSecondPart = parseInt(event.detail.value.split(':')[1]);
        let hrsFirstPart = parseInt(this.value.split(':')[0]);
        let hrsSeondPart = isNaN(parseInt(this.value.split(':')[1])) ? 0 : parseInt(this.value.split(':')[1]);
        this.fastingEndTime = (startTimeFirstPart + hrsFirstPart) + ":" + (startTimeSecondPart + hrsSeondPart);
        if (parseInt(this.fastingEndTime.split(':')[0]) > 24) {
            this.fastingEndTime = parseInt(this.fastingEndTime.split(':')[0]) - 24 + ':' + this.fastingEndTime.split(':')[1];
        }
        let fisting = {
            startTime: this.fastingStartTime,
            hours: this.value
        };
        this.storage.set("fastingTime", JSON.stringify(fisting));
    }
    selectedDuration() {
        let startTimeFirstPart = parseInt(this.fastingStartTime.split(':')[0]);
        let startTimeSecondPart = parseInt(this.fastingStartTime.split(':')[1]);
        let hrsFirstPart = parseInt(this.value.split(':')[0]);
        let hrsSeondPart = isNaN(parseInt(this.value.split(':')[1])) ? 0 : parseInt(this.value.split(':')[1]);
        this.fastingEndTime = (startTimeFirstPart + hrsFirstPart) + ":" + (startTimeSecondPart + hrsSeondPart);
        if (parseInt(this.fastingEndTime.split(':')[0]) > 24) {
            this.fastingEndTime = parseInt(this.fastingEndTime.split(':')[0]) - 24 + ':' + this.fastingEndTime.split(':')[1];
        }
        let fisting = {
            startTime: this.fastingStartTime,
            hours: this.value
        };
        this.storage.set("fastingTime", JSON.stringify(fisting));
    }
    startTimer(time) {
        this.time = time;
        clearInterval(this.interval);
        this.interval = setInterval(() => {
            if (this.time === 0) {
                clearInterval(this.interval);
            }
            else {
                this.time++;
            }
            this.fastEndCountDown = this.transform(this.time);
        }, 1000);
    }
    transform(value) {
        var sec_num = value;
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - hours * 3600) / 60);
        var seconds = sec_num - hours * 3600 - minutes * 60;
        this.fastingHours = hours;
        return ((hours < 10 ? "0" + hours : hours) +
            ":" +
            (minutes < 10 ? "0" + minutes : minutes) +
            ":" +
            (seconds < 10 ? "0" + seconds : seconds));
    }
    pauseTimer() {
        clearInterval(this.interval);
    }
    // setNotifications() {
    //   const year = new Date().getFullYear();
    //   const month = new Date().getMonth();
    //   const date = new Date().getDate();
    //   this.localNotifications.schedule([
    //     // {
    //     //   id: 1,
    //     //   title: "Drink Water!",
    //     //   text: "Easiest way to keep metabolism high",
    //     //   smallIcon: 'res://sm_icon.png',
    //     //   //  icon: "https://test.fightitaway.com/images/water.png",
    //     //   trigger: {
    //     //     at: new Date(new Date(year, month, date, 9, 0, 0).getTime())
    //     //   },
    //     //   actions: [
    //     //     { id: "d1", title: "Done" },
    //     //     { id: "s1", title: "Snooze" }
    //     //   ]
    //     // },
    //     // {
    //     //   id: 1,
    //     //   title: "Drink Water!",
    //     //   text: "Easiest way to keep metabolism high",
    //     //   smallIcon: 'res://sm_icon.png',
    //     //   // icon: "https://test.fightitaway.com/images/water.png",
    //     //   trigger: {
    //     //     at: new Date(new Date(year, month, date, 15, 0, 0).getTime())
    //     //   },
    //     //   actions: [
    //     //     { id: "d1", title: "Done" },
    //     //     { id: "s1", title: "Snooze" }
    //     //   ]
    //     // },
    //     // {
    //     //   id: 1,
    //     //   title: "Drink Water!",
    //     //   text: "Easiest way to keep metabolism high",
    //     //   smallIcon: 'res://sm_icon.png',
    //     //   //  icon: "https://test.fightitaway.com/images/water.png",
    //     //   trigger: {
    //     //     at: new Date(new Date(year, month, date, 17, 0, 0).getTime())
    //     //   },
    //     //   actions: [
    //     //     { id: "d1", title: "Done" },
    //     //     { id: "s1", title: "Snooze" }
    //     //   ]
    //     // },
    //     {
    //       id: 2,
    //       title: "12 to 14 Hrs. fasting is must !",
    //       text: "Align yourself to circadian cycle",
    //       smallIcon: 'res://sm_icon.png',
    //       //   icon: "https://test.fightitaway.com/images/apple.png",
    //       trigger: {
    //         at: new Date(new Date(year, month, date, 9, 0, 0).getTime())
    //       },
    //       actions: [
    //         { id: "d2", title: "Done" },
    //         { id: "s2", title: "Snooze" }
    //       ]
    //     },
    //     {
    //       id: 2,
    //       title: "12 to 14 Hrs. fasting is must !",
    //       text: "Align yourself to circadian cycle",
    //       smallIcon: 'res://sm_icon.png',
    //       //  icon: "https://test.fightitaway.com/images/apple.png",
    //       trigger: {
    //         at: new Date(new Date(year, month, date, 21, 0, 0).getTime())
    //       },
    //       actions: [
    //         { id: "d2", title: "Done" },
    //         { id: "s2", title: "Snooze" }
    //       ]
    //     },
    //     //notify 3
    //     {
    //       id: 3,
    //       title: "Walk atleast 45 Minutes !",
    //       text: "Only diet will never help.",
    //       smallIcon: 'res://sm_icon.png',
    //       //  icon: "https://test.fightitaway.com/images/running.png",
    //       trigger: {
    //         at: new Date(new Date(year, month, date, 7, 0, 0).getTime())
    //       },
    //       actions: [
    //         { id: "d3", title: "Done" },
    //         { id: "s3", title: "Snooze" }
    //       ]
    //     },
    //     {
    //       id: 5,
    //       title: "Dinner should be lightest!",
    //       text: "Align yourself to circadian cycle",
    //       smallIcon: 'res://sm_icon.png',
    //       // icon: "https://test.fightitaway.com/images/dinner.png",
    //       trigger: {
    //         at: new Date(new Date(year, month, date, 18, 0, 0).getTime())
    //       },
    //       actions: [
    //         { id: "d5", title: "Done" },
    //         { id: "s5", title: "Snooze" }
    //       ]
    //     }
    //   ]);
    // }
    bindSlotFromLocal() {
        var d = new Date();
        var t = d.toLocaleTimeString();
        console.log("Time", t);
        const hoursMint = t.split(":");
        console.log("diet navaid1111111:-", this.timerStorage.dietPlan.diets);
        // if (this.timerStorage && this.timerStorage.dietPlan && this.timerStorage.dietPlan.diets && this.timerStorage.dietPlan.diets.length > 0)
        // this.scheduleNotification(
        //   parseInt(this.timerStorage.dietPlan.diets[2].time.split(":")[0]),
        //   parseInt(this.timerStorage.dietPlan.diets[7].time.split(":")[0]),
        //   parseInt(this.timerStorage.dietPlan.diets[0].time.split(":")[0])
        // );
    }
    habitInfoClick() {
        console.log("true");
        this.isOpenMealInfo = true;
    }
    infoClick() {
        this.isOpenMantraInfo = true;
    }
    dismiss() {
        this.isOpenMantraInfo = false;
        this.isOpenMealInfo = false;
    }
    dismissSource() {
        this.showSourceInfo = false;
        // this.dietChoices = false;
    }
    todaysCalCount() {
        // this.totalTodaysCalories = 0;
        let totalTodaysCalories = 0;
        this.storage.get("dietData").then((res) => {
            let dietPlan = _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox ? 'detox' : _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedDietPlan;
            if (res && res[moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date()).format("DDMMYYYY")] && res[moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date()).format("DDMMYYYY")][dietPlan]) {
                let dietData = res[moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date()).format("DDMMYYYY")][dietPlan];
                dietData.diets.forEach((ele) => {
                    let slotCalories = 0;
                    ele.data.forEach((ele1) => {
                        if (ele1.eaten > 0) {
                            totalTodaysCalories = totalTodaysCalories + ele1["Calories"];
                        }
                    });
                });
                this.totalTodaysCalories = Math.round(totalTodaysCalories);
                this.totalTodaysCaloriesPerc = Math.round((totalTodaysCalories * 100) / dietData["recomended"]);
                this.totalCaloriesPer = dietData["totalCaloriesPer"];
                this.tolalCalories = dietData["tolalCalories"];
                this.kcounter.emit(true);
            }
        });
    }
    goToTodaysCaloriesCounter(flag) {
        if (this.currentDateIndex == 0) {
            if (flag) {
                //   this.utilities.logEvent("Counter_click_header", {});
            }
            else {
                //   this.utilities.logEvent("Counter_click_tracker", {});
            }
            this.router.navigate(['todays-calorie-count']);
        }
        else {
            //   this.utilities.showErrorToast("You can't check KCal counter for future date");
        }
    }
    getConsumedProtien() {
        this.storage.get("profileData").then(val => {
            let profile = this.utilities.parseJSON(val);
            console.log("ZprofileData:-", profile);
            let averageProtein = profile["lifeStyle"]["protien"];
            this.totalProtien = 0;
            this.protienConsumed = 0;
            let diet = [];
            this.diets.forEach((ele) => {
                diet = [...diet, ...ele["data"]];
            });
            diet.forEach((ele) => {
                this.totalProtien = this.roundingVal(this.totalProtien + ele.Protien);
                if (ele.eaten == 2)
                    this.protienConsumed = this.roundingVal(this.protienConsumed + ele.Protien);
            });
            this.protienConsumedPer = this.roundUpvalue((this.protienConsumed * 100) / parseFloat(averageProtein));
        });
    }
    // bindPortionInTextbox(slotDiet){
    //   for (let index = 0; index < slotDiet.length; index++) {
    //     for (let j = 0; j < slotDiet[index].data.length; j++) {
    //       this.customPortion[[index][j]]=slotDiet[index].data[j].portion;
    //     }
    //   }
    // }
    setDiet(success, isDetox, date) {
        this.utilities.hideLoader();
        let data = this.utilities.parseJSON(this.utilities.parseString(success));
        this.timerStorage.dietPlan = data;
        console.log("log Diet:- ", data);
        this.calories = { calories: data.recomended };
        this.diets = data.diets;
        //this. bindPortionInTextbox(this.diets);
        if (!localStorage.getItem("loadHighProtienData")) {
            localStorage.setItem("loadHighProtienData", new Date().toString());
            //  this.highProteinSlots();
        }
        else if (this.firstDateIsPastDayComparedToSecond(new Date(localStorage.getItem("loadHighProtienData")), new Date())) {
            localStorage.setItem("loadHighProtienData", new Date().toString());
            //  this.highProteinSlots();
        }
        // this.toDoListLoadData();
        this.todaysCalCount();
        this.storage.get("profileData").then(profileData => {
            let profile = this.utilities.parseJSON(profileData);
            if (profile['profile']['dietPlanType'] != undefined) {
                if ((profile['profile']['dietPlanType'].includes('fatShredding') || profile['profile']['dietPlanType'].includes('muscleGain'))) {
                    this.getConsumedProtien();
                }
            }
        });
        if (this.dietTimings) {
            this.dietTimings.forEach(element => {
                if (element.slot == 0) {
                    this.diets[0].time = element.time;
                }
                if (element.slot == 1) {
                    this.diets[1].time = element.time;
                }
                if (element.slot == 2) {
                    this.diets[2].time = element.time;
                }
                if (element.slot == 3) {
                    this.diets[3].time = element.time;
                }
                if (element.slot == 4) {
                    this.diets[4].time = element.time;
                }
                if (element.slot == 5) {
                    this.diets[5].time = element.time;
                }
                if (element.slot == 6) {
                    this.diets[6].time = element.time;
                }
                if (element.slot == 7) {
                    this.diets[7].time = element.time;
                }
            });
        }
        if (!date) {
            date = this.weeks[0].formatDate;
        }
        let findRec = this.weeks.find(o => o.formatDate == date);
        if (moment__WEBPACK_IMPORTED_MODULE_5___default()().format("MM/DD/YYYY") == moment__WEBPACK_IMPORTED_MODULE_5___default()(findRec === null || findRec === void 0 ? void 0 : findRec.date).format("MM/DD/YYYY")) {
            this.currentDayDiets = this.diets;
            // current time
            let consumed = 0;
            this.currentDayDiets.filter(o => o.time).forEach(element => {
                let time = parseInt(element.time.split(":")[0]);
                if ((moment__WEBPACK_IMPORTED_MODULE_5___default()().hour() + (moment__WEBPACK_IMPORTED_MODULE_5___default()().minute() / 60))
                    >= time) {
                    consumed = consumed + parseInt(element.totalCalories);
                }
            });
            this.consmedAtCurrentTime = consumed;
        }
        // if(localStorage.getItem("startActivity") || this.isIosDevice) {
        //   this.testGoogleFit();
        //   this.startActivityTracking();
        // }
        this.storage.set("diets", this.diets);
        console.log("this.diets", this.diets);
        // const random = Math.floor(Math.random() * this.diets.length);
        // this.recipeData = this.diets[random];
        this.isTodayDietPlan = _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate == moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date()).format("DDMMYYYY");
        if (this.diets != undefined) {
            this.totalCaloriesFunct(this.diets, data);
            this.localData.otherMaster.dietPlan = this.diets;
            this.isActiveButton = 0;
            this.isDistributionActiveButton = 0;
            if (counterSwipe == 0) {
                // setTimeout(() => {
                //   this.bottomSlide.slideTo(3);
                // }, 300);
            }
            counterSwipe++;
        }
        else {
            this.storage.set("localData", "");
            this.storage.set("profileData", "");
            // this.utilities.presentAlert(
            //   "Something went wrong! Please try after some time."
            // );
        }
        // setTimeout(() => {
        //  // this.fillWaterProgress();
        // //  this.weightGraph();
        //   this.bindSlotFromLocal();
        //   // if(!localStorage.getItem("freeTrialSet")) {
        //   //   let self  = this;
        //   //   setTimeout(() => {
        //   //     self.openFreeTrial();
        //   //   },3000000);
        //   // }
        //   this.calory = "0";
        //   this.isPinkText = isDetox;
        // }, 10);
        let self = this;
        setTimeout(() => {
            if (localStorage.getItem("waterDoneClicked") == 'true') {
                localStorage.removeItem("waterDoneClicked");
                self.router.navigate(["water-guage"], {
                    queryParams: {
                        drankwater: self.drankwater,
                        recommendedwater: self.recommendedwater,
                        waterPercentage: self.waterPercentage,
                        clickedWaterNotiDone: true
                    }
                });
            }
        }, 1000);
        this.meal.night = this.diets.find(o => o.slot == 8).time ?
            this.tConvert(this.diets.find(o => o.slot == 8).time) :
            this.tConvert(this.diets.find(o => o.slot == 7).time);
        this.updateFastingGauge();
    }
    fetchDiet(isDetox, date) {
        console.log("------------- fetchDiet ---------");
        let self = this;
        self.appService.getDietPlans(isDetox, date, _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.country, _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.defaultCalories).then(success => {
            console.log("XXX:-", success);
            self.storage.get("dietData").then((val) => {
                if (val) {
                    val[date] = {};
                    let planName = isDetox ? "detox" : _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedDietPlan;
                    val[date][planName] = success;
                    self.storage.set("dietData", val).then(() => {
                        self.setDiet(success, isDetox, date);
                    });
                }
                else {
                    let obj = {};
                    let planName = isDetox ? "detox" : _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedDietPlan;
                    obj[planName] = success;
                    let res1 = {};
                    res1[date] = obj;
                    self.storage.set("dietData", res1).then(() => {
                        self.setDiet(success, isDetox, date);
                    });
                }
            });
        }, error => {
            self.storage.set("localData", "");
            self.storage.set("profileData", "");
            self.utilities.hideLoader();
            console.log("DietPlan Error:", error);
            // self.utilities.presentAlert(message.globalError);
            self.router.navigate(["profile"]);
        });
    }
    fetchDietPlan() {
        //debugger;
        this.storage.get("dietData").then((res) => {
            if (moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date()).format("DDMMYYYY") == _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate && this.isDetox) {
                this.deficitTotal = 900;
            }
            let dietData = res && res[_core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate] ? res[_core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate] : null;
            if (dietData) {
                let planName = this.isDetox ? "detox" : _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedDietPlan;
                if (res[_core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate][planName]) {
                    this.setDiet(res[_core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate][planName], this.isDetox, _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate);
                }
                else {
                    //this.fetchDiet(this.isDetox, CONSTANTS.dietDate)
                }
            }
            else {
                //this.fetchDiet(this.isDetox, CONSTANTS.dietDate)
            }
        });
    }
    deleteOldDiet(data) {
        Object.keys(data).forEach((ele) => {
            if (!this.firstDateIsPastDayComparedToSecond(new Date(new Date().setDate(new Date().getDate() - 1)), new Date(moment__WEBPACK_IMPORTED_MODULE_5___default()(ele, "DDMMYYYY").valueOf()))) {
                delete data[ele];
            }
        });
        this.storage.set("dietData", data);
    }
    totalCaloriesFunct(diets, totalData) {
        this.isMessageRed = false;
        let cal = 0;
        const totalCalories = totalData.totalCalories;
        const totalCarbs = totalData.totalCarbs;
        let totalCarbsPer = totalData.totalCarbsPer;
        const totalProtien = totalData.totalProtien;
        let totalProtienPer = totalData.totalProtienPer;
        const totalFat = totalData.totalFat;
        let totalFatPer = totalData.totalFatPer;
        const totalFiber = totalData.totalFiber;
        let totalFiberPer = totalData.totalFiberPer;
        let minus10 = 0;
        let plus10 = 0;
        let minus1010 = 0;
        // this.timeDiffernce(diets[1]?.time, diets[8]?.time ? diets[8]?.time : diets[7]?.time);
        for (let index = 0; index < diets.length; index++) {
            diets[index].slotName = diets[index].message;
            cal = cal + Number(diets[index].totalCalories);
            this.totalCalForActivity = cal;
            if (index == 3) {
                this.caloryAsPerPlan[0] = cal;
                cal = 0;
            }
            else if (index == 6) {
                this.caloryAsPerPlan[1] = cal;
                cal = 0;
            }
            else if (index == 8) {
                this.caloryAsPerPlan[2] = cal;
                cal = 0;
            }
        }
        totalCarbsPer = parseInt(((totalCarbs * 100 * 4) / totalCalories).toFixed(0));
        totalProtienPer = parseInt(((totalProtien * 100 * 4) / totalCalories).toFixed(0));
        totalFatPer = parseInt(((totalFat * 100 * 9) / totalCalories).toFixed(0));
        totalFiberPer = parseInt(((totalFiber * 100) / totalCalories).toFixed(0));
        if (totalCarbsPer + totalProtienPer + totalFatPer > 100) {
            totalCarbsPer =
                totalCarbsPer - (totalCarbsPer + totalProtienPer + totalFatPer - 100);
        }
        else if (totalCarbsPer + totalProtienPer + totalFatPer < 100) {
            totalCarbsPer =
                totalCarbsPer + (100 - (totalCarbsPer + totalProtienPer + totalFatPer));
        }
        minus10 = parseInt(this.calories.calories) * 0.8;
        minus1010 = parseInt(this.calories.calories) * 0.8;
        plus10 = parseInt(this.calories.calories) * 1.05;
        this.isStrait =
            totalCalories >= minus1010 && totalCalories < minus10 ? true : false;
        this.suggestedCalories = {
            totalCalories: Number(totalCalories.toFixed(0)),
            recomended: totalData.recomended,
            totalCarbs: totalCarbs.toFixed(0),
            totalCarbsPer: totalCarbsPer.toFixed(0),
            totalFat: totalFat.toFixed(0),
            totalFatPer: totalFatPer.toFixed(0),
            totalFiber: totalFiber.toFixed(0),
            totalFiberPer: totalFiberPer.toFixed(0),
            totalProtien: totalProtien.toFixed(0),
            totalProtienPer: totalProtienPer.toFixed(0),
            minus10: Math.round(minus10),
            plus10: Math.round(plus10),
            isDown: totalCalories > minus1010 && totalCalories < plus10 ? false : true,
            isStrait: this.isStrait,
            minus1010
        };
        // text color
        let calorPer = (this.suggestedCalories.totalCalories * 100) / this.suggestedCalories.recomended;
        console.log("Calore pere ", calorPer);
        if (calorPer > 110 && calorPer <= 120) {
            this.scoreColor = "orange";
        }
        else if (calorPer > 120) {
            this.scoreColor = "red";
        }
        else {
            this.scoreColor = "#0F8E8E";
        }
        if (moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date()).format("DDMMYYYY") == _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate) {
            this.todaySuggestedCalories = this.suggestedCalories["totalCalories"];
            localStorage.setItem("todaySuggestedCalories", this.todaySuggestedCalories.toString());
        }
        if (this.isDetox) {
            this.suggestedCalories.minus10 = 500;
            this.suggestedCalories.plus10 = 1000;
        }
        setTimeout(() => {
            if (parseInt(this.suggestedCalories.totalCarbsPer) < 35 ||
                parseInt(this.suggestedCalories.totalCarbsPer) > 70 ||
                parseInt(this.suggestedCalories.totalProtienPer) < 12 ||
                parseInt(this.suggestedCalories.totalProtienPer) > 30 ||
                parseInt(this.suggestedCalories.totalFatPer) < 10 ||
                parseInt(this.suggestedCalories.totalFatPer) > 40 ||
                this.caloryDistri[0] < this.caloriesDistribution[0].min ||
                this.caloryDistri[0] > this.caloriesDistribution[0].max ||
                this.caloryDistri[1] < this.caloriesDistribution[1].min ||
                this.caloryDistri[1] > this.caloriesDistribution[1].mmax ||
                this.caloryDistri[2] > this.caloriesDistribution[2].max ||
                this.suggestedCalories.totalCalories < this.suggestedCalories.minus10 ||
                this.suggestedCalories.totalCalories > this.suggestedCalories.plus10) {
                this.isMessageRed = true;
            }
            if (parseInt(this.suggestedCalories.totalCarbsPer) < 35 ||
                parseInt(this.suggestedCalories.totalCarbsPer) > 70 ||
                parseInt(this.suggestedCalories.totalProtienPer) < 12 ||
                parseInt(this.suggestedCalories.totalProtienPer) > 30 ||
                parseInt(this.suggestedCalories.totalFatPer) < 10 ||
                parseInt(this.suggestedCalories.totalFatPer) > 40) {
                this.caloryChanged();
            }
            if (this.caloryDistri[3] < (this.caloriesDistribution[0].min + this.caloriesDistribution[1].min) ||
                this.caloryDistri[3] > (this.caloriesDistribution[0].max + this.caloriesDistribution[1].max) ||
                this.caloryDistri[3] < (this.caloriesDistribution[1].min + this.caloriesDistribution[0].min) ||
                this.caloryDistri[3] > (this.caloriesDistribution[1].max + this.caloriesDistribution[0].max) ||
                this.caloryDistri[2] > this.caloriesDistribution[2].max) {
                this.caloryDistributionChanged();
            }
            this.meal.points = 0;
            this.recommendedCheck.points = 0;
            this.caloriesCheck.points = 0;
            this.distributionCheck.points = 0;
            this.nutrientsCheck.points = 0;
            this.getDietPlanScore(null);
        }, 200);
        console.log("caloryAsPerPlan:-", this.caloryAsPerPlan + "--" + this.suggestedCalories.totalCalories);
        console.log("caloryAsPerPlan:-", this.caloryAsPerPlan + "--" + this.suggestedCalories.totalCalories);
        console.log("caloryAsPerPlan:-", this.caloryAsPerPlan[0] +
            this.caloryAsPerPlan[1] +
            this.caloryAsPerPlan[2] +
            "--" +
            this.suggestedCalories.totalCalories);
        //  this.caloryAsPerPlann(diets);
    }
    senitizeHTML(htmlValue) {
        return this._sanitizer.bypassSecurityTrustHtml(htmlValue);
    }
    getVirtualColor(value, min, max) {
        if (value < min || value > 70) {
            return "#E36049";
        }
        else {
            return "#4cc2c3";
        }
    }
    getPercentageCal(value, total, percentage) {
        let data = Math.round((Number(value) * 100) / Number(total));
        if (percentage == this.caloriesDistribution[0].min) {
            this.caloryDistri[0] = data;
        }
        else if (percentage == this.caloriesDistribution[1].min) {
            this.caloryDistri[1] = data;
        }
        else if (percentage == this.caloriesDistribution[2].min) {
            this.caloryDistri[2] = data;
        }
        else if (percentage == this.caloriesDistribution[1].min + this.caloriesDistribution[0].min) {
            this.caloryDistri[3] = data;
        }
        if (data >= 70)
            this.meal.points += 10;
        return data;
    }
    getVirtualPervcent2(percent, total, minVal) {
        const perc = Math.round((((Number(percent) * 100) / Number(total)) * 100) / Number(minVal)).toFixed(0);
        if (parseInt(perc) > 100) {
            if (Number(minVal) == this.caloriesDistribution[0].min) {
                this.barPercent2[0] = parseInt(perc);
            }
            else if (Number(minVal) == this.caloriesDistribution[1].min) {
                this.barPercent2[1] = parseInt(perc);
            }
            else if (Number(minVal) == this.caloriesDistribution[2].min) {
                this.barPercent2[2] = parseInt(perc);
            }
            return 100;
        }
        else {
            if (Number(minVal) == this.caloriesDistribution[0].min) {
                this.barPercent2[0] = parseInt(perc);
            }
            else if (Number(minVal) == this.caloriesDistribution[1].min) {
                this.barPercent2[1] = parseInt(perc);
            }
            else if (Number(minVal) == this.caloriesDistribution[2].min) {
                this.barPercent2[2] = parseInt(perc);
            }
            return perc;
        }
    }
    getPerPlanPerimeter(value, total, place) {
        let percent = (value * 100) / total;
        this.myPercent = percent;
        if (place == "0") {
            // this.getPerPlanLike(percent, "40-55", 0);
        }
        if (place == "1") {
            this.getPerPlanLike(percent, "30-35", 1);
        }
        if (place == "2") {
            this.getPerPlanLike(percent, "15-20", 2);
        }
        return percent + " " + (100 - percent);
    }
    getPerPlanLike(percent, total, place) {
        let totalP = total.split("-");
        if (percent > Number(totalP[1])) {
            this.isUp.push(0);
        }
        else if (percent >= Number(totalP[0]) && percent <= Number(totalP[1])) {
            this.isUp.push(1);
        }
        else if (percent < Number(totalP[0])) {
            this.isUp.push(2);
        }
        setTimeout(() => {
            this.isUp = this.isUp;
        }, 100);
    }
    doRefresh(event) {
        setTimeout(() => {
            if (event)
                event.target.complete();
        }, 1500);
        this.storage.set("dietData", null).then(() => {
            this.storage.set("optionsData", null).then(() => {
                this.isPullReferesh = true;
                this.diets = [];
                this.loadHomeData();
            });
        });
    }
    gotoGraphView() {
        this.isGraphView = !this.isGraphView;
    }
    removeMessageItem() {
        this.tipMessage.splice(4, 1);
        this.tipMessage.splice(4, 1);
        this.isFitStarted = true;
    }
    computeCalories(steps) {
        this.storage.get("profileData").then(val => {
            let profile = this.utilities.parseJSON(val);
            let total = this.utilities.getCaloriesBurned(profile, this.diets, steps, 0, 0, moment__WEBPACK_IMPORTED_MODULE_5___default()().hour() + (moment__WEBPACK_IMPORTED_MODULE_5___default()().minute() / 60));
            _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.calBurnedToday = this.total_cal = Math.round(total);
            this.activityCalPer = this.totalCalForActivity * 100 / this.suggestedCalories.totalCalories;
            this.cal_per = Math.round((this.total_cal / this.target_cal) * 100);
            this.total_steps = steps;
            this.activityStepsPer = this.total_steps * 100 / this.apiCaloriesFit.activityLevels;
            this.deficitToday = this.total_cal - this.consmedAtCurrentTime;
            if (this.deficitToday > 0) {
                this.deficitTodayPer = (this.deficitToday) > 500 ? 100 : this.deficitToday / 500 * 100;
            }
            else {
                this.deficitTodayPer = 0;
            }
        });
    }
    loadDataOnloadandRefresh() {
        this.time_options = [
            { label: "Today", value: "today" },
            { label: "Yesterday", value: "yesterday" },
            { label: "Last 7 Days", value: "last_seven" },
            { label: "Last 15 Days", value: "last_fifteen" },
            { label: "Last 30 Days", value: "last_thirty" },
        ];
        //this.setNotifications();
        // this.activateFitAPI();
        if (this.myInterval) {
            clearInterval(this.myInterval);
        }
        if (this.deficitInterval) {
            clearInterval(this.deficitInterval);
        }
        this.myInterval = setInterval(() => {
            // this.getHabitsForUpdate();
        }, 3600 * 1000);
        this.deficitInterval = setInterval(() => {
            let consumed = 0;
            this.currentDayDiets.filter(o => o.time).forEach(element => {
                let time = parseInt(element.time.split(":")[0]);
                if ((moment__WEBPACK_IMPORTED_MODULE_5___default()().hour() + (moment__WEBPACK_IMPORTED_MODULE_5___default()().minute() / 60))
                    >= time) {
                    consumed = consumed + parseInt(element.totalCalories);
                }
            });
            this.consmedAtCurrentTime = consumed;
        }, 15 * 60 * 1000);
        //  this.ActivityData();
        this.storage.get("newProfilePic").then(profile => {
            this.storage.get("localData").then(val => {
                // //this.utilities.presentLoading();
                //   this.getHabitsForUpdateOneTime();
            });
            let data = this.utilities.parseJSON(profile);
            this.profilePic = data.photoUrl ? data.photoUrl : 'assets/images/logo.png';
            this.userFirstName = data.firstName;
        });
        var today = new Date();
        var currentdate = `${today.getDate()}`.padStart(2, "0") +
            `${today.getMonth() + 1}`.padStart(2, "0") +
            today.getFullYear() +
            `${today.getHours()}`.padStart(2, "0") +
            `${today.getMinutes()}`.padStart(2, "0") +
            `${today.getSeconds()}`.padStart(2, "0");
        const element = document.getElementById("alarm");
        this.localalarmstatus = localStorage.getItem("localalarm");
        if (this.localalarmstatus == "Deactive") {
            if (element) {
                element.style.color = "grey";
            }
        }
        else {
            if (element) {
                element.style.color = "#2569b0";
            }
        }
    }
    dateChanged(weeks, index) {
        this.weeks = weeks;
        let selectedDay = this.weeks[index];
        var dayValue = selectedDay.formatDate;
        _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate = dayValue;
        // this.utilities.logEvent("DietPlan_02ChangeDate", {
        //   date: CONSTANTS.dietDate
        // });
        this.storage.get("dietData").then((res) => {
            let dietData = res && res[_core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate];
            if (!dietData) {
                this.isDetox = false;
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox = false;
                this.detoxToggle = false;
                if (this.selectedDietPlan == 'weightLoss') {
                    this.selectedThemeColor = this.weightLossThemeColor;
                }
                else if (this.selectedDietPlan == 'immunity_booster') {
                    this.selectedThemeColor = this.immunityThemeColor;
                }
                else if (this.selectedDietPlan == 'weightLossPlus') {
                    this.selectedThemeColor = this.weightLossPlusThemeColor;
                }
                else if (this.selectedDietPlan == 'post_covid') {
                    this.selectedThemeColor = this.postCovidThemeColor;
                }
                else if (this.selectedDietPlan == 'diabetes') {
                    this.selectedThemeColor = this.diabetesThemeColor;
                }
                else if (this.selectedDietPlan == 'hypertension') {
                    this.selectedThemeColor = this.hypertensionTheme;
                }
                else if (this.selectedDietPlan == 'cholesterol') {
                    this.selectedThemeColor = this.cholesterolTheme;
                }
                else if (this.selectedDietPlan == 'pcos') {
                    this.selectedThemeColor = this.pcosThemeColor;
                }
                else if (this.selectedDietPlan.split('_').length > 0 && this.selectedDietPlan.split('_')[0] == 'muscleGain') {
                    this.selectedThemeColor = this.muscleThemeColor;
                }
                else if (this.selectedDietPlan.split('_').length > 0 && this.selectedDietPlan.split('_')[0] == 'fatShredding') {
                    this.selectedThemeColor = this.fatThemeColor;
                }
                else {
                    this.selectedThemeColor = this.weightLossThemeColor;
                }
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedPlanThemeColor = this.selectedThemeColor;
            }
            else if ('Detox' in dietData || 'detox' in dietData) {
                this.isDetox = true;
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox = true;
                this.detoxToggle = true;
                this.selectedThemeColor = this.detoxThemeColor;
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedPlanThemeColor = this.selectedThemeColor;
            }
            else {
                this.isDetox = false;
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox = false;
                this.detoxToggle = false;
                if (this.selectedDietPlan == 'weightLoss') {
                    this.selectedThemeColor = this.weightLossThemeColor;
                }
                else if (this.selectedDietPlan == 'immunity_booster') {
                    this.selectedThemeColor = this.immunityThemeColor;
                }
                else if (this.selectedDietPlan == 'weightLossPlus') {
                    this.selectedThemeColor = this.weightLossPlusThemeColor;
                }
                else if (this.selectedDietPlan == 'post_covid') {
                    this.selectedThemeColor = this.postCovidThemeColor;
                }
                else if (this.selectedDietPlan == 'diabetes') {
                    this.selectedThemeColor = this.diabetesThemeColor;
                }
                else if (this.selectedDietPlan == 'hypertension') {
                    this.selectedThemeColor = this.hypertensionTheme;
                }
                else if (this.selectedDietPlan == 'cholesterol') {
                    this.selectedThemeColor = this.cholesterolTheme;
                }
                else if (this.selectedDietPlan == 'pcos') {
                    this.selectedThemeColor = this.pcosThemeColor;
                }
                else if (this.selectedDietPlan.split('_').length > 0 && this.selectedDietPlan.split('_')[0] == 'muscleGain') {
                    this.selectedThemeColor = this.muscleThemeColor;
                }
                else if (this.selectedDietPlan.split('_').length > 0 && this.selectedDietPlan.split('_')[0] == 'fatShredding') {
                    this.selectedThemeColor = this.fatThemeColor;
                }
                else {
                    this.selectedThemeColor = this.weightLossThemeColor;
                }
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedPlanThemeColor = this.selectedThemeColor;
            }
            this.platform.ready().then(() => {
                //  this.statusBar.backgroundColorByHexString(this.selectedThemeColor);
            });
            // this.setToggleButtons();
            //  //this.utilities.presentLoading();
            // this.fetchDietPlan();
        });
    }
    openLink(url) {
        this.iab.create(url, "_blank", "location=yes");
    }
    loadHomeData() {
        // alert("Load home data");
        this.weeks = [];
        for (var i = 0; i < 7; i++) {
            let date = moment__WEBPACK_IMPORTED_MODULE_5___default()().add(i, "days");
            this.weeks.push({
                date: date.toDate(),
                formatDate: date.format("DDMMYYYY"),
                detoxDate: date.format("DD-MMM-YYYY"),
                weekName: date.format('ddd'),
                displayFormat: date.format("ddd, DD MMM")
                // displayFormat: i == 0 ? "Today,  " + date.format("DD MMM") : date.format("ddd, DD MMM")
            });
        }
        let find = this.weeks.find(o => o.formatDate == _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate);
        if (find) {
            _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate = this.dayValue = find.formatDate;
        }
        else {
            this.dayValue = this.weeks[0].formatDate;
            _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate = this.dayValue;
        }
        if (moment__WEBPACK_IMPORTED_MODULE_5___default()().format("DDMMYYYY") == this.dayValue) {
            this.isToday = true;
        }
        else {
            this.isToday = false;
        }
        this.profile = _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.profile;
        this.storage.get("localData").then((val) => {
            var _a, _b, _c;
            let data = this.utilities.parseJSON(val);
            if (((_b = (_a = data === null || data === void 0 ? void 0 : data.otherMaster) === null || _a === void 0 ? void 0 : _a.diseases) === null || _b === void 0 ? void 0 : _b.length) > 0) {
                let disaeases = data.otherMaster.diseases;
                let filters = disaeases.filter(o => (o.code == 'L' && o.isSelected == true) ||
                    (o.code == 'T' && o.isSelected == true) ||
                    (o.code == 'K' && o.isSelected == true) ||
                    (o.code == 'M' && o.isSelected == true));
                if (filters.length > 0) {
                    this.healthProblem = true;
                    _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox = false;
                    this.isDetox = false;
                    this.detoxToggle = false;
                    if (this.selectedDietPlan == 'weightLoss') {
                        this.selectedThemeColor = this.weightLossThemeColor;
                    }
                    else if (this.selectedDietPlan == 'immunity_booster') {
                        this.selectedThemeColor = this.immunityThemeColor;
                    }
                    else if (this.selectedDietPlan == 'weightLossPlus') {
                        this.selectedThemeColor = this.weightLossPlusThemeColor;
                    }
                    else if (this.selectedDietPlan == 'post_covid') {
                        this.selectedThemeColor = this.postCovidThemeColor;
                    }
                    else if (this.selectedDietPlan == 'diabetes') {
                        this.selectedThemeColor = this.diabetesThemeColor;
                    }
                    else if (this.selectedDietPlan == 'hypertension') {
                        this.selectedThemeColor = this.hypertensionTheme;
                    }
                    else if (this.selectedDietPlan == 'cholesterol') {
                        this.selectedThemeColor = this.cholesterolTheme;
                    }
                    else if (this.selectedDietPlan == 'pcos') {
                        this.selectedThemeColor = this.pcosThemeColor;
                    }
                    else if (this.selectedDietPlan.split('_').length > 0 && this.selectedDietPlan.split('_')[0] == 'muscleGain') {
                        this.selectedThemeColor = this.muscleThemeColor;
                    }
                    else if (this.selectedDietPlan.split('_').length > 0 && this.selectedDietPlan.split('_')[0] == 'fatShredding') {
                        this.selectedThemeColor = this.fatThemeColor;
                    }
                    else {
                        this.selectedThemeColor = this.weightLossThemeColor;
                    }
                    _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedPlanThemeColor = this.selectedThemeColor;
                    this.platform.ready().then(() => {
                        //  this.statusBar.backgroundColorByHexString(this.selectedThemeColor);
                    });
                }
                else {
                    this.healthProblem = false;
                }
                // this.setToggleButtons();
            }
            _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.country = (_c = data === null || data === void 0 ? void 0 : data.otherMaster) === null || _c === void 0 ? void 0 : _c.country;
        });
    }
    firstDateIsPastDayComparedToSecond(firstDate, secondDate) {
        if (firstDate.setHours(0, 0, 0, 0) - secondDate.setHours(0, 0, 0, 0) >= 0) { //first date is in future, or it is today
            return false;
        }
        return true;
    }
    dateCompare(firstDate, secondDate) {
        if (firstDate.setHours(0, 0, 0, 0) - secondDate.setHours(0, 0, 0, 0) > 0) { //first date is in future, or it is today
            return false;
        }
        return true;
    }
    initAllFuncLoad() {
        // this.getWordPressHabits();
        this.storage.get("dietData").then((res) => {
            if (res) {
                this.deleteOldDiet(res);
            }
        });
    }
    loadAllData() {
        if (!localStorage.getItem("loadAllDataAtConsume")) {
            localStorage.setItem("loadAllDataAtConsume", new Date().toString());
            this.initAllFuncLoad();
        }
        else if (this.firstDateIsPastDayComparedToSecond(new Date(localStorage.getItem("loadAllDataAtConsume")), new Date())) {
            localStorage.setItem("loadAllDataAtConsume", new Date().toString());
            this.initAllFuncLoad();
        }
    }
    startCelebration() {
        var self = this;
        var defaults = {
            origin: { y: 0.9 }
        };
        var count = 350;
        function fire(particleRatio, opts) {
            self.confetti(Object.assign({}, defaults, opts, {
                particleCount: Math.floor(count * particleRatio)
            }));
        }
        fire(0.25, {
            spread: 26,
            startVelocity: 55,
        });
        fire(0.2, {
            spread: 60,
        });
        fire(0.35, {
            spread: 100,
            decay: 0.91,
            scalar: 0.8
        });
        fire(0.1, {
            spread: 120,
            startVelocity: 25,
            decay: 0.92,
            scalar: 1.2
        });
        fire(0.1, {
            spread: 120,
            startVelocity: 45,
        });
    }
    confetti(args) {
        return window['confetti'].apply(this, arguments);
    }
    loadLess100Data() {
        // if(this.lessThan100FoodItemsCounter <= this.lessThan100FoodItemsAtTime.length){
        let selectedSlotFoodItems = this.filteredLess100Food[this.lessThan100FoodItemSlotSelected];
        let slot = this.diets[this.lessThan100FoodItemSlotSelected];
        console.log("Load less func called");
        for (let i = this.lessThan100FoodItemsCounter - 5; i < this.lessThan100FoodItemsCounter; i++) {
            // console.log(" data:" , i , " ", this.lessThan100FoodItemsAtTime[i] )
            if (selectedSlotFoodItems[i]) {
                this.lessThan100FoodItems[i] = selectedSlotFoodItems[i];
                let foodItemInSlot = [];
                foodItemInSlot = slot.data.filter((ele) => {
                    return ele.code == selectedSlotFoodItems[i].code;
                });
                if (foodItemInSlot.length > 0)
                    this.lessThan100FoodItems[i]["isExist"] = true;
                else
                    this.lessThan100FoodItems[i]["isExist"] = false;
            }
        }
        this.lessThan100FoodItemsCounter = this.lessThan100FoodItems.length + 5;
        console.log("Data ", this.lessThan100FoodItems);
        // }
    }
    filterLess100FoodItems() {
        this.filteredLess100Food = {};
        console.log(" Foods ", this.lessThan100FoodItemsAtTime);
        this.filteredLess100Food["2"] = [];
        this.filteredLess100Food["3"] = [];
        this.filteredLess100Food["4"] = [];
        this.filteredLess100Food["6"] = [];
        this.filteredLess100Food["7"] = [];
        this.lessThan100FoodItemsAtTime.forEach((ele, i) => {
            if (ele["Slots"].indexOf(2) > -1) {
                this.filteredLess100Food["2"].push(ele);
            }
            if (ele["Slots"].indexOf(3) > -1) {
                this.filteredLess100Food["3"].push(ele);
            }
            if (ele["Slots"].indexOf(4) > -1) {
                this.filteredLess100Food["4"].push(ele);
            }
            if (ele["Slots"].indexOf(6) > -1) {
                this.filteredLess100Food["6"].push(ele);
            }
            if (ele["Slots"].indexOf(7) > -1) {
                this.filteredLess100Food["7"].push(ele);
            }
            if (ele.video && ele.video != "" && ele.video.indexOf("http") > -1) {
                ele["safeVideoUrl"] = this._sanitizer.bypassSecurityTrustResourceUrl(ele.video);
            }
            if (i + 1 == this.lessThan100FoodItemsAtTime.length) {
                this.loadLess100Data();
            }
        });
    }
    highProteinData() {
        // if(this.highProteinFoodItemsCounter <= this.highProteinFoodItemsAtTime.length){
        let selectedSlotFoodItems = this.filteredHighProtein[this.highProteinFoodItemSlotSelected];
        let slot = this.diets[this.highProteinFoodItemSlotSelected];
        console.log("Load less func called");
        for (let i = this.highProteinFoodItemsCounter - 5; i < this.highProteinFoodItemsCounter; i++) {
            // console.log(" data:" , i , " ", this.highProteinFoodItemsAtTime[i] )
            if (selectedSlotFoodItems[i]) {
                this.highProteinFoodItems[i] = selectedSlotFoodItems[i];
                let foodItemInSlot = [];
                foodItemInSlot = slot.data.filter((ele) => {
                    return ele.code == selectedSlotFoodItems[i].code;
                });
                if (foodItemInSlot.length > 0)
                    this.highProteinFoodItems[i]["isExist"] = true;
                else
                    this.highProteinFoodItems[i]["isExist"] = false;
            }
        }
        this.highProteinFoodItemsCounter = this.highProteinFoodItems.length + 5;
        console.log("Data ", this.highProteinFoodItems);
        // }
    }
    filterHighProtienItems() {
        this.filteredHighProtein = {};
        console.log(" filteredHighProtein ", this.highProteinFoodItemsAtTime);
        this.filteredHighProtein["2"] = [];
        this.filteredHighProtein["3"] = [];
        this.filteredHighProtein["4"] = [];
        this.filteredHighProtein["6"] = [];
        this.filteredHighProtein["7"] = [];
        this.highProteinFoodItemsAtTime.forEach((ele, i) => {
            if (ele["Slots"].indexOf(2) > -1) {
                this.filteredHighProtein["2"].push(ele);
            }
            if (ele["Slots"].indexOf(3) > -1) {
                this.filteredHighProtein["3"].push(ele);
            }
            if (ele["Slots"].indexOf(4) > -1) {
                this.filteredHighProtein["4"].push(ele);
            }
            if (ele["Slots"].indexOf(6) > -1) {
                this.filteredHighProtein["6"].push(ele);
            }
            if (ele["Slots"].indexOf(7) > -1) {
                this.filteredHighProtein["7"].push(ele);
            }
            if (ele.video && ele.video != "" && ele.video.indexOf("http") > -1) {
                ele["safeVideoUrl"] = this._sanitizer.bypassSecurityTrustResourceUrl(ele.video);
            }
            if (i + 1 == this.highProteinFoodItemsAtTime.length) {
                this.highProteinData();
            }
        });
    }
    addFoodItem(type, foodItem, parentIndex) {
        //this.utilities.presentLoading();
        let slotIndex = type == "less100" ? this.lessThan100FoodItemSlotSelected : type == "highProtien" ? this.highProteinFoodItemSlotSelected : this.healthData[parentIndex]["healthyChoicesFoodItemSlotSelected"];
        let slot = this.diets[slotIndex]; //type == "less100" ? this.diets[this.lessThan100FoodItemSlotSelected] : this.diets[this.highProteinFoodItemSlotSelected];
        let data = slot.data;
        let foodCodeList = [];
        let sameCategoryExist = false;
        foodItem.portion = foodItem.portion ? foodItem.portion : 1;
        for (let i = 0; i < data.length; i++) {
            if (data[i]["Type"].slice(0, 1).toLowerCase() == foodItem["Type"].slice(0, 1).toLowerCase()) {
                foodCodeList.push({ code: foodItem.code, portion: parseFloat(foodItem.portion), "foodSource": foodItem["foodSource"] ? foodItem["foodSource"] : "INTERNAL", "eaten": foodItem.eaten ? foodItem.eaten : -1 });
                sameCategoryExist = true;
            }
            else {
                foodCodeList.push({ code: data[i].code, portion: parseFloat(data[i].portion), "foodSource": foodItem["foodSource"] ? foodItem["foodSource"] : "INTERNAL", "eaten": foodItem.eaten ? foodItem.eaten : -1 });
            }
        }
        if (!sameCategoryExist) {
            foodCodeList.push({ code: foodItem.code, portion: parseFloat(foodItem.portion), "foodSource": foodItem["foodSource"] ? foodItem["foodSource"] : "INTERNAL", "eaten": foodItem.eaten ? foodItem.eaten : -1 });
        }
        let reqBody = {
            foodCodeList,
            slot: slotIndex,
            detox: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox,
            date: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate,
            country: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.country
        };
        console.log("reqBody", reqBody);
        this.appServices.postOptionFoodList(reqBody, _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.email).then(success => {
            // this.utilities.hideLoader();
            // this.router.navigate(["consume"]);
            console.log("detail", success);
            if (sameCategoryExist) {
                //   this.utilities.showSuccessToast("In " + slot.slotName + " item replaced another of same category");
            }
            else {
                //   this.utilities.showSuccessToast("In " + slot.slotName + ", item is added");
            }
            //  if (type == "less100") this.utilities.logEvent("less_than_100_foodItem_added", {});
            //   else if (type == "highProtien") this.utilities.logEvent("high_protien_foodItem_added", {});
            //   else this.utilities.logEvent("healthy_foodItem_added", {});
            //this.fetchDiet(CONSTANTS.isDetox, CONSTANTS.dietDate);
            // this.ionContent.scrollToTop();
            // this.slides.slideTo(slotIndex, 200);
            this.loadAgain = true;
        }, err => {
            this.utilities.hideLoader();
            console.log("details error", err);
        });
    }
    longDescription(html) {
        return this.convertString(html.replace(/\[(.*?)\]/ig, ""));
    }
    convertString(input) {
        return input.split('<a').join('<a target="_blank"');
    }
    checkUserPlanStatus() {
        // alert("Check user plan status...");
        this.appService.getOnePlan().then(res => {
            this.storage.set("userPlanData", res);
            // alert("Got user plan status...")
            let data = JSON.parse(JSON.stringify(res));
            let a = moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date(data.planExpiryDate), "DD.MM.YYYY");
            let b = moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date(data.profile.createdDate), "DD.MM.YYYY");
            let diffDays = a.diff(b, 'days');
            let expDateDiff = this.dateCompare(new Date(), new Date(data.planExpiryDate));
            // if(moment(new Date(data.planExpiryDate)).format("DDMMYYYY") == moment(new Date(data.profile.createdDate)).format("DDMMYYYY") || (diffDays <= 7 && expDateDiff)) {
            //   this.showFreeTrialOfferIcon = true;
            // }
            // CONSTANTS.isPlanActiveParent = this.isPlanActive = this.isPlanActiveForDiet = data.isPlanActive;
            this.profile = _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.profile = data.profile;
            _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.email = _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.profile.email;
            //  this.recipeLoadData();
            // alert("Checkiing plan expiry date");
            if (this.firstDateIsPastDayComparedToSecond(new Date(), new Date(data['planExpiryDate']))) {
                // alert("Find plan active");
                this.planStatusChecked = true;
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isPlanActiveParent = this.isPlanActive = this.isPlanActiveForDiet = true;
                this.isRandomLock = false;
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isRandomLock = this.isRandomLock;
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.Diet_plan_open = true;
                // this.checkAnotherLockFlow();
                // this.dateSlides.lockSwipes(!data.isPlanActive);
            }
            else if (this.utilities.isDeviceiOS() || this.utilities.isDeviceAndroid()) {
                // alert("Checkiing plan for android and ios");
                // Checking user plan from firestore
                //   this.fetchPlan(data);
            }
            else {
                // alert("Checkiing plan expiry date");
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isPlanActiveParent = this.isPlanActive = this.isPlanActiveForDiet = false;
                //   this.checkAnotherLockFlow();
                this.planStatusChecked = true;
            }
            if (!localStorage.getItem("showedOffer") && !_core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isPlanActiveParent) {
                localStorage.setItem("showedOffer", "true");
                let self = this;
                setTimeout(() => {
                    //    self.getCouponDataConsumer();
                }, 60000 * 3);
            }
            // Survey Page check
            if (!_core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isPlanActiveParent && !localStorage.getItem('surveyOpend')) {
                setTimeout(() => {
                    if (!localStorage.getItem('surveyOpend')) {
                        //      this.checkSurveyStatus();
                    }
                }, this.checkSurveyInterval);
            }
            this.appService.isPremiumUserFunc();
        }, err => {
            console.log("PlanOne api Error: ", err);
            // this.utilities.presentAlert(
            //   "Something went wrong! Please try after some time."
            // );
        });
    }
    mainCircleRadiousAnimate() {
        let self = this;
        setInterval(() => {
            self.radiousValue = !self.radiousValue; // == 55 ? 52 : 55
        }, 2000);
    }
    upgradePlan() {
        this.paymentSubscribeModel('update diet plan');
        // this.router.navigate(["upgrade-plan"]);
    }
    upgradeLockPlan() {
        this.paymentSubscribeModel('unlock your diet plan');
        // this.router.navigate(["upgrade-plan"]);
    }
    Continue() {
        this.isExpired = false;
        console.log("close plan");
    }
    // weightGraph() {
    //   this.appService.getWeightGraph().then(
    //     res => {
    //       this.utilities.hideLoader();
    //       this.weightGraphData = JSON.parse(JSON.stringify(res));
    //       console.log("this.weightGraphData", this.weightGraphData);
    //       setTimeout(()=>{
    //       this.storage.get("profileData").then(val => {
    //         let profile = this.utilities.parseJSON(val);
    //         console.log("ZprofileData Alam:-", profile);
    //         if(profile.profile.dietPlanType==="muscleGain" || profile.profile.dietPlanType==="fatShredding"){
    //           this.isManjhari=true;
    //           this.isManjhariNew = "manjhari.jpeg";
    //           this.resultData[0].isPlantype = true;
    //           this.resultData[1].isPlantype = true;
    //           this.resultData[2].isPlantype = true;
    //           //this.videoManjhari = "https://www.youtube.com/embed/raL0GedTV-Q";
    //           }
    //           else{
    //             this.isManjhari=false;
    //             this.isManjhariNew = "manjhari2.png";
    //             this.resultData[0].isPlantype = false;
    //             this.resultData[1].isPlantype = false;
    //             this.resultData[2].isPlantype = false;
    //           }
    //           this.resultDataUpdated = [...this.resultData].filter(item=>{
    //             return item.isPlantype===true;
    //           });
    //       });
    //     },100);
    //     //  this.weightIsCollaped();
    //     },
    //     err => {
    //       this.utilities.hideLoader();
    //     }
    //   );
    // }
    ngOnDestroy() {
        console.log("destroy");
        clearInterval(this.intervalPercent);
        clearInterval(this.myInterval);
        clearInterval(this.interval);
        clearInterval(this.deficitInterval);
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            //  await this.setToggleButtons();
            //  await this.willLeave();
        });
    }
    ngAfterViewInit() {
        this.fetchDietPlan();
    }
    filterActualDietPlan(data) {
        return data.filter(item => {
            return item.currentSlot == true;
        });
    }
    caloryChanged() {
        console.log("caloryChanged", this.isActiveButton);
        if (this.isActiveButton == 0) {
            this.isActiveButton = 1;
            this.isDistributionActiveButton = 0;
            this.calory = 1;
        }
        else {
            this.isActiveButton = 0;
            this.calory = 0;
        }
    }
    caloryDistributionChanged() {
        console.log("caloryDistributionChanged", this.isDistributionActiveButton);
        if (this.isDistributionActiveButton == 0) {
            this.isDistributionActiveButton = 1;
            this.isActiveButton = 0;
            this.calory = 2;
        }
        else {
            this.isDistributionActiveButton = 0;
            this.calory = 0;
        }
    }
    fetchFormatedDate(date) {
        const day = date.toString().substring(0, 2);
        const month = date.toString().substring(2, 4);
        const year = date.toString().substring(4, 8);
        console.log("formatedDate", year + "-" + month + "-" + day);
        return year + "-" + month + "-" + day;
    }
    returnLength(obj) {
        return this.utilities.objectLength(obj);
    }
    dateSlideChanged(ev) {
        console.log("Evee ", ev);
    }
    slideChanged(event) {
    }
    eatenStatusUpdate(item, slot, slotIndex, foodIndex) {
        if (this.currentDateIndex == 0) {
            let foodCodeList = [];
            let eatenStatus = false;
            //   this.utilities.logEvent("Counter_add_home", {});
            slot.data.filter((ele) => {
                if (ele.itemCode == item.itemCode) {
                    if (ele.eaten > 0)
                        eatenStatus = false;
                    else
                        eatenStatus = true;
                    foodCodeList.push({ code: ele.code, eaten: ele.eaten > 0 ? -1 : 2, slot: slot.slot });
                }
            });
            this.diets[slotIndex]["data"][foodIndex].eaten = this.diets[slotIndex]["data"][foodIndex].eaten > 0 ? -1 : 2;
            this.storage.get("dietData").then((res) => {
                let dietPlan = _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox ? 'detox' : _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.selectedDietPlan;
                if (res && res[moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date()).format("DDMMYYYY")] && res[moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date()).format("DDMMYYYY")][dietPlan]) {
                    res[moment__WEBPACK_IMPORTED_MODULE_5___default()(new Date()).format("DDMMYYYY")][dietPlan]["diets"][slotIndex]['data'][foodIndex] = this.diets[slotIndex]["data"][foodIndex];
                    this.storage.set("dietData", res).then((res) => {
                        this.todaysCalCount();
                        if (this.showProtienTracker) {
                            this.getConsumedProtien();
                        }
                    });
                }
            });
            let reqBody = {
                foodCodeList,
                date: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate
            };
            console.log("reqBody", reqBody);
            //  this.utilities.logEvent("update_food_details", reqBody);
            this.appServices.updateEatenFoodItems(reqBody).then(success => {
                console.log("detail", success);
                // this.utilities.showSuccessToast(message);
                let foodName = item.Food.length > 28 ? item.Food.slice(0, 28) + ".." : item.Food;
                let orangeFoodItem = item.Food.length > 12 ? item.Food.slice(0, 12) + ".." : item.Food;
                if (eatenStatus) {
                    let message = "";
                    if (Number(item.Score) == 9 || Number(item.Score) == 6) {
                        message = "You just added " + foodName + ". <span style='color:green'>Excellent choice! </span>";
                        message = "You just added " + foodName + ". <span style='color:green'>Excellent choice! </span>";
                        message = "You just added " + foodName + ". <span style='color:green'>Excellent choice! </span>";
                        message = "You just added " + foodName + ". <span style='color:green'>Excellent choice! </span>";
                        message = "You just added " + foodName + ". <span style='color:green'>Excellent choice! </span>";
                        message = "You just added " + foodName + ". <span style='color:green'>Excellent choice! </span>";
                        message = "You just added " + foodName + ". <span style='color:green'>Excellent choice! </span>";
                        message = "You just added " + foodName + ". <span style='color:green'>Excellent choice! </span>";
                        message = "You just added " + foodName + ". <span style='color:green'>Excellent choice! </span>";
                    }
                    else if (Number(item.Score) == 3) {
                        message = "You just had " + foodName + ".  <span style='color:orange'>Good choice.</span>";
                        message = "You just had " + foodName + ".  <span style='color:orange'>Good choice.</span>";
                        message = "You just had " + foodName + ".  <span style='color:orange'>Good choice.</span>";
                        message = "You just had " + foodName + ".  <span style='color:orange'>Good choice.</span>";
                        message = "You just had " + foodName + ".  <span style='color:orange'>Good choice.</span>";
                        message = "You just had " + foodName + ".  <span style='color:orange'>Good choice.</span>";
                        message = "You just had " + foodName + ".  <span style='color:orange'>Good choice.</span>";
                        message = "You just had " + foodName + ".  <span style='color:orange'>Good choice.</span>";
                        message = "You just had " + foodName + ".  <span style='color:orange'>Good choice.</span>";
                    }
                    else if (Number(item.Score) == 1) {
                        message = "I see you have consumed " + orangeFoodItem + ". Explore better “Recommended options”";
                        message = "I see you have consumed " + orangeFoodItem + ". Explore better “Recommended options”";
                        message = "I see you have consumed " + orangeFoodItem + ". Explore better “Recommended options”";
                        message = "I see you have consumed " + orangeFoodItem + ". Explore better “Recommended options”";
                        message = "I see you have consumed " + orangeFoodItem + ". Explore better “Recommended options”";
                        message = "I see you have consumed " + orangeFoodItem + ". Explore better “Recommended options”";
                        message = "I see you have consumed " + orangeFoodItem + ". Explore better “Recommended options”";
                        message = "I see you have consumed " + orangeFoodItem + ". Explore better “Recommended options”";
                        message = "I see you have consumed " + orangeFoodItem + ". Explore better “Recommended options”";
                    }
                    else if (!item.Score) {
                        message = "You ate " + foodName + ". I have better suggestions for you!";
                    }
                    //eatenStatus ? "Added " + item.Calories + " kcal in to Calories counter" : "Removed " + item.Calories + " kcal from to Calories counter";
                    clearTimeout(this.tipTimeout);
                    console.log("Timerr Cleared  ############################################");
                    clearTimeout(this.tipMsgInterval);
                    this.activeTip = this.senitizeHTML('<span class=""><img src="./assets/img/typing_icon.gif" class="loading-noti-msg"></img></span>');
                    this.tipMsgInterval = setTimeout(() => {
                        clearTimeout(this.tipMsgInterval);
                        this.activeTip = this.senitizeHTML(message);
                        localStorage.removeItem("foodNotificationMsg");
                        this.tipMsgInterval = setTimeout(() => {
                            clearTimeout(this.tipMsgInterval);
                            //     this.tip();
                        }, this.loadingMsgTime);
                    }, this.loadingMsgImgTime);
                }
                // this.activeTip = this.senitizeHTML(message);
                // console.log("Timerr Started  ############################################")
                // this.tipTimeout = setTimeout(() => {
                //   this.tip();
                //   console.log("Timerr Completed  ############################################")
                // }, 10000);
                // //this.fetchDiet(CONSTANTS.isDetox, CONSTANTS.dietDate);
            }, err => {
                console.log("details error", err);
            });
        }
        // else{
        //   this.utilities.showErrorToast('You can not eat in future date')
        // }
    }
    removeFoodItem(item, slot) {
        this.removeItem.emit({ item: item, slot: slot });
    }
    removeDietSlot(i) {
        //this.utilities.presentLoading();
        console.log("Clicked index ", i);
        //  this.utilities.logEvent("DietPlan_07aDeleteFromMainPage", {})
        let foodCodeList = [];
        let reqBody = {
            foodCodeList,
            slot: i,
            detox: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.isDetox,
            date: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.dietDate,
            country: _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.country
        };
        console.log("reqBody", reqBody);
        this.appServices.postOptionFoodList(reqBody, _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.email).then(success => {
            console.log("detail", success);
            //this.fetchDiet(CONSTANTS.isDetox, CONSTANTS.dietDate);
        }, err => {
            this.utilities.hideLoader();
            console.log("details error", err);
        });
    }
    refreshFoodItem(dataItem, index) {
        // 
        //  this.loadHomeData();
        this.refreshDiet.emit({ dataItem: dataItem, index: index });
    }
    refresh() {
        //this.utilities.presentLoading();
        //this.testGoogleFit();
        //  this.startActivityTracking();
    }
    selectOption(ind) {
        if (this.utilities.isPlanExpired() && ind != this.slotIndex) {
            this.paymentSubscribeModel('update diet plan');
            // this.upgradePlan();
            return true;
        }
        else {
            // //this.utilities.presentLoading();
            // if (ind != this.slotIndex) {
            //   if (!this.isPlanActiveForDiet) {
            //     this.paymentSubscribeModel('update diet plan');
            //     // this.router.navigate(['upgrade-plan']);
            //   }
            //   else {
            //     this.isExpired = false;
            //     let code = "";
            //     let portion = "";
            //     let category = "";
            //     for (let index = 0; index < this.diets[ind].data.length; index++) {
            //       if (this.diets[ind].data.length > 1) {
            //         code = code + this.diets[ind].data[index].code + ",";
            //         portion = portion + this.diets[ind].data[index].portion + ",";
            //         category = category+ this.diets[ind].data[index].category +",";
            //       } else {
            //         code = code + this.diets[ind].data[index].code;
            //         portion = portion + this.diets[ind].data[index].portion;
            //         category = category+ this.diets[ind].data[index].category;
            //       }
            //     }
            //     //this.router.navigate(["options-new"], {
            //     this.router.navigate(["options"], {
            //       queryParams: {
            //         param: this.diets[ind].slotName,
            //         slot: ind,
            //         foodCode: code,
            //         portion,
            //         isV: true,
            //         category:category
            //       }
            //     });
            //   }
            // }
            // else {
            let code = "";
            let portion = "";
            let category = "";
            for (let index = 0; index < this.diets[ind].data.length; index++) {
                if (this.diets[ind].data.length > 1) {
                    code = code + this.diets[ind].data[index].code + ",";
                    portion = portion + this.diets[ind].data[index].portion + ",";
                    category = category + this.diets[ind].data[index].category + ",";
                }
                else {
                    code = code + this.diets[ind].data[index].code;
                    portion = portion + this.diets[ind].data[index].portion;
                    category = category + this.diets[ind].data[index].category;
                }
            }
            // this.router.navigate(["options-new"], {
            this.router.navigate(["options"], {
                queryParams: {
                    param: this.diets[ind].slotName,
                    slot: ind,
                    foodCode: code,
                    portion,
                    isV: true,
                    category: category
                }
            });
            // }
        }
    }
    gotoEditProfile() {
        this.storage.set("isNavigateDiet", true);
        this.router.navigate(["drinks"], { queryParams: { prop: 'edit' } });
    }
    gotoPersonalise() {
        this.router.navigate(["personalise"], { queryParams: { prop: 'edit', toConsume: 'toConsume' } });
    }
    gotoEditPersonalPlan() {
        this.router.navigate(["other-foods-selection"], { queryParams: { isPlanActiveForDiet: this.isPlanActiveForDiet, diets: JSON.stringify(this.diets) } });
    }
    faq() {
        this.router.navigate(["faq"]);
    }
    presentToast(txt) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: txt,
                duration: 3000,
                position: "top",
                mode: "ios",
                color: "dark"
            });
            toast.present();
        });
    }
    customNotification(custom_element, custom_message) {
        const element = this.el.nativeElement.querySelector(custom_element);
        element.innerText = custom_message;
        element.className = "slide-up fade-out glass-text";
        setTimeout(function () {
            element.className = "glass-text";
        }, 2000);
    }
    custom_notification(custom_element, custom_message) {
        this.message = custom_message;
        const alarm_element = this.el.nativeElement.querySelector(custom_element);
        alarm_element.classList.add("slide-up");
        setTimeout(function () {
            alarm_element.classList.remove("slide-up");
        }, 2000);
    }
    doSomethingWithCurrentValue(e) {
        // console.log('event', e);
    }
    initializeApp() { }
    showLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            //this.utilities.presentLoading();
            console.log("Loading dismissed!");
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            this.loadingController
                .getTop()
                .then(res => (res ? this.loadingController.dismiss() : null));
        });
    }
    gotoFoodDetail(foodCode, ind) {
        if (!this.isPlanActiveForDiet && ind != this.slotIndex) {
            return;
        }
        let foodDetails = this.diets[ind]["data"].filter((ele) => {
            return ele._id == foodCode;
        })[0];
        if (foodDetails["foodSource"] && (foodDetails["foodSource"].toLowerCase() == 'personal' || foodDetails["foodSource"].toLowerCase() == 'external')) {
            this.router.navigate(['add-edit-todays-calorie-count'], {
                queryParams: {
                    selectedSlotIndex: ind,
                    selectedFoodItemIndex: foodCode,
                    foodDetails: JSON.stringify(foodDetails),
                    foodSource: foodDetails["foodSource"],
                    isFromHistory: 'false',
                    toMainPage: 'true',
                    isEdit: 'true'
                }
            });
            return true;
        }
        if (!this.isPlanActiveForDiet && ind != this.slotIndex) {
            this.paymentSubscribeModel('food detail');
            // this.upgradePlan();
            return true;
        }
        if (this.utilities.isPlanExpired() && this.slotIndex != ind) {
            // this.upgradePlan();
            this.paymentSubscribeModel('food detail');
            return true;
        }
        else {
            this.isExpired = false;
            let code = "";
            let portion = "";
            for (let index = 0; index < this.diets[ind].data.length; index++) {
                if (this.diets[ind].data.length > 1) {
                    code = code + this.diets[ind].data[index].code + ",";
                    portion = portion + this.diets[ind].data[index].portion + ",";
                }
                else {
                    code = code + this.diets[ind].data[index].code;
                    portion = portion + this.diets[ind].data[index].portion;
                }
            }
            this.dataSlot = JSON.stringify(this.diets[ind].data);
            this.router.navigate(["food-detail"], {
                queryParams: {
                    param: this.diets[ind].slotName,
                    slot: ind,
                    foodCode: this.dataSlot,
                    portion,
                    mainCode: foodCode,
                    isV: "true"
                }
            });
        }
    }
    gotoNextItemDetail(data, diet, ing, xindex) {
        console.log("Next Call:-", data, diet, ing, xindex);
    }
    roundUpvalue(val) {
        return Math.round(val);
    }
    DateFormat(isDate) {
        if (isDate != undefined) {
            isDate = isDate.toString();
            if (isDate == "false") {
                return new Date().toDateString();
            }
            else {
                return isDate;
            }
        }
        else {
            return "";
        }
    }
    getAbsoluteWeight(a, b) {
        if (a != undefined && b != undefined) {
            return Math.abs(a - b).toFixed(1);
        }
        else {
            return 0;
        }
    }
    getCurrentWeight(weight) {
        return Math.abs(weight).toFixed(1);
    }
    DayCount(obj) {
        if (obj.startDate != undefined) {
            let dateArr = obj.startDate.split('-');
            let fiterData = this.months.filter(item => {
                return item.month == dateArr[1].toUpperCase();
            });
            let firstNum = new Date(dateArr[2], fiterData[0].ind - 1, dateArr[0]);
            const date1 = firstNum;
            const date2 = new Date();
            const diffTime = Math.abs(date2 - date1);
            const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
            return diffDays;
        }
        else {
            return "";
        }
    }
    customRoundof(num) {
        if (num != undefined && num > 0 && !Number.isNaN(num)) {
            return Math.round(num);
        }
        else {
            return 0;
        }
    }
    openMoreHabit(ind) {
        this.isMore[ind] = !this.isMore[ind];
    }
    roundingVal(val) {
        if (isNaN(val)) {
            return 0;
        }
        return Math.round(val);
    }
    editDiets(val, diets) {
        if (!this.isPlanActiveForDiet) {
            this.router.navigate(['upgrade-plan']);
        }
        else {
            if (this.utilities.isPlanExpired()) {
                this.upgradePlan();
                return true;
            }
            else {
                this.storage.remove("slotTiming");
                this.storage.set("diets", diets);
                this.router.navigate(['slot-dinner-time']);
            }
        }
    }
    showTime(diet) {
        if (diet.slot == 0 || diet.slot == 1) {
            return "";
        }
        else {
            if (diet.slot == 3 || diet.slot == 5 || diet.slot == 6 || !diet.time) {
                return ""; //"Optional"; //"As per choice";
            }
            else {
                return diet.time;
            }
        }
    }
};
HomeVerticalComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.Platform },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.Router },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_12__.Storage },
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__.UTILITIES },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_4__.AppService },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ChangeDetectorRef },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.ToastController },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ElementRef },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_13__.DomSanitizer },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__.InAppBrowser },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_4__.AppService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.ActivatedRoute }
];
HomeVerticalComponent.propDecorators = {
    refreshDiet: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Output, args: ['refreshDiet',] }],
    removeItem: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Output, args: ['removeItem',] }],
    kcounter: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Output, args: ['kcounter',] }],
    caloriesDeficitCanvas: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ["caloriesDeficitCanvas",] }],
    bottomSlide: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ["bottomSlide",] }],
    videoSlideWhySDP: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ["videoSlideWhySDP", { static: false },] }],
    videoSlide: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ["videoSlide", { static: false },] }],
    ionContent: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_10__.IonContent, { static: false },] }],
    resultSlide: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.ViewChild, args: ["resultSlide", { static: false },] }],
    isPlanActiveForDiet: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Input }],
    diets: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Input, args: ['diets',] }],
    slotIndex: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Input, args: ['slotIndex',] }]
};
HomeVerticalComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-home-vertical',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_vertical_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_home_vertical_component_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], HomeVerticalComponent);



/***/ }),

/***/ 99737:
/*!***************************************************************!*\
  !*** ./src/app/components/home-water/home-water.component.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomeWaterComponent": () => (/* binding */ HomeWaterComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_water_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./home-water.component.html */ 10047);
/* harmony import */ var _home_water_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home-water.component.scss */ 74450);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);




let HomeWaterComponent = class HomeWaterComponent {
    constructor() {
        this.currentDateIndex = 0;
        this.drankwater = 0;
        this.totalTodaysCalories = 0;
        this.tolalCalories = 0;
        this.habitList = [];
        this.difference = "";
    }
    ngOnInit() { }
    roundingVal(val) {
        if (isNaN(val)) {
            return 0;
        }
        return Math.round(val);
    }
};
HomeWaterComponent.ctorParameters = () => [];
HomeWaterComponent.propDecorators = {
    currentDateIndex: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['currentDateIndex',] }],
    drankwater: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['drankwater',] }],
    totalTodaysCalories: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['totalTodaysCalories',] }],
    tolalCalories: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['tolalCalories',] }],
    habitList: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['habitList',] }],
    difference: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input, args: ['difference',] }]
};
HomeWaterComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-home-water',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_water_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_home_water_component_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], HomeWaterComponent);



/***/ }),

/***/ 95642:
/*!***************************************************************!*\
  !*** ./src/app/components/recipe-day/recipe-day.component.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RecipeDayComponent": () => (/* binding */ RecipeDayComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_recipe_day_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./recipe-day.component.html */ 906);
/* harmony import */ var _recipe_day_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./recipe-day.component.scss */ 98788);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../app.service */ 38198);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/platform-browser */ 86219);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../core/utility/utilities */ 53533);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ 78713);
/* harmony import */ var _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../core/constants/constants */ 92133);










let RecipeDayComponent = class RecipeDayComponent {
    constructor(storage, appServices, router, route, _sanitizer, utilities) {
        this.storage = storage;
        this.appServices = appServices;
        this.router = router;
        this.route = route;
        this._sanitizer = _sanitizer;
        this.utilities = utilities;
        // postArray=[];
        this.foodDetails = { video: "", recipe: "", steps: "", Name: "", Calories: "", Carbs: "", Protien: "", Fat: "", Fiber: "" };
        this.data = { data: [{ imageUrl: "", Food: "", Calories: "", Carbs: "", Protien: "", Fat: "" }] };
    }
    ngOnInit() {
        if (_core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.country && _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.country == "IND")
            this.loadData();
    }
    senitizedData(video) {
        console.log("videoUrls", video);
        this.videoUrl = this._sanitizer.bypassSecurityTrustResourceUrl(video);
    }
    initData(res) {
        let reqBody = {
            foodId: res.foodCode
        };
        this.appServices.fetchFood(reqBody, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.email).subscribe(res => {
            this.foodDetails = JSON.parse(JSON.stringify(res)).dietItem;
            console.log("fetchFood Response:", res);
            if (this.foodDetails && (this.foodDetails.video || this.foodDetails.video != "-")) {
                this.foodDetails.video = this.foodDetails.video
                    .toString()
                    .replace('"', "")
                    .replace('"', "");
            }
            this.senitizedData(this.foodDetails.video);
            if (this.foodDetails) {
                if (this.foodDetails.steps != null) {
                    this.foodDetails.steps = this.foodDetails.steps.trim();
                }
                if (this.foodDetails.steps == "") {
                    this.foodDetails.steps = "~";
                }
                if (this.foodDetails.recipe != null) {
                    this.foodDetails.recipe = this.foodDetails.recipe.trim();
                }
                if (this.foodDetails.recipe == "") {
                    this.foodDetails.recipe = "~";
                }
            }
            // this.portion = this.foodDetails.portion;
        }, err => {
            console.log("fetchFood error:", err);
        });
    }
    loadData() {
        this.videoUrl = "";
        let allData = [];
        let code = ["156",
            "015",
            "010",
            "383",
            "004",
            "003",
            "027",
            "054",
            "078",
            "083",
            "128",
            "129",
            "148",
            "149",
            "167",
            "169",
            "168",
            "172",
            "176",
            "254",
            "260",
            "271",
            "302",
            "304",
            "305",
            "306",
            "311",
            "313",
            "315",
            "324",
            "329",
            "372",
            "414",
            "421",
            "427",
            "430"];
        this.storage.get("recipeDate").then(resDate => {
            if (resDate && new Date(resDate).getDate() == new Date().getDate() && new Date(resDate).getMonth() == new Date().getMonth()) {
                this.storage.get("recipeDay").then(res => {
                    this.initData(res);
                    // this.data = code[Number(res)];
                    // console.log("repostArray",this.data);
                    // // setTimeout(() => {
                    // let reqBody = {
                    //   foodId: this.data
                    // };
                    // this.appServices.fetchFood(reqBody).subscribe(
                    //   res => {
                    //     this.foodDetails = JSON.parse(JSON.stringify(res)).dietItem;
                    //     console.log("fetchFood Response:", res);
                    //     if (this.foodDetails.video || this.foodDetails.video != "-") {
                    //       this.foodDetails.video = this.foodDetails.video
                    //         .toString()
                    //         .replace('"', "")
                    //         .replace('"', "");
                    //     }
                    //     this.senitizedData(this.foodDetails.video);
                    //     if (this.foodDetails.steps != null) {
                    //       this.foodDetails.steps = this.foodDetails.steps.trim();
                    //     }
                    //     if (this.foodDetails.steps == "") {
                    //       this.foodDetails.steps = "~";
                    //     }
                    //     if (this.foodDetails.recipe != null) {
                    //       this.foodDetails.recipe = this.foodDetails.recipe.trim();
                    //     }
                    //     if (this.foodDetails.recipe == "") {
                    //       this.foodDetails.recipe = "~";
                    //     }
                    //     // this.portion = this.foodDetails.portion;
                    //   },
                    //   err => {
                    //     console.log("fetchFood error:", err);
                    //   }
                    // );
                });
            }
            else {
                this.appServices.getRecipeOfTheDay().then(res => {
                    this.storage.set("recipeDate", new Date());
                    this.storage.set("recipeDay", res);
                    this.initData(res);
                });
                // let index="0";
                // this.storage.set("recipeDay",0);
                // this.storage.set("recipeDate",new Date());
                // this.storage.get("recipeDay").then(res=>{
                //   if(res=="0"){
                //     index="0";
                //   }
                //   else{
                //     index = res;
                //   }
                // });
                // if(code.length-1>=Number(index)){
                // this.data = code[Number(index)+1];
                //   console.log("repostArray",this.data);
                //   // setTimeout(() => {
                //    let reqBody = {
                //     foodId: this.data
                //   };
                //   this.appServices.fetchFood(reqBody).subscribe(
                //     res => {
                //       this.storage.set("recipeDay",index);
                //       this.storage.set("recipeDate",new Date());
                // this.foodDetails = JSON.parse(JSON.stringify(res)).dietItem;
                // console.log("fetchFood Response:", res);
                // if (this.foodDetails.video || this.foodDetails.video != "-") {
                //   this.foodDetails.video = this.foodDetails.video
                //     .toString()
                //     .replace('"', "")
                //     .replace('"', "");
                // }
                // this.senitizedData(this.foodDetails.video);
                // if (this.foodDetails.steps != null) {
                //   this.foodDetails.steps = this.foodDetails.steps.trim();
                // }
                // if (this.foodDetails.steps == "") {
                //   this.foodDetails.steps = "~";
                // }
                // if (this.foodDetails.recipe != null) {
                //   this.foodDetails.recipe = this.foodDetails.recipe.trim();
                // }
                // if (this.foodDetails.recipe == "") {
                //   this.foodDetails.recipe = "~";
                // }
                //       // this.portion = this.foodDetails.portion;
                //     },
                //     err => {
                //       console.log("fetchFood error:", err);
                //     }
                //   );
                // }
            }
        });
    }
};
RecipeDayComponent.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__.Storage },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_7__.DomSanitizer },
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_3__.UTILITIES }
];
RecipeDayComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-recipe-day',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_recipe_day_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_recipe_day_component_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], RecipeDayComponent);



/***/ }),

/***/ 92133:
/*!*********************************************!*\
  !*** ./src/app/core/constants/constants.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LANGUAGE": () => (/* binding */ LANGUAGE),
/* harmony export */   "ProfileInfo": () => (/* binding */ ProfileInfo),
/* harmony export */   "CONSTANTS": () => (/* binding */ CONSTANTS),
/* harmony export */   "APIS": () => (/* binding */ APIS),
/* harmony export */   "message": () => (/* binding */ message),
/* harmony export */   "constantsJson": () => (/* binding */ constantsJson)
/* harmony export */ });
const LANGUAGE = {
    EN: "en",
    ES: "es"
};
const ProfileInfo = {
    profileName: "",
    profilePic: ""
};
const CONSTANTS = {
    islocal: true,
    url: "./assets/apis/Master.json",
    isPlanActiveParent: false,
    isDetox: false,
    profile: {
        "freePlanExpiryDateTime": "",
        "planType": "",
        "isPaymentCancelOptionAvailable": false
    },
    userDetails: {},
    dietDate: '',
    dietDateforHome: '',
    country: '',
    location_country: 'India',
    email: "",
    calBurnedToday: -1,
    selectedDietPlan: "weightLoss",
    selectedPlanThemeColor: '#01A3A4',
    defaultCalories: 0,
    Refund_policy: true,
    Diet_plan_open: true,
    isRandomLock: false,
    image_URL: 'https://app.smartdietplanner.com/images/',
    wellness_image_URL: 'https://app.smartdietplanner.com/wellness/',
    isTestEnv: false,
    isNewAPIs: true,
    encryptKey: "base64:13rCQCD8hoWXg47PhGA4y4/hCkiIOH7gRVN3SbDL7ZM=",
    postImageBaseURL: "https://smartdietplanner.com/wp-content/uploads/",
    beatoAPI: "https://portalapi.internal.beatoapp.com/merchant/v1/diet/recall_api",
    updateTargetCal: "api/updateTargetCalories?calories=",
    updateSlotRemarks: "updateRemarks?userId=",
    getSlotRemarks: "getRemarks?userId="
};
const APIS = {
    BASEURL: "https://app.smartdietplanner.com/api/",
    BASEURL8445: "https://app.smartdietplanner.com:8445/",
    BASEURL8444: "https://app.smartdietplanner.com:8444/",
    refreshBaseUrl: "https://app.smartdietplanner.com:8443/api/",
    WP_BASEURL: "https://smartdietplanner.com/wp-json/wp/v2/posts",
    defaultDetail: "defaultDetail",
    profile: "profile",
    authenticateExternal: 'authenticateExternal',
    updateProfile: "updateProfile",
    updateDemographic: "updateDemographic",
    updateLifeStyle: "updateLifeStyle",
    updateDiet: "updateDiet",
    authUrl: "authenticate?access_token=",
    loginMethod: "login",
    dietPlans: "v3/dietPlans",
    dietPlansDirect: "fetchDietPlan",
    dietCovidPlans: "v2/dietPlans",
    getOnePlan: "customer/getOnePlan",
    habitMaster: "habitMaster",
    customerHabit: "customer/habits",
    createHabit: "customer/createHabit",
    updateHabit: "customer/updateHabit",
    deleteHabits: "customer/deleteCustomerHabit",
    getHabitsForUpdate: "customer/habitsForUpdate",
    gerWeightGraph: "customer/weightGraphData",
    optionsData: "options",
    optionsDataNew: "dietPlans/options",
    optionSelection: "addDietPref",
    optionSelectionNew: "customer/addDietPref",
    payment: "payment",
    getCouponList: "getCouponList",
    subscribePlanByCoupon: "customer/subscribePlanByCoupon",
    paymentConfirm: "payment/confirm",
    removePlan: "removePlan",
    updateWeight: "customer/updateWeight",
    googleFit: "https://www.googleapis.com/fitness/v1/users/",
    sendMail: "customer/sendEmail",
    addTnC: "addTnC",
    refresh: "refresh/option",
    refreshInternational: "refresh/options",
    fetchFood: "fetch/food",
    dietPlanTiming: "save/dietPlan/timings",
    fetchDietPlans: "fetch/dietplans",
    fetchHelp: "fetch/help",
    detoxStatus: "saveupdate/detox/dietPlan/status",
    updateExpiryDate: "update/payment/details",
    updateTargetWeight: "update/target/weight",
    updateCurrentWeigt: "update/current/weight",
    timings: "fetch/dietPlan/timings",
    getCouponListOffered: "getCouponList?isOffered=true",
    getRecipeOfTheDay: "fetch/countrywise/food/items?country=IND",
    lessThan100CaloriesFoodItem: "fetch/low/calories/foods",
    highProteinFoodItem: "fetch/high/protein/foods",
    healthyChoicesFoodItem: "fetch/healthy/choices",
    lessThan100SlotsFoodItem: "fetch/cust/diet/details",
    lessThan100SlotsFoodItemNew: "fetchCustDietDetails",
    updateCustDietPlan: "update/cust/dietplan",
    referralCode: "fetch/referral/email",
    referralUser: "save/cust/payment/referral/details",
    updateReferralUser: "update/cust/payment/referral/details",
    referralUserTransactions: "fetch/cust/payment/referral/transactions",
    ratings: "save/cust/ratings",
    getDietPreference: "getDietPreference?userId=",
    updateDietPref: "updateDietPrefNew",
    fetchTodoList: "fetchTodoList",
    saveOrUpdateCustDailyTodo: "saveOrUpdateCustDailyTodo",
    saveHotLeads: "saveHotLeads",
    deleteHotLeads: "deleteHotLeads",
    getFoodPrefHistory: "getFoodPrefHistory",
    searchFoodItemByName: "searchFoodItemByName",
    updateDietPlan: "updateDietPlan",
    updateEatenFoodItems: "updateEatenFoodItems",
    postSaveFastingDetails: "saveFastingDetails",
    postFacts: "getMasterFacts",
    postFavorite: "setFavFacts",
    getRecipies: "fetchRecipes",
    fetchAffiliate: "fetchAffiliate",
    sendOTP: "sendOTP",
    verifyOTP: "verifyOTP",
    fetchOrder: "fetchOrders",
    createOrder: "createOrUpdateOrder",
    storeSurveyResponse: "storeSurveyResponse",
    copyDietPlanWeekly: "copyDietPlanWeekly",
    /* Seprate Apis to save data in DB */
    saveFasting: "saveFasting",
    getFasting: "getFasting",
    // end
    fetchFoodOptions: "custom/dietPlan/fetchFoodOptions",
    copyDietPlan: "copyDietPlan",
    searchFoodItem: "searchFoodItem"
};
const message = {
    age: "Please select age",
    single: "Please select gender",
    health: "Please select health conditions",
    marital: "Please select marital status",
    stress: "Please select any stress",
    activity: "Please select any activity",
    wakeup: "Please select wakeup time",
    sleep: "Please select sleep time",
    sleepType: "Please select sleep type",
    leaveOffice: "Please select time",
    comeBack: "Please select come back time",
    community: "Please select community",
    foodPref: "Please select food preferences",
    alchohal: "Please select alchohal",
    waterDrink: "Please select glass of water",
    food: "Please select food type",
    drinks: "Please select drinks",
    snacks: "Please select snacks",
    fruits: "Please select fruits",
    dishes: "Please select dishes",
    pules: "Please select pules and curries",
    rice: "Please select rice",
    roti: "Please select roti",
    dinner: "Please select dinner type",
    timing: "Please select time slot",
    globalError: "Something went wrong please try again!",
    country: "Please select country",
    workOuttiming: "Please select workout timing",
    selectType: "Please select your objective"
};
const constantsJson = {
    premiumPlanInculdes: [
        {
            "image": "./assets/img/free-plan/30days.png",
            "title": "100% Result oriented.",
            "subtitle": "We are 100% result oriented",
            "bgcolor": "#ffa601",
            "popupTitle": "We are 100% result oriented",
            "desc": "Our app is intelligent enough to guide you in your transformation journey. To ensure the results our dieticians will also be available to guide/support you to ensure that you get the desired results.",
            "popImage": "./assets/img/free-plan/30days.png"
        },
        {
            "image": "./assets/img/deficit-story-4.jpg",
            "title": "Access complete diet plan",
            "subtitle": "Access to all diet plan slots",
            "bgcolor": "#ffa610",
            "popupTitle": "Access to all diet plan slots",
            "desc": "We provide ample of healthy recommendations for each slot . We will also give you the access to food details along with healthy recipe suggestions. <br/> <br/>Eating healthy food of your choice and in limited portions is the only way to change your lifestyle. ",
            "popImage": "./assets/img/deficit-story-4.jpg"
        }, {
            "image": "./assets/img/free-plan/7-days-free-plan.png",
            "title": "7 Days Diet Plan",
            "subtitle": "Plan groceries in advance",
            "bgcolor": "#ffa601",
            "popupTitle": "Plan groceries in advance",
            "desc": "We can provide 7 days diet plan in advance which you can use to plan the groceries in advance and make this journey easier for you",
            "popImage": "./assets/img/dietplan-story-4.jpg"
        }, {
            "image": "./assets/img/free-plan/detox-plan-free-plan.png",
            "title": "Detox Plan",
            "subtitle": "Accelerate weight loss",
            "bgcolor": "#f8719f",
            "popupTitle": "Accelerate the weight loss",
            "desc": "You had a party and you had eaten too much. Now with detox or low calories plan you can try to come back to 3500 cal target of the week. <br/> <br/>I have designed the plan in such a way that you will not feel hungry and you will get sufficient nutrients also in the day.",
            "popImage": "./assets/img/deficit-story-1.jpg"
        }, {
            "image": "./assets/img/free-plan/nutri-check.svg",
            "title": "Nutricheck",
            "subtitle": "Meter to know what’s good or bad",
            "bgcolor": "#509bee",
            "popupTitle": "Meter to find healthy and tasty items",
            "desc": "Nutricheck uses an algorithm based on Net carbs, Protein, Calories density and fats to compute a score which indicates what is a healthy choice for you. <br/> <br/>From a database of 80,000 food items, find healthy choices, which are aligned to you taste.",
            "popImage": "./assets/img/free-plan/nutri-check.svg"
        },
        // {
        //   "image": "./assets/img/free-plan/diet-plan-analysis-free-plan.png",
        //   "title": "Diet Plan Analysis",
        //   "subtitle": "Understand Science Behind Diet plan",
        //   "bgcolor":"#ffa610",
        //   "popupTitle": "Understand the considerations made in creating the diet plan for you",
        //   "desc": "Diet plan created by us takes care  many consideration -  calories need for you,  the nutrients proportions, healthy choices based on your lifestyle disorder if any and the timings aspects. <br/><br/>Analysis will explain you how many considerations we took to create this plan for you.",
        //   "popImage" : "./assets/img/dietplan-story-1.jpg"
        // },
        {
            "image": "./assets/img/free-plan/daily-diet-reco-free-plan.png",
            "title": "Personalise diet plan",
            "subtitle": "Align Plan with your taste",
            "bgcolor": "#f8719f",
            "popupTitle": "Align plan with your taste",
            "desc": "From a list of food items you can specify what are your personal preferences. <br/> <br/>We will use the choices given by you while creating the diet plan and will try to align it with your taste so that you can follow it for lifetime",
            "popImage": "./assets/img/dietplan-story-2.jpg"
        }, {
            "image": "./assets/img/free-plan/calories-analysis-free-plan.png",
            "title": "Special recommendations",
            "subtitle": "Best healthy options",
            "bgcolor": "#509bee",
            "popupTitle": "Best healthy options",
            "desc": "We help you to choose low calories , High protein or low fat items which are recommended for you. We also guide you what is not so good for your lifestyle disorder. <br/> <br/>This will again help you in eating healthy in the day",
            "popImage": "./assets/img/deficit-story-2.jpg"
        },
        // {
        //   "image": "./assets/img/free-plan/weight-analysis-free-plan.png",
        //   "title": "Calories Deficit",
        //   "subtitle": "Most scientific way to track",
        //   "bgcolor":"#51c877"
        // },
        // {
        //   "image": "./assets/img/free-plan/meal-fasting-free-plan.png",
        //   "title": "Fasting Tracker",
        //   "subtitle": "Fast with ease",
        //   "bgcolor":"#51c877"
        // },
        {
            "image": "./assets/img/free-plan/download-diet-plan.png",
            "title": "Download plan in pdf",
            "subtitle": "Follow Diet in Offline Mode",
            "bgcolor": "#51c877",
            "popupTitle": "Follow diet plan in offline mode",
            "desc": "Diet plan for 3 days can be downloaded in pdf format.<br/> <br/> Take a printout and paste it in your kitchen. This way you can ensure that you will stick to the recommended plan.",
            "popImage": "./assets/img/dietplan-story-3.jpg"
        }, {
            "image": "./assets/img/free-plan/to-dos-follow.svg",
            "title": "To-Dos to follow daily habits",
            "subtitle": "Transform Your Lifestyle",
            "bgcolor": "#51c877",
            "popupTitle": "Personalised TO DOs for you",
            "desc": "Based on you lifestyle disorders and your app usage we give you personalised TO DOs <br/> <br/>Follow the TO DOs to gain most from this app and have maximum positive impact on your health.",
            "popImage": "./assets/img/free-plan/to-dos-follow.svg"
        }, {
            "image": "./assets/img/free-plan/app-intro-call.svg",
            "title": "An app intro call from dietitian",
            "subtitle": "Schedule as per convenience",
            "bgcolor": "#51c877",
            "popupTitle": "Schedule as per convenience",
            "desc": "Being a premium member you can request for an Intro call where in our Dietician will explain how to use the app and will also explain you your diet plan",
            "popImage": "./assets/img/free-plan/app-intro-call.svg"
        }
    ],
    diet_plans: [
        {
            "subDesc": "Live healthy ",
            "isVisible": true,
            "name": "Weight Loss + Diabetes",
            "isActivated": false,
            "note": "The content of this app is provided as an information source and is not intended as a substitute for professional medical advice.",
            "id": "diabetes",
            "img": "../../assets/img/diabetes.jpeg",
            "desc": "Switch to Diabetes plan.",
            "subDescLine2": "without sacrificing taste"
        },
        {
            "subDesc": "Inspired from ",
            "isActivated": false,
            "note": "The content of this app is provided as an information source and is not intended as a substitute for professional medical advice.",
            "name": "Weight Loss + Hypertension",
            "subDescLine2": "DASH diet",
            "img": "../../assets/img/hypertension.jpg",
            "isVisible": true,
            "desc": "Switch to Hypertension plan.",
            "id": "hypertension"
        },
        {
            "desc": "Switch to Cholesterol plan.",
            "note": "The content of this app is provided as an information source and is not intended as a substitute for professional medical advice.",
            "isVisible": true,
            "isActivated": false,
            "id": "cholesterol",
            "subDesc": "Align diet ",
            "img": "../../assets/img/cholesterol-new.jpg",
            "subDescLine2": "with good fat",
            "name": "Weight Loss + Cholesterol"
        },
        {
            "id": "immunity_booster",
            "name": "Weight Loss + Immunity",
            "desc": "Switch to Preventive Immunity booster plan.",
            "note": "The content of this app is provided as an information source and is not intended as a substitute for professional medical advice.",
            "subDesc": "Strenghten immunity with Nutrition",
            "isActivated": false,
            "img": "../../assets/img/immunity.jpeg",
            "isVisible": true
        },
        {
            "isActivated": false,
            "img": "../../assets/img/post_covid.jpg",
            "subDesc": "Nutrition plays vital role in healing.",
            "note": "The content of this app is provided as an information source and is not intended as a substitute for professional medical advice.",
            "isVisible": true,
            "name": "Post Covid",
            "id": "post_covid",
            "desc": "Switch to Post Covid plan."
        },
        {
            "img": "../../assets/img/weight-loss.jpg",
            "subDesc": "Diet contributes 80% for weight loss",
            "desc": "Switch to Weight Loss plan.",
            "note": "The content of this app is provided as an information source and is not intended as a substitute for professional medical advice.",
            "isVisible": true,
            "isActivated": false,
            "name": "Weight Loss",
            "id": "weightLoss"
        },
        {
            "subDesc": "Accelerate the weight loss.",
            "img": "../../assets/img/fast-weight-loss.jpg",
            "desc": "Switch to Weight Loss Fast-Track",
            "isVisible": true,
            "isActivated": false,
            "name": "Fast-track Weight Loss",
            "id": "weightLossPlus",
            "note": "The content of this app is provided as an information source and is not a substitute for professional advice."
        },
        {
            "subDescLine2": " medicine for PCOS",
            "isActivated": true,
            "desc": "Switch to PCOS Plan",
            "name": "PCOS",
            "img": "../../assets/img/pcos.jpg",
            "isVisible": true,
            "note": "The content of this app is provided as an information source and is not intended as a substitute for professional medical advice.",
            "subDesc": "Food is the best",
            "id": "pcos"
        },
        {
            "subDescLine2": "",
            "isActivated": true,
            "isGenderCheck": true,
            "desc": "Maintain Muscle & Shred Fat",
            "name": "Tone Up",
            "img": "../../assets/img/WR2.svg",
            "isVisible": true,
            "note": "The content of this app is provided as an information source and is not intended as a substitute for professional medical advice.",
            "subDesc": "Maintain Muscle & Shred Fat",
            "id": "fatShredding_morning"
        },
        {
            "subDescLine2": "",
            "isActivated": true,
            "isGenderCheck": true,
            "desc": "Pump Up & Get Stronger",
            "name": "Muscle Building",
            "img": "../../assets/img/WR3.svg",
            "isVisible": true,
            "note": "The content of this app is provided as an information source and is not intended as a substitute for professional medical advice.",
            "subDesc": "Pump Up & Get Stronger",
            "id": "muscleGain_morning"
        }
    ],
    wellnessData: [
        {
            title: "Ragi Flour",
            subtitle: "",
            image: CONSTANTS.wellness_image_URL + "ragi_flour.png",
            quantity: "500 Gms"
        }, {
            title: "Himalyan Pink Salt",
            subtitle: "",
            image: CONSTANTS.wellness_image_URL + "himalyan_pink_Salt.png",
            quantity: "500 Gms"
        }, {
            title: "Amarnath flour",
            subtitle: "",
            image: CONSTANTS.wellness_image_URL + "amarnath_flour.png",
            quantity: "500 Gms"
        }, {
            title: "Apple Cider Vinegar",
            subtitle: "",
            image: CONSTANTS.wellness_image_URL + "apple_cider_vinegar.png",
            quantity: "500 ML"
        }, {
            title: "Ultra low Carb flour",
            subtitle: "",
            image: CONSTANTS.wellness_image_URL + "ultra_low_carb_flour.png",
            quantity: "1000 Gms"
        }, {
            title: "Seeds trail mix",
            subtitle: "",
            image: CONSTANTS.wellness_image_URL + "seed_trails_mix.png",
            quantity: "250 Gms"
        }, {
            title: "Red rice",
            subtitle: "",
            image: CONSTANTS.wellness_image_URL + "red_rice.png",
            quantity: "500 Gms"
        }
    ],
    stateData: [
        {
            "id": 1,
            "state": "Rajasthan"
        },
        {
            "id": 2,
            "state": "Uttar Pradesh"
        },
        {
            "id": 3,
            "state": "Tamil Nadu"
        },
        {
            "id": 4,
            "state": "Assam"
        },
        {
            "id": 5,
            "state": "West Bengal"
        },
        {
            "id": 6,
            "state": "Madhya Pradesh"
        },
        {
            "id": 7,
            "state": "Uttarakhand"
        },
        {
            "id": 8,
            "state": "Maharashtra"
        },
        {
            "id": 9,
            "state": "Delhi"
        },
        {
            "id": 10,
            "state": "Odisha"
        },
        {
            "id": 11,
            "state": "Andhra Pradesh"
        },
        {
            "id": 12,
            "state": "Karnataka"
        },
        {
            "id": 13,
            "state": "Gujarat"
        },
        {
            "id": 14,
            "state": "Bihar"
        },
        {
            "id": 15,
            "state": "Chattisgarh"
        },
        {
            "id": 16,
            "state": "Telangana"
        },
        {
            "id": 17,
            "state": "Haryana"
        },
        {
            "id": 18,
            "state": "Goa"
        },
        {
            "id": 19,
            "state": "Jharkhand"
        },
        {
            "id": 20,
            "state": "Arunachal Pradesh"
        },
        {
            "id": 21,
            "state": "Tripura"
        },
        {
            "id": 22,
            "state": "Punjab"
        },
        {
            "id": 23,
            "state": "Kerala"
        },
        {
            "id": 24,
            "state": "Andaman And Nico.in."
        },
        {
            "id": 25,
            "state": "Jammu And Kashmir"
        },
        {
            "id": 26,
            "state": "Pondicherry"
        },
        {
            "id": 27,
            "state": "Megalaya"
        },
        {
            "id": 28,
            "state": "Himachal Pradesh"
        },
        {
            "id": 29,
            "state": "Chandigarh"
        },
        {
            "id": 30,
            "state": "Lakshadweep"
        },
        {
            "id": 31,
            "state": "Nagaland"
        },
        {
            "id": 32,
            "state": "Manipur"
        },
        {
            "id": 33,
            "state": "Sikkim"
        },
        {
            "id": 34,
            "state": "Mizoram"
        },
        {
            "id": 35,
            "state": "Dadra And Nagar Hav."
        },
        {
            "id": 36,
            "state": "Daman And Diu"
        }
    ]
};


/***/ }),

/***/ 53533:
/*!*******************************************!*\
  !*** ./src/app/core/utility/utilities.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UTILITIES": () => (/* binding */ UTILITIES)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ 78713);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! moment */ 29243);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _constants_constants__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../constants/constants */ 92133);







let UTILITIES = class UTILITIES {
    constructor(toastController, alertController, storage, loadingController, platform) {
        this.toastController = toastController;
        this.alertController = alertController;
        this.storage = storage;
        this.loadingController = loadingController;
        this.platform = platform;
        this.isLoading = true;
        this.months = [
            "JAN",
            "FEB",
            "MAR",
            "APR",
            "MAY",
            "JUN",
            "JUL",
            "AUG",
            "SEP",
            "OCT",
            "NOV",
            "DEC"
        ];
    }
    changeTime(time) {
        let times = time.split(':');
        return (parseInt(times[0]) + 12) + ':' + times[1];
    }
    objectLength(obj) {
        let size = 0;
        for (let key in obj) {
            if (obj.hasOwnProperty(key) && obj[key] != null) {
                size++;
            }
        }
        return size;
    }
    GetFormattedDate(date) {
        var newDate = new Date(date);
        var formattedDate = new Date().getDate() +
            "-" +
            this.months[newDate.getMonth()] +
            "-" +
            newDate.getFullYear();
        return formattedDate;
    }
    GetFormattedDateFromMonthName(date) {
        let arr = date.split('-');
        let i = 1;
        for (i; i <= this.months.length; i++) {
            if (this.months[i] != undefined) {
                if (this.months[i].toUpperCase() == arr[1].toUpperCase()) {
                    break;
                }
            }
        }
        var formatddate = (i + 1) + '/' + arr[0] + '/' + arr[2];
        return new Date(formatddate);
    }
    presentLoading() {
        return this.loadingController.create({
            cssClass: "my-custom-class",
            showBackdrop: false,
            spinner: null,
            message: `<ion-spinner name="bubbles"></ion-spinner>`,
        }).then(a => {
            this.isLoading = true;
            a.present().then(() => {
                setTimeout(() => {
                    a.dismiss();
                }, 5000);
            });
        });
    }
    showLoading() {
        return this.loadingController.create({
            cssClass: "my-custom-class",
            showBackdrop: false,
            spinner: null,
            message: `<ion-spinner name="bubbles"></ion-spinner>`,
        }).then(a => {
            this.isLoading = true;
            a.present();
        });
    }
    hideLoader() {
        if (this.isLoading == true) {
            this.isLoading = false;
            this.loadingController.dismiss().then(() => console.log('dismissed'));
        }
    }
    showLdr() {
        return this.loadingController.create({
            cssClass: "my-custom-class",
            showBackdrop: false,
            spinner: null,
            message: `<ion-spinner name="bubbles"></ion-spinner>`,
        }).then(a => {
            this.isLoading = true;
            a.present();
        });
    }
    hideLdr() {
        if (this.isLoading == true) {
            this.isLoading = false;
            this.loadingController.dismiss().then(() => console.log('dismissed'));
        }
    }
    presentAlert(mesage) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: "Alert-class",
                header: "Alert",
                // subHeader: 'Subtitle',
                message: mesage,
                buttons: ["OK"]
            });
            yield alert.present();
        });
    }
    presentAlertNoInternet(mesage, cb) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: "Alert-class",
                header: "Alert",
                // subHeader: 'Subtitle',
                message: mesage,
                buttons: [{
                        text: "Ok",
                        handler: blah => {
                            cb();
                        }
                    }]
            });
            yield alert.present();
        });
    }
    presentAlertMultipleButtons() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: "my-custom-class",
                header: "Alert",
                subHeader: "Subtitle",
                message: "This is an alert message.",
                buttons: ["Cancel", "Open Modal", "Delete"]
            });
            yield alert.present();
        });
    }
    presentAlertConfirm(message) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: "my-custom-class",
                header: "Confirm!",
                message: message,
                buttons: [
                    {
                        text: "Close",
                        role: "cancel",
                        cssClass: "secondary",
                        handler: blah => {
                            console.log("Confirm Cancel: blah");
                        }
                    },
                    {
                        text: "Continue",
                        handler: () => {
                            console.log("Confirm Okay");
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    customSort(items) {
        items.sort((a, b) => {
            return (b.option - a.option);
        });
        console.log("items", items);
        return items;
    }
    parseJSON(stringData) {
        if (!stringData) {
            return;
        }
        return JSON.parse(stringData);
    }
    parseString(stringData) {
        return JSON.stringify(stringData);
    }
    presentToast(message, duration, cssClass) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: message,
                duration: duration ? duration : 3000,
                position: "top",
                cssClass: cssClass ? cssClass : "iontoast-danger"
            });
            toast.present();
        });
    }
    setFalseSingleData(data) {
        for (var i = 0; i < data.length; i++) {
            data[i].isSelected = false;
        }
        return data;
    }
    getSelectedData(data) {
        return data.filter((ele) => {
            return ele.isSelected;
        });
    }
    filterValueFromSingleData(data) {
        const dataConst = data.filter(item => {
            return item.isSelected == true;
        });
        if (dataConst.length > 0) {
            return dataConst[0].value;
        }
        else {
            return "Office";
        }
    }
    filterValueForOption(data) {
        const option = data.filter(item => {
            return item.isSelected == true;
        });
        if (option.length <= 0) {
            return "";
        }
        else {
            return option[0].code;
        }
    }
    addDays(days) {
        const copy = new Date();
        copy.setDate(new Date().getDate() + days);
        return copy;
    }
    getMonth(days) {
        const date = new Date();
        const newDate = this.addDays(days);
        if (newDate.getFullYear() == new Date().getFullYear()) {
            return this.months[newDate.getMonth() + 1];
        }
        else {
            return this.months[newDate.getMonth() + 1] + "~" + newDate.getFullYear();
        }
    }
    setFalseDoubleData(data) {
        for (var i = 0; i < data.length; i++) {
            data[i].isSelected = false;
        }
        return data;
    }
    getTrueanySelected(data) {
        if (data.filter(item => {
            return item.isSelected == true;
        }).length > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    getTrueOfAnyTwo(data) {
        if (data.filter(item => {
            return item.isSelected == true;
        }).length > 0) {
            return true;
        }
        else {
            return false;
        }
    }
    getTrueOfAnyTwoMultiSelection(data) {
        if (data.filter(item => {
            return item.isSelected == true;
        }).length > 0) {
            return data.filter(item => {
                return item.isSelected == true;
            }).length;
        }
        else {
            return 0;
        }
    }
    filterValue(data, type) {
        let filteredData = [];
        for (let index = 0; index < data.length; index++) {
            for (let j = 0; j < type.length; j++) {
                if (data[index].Type.toString().trim() == type[j]) {
                    filteredData.push(data[index]);
                }
            }
        }
        return filteredData;
    }
    filterSelectedValue(data, type) {
        let filteredData = [];
        for (let index = 0; index < data.length; index++) {
            for (let j = 0; j < type.length; j++) {
                if (data[index].Type.toString().trim() == type[j] &&
                    data[index].isSelected == true) {
                    filteredData.push(data[index].code);
                }
            }
        }
        return filteredData;
    }
    getTrueOfAnyOneMultiSelection(data) {
        if (data.filter(item => {
            return item.isSelected == true;
        }).length > 0) {
            return data.filter(item => {
                return item.isSelected == true;
            }).length;
        }
        else {
            return 0;
        }
    }
    convertTimeFormat(data) {
        let time = data.split(":");
        let hour = time[0].split("T");
        if (parseInt(hour[1]) > 12) {
            return parseInt(hour[1]) - 12 + ":" + time[1] + " PM";
        }
        else if (parseInt(hour[1]) == 12) {
            return parseInt(hour[1]) + ":" + time[1] + " PM";
        }
        else if (parseInt(hour[1]) == 0) {
            return "12:" + time[1] + " AM";
        }
        else {
            return parseInt(hour[1]) + ":" + time[1] + " AM";
        }
    }
    getArrayifSelelctedSingle(data) {
        return data.filter(item => {
            return item.isSelected == true;
        });
    }
    getArrayIfSelectedFirstInDouble(data) {
        return data.filter(item => {
            return item.isSelected == true;
        });
    }
    getArrayIfSelectedSecondInDouble(data) {
        return data.filter(item => {
            return item.isSelected == true;
        });
    }
    getDemographicRequest(data) {
        const gender = this.getArrayifSelelctedSingle(data.gender);
        const ageData = this.getArrayifSelelctedSingle(data.ageMaster.data);
        let reqBody = {
            gender: {
                code: gender[0].code,
                gender: gender[0].value
            },
            height: {
                unit: data.height[0].param,
                value: parseFloat(data.height[0].value),
                ischecked: true
            },
            weight: {
                unit: data.weight[0].param,
                value: parseFloat(data.weight[0].value),
                ischecked: true
            },
            age: {
                code: ageData[0].code,
                label: ageData[0].value,
                avg_age: Math.ceil(ageData[0].avg_age)
            }
        };
        console.log("demographicdata", reqBody);
        return reqBody;
    }
    generateMultiSelectionDouble(data) {
        let arrData = [];
        for (let index = 0; index < data.length; index++) {
            if (data[index].isSelected == true) {
                arrData.push(data[index].code);
            }
        }
        return arrData;
    }
    generateMultiSelectionDoubleForTime(data) {
        console.log("request", data);
        let arrData = [];
        for (let index = 0; index < data.length; index++) {
            if (data[index].isSelected == true) {
                if (data[index].value == "Other") {
                    arrData.push({
                        code: data[index].code,
                        value: data[index].otherValue
                    });
                }
                else {
                    arrData.push({
                        code: data[index].code
                    });
                }
                break;
            }
        }
        console.log("arrData", arrData);
        return arrData;
    }
    getArrayifSelelctedSingleForTime(data) {
        return data.filter(item => {
            return item.isSelected == true;
        });
    }
    getTypeAndPrefWorkOut(data) {
        const types = this.getArrayifSelelctedSingle(data.type);
        let reqBody = {
            dietPlanType: types && types.length ? types[0].name : ''
        };
        return reqBody;
    }
    getLifeStyleRequest(data) {
        //  const single = this.getArrayifSelelctedSingle(data.single);
        // const marital = this.getArrayifSelelctedSingle(data.marital);
        // const stress = this.getArrayifSelelctedSingle(data.stress);
        const activity = this.getArrayifSelelctedSingle(data.activities);
        // const sleepType = this.getArrayifSelelctedSingle(data.sleepType);
        const community = this.getArrayifSelelctedSingle(data.community);
        const foodPref = this.getArrayifSelelctedSingle(data.foodPref);
        const workOutTime = this.getArrayifSelelctedSingle(data.workoutTime);
        // const alchohal = this.getArrayifSelelctedSingle(data.alcohol);
        // const waterDrink = this.getArrayifSelelctedSingle(data.waterDrink);
        // const ender = this.getArrayifSelelctedSingle(data.single);
        const leaveForOffice = this.getArrayifSelelctedSingleForTime(data.leaveForOffice);
        // const comeBack = this.getArrayifSelelctedSingleForTime(data.comeBack);
        const healthsData = this.generateMultiSelectionDouble(data.diseases);
        const communityArray = this.generateMultiSelectionDouble(data.community);
        communityArray.push("U");
        const wakeup = this.generateMultiSelectionDoubleForTime(data.wakeup.data);
        // const sleep = this.generateMultiSelectionDoubleForTime(data.sleep.data);
        let reqBody = {
            //   ender: single[0].code,
            diseases: healthsData,
            //    marital: marital[0].code,
            //    stress: stress[0].code,
            activities: {
                code: activity[0].code,
                data: activity[0].data
            },
            wakeup: {
                code: wakeup[0].code,
                value: wakeup[0].value
            },
            // sleep: {
            //   code: sleep[0].code,
            //   value: sleep[0].value
            // },
            //   sleepType: sleepType[0].code,
            leaveForOffice: {
                code: leaveForOffice[0].code,
                value: leaveForOffice[0].value,
                otherValue: leaveForOffice[0].otherValue == undefined ? "" : leaveForOffice[0].otherValue
            },
            // comeBack: {
            //   code:comeBack[0].code,
            //   value:comeBack[0].value,
            //   otherValue: comeBack[0].otherValue == undefined ? "" : comeBack[0].otherValue
            // },
            prefWorkOutTime: workOutTime && workOutTime.length ? workOutTime[0].name : '',
            communities: communityArray,
            foodType: foodPref[0].code,
            country: data.country
            //    alchohal: alchohal[0].code,
            //    waterDrink: waterDrink[0].code
        };
        console.log("lifeStyleRequest", reqBody);
        return reqBody;
    }
    getMultipleSingle(data) {
        let arr = [];
        for (let index = 0; index < data.length; index++) {
            if (data[index].isSelected == true) {
                arr.push(data[index].code);
            }
        }
        return arr;
    }
    getDietRequest(data, countryId) {
        let food, drinks, snacks, fruits, dishes, pules, rice, reqBody, beverages, meals;
        if (countryId && countryId == "IND") {
            food = this.getMultipleSingle(data.otherMaster.food.data);
            drinks = this.generateMultiSelectionDouble(data.Master.drinks);
            snacks = this.generateMultiSelectionDouble(data.Master.snacks);
            fruits = this.generateMultiSelectionDouble(data.Master.fruits);
            dishes = this.generateMultiSelectionDouble(data.Master.dishes);
            pules = this.generateMultiSelectionDouble(data.Master.plscurries);
            rice = this.generateMultiSelectionDouble(data.Master.rice);
            reqBody = {
                food: food,
                drinks: drinks,
                snacks: snacks,
                fruits: fruits,
                dishes: dishes,
                pules: pules,
                rice: rice
            };
        }
        else {
            food = this.getMultipleSingle(data.otherMaster.food.data);
            beverages = this.generateMultiSelectionDouble(data.Master.beverages);
            snacks = this.generateMultiSelectionDouble(data.Master.snacks);
            meals = this.generateMultiSelectionDouble(data.Master.meals);
            reqBody = {
                food: food,
                beverages: beverages,
                snacks: snacks,
                meals: meals,
            };
        }
        console.log("DietRequest", reqBody);
        return reqBody;
    }
    updateDemographicData(data, filterData, isMulti) {
        for (let index = 0; index < data.length; index++) {
            if (data[index].code == filterData.code) {
                data[index].isSelected = true;
                break;
            }
        }
        return data;
    }
    updateLifeStyleDataForSingleValue(data, filterData, isMulti) {
        for (let index = 0; index < data.length; index++) {
            if (data[index].code == filterData) {
                data[index].isSelected = true;
                break;
            }
        }
        return data;
    }
    updateLifeStyleData(data, filterData, isMulti) {
        for (let index = 0; index < data.length; index++) {
            if (isMulti == false) {
                if (data[index].code == filterData.code) {
                    data[index].isSelected = true;
                    break;
                }
            }
            else {
                if (data[index].code1 == filterData.code1) {
                    data[index].isSelected1 = true;
                    break;
                }
                else if (data[index].code2 == filterData.code2) {
                    data[index].isSelected2 = true;
                    break;
                }
            }
        }
        return data;
    }
    updateDataMultiSeletion(data, filterData, isMulti) {
        if (data != undefined && filterData != undefined) {
            for (let index = 0; index < data.length; index++) {
                for (let j = 0; j < filterData.length; j++) {
                    if (data[index].code == filterData[j]) {
                        data[index].isSelected = true;
                    }
                }
            }
        }
        return data;
    }
    formatDate(date) {
        let todays = date;
        let dd = todays.getDate();
        let mm = todays.getMonth() + 1;
        let yyyy = todays.getFullYear();
        let days = "";
        let months = "";
        let fullDate = "";
        if (dd < 10) {
            days = "0" + dd.toString();
        }
        if (mm < 10) {
            months = "0" + mm.toString();
        }
        fullDate = dd + "" + mm + "" + yyyy;
        return fullDate;
    }
    updateSuggestdCalories(filterData, isMulti) {
        let calories = {
            calories: 0,
            carb: 0,
            fat: 0,
            fiber: 0,
            protien: 0
        };
        calories.calories = filterData.calories;
        calories.carb = filterData.carb;
        calories.fat = filterData.fat;
        calories.fiber = filterData.fiber;
        calories.protien = filterData.protien;
        return calories;
    }
    updateLifeStyleDataForTime(data, filterData, isMulti) {
        for (let index = 0; index < data.length; index++) {
            if (data[index].code == filterData.code) {
                data[index].isSelected = true;
                if (data[index].value == "Other") {
                    data[index].otherValue = filterData.value;
                }
                break;
            }
        }
        console.log("datawakeup:-", filterData);
        return data;
    }
    isDeviceiOS() {
        if (this.platform.is('cordova') && this.platform.is('ios')) {
            return true;
        }
        else {
            return false;
        }
    }
    isDeviceAndroid() {
        if (this.platform.is('cordova') && this.platform.is('android')) {
            return true;
        }
        else {
            return false;
        }
    }
    // updateLifeStyleDataForTimeSingle(data, filterData, isMulti) {
    //   for (let index = 0; index < data.length; index++) {
    //     if (data[index].code == filterData.code) {
    //       data[index].isSelected = true;
    //       if (data[index].value == "Other") {
    //         data[index].otherValue = filterData.value;
    //       }
    //       break;
    //     }
    //   }
    //   console.log("datawakeup:-", filterData);
    //   return data;
    // }
    // updateLocalDataDiet(localData, updatedData) {}
    getCaloriesBurned(profile, diets, steps, numDays, startHour, endHour) {
        if (numDays == 0 && startHour == 0 && endHour == 0) {
            return 0;
        }
        let height = profile.demographic.height.value;
        if (profile.demographic.height.unit == 'in') {
            height = profile.demographic.height.value * 2.54;
        }
        let weight = profile.demographic.weight.value;
        if (profile.demographic.weight.unit != 'kg') {
            weight = profile.demographic.height.value * 0.45;
        }
        let gender = profile.demographic.gender.gender;
        let age = profile.demographic.age.avg_age;
        let caloriesAtRest = gender == 'Male' ?
            (10 * weight) + (6.25 * height) - (5 * age) + 5 :
            (10 * weight) + (6.25 * height) - (5 * age) + 161;
        let dayTime = caloriesAtRest * 1.025 / (24 * 3600); //per second
        let nightTime = caloriesAtRest * 0.95 / (24 * 3600); // per second
        let caloriesForEachStep = 0.04;
        let sleepTime = 24;
        let wakeUp = "7:00";
        if (diets && diets.length > 0) {
            wakeUp = diets.find(o => o.slot == 0).time;
        }
        let wakeupTime = moment__WEBPACK_IMPORTED_MODULE_0___default()(wakeUp, "HH:mm");
        let wakeupTime1 = moment__WEBPACK_IMPORTED_MODULE_0___default()(wakeupTime).hour() + (moment__WEBPACK_IMPORTED_MODULE_0___default()(wakeupTime).minute() / 60);
        let dayHours = sleepTime - wakeupTime1;
        let nightHours = 24 - dayHours;
        let totalBurned = 0;
        if (numDays) {
            totalBurned = totalBurned + (steps * caloriesForEachStep) + (numDays * (dayHours * dayTime * 60 * 60) + (nightHours * (nightTime * 60 * 60)));
        }
        if (endHour) {
            if (startHour < wakeupTime1 && endHour < nightHours) {
                let hours = endHour - startHour; // night hours
                totalBurned = totalBurned + (hours * (nightTime * 60 * 60));
            }
            else if (startHour > wakeupTime1 && endHour > wakeupTime1 && endHour < sleepTime) {
                let hours = endHour - startHour; // day hours
                totalBurned = totalBurned + (hours * (dayTime * 60 * 60));
            }
            else if (startHour < wakeupTime1 && endHour > wakeupTime1 && endHour < sleepTime) {
                let night = wakeupTime1 - startHour; // night hours
                let day = endHour - wakeupTime1; // day hours        
                totalBurned = totalBurned + (night * (nightTime * 60 * 60)) +
                    (day * (dayTime * 60 * 60));
            }
            else {
                let hours = endHour - startHour; // day hours                
                totalBurned = totalBurned + (hours * (dayTime * 60 * 60));
            }
            totalBurned = totalBurned + (steps * caloriesForEachStep);
        }
        return totalBurned;
    }
    isPlanExpired() {
        let profile = _constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.profile;
        if (!_constants_constants__WEBPACK_IMPORTED_MODULE_1__.CONSTANTS.isPlanActiveParent) {
            if (profile.planType == 'trialEnd' || profile.planType == 'premiumEnd') {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
};
UTILITIES.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.ToastController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.AlertController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__.Storage },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.LoadingController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.Platform }
];
UTILITIES = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: "root"
    })
], UTILITIES);



/***/ }),

/***/ 7217:
/*!*************************************************************!*\
  !*** ./src/app/modal/summary-update/summary-update.page.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SummaryUpdatePage": () => (/* binding */ SummaryUpdatePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_summary_update_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./summary-update.page.html */ 32220);
/* harmony import */ var _summary_update_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./summary-update.page.scss */ 20711);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services */ 65190);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! moment */ 29243);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_3__);








let SummaryUpdatePage = class SummaryUpdatePage {
    constructor(modalController, appService, router, loaderService) {
        this.modalController = modalController;
        this.appService = appService;
        this.router = router;
        this.loaderService = loaderService;
        this.remarkText = '';
        this.updateRemarkValid = false;
    }
    ngOnInit() {
        console.log("leadData", this.leadData.leadInfo);
    }
    ionViewWillEnter() {
        this.leadType = this.leadData.leadInfo.leadType;
        this.leadStatus = this.leadData.leadInfo.status;
    }
    checkRemarkTextLength(value) {
        if (value.length <= 25) {
            //min length found. 
            this.updateRemarkValid = false;
        }
        else {
            this.updateRemarkValid = true;
        }
    }
    closeModal() {
        this.modalController.dismiss();
    }
    leadStatusChange(data) {
        console.log('leadStatusChange');
        this.leadStatus = data.target.value;
    }
    leadTypeChange(leadType) {
        console.log('leadTypeChange');
        this.leadType = leadType.target.value;
    }
    updateSummary() {
        console.log("leadData", this.leadType);
        console.log("leadData", this.leadStatus);
        console.log("leadData", this.remarkText);
        let obj = {
            "dateOfCall": moment__WEBPACK_IMPORTED_MODULE_3___default()().format("YYYY-MM-DD"),
            "leadType": this.leadType,
            "option": this.leadData['leadInfo']['option'],
            "remarks": [
                this.remarkText
            ],
            "status": this.leadStatus ? this.leadStatus : this.leadData['leadInfo']['status'],
            "subOption": this.leadData['leadInfo']["subOption"],
            "timePreference": this.leadData['leadInfo']['timePreference']
        };
        this.loaderService.presentLoader("Please Wait").then((_) => {
            this.appService.updateUser(this.leadData["customerInfo"]["_id"], obj).subscribe((response) => {
                if (response) {
                    // this.storageService.set('acess_token',response.access_token).then(() =>{
                    // call back 
                    this.loaderService.hideLoader();
                    this.modalController.dismiss({ searchQueryType: this.searchQueryType, searchQueryCode: this.searchQueryCode });
                    // this.router.navigate(['/tabs/home'])
                    // })
                }
                (error) => {
                    this.loaderService.hideLoader();
                };
            });
        });
    }
};
SummaryUpdatePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ModalController },
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.LoaderService }
];
SummaryUpdatePage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-summary-update',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_summary_update_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_summary_update_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], SummaryUpdatePage);



/***/ }),

/***/ 98522:
/*!******************************************************************!*\
  !*** ./src/app/select-plan-popup/select-plan-popup.component.ts ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectPlanPopupComponent": () => (/* binding */ SelectPlanPopupComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_select_plan_popup_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./select-plan-popup.component.html */ 4989);
/* harmony import */ var _select_plan_popup_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./select-plan-popup.component.scss */ 49930);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ 38198);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/utility/utilities */ 53533);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ 78713);








let SelectPlanPopupComponent = class SelectPlanPopupComponent {
    constructor(modalController, appService, utilities, storage) {
        this.modalController = modalController;
        this.appService = appService;
        this.utilities = utilities;
        this.storage = storage;
    }
    ngOnInit() {
        console.log("Data ", this.planType);
    }
    close() {
        let data = {};
        this.modalController.dismiss(data);
    }
    doActivate(flag, planId) {
        let data = {
            isActivate: true,
            planId: planId
        };
        this.modalController.dismiss(data);
    }
};
SelectPlanPopupComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.ModalController },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_3__.UTILITIES },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__.Storage }
];
SelectPlanPopupComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-select-plan-popup',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_select_plan_popup_component_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_select_plan_popup_component_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], SelectPlanPopupComponent);



/***/ }),

/***/ 61535:
/*!*********************************************!*\
  !*** ./src/app/services/app/app.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppService": () => (/* binding */ AppService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 83981);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../shared/constants/constants */ 66239);
/* harmony import */ var _http_http_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../http/http.service */ 72641);





let AppService = class AppService {
    constructor(httpService, http) {
        this.httpService = httpService;
        this.http = http;
    }
    signIn(data) {
        return this.httpService.post(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.login, data);
    }
    refs() {
        return this.httpService.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.refs);
    }
    leadSummary() {
        return this.httpService.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.summary);
    }
    searchLead(searchQuery) {
        return this.httpService.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.search + searchQuery);
    }
    languagePref(language, userId) {
        return this.httpService.post(`updateProfile?userId=${userId}`, { "languagePref": language });
    }
    readyToRelease(userId, date, isready) {
        console.log("userId,date,isready", userId, date, isready);
        return this.httpService.post(`updateDietPlan?userId=${userId}&date=${date}&releaseReady=${isready}`);
    }
    updateUser(id, data) {
        return this.httpService.put(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.updateUser + "/" + id, data);
    }
    getUserToken(email) {
        return this.httpService.getUser(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.getToken + "email=" + email);
    }
    updateExpDate(data) {
        return this.httpService.post(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.updateExpDate, data);
    }
    updatRefferal(data) {
        return this.httpService.post(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.updateReferral, data);
    }
    fetchDietPlanNames() {
        return this.httpService.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.fetchDietPlanNames);
    }
    fetchSlotsDetails(email, dietname) {
        return this.httpService.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.fetchSlotsDetails + `?email=${email}&dietPlanName=${dietname}`);
    }
    updateSlotDetail(payload, email, dietname) {
        return this.httpService.post(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.updateSlotsDetails + `?email=${email}&selectedDietPlan=${dietname}`, payload);
    }
    updatediets(username, reqBody) {
        return this.httpService.put(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.user + `/${username}`, reqBody);
    }
    fetchAllCategories(payload) {
        return this.httpService.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.fetchAllCategories + `?email=${payload.email}&slot=${payload.slot}&dietPlanName=${payload.dietPlan}`);
    }
    updateAllCategories(payload) {
        return this.httpService.post(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.updateCategory + `?email=${payload.email}&slot=${payload.slot}`, payload.data);
    }
    fetchFoodDetails(payload) {
        return this.httpService.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.fetchFoodDetails + `?email=${payload.email}&category=${payload.cat}&slot=${payload.slot}&dietPlanName=${payload.dietPlan}`);
    }
    updateFoodItems(payload) {
        return this.httpService.post(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.updateFoodItems + `?email=${payload.email}&slot=${payload.slot}&category=${payload.cat}`, payload.data);
    }
    getPreferences(payload) {
        console.log("payload", payload);
        return this.httpService.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.fetchCustomerProfile + `${payload.email}`);
    }
    updateDietPref(payload) {
        return this.httpService.post(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.updateCustomerDetails, payload);
    }
    userAdd(reqBody) {
        return this.httpService.post(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.user, reqBody);
    }
    userSearch(username) {
        return this.httpService.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.user + `/${username}`);
    }
    userUpdate(username, reqBody) {
        return this.httpService.put(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.user + `/${username}`, reqBody);
    }
    userDelete(username) {
        return this.httpService.delete(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.user + `/${username}`);
    }
    getDietPlans() {
        return this.httpService.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.user);
    }
    defaultData() {
        const url = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.SUBAPIS.defaultDetail;
        return this.httpService.getUserDefault(url);
    }
};
AppService.ctorParameters = () => [
    { type: _http_http_service__WEBPACK_IMPORTED_MODULE_1__.HttpService },
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient }
];
AppService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], AppService);



/***/ }),

/***/ 72641:
/*!***********************************************!*\
  !*** ./src/app/services/http/http.service.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BYPASS_LOG": () => (/* binding */ BYPASS_LOG),
/* harmony export */   "HttpService": () => (/* binding */ HttpService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 83981);
/* harmony import */ var _shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../shared/constants/constants */ 66239);
/* harmony import */ var src_app_shared_interceptors_jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/shared/interceptors/jwt.interceptor */ 45821);





const BYPASS_LOG = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpContextToken(() => false);
let HttpService = class HttpService {
    // BYPASS_LOG = new HttpContextToken(() => false);
    constructor(httpClient) {
        this.httpClient = httpClient;
    }
    post(serviceName, data = {}) {
        return this.httpClient.post(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.APIS.baseurl + serviceName, data);
    }
    post4444(serviceName, data = {}) {
        return this.httpClient.post(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.APIS.baseurl4444 + serviceName, data);
    }
    put(serviceName, data = {}) {
        return this.httpClient.put(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.APIS.baseurl + serviceName, data);
    }
    get(serviceName) {
        return this.httpClient.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.APIS.baseurl + serviceName);
    }
    getWithMainUrl(serviceName) {
        return this.httpClient.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.APIS.baseurl4444 + serviceName);
    }
    getUser(serviceName) {
        return this.httpClient.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.APIS.userBaseUrl + serviceName, { context: (0,src_app_shared_interceptors_jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__.byPassLog)() });
    }
    getUserDefault(serviceName) {
        return this.httpClient.get(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.APIS.userBaseUrl + serviceName);
    }
    delete(serviceName) {
        return this.httpClient.delete(_shared_constants_constants__WEBPACK_IMPORTED_MODULE_0__.APIS.userBaseUrl + serviceName, { context: (0,src_app_shared_interceptors_jwt_interceptor__WEBPACK_IMPORTED_MODULE_1__.byPassLog)() });
    }
};
HttpService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient }
];
HttpService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], HttpService);



/***/ }),

/***/ 65190:
/*!***********************************!*\
  !*** ./src/app/services/index.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppService": () => (/* reexport safe */ _app_app_service__WEBPACK_IMPORTED_MODULE_0__.AppService),
/* harmony export */   "BYPASS_LOG": () => (/* reexport safe */ _http_http_service__WEBPACK_IMPORTED_MODULE_1__.BYPASS_LOG),
/* harmony export */   "HttpService": () => (/* reexport safe */ _http_http_service__WEBPACK_IMPORTED_MODULE_1__.HttpService),
/* harmony export */   "StorageService": () => (/* reexport safe */ _storage_storage_service__WEBPACK_IMPORTED_MODULE_2__.StorageService),
/* harmony export */   "LoaderService": () => (/* reexport safe */ _loader_loader_service__WEBPACK_IMPORTED_MODULE_3__.LoaderService)
/* harmony export */ });
/* harmony import */ var _app_app_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.service */ 61535);
/* harmony import */ var _http_http_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./http/http.service */ 72641);
/* harmony import */ var _storage_storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./storage/storage.service */ 3789);
/* harmony import */ var _loader_loader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./loader/loader.service */ 62239);






/***/ }),

/***/ 62239:
/*!***************************************************!*\
  !*** ./src/app/services/loader/loader.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoaderService": () => (/* binding */ LoaderService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ 78099);



let LoaderService = class LoaderService {
    constructor(loadingController) {
        this.loadingController = loadingController;
    }
    presentLoader(title, dur, spin) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            const loading = yield this.loadingController.create({
                message: title,
                translucent: true,
                spinner: spin || "circles" || 0,
                keyboardClose: true,
            });
            yield loading.present();
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            const res = yield this.loadingController.dismiss().then(() => { }, () => { });
            return yield res;
            //  const { role, data } = await loading.onDidDismiss();
            // console.log('Loading dismissed with role:', role);
        });
    }
};
LoaderService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__.LoadingController }
];
LoaderService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root'
    })
], LoaderService);



/***/ }),

/***/ 3789:
/*!*****************************************************!*\
  !*** ./src/app/services/storage/storage.service.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StorageService": () => (/* binding */ StorageService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/storage */ 78713);



let StorageService = class StorageService {
    constructor(storage) {
        this.storage = storage;
        this._storage = null;
        // this.init();
    }
    // async init() {
    //   // If using, define drivers here: await this.storage.defineDriver(/*...*/);
    //   const storage = await this.storage.create();
    //   this._storage = storage;
    // }
    set(key, value) {
        var _a;
        return (_a = this._storage) === null || _a === void 0 ? void 0 : _a.set(key, value);
    }
    get(key) {
        var _a;
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            return (_a = this._storage) === null || _a === void 0 ? void 0 : _a.get(key);
        });
    }
    remove(key) {
        var _a;
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            return yield ((_a = this._storage) === null || _a === void 0 ? void 0 : _a.remove(key));
        });
    }
    clear() {
        var _a;
        (_a = this._storage) === null || _a === void 0 ? void 0 : _a.clear().then(() => {
            console.log("all keys cleared");
        });
    }
};
StorageService.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_1__.Storage }
];
StorageService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: "root",
    })
], StorageService);



/***/ }),

/***/ 66239:
/*!***********************************************!*\
  !*** ./src/app/shared/constants/constants.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "APIS": () => (/* binding */ APIS),
/* harmony export */   "SUBAPIS": () => (/* binding */ SUBAPIS)
/* harmony export */ });
const APIS = {
    baseurl: "https://app.smartdietplanner.com:8444/",
    userBaseUrl: "https://app.smartdietplanner.com/api/",
    mainUrl: "https://app.smartdietplanner.com",
    baseurl4444: "https://app.smartdietplanner.com:4444",
    baseurlApi: "https://app.smartdietplanner.com:4444/api/"
};
const SUBAPIS = {
    login: "login",
    refs: "refs",
    summary: "summary",
    search: "customers?",
    updateUser: "customers",
    getToken: "getToken?",
    updateExpDate: "customer-update/update-premium",
    updateReferral: "customer-update/update-referral",
    fetchDietPlanNames: "custom/dietPlan/fetchDietPlanNames",
    fetchSlotsDetails: "custom/dietPlan/fetchSlotsDetails",
    updateSlotsDetails: "custom/dietPlan/updateSlot",
    fetchAllCategories: "custom/dietPlan/fetchAllCategories",
    updateCategory: "custom/dietPlan/updateCategory",
    updateFoodItems: "custom/dietPlan/updateFoodItems",
    fetchFoodDetails: "custom/dietPlan/fetchFoodDetails",
    user: "user",
    defaultDetail: 'defaultDetail',
    fetchCustomerProfile: 'fetchCustomerProfile?custId=',
    updateCustomerDetails: 'updateCustomerDetails',
    saveDietRecall: "saveDietRecall?userId=",
    getDietRecall: "getDietRecall?userId=",
};


/***/ }),

/***/ 45821:
/*!********************************************************!*\
  !*** ./src/app/shared/interceptors/jwt.interceptor.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "byPassLog": () => (/* binding */ byPassLog),
/* harmony export */   "InterceptorProvider": () => (/* binding */ InterceptorProvider)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ 83981);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! rxjs */ 9500);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ 88377);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ 10592);
/* harmony import */ var _services_storage_storage_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../services/storage/storage.service */ 3789);







const BYPASS_LOG = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpContextToken(() => false);
function byPassLog() {
    return new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpContext().set(BYPASS_LOG, true);
}
let InterceptorProvider = class InterceptorProvider {
    constructor(alertController, storageService) {
        this.alertController = alertController;
        this.storageService = storageService;
    }
    // Intercepts all HTTP requests!
    intercept(request, next) {
        if (request.context.get(BYPASS_LOG) === true)
            return next.handle(request);
        console.log("request", request);
        // localStorage.setItem('personal_token',token);
        let token = "";
        if (request.url.includes('getOnePlan')) {
            token = localStorage.getItem('personal_token');
            console.log("token", token);
        }
        else {
            token = localStorage.getItem('acess_token');
        }
        // const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJuaWtrIiwicm9sZXMiOltdLCJpc3MiOiJodHRwczovL3Rlc3QuZmlnaHRpdGF3YXkuY29tOjg0NDQvbG9naW4iLCJleHAiOjE2NDQ1NzIzMDZ9.-ZJ63t5-_DpAVx-qp4Exne8pp_XITHM8aJaT3Loe0-M"
        //Authentication by setting header with token value
        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                },
            });
        }
        if (!request.headers.has("Content-Type")) {
            request = request.clone({
                setHeaders: {
                    "content-type": "application/json",
                },
            });
        }
        request = request.clone({
            headers: request.headers.set("Accept", "application/json"),
        });
        return next.handle(request).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_2__.map)((event) => {
            if (event instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpResponse) {
                console.log("event--->>>", event);
            }
            return event;
        }), (0,rxjs_operators__WEBPACK_IMPORTED_MODULE_3__.catchError)((error) => {
            console.error(error);
            return (0,rxjs__WEBPACK_IMPORTED_MODULE_4__.throwError)(error);
        }));
    }
};
InterceptorProvider.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.AlertController },
    { type: _services_storage_storage_service__WEBPACK_IMPORTED_MODULE_0__.StorageService }
];
InterceptorProvider = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Injectable)()
], InterceptorProvider);



/***/ }),

/***/ 18260:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 90271:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 42577);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 34750);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 18260);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch(err => console.log(err));


/***/ }),

/***/ 50863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./ion-accordion_2.entry.js": [
		83477,
		"common",
		"node_modules_ionic_core_dist_esm_ion-accordion_2_entry_js"
	],
	"./ion-action-sheet.entry.js": [
		67196,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		38081,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		75017,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		69721,
		"common",
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		99216,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		96612,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-breadcrumb_2.entry.js": [
		42694,
		"common",
		"node_modules_ionic_core_dist_esm_ion-breadcrumb_2_entry_js"
	],
	"./ion-button_2.entry.js": [
		22938,
		"common",
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		51379,
		"common",
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		97552,
		"common",
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		37218,
		"common",
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		97479,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		64134,
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		71439,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		76397,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		33296,
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		12413,
		"common",
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		39411,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		99133,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		79003,
		"common",
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		96065,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		86991,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		82947,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-picker-column-internal.entry.js": [
		25919,
		"common",
		"node_modules_ionic_core_dist_esm_ion-picker-column-internal_entry_js"
	],
	"./ion-picker-internal.entry.js": [
		93109,
		"node_modules_ionic_core_dist_esm_ion-picker-internal_entry_js"
	],
	"./ion-popover.entry.js": [
		99459,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		20301,
		"common",
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		43799,
		"common",
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		12140,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		86197,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		41975,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		58387,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		98659,
		"common",
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		26404,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		85253,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		92619,
		"common",
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		82871,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		17668,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		55342,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		174,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		86185,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		97337,
		"common",
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		4833,
		"common",
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		9468,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		25705,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		87463,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 50863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 75158:
/*!***************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/app.component.html ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-app>\n  <ion-router-outlet></ion-router-outlet>\n</ion-app>\n");

/***/ }),

/***/ 78962:
/*!***********************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/change-plan/change-plan.component.html ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header style=\"height: 4rem;\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/dietplan\"></ion-back-button>\n    </ion-buttons>\n    <ion-title>Diet Plans Options</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"ion-padding-top\">\n\n  <div class=\"personalise-covid text-center\" *ngIf=\"containPlanTypes.length > 0\">\n    <div *ngFor=\"let plan of containPlanTypes;let ing=index;\">\n      <!-- && (plan.id == selectedPlan || (selectedPlan.split('_').length > 0 && plan.id.split('_').length > 0 && selectedPlan.split('_')[0] == plan.id.split('_')[0])) -->\n     <ion-card class=\"plan-container\"\n      *ngIf=\"plan.isVisible\"\n      [ngStyle]=\"{'background-image':' url(' + plan.img + ')', 'margin-top': ing == 0 ? '15px': '', 'background-size' : plan.id == 'diabetes' || plan.id == 'hypertension' ? '200px' : 'contain'}\">\n      <div style=\"border-left: 5px solid;\" [ngStyle]=\"{'border-color': plan.id == 'post_covid' ? '#754B29' :  plan.id =='weightLoss' ? '#01a3a4' : plan.id =='immunity_booster' ? '#FD9F33' : plan.id =='detox' ? '#4CB271' :\n      plan.id =='diabetes' ? '#D14322' : plan.id =='cholesterol' ? '#A31E79' : plan.id =='hypertension' ? '#FF5D56' :  plan.id =='pcos' ? '#FF5A7D':\n      '#0B94C1'}\" class=\"legend-border\"></div>\n\n      <ion-row>\n\n        <ion-col class=\"ion-text-left ion-align-self-center \" style=\"padding: 15px;\">\n          <ion-row>\n            <div class=\"legend\"></div>\n\n            <ion-col>\n              <span class=\"sub-foodItem-header \">{{plan.name}}</span>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"padding-top: 5px;\">\n            <ion-col>\n              <span class=\"sub-foodItem-sub-header\">{{plan.subDesc}}</span>\n              <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header2\" *ngIf=\"plan.subDescLine2\">{{plan.subDescLine2}}</span>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n        <ion-col class=\"ion-text-center ion-align-self-center\">\n        </ion-col>\n      </ion-row>\n\n      <ion-button *ngIf=\"!(plan.id == selectedPlan || (selectedPlan.split('_').length > 0 && plan.id.split('_').length > 0 && selectedPlan.split('_')[0] == plan.id.split('_')[0]))\" shape=\"round\" fill='outline' style=\"--background: #FFF !important\" (click)=\"gotoEditPlanSelection($event, plan.id, plan, plan.note)\"\n        [ngClass]=\"{'immunity-section-border': (plan.id =='immunity_booster'), 'weight-loss-border' : (plan.id\n        =='weightLoss'), 'weight-loss-plus-section-border': (plan.id =='weightLossPlus'),\n        'post-covid-section-border': ( plan.id  =='post_covid'),\n        'diabetes-section-border': (plan.id =='diabetes'),\n        'cholesterol-section-border': (plan.id =='cholesterol'),\n        'hypertension-section-border': (plan.id =='hypertension'),\n        'pcos-section-border': (plan.id =='pcos'),\n        'mucsle-border': (selectedPlan.split('_').length > 0 && plan.id.split('_').length > 0 && plan.id.split('_')[0] == 'muscleGain' ),\n        'fat-border': (selectedPlan.split('_').length > 0 && plan.id.split('_').length > 0 && plan.id.split('_')[0] == 'fatShredding' )\n        }\">\n        {{(plan.id == selectedPlan || (selectedPlan.split('_').length > 0 && plan.id.split('_').length > 0 && selectedPlan.split('_')[0] == plan.id.split('_')[0])) ? 'Activated' : \"Activate\" }}\n      </ion-button>\n   </ion-card>\n    </div>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ 51292:
/*!**************************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/components/add-update-customer/add-update-customer.component.html ***!
  \**************************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>\n      Add/Update User\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n\n \n    <ion-item class=\"main\">\n      <ion-label>Name:</ion-label>\n      <ion-input \n      placeholder=\"Enter  Name\"\n      [(ngModel)]=\"name\"></ion-input>\n    </ion-item>\n    <ion-radio-group value=\"Email\" (ionChange)=\"getValue($event)\"> \n    <ion-item class=\"main\">\n      <ion-label >Registering By:</ion-label>\n     \n      <ion-item >\n        <ion-label>Email</ion-label>\n        <ion-radio  value=\"Email\">Email</ion-radio>\n      </ion-item>\n      \n      <ion-item >\n        <ion-label>Mobile</ion-label>\n        <ion-radio value=\"Mobile\">Mobile</ion-radio>\n      </ion-item>\n       \n    </ion-item>\n    </ion-radio-group>\n    <ion-item *ngIf=\"!isMobile\" class=\"main\">\n      <ion-label>Email:</ion-label>\n      <ion-input placeholder=\"Enter Email\" [(ngModel)]=\"emailDetail\"></ion-input>\n    </ion-item>\n    <ion-item *ngIf=\"isMobile\" class=\"main\">\n      <ion-label>Mobile:</ion-label>\n      <ion-intl-tel-input\n                    id=\"phone-number\"\n                    (focus)=\"onFocus()\"\n                    name=\"phone-number\"\n                    [defaultCountryiso]=\"currentCountryCode\"\n                    [enableAutoCountrySelect]=\"true\"\n                    [(ngModel)]=\"phoneNumber\"\n                    #phoneNumberControl=\"ngModel\"\n                  >\n                  </ion-intl-tel-input>\n    </ion-item>\n    <ion-item class=\"update-btn\">\n      <ion-button\n        (click)=\"getToken()\"\n        shape=\"round\"\n        style=\"    width: 50%;\n        height: 40px;\"\n        class=\"ion-color ion-color-theme button button-small button-round button-outline\"\n        >\n        Submit\n      </ion-button>\n      </ion-item>\n    \n\n</ion-content>\n");

/***/ }),

/***/ 274:
/*!**************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/components/home-dietplan/home-dietplan.component.html ***!
  \**************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<div\n[ngClass]=\"{'heart-beat-animation': !isDetox && (((suggestedCalories.totalCalories*100)/suggestedCalories.recomended) > 110), 'low-heart-beat-animation' : !isDetox && !isAnalysisPageVisited}\">\n\n<circle-progress radius=\"55\"\n  percent=\"{{(suggestedCalories.totalCalories*100)/suggestedCalories.recomended}}\"\n  outerStrokeGradient=\"{{((suggestedCalories.totalCalories*100)/suggestedCalories.recomended) <= 110 && selectedDietPlan == 'weightLoss' ? true : false}}\"\n  [outerStrokeGradientStopColor]=\"isDetox ? suggestedCalories.totalCalories > detoxMaxValue ? 'orange' : detoxThemeColor: \n  !isDetox && ((suggestedCalories.totalCalories*100)/suggestedCalories.recomended) > 110 ? '#df5151' : selectedThemeColor\"\n  [innerStrokeColor]=\"'rgb(125,125,125,0.2)'\"\n  [outerStrokeColor]=\"isDetox ? suggestedCalories.totalCalories > detoxMaxValue ? 'orange' : detoxThemeColor: \n  !isDetox && ((suggestedCalories.totalCalories*100)/suggestedCalories.recomended) > 110 ? '#df5151' : selectedThemeColor\" backgroundColor=\"#fff\"\n  outerStrokeWidth=\"5\" innerStrokeWidth=\"5\" title=\"{{roundingVal(suggestedCalories.totalCalories)}}\"\n  [titleFontSize]=\"22\"\n  titleColor=\"{{isDetox ? suggestedCalories.totalCalories > detoxMaxValue ? 'orange' : detoxThemeColor: \n  !isDetox && ((suggestedCalories.totalCalories*100)/suggestedCalories.recomended) > 110 ? '#df5151' : selectedThemeColor}}\"\n  [titleFontWeight]=\"500\" subtitle=\"Diet Plan\"\n  subtitleColor=\"{{isDetox ? suggestedCalories.totalCalories > detoxMaxValue ? 'orange' : detoxThemeColor:  \n  !isDetox && ((suggestedCalories.totalCalories*100)/suggestedCalories.recomended) > 110 ? '#df5151' : selectedThemeColor}}\" [subtitleFontWeight]=\"400\"\n  [subtitleFontSize]=\"15\" units=\"kcal\" [unitsFontSize]=\"10\" [unitsFontWeight]=\"400\"\n  unitsColor=\"{{isDetox ? suggestedCalories.totalCalories > detoxMaxValue ? 'orange' : detoxThemeColor: \n  !isDetox && ((suggestedCalories.totalCalories*100)/suggestedCalories.recomended) > 110 ? '#df5151' : selectedThemeColor}}\">\n</circle-progress>\n</div>");

/***/ }),

/***/ 65754:
/*!**************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/components/home-kcounter/home-kcounter.component.html ***!
  \**************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- <img class=\"disable-traker-container\" src=\"../../../assets/img/eaten_kcal_main_disabled.svg\" *ngIf=\"currentDateIndex > 0\"> -->\n\n<!-- <circle-progress radius=\"30\" percent=\"{{totalTodaysCaloriesPerc}}\" [innerStrokeColor]=\"'rgb(125,125,125,0.2)'\" \n  outerStrokeGradient=\"true\"\n  [outerStrokeGradientStopColor]=\"totalTodaysCaloriesPerc < 100 ? '#93EAA9' : totalTodaysCaloriesPerc > 100 && totalTodaysCaloriesPerc <= 110 ? '#FCAF54'  : '#df5151'\"\n  [outerStrokeColor]=\"totalTodaysCaloriesPerc < 100 ? '#45AD04' : totalTodaysCaloriesPerc > 100 && totalTodaysCaloriesPerc <= 110 ? '#FCAF54' : '#df5151'\"\n  showImage=\"true\" imageSrc=\"../../../assets/img/eaten_kcal_main.svg\" imageHeight=\"50\" imageWidth=\"50\"\n  backgroundColor=\"#fff\" outerStrokeWidth=\"3\" innerStrokeWidth=\"3\"\n  [title]=\"roundingVal(totalTodaysCalories * 100/tolalCalories)\" [titleColor]=\"'#45AD04'\" space=\"-3\"\n  [subtitle]=\"habitList?.length+'habits'\" [subtitleColor]=\"'#45AD04'\" units=\"%\" [unitsColor]=\"'#45AD04'\">\n</circle-progress> -->\n\n <ion-row style=\"position: relative;\n top: -56px;\">\n  <ion-col class=\"ion-text-center ion-align-self-start\">\n    <span style=\"font-size: .75rem;color:#007881\">\n      Recommended\n    </span><br>\n    <span class=\"font-size75\" style=\"white-space: nowrap;\"><span style=\"color:#007881;font-size: 1.2rem;font-weight: 600;\"> {{totalTodaysCalories}}</span> Kcal\n    </span>\n  </ion-col>\n</ion-row> ");

/***/ }),

/***/ 90848:
/*!**************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/components/home-vertical/home-vertical.component.html ***!
  \**************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-card  *ngFor=\"let diet of diets;let ing=index;\">\n  <ion-card-content>\n    <ion-item class=\"ion-card-header-custom\" style=\"border: 0;--inner-padding-end: 0 !important;\"\n      *ngIf=\"diet.data && diet.data.length > 0\">\n      <ion-row style=\"width: 100%;\"> \n        <ion-col *ngIf=\"diet.slot == '2' ||diet.slot == '4' || diet.slot =='7' || diet.slot == '8'\"\n          size=\"1\">\n          <img (click)=\"editDiets(diet,diets)\" src=\"assets/img/edit-pen.svg\">\n        </ion-col>\n        <ion-col size=\"6\" class=\"ion-align-self-end text-left\"\n          style=\"min-height: 40px;padding-left:5px;\">\n          <span class=\"medium-text food no-wrap\"\n            style=\"font-size: 1rem;color: '#333'; font-weight: 200;\">{{diet.slotName===undefined?diet.message:diet.slotName}} </span>\n          <span [innerHTML]=\"showTime(diet)\" class=\"small-text food\"\n            style=\"display: block; color: #AAA;\"></span>\n        </ion-col>\n        <ion-col\n          [size]=\"(diet.slot == '2' ||diet.slot == '4' || diet.slot =='7' || diet.slot == '8') ? 5 :6 \"\n          class=\"ion-align-self-end text-right\" style=\"min-height: 40px;padding-left:5px;\">\n          <span style=\" font-size: 1rem;color:'#8C8E93'\"\n            class=\"medium-text food no-wrap\">{{roundUpvalue(diet.totalCalories)}} Kcal</span>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <ion-item class=\"ion-card-header-custom no-padding\"  style=\"border-top: 0.5px solid #b3b3b3;\n    margin: 0;\n    padding: 0.5rem 0;\">\n        <hr style=\"border-top:1px; border-color:#b3b3b3;\">\n        <ion-row style=\"width: 100%;\">\n          <ion-col size=\"4\" class=\"ion-text-left\">\n            <ion-label style=\"color:#01A3A4;font-size:.8rem;\">Protein</ion-label>\n   \n            <span style=\"color:#707070;font-size: .8rem;\">{{diet.totalProtien}} Gm</span><br>\n\n            <span style=\"color:#707070;font-size: .8rem;font-weight: 700;\" *ngIf=\"diet.totalProtien!=0\">({{diet.totalCalories==0?0:((diet.totalProtien)*400/(diet.totalCalories)).toFixed(0)}} %)</span>\n            <span style=\"color:#707070;font-size: .8rem;font-weight: 700;\" *ngIf=\"diet.totalProtien==0\">(0%)</span>\n          </ion-col>\n          <ion-col size=\"4\" class=\"ion-text-center\">\n            <ion-label style=\"color:#01A3A4;font-size:.8rem;\">Fat</ion-label>\n         \n            <span style=\"color:#707070;font-size: .8rem;\">{{diet.totalFat}} Gm</span><br>\n            <span style=\"color:#707070;font-size: .8rem;font-weight: 700;\" *ngIf=\"diet.totalFat!=0\">({{diet.totalCalories==0?0:((diet.totalFat)*900/(diet.totalCalories)).toFixed(0)}} %)</span>\n            <span style=\"color:#707070;font-size: .8rem;font-weight: 700;\" *ngIf=\"diet.totalFat==0\">(0%)</span>\n          </ion-col>\n          <ion-col size=\"4\" class=\"ion-text-right\">\n            <ion-label style=\"color:#01A3A4;font-size:.8rem;\">Carbs</ion-label>\n       \n            <span style=\"color:#707070;font-size: .8rem;\">{{diet.totalCarbs}} Gm</span><br>\n            <span style=\"color:#707070;font-size: .8rem;font-weight: 700;\" *ngIf=\"diet.totalCarbs!=0\">({{diet.totalCalories==0?0:((diet.totalCarbs)*400/(diet.totalCalories)).toFixed(0)}} %)</span>\n            <span style=\"color:#707070;font-size: .8rem;font-weight: 700;\" *ngIf=\"diet.totalCarbs==0\">(0%)</span>\n          </ion-col>\n\n\n        </ion-row>\n    </ion-item >\n    <ion-item *ngIf=\"!diet.data || diet.data.length == 0\" class=\"ion-card-header-custom\"\n      style=\"border: 0;\">\n      <ion-row style=\"width: 100%;\">\n        <ion-col class=\"ion-text-center\">\n          <span class=\"medium-text food no-wrap\" style=\"font-size: 1rem;color: #7d7d7d;\">\"Eat Better\n            Not Less\"</span>\n        </ion-col>\n      </ion-row>\n    </ion-item>\n    <ion-row *ngIf=\"diet.data && diet.data.length > 0\">\n      <ion-col size=\"12\" class=\"text-center\" style=\"border-top: 1px solid #eee;\"></ion-col>\n    </ion-row>\n   \n    <ion-list *ngIf=\"diet.data && diet.data.length > 0\">\n\n      <div class=\"custom-height-scroll\">\n        <ion-item class=\"ion-no-padding\" *ngFor=\"let data of diet.data;let xindex=index;\"\n          style=\"--padding-end:15px; --border-color: #EEE\" [lines]=\"xindex==diet.data.length-1?'none':''\">\n         \n          <div slot=\"start\" class=\"legend\"\n            [ngClass]=\"{'legend-9':data.Score =='9', 'legend-6': data.Score =='6', 'legend-3': data.Score =='3' , 'legend-1':data.Score =='1' , 'legend-0': !data.Score || data.Score == '' }\">\n\n          </div>\n          <ion-thumbnail style=\"border: 1px solid #ccc; border-radius: 1rem;\">\n            <img src=\"./assets/img/health1.svg\" class=\"pic-food-heart\"\n              *ngIf=\"selectedDietPlan != 'immunity_booster' && data.recommendedFor && data.recommendedFor.length>0\" />\n            <img src=\"./assets/img/immunity.svg\" class=\"pic-food-heart\"\n              *ngIf=\"selectedDietPlan == 'immunity_booster' && data.Score == '9'\" />\n            <img src=\"./assets/img/health1.svg\" class=\"pic-food-heart\"\n              *ngIf=\"selectedDietPlan == 'immunity_booster' && data.recommendedFor && data.recommendedFor.length> 0 && data.Score != '9'\" />\n            <img src=\"{{data.imageUrl}}\"\n              *ngIf=\"!data.foodSource || data.foodSource.toLowerCase() == 'internal'\" class=\"pic-food\"\n              (click)=\"gotoFoodDetail(data.code,ing)\"  \n              onerror=\"this.src='./assets/newImages/null.jpg';this.style='object-fit: contain'\"\n              [ngClass]=\"{'blur-it':(!isPlanActiveForDiet && slotIndex!=ing)}\"/>\n            <img src=\"./assets/img/food-item.png\" class=\"pic-food\"\n              *ngIf=\"data.foodSource && (data.foodSource.toLowerCase() == 'external' || data.foodSource.toLowerCase() == 'personal')\"\n              (click)=\"gotoFoodDetail(data.code,ing)\" />\n          </ion-thumbnail>\n          <ion-label class=\"medium-text\" style=\"white-space: normal\">\n            <span class=\"food-name-container\">\n              {{data.portion}} {{data.portion_unit}},\n              <a class=\"food-name\" (click)=\"gotoFoodDetail(data.code,ing)\" [ngClass]=\"{'blur-it':(!isPlanActiveForDiet && slotIndex!=ing)}\">{{data.Food}}</a>\n            </span>\n            <ion-row>\n              <ion-col size=\"4\">\n                <span class=\"light-text\" style=\"color: #01A3A4;display: flex;\">{{data.Calories}}\n                  cals</span>\n              </ion-col>\n              <ion-col>\n                <ion-row>\n                  <ion-col>\n                    <div style=\"border:1px solid #b3b3b3;\">\n                    <input type=\"number\" [(ngModel)]=\"data.portion\" (input)=\"updateSlotItems(data.portion,ing,xindex)\" style=\"width: 86%;height: 100%;border:none;text-align: center;\" /> \n                  </div>\n                  </ion-col>\n                  <ion-col (click)=\"refreshFoodItem(data,ing)\">\n                   <ion-icon name=\"refresh\"  class=\"get-remove-icon\"\n                      [ngStyle]=\"{'color': (!isPlanActiveForDiet && (slotIndex!=ing) ||  (data.foodSource && (data.foodSource.toLowerCase() == 'external' || data.foodSource.toLowerCase() == 'personal'))) ? 'grey' : '#01A3A4'}\"\n                      style=\"cursor: pointer;\"></ion-icon> \n                  </ion-col>\n\n                  <ion-col (click)=\"removeFoodItem(data, diet)\">\n                    <ion-icon style=\"cursor: pointer;\"  name=\"close\"\n                      class=\"get-remove-icon\">\n                    </ion-icon>\n                  </ion-col>\n                  <ion-col>\n                    <span *ngIf=\"data?.eaten===2\">\n                      Logged\n                    </span>\n                    <!-- <ion-icon (click)=\"gotoNextItemDetail(data, diet, ing, xindex)\"\n                      name=\"arrow-forward-outline\" class=\"get-remove-icon\"\n                      style=\"color:#01A3A4\"\n                      [ngStyle]=\"{'color': currentDateIndex > 0 ? '#7d7d7d' : '#01A3A4'}\">\n                    </ion-icon> -->\n                  </ion-col>\n                </ion-row>\n                 </ion-col>\n            </ion-row>\n\n          </ion-label>\n         </ion-item>\n      </div>\n    </ion-list>\n    <div *ngIf=\"!diet.data || diet.data.length == 0\">\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <span>Your slot is empty.</span>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <span>Add something from the meal options.</span>\n        </ion-col>\n      </ion-row>\n      <div class=\"no-meal-container\">\n        <img src=\"./assets/images/no-meal.png\">\n      </div>\n    </div>\n  </ion-card-content>\n  <ion-item class=\"ion-no-padding ion-item ion-card-footer\" lines=\"none\"\n    style=\"background-color: #F6F7FC; --inner-padding-end:0px\">\n\n    <ion-row style=\"width: 100%; bottom: 0;\">\n       <ion-col size=\"12\" class=\"text-center\" style=\"border-top: 1px solid rgb(228 217 217);\" [ngStyle]=\"{'background':(!isPlanActiveForDiet && (slotIndex!==ing))?'none':'rgb(255,255,255)'}\">\n        <ion-button (click)=\"selectOption(ing);\" *ngIf=\"diet.data && diet.data.length > 0\" mode=\"ios\"\n          class=\"button button-cancel button-full color-gray\"\n          [ngClass]=\"{'color-black':diet.Locked,'other-meal':!diet.Locked, 'custom-background-fff': (!isPlanActiveForDiet && (slotIndex!==ing))}\" shape=\"round\"\n          style=\"--color:#007881;border:none;height: 50px;\">\n          <span\n            [ngClass]=\"{'custom-color-other':(!isPlanActiveForDiet && (slotIndex==ing)) ||  isPlanActiveForDiet, 'custom-orange custom-background-fff': (!isPlanActiveForDiet && (slotIndex!==ing))}\">\n            {{\n            (!isPlanActiveForDiet && (slotIndex==ing)) \n            || isPlanActiveForDiet ?\n             'Other Meal Option' : 'Update to Premium'\n              }}</span>\n        </ion-button>\n        <ion-button (click)=\"selectOption(ing);\" *ngIf=\"!diet.data || diet.data.length == 0\" mode=\"ios\"\n          class=\"button button-cancel button-full color-gray\"\n          [ngClass]=\"{'color-black':diet.Locked,'other-meal':!diet.Locked}\" shape=\"round\"\n          style=\"--color:#007881;border:none;height: 50px; --background:rgba(255,255,255);\">\n          <span  [ngClass]=\"{'custom-color-other':true}\">Add meal</span>\n        </ion-button>\n         <img (click)=\"upgradePlan()\" class=\"premium\" src=\"./assets/img/premium.jpg\"\n          *ngIf=\"(!isPlanActiveForDiet && (slotIndex!=ing))\">\n        <!-- <img src=\"assets/img/premium-crown.png\" *ngIf=\"(isPlanActiveForDiet && ing>2 )\"\n          class=\"premium\" style=\"height: 3vh; top:7px\" /> -->\n      </ion-col>\n    </ion-row>\n\n  </ion-item>\n</ion-card>");

/***/ }),

/***/ 10047:
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/components/home-water/home-water.component.html ***!
  \********************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<div>\n  <!-- <img class=\"disable-traker-container\" src=\"../../../assets/img/water_glass_main_disaled.svg\" *ngIf=\"currentDateIndex > 0\">\n  <circle-progress radius=\"30\" percent=\"{{(drankwater*100/12)}}\" outerStrokeGradient=\"true\" *ngIf=\"currentDateIndex == 0\"\n    outerStrokeGradientStopColor=\"#00B8F6\" showImage=\"true\" imageSrc=\"../../../assets/img/water_glass_main.svg\"\n    imageHeight=\"50\" imageWidth=\"50\" [innerStrokeColor]=\"'rgb(125,125,125,0.2)'\" [outerStrokeColor]=\"'#1437F4'\"\n    backgroundColor=\"#fff\" outerStrokeWidth=\"3\" innerStrokeWidth=\"3\"\n    title=\"{{roundingVal(totalTodaysCalories * 100/tolalCalories)}} \" [titleColor]=\"'#1437F4'\"\n    subtitle=\"{{habitList?.length}} habits\" [subtitleColor]=\"'#1437F4'\" units=\"%\" space=\"-3\"\n    [unitsColor]=\"'#1437F4'\">\n  </circle-progress> -->\n \n  <ion-row>\n    <ion-col class=\"ion-text-center ion-align-self-top\">\n      <span style=\"position: relative;\n      top: -56px; white-space: nowrap;\">\n       <span style=\"font-size: .75rem;color:#E50721\">\n        Eaten\n      </span><br>\n      <span style=\"font-size: 1.2rem;font-weight: 600;\">{{difference}}</span> Kcal</span>\n    </ion-col>\n  </ion-row>\n</div>");

/***/ }),

/***/ 906:
/*!********************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/components/recipe-day/recipe-day.component.html ***!
  \********************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<div>\n   \n    <ion-row>\n      <ion-col>\n        <ion-row>\n          <ion-col class=\"ion-text-left qa-day\">Recipe of the day</ion-col>\n        </ion-row>\n       \n        <div class=\"padding-top-1\" >\n          <!-- <video *ngIf=\"foodDetails.video && foodDetails.video != '--'\" [attr.src]=\"videoUrl\" \n          style=\"width: 100%;height: 200px;\" ></video> -->\n           <iframe *ngIf=\"foodDetails && foodDetails.video && foodDetails.video != '-' && foodDetails.video != '--'\" [src]='videoUrl' frameborder=\"0\"\n            allowfullscreen style=\"width: 100%;height: 200px;\"></iframe> \n          <br>\n         \n          <ion-row>\n              <ion-col class=\"ion-text-left title\" size=\"9\">{{foodDetails?.Name}} </ion-col>\n              <ion-col class=\"ion-text-right title\">{{foodDetails?.Calories}} Kcals</ion-col>\n            </ion-row>\n            <ion-row >\n              <ion-col>\n                <br>\n                <ion-row class=\"content-size\" >\n                    <ion-col class=\"ion-text-center detail padding-right-5\" >{{foodDetails?.Protien}} Gm \n                        <br><span>Proteins </span></ion-col>\n                      <ion-col class=\"ion-text-center detail padding-left-5\" >{{foodDetails?.Fat}} Gm <br>\n                        <span>Fats</span></ion-col>\n                      <ion-col class=\"ion-text-center detail padding-left-5\" >{{foodDetails?.Carbs}} Gm<br> <span>Carbs</span></ion-col>\n                      <ion-col class=\"ion-text-center  padding-left-5\" >{{foodDetails?.Fiber}} Gm <br><span><b>Fiber</b></span></ion-col>\n                </ion-row>\n              </ion-col>\n             <!-- <ion-col size=\"4\"></ion-col> -->\n              </ion-row>\n              <br>\n          <!-- <div class=\"view-more\">\n            <a style=\"color:#60AFF5 !important;\">Receipe By: -----</a>\n          </div> -->\n        </div>\n      </ion-col>\n    </ion-row>\n  </div>");

/***/ }),

/***/ 32220:
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/modal/summary-update/summary-update.page.html ***!
  \******************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n  \n  <div  >\n    <ion-card class=\"br-23\">\n      <div class=\"ion-padding-20\">\n       \n        <ion-label class=\"ion-padding-bottom-5 labelFont\">Remark</ion-label>\n        <ion-icon class=\"close-icon\" name=\"close-circle-outline\" (click)=\"closeModal()\"></ion-icon>\n        <ion-textarea minlength=\"25\" [(ngModel)]=\"remarkText\" (ionInput)=\"checkRemarkTextLength($event.target.value)\" placeholder=\"Write Remarks\"  class=\"ion-margin-top-5 ion-margin-bottom-20 textAreaBorder\"></ion-textarea>\n        <ion-label class=\"labelFont\">Status</ion-label>\n        <!-- <ion-list> -->\n          <ion-row>\n            <ion-col>\n              <ion-radio-group class=\"\" (ionFocus)=\"leadStatusChange($event)\" [value]=\"leadData.leadInfo.status\">\n            <ion-item lines=\"none\" class=\"ion-no-padding radioList\">\n              <ion-label >Not Interested</ion-label>\n              <ion-radio value=\"Not interested\" class=\"mr-10\" color=\"theme\" slot=\"start\"  mode=\"md\"></ion-radio>\n            </ion-item>\n            <ion-item lines=\"none\" class=\"ion-no-padding radioList\">\n              <ion-label >Call Me</ion-label>\n              <ion-radio value=\"Call Me\" class=\"mr-10\" color=\"theme\" slot=\"start\" mode=\"md\"></ion-radio>\n            </ion-item>\n            <ion-item lines=\"none\" class=\"ion-no-padding radioList\">\n              <ion-label >Call Afterwards</ion-label>\n              <ion-radio value=\"Call afterwards\" class=\"mr-10\" color=\"theme\" slot=\"start\" mode=\"md\"></ion-radio>\n            </ion-item>\n            <ion-item lines=\"none\" class=\"ion-no-padding radioList\">\n              <ion-label >Interested to Pay</ion-label>\n              <ion-radio value=\"Interested to Pay\"  class=\"mr-10\" color=\"theme\" slot=\"start\" mode=\"md\"></ion-radio>\n            </ion-item>\n          </ion-radio-group>\n            </ion-col>\n          </ion-row>\n          \n        <!-- </ion-list> -->\n        <ion-row class=\"ion-margin-top-20\">\n          <ion-col>\n            <ion-label class=\"labelFont\">Lead Type</ion-label>\n          </ion-col>\n        </ion-row>\n        <ion-row class=\"ion-margin-bottom-20\">\n          <ion-col class=\"radio-group-class\">\n            <ion-radio-group class=\"leadRadioPosition\" (ionFocus)=\"leadTypeChange($event)\" [value]=\"leadData.leadInfo.leadType\">\n              <ion-item lines=\"none\" class=\"ion-no-padding radioList\">\n                <ion-label >Cold</ion-label>\n                <ion-radio value=\"Cold\" class=\"mr-10\" color=\"theme\" slot=\"start\"  mode=\"md\"></ion-radio>\n              </ion-item>\n              <ion-item lines=\"none\" class=\"ion-no-padding radioList\">\n                <ion-label >Warm</ion-label>\n                <ion-radio value=\"Warm\" class=\"mr-10\" color=\"theme\" slot=\"start\" mode=\"md\"></ion-radio>\n              </ion-item>\n              <ion-item lines=\"none\" class=\"ion-no-padding radioList\">\n                <ion-label >Hot</ion-label>\n                <ion-radio value=\"Hot\" class=\"mr-10\" color=\"theme\" slot=\"start\" mode=\"md\"></ion-radio>\n              </ion-item>\n            </ion-radio-group>\n          </ion-col>\n        </ion-row>\n        \n        <ion-row >\n          <ion-col class=\"buttonBackground ion-text-center\">\n            <ion-button [disabled]=\"!updateRemarkValid\" (click)=\"updateSummary()\" shape=\"round\" color=\"theme\" fill=\"outline\">\n            Update\n          </ion-button></ion-col>\n        </ion-row>\n      </div>\n    </ion-card>\n  </div>\n\n<!-- </ion-content> -->\n");

/***/ }),

/***/ 4989:
/*!***********************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/select-plan-popup/select-plan-popup.component.html ***!
  \***********************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n<div class=\"offer-bottom-popup\">\n  <div>\n    <ion-card class=\"main-sec\">\n      <ion-card-header [ngStyle]=\"{'border-color': plan.id == 'post_covid' ? '#754B29' : planType =='weightLoss' ? '#01a3a4' : planType =='immunity_booster' ? '#FD9F33' :\n        planType =='detox' ? '#4CB271' : plan.id =='diabetes' ? '#D14322' : plan.id =='cholesterol' ? '#A31E79' :\n        plan.id =='hypertension' ? '#FF5D56' : plan.id == 'pcos' ? '#FF5A7D' : \n        (planType.split('_').length > 0 && planType.split('_')[0] == 'muscleGain') ? '#0B94C1' : \n        (planType.split('_').length > 0 && planType.split('_')[0] == 'fatShredding') ? '#FD980F' :\n        '#0B94C1'}\" style=\"border-top: 5px solid;\">\n      \n      </ion-card-header>\n\n      <ion-card-content [ngStyle]=\"{'border-color': plan.id == 'post_covid' ? '#754B29' : planType =='weightLoss' ? '#01a3a4' :  planType =='immunity_booster' ? '#FD9F33' : planType =='detox' ? '#4CB271' : plan.id =='diabetes' ? '#D14322' : plan.id =='cholesterol' ? '#A31E79' : plan.id =='hypertension' ? '#FF5D56' :\n      planType == 'pcos' ? '#FF5A7D' : \n      (planType.split('_').length > 0 && planType.split('_')[0] == 'muscleGain') ? '#0B94C1' : \n      (planType.split('_').length > 0 && planType.split('_')[0] == 'fatShredding') ? '#FD980F' :\n        '#0B94C1'}\" style=\"border-bottom: 5px solid;\">\n      \n        <div class=\"plan-container\"  *ngIf=\"planType =='weightLoss'\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">{{plan.name}}</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.subDesc}}</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img src=\"./assets/img/weight-loss.jpg\" />\n            </ion-col>\n          </ion-row>\n          <br />\n          <ion-row>\n            <ion-col>\n              <span class=\"text-title\">Being overweight or having excess percentage body fat is a metabolic error that is caused by an increased intake of simple carbohydrates and a sedentary lifestyle.</span>\n              <br /><br />\n              <span class=\"text-desc\">The weight loss plan nutritional recommendations are based on the principles of a balanced diet with limited calories and that is personalized for the user. The diet plan will help maintain the macro and micronutrients in the desired  range. \n              </span>\n              <br /><br />\n\n              <!-- <span class=\"text-desc\">Changes made on the plan and calories counted for today will get removed on switching to {{plan.name}} plan</span>\n              <br /><br /> -->\n\n              <span class=\"text-desc-note\"><b>Note :</b> The content of this app is provided as an information source\n                and is not intended as a substitute for professional medical advice.</span>\n            </ion-col>\n          </ion-row>\n          <br />\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"weight-loss-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"weight-loss-button-activate\" (click)=\"doActivate(true, plan.id)\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div class=\"plan-container ion-margin-top\" *ngIf=\"planType =='immunity_booster'\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">{{plan.name}}</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.subDesc}}</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img [src]=\"plan.img\" />\n            </ion-col>\n          </ion-row>\n          <br />\n          <ion-row>\n            <ion-col>\n              <span class=\"text-title\">Being overweight or having excess percentage body fat is a metabolic error that is caused by an increased intake of simple carbohydrates and a sedentary lifestyle.</span>\n              <br /><br />\n              <span class=\"text-desc\">Nutritional recommendations are based on the principles of a balanced diet with limited calories and that is personalized for the user. It also takes care of those food elements which improves the immunity.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">The diet plan will help maintain the macro and micronutrients in the desired  range for weight loss and for improving immunity.</span>\n              <br /><br />\n\n              <span class=\"text-desc-note\"><b>Note :</b> {{plan.note}}</span>\n            </ion-col>\n          </ion-row>\n          <br />\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"immunity-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"immunity-button-activate\" (click)=\"doActivate(true, plan.id)\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div class=\"plan-container\" *ngIf=\"planType =='post_covid'\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">{{plan.name}}</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.subDesc}}</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img [src]=\"plan.img\" />\n            </ion-col>\n          </ion-row>\n          <br />\n          <ion-row>\n            <ion-col>\n              <span class=\"text-title\">Recommendations under this plan are NOT FOR WEIGHT LOSS but we suggest foods\n                which\n                can help recovery in post covid.</span>\n              <br /><br />\n              <span class=\"text-desc\">We don’t show weight loss trackers in this plan. We don’t suggest a detox plan\n                option in this plan.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">Changes made on the plan and calories counted for today will get removed on switching to {{plan.name}} plan</span>\n              <br /><br />\n\n              <span class=\"text-desc-note\"><b>Note :</b> {{plan.note}}</span>\n            </ion-col>\n          </ion-row>\n          <br />\n\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"post-covid-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"post-covid-button-activate\" (click)=\"doActivate(true, plan.id)\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div class=\"plan-container\" *ngIf=\"planType =='detox'\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">Detox Plan</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">Low calories plan to accelerate\n                    weight loss</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img src=\"./assets/img/detox.jpg\" />\n            </ion-col>\n          </ion-row>\n          <br />\n          <ion-row>\n            <ion-col>\n              <span class=\"text-title\">Detox / Fasting / Low-Calorie plans can be good for healing of the body & to\n                accelerate weight loss. You can do it once or twice a week. Start with once a week.</span>\n              <br /><br />\n              <span class=\"text-desc\">It is not recommended for people with diabetes, cancer, heart disease or any other\n                chronic medical condition. </span>\n              <br /><br />\n\n              <span class=\"text-desc\">Changes made on the plan and calories counted for today will get removed on switching to {{plan.name}} plan</span>\n              <br /><br />\n\n              <span class=\"text-desc-note\"><b>Note :</b> {{plan.note}}</span>\n            </ion-col>\n          </ion-row>\n          <br />\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"detox-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"detox-button-activate\" (click)=\"doActivate(true, 'detox')\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div class=\"plan-container\" *ngIf=\"planType =='diabetes'\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">{{plan.name}}</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.subDesc}}{{plan.subDescLine2}}</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img [src]=\"plan.img\" />\n            </ion-col>\n          </ion-row>\n          <br />\n          <ion-row>\n            <ion-col>\n              <span class=\"text-desc\">It is caused by insulin resistance and presents as high sugar levels in the blood stream. The nutritional recommendations are based on correcting insulin resistance and reducing inflammation.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">The diet eliminates inflammatory foods such as simple sugar, ultra processed foods, unhealthy fats, excess dairy and focuses on timed and portion controlled meals that are rich in colorful vegetables and fruits, nuts and seeds and unpolished grains and lentils.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">The diet plan will help maintain the macro and micronutrients in the desired  range which can help in resolving metabolic disorder and also eliminating abdominal fat which is one of the most difficult ones to  address.</span>\n              <br /><br />\n              \n              <span class=\"text-desc-note\"><b>Note :</b> {{plan.note}}</span>\n            </ion-col>\n          </ion-row>\n          <br />\n\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"diabetes-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"diabetes-button-activate\" (click)=\"doActivate(true, plan.id)\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div class=\"plan-container\" *ngIf=\"planType =='cholesterol'\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">{{plan.name}}</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.subDesc}}{{plan.subDescLine2}}</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img [src]=\"plan.img\" />\n            </ion-col>\n          </ion-row>\n          <br />\n          <ion-row>\n            <ion-col>\n              <span class=\"text-desc\">Being overweight or having excess percentage body fat along with high cholesterol  is a metabolic error that is caused by an increased intake of bad quality fats , excess of simple carbohydrates and a sedentary lifestyle.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">The cholesterol lowering nutritional recommendation includes providing good quality fats and Omega 3 from nuts and oilseeds and the essential micronutrients and FIBRE from plenty of fresh vegetables ,fruits with millets and whole pulses.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">The diet plan will help maintain the micronutrients and minerals in the desired  range which can help in resolving metabolic disorder and also eliminating abdominal fat which is one of the most difficult ones to address.</span>\n              <br /><br />\n              \n              <span class=\"text-desc-note\"><b>Note :</b> {{plan.note}}</span>\n            </ion-col>\n          </ion-row>\n          <br />\n\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"cholesterol-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"cholesterol-button-activate\" (click)=\"doActivate(true, plan.id)\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div class=\"plan-container\" *ngIf=\"planType =='hypertension'\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">{{plan.name}}</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.subDesc}}{{plan.subDescLine2}}</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img [src]=\"plan.img\" />\n            </ion-col>\n          </ion-row>\n          <!-- <br /> -->\n          <ion-row>\n            <ion-col>\n              <span class=\"text-desc\">Being overweight or having excess percentage body fat along with hypertension is a metabolic error that is caused by an increased intake of simple carbohydrates and a sedentary lifestyle.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">The nutritional recommendations are based on the concept of DASH DIET (Dietary Approaches to Stop Hypertension).</span>\n              <br /><br />\n\n              <span class=\"text-desc\">The Dietary Approach to Stop Hypertension hinges on reduced Sodium consumption and an increased intake of minerals such Potassium, Magnesium and Fibre which regulate blood pressure.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">The diet plan will help maintain the micronutrients and minerals in the desired  range which can help in resolving metabolic disorder and also eliminating abdominal fat which is one of the most difficult ones to  address.</span>\n              <br /><br />\n\n              <span class=\"text-desc-note\"><b>Note :</b> {{plan.note}}</span>\n            </ion-col>\n          </ion-row>\n          <br />\n\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"hypertension-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"hypertension-button-activate\" (click)=\"doActivate(true, plan.id)\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n        <div class=\"plan-container\" *ngIf=\"planType =='pcos'\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">{{plan.name}}</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.subDesc}}{{plan.subDescLine2}}</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img [src]=\"plan.img\" />\n            </ion-col>\n          </ion-row>\n          <!-- <br /> -->\n          <ion-row>\n            <ion-col>\n              <span class=\"text-desc\">It is a hormone disorder that is caused by insulin resistance and presents as irregular periods, acne, weight gain etc. The PCOS nutritional recommendations are based on correcting insulin resistance and reducing inflammation.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">The diet eliminates inflammatory foods such as simple sugar, ultra processed foods, unhealthy fats, excess dairy and focuses on timed and portion controlled meals that are rich in colorful vegetables and fruits, nuts and seeds and unpolished grains and lentils. </span>\n              <br /><br />\n\n              <span class=\"text-desc\">The diet plan will help maintain the macro and micronutrients in the desired  range which can help in resolving metabolic disorder and also eliminating abdominal fat which is one of the most difficult ones to  address.</span>\n              <br /><br />\n\n              <span class=\"text-desc-note\"><b>Note :</b> {{plan.note}}</span>\n            </ion-col>\n          </ion-row>\n          <br />\n\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"pcos-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"pcos-button-activate\" (click)=\"doActivate(true, plan.id)\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div class=\"plan-container\" *ngIf=\"planType =='weightLossPlus'\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">{{plan.name}}</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.subDesc}}{{plan.subDescLine2}}</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img [src]=\"plan.img\" />\n            </ion-col>\n          </ion-row>\n          <!-- <br /> -->\n          <ion-row>\n            <ion-col>\n              <span class=\"text-desc\">Being overweight or having excess percentage body fat is a metabolic error that is caused by an increased intake of simple carbohydrates and a sedentary lifestyle.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">The weight loss plus nutritional recommendations are based on the HFLC diet in a moderate form and that is personalized for the user. The total Net Carbohydrates per day is closely monitored to accelerate weight loss and reduce insulin resistance.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">Keeping the Net Carbs under  a specific  range helps to eliminate  abdominal fat which is one of the most difficult ones to address.</span>\n              <br /><br />\n\n              <span class=\"text-desc-note\"><b>Note :</b> The content of this app is provided as an information source and is not a substitute for professional advice.</span>\n            </ion-col>\n          </ion-row>\n          <br />\n\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"weight-loss-plus-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"weight-loss-plus-button-activate\" (click)=\"doActivate(true, plan.id)\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div class=\"plan-container\" *ngIf=\"(planType.split('_').length > 0 && planType.split('_')[0] == 'muscleGain')\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">{{plan.name}}</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.subDesc}}{{plan.subDescLine2}}</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img [src]=\"plan.img\" />\n            </ion-col>\n          </ion-row>\n          <!-- <br /> -->\n          <ion-row>\n            <ion-col>\n              <span class=\"text-desc\">The Food we eat has the potential to build our muscles and also to break down the existing muscle for energy purposes. During the Muscle Building process, diet is a major component that needs to be taken care of. </span>\n              <br /><br />\n\n              <span class=\"text-desc\">The extensive workout needs to be supported by an adequate amount of nutrients like energy, protein, water and electrolyte intake. </span>\n              <br /><br />\n\n              <span class=\"text-desc\">Right nutrition can help you recover faster, build muscles and prevent muscle cramps post workout. </span>\n              <br /><br />\n\n              <span class=\"text-desc-note\"><b>Note :</b> The content of this app is provided as an information source and is not a substitute for professional advice.</span>\n            </ion-col>\n          </ion-row>\n          <br />\n\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"muscle-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"muscle-button-activate\" (click)=\"doActivate(true, plan.id)\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n\n        <div class=\"plan-container\" *ngIf=\"(planType.split('_').length > 0 && planType.split('_')[0] == 'fatShredding')\">\n          <ion-row>\n            <ion-col class=\"ion-text-left ion-align-self-center\">\n              <ion-row>\n                <ion-col>\n                  <span class=\"sub-foodItem-header\">{{plan.name}}</span>\n                </ion-col>\n              </ion-row>\n              <ion-row style=\"padding-top: 5px;\">\n                <ion-col>\n                  <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.subDesc}}{{plan.subDescLine2}}</span>\n                </ion-col>\n              </ion-row>\n              <!-- <ion-row>\n              <ion-col>\n                <span class=\"sub-foodItem-sub-header sub-foodItem-sub-header-custom\">{{plan.desc}}</span>\n              </ion-col>\n            </ion-row> -->\n            </ion-col>\n            <ion-col class=\"ion-text-center ion-align-self-center\">\n              <img [src]=\"plan.img\" />\n            </ion-col>\n          </ion-row>\n          <!-- <br /> -->\n          <ion-row>\n            <ion-col>\n              <span class=\"text-desc\">During the muscle building process, there can also be some fat gain. After a few weeks of muscle building phase, the Fat shredding phase occurs. </span>\n              <br /><br />\n\n              <span class=\"text-desc\">Adequate nutrition can help you shred fat without losing your muscle mass. Fat shredding is also known as Cutting phase. Weightlifting and cutting diets can help you lose fats while maintaining muscle mass.</span>\n              <br /><br />\n\n              <span class=\"text-desc\">Calorie deficit with adequate protein, fat and carbs ratio along with a good amount of micronutrients helps you burn your extra fat and reach your goal. This is where we help you</span>\n              <br /><br />\n\n              <span class=\"text-desc-note\"><b>Note :</b> The content of this app is provided as an information source and is not a substitute for professional advice.</span>\n            </ion-col>\n          </ion-row>\n          <br />\n\n          <ion-row class=\"bottom-btn\">\n            <ion-col class=\"ion-text-center\">\n              <ion-button fill=\"outline\" class=\"fat-button-cancel\" (click)=\"close()\">Cancel</ion-button>\n            </ion-col>\n            <ion-col class=\"ion-text-center\">\n              <ion-button class=\"fat-button-activate\" (click)=\"doActivate(true, plan.id)\">Activate</ion-button>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-card-content>\n      <!-- <span><span class=\"border-section-btm\"\n          [ngStyle]=\"{'background-color': planType =='weightLoss' ? '#01a3a4' :  planType =='immunity_booster' ? '#FD9F33' : planType =='detox' ? '#4CB271' : '#0B94C1'}\"></span></span> -->\n    </ion-card>\n  </div>\n</div>");

/***/ }),

/***/ 46700:
/*!***************************************************!*\
  !*** ./node_modules/moment/locale/ sync ^\.\/.*$ ***!
  \***************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./af": 32139,
	"./af.js": 32139,
	"./ar": 22600,
	"./ar-dz": 81001,
	"./ar-dz.js": 81001,
	"./ar-kw": 99842,
	"./ar-kw.js": 99842,
	"./ar-ly": 9826,
	"./ar-ly.js": 9826,
	"./ar-ma": 15452,
	"./ar-ma.js": 15452,
	"./ar-sa": 11802,
	"./ar-sa.js": 11802,
	"./ar-tn": 4094,
	"./ar-tn.js": 4094,
	"./ar.js": 22600,
	"./az": 96375,
	"./az.js": 96375,
	"./be": 2086,
	"./be.js": 2086,
	"./bg": 85236,
	"./bg.js": 85236,
	"./bm": 81704,
	"./bm.js": 81704,
	"./bn": 94506,
	"./bn-bd": 34466,
	"./bn-bd.js": 34466,
	"./bn.js": 94506,
	"./bo": 47891,
	"./bo.js": 47891,
	"./br": 93348,
	"./br.js": 93348,
	"./bs": 84848,
	"./bs.js": 84848,
	"./ca": 35928,
	"./ca.js": 35928,
	"./cs": 31839,
	"./cs.js": 31839,
	"./cv": 59151,
	"./cv.js": 59151,
	"./cy": 35761,
	"./cy.js": 35761,
	"./da": 56686,
	"./da.js": 56686,
	"./de": 85177,
	"./de-at": 2311,
	"./de-at.js": 2311,
	"./de-ch": 54407,
	"./de-ch.js": 54407,
	"./de.js": 85177,
	"./dv": 79729,
	"./dv.js": 79729,
	"./el": 60430,
	"./el.js": 60430,
	"./en-au": 28430,
	"./en-au.js": 28430,
	"./en-ca": 61139,
	"./en-ca.js": 61139,
	"./en-gb": 56747,
	"./en-gb.js": 56747,
	"./en-ie": 79466,
	"./en-ie.js": 79466,
	"./en-il": 52121,
	"./en-il.js": 52121,
	"./en-in": 41167,
	"./en-in.js": 41167,
	"./en-nz": 62030,
	"./en-nz.js": 62030,
	"./en-sg": 43646,
	"./en-sg.js": 43646,
	"./eo": 73126,
	"./eo.js": 73126,
	"./es": 38819,
	"./es-do": 69293,
	"./es-do.js": 69293,
	"./es-mx": 65304,
	"./es-mx.js": 65304,
	"./es-us": 66068,
	"./es-us.js": 66068,
	"./es.js": 38819,
	"./et": 23291,
	"./et.js": 23291,
	"./eu": 1400,
	"./eu.js": 1400,
	"./fa": 70043,
	"./fa.js": 70043,
	"./fi": 16138,
	"./fi.js": 16138,
	"./fil": 11466,
	"./fil.js": 11466,
	"./fo": 76803,
	"./fo.js": 76803,
	"./fr": 65523,
	"./fr-ca": 697,
	"./fr-ca.js": 697,
	"./fr-ch": 69001,
	"./fr-ch.js": 69001,
	"./fr.js": 65523,
	"./fy": 21116,
	"./fy.js": 21116,
	"./ga": 66151,
	"./ga.js": 66151,
	"./gd": 93094,
	"./gd.js": 93094,
	"./gl": 11279,
	"./gl.js": 11279,
	"./gom-deva": 64458,
	"./gom-deva.js": 64458,
	"./gom-latn": 66320,
	"./gom-latn.js": 66320,
	"./gu": 78658,
	"./gu.js": 78658,
	"./he": 52153,
	"./he.js": 52153,
	"./hi": 98732,
	"./hi.js": 98732,
	"./hr": 84960,
	"./hr.js": 84960,
	"./hu": 76339,
	"./hu.js": 76339,
	"./hy-am": 11862,
	"./hy-am.js": 11862,
	"./id": 71068,
	"./id.js": 71068,
	"./is": 61260,
	"./is.js": 61260,
	"./it": 1007,
	"./it-ch": 78063,
	"./it-ch.js": 78063,
	"./it.js": 1007,
	"./ja": 6854,
	"./ja.js": 6854,
	"./jv": 92390,
	"./jv.js": 92390,
	"./ka": 35958,
	"./ka.js": 35958,
	"./kk": 67216,
	"./kk.js": 67216,
	"./km": 61061,
	"./km.js": 61061,
	"./kn": 24060,
	"./kn.js": 24060,
	"./ko": 55216,
	"./ko.js": 55216,
	"./ku": 50894,
	"./ku.js": 50894,
	"./ky": 609,
	"./ky.js": 609,
	"./lb": 3591,
	"./lb.js": 3591,
	"./lo": 38381,
	"./lo.js": 38381,
	"./lt": 56118,
	"./lt.js": 56118,
	"./lv": 67889,
	"./lv.js": 67889,
	"./me": 94274,
	"./me.js": 94274,
	"./mi": 39226,
	"./mi.js": 39226,
	"./mk": 528,
	"./mk.js": 528,
	"./ml": 27938,
	"./ml.js": 27938,
	"./mn": 35456,
	"./mn.js": 35456,
	"./mr": 94393,
	"./mr.js": 94393,
	"./ms": 93647,
	"./ms-my": 33049,
	"./ms-my.js": 33049,
	"./ms.js": 93647,
	"./mt": 26097,
	"./mt.js": 26097,
	"./my": 66277,
	"./my.js": 66277,
	"./nb": 67245,
	"./nb.js": 67245,
	"./ne": 3988,
	"./ne.js": 3988,
	"./nl": 42557,
	"./nl-be": 20478,
	"./nl-be.js": 20478,
	"./nl.js": 42557,
	"./nn": 9046,
	"./nn.js": 9046,
	"./oc-lnc": 83131,
	"./oc-lnc.js": 83131,
	"./pa-in": 51731,
	"./pa-in.js": 51731,
	"./pl": 8409,
	"./pl.js": 8409,
	"./pt": 41178,
	"./pt-br": 56558,
	"./pt-br.js": 56558,
	"./pt.js": 41178,
	"./ro": 84138,
	"./ro.js": 84138,
	"./ru": 73380,
	"./ru.js": 73380,
	"./sd": 42889,
	"./sd.js": 42889,
	"./se": 22774,
	"./se.js": 22774,
	"./si": 53776,
	"./si.js": 53776,
	"./sk": 9597,
	"./sk.js": 9597,
	"./sl": 93871,
	"./sl.js": 93871,
	"./sq": 44228,
	"./sq.js": 44228,
	"./sr": 40774,
	"./sr-cyrl": 61928,
	"./sr-cyrl.js": 61928,
	"./sr.js": 40774,
	"./ss": 83176,
	"./ss.js": 83176,
	"./sv": 52422,
	"./sv.js": 52422,
	"./sw": 52530,
	"./sw.js": 52530,
	"./ta": 5731,
	"./ta.js": 5731,
	"./te": 18025,
	"./te.js": 18025,
	"./tet": 53934,
	"./tet.js": 53934,
	"./tg": 99958,
	"./tg.js": 99958,
	"./th": 84251,
	"./th.js": 84251,
	"./tk": 65494,
	"./tk.js": 65494,
	"./tl-ph": 38568,
	"./tl-ph.js": 38568,
	"./tlh": 73158,
	"./tlh.js": 73158,
	"./tr": 49574,
	"./tr.js": 49574,
	"./tzl": 64311,
	"./tzl.js": 64311,
	"./tzm": 99990,
	"./tzm-latn": 42380,
	"./tzm-latn.js": 42380,
	"./tzm.js": 99990,
	"./ug-cn": 52356,
	"./ug-cn.js": 52356,
	"./uk": 54934,
	"./uk.js": 54934,
	"./ur": 84515,
	"./ur.js": 84515,
	"./uz": 40058,
	"./uz-latn": 41875,
	"./uz-latn.js": 41875,
	"./uz.js": 40058,
	"./vi": 13325,
	"./vi.js": 13325,
	"./x-pseudo": 39208,
	"./x-pseudo.js": 39208,
	"./yo": 18742,
	"./yo.js": 18742,
	"./zh-cn": 42378,
	"./zh-cn.js": 42378,
	"./zh-hk": 21074,
	"./zh-hk.js": 21074,
	"./zh-mo": 74671,
	"./zh-mo.js": 74671,
	"./zh-tw": 20259,
	"./zh-tw.js": 20259
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 46700;

/***/ }),

/***/ 30836:
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 64651:
/*!********************************************************!*\
  !*** ./src/app/change-plan/change-plan.component.scss ***!
  \********************************************************/
/***/ ((module) => {

"use strict";
module.exports = "@font-face {\n  font-family: gt-america-standard-medium;\n  src: url(\"/assets/fonts/Roboto-Medium.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-regular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-bold;\n  src: url(\"/assets/fonts/Roboto-Bold.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-extendedMedium;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-light;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: Source-Sans-Pro-ragular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\nion-content {\n  --background: #F6F7FC !important;\n  --padding-bottom: 20% !important;\n}\n.personalise-covid ion-card {\n  display: inline-block;\n  width: 90%;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.plan-container {\n  background-size: contain;\n  background-repeat: no-repeat;\n  background-position: right;\n  margin-bottom: 0;\n  height: 175px;\n}\n.sub-foodItem-header {\n  font-size: 1rem;\n  font-weight: bold;\n  color: #4C5264;\n}\n.sub-foodItem-sub-header {\n  font-size: 0.85rem;\n}\n.sub-foodItem-sub-header2 {\n  display: block;\n}\n.sub-foodItem-btn {\n  border: none !important;\n  width: auto !important;\n  color: #1492E6;\n  --background: transparent;\n  min-width: 80px !important;\n  margin-top: 5px;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.sub-cards {\n  background: #fff;\n  margin-top: 0;\n}\n.ion-card-footer {\n  width: 100%;\n  bottom: 0;\n  position: absolute;\n}\n.card-footer {\n  width: 100%;\n  bottom: 0;\n  position: absolute;\n  height: 20%;\n  background: #03a3a4;\n}\n.premium {\n  height: 30px;\n  position: absolute;\n  bottom: 5px;\n  display: block;\n  left: 18px;\n}\n.qa-day-skeleton {\n  margin-top: 0;\n  padding: 0;\n  height: 300px;\n  width: 90%;\n  display: inline-table;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.activity-skeleton {\n  width: 100%;\n  height: 100%;\n  margin-top: 0;\n}\n.immunity-section {\n  --background: #FD9F33 !important;\n}\n.detox-section {\n  --background: #4CB271 !important;\n}\n.post-covid-section {\n  --background: #754B29 !important;\n}\n.weight-loss-plus-section {\n  --background: #0B94C1 !important;\n}\n.diabetes-section {\n  --background: #D14322 !important;\n}\n.cholesterol-section {\n  --background: #A31E79 !important;\n}\n.hypertension-section {\n  --background: #FF5D56 !important;\n}\n.pcos-section {\n  --background: #FF5A7D !important;\n}\n.weight-loss {\n  --background: #01A3A4 !important;\n}\n.mucsle {\n  --background: #0B94C1 !important;\n}\n.fat {\n  --background: #FD980F !important;\n}\n.premium-btn {\n  --background: #fff !important;\n}\n.premium-lock {\n  height: 3vh;\n  padding-right: 2px;\n}\nion-button {\n  max-height: 1.75rem;\n  font-size: 0.8rem;\n  width: 100px;\n  position: absolute;\n  bottom: 40px;\n  left: 18px;\n  border-radius: 100px;\n}\n::ng-deep .immunity-section-border {\n  --border-color: #FD9F33 !important;\n  --color: #FD9F33 !important;\n}\n::ng-deep .detox-section-border {\n  --border-color: #4CB271 !important;\n  --color: #4CB271 !important;\n}\n::ng-deep .post-covid-section-border {\n  --border-color: #754B29 !important;\n  --color: #754B29 !important;\n}\n::ng-deep .weight-loss-plus-section-border {\n  --border-color: #0B94C1 !important;\n  --color: #0B94C1 !important;\n}\n::ng-deep .diabetes-section-border {\n  --border-color: #D14322 !important;\n  --color: #D14322 !important;\n}\n::ng-deep .pcos-section-border {\n  --border-color: #FF5A7D !important;\n  --color: #FF5A7D !important;\n}\n::ng-deep .mucsle-border {\n  --border-color: #0B94C1 !important;\n  --color: #0B94C1 !important;\n}\n::ng-deep .fat-border {\n  --border-color: #FD980F !important;\n  --color: #FD980F !important;\n}\n::ng-deep .cholesterol-section-border {\n  --border-color: #A31E79 !important;\n  --color: #A31E79 !important;\n}\n::ng-deep .hypertension-section-border {\n  --border-color: #FF5D56 !important;\n  --color: #FF5D56 !important;\n}\n::ng-deep .weight-loss-border {\n  --border-color: #01A3A4 !important;\n  --color: #01A3A4 !important;\n}\n.white-background {\n  --background: #FFF;\n}\n.personalisation {\n  font-size: 0.85rem;\n  font-style: italic;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #4c5264;\n  background: #fff;\n}\n.legend-border {\n  height: 125px;\n  position: absolute;\n  z-index: 9;\n  top: 14%;\n  border-radius: 100px;\n}\n.recom {\n  width: 13vh;\n  margin-left: 16px;\n  margin-bottom: 10px;\n  position: absolute;\n  bottom: 0px;\n  left: 5px;\n}\n.stacked-cards {\n  position: absolute;\n  width: 100%;\n  transition-duration: 0.3s;\n}\n.expand-cards {\n  position: absolute;\n  width: 100%;\n  transition-duration: 0.3s;\n}\n.show-less-txt {\n  color: #1492E6;\n  font-size: 0.9rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL2Fzc2V0cy9mb250cy9mb250cy5zY3NzIiwiY2hhbmdlLXBsYW4uY29tcG9uZW50LnNjc3MiLCIuLi8uLi90aGVtZS9jdXN0b20tdGhlbWVzL3ZhcmlhYmxlcy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsdUNBQUE7RUFDQSw4REFBQTtBQ0NGO0FERUE7RUFDRSx3Q0FBQTtFQUNBLCtEQUFBO0FDQUY7QURHQTtFQUNFLHFDQUFBO0VBQ0EsNERBQUE7QUNERjtBRElBO0VBQ0UsK0NBQUE7RUFDQSw2REFBQTtBQ0ZGO0FES0E7RUFDRSxzQ0FBQTtFQUNBLDZEQUFBO0FDSEY7QURPQTtFQUNFLG9DQUFBO0VBQ0EsK0RBQUE7QUNMRjtBQXJCQTtFQUNFLGdDQUFBO0VBQ0EsZ0NBQUE7QUF1QkY7QUFuQkU7RUFDRSxxQkFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGdFQUFBO0FBc0JKO0FBaEJBO0VBQ0Usd0JBQUE7RUFDQSw0QkFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0FBbUJGO0FBZkE7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBa0JGO0FBZkE7RUFDRSxrQkFBQTtBQWtCRjtBQWZBO0VBQ0UsY0FBQTtBQWtCRjtBQWZBO0VBQ0UsdUJBQUE7RUFDQSxzQkFBQTtFQUVBLGNBQUE7RUFDQSx5QkFBQTtFQUVBLDBCQUFBO0VBQ0EsZUFBQTtFQUVBLDRFQUFBO0FBZUY7QUFaQTtFQUNFLGdCQUFBO0VBRUEsYUFBQTtBQWNGO0FBWEE7RUFFRSxXQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0FBYUY7QUFWQTtFQUNFLFdBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUFhRjtBQVZBO0VBQ0UsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxVQUFBO0FBYUY7QUFWQTtFQUNFLGFBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUFhRjtBQVZBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLGdDQUFBO0FBYUY7QUFWQTtFQUNFLDZCQUFBO0FBYUY7QUFWQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtBQWFGO0FBVkE7RUFFRSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUVBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxvQkFBQTtBQVdGO0FBUkE7RUFDRSxrQ0FBQTtFQUNBLDJCQUFBO0FBV0Y7QUFSQTtFQUNFLGtDQUFBO0VBQ0EsMkJBQUE7QUFXRjtBQVJBO0VBQ0Usa0NBQUE7RUFDQSwyQkFBQTtBQVdGO0FBUkE7RUFDRSxrQ0FBQTtFQUNBLDJCQUFBO0FBV0Y7QUFSQTtFQUNFLGtDQUFBO0VBQ0EsMkJBQUE7QUFXRjtBQVJBO0VBQ0Usa0NBQUE7RUFDQSwyQkFBQTtBQVdGO0FBUkE7RUFDRSxrQ0FBQTtFQUNBLDJCQUFBO0FBV0Y7QUFSQTtFQUNFLGtDQUFBO0VBQ0EsMkJBQUE7QUFXRjtBQVJBO0VBQ0Usa0NBQUE7RUFDQSwyQkFBQTtBQVdGO0FBUkE7RUFDRSxrQ0FBQTtFQUNBLDJCQUFBO0FBV0Y7QUFSQTtFQUNFLGtDQUFBO0VBQ0EsMkJBQUE7QUFXRjtBQVJBO0VBQ0Usa0JBQUE7QUFXRjtBQVJBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtFQUNBLCtFQzdOSztFRDhOTCxjQUFBO0VBQ0EsZ0JBQUE7QUFXRjtBQVJBO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7RUFDQSxvQkFBQTtBQVdGO0FBUkE7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7QUFXRjtBQVJBO0VBQ0Usa0JBQUE7RUFHQSxXQUFBO0VBQ0EseUJBQUE7QUFTRjtBQUpBO0VBQ0Usa0JBQUE7RUFHQSxXQUFBO0VBQ0EseUJBQUE7QUFLRjtBQUFBO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0FBR0YiLCJmaWxlIjoiY2hhbmdlLXBsYW4uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJAZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtbWVkaXVtO1xuICBzcmM6IHVybChcIi9hc3NldHMvZm9udHMvUm9ib3RvLU1lZGl1bS50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1yZWd1bGFyO1xuICBzcmM6IHVybChcIi9hc3NldHMvZm9udHMvUm9ib3RvLVJlZ3VsYXIudHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtYm9sZDtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1Cb2xkLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLWV4dGVuZGVkTWVkaXVtO1xuICBzcmM6IHVybChcIi9hc3NldHMvZm9udHMvUm9ib3RvLUxpZ2h0LnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLWxpZ2h0O1xuICBzcmM6IHVybChcIi9hc3NldHMvZm9udHMvUm9ib3RvLUxpZ2h0LnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IFNvdXJjZS1TYW5zLVByby1yYWd1bGFyO1xuICBzcmM6IHVybChcIi9hc3NldHMvZm9udHMvUm9ib3RvLVJlZ3VsYXIudHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuIiwiQGltcG9ydCBcIi4uLy4uL3RoZW1lL2ZvbnRzLnNjc3NcIjtcblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6ICNGNkY3RkMgIWltcG9ydGFudDtcbiAgLS1wYWRkaW5nLWJvdHRvbTogMjAlICFpbXBvcnRhbnQ7XG4gfVxuXG4ucGVyc29uYWxpc2UtY292aWQge1xuICBpb24tY2FyZCB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYigxMjkgMTMzIDEzMSAvIDEwJSkgIWltcG9ydGFudDtcbiAgICAvLyBtYXJnaW46IDIwcHg7XG5cbiAgfVxufVxuXG4ucGxhbi1jb250YWluZXIge1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IHJpZ2h0O1xuICBtYXJnaW4tYm90dG9tOiAwO1xuICBoZWlnaHQ6IDE3NXB4O1xufVxuXG5cbi5zdWItZm9vZEl0ZW0taGVhZGVyIHtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM0QzUyNjQ7XG59XG5cbi5zdWItZm9vZEl0ZW0tc3ViLWhlYWRlciB7XG4gIGZvbnQtc2l6ZTogMC44NXJlbTtcbn1cblxuLnN1Yi1mb29kSXRlbS1zdWItaGVhZGVyMntcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5zdWItZm9vZEl0ZW0tYnRuIHtcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XG4gIC8vIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGNvbG9yOiAjMTQ5MkU2O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAvLyAtLWJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIG1pbi13aWR0aDogODBweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIC8vIGZvbnQtc2l6ZTogMC44NzVyZW0gIWltcG9ydGFudDtcbiAgZm9udC1mYW1pbHk6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1ib2xkXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG59XG5cbi5zdWItY2FyZHMge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAvLyBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBtYXJnaW4tdG9wOiAwO1xufVxuXG4uaW9uLWNhcmQtZm9vdGVyIHtcbiAgLy8gcG9zaXRpb246IGZpeGVkO1xuICB3aWR0aDogMTAwJTtcbiAgYm90dG9tOiAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5jYXJkLWZvb3RlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBib3R0b206IDA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgaGVpZ2h0OiAyMCU7XG4gIGJhY2tncm91bmQ6ICMwM2EzYTQ7XG59XG5cbi5wcmVtaXVtIHtcbiAgaGVpZ2h0OiAzMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogNXB4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgbGVmdDogMThweDtcbn1cblxuLnFhLWRheS1za2VsZXRvbiB7XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmc6IDA7XG4gIGhlaWdodDogMzAwcHg7XG4gIHdpZHRoOiA5MCU7XG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYm94LXNoYWRvdzogMHB4IDVweCAxMHB4IDBweCByZ2IoMTI5IDEzMyAxMzEgLyAxMCUpICFpbXBvcnRhbnQ7XG59XG5cbi5hY3Rpdml0eS1za2VsZXRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5cbi5pbW11bml0eS1zZWN0aW9uIHtcbiAgLS1iYWNrZ3JvdW5kOiAjRkQ5RjMzICFpbXBvcnRhbnQ7XG59XG5cbi5kZXRveC1zZWN0aW9uIHtcbiAgLS1iYWNrZ3JvdW5kOiAjNENCMjcxICFpbXBvcnRhbnQ7XG59XG5cbi5wb3N0LWNvdmlkLXNlY3Rpb24ge1xuICAtLWJhY2tncm91bmQ6ICM3NTRCMjkgIWltcG9ydGFudDtcbn1cblxuLndlaWdodC1sb3NzLXBsdXMtc2VjdGlvbiB7XG4gIC0tYmFja2dyb3VuZDogIzBCOTRDMSAhaW1wb3J0YW50O1xufVxuXG4uZGlhYmV0ZXMtc2VjdGlvbiB7XG4gIC0tYmFja2dyb3VuZDogI0QxNDMyMiAhaW1wb3J0YW50O1xufVxuXG4uY2hvbGVzdGVyb2wtc2VjdGlvbiB7XG4gIC0tYmFja2dyb3VuZDogI0EzMUU3OSAhaW1wb3J0YW50O1xufVxuXG4uaHlwZXJ0ZW5zaW9uLXNlY3Rpb24ge1xuICAtLWJhY2tncm91bmQ6ICNGRjVENTYgIWltcG9ydGFudDtcbn1cblxuLnBjb3Mtc2VjdGlvbiB7XG4gIC0tYmFja2dyb3VuZDogI0ZGNUE3RCAhaW1wb3J0YW50O1xufVxuXG4ud2VpZ2h0LWxvc3Mge1xuICAtLWJhY2tncm91bmQ6ICMwMUEzQTQgIWltcG9ydGFudDtcbn1cblxuLm11Y3NsZSB7XG4gIC0tYmFja2dyb3VuZDogIzBCOTRDMSAhaW1wb3J0YW50O1xufVxuXG4uZmF0IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRkQ5ODBGICFpbXBvcnRhbnQ7XG59XG5cbi5wcmVtaXVtLWJ0bntcbiAgLS1iYWNrZ3JvdW5kOiAjZmZmICFpbXBvcnRhbnQ7XG59XG5cbi5wcmVtaXVtLWxvY2t7XG4gIGhlaWdodDogM3ZoO1xuICBwYWRkaW5nLXJpZ2h0OiAycHg7XG59XG5cbmlvbi1idXR0b24ge1xuICAvL2NvbG9yOiByZ2IoMjU1LCAyNTUsIDI1NSk7XG4gIG1heC1oZWlnaHQ6IDEuNzVyZW07XG4gIGZvbnQtc2l6ZTogMC44cmVtO1xuICB3aWR0aDogMTAwcHg7XG5cbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDQwcHg7XG4gIGxlZnQ6IDE4cHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG46Om5nLWRlZXAgLmltbXVuaXR5LXNlY3Rpb24tYm9yZGVyIHtcbiAgLS1ib3JkZXItY29sb3I6ICNGRDlGMzMgIWltcG9ydGFudDtcbiAgLS1jb2xvcjogI0ZEOUYzMyAhaW1wb3J0YW50O1xufVxuXG46Om5nLWRlZXAgLmRldG94LXNlY3Rpb24tYm9yZGVyIHtcbiAgLS1ib3JkZXItY29sb3I6ICM0Q0IyNzEgIWltcG9ydGFudDtcbiAgLS1jb2xvcjogIzRDQjI3MSAhaW1wb3J0YW50O1xufVxuXG46Om5nLWRlZXAgLnBvc3QtY292aWQtc2VjdGlvbi1ib3JkZXIge1xuICAtLWJvcmRlci1jb2xvcjogIzc1NEIyOSAhaW1wb3J0YW50O1xuICAtLWNvbG9yOiAjNzU0QjI5ICFpbXBvcnRhbnQ7XG59XG5cbjo6bmctZGVlcCAud2VpZ2h0LWxvc3MtcGx1cy1zZWN0aW9uLWJvcmRlciB7XG4gIC0tYm9yZGVyLWNvbG9yOiAjMEI5NEMxICFpbXBvcnRhbnQ7XG4gIC0tY29sb3I6ICMwQjk0QzEgIWltcG9ydGFudDtcbn1cblxuOjpuZy1kZWVwIC5kaWFiZXRlcy1zZWN0aW9uLWJvcmRlciB7XG4gIC0tYm9yZGVyLWNvbG9yOiAjRDE0MzIyICFpbXBvcnRhbnQ7XG4gIC0tY29sb3I6ICNEMTQzMjIgIWltcG9ydGFudDtcbn1cblxuOjpuZy1kZWVwIC5wY29zLXNlY3Rpb24tYm9yZGVyIHtcbiAgLS1ib3JkZXItY29sb3I6ICNGRjVBN0QgIWltcG9ydGFudDtcbiAgLS1jb2xvcjogI0ZGNUE3RCAhaW1wb3J0YW50O1xufVxuXG46Om5nLWRlZXAgLm11Y3NsZS1ib3JkZXIge1xuICAtLWJvcmRlci1jb2xvcjogIzBCOTRDMSAhaW1wb3J0YW50O1xuICAtLWNvbG9yOiAjMEI5NEMxICFpbXBvcnRhbnQ7XG59XG5cbjo6bmctZGVlcCAuZmF0LWJvcmRlciB7XG4gIC0tYm9yZGVyLWNvbG9yOiAjRkQ5ODBGICFpbXBvcnRhbnQ7XG4gIC0tY29sb3I6ICNGRDk4MEYgIWltcG9ydGFudDtcbn1cblxuOjpuZy1kZWVwIC5jaG9sZXN0ZXJvbC1zZWN0aW9uLWJvcmRlciB7XG4gIC0tYm9yZGVyLWNvbG9yOiAjQTMxRTc5ICFpbXBvcnRhbnQ7XG4gIC0tY29sb3I6ICNBMzFFNzkgIWltcG9ydGFudDtcbn1cblxuOjpuZy1kZWVwIC5oeXBlcnRlbnNpb24tc2VjdGlvbi1ib3JkZXIge1xuICAtLWJvcmRlci1jb2xvcjogI0ZGNUQ1NiAhaW1wb3J0YW50O1xuICAtLWNvbG9yOiAjRkY1RDU2ICFpbXBvcnRhbnQ7XG59XG5cbjo6bmctZGVlcCAud2VpZ2h0LWxvc3MtYm9yZGVyIHtcbiAgLS1ib3JkZXItY29sb3I6ICMwMUEzQTQgIWltcG9ydGFudDtcbiAgLS1jb2xvcjogIzAxQTNBNCAhaW1wb3J0YW50O1xufVxuXG4ud2hpdGUtYmFja2dyb3VuZCB7XG4gIC0tYmFja2dyb3VuZDogI0ZGRjtcbn1cblxuLnBlcnNvbmFsaXNhdGlvbiB7XG4gIGZvbnQtc2l6ZTogMC44NXJlbTtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xuICBmb250LWZhbWlseTogJGZvbnQ7XG4gIGNvbG9yOiAjNGM1MjY0O1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuXG4ubGVnZW5kLWJvcmRlcntcbiAgaGVpZ2h0OiAxMjVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA5O1xuICB0b3A6IDE0JTtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi5yZWNvbXtcbiAgd2lkdGg6IDEzdmg7XG4gIG1hcmdpbi1sZWZ0OiAxNnB4O1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMHB4O1xuICBsZWZ0OiA1cHg7XG59XG5cbi5zdGFja2VkLWNhcmRze1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8vIHRvcDogMTBwO1xuICAvLyBwYWRkaW5nLXRvcDogMTJweDtcbiAgd2lkdGg6IDEwMCU7XG4gIHRyYW5zaXRpb24tZHVyYXRpb246IDAuM3M7XG4gIC8vIHRyYW5zaXRpb246IHRyYW5zZm9ybSAxLjVzIDBzLCBvcGFjaXR5IDEuMjVzIDBzO1xuICAvLyB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoNTB2dyk7XG59XG5cbi5leHBhbmQtY2FyZHN7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgLy8gdG9wOiAxMHA7XG4gIC8vIHBhZGRpbmctdG9wOiAxMnB4O1xuICB3aWR0aDogMTAwJTtcbiAgdHJhbnNpdGlvbi1kdXJhdGlvbjogMC4zcztcbiAgLy8gdHJhbnNpdGlvbjogdHJhbnNmb3JtIDEuNXMgMHMsIG9wYWNpdHkgMS4yNXMgMHM7XG4gIC8vIHRyYW5zZm9ybTogdHJhbnNsYXRlWSg1MHZ3KTtcbn1cblxuLnNob3ctbGVzcy10eHR7XG4gIGNvbG9yOiAjMTQ5MkU2O1xuICBmb250LXNpemU6IDAuOXJlbTtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vYXNzZXRzL2ZvbnRzL2ZvbnRzLnNjc3NcIjtcblxuLy8gRm9udHNcbiRmb250LXNpemU6IDE2cHg7XG5cbiRmb250OiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhclwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtYm9sZDogXCJndC1hbWVyaWNhLXN0YW5kYXJkLWJvbGRcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiRmb250LWxpZ2h0OiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtbGlnaHRcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiRmb250LW1lZGl1bTogXCJndC1hbWVyaWNhLXN0YW5kYXJkLW1lZGl1bVwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtbWVkaXVtLWV4dGVuZGVkOiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtZXh0ZW5kZWRNZWRpdW1cIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLFxuICBBcmlhbCwgc2Fucy1zZXJpZjtcblxuICAkc2Fucy1mb250LXJlZ3VsYXI6IFwiU291cmNlLVNhbnMtUHJvLXJhZ3VsYXJcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjsiXX0= */";

/***/ }),

/***/ 98664:
/*!***********************************************************************************!*\
  !*** ./src/app/components/add-update-customer/add-update-customer.component.scss ***!
  \***********************************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "ion-item.main {\n  padding: 1rem;\n}\n\nion-radio-group {\n  margin: none;\n}\n\nion-radio {\n  --color:#01A3A4;\n  --color-checked:#01A3A4;\n  margin: 0.1rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkZC11cGRhdGUtY3VzdG9tZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFBO0FBQ0o7O0FBQ0E7RUFDSSxZQUFBO0FBRUo7O0FBQUE7RUFDSSxlQUFBO0VBQ0EsdUJBQUE7RUFDQSxjQUFBO0FBR0oiLCJmaWxlIjoiYWRkLXVwZGF0ZS1jdXN0b21lci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1pdGVtLm1haW57XG4gICAgcGFkZGluZzogMXJlbTtcbn1cbmlvbi1yYWRpby1ncm91cHtcbiAgICBtYXJnaW46IG5vbmU7XG59XG5pb24tcmFkaW97XG4gICAgLS1jb2xvcjojMDFBM0E0O1xuICAgIC0tY29sb3ItY2hlY2tlZDojMDFBM0E0O1xuICAgIG1hcmdpbjogLjFyZW07XG5cbn0iXX0= */";

/***/ }),

/***/ 95425:
/*!***********************************************************************!*\
  !*** ./src/app/components/home-dietplan/home-dietplan.component.scss ***!
  \***********************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJob21lLWRpZXRwbGFuLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 20051:
/*!***********************************************************************!*\
  !*** ./src/app/components/home-kcounter/home-kcounter.component.scss ***!
  \***********************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "@font-face {\n  font-family: gt-america-standard-medium;\n  src: url(\"/assets/fonts/Roboto-Medium.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-regular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-bold;\n  src: url(\"/assets/fonts/Roboto-Bold.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-extendedMedium;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-light;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: Source-Sans-Pro-ragular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n.disable-traker-container {\n  height: 65px;\n  margin-bottom: 5px;\n}\n.font-size75 {\n  font-size: 0.75rem !important;\n  color: #7d7d7d !important;\n}\ncircle-progress svg {\n  box-shadow: 2px 8px 10px #000;\n  border-radius: 50%;\n  width: 140px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2Fzc2V0cy9mb250cy9mb250cy5zY3NzIiwiaG9tZS1rY291bnRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVDQUFBO0VBQ0EsOERBQUE7QUNDRjtBREVBO0VBQ0Usd0NBQUE7RUFDQSwrREFBQTtBQ0FGO0FER0E7RUFDRSxxQ0FBQTtFQUNBLDREQUFBO0FDREY7QURJQTtFQUNFLCtDQUFBO0VBQ0EsNkRBQUE7QUNGRjtBREtBO0VBQ0Usc0NBQUE7RUFDQSw2REFBQTtBQ0hGO0FET0E7RUFDRSxvQ0FBQTtFQUNBLCtEQUFBO0FDTEY7QUFwQkE7RUFDSSxZQUFBO0VBQ0Esa0JBQUE7QUFzQko7QUFwQkU7RUFDRSw2QkFBQTtFQUNBLHlCQUFBO0FBdUJKO0FBbkJBO0VBQ0UsNkJBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUFzQkYiLCJmaWxlIjoiaG9tZS1rY291bnRlci5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1tZWRpdW07XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tTWVkaXVtLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLXJlZ3VsYXI7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tUmVndWxhci50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1ib2xkO1xuICBzcmM6IHVybChcIi9hc3NldHMvZm9udHMvUm9ib3RvLUJvbGQudHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtZXh0ZW5kZWRNZWRpdW07XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tTGlnaHQudHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtbGlnaHQ7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tTGlnaHQudHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogU291cmNlLVNhbnMtUHJvLXJhZ3VsYXI7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tUmVndWxhci50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG4iLCJcbkBpbXBvcnQgXCIuLi8uLi8uLi90aGVtZS9mb250cy5zY3NzXCI7XG5cbi5kaXNhYmxlLXRyYWtlci1jb250YWluZXJ7XG4gICAgaGVpZ2h0OiA2NXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDVweDtcbiAgfVxuICAuZm9udC1zaXplNzV7XG4gICAgZm9udC1zaXplOiAwLjc1cmVtICFpbXBvcnRhbnQ7XG4gICAgY29sb3I6IzdkN2Q3ZCAhaW1wb3J0YW50O1xuICB9XG4gIFxuXG5jaXJjbGUtcHJvZ3Jlc3Mgc3Zne1xuICBib3gtc2hhZG93OiAycHggOHB4IDEwcHggIzAwMDtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICB3aWR0aDogMTQwcHg7XG59XG4iXX0= */";

/***/ }),

/***/ 42685:
/*!***********************************************************************!*\
  !*** ./src/app/components/home-vertical/home-vertical.component.scss ***!
  \***********************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "@charset \"UTF-8\";\n@font-face {\n  font-family: gt-america-standard-medium;\n  src: url(\"/assets/fonts/Roboto-Medium.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-regular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-bold;\n  src: url(\"/assets/fonts/Roboto-Bold.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-extendedMedium;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-light;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: Source-Sans-Pro-ragular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n.blur-it {\n  filter: blur(4px) !important;\n}\nion-content {\n  --background: #F6F7FC !important;\n}\nion-content .--transform {\n  min-height: 250px;\n  border-radius: 1;\n  border-radius: 12%;\n  position: relative;\n  top: 8px;\n}\nion-content .medium-text-all {\n  color: #333;\n  font-size: 0.9rem !important;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  margin: 0;\n  white-space: normal;\n  text-align: left;\n}\nion-content .get-button-icon {\n  color: #fff;\n  background-color: #01a3a4;\n  border: 2px solid #fff;\n  border-radius: 100px;\n  position: fixed;\n  right: 1.2rem;\n  height: 35px;\n  width: 35px;\n  z-index: 11;\n  top: 4rem;\n}\n.--manjhari-vid {\n  text-align: center;\n}\n.--manjhari-vid .card {\n  width: 90%;\n  display: inline-block;\n  min-height: 120px;\n  border-radius: 25px;\n  background: #fff;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1);\n}\n.--manjhari-vid .card .left .content {\n  color: #565656;\n  font-size: 0.87rem;\n  left: 1rem;\n  justify-content: left;\n  padding-top: 2px;\n}\n.--manjhari-vid .card .right img {\n  border-top-right-radius: 25px;\n  border-bottom-right-radius: 25px;\n  width: 100%;\n  min-height: 200px;\n  margin-left: 1px;\n  margin-bottom: -4px;\n  height: 200px;\n}\n.center-percent-fasting {\n  width: 50%;\n  height: 50%;\n  border-radius: 50%;\n  border: 1x solid #b3b3b3;\n  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);\n  display: inline-block;\n  top: 26%;\n  position: absolute;\n  left: 24%;\n  padding-top: 8%;\n}\n.refresh-lock {\n  position: absolute;\n  right: 1px;\n  width: 14px;\n  background: transparent;\n  opacity: 0.7;\n}\na.moreless {\n  color: #1492E6 !important;\n}\n.custom-red {\n  color: #E74C3C;\n}\n.custom-orange {\n  color: orange;\n}\n.custom-background-fff {\n  background: #fff;\n  --border: #fff;\n  --border-color: #fff;\n}\n.custom-color-other {\n  color: #007881 !important;\n}\n::ng-deep .custom-link-color {\n  color: #1492E6 !important;\n}\n::ng-deep .loading-noti-msg {\n  height: 30px;\n  position: absolute;\n  left: 0px;\n  top: 12px;\n}\n.lock-premium-color {\n  color: #DEB221 !important;\n}\n.custom-color-btn-white {\n  color: #FFF !important;\n  font-size: 1rem;\n}\n.water-center-percent {\n  width: 75%;\n  height: 75%;\n  z-index: 14;\n  background: #fff;\n  left: 12%;\n  top: 14%;\n  padding-top: 25%;\n  /* box-shadow: 0 3px 6px rgb(0 0 0 / 16%); */\n  display: inline-block;\n  border-radius: 50%;\n  position: absolute;\n  color: #1492E6;\n}\n.center-percent {\n  width: 42%;\n  height: 42%;\n  border-radius: 50%;\n  border: 1x solid #b3b3b3;\n  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16);\n  display: inline-block;\n  top: 27%;\n  position: absolute;\n  left: 29%;\n  padding-top: 8%;\n}\n.box-activity {\n  max-width: 260px;\n  width: 154px;\n  height: 125px;\n  position: absolute;\n  margin-top: 30%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  border-radius: 100%;\n  overflow: hidden;\n}\n.box-fasting {\n  max-width: 150px;\n  width: 112px;\n  /* height: 115px; */\n  /* position: absolute; */\n  /* margin-top: 12%; */\n  /* left: 50%; */\n  /* transform: translate(-50%, -50%); */\n  border-radius: 100%;\n  margin: auto;\n  /* overflow: hidden; */\n}\n.box-weight {\n  max-width: 150px;\n  width: 115px;\n  height: 115px;\n  position: absolute;\n  margin-top: 12%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  border-radius: 100%;\n  overflow: hidden;\n}\n.absolute-pos {\n  position: absolute;\n  width: 100%;\n  bottom: 10px;\n}\n.weight-small {\n  color: #707070;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.weight-small-diff {\n  color: #01a3a4;\n  font-size: 0.875rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.margin-top-21per {\n  margin-top: 42% !important;\n}\n.padding-0 {\n  padding: 0;\n}\n.padding-10 {\n  padding: 10px;\n}\n.custom-height-width {\n  height: 480px !important;\n  width: 100%;\n  max-height: 1000px;\n}\n.custom-height-width-430 {\n  height: 508px !important;\n  width: 100%;\n  max-height: 1000px;\n}\n.responsive-box {\n  position: relative;\n  width: 100%;\n}\n.responsive-box:after {\n  content: \"\";\n  display: block;\n  padding-bottom: 100%;\n}\n.ion-no-padding {\n  padding-left: 0;\n}\n.content {\n  position: absolute;\n  width: 98%;\n  height: 96%;\n  bottom: 0;\n}\n.title {\n  font-size: 0.875rem !important;\n  color: #3580F2;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif !important;\n}\n.title-recipe {\n  padding-top: 10px;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif !important;\n  font-size: 0.95rem !important;\n  padding-left: 0 !important;\n}\n.title-big {\n  font-size: 1.2rem !important;\n  color: #3580F2;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif !important;\n}\n.custom-height-width-155 {\n  height: 155px !important;\n  width: 155px !important;\n  max-height: 1000px;\n}\n.fire {\n  width: 10%;\n  display: inline-block;\n  height: auto;\n  margin-right: 0.2rem;\n}\n.height-410 {\n  height: 410px !important;\n}\n.max-width-200 {\n  width: 248px !important;\n  height: 248px !important;\n  margin-top: 35% !important;\n}\n.pic-food-heart {\n  width: 15px !important;\n  height: auto !important;\n  position: absolute;\n  left: 60px;\n  top: 4px;\n}\n::ng-deep .pic-food-heart-msg {\n  width: 15px !important;\n  height: auto !important;\n  left: 60px;\n  top: 4px;\n}\n.pic-food-heart-healthy {\n  width: 16px !important;\n  margin-right: 5px;\n  margin-top: 10px;\n  height: 15px !important;\n}\n.height-345 {\n  height: 508px;\n  max-height: 1000px !important;\n}\n.bottom-25 {\n  bottom: 25px !important;\n  width: 95%;\n}\n.top-16 {\n  top: 16%;\n}\n.top-19 {\n  top: 19%;\n}\n.height-453 {\n  height: 535px;\n}\n.height-369 {\n  height: 535px !important;\n}\n.height-452 {\n  height: 535px !important;\n}\n.activity-fasting-section {\n  background: #F6F7FC;\n  padding: 0 0.5rem 0 0.5rem;\n}\n.activity-fasting-section .activity {\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.16);\n}\n.activity-fasting-section .activity .activity-header {\n  padding-top: 8px;\n}\n.activity-fasting-section .activity .activity-header h2 {\n  margin: 0 !important;\n  color: #14CEB7;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.activity-fasting-section .activity .activity-header .steps span {\n  color: #778CA3;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.activity-fasting-section .activity .activity-header .steps ion-icon {\n  color: #778CA3;\n  font-size: 0.8rem;\n}\n.activity-fasting-section .activity ion-card-content .footer {\n  position: absolute;\n  width: 100%;\n  bottom: 0;\n}\n.activity-fasting-section .fasting {\n  width: 99%;\n  margin-left: 1%;\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.16);\n}\n.activity-fasting-section .fasting .fasting-header {\n  padding-top: 8px;\n}\n.activity-fasting-section .fasting .fasting-header h2 {\n  margin: 0 !important;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.activity-fasting-section .fasting .fasting-header .remaining {\n  font-size: 0.7rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.deficit-section {\n  background: #F6F7FC;\n  padding-top: 20px;\n}\n.keymantra-section {\n  background: #F6F7FC;\n}\n.water-weight-section {\n  background: #F6F7FC;\n  padding: 0.5rem 0.5rem 0 0.5rem;\n}\n.water-weight-section .water {\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.16);\n}\n.water-weight-section .water .water-header {\n  padding-top: 7px;\n}\n.water-weight-section .water .water-header h2 {\n  margin: 0 !important;\n  color: #3498DB;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.water-weight-section .water .water-header .steps span {\n  color: #778CA3;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.water-weight-section .water .water-header .steps ion-icon {\n  color: #778CA3;\n  font-size: 0.8rem;\n}\n.water-weight-section .water ion-card-content .footer {\n  position: absolute;\n  width: 100%;\n  bottom: 0;\n}\n.water-weight-section .weight {\n  width: 99%;\n  margin-left: 1%;\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.16);\n}\n.water-weight-section .weight .weight-header {\n  padding-top: 8px;\n  padding-left: 0;\n  padding-right: 10px;\n}\n.water-weight-section .weight .weight-header h2 {\n  margin: 0 !important;\n  color: #01A3A4;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.water-weight-section .weight .weight-header .weight-color {\n  color: #01A3A4;\n}\n.google-fit .round_progress_bar {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.google-fit .heart_points .cals_txt,\n.google-fit .steps .steps_txt {\n  font-weight: bold;\n}\n.google-fit .heart_points span,\n.google-fit .steps span {\n  font-size: 18px;\n}\n.google-fit .steps ion-icon {\n  font-size: 20px;\n  color: #164EA8;\n  position: relative;\n  top: 3px;\n}\n.google-fit .stats_row h5 {\n  color: #1A73F7 !important;\n  font-size: 27px;\n}\n.google-fit .stats_row {\n  margin-top: 15px;\n}\n.google-fit .inner_rp_bar {\n  position: relative;\n  top: -200px;\n  margin: auto;\n}\n.google-fit .counts_blk {\n  position: absolute;\n  top: 22%;\n  left: 0;\n  right: 0;\n  z-index: 999;\n}\n.google-fit .counts_blk h3 {\n  font-weight: bold;\n  font-size: 25px;\n  color: #fbc030;\n}\n.google-fit .response_txt {\n  color: #fbc030;\n}\n.google-fit .counts_blk h4 {\n  font-weight: bold;\n  font-size: 20px;\n  color: #164EA8;\n}\n.google-fit .activate_fit_btn {\n  margin-top: 35px;\n  --color: red;\n  --background-activated: none;\n  --background-activated-opacity: none;\n  --background-focused: none;\n  font-weight: bold;\n}\n.google-fit .cal_image {\n  width: 20px;\n  margin: 0px 6px;\n  top: 3px;\n  position: relative;\n}\n.google-fit .counts_blk .cal_image {\n  width: 20px;\n  top: 0px;\n  margin: 0px;\n  position: relative;\n  border-radius: 25px;\n}\n.google-fit ion-text {\n  margin: auto;\n}\n.google-fit .refresh_btn {\n  --padding-top: 27px;\n  --padding-bottom: 27px;\n  --padding-start: 18px;\n  --padding-end: 17px;\n  margin-top: 20px;\n}\n.google-fit ion-button .reload_image {\n  transform: rotate(0deg);\n  transition: transform 0.2s linear;\n}\n.google-fit ion-button.ion-activated .reload_image {\n  transform: rotate(360deg);\n  transition: transform 0.2s linear;\n}\n.google-fit .googlefit_img {\n  width: 100px;\n  margin: 90px;\n}\n.google-fit .reload_image {\n  width: 40px;\n  margin-top: 5px;\n}\n.custon-height {\n  height: 450px;\n}\n.height-660 {\n  height: 570px;\n  margin-bottom: -1.2rem;\n}\n.height-266 {\n  height: 266px !important;\n}\n.height-385 {\n  height: 385px;\n}\n.height-516 {\n  height: 516px !important;\n  margin-bottom: -0.7rem;\n}\n.height-614 {\n  height: 675px !important;\n}\n.width-100 {\n  width: 100% !important;\n}\n.height-500 {\n  height: 516px !important;\n}\n.height-350 {\n  height: 350px !important;\n  width: 100%;\n  margin-bottom: 0.5rem;\n}\n.graph-color-green {\n  color: #a1bf26 !important;\n}\n.blue-color {\n  color: #00A86B;\n}\n.pink-color {\n  color: #F8719F;\n}\n.message-red {\n  width: 100%;\n  background: #fff;\n  color: #E74C3C;\n  text-align: center;\n  display: block !important;\n  font-size: 1rem;\n  padding: 6px;\n  font-family: \"gt-america-standard-medium\", \"Helvetica Neue\", Arial, sans-serif;\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.16);\n}\n.message {\n  font-size: 1.3rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #fff;\n}\n.overlay {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  content: \"\";\n  background: #000;\n  opacity: 0.8;\n  z-index: 99990;\n}\n.event-container {\n  position: fixed;\n  width: 80%;\n  bottom: 60px;\n}\n.custom-info-margin {\n  margin-left: 0.5rem !important;\n  color: #b7092e;\n}\n.inner-message {\n  position: fixed;\n  width: 83%;\n  height: 70%;\n  z-index: 99990;\n  bottom: 5%;\n  padding-bottom: 10%;\n  text-align: center;\n  left: 10%;\n}\n.circle-white {\n  border: 1px solid #E74C3C;\n  padding: 6px;\n  border-radius: 22px;\n  margin-left: 0.5rem;\n  top: -8px;\n  position: relative;\n}\n.food {\n  font-weight: 600;\n}\n.circle-head {\n  background: #ff0036;\n  padding: 6px;\n  border-radius: 22px;\n}\n.circle-amber-head {\n  background: #FFC200;\n  padding: 4px 7px;\n  border-radius: 22px;\n  margin-left: 1rem;\n  top: -8px;\n  position: relative;\n}\n.like-strait {\n  width: 15px;\n  vertical-align: middle;\n  top: -2px;\n  position: relative;\n}\n.circle-amber {\n  background: #FFC200;\n  padding: 6px;\n  border-radius: 22px;\n}\n.circle-green {\n  background: #a1bf26;\n  padding: 6px;\n  border-radius: 22px;\n}\n.dislike {\n  width: 18px;\n  /* display: inline-block; */\n  vertical-align: middle;\n  top: 0px;\n  position: relative;\n}\n.like {\n  width: 18px;\n  /* display: inline-block; */\n  vertical-align: middle;\n  top: -2px;\n  position: relative;\n}\nion-menu-button {\n  --color: #fff;\n}\n.close-circle {\n  width: 1.4rem;\n  position: absolute;\n  top: 0;\n  right: 0;\n  margin: 0;\n  color: #b7092e;\n}\n.habbit-label {\n  font-size: 1.1rem;\n  color: #333 !important;\n  white-space: normal;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.habbit-arrow {\n  width: 2.5rem;\n  height: auto;\n  left: -27px;\n  position: relative;\n  padding: 10px;\n}\n.habbit-prog-container {\n  width: 83%;\n  height: 5px;\n  border-radius: 5px;\n  background: gray;\n  position: absolute;\n  bottom: 25px;\n  left: 0;\n}\n.habbit-prog-label {\n  color: #5D5D5D;\n  font-size: 0.9rem !important;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  position: relative;\n  left: 0;\n  padding-bottom: 3rem;\n  padding-top: 1rem;\n  width: 98%;\n}\n.habbit-prog-label div {\n  font-size: 1rem;\n}\n.key-mantra {\n  font-size: 1.25rem !important;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #01A3A4;\n}\n.other-meal {\n  cursor: pointer;\n}\n.add-habbits {\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.75rem;\n  cursor: pointer;\n  color: #707070;\n}\n.add-more {\n  vertical-align: middle;\n  width: auto;\n  height: 1rem;\n}\n.custom-height-scroll ion-item {\n  margin-top: -0.2rem;\n}\n.custom-height-scroll ion-item .item-inner {\n  padding-inline-end: 0 !important;\n}\n.custom-min-height {\n  min-height: 129px;\n}\n.button-cancel {\n  font-size: 0.875rem !important;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  border-color: #fff !important;\n}\n.button-fit {\n  font-size: 0.875rem !important;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.lock {\n  width: 15px;\n  position: absolute;\n  bottom: 2px;\n  right: 14%;\n  top: 10px;\n}\n.habbit {\n  width: 100%;\n  margin-left: 0;\n  margin-right: 0;\n  display: inline-block;\n  margin-top: 5px;\n  margin-bottom: 5px;\n  box-shadow: none;\n  box-shadow: 0.2rem 0.2rem 0.5rem #b3b3b3;\n}\n.habbit-prog-percent {\n  font-size: 0.75rem;\n  font-weight: bold;\n  color: #14ceb7;\n  bottom: 25px;\n  position: absolute;\n  right: 17px;\n  bottom: 22px;\n}\n.habbit-progress {\n  height: 4px;\n  border-radius: 5px;\n}\n.color-green {\n  background: #a1bf26;\n}\n.color-green-text {\n  color: #a1bf26;\n}\n.color-red {\n  background: #01A3A4;\n}\n.color-red-text {\n  color: #01A3A4;\n}\n.color-yellow {\n  background: #ff8c46;\n}\n.color-yellow-text {\n  color: #ff8c46;\n}\n.medium-text {\n  color: #333;\n  font-size: 0.9rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  margin: 0;\n  white-space: nowrap;\n}\n.medium-text .light-text {\n  color: #979797;\n}\n.custom-padding-right {\n  padding-right: 1.2rem;\n}\n.fasting .collaps-view h3 {\n  font-size: 1rem;\n  color: #F8719F;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-weight: 500;\n}\n.fasting .collaps-view h2 {\n  font-size: 1.4rem;\n  color: #F8719F;\n  font-weight: 500;\n}\n.fasting .collaps-view h5 {\n  font-size: 1rem;\n  color: #707070;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.fasting .expended-view .start-time {\n  border-right: 1px solid #b3b3b3;\n}\n.fasting .expended-view .start-time span:first-child {\n  color: #333333;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1.3rem;\n}\n.fasting .expended-view .start-time span:last-child {\n  color: #333333;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.8rem;\n}\n.fasting .expended-view .end-time span:first-child {\n  color: #333333;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1.3rem;\n}\n.fasting .expended-view .end-time span:last-child {\n  color: #333333;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.8rem;\n}\n.fasting .expended-view .middle-time {\n  border-right: 1px solid #b3b3b3;\n}\n.fasting .expended-view .middle-time span:first-child {\n  color: #F8719F;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1.5rem;\n}\n.fasting .expended-view .middle-time span:last-child {\n  color: #333333;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.8rem;\n}\n.fasting .expended-view .fasting-bottom span:first-child {\n  color: #333333;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.8rem;\n}\n.fasting .expended-view .fasting-bottom span:last-child {\n  color: #F8719F;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1rem;\n}\n.water .collaps-view {\n  padding-top: 0;\n}\n.water .collaps-view h3 {\n  font-size: 0.9rem;\n  color: #3498DB;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-weight: 500;\n}\n.water .collaps-view h2 {\n  font-size: 1.1rem;\n  color: #F8719F;\n  font-weight: 500;\n}\n.water .collaps-view h5 {\n  font-size: 0.6rem;\n  color: #707070;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.water .expended-view .start-time {\n  border-right: 1px solid #b3b3b3;\n}\n.water .expended-view .start-time span:first-child {\n  color: #707070;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1.5rem;\n}\n.water .expended-view .start-time span:last-child {\n  color: #333333;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.8rem;\n}\n.water .expended-view .end-time span:first-child {\n  color: #707070;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1.5rem;\n}\n.water .expended-view .end-time span:last-child {\n  color: #333333;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.8rem;\n}\n.water .expended-view .middle-time {\n  border-right: 1px solid #b3b3b3;\n}\n.water .expended-view .middle-time span:first-child {\n  color: #F8719F;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1.5rem;\n}\n.water .expended-view .middle-time span:last-child {\n  color: #5f5656;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.8rem;\n}\n.water .expended-view .fasting-bottom span:first-child {\n  color: #707070;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1.5rem;\n}\n.water .expended-view .fasting-bottom span:last-child {\n  color: #F8719F;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.8rem;\n}\n.small-box {\n  margin-left: 1rem;\n  margin-right: 1rem;\n  width: 100%;\n  box-shadow: 0.2rem 0.2rem 0.5rem #b3b3b3;\n  top: 0;\n  padding: 0.5rem;\n}\n.parent-color {\n  background: #F6F7FC !important;\n  border-bottom-right-radius: 7rem;\n  height: 400px;\n}\n.item-text {\n  color: #555555;\n  font-size: 0.6rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  margin: 0;\n  white-space: normal;\n  width: 58px;\n  white-space: nowrap;\n}\n.profile {\n  width: 3rem;\n  height: 3rem;\n  background: #fff;\n  border-radius: 50%;\n  margin-right: 0.5rem;\n  margin-left: 0.5rem;\n}\n.profile img {\n  border-radius: 25px;\n}\n.profile-small {\n  color: #fff;\n  font-size: 0.9rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  margin: 0;\n  position: relative;\n  top: 3px;\n}\n.profile-big {\n  color: #fff;\n  font-size: 1rem;\n  font-family: \"gt-america-standard-medium\", \"Helvetica Neue\", Arial, sans-serif;\n  margin: 0;\n}\nion-list {\n  padding-left: 1rem;\n}\nion-list ion-item {\n  margin-bottom: 0.5rem;\n}\nion-label {\n  white-space: normal;\n}\nion-thumbnail {\n  display: inline-block;\n  margin: 0.5rem 0;\n  margin-right: 0.6rem;\n  width: 68px;\n  height: 58px;\n}\nion-thumbnail img {\n  border-radius: 12px;\n  height: 100%;\n  width: 100%;\n}\n.margin-right-1 {\n  margin-right: 1rem !important;\n}\n.custom-border-bottom {\n  border-bottom: 1px solid #b5b5b5;\n}\n.circle {\n  height: 4rem;\n  width: 4rem;\n  background: #fff;\n  border-radius: 50%;\n  display: inline-block;\n}\n.circle img {\n  height: auto;\n  width: 35% !important;\n  vertical-align: middle;\n  display: inline-block;\n  top: 32%;\n  position: relative;\n}\n.small-text {\n  color: #555555;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  margin: 0;\n}\n.small-text1 {\n  color: #565656 !important;\n  font-size: 0.8rem !important;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif !important;\n  margin: 0 !important;\n  margin-top: 0.5rem !important;\n}\n.big-text {\n  color: #01A3A4;\n  font-size: 1.2rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  margin: 0;\n}\n.padding-left-right {\n  padding-left: 1rem !important;\n  padding-right: 1rem !important;\n}\n.border-bottom {\n  border-bottom: 1px solid #f1f1f1;\n}\n.graph {\n  text-align: center;\n  position: relative;\n  left: 2px;\n  width: 98%;\n  margin-top: 0.5rem;\n}\n.graph-label {\n  color: #555555;\n  font-size: 0.75rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  margin: 0;\n}\n.graph-number {\n  color: #555555;\n  font-size: 1.1rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.custom-margin-left-right {\n  margin-left: 1rem;\n  margin-right: 0.6rem;\n}\n.food-name {\n  color: #333;\n}\n.food-name-container {\n  display: -webkit-box;\n  -webkit-line-clamp: 2;\n  -webkit-box-orient: vertical;\n  overflow: hidden;\n}\n.x-button {\n  width: 100%;\n  height: 33px;\n  font-weight: bold;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  text-align: center;\n  color: #01A3A4;\n  padding-top: 5px;\n}\n.x-button-active-2 {\n  width: 100%;\n  height: 33px;\n  vertical-align: middle;\n  border-top-right-radius: 1rem;\n  border-bottom-left-radius: 1rem;\n  text-align: center;\n  padding-top: 5px;\n  background: #F6F7FC;\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.16);\n  color: #141414;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.x-button-active-1 {\n  width: 100%;\n  height: 33px;\n  vertical-align: middle;\n  border-top-left-radius: 1rem;\n  border-bottom-right-radius: 1rem;\n  text-align: center;\n  padding-top: 5px;\n  background: #F6F7FC;\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.16);\n  color: #141414;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.bar-2 {\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  float: right;\n  color: gray;\n}\n.bar-bar .bar-parent {\n  float: left;\n  font-size: 0.7rem;\n  width: 100%;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.bar-bar .bar-parent .bar-1 {\n  float: left;\n  color: #707070;\n}\n.bar-bar .habbit-prog-container {\n  width: 100%;\n  border-radius: 5px;\n  background: antiquewhite;\n  margin-left: 2rem;\n}\n.bar-bar .habbit-prog-container iframe {\n  width: 100%;\n  left: -32px;\n  position: relative;\n  height: 235px;\n}\n.bar-bar .bar-child-container {\n  width: 100%;\n  background: gray;\n  border-radius: 10px;\n  height: 5px;\n  float: left;\n  margin-top: 5px;\n  margin-bottom: 5px;\n}\n.bar-bar .bar-child-container .fill-amber {\n  background: #FFC200;\n  border-radius: 25px;\n  height: 5px;\n}\n.bar-bar .bar-child-container .fill-red {\n  background: #E74C3C;\n  border-radius: 25px;\n  height: 5px;\n}\n.bar-bar .bar-child-container .fill-green {\n  background: #01A3A4;\n  border-radius: 25px;\n  height: 5px;\n}\n.bar-bar .bar-child-container .bar-fill-c {\n  background: #f2f857;\n  border-radius: 25px;\n  height: 5px;\n}\n.bar-bar .bar-child-container .bar-fill-p {\n  background: #7de2ef;\n  border-radius: 25px;\n  height: 5px;\n}\n.bar-bar .bar-child-container .bar-fill-f {\n  background: #ad8efd;\n  border-radius: 25px;\n  height: 5px;\n}\n.bar-bar .fill-red-p {\n  color: #E74C3C;\n  float: left;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.bar-bar .fill-amber-p {\n  color: #FFC200;\n  float: left;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.bar-bar .fill-green-p {\n  color: #01A3A4;\n  float: left;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.bar-bar .bar-percent-c {\n  float: left;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #f2f857;\n}\n.bar-bar .bar-percent-p {\n  float: left;\n  font-size: 0.8rem;\n  color: #7de2ef;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.bar-bar .bar-percent-f {\n  float: left;\n  font-size: 0.8rem;\n  color: #ad8efd;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.calories-total {\n  padding-left: 10px;\n  padding-right: 10px;\n}\nion-card {\n  width: 90%;\n  margin-left: 5%;\n  margin-right: 0;\n  box-shadow: none;\n  min-height: 100%;\n  margin-top: 10px;\n  display: inline-block;\n  border-radius: 15px;\n}\nion-card ion-card-header {\n  font-size: 0.875rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\nion-card ion-card-header ion-row {\n  min-height: 3rem;\n  max-height: 3rem;\n}\nion-card ion-card-header ion-row img {\n  vertical-align: middle;\n  display: inline-block;\n}\nion-card ion-card-header ion-row span.title {\n  font-weight: bold;\n  font-size: 16px !important;\n  vertical-align: middle;\n  display: inline-block;\n  padding-left: 5px;\n  text-transform: capitalize;\n}\nion-card ion-card-header ion-row span.title-activity {\n  color: #14CEB7;\n}\nion-card ion-card-content {\n  font-size: 0.875rem;\n  padding-left: 0px;\n  padding-right: 5px;\n  padding-bottom: 5px;\n}\nion-card ion-card-content h2 {\n  margin-left: 0.5rem;\n}\nion-card .card-content {\n  padding-bottom: 28px;\n}\nion-card.weight-graph {\n  box-shadow: 0.2rem 0.2rem 0.5rem #b3b3b3;\n}\nion-card.weight-graph ion-card-content {\n  margin-right: 0 !important;\n  padding-right: 0 !important;\n}\n.second-section {\n  background: #F6F7FC;\n  padding-left: 1rem;\n  padding-right: 1rem;\n}\n.second-section div h2 {\n  font-size: 0.875rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #555555;\n  margin-bottom: 0;\n}\n.second-section div p {\n  font-size: 0.875rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #b3b3b3;\n}\n.second-section img {\n  height: auto;\n  width: 50% !important;\n}\n.custom-color {\n  color: #ffa713;\n}\n.overlay {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  content: \"\";\n  background: #000;\n  opacity: 0.8;\n  z-index: 99990;\n  top: 0;\n}\n.parent {\n  margin: 1.5rem 1.5rem;\n  background: #fff;\n  border-radius: 1rem;\n  position: absolute;\n  z-index: 100000;\n  top: 7px;\n  min-height: 516px;\n  padding: 15px;\n}\n.key-mantra-info .custom-scroll,\n.other-meal-info .custom-scroll {\n  overflow-y: scroll;\n  height: 520px;\n}\n.key-mantra-info .overlay,\n.other-meal-info .overlay {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  content: \"\";\n  background: #000;\n  opacity: 0.8;\n  z-index: 99990;\n  top: 0;\n}\n.key-mantra-info .p,\n.other-meal-info .p {\n  color: #555555;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.key-mantra-info .parent,\n.other-meal-info .parent {\n  margin: 1.5rem 1.5rem;\n  background: #fff;\n  border-radius: 1rem;\n  position: absolute;\n  z-index: 100000;\n  top: 7px;\n  min-height: 516px;\n  padding: 15px;\n}\n.key-mantra-info ion-button,\n.other-meal-info ion-button {\n  margin-left: 1rem;\n  margin-right: 1rem;\n  position: absolute;\n  right: -26px;\n  top: -11px;\n}\n.key-mantra-info .custom-scroll,\n.other-meal-info .custom-scroll {\n  overflow-y: scroll;\n  height: 520px;\n}\n.key-mantra-info .overlay,\n.other-meal-info .overlay {\n  position: fixed;\n  width: 100%;\n  height: 78px;\n  content: \"\";\n  background: #000;\n  opacity: 0.8;\n  z-index: 99990;\n  top: auto;\n  bottom: 55px;\n}\n.key-mantra-info .p,\n.other-meal-info .p {\n  color: #555555;\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.key-mantra-info .parent,\n.other-meal-info .parent {\n  margin: 1.5rem 1.5rem;\n  background: #fff;\n  border-radius: 1rem;\n  position: absolute;\n  z-index: 100000;\n  top: 7px;\n  min-height: 516px;\n  padding: 15px;\n}\n.key-mantra-info ion-button,\n.other-meal-info ion-button {\n  margin-left: 1rem;\n  margin-right: 1rem;\n  position: absolute;\n  right: -26px;\n  top: -11px;\n  width: 30px;\n}\n.bg_color {\n  background-color: #f9f6fe !important;\n  --background: #f9f6fe !important;\n}\n.progress_wrapper {\n  margin: 0 auto;\n  height: 237px;\n  width: 250px;\n  position: relative;\n  margin-top: 10px;\n}\n#progress {\n  width: 275px;\n  height: 275px;\n  position: absolute;\n  left: 50%;\n  transform: translateX(-50%);\n  z-index: 1000;\n}\n.circle {\n  background-size: cover;\n  height: 240px;\n  width: 240px;\n  border-radius: 50%;\n  position: absolute;\n  left: 50%;\n  transform: translateX(-50%);\n  top: 20px;\n}\n.horizontal_center {\n  margin-left: 50%;\n  transform: translateX(-50%);\n}\n.alarm {\n  position: absolute;\n  top: 25px;\n  font-size: 30px;\n  color: grey;\n  z-index: 10000;\n  transition: transform ease-in 0.1s, box-shadow ease-in 0.25s;\n  left: 0;\n}\n.alarm:before, .alarm:after {\n  transition: all ease-in-out 0.5s;\n}\n.alarm:active {\n  font-size: 25px;\n}\n.glass {\n  position: absolute;\n  bottom: 0px;\n  width: 35px;\n  height: 45px;\n  padding: 0;\n  z-index: 10000;\n  outline: none !important;\n  transition: all ease-in-out 0.3s;\n  left: 0;\n}\n.volume {\n  position: absolute;\n  bottom: 30px;\n  left: 51%;\n  transform: translateX(-50%);\n  z-index: 1000000;\n  font-size: 12px;\n  color: grey;\n  transition: all ease-in-out 0.3s;\n}\n.glass_container {\n  transition: transform ease-in 0.1s, box-shadow ease-in 0.25s;\n}\n.glass_container:before, .glass_container:after {\n  transition: all ease-in-out 1s;\n}\n.glass_container:active .glass {\n  width: 33px;\n}\n.glass_container:active .volume {\n  font-size: 10px;\n}\n.center_text {\n  position: absolute;\n  top: 30%;\n  font-size: 30px;\n}\n/* General CSS Setup */\nbody {\n  background-color: lightblue;\n  font-family: \"Ubuntu-Italic\", \"Lucida Sans\", helvetica, sans;\n}\n/* container */\n.container {\n  padding: 5% 5%;\n}\n/* CSS talk bubble */\n.more_tip_container {\n  display: inline-flex;\n}\n.tip {\n  height: 65px;\n  align-self: center;\n}\n.more_tip {\n  height: 33px;\n  align-self: center;\n}\n.talktext {\n  font-size: 13px;\n  font-weight: 400;\n}\n.more_text {\n  font-size: 12px;\n  font-weight: bold;\n  color: #509bed;\n  margin: 0;\n}\n.more_tip_box {\n  align-self: center;\n  text-align: center;\n}\n.dry {\n  position: absolute;\n  left: 0;\n  width: 32px;\n  top: 51%;\n  margin-left: -15px;\n  height: 30px;\n  width: 30px;\n}\n.drop {\n  position: absolute;\n  right: 0;\n  top: 51%;\n  margin-right: -15px;\n  height: 30px;\n  width: 30px;\n}\n.confirm {\n  font-size: 10px;\n  color: grey;\n  text-align: center;\n  margin: 0;\n  margin-top: -10px;\n}\n.ion_container {\n  position: absolute;\n  top: 70%;\n  transform: translateY(-50%);\n}\n.hydrated {\n  align-self: center;\n}\n.volume_text {\n  font-size: 5px !important;\n}\n@keyframes slide-up {\n  from {\n    opacity: 1;\n    line-height: 300px;\n  }\n  to {\n    opacity: 0;\n    line-height: 25px;\n    visibility: hidden;\n  }\n}\n.slide-up {\n  animation: slide-up 4s;\n}\n.custom_toast {\n  position: absolute;\n  top: -30%;\n  left: 50%;\n  transform: translateX(-50%);\n  font-size: 15px;\n  opacity: 0;\n  color: #509bed;\n  width: 200px;\n  font-weight: bolder;\n}\n.fade-in {\n  animation: fadeIn ease 10s;\n  -webkit-animation: fadeIn ease 10s;\n  -moz-animation: fadeIn ease 10s;\n  -o-animation: fadeIn ease 10s;\n  -ms-animation: fadeIn ease 10s;\n}\n@keyframes fadeIn {\n  0% {\n    opacity: 0;\n  }\n  100% {\n    opacity: 1;\n  }\n}\n.round_progress_bar {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.heart_points .cals_txt,\n.steps .steps_txt {\n  font-weight: bold;\n}\n.heart_points span,\n.steps span {\n  font-size: 16px;\n}\n.steps ion-icon {\n  font-size: 20px;\n  color: #164EA8;\n  position: relative;\n  top: 3px;\n}\n.stats_row h5 {\n  color: #1A73F7 !important;\n  font-size: 27px;\n}\n.stats_row {\n  margin-top: 15px;\n}\n.inner_rp_bar {\n  position: relative;\n  top: -200px;\n  margin: auto;\n}\n.counts_blk {\n  position: absolute;\n  top: 13%;\n  left: 0;\n  right: 0;\n  z-index: 999;\n}\n.counts_blk h3 {\n  font-weight: bold;\n  font-size: 25px;\n  color: #fbc030;\n}\n.response_txt {\n  color: #fbc030;\n}\n.counts_blk h4 {\n  font-weight: bold;\n  font-size: 20px;\n  color: #164EA8;\n}\n.activate_fit_btn {\n  margin-top: 35px;\n  --color: red;\n  --background-activated: none;\n  --background-activated-opacity: none;\n  --background-focused: none;\n  font-weight: bold;\n}\n.cal_image {\n  width: 20px;\n  margin: 0px 6px;\n  top: 3px;\n  position: relative;\n}\n.counts_blk .cal_image {\n  width: 20px;\n  top: 0px;\n  margin: 0px;\n  position: relative;\n  border-radius: 25px;\n}\nion-text {\n  margin: auto;\n}\n.refresh_btn {\n  --padding-top: 27px;\n  --padding-bottom: 27px;\n  --padding-start: 18px;\n  --padding-end: 17px;\n  margin-top: 20px;\n}\nion-button .reload_image {\n  transform: rotate(0deg);\n  transition: transform 0.2s linear;\n}\nion-button.ion-activated .reload_image {\n  transform: rotate(360deg);\n  transition: transform 0.2s linear;\n}\n.googlefit_img {\n  width: 100px;\n  margin: 90px;\n}\n.reload_image {\n  width: 40px;\n  margin-top: 5px;\n}\n.status_title {\n  font-size: 23px;\n  font-weight: bold;\n  white-space: break-spaces;\n}\n.day {\n  font-size: 20px;\n  font-weight: bold;\n  color: #355bb7;\n}\n.consumed {\n  text-align: left;\n  font-size: 16px;\n}\n.consumed_col {\n  text-align: left;\n}\n.consumed_value {\n  text-align: left;\n  font-size: 16px;\n  color: white;\n}\n.consumed_value_col {\n  background-color: #e62e25;\n  text-align: left;\n  padding: 10px;\n  align-self: center;\n}\n.consumed_row {\n  border-top: 2px solid #5778c5;\n  margin-top: 10px;\n  padding-top: 10px;\n}\n.loss_value_col {\n  background-color: #46a640;\n  text-align: left;\n  padding: 10px;\n  align-self: center;\n}\n.loss_row {\n  margin-top: 20px;\n}\n.result {\n  margin-top: 20px;\n  color: #9f9a9b;\n  font-size: 20px;\n  text-align: left;\n  font-style: italic;\n}\n.calories_card {\n  max-height: unset;\n  padding: 10px;\n  width: 90%;\n}\n.target {\n  font-size: 18px;\n  font-weight: bold;\n  color: #9f9a9b;\n}\n.water_card {\n  padding: 10px;\n  padding-top: 10px;\n  padding-bottom: 65px;\n  max-height: unset;\n  width: 95%;\n  box-shadow: 0.2rem 0.2rem 0.5rem #b3b3b3;\n}\n.weight_card {\n  padding: 13px;\n  padding-top: 10px;\n  padding-bottom: 25px;\n  max-height: unset;\n  width: 90%;\n}\n.weight_text {\n  font-size: 20px;\n  text-align: left;\n  color: #454545;\n  line-height: 30px;\n}\n.weight_date {\n  color: #858585;\n}\n.kg {\n  font-size: 15px;\n}\n.weight_value {\n  font-size: 30px;\n  text-align: left;\n  color: black;\n  font-weight: 600;\n  font-family: \"Roboto Slab\", serif;\n}\n.weight_row {\n  border-bottom: 1px solid #f5f0ec;\n  padding-bottom: 10px;\n  padding-top: 10px;\n  font-family: \"Roboto Slab\", serif;\n}\n.weight_card > .weight_row:last-child {\n  border-bottom: none !important;\n}\n.weight_improve {\n  color: #8fb51d;\n}\n.weight_img {\n  width: 100%;\n  padding-top: 15px;\n  padding-bottom: 15px;\n}\n.cal {\n  font-size: 15px;\n  vertical-align: 3px;\n}\n.text_center {\n  text-align: -webkit-center;\n  text-align: -moz-center;\n}\n.activity h3 {\n  color: #14CEB7;\n}\n.activity .add-more-percentage {\n  top: 10% !important;\n  left: 30% !important;\n}\n.weight {\n  padding-top: 0 !important;\n}\n.weight h3.weight-color {\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.9rem;\n  color: #01A3A4;\n  font-weight: 500;\n}\n.weight .first-sec {\n  font-size: 1.1rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #707070;\n}\n.weight .first-sec-2 {\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #707070;\n}\n.weight .second-sec {\n  font-size: 1.5rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #01A3A4;\n}\n.weight .second-sec-2 {\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #01A3A4;\n}\n.weight .third-sec {\n  font-size: 1.1rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #707070;\n}\n.weight .third-sec-2 {\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #707070;\n}\n.water-animation *,\n.water-animation *:before,\n.water-animation *:after {\n  box-sizing: border-box;\n  outline: none;\n}\n.water-animation body {\n  background: #020438;\n  font: 14px/1 \"Open Sans\", helvetica, sans-serif;\n  -webkit-font-smoothing: antialiased;\n}\n.water-animation .percentNum {\n  font-size: 0.875rem;\n}\n.water-animation .percentB {\n  font-size: 0.875rem;\n}\n.water-animation .box {\n  width: 110px;\n  height: 110px;\n  position: absolute;\n  margin-top: 27%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  background: #d4eeff;\n  border: 2px solid #7daef9;\n  border-radius: 100%;\n  overflow: hidden;\n}\n.water-animation .box .percent {\n  position: absolute;\n  left: 0;\n  top: 0;\n  z-index: 3;\n  width: 100%;\n  height: 100%;\n  display: flex;\n  display: -webkit-flex;\n  align-items: center;\n  justify-content: center;\n  color: #fff;\n  font-size: 64px;\n}\n.water-animation .box .water {\n  position: absolute;\n  left: 0;\n  top: 0;\n  z-index: -1;\n  width: 100%;\n  height: 100%;\n  transform: translate(0, 100%);\n  background: #7daef9;\n  transition: all 0.3s;\n}\n.water-animation .box .water_wave {\n  width: 200%;\n  position: absolute;\n  bottom: 100%;\n}\n.water-animation .box .water_wave_back {\n  right: 0;\n  fill: #7daef9;\n  animation: wave-back 1.4s infinite linear;\n}\n.water-animation .box .water_wave_front {\n  left: 0;\n  fill: #7daef9;\n  margin-bottom: -1px;\n  animation: wave-front 0.7s infinite linear;\n}\n@keyframes wave-front {\n  100% {\n    transform: translate(-50%, 0);\n  }\n}\n@keyframes wave-back {\n  100% {\n    transform: translate(50%, 0);\n  }\n}\nion-slide ion-card {\n  margin-top: 20px;\n  margin-bottom: 10px;\n  margin-right: 20px;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.detox-card {\n  box-shadow: 0px 10px 8px rgba(0, 0, 0, 0.16);\n}\n.detox-card .main-title {\n  color: #2CB2B3;\n  font-size: 15px;\n  font-weight: 700;\n  padding-left: 20px;\n}\n.detox-card .detox-title {\n  font-size: 0.7rem;\n  color: #666666;\n  font-weight: 600;\n}\n.detox-normal-plan ion-button {\n  text-transform: inherit;\n  margin: 0px;\n  height: 20px;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  --padding-start: 0;\n  --padding-end: 0;\n  --padding-bottom: 0px;\n  --border-width: 1px !important;\n  box-shadow: 1px 1px 10px 2px #008182;\n  border-radius: 6px;\n  background: #FFF;\n  --color: #01A3A4;\n  border-width: 0px !important;\n  width: 7rem;\n  font-size: 0.8rem;\n}\n.text-green {\n  color: #01A3A4;\n}\n.text-orange {\n  color: orange;\n}\n.trial_expire {\n  color: red;\n  font-weight: bold;\n  text-align: center;\n  padding: 5px 10px 5px 10px;\n  position: fixed;\n  z-index: 999;\n  top: 40%;\n  height: 30px;\n  background: #fff;\n  border-radius: 8px;\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  font-size: 14px;\n  border: solid 1px #e5e5e5;\n}\n.trial {\n  text-align: center;\n  padding: 5px 10px 5px 10px;\n  color: red;\n  position: fixed;\n  z-index: 999;\n  top: 40%;\n  height: 30px;\n  background: #fff;\n  border-radius: 8px;\n  box-shadow: rgba(0, 0, 0, 0.12) 0px 4px 16px;\n  font-size: 14px;\n  border: solid 1px #e5e5e5;\n}\nion-segment-button {\n  min-width: 0px;\n  --color-checked: #303030;\n  --color: #858585;\n  height: 34px;\n  min-height: 25px;\n}\nion-segment.ion-color-segement {\n  --color-checked: #303030;\n  --color: #858585;\n}\nion-segment.ion-color-segement ion-label {\n  text-transform: uppercase;\n  font-weight: 500;\n  font-size: 0.65rem;\n}\nion-segment.ion-color-segement .segment-button-checked {\n  --indicator-box-shadow: none;\n  --indicator-color: #01A3A4;\n  --indicator-height: 3px;\n  --indicator-transition: transform 250ms cubic-bezier(0.4, 0, 0.2, 1);\n  text-transform: uppercase;\n}\nion-segment.ion-color-segement .segment-button-checked ion-label {\n  font-weight: 700;\n  font-size: 0.78rem;\n}\nion-segment-button {\n  min-width: 0px;\n  --color-checked: #303030;\n  --color: #858585;\n  height: 34px;\n}\nion-fab-button {\n  width: 30px;\n  height: 30px;\n}\nion-fab-list {\n  margin-left: 35px;\n  margin-top: -13px;\n}\n.detox-heading {\n  color: #01a3a4;\n  font-weight: 700;\n  padding-left: 10px;\n}\n.cal-button {\n  width: 100%;\n  height: 33px;\n  text-align: center;\n  padding-top: 5px;\n  background: #FFF;\n  color: #01a3a4;\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.16);\n}\n.cal-button-active {\n  width: 100%;\n  height: 33px;\n  vertical-align: middle;\n  text-align: center;\n  padding-top: 5px;\n  color: #141414;\n  font-weight: bold;\n  background: #01a3a4;\n  color: #FFF;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.cal-header {\n  --background: #FFF;\n  height: 35px;\n  --min-height: 2.6rem !important;\n}\n.cal-header ion-segment {\n  position: absolute;\n  z-index: 13;\n  top: 0px;\n  min-height: 30px !important;\n}\n.sample {\n  position: absolute;\n  top: 50%;\n  left: 25%;\n  background: transparent;\n  opacity: 0.2;\n}\n.sample img {\n  height: 100px;\n}\n.sample-chart {\n  text-align: center;\n  width: 100%;\n}\n.deficit-premium {\n  height: 50px;\n  float: right;\n}\n.center-weight {\n  top: 34%;\n  position: absolute;\n  left: 2%;\n  width: 106px;\n  text-align: center;\n  padding-top: 8%;\n}\n.center-meal {\n  display: inline-block;\n  position: absolute;\n  left: 40%;\n  padding-top: 8%;\n}\n.water-plus {\n  font-size: 2.5rem;\n  color: #3580F2;\n  position: relative;\n  top: 15px;\n}\n.water-minus {\n  font-size: 2.5rem;\n  color: #3580F2;\n  position: relative;\n  top: 15px;\n}\n.carbs-per {\n  position: absolute;\n  left: 38px;\n  top: 18px;\n  color: #4cc2c3;\n}\n.carbs-label {\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  position: absolute;\n  color: gray;\n  left: 28px;\n  top: 35px;\n}\n.burned-label {\n  font-size: 0.8rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  position: absolute;\n  color: gray;\n  left: 28px;\n  top: 35px;\n}\n.carbs-l2 {\n  color: #707070;\n  position: absolute;\n  left: 28px;\n  top: 56px;\n  font-weight: bold;\n  font-size: 1rem;\n}\n.water-label {\n  width: 52%;\n  height: 52%;\n  border-radius: 50%;\n  border: 1x solid #b3b3b3;\n  display: inline-block;\n  top: 30%;\n  position: absolute;\n  left: 25%;\n  padding-top: 8%;\n  color: #3498DB;\n}\n.first-progress {\n  margin-top: 1rem;\n  margin-left: 0.5rem;\n  margin-right: 0.5rem;\n  height: 4.5rem;\n}\n.second-progress {\n  height: 4.5rem;\n  margin-top: 1rem;\n  margin-left: 0.5rem;\n  margin-right: 0.5rem;\n}\n.act-cal {\n  color: #01A3A4;\n}\n.act-cal img {\n  height: 15px;\n  width: 11px;\n  position: relative;\n  top: 2px;\n  margin-right: 2px;\n}\n.act-cal span {\n  font-size: 0.9rem;\n}\n::ng-deep .plan {\n  font-size: 1rem;\n  text-transform: uppercase;\n}\n::ng-deep .plan ion-icon {\n  display: inline-block;\n  vertical-align: middle;\n}\n.header-progress-circle {\n  background-color: #01A3A4;\n  border-top-left-radius: 0;\n  border-top-right-radius: 0;\n  color: white;\n}\n.header-progress-circle .header-progress-circle-row {\n  margin: -15px 0 -20px 0;\n}\n.header-progress-circle .header-calorie-ring-text {\n  color: #01A3A4;\n  top: 24%;\n  position: absolute;\n  left: 10%;\n  width: 80%;\n}\n.header-progress-circle ion-label.date {\n  font-size: 1rem;\n  color: grey;\n}\n.header-progress-circle ion-label.darkGrey {\n  color: #060606;\n  color: #888;\n  font-weight: bold;\n}\n.header-progress-circle ion-icon.arrow {\n  font-size: 2rem;\n  opacity: 0.8rem;\n}\n.header-progress-circle .total-calories {\n  line-height: 1.75;\n}\n.header-progress-circle .total-calories .cur-calories {\n  font-size: 2rem;\n  font-weight: bold;\n}\n.header-progress-circle .total-calories .cur-calories span {\n  font-size: 0.7rem;\n}\n.header-progress-circle .total-calories .kcalText {\n  display: block;\n  line-height: 0px;\n}\n.header-progress-circle .diet-plan {\n  font-size: 0.9rem;\n}\n.header-progress-circle .nutrients-value {\n  font-size: 0.9rem;\n  text-transform: uppercase;\n  color: white !important;\n}\n.header-progress-circle .nutrients-value ion-label {\n  width: 5rem;\n  display: inline-block;\n  padding-bottom: 5px;\n  color: white !important;\n}\n.no-meal-container {\n  height: 20vh;\n}\n@media only screen and (min-width: 768px) {\n  .no-meal-container {\n    height: 14vh;\n  }\n\n  .total-calories {\n    left: 25%;\n  }\n\n  .cur-calories {\n    font-size: 4rem;\n    font-weight: bold;\n  }\n\n  .plan {\n    color: #fff;\n    font-size: 1.5rem;\n  }\n\n  .header-progress-circle .header-calorie-ring-text {\n    top: 35%;\n  }\n\n  .header-progress-oval {\n    background-size: 400%;\n  }\n}\n​ .act-steps {\n  color: #3498DB;\n  position: relative;\n  top: 85px;\n  color: #01A3A4;\n}\n​ .act-steps img {\n  position: relative;\n  top: 3px;\n  margin-right: 2px;\n}\n​ .act-steps span {\n  font-size: 0.9rem;\n}\n.source {\n  color: #1492E6;\n  text-decoration: underline;\n  font-size: 0.8em;\n  padding-top: 2px;\n}\n.meal-label {\n  top: 11%;\n  font-size: 0.9rem;\n  position: relative;\n}\n.def-label {\n  color: #1492E6;\n  font-size: 0.8rem;\n  text-decoration: underline;\n}\n.glass-text {\n  position: absolute;\n  left: 3%;\n  top: -125px;\n  color: #509bed;\n  z-index: 999;\n  width: 152px;\n}\n.add {\n  position: relative;\n  top: 30px;\n  left: 2px;\n  font-size: 1.2rem;\n  color: #509bed;\n}\n.minus {\n  position: relative;\n  top: 30px;\n  right: 2px;\n  font-size: 1.2rem;\n  color: #509bed;\n}\n.scrolling-wrapper {\n  overflow-x: scroll;\n  overflow-y: hidden;\n  white-space: nowrap;\n  background: #F6F7FC;\n  padding: 10px;\n}\n.scrolling-wrapper ion-item {\n  display: inline-block;\n  padding: 0px;\n  --padding-start: 0px;\n  margin: 0px;\n  --padding-end: 0px;\n}\n.scroll-list {\n  height: 200px;\n  width: 200px;\n}\n.white-space {\n  display: inherit !important;\n  white-space: pre-line;\n}\n.premium {\n  position: absolute;\n  left: 1rem;\n  top: 2px;\n  height: 80%;\n  width: auto;\n}\n.premium-right {\n  height: 3vh;\n  top: 7px;\n  right: 1rem;\n  z-index: 9;\n  position: absolute;\n}\n.ion-card-footer {\n  width: 100%;\n  bottom: 0;\n  position: relative;\n}\n.ion-card-button {\n  --color: #707070;\n  width: 60%;\n  margin: auto;\n  border-radius: 1rem;\n  background: #46A1A3;\n  --color: white !important;\n  height: 2rem;\n  margin-top: 1rem;\n  margin-bottom: 1rem;\n}\n.ion-card-header-custom {\n  --padding-start: 20px;\n  --padding-end: 15px;\n  --border-color: #eee0;\n  --inner-padding-bottom: 5px;\n}\n.secondary-slide {\n  background-color: #F6F7FC;\n}\n.secondary-slide ::ng-deep ion-slide {\n  margin-right: 15px !important;\n}\n.secondary-slide ion-slide ion-card {\n  margin-bottom: -2px;\n}\n.secondary-slide ion-slide ion-card ion-card-header {\n  padding: 0;\n  background-color: #FFF;\n}\n.secondary-slide ion-slide ion-card ion-card-header img.premium {\n  height: 90%;\n  width: 90%;\n  position: inherit;\n  margin-left: 0.75rem;\n}\n.secondary-slide ion-card-content ion-row {\n  transform: translateY(100%);\n}\n::ng-deep .secondary-slide .swiper-pagination {\n  padding-top: 35px;\n  margin-bottom: -5px;\n  padding-right: 8% !important;\n}\n::ng-deep .swiper-pagination {\n  position: relative !important;\n  padding-top: 15px;\n  transform: scale(0.5);\n}\n::ng-deep .main-card .swiper-pagination {\n  padding-top: 0 !important;\n}\n.header-round {\n  position: relative;\n  top: 0%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n}\n.date-arrows {\n  text-align: center;\n  font-size: 1.5rem;\n}\n.header-progress-oval {\n  height: 3.3rem;\n  background-repeat: no-repeat;\n  background-position: bottom;\n}\n.header-progress-oval ion-icon {\n  transform: rotate(90deg);\n  position: relative;\n  bottom: -0.5rem;\n  color: white;\n  font-size: 2rem;\n  font-weight: bold;\n}\n.header-progress-oval .nutrients-title {\n  font-size: 0.9rem;\n  text-transform: uppercase;\n  color: white;\n}\n.header-progress-oval .nutrients-title ion-label {\n  width: 4rem;\n  display: inline-block;\n  padding-bottom: 5px;\n  border-bottom: 1px solid rgba(255, 255, 255, 0.5);\n}\n.header-progress-circle .nutrients-title {\n  font-size: 0.9rem;\n  text-transform: uppercase;\n  color: white;\n}\n.header-progress-circle .nutrients-title #burned-label {\n  width: 4rem;\n  display: inline-block;\n  padding-bottom: 5px;\n  border-bottom: 1px solid rgba(255, 255, 255, 0.5);\n}\n.alert-abnormal {\n  animation: shake 0.5s;\n  animation-iteration-count: infinite;\n}\n.alert-abnormal-right {\n  animation: shakeRight 0.5s;\n  animation-iteration-count: infinite;\n}\n.alert-abnormal-left {\n  animation: shakeLeft 0.5s;\n  animation-iteration-count: infinite;\n}\n@media (min-width: 770px) {\n  .water-weight-section {\n    height: 356px;\n  }\n}\n@keyframes shakeLeft {\n  50% {\n    transform: translate(-5px) scale(1.2);\n  }\n}\n@keyframes shakeRight {\n  50% {\n    transform: translate(5px) scale(1.2);\n  }\n}\n@keyframes shake {\n  50% {\n    transform: translate(0px, 3px) rotate(90deg);\n  }\n}\n.refresh-icon {\n  color: #1492E6;\n  margin-left: 0;\n  margin-right: -10px;\n}\nion-header.header-ios {\n  height: 3rem !important;\n}\nion-header {\n  height: 3rem !important;\n}\nion-header ion-toolbar {\n  --min-height: 3rem !important;\n  --background: white;\n  --color: #01A3A4;\n}\n.custom-bullet {\n  display: inline-block;\n  height: 10px;\n  width: 30px;\n  background: #DDD;\n}\n.active-custom-bullet {\n  background: #01A3A4;\n}\n.custom-bullet-space {\n  display: inline-block;\n  height: 10px;\n  width: 10px;\n  background: #F6F7FC;\n}\n.layer-on-diets {\n  height: 100%;\n  width: 100%;\n  position: absolute;\n  background: #5a5c5cc9;\n  z-index: 1111;\n}\n.layer-on-diets-random-lock {\n  height: 60vh;\n  width: 100%;\n  position: absolute;\n  z-index: 1111;\n}\n.layer-on-diets-back {\n  height: 100%;\n  width: 100%;\n  position: absolute;\n  background: transparent;\n  z-index: 1110;\n}\n.upgrade-slider-lock {\n  background-repeat: no-repeat;\n  background-position: bottom;\n  height: 60%;\n  width: 100%;\n}\n.upgrade-slider-lock-text {\n  text-transform: uppercase;\n  color: #fff;\n  font-size: 1.2rem;\n  line-height: 3;\n}\n.upgrade-slider-random-lock-text {\n  text-transform: uppercase;\n  color: #fff;\n  font-size: 1.2rem;\n  line-height: 3;\n  position: absolute;\n  top: 36%;\n  width: 100%;\n  left: 0;\n  bottom: 10px;\n}\n.improve {\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.8rem;\n  color: #707070;\n}\n.qa-day ion-card {\n  width: 90%;\n  padding: 10px 20px;\n  display: inline-table;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.news-day ion-card {\n  display: inline-block;\n  width: 90%;\n  padding: 10px 20px;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.result-main-container {\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  border-radius: 30px !important;\n}\n.results-cards {\n  margin: 0 !important;\n  border-radius: 30px !important;\n  box-shadow: none !important;\n  background-color: #fff !important;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  margin-right: 1rem;\n  border-radius: 30px !important;\n  padding: 1rem;\n}\n.gauge-chart ion-card {\n  display: inline-block;\n  width: 90%;\n  padding: 10px 20px;\n  padding-top: unset;\n  padding-left: unset;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  height: 100px;\n}\n.recipe-day ion-card {\n  display: inline-block;\n  width: 90%;\n  padding: 10px 20px;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.less-than-100-food-item-container {\n  display: inline-block;\n  width: 90%;\n  padding: 10px 20px;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.less-than-100-food-item-inner-container {\n  width: 100%;\n  padding: 10px 20px;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.personalise-now ion-card {\n  display: inline-block;\n  width: 90%;\n  padding: 10px 20px;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.personalise-covid ion-card {\n  display: inline-block;\n  width: 90%;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.other-food-item ion-card {\n  display: inline-block;\n  width: 90%;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.less-100-cal ion-card {\n  display: inline-block;\n  width: 90%;\n  padding: 10px 20px;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.title-text {\n  font-family: \"gt-america-standard\", \"Helvetica Neue\", Arial, sans-serif !important;\n  font-size: 0.95rem !important;\n  color: #3880ff !important;\n}\n.activity-section {\n  max-height: none;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  margin-left: auto;\n  border: 1px solid #EEE;\n  border-radius: 35px;\n  margin: 16px;\n  width: auto;\n}\n.activity-section .row .activity:first-child {\n  border-right: 1px solid #EEE;\n}\n.activity-section .row .activity {\n  border-bottom: 1px solid #EEE;\n  height: 189px;\n}\n.activity-section .header {\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1rem;\n  text-transform: capitalize;\n  padding: 15px 15px 10px;\n  color: #737777;\n}\n.activity-section .premium {\n  position: relative;\n  left: 1rem;\n  top: 3px;\n  height: 80%;\n  width: 80%;\n}\n.activity-section .tracker-premium {\n  height: 3vh;\n  width: 4vh;\n  position: absolute;\n  top: 80%;\n  left: 80%;\n}\n.activity-section .activity-circle-progress {\n  padding: 0px 25px 10px 25px;\n}\n.discover-section {\n  max-height: none;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  border: 1px solid #EEE;\n  border-radius: 35px;\n  margin: 16px;\n  width: auto;\n}\n.discover-section ion-row.discover-content {\n  border: 1px solid #EEE;\n  border-radius: 25px;\n}\n.discover-section ion-row.discover-content .left-content .header {\n  font-size: 1rem;\n  font-weight: bold;\n  padding-bottom: 10px;\n}\n.discover-section ion-row.discover-content .left-content .discover-description {\n  font-size: 0.8rem;\n  padding-bottom: 10px;\n}\n.discover-section ion-row.discover-content .left-content ion-button {\n  max-height: 1.5rem;\n  --background: #03a3a4;\n  --color: white;\n  font-size: 0.8rem;\n  font-weight: bold;\n}\n.discover-section ion-row.discover-content .right-content img {\n  display: inline-block;\n  vertical-align: middle;\n  max-height: 100px;\n  max-width: 100px;\n}\n.discover-section ion-row.discover-content .right-content .no-max-size {\n  max-height: 100%;\n  max-width: 100%;\n}\n.diet-plan-skeleton {\n  width: 100%;\n  height: 105%;\n  margin-top: -5px;\n}\n.activity-skeleton {\n  width: 100%;\n  height: 100%;\n  margin-top: 0;\n}\n.activity-skeleton-row {\n  border: 1px solid #7d7d7d4a;\n  height: 100%;\n}\n.qa-day-skeleton {\n  margin-top: 0;\n  padding: 0;\n  height: 300px;\n  width: 90%;\n  display: inline-table;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.news-day-skeleton {\n  margin-top: 0;\n  padding: 0;\n  height: 300px;\n  width: 90%;\n  display: inline-table;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.recipe-day-skeleton {\n  margin-top: 0;\n  padding: 0;\n  height: 300px;\n  width: 90%;\n  display: inline-table;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.personalise-now-skeleton {\n  background: #fff;\n  border-radius: 25px;\n  padding: 0;\n  height: 100px;\n  display: inline-block;\n  width: 90%;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\nion-refresher-content {\n  background-color: #fff;\n  --color: #7d7d7d;\n  color: #7d7d7d !important;\n}\n.get-button-icon {\n  color: #7d7d7d;\n  /* background-color: #fff; */\n  border-radius: 100px;\n  position: absolute;\n  border: 1px solid;\n  left: 1.2rem;\n  top: 0.7rem;\n  height: 25px;\n  width: 25px;\n  left: 85%;\n  z-index: 2;\n}\n.get-remove-icon {\n  color: #01A3A4;\n  /* background-color: #fff; */\n  border-radius: 100px;\n  height: 20px;\n  width: 20px;\n  z-index: 2;\n  margin-left: 5px;\n  margin-top: 5px;\n}\n::ng-deep .get-remove-icon-msg {\n  color: #01A3A4;\n  border-radius: 100px;\n  height: 15px;\n  width: 15px;\n}\n.add-food-icon {\n  color: #03a3a4;\n  position: absolute;\n  top: 1rem;\n  height: 2rem;\n  width: 2rem;\n  left: 80%;\n  z-index: 2;\n}\n.add-food-title {\n  padding-top: 10px;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif !important;\n  font-size: 0.8rem !important;\n  padding-left: 0 !important;\n}\n.replace-food-icon {\n  color: #03a3a4;\n  /* border-radius: 100px; */\n  position: absolute;\n  /* border: 1px solid; */\n  left: 1.2rem;\n  top: 8rem;\n  height: 2rem;\n  width: 2rem;\n  left: 80%;\n  z-index: 2;\n}\n.content-size {\n  font-size: 0.75rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.detail {\n  border-right: 1px solid #7377774a;\n}\n.detail span {\n  font-weight: 700;\n}\n.padding-left-5 {\n  padding-left: 5px;\n}\n.padding-right-5 {\n  padding-right: 5px;\n}\n.darkgreen, .legend-9 {\n  border-color: #38A534;\n  background-color: #38A534;\n}\n.light-green, .legend-6 {\n  border-color: #94EA0A;\n  background-color: #94EA0A;\n}\n.yellow, .legend-3 {\n  border-color: #EADC18;\n  background-color: #EADC18;\n}\n.yellow-orange, .legend-1 {\n  border-color: #ffa500;\n  background-color: #ffa500;\n}\n.grey, .legend-0 {\n  border-color: #7d7d7d5e;\n}\n.legend {\n  border-width: 1px;\n  border-style: solid;\n  height: 56px;\n  width: 2px;\n  margin-right: 5px;\n}\n.qa-day {\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1rem;\n  color: #01A3A4;\n}\n.todo-header {\n  font-family: \"gt-america-standard\", \"Helvetica Neue\", Arial, sans-serif !important;\n}\n.slot-selection-card {\n  display: flex;\n  overflow-x: scroll;\n  height: 40px;\n}\n.slot-selection-card .slot-selection-item {\n  display: inline-block !important;\n  min-width: 100px !important;\n  width: auto !important;\n  border: 1px solid #03a3a4;\n  border-radius: 20px;\n  color: #03a3a4;\n  --background: #fff;\n  --border-radius: 20px;\n}\n.slot-selection-btn-active {\n  color: #fff !important;\n  --background: #03a3a4 !important;\n}\n::ng-deep .food-less-100 ion-card, ::ng-deep .food-less-100 ion-slide {\n  margin-top: 0 !important;\n}\n::ng-deep .food-less-100 .swiper-pagination {\n  padding-top: 0 !important;\n  margin-top: -10px !important;\n}\n.darkgreen, .legend-9-lower {\n  border-color: #38A534;\n}\n.light-green, .legend-6-lower {\n  border-color: #94EA0A;\n}\n.yellow, .legend-3-lower {\n  border-color: #EADC18;\n}\n.yellow-orange, .legend-1-lower {\n  border-color: #ffa500;\n}\n.grey, .legend-0-lower {\n  border-color: #7d7d7d5e;\n}\n.legend-lower {\n  border-left-width: 5px;\n  border-left-style: solid;\n}\n.sub-foodItem-header {\n  font-size: 1.2rem;\n  font-weight: bold;\n  color: #4C5264;\n}\n.sub-foodItem-sub-header {\n  font-size: 0.75rem;\n}\n.sub-foodItem-btn {\n  width: auto !important;\n  border: 1px solid #03a3a4;\n  border-radius: 20px;\n  color: #fff;\n  --background: #03a3a4;\n  --border-radius: 20px;\n  min-width: 80px !important;\n  margin-top: 15px;\n}\n.sub-fasting-btn {\n  width: auto !important;\n  border: 1px solid #03a3a4;\n  border-radius: 20px;\n  color: #fff;\n  --background: #03a3a4;\n  --border-radius: 20px;\n  min-width: 80px !important;\n}\n.discover-section {\n  max-height: none;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  border: 1px solid #EEE;\n  border-radius: 30px;\n  margin: 16px;\n  width: auto;\n}\n.discover-section ion-row.discover-content {\n  border: 1px solid #EEE;\n  border-radius: 25px;\n}\n.discover-section ion-row.discover-content .left-content .header {\n  font-size: 1rem;\n  font-weight: bold;\n  padding-bottom: 10px;\n}\n.discover-section ion-row.discover-content .left-content .discover-description {\n  font-size: 0.8rem;\n  padding-bottom: 10px;\n}\n.discover-section ion-row.discover-content .left-content ion-button {\n  max-height: 1.5rem;\n  --background: #03a3a4;\n  --color: white;\n  font-size: 0.8rem;\n  font-weight: bold;\n}\n.discover-section ion-row.discover-content .diet-plan img {\n  transform: rotate(90deg);\n  border-radius: 50%;\n}\n.discover-section ion-row.discover-content .right-content img {\n  display: inline-block;\n  vertical-align: middle;\n  max-height: 100px;\n  max-width: 100px;\n}\n.discover-section ion-row.discover-content .right-content .no-max-size {\n  max-height: 100%;\n  max-width: 100%;\n}\n.immunity-container {\n  background-size: contain;\n  background-repeat: no-repeat;\n  background-position: right;\n}\n.qa-day {\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1rem;\n  color: #01A3A4;\n}\n.qa-day-see {\n  font-family: \"gt-america-standard-medium\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1rem;\n  color: #60AFF5;\n}\n.display-content {\n  display: contents;\n}\n.description1 {\n  padding-top: 10px;\n}\n.description1 p {\n  font-size: 1rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  text-align: start;\n  text-justify: auto;\n  float: left;\n  color: #4C5264 !important;\n  position: relative;\n}\n::ng-deep .desc {\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 0.9rem;\n  display: contents;\n  margin-top: 10px !important;\n}\n::ng-deep .desc p {\n  margin-bottom: 0 !important;\n  display: contents;\n}\n::ng-deep .dispBlock {\n  margin: 0 !important;\n}\n::ng-deep .dispBlock p {\n  display: block !important;\n}\n.padding-top-1 {\n  padding-top: 10px;\n}\n.qa-day-img {\n  border-radius: 10px;\n}\n.view-more {\n  text-align: left;\n  font-size: 1rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #60AFF5 !important;\n  font-style: italic;\n}\n.qa-title {\n  font-weight: bold;\n  color: #4C5264;\n  font-size: 1rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  margin: 0;\n  white-space: pre-line;\n  margin-top: 5px;\n}\n.qa-day-img {\n  text-align: center;\n  background-repeat: no-repeat;\n  background-size: cover;\n  height: 200px;\n  border-radius: 15px;\n  margin-top: 10px;\n  max-height: 200px;\n  width: 100%;\n}\n.two-lines {\n  display: block;\n  display: -webkit-box;\n  max-width: 100%;\n  height: 35px;\n  margin: 0 auto;\n  font-size: 14px;\n  -webkit-line-clamp: 2;\n  -webkit-box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n::ng-deep .two-lines-noti-msg {\n  display: block;\n  display: -webkit-box;\n  max-width: 100%;\n  height: 38px;\n  margin: 0 auto;\n  font-size: 14px;\n  -webkit-line-clamp: 2;\n  -webkit-box-orient: vertical;\n  overflow: hidden;\n  text-overflow: ellipsis;\n}\n.height24 {\n  height: 24px !important;\n}\n::ng-deep .single-line-noti-msg-center {\n  justify-content: center;\n  align-items: center;\n  font-size: 14px;\n}\n.immunity-section {\n  --background: #FD9F33 !important;\n  color: #FD9F33 !important;\n}\n.detox-section {\n  --background: #4CB271 !important;\n  color: #4CB271 !important;\n}\n.weight-loss-plus-section {\n  --background: #0B94C1 !important;\n  color: #0B94C1 !important;\n}\n.post-covid-section {\n  --background: #754B29 !important;\n  color: #754B29 !important;\n}\n.cholesterol-section {\n  --background: #A31E79 !important;\n  color: #A31E79 !important;\n}\n.diabetes-section {\n  --background: #D14322 !important;\n  color: #D14322 !important;\n}\n.hypertension-section {\n  --background: #FF5D56 !important;\n  color: #FF5D56 !important;\n}\n.mucsle {\n  --background: #0B94C1 !important;\n  color: #0B94C1 !important;\n}\n.fat {\n  --background: #FD980F !important;\n  color: #FD980F !important;\n}\n.pcos-section {\n  --background: #FF5A7D !important;\n  color: #FF5A7D !important;\n}\n.header-message-tool {\n  color: #333;\n  font-size: 0.8rem;\n}\n.cholesterol-section-stroke {\n  --background: #A31E79 !important;\n}\n.diabetes-section-stroke {\n  --background: #D14322 !important;\n}\n.hypertension-section-stroke {\n  --background: #FF5D56 !important;\n}\n.pcos-section-stroke {\n  --background: #FF5A7D !important;\n}\n.mucsle-stroke {\n  --background: #0B94C1 !important;\n}\n.fat-stroke {\n  --background: #FD980F !important;\n}\n.weight-loss {\n  --background: #01A3A4 !important;\n}\n.immunity-section-stroke {\n  --background: #FD9F33 !important;\n}\n.detox-section-stroke {\n  stroke: #4CB271 !important;\n}\n.weight-loss-plus-section-stroke {\n  stroke: #0B94C1 !important;\n}\n.weight-loss-stroke {\n  stroke: #72BDCA !important;\n}\n.detox-stroke {\n  stroke: #4cb271 !important;\n}\n.notification-border {\n  border: 2px solid #01A3A4 !important;\n}\n.cholesterol-section-border {\n  border: 2px solid #A31E79 !important;\n}\n.diabetes-section-border {\n  border: 2px solid #D14322 !important;\n}\n.hypertension-section-border {\n  border: 2px solid #FF5D56 !important;\n}\n.pcos-section-border {\n  border: 2px solid #FF5A7D !important;\n}\n.mucsle-border {\n  border: 2px solid #0B94C1 !important;\n}\n.fat-border {\n  border: 2px solid #FD980F !important;\n}\n.weight-loss-border {\n  border: 2px solid #01A3A4 !important;\n}\n.immunity-section-border {\n  border: 2px solid #FD9F33 !important;\n}\n.detox-section-border {\n  border: 2px solid #4CB271 !important;\n}\n.weight-loss-plus-section-border {\n  border: 2px solid #0B94C1 !important;\n}\n.post-covid-section-border {\n  border: 2px solid #754B29 !important;\n}\n.weight-loss-border {\n  border: 2px solid #72BDCA !important;\n}\n.detox-border {\n  border: 2px solid #4cb271 !important;\n}\n.personalise-card-img {\n  height: 150px;\n}\n.center-content {\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%) !important;\n  display: block !important;\n}\n.toggleswitch ion-card {\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  border: 1px solid #EEE;\n  border-radius: 35px;\n  height: 32px;\n}\n.toggleswitch ion-card div {\n  color: white;\n  border-radius: 35px;\n  height: 30px;\n  width: 50%;\n  display: inline-block;\n  line-height: 2;\n}\n.toggleswitch ion-card div ion-icon,\n.toggleswitch ion-card div span {\n  display: inline-block;\n  vertical-align: middle;\n}\n.toggleswitch ion-card .active {\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.toggleswitch ion-card .deactive {\n  box-shadow: unset;\n}\n.video-iframe {\n  transform: rotate(-90deg);\n  transform-origin: left top;\n  width: 100vh;\n  overflow-x: hidden;\n  position: absolute;\n  top: 100%;\n  left: 0;\n  height: 50vh;\n}\n.get-button-icon-potrait {\n  color: #fff;\n  background-color: #01a3a4;\n  border: 2px solid #fff;\n  border-radius: 100px;\n  position: absolute;\n  right: 1.2rem;\n  top: 3rem;\n  height: 35px;\n  width: 35px;\n}\n.get-button-icon-landscape {\n  color: #fff;\n  background-color: #01a3a4;\n  border: 2px solid #fff;\n  border-radius: 100px;\n  position: absolute;\n  right: 1.2rem;\n  top: 2rem;\n  height: 35px;\n  width: 35px;\n  left: 20%;\n}\n.pic-todo {\n  height: 50px !important;\n  width: 50px !important;\n}\n.todo-icon {\n  margin-left: 0;\n  margin-right: -10px;\n  --background-checked:#01A3A4;\n  --border-color-checked:#01A3A4;\n}\n.custom-todo-height-scroll ion-item {\n  margin-top: -0.2rem;\n}\n.custom-todo-height-scroll .wp-prog-label {\n  color: #5D5D5D;\n  font-size: 0.9rem !important;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  position: relative;\n  left: 0;\n  padding-bottom: 1rem;\n  width: 100%;\n}\n.custom-todo-height-scroll .wp-prog-label div {\n  font-size: 1rem;\n}\n.active-eaten {\n  --background: #01A3A4 !important;\n  color: #fff !important;\n  border: 1px solid #01A3A4 !important;\n}\n.food-action-btn {\n  --background: #fff;\n  font-size: 0.6rem;\n  border: 1px solid #8c8E93;\n  border-radius: 100px;\n  color: #8c8E93;\n  text-transform: capitalize;\n}\n.food-action-btn-container {\n  display: flex;\n  position: relative;\n  margin-top: -20px;\n  margin-left: 30%;\n  overflow-x: scroll;\n}\nion-item {\n  --highlight-color-focused: none !important;\n  --highlight-color-valid: none !important;\n  --highlight-color-invalid: none !important;\n}\n.start-fasting {\n  --background: #01a3a4;\n  --color: #fff;\n  --border-color: #01a3a4;\n  --border-radius: 100px;\n  border-radius: 100px;\n  height: 1.5rem;\n  font-size: 0.75rem;\n  position: relative;\n  top: -90px;\n  text-transform: uppercase;\n}\n.fasting-controller {\n  position: relative;\n  top: -95px;\n}\n.fasting-controller .count-down {\n  margin-bottom: 5px;\n}\n.fasting-controller ion-icon {\n  font-size: 1.75rem;\n}\n.result-main-container {\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  border-radius: 30px !important;\n}\n.result-title {\n  font-size: 1.1rem !important;\n  color: #01A3A4 !important;\n}\n.age-container {\n  color: #666666;\n}\n.transformation ion-card {\n  display: inline-block;\n  width: 94%;\n  box-shadow: unset !important;\n  background: transparent;\n  min-height: 490px;\n  margin-top: 0.7rem !important;\n}\n.transformation-card {\n  position: relative;\n  min-height: unset;\n  max-height: unset;\n  border-radius: 30px;\n}\n.result-content {\n  color: #707070;\n  font-style: italic;\n  font-size: 0.9rem;\n  text-align: justify;\n}\n.ion-padding-bottom-10 {\n  padding-bottom: 10px;\n}\n.bottom-space {\n  padding-bottom: 15px;\n}\n.border-radious15 {\n  border-radius: 15px;\n}\n.theme-border {\n  border: 1px solid #03a3a4;\n}\n.offer-text {\n  height: auto !important;\n  color: #D5A620;\n  font-weight: bold;\n  margin-left: 0 !important;\n}\n.gold-background {\n  --background: #D5A620;\n  border: 3px solid #D5A620;\n}\n.gold-border {\n  border: 3px solid #D5A620;\n}\n.font-size75 {\n  font-size: 0.75rem !important;\n}\n.font-size-1-2 {\n  font-size: 1.2rem;\n}\ncircle-progress svg {\n  box-shadow: 2px 8px 10px #000;\n  border-radius: 50%;\n  width: 140px;\n}\n.curve {\n  border-bottom-left-radius: 50% 150px !important;\n  border-bottom-right-radius: 50% 150px !important;\n  position: relative;\n  overflow: hidden;\n  width: 160%;\n  left: -30%;\n  background-color: red;\n  background-size: 42% auto;\n  height: 300px;\n  top: -63px;\n  z-index: 0;\n  background: white;\n  /* box-shadow: 0px 33px 19px -35px; */\n}\n.bottom-activities {\n  position: relative;\n  top: -10px;\n}\n.light-grey {\n  color: #7d7d7d;\n}\n.heart-beat-animation {\n  animation: animateHeart 2s infinite;\n}\n@keyframes animateHeart {\n  0% {\n    transform: scale(0.8);\n  }\n  5% {\n    transform: scale(0.9);\n  }\n  10% {\n    transform: scale(0.8);\n  }\n  15% {\n    transform: scale(1);\n  }\n  50% {\n    transform: scale(0.8);\n  }\n  100% {\n    transform: scale(0.8);\n  }\n}\n.low-heart-beat-animation {\n  animation: animateHeart 2s infinite;\n}\n@keyframes animateHeart {\n  50% {\n    transform: scale(0.9);\n  }\n  100% {\n    transform: scale(1);\n  }\n}\n.overlay-bottom-msg {\n  margin-top: 0.5rem;\n  position: fixed;\n  width: 100%;\n  bottom: 55px;\n  text-align: center;\n  background: #D09E03;\n  z-index: 9;\n  height: 5vh;\n}\n.font-white {\n  color: white;\n}\n.double-arrow {\n  animation: shake 1s infinite ease-in-out;\n}\n@keyframes shake {\n  0% {\n    transform: translateX(-5px);\n  }\n  40% {\n    transform: translateX(5px);\n  }\n  60% {\n    transform: translateX(-5px);\n  }\n  80% {\n    transform: translateX(5px);\n  }\n  100% {\n    transform: translateX(0);\n  }\n}\n.loading-protien-tracker {\n  height: 60px;\n  width: 60px;\n  border-radius: 100px;\n  margin: auto;\n}\n.rotate-90 {\n  transform: rotate(90deg);\n}\n.defiicient {\n  font-size: 10px;\n  position: absolute;\n  top: 78px;\n  font-weight: 900;\n}\n.defiicient.green-defiicient {\n  color: #00B05E;\n}\n.defiicient.red-deficient {\n  color: #C53F3F;\n}\n.defiicient.burnt-deficient {\n  color: #407BFF;\n}\n.defiicient-data {\n  font-size: 9px;\n  position: absolute;\n  top: 10px;\n  left: 10px;\n  font-weight: 900;\n}\n.defiicient-data-burnt {\n  font-size: 9px;\n  position: absolute;\n  top: 10px;\n  left: 18px;\n  font-weight: 900;\n}\n.defiicient-data-lost {\n  font-size: 9px;\n  position: absolute;\n  top: 10px;\n  left: 18px;\n  font-weight: 900;\n}\n.disable-traker-container {\n  height: 65px;\n  margin-bottom: 5px;\n}\n.consumed-container {\n  height: 70px;\n  width: 85px;\n  background-repeat: no-repeat;\n}\n.walk-man-container {\n  height: 93px;\n  width: 50px;\n  background-repeat: no-repeat;\n  background-size: cover;\n}\n.deficit-container {\n  height: 70px;\n  width: 85px;\n  background-repeat: no-repeat;\n}\n.equal-container {\n  height: 11px;\n  width: 19px;\n  background-size: contain;\n  background-repeat: no-repeat;\n  margin-left: 8px;\n}\n.lost-container {\n  height: 73px;\n  background-repeat: no-repeat;\n  background-size: contain;\n  width: 100px;\n  margin-bottom: -10px;\n  background-position: 0 -5px;\n}\n.deficiet-value {\n  color: blue;\n  display: block;\n  font-size: 0.725rem;\n  text-decoration: underline;\n}\n.deficiet-info {\n  margin-top: 35px;\n  font-size: 0.725rem;\n}\n.fasting-card::before {\n  content: \"\";\n  position: absolute;\n  width: 125px;\n  height: 115px;\n  top: 50px;\n  right: -10px;\n  background-size: contain;\n  background-repeat: no-repeat;\n  background-position: right;\n  margin-bottom: 0;\n  transform: rotate(90deg);\n}\n.no-margin-top {\n  margin-top: 0 !important;\n}\n.mrg-top-10 {\n  margin-top: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUtdmVydGljYWwuY29tcG9uZW50LnNjc3MiLCIuLi8uLi8uLi9hc3NldHMvZm9udHMvZm9udHMuc2NzcyIsIi4uLy4uLy4uL3RoZW1lL2N1c3RvbS10aGVtZXMvdmFyaWFibGVzLnNjc3MiLCIuLi8uLi8uLi90aGVtZS9jdXN0b20tdGhlbWVzL2NvbG9ycy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQ0FoQjtFQUNFLHVDQUFBO0VBQ0EsOERBQUE7QURFRjtBQ0NBO0VBQ0Usd0NBQUE7RUFDQSwrREFBQTtBRENGO0FDRUE7RUFDRSxxQ0FBQTtFQUNBLDREQUFBO0FEQUY7QUNHQTtFQUNFLCtDQUFBO0VBQ0EsNkRBQUE7QURERjtBQ0lBO0VBQ0Usc0NBQUE7RUFDQSw2REFBQTtBREZGO0FDTUE7RUFDRSxvQ0FBQTtFQUNBLCtEQUFBO0FESkY7QUF0QkE7RUFDRSw0QkFBQTtBQXdCRjtBQXRCQTtFQUNFLGdDQUFBO0FBeUJGO0FBdEJFO0VBQ0UsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0FBd0JKO0FBdEJFO0VBQ0UsV0FBQTtFQUNBLDRCQUFBO0VBQ0EsK0VFZEc7RUZlSCxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQXdCSjtBQXBCRTtFQUNFLFdBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7QUFzQko7QUFsQkE7RUFDRSxrQkFBQTtBQXFCRjtBQXBCRTtFQUNFLFVBQUE7RUFDQSxxQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLHFEQUFBO0FBc0JKO0FBcEJNO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLHFCQUFBO0VBQ0EsZ0JBQUE7QUFzQlI7QUFqQk07RUFDRSw2QkFBQTtFQUNBLGdDQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7QUFtQlI7QUFiQTtFQUNFLFVBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx3QkFBQTtFQUNBLHlDQUFBO0VBQ0EscUJBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQWdCRjtBQWJBO0VBQ0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLHVCQUFBO0VBQ0EsWUFBQTtBQWdCRjtBQWJBO0VBQ0UseUJBQUE7QUFnQkY7QUFiQTtFQUNFLGNBQUE7QUFnQkY7QUFiQTtFQUNFLGFBQUE7QUFnQkY7QUFkQTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG9CQUFBO0FBaUJGO0FBZEE7RUFDRyx5QkFBQTtBQWlCSDtBQWRBO0VBQ0UseUJBQUE7QUFpQkY7QUFkQTtFQUtFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0FBYUY7QUFWQTtFQUNFLHlCQUFBO0FBYUY7QUFWQTtFQUNFLHNCQUFBO0VBQ0EsZUFBQTtBQWFGO0FBVkE7RUFDRSxVQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsZ0JBQUE7RUFDQSw0Q0FBQTtFQUNBLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QUFhRjtBQVZBO0VBQ0UsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLHdCQUFBO0VBQ0EseUNBQUE7RUFDQSxxQkFBQTtFQUNBLFFBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0FBYUY7QUFWQTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FBYUY7QUFWQTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0Esd0JBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxzQ0FBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0FBYUY7QUFWQTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsZ0NBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FBYUY7QUFWQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUFhRjtBQVZBO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsK0VFaE5LO0FGNk5QO0FBVkE7RUFDRSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSwrRUV0Tks7QUZtT1A7QUFWQTtFQUNFLDBCQUFBO0FBYUY7QUFWQTtFQUNFLFVBQUE7QUFhRjtBQVZBO0VBQ0UsYUFBQTtBQWFGO0FBVkE7RUFDRSx3QkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQWFGO0FBVkE7RUFDRSx3QkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQWFGO0FBVkE7RUFDRSxrQkFBQTtFQUNBLFdBQUE7QUFhRjtBQVZBO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtBQWFGO0FBVkE7RUFDRSxlQUFBO0FBYUY7QUFWQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FBYUY7QUFWQTtFQUNFLDhCQUFBO0VBQ0EsY0FBQTtFQUNBLDBGQUFBO0FBYUY7QUFWQTtFQUNFLGlCQUFBO0VBQ0EsdUZBQUE7RUFDQSw2QkFBQTtFQUNBLDBCQUFBO0FBYUY7QUFWQTtFQUNFLDRCQUFBO0VBQ0EsY0FBQTtFQUNBLDBGQUFBO0FBYUY7QUFWQTtFQUNFLHdCQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtBQWFGO0FBVkE7RUFDRSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7QUFhRjtBQVZBO0VBQ0Usd0JBQUE7QUFhRjtBQVZBO0VBQ0UsdUJBQUE7RUFDQSx3QkFBQTtFQUNBLDBCQUFBO0FBYUY7QUFWQTtFQUNFLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0FBYUY7QUFWQTtFQUNFLHNCQUFBO0VBQ0EsdUJBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtBQWFGO0FBVkE7RUFDRSxzQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQWFGO0FBVkE7RUFDRSxhQUFBO0VBQ0EsNkJBQUE7QUFhRjtBQVZBO0VBQ0UsdUJBQUE7RUFDQSxVQUFBO0FBYUY7QUFWQTtFQUNFLFFBQUE7QUFhRjtBQVZBO0VBQ0UsUUFBQTtBQWFGO0FBVkE7RUFDRSxhQUFBO0FBYUY7QUFWQTtFQUNFLHdCQUFBO0FBYUY7QUFWQTtFQUNFLHdCQUFBO0FBYUY7QUFWQTtFQUNFLG1CQUFBO0VBQ0EsMEJBQUE7QUFhRjtBQVhFO0VBRUUseUNBQUE7QUFZSjtBQVZJO0VBQ0UsZ0JBQUE7QUFZTjtBQVZNO0VBQ0Usb0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSw0RUVuWEk7QUYrWFo7QUFSUTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLDRFRTFYRTtBRm9ZWjtBQVBRO0VBQ0UsY0FBQTtFQUNBLGlCQUFBO0FBU1Y7QUFITTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7QUFLUjtBQUFFO0VBRUUsVUFBQTtFQUNBLGVBQUE7RUFDQSx5Q0FBQTtBQUNKO0FBQ0k7RUFDRSxnQkFBQTtBQUNOO0FBQ007RUFDRSxvQkFBQTtFQUVBLGlCQUFBO0VBQ0EsNEVFMVpJO0FGMFpaO0FBR007RUFFRSxpQkFBQTtFQUNBLCtFRWphRDtBRitaUDtBQVFBO0VBQ0UsbUJBQUE7RUFDQSxpQkFBQTtBQUxGO0FBUUE7RUFDRSxtQkFBQTtBQUxGO0FBUUE7RUFDRSxtQkFBQTtFQUNBLCtCQUFBO0FBTEY7QUFRRTtFQUVFLHlDQUFBO0FBUEo7QUFTSTtFQUNFLGdCQUFBO0FBUE47QUFTTTtFQUNFLG9CQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsNEVFL2JJO0FGd2JaO0FBV1E7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSw0RUV0Y0U7QUY2Ylo7QUFZUTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtBQVZWO0FBZ0JNO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtBQWRSO0FBbUJFO0VBRUUsVUFBQTtFQUNBLGVBQUE7RUFDQSx5Q0FBQTtBQWxCSjtBQW9CSTtFQUNFLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FBbEJOO0FBb0JNO0VBQ0Usb0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSw0RUV4ZUk7QUZzZFo7QUFxQk07RUFDRSxjQUFBO0FBbkJSO0FBOEJFO0VBQ0UsWUFBQTtFQUNBLG1CQUFBO0FBM0JKO0FBOEJFOztFQUVFLGlCQUFBO0FBNUJKO0FBK0JFOztFQUVFLGVBQUE7QUE3Qko7QUF3Q0U7RUFDRSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsUUFBQTtBQXRDSjtBQTZDRTtFQUNFLHlCQUFBO0VBQ0EsZUFBQTtBQTNDSjtBQStDRTtFQUNFLGdCQUFBO0FBN0NKO0FBZ0RFO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtBQTlDSjtBQWlERTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtBQS9DSjtBQWtERTtFQUNFLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFoREo7QUFtREU7RUFDRSxjQUFBO0FBakRKO0FBb0RFO0VBQ0UsaUJBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQWxESjtBQXFERTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLDRCQUFBO0VBQ0Esb0NBQUE7RUFDQSwwQkFBQTtFQUNBLGlCQUFBO0FBbkRKO0FBc0RFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7QUFwREo7QUF1REU7RUFDRSxXQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FBckRKO0FBd0RFO0VBQ0UsWUFBQTtBQXRESjtBQXlERTtFQUNFLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUF2REo7QUEwREU7RUFDRSx1QkFBQTtFQUNBLGlDQUFBO0FBeERKO0FBMkRFO0VBQ0UseUJBQUE7RUFDQSxpQ0FBQTtBQXpESjtBQTRERTtFQUNFLFlBQUE7RUFDQSxZQUFBO0FBMURKO0FBNkRFO0VBQ0UsV0FBQTtFQUNBLGVBQUE7QUEzREo7QUErREE7RUFDRSxhQUFBO0FBNURGO0FBK0RBO0VBQ0UsYUFBQTtFQUNBLHNCQUFBO0FBNURGO0FBK0RBO0VBQ0Usd0JBQUE7QUE1REY7QUErREE7RUFDRSxhQUFBO0FBNURGO0FBK0RBO0VBQ0Usd0JBQUE7RUFDQSxzQkFBQTtBQTVERjtBQStEQTtFQUNFLHdCQUFBO0FBNURGO0FBK0RBO0VBQ0Usc0JBQUE7QUE1REY7QUErREE7RUFDRSx3QkFBQTtBQTVERjtBQStEQTtFQUNFLHdCQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0FBNURGO0FBK0RBO0VBQ0UseUJBQUE7QUE1REY7QUErREE7RUFDRSxjQUFBO0FBNURGO0FBK0RBO0VBQ0UsY0FBQTtBQTVERjtBQXdFQTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0EsZUFBQTtFQUVBLFlBQUE7RUFDQSw4RUU5ckJZO0VGK3JCWix5Q0FBQTtBQXRFRjtBQXlFQTtFQUNFLGlCQUFBO0VBQ0EsK0VFdnNCSztFRndzQkwsV0FBQTtBQXRFRjtBQXlFQTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0FBdEVGO0FBeUVBO0VBQ0UsZUFBQTtFQUNBLFVBQUE7RUFDQSxZQUFBO0FBdEVGO0FBeUVBO0VBQ0UsOEJBQUE7RUFDQSxjQUFBO0FBdEVGO0FBeUVBO0VBQ0UsZUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtBQXRFRjtBQXlFQTtFQUNFLHlCQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7QUF0RUY7QUF5RUE7RUFDRSxnQkFBQTtBQXRFRjtBQXlFQTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBdEVGO0FBeUVBO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7QUF0RUY7QUF5RUE7RUFDRSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7QUF0RUY7QUF5RUE7RUFDRSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtBQXRFRjtBQXlFQTtFQUNFLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBdEVGO0FBeUVBO0VBQ0UsV0FBQTtFQUNBLDJCQUFBO0VBQ0Esc0JBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7QUF0RUY7QUF5RUE7RUFDRSxXQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtBQXRFRjtBQXlFQTtFQUNFLGFBQUE7QUF0RUY7QUF5RUE7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSxjQUFBO0FBdEVGO0FBeUVBO0VBQ0UsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsK0VFM3pCSztBRnF2QlA7QUF5RUE7RUFDRSxhQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUF0RUY7QUF5RUE7RUFDRSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxPQUFBO0FBdEVGO0FBeUVBO0VBQ0UsY0FBQTtFQUNBLDRCQUFBO0VBQ0EsK0VFbjFCSztFRm8xQkwsa0JBQUE7RUFDQSxPQUFBO0VBQ0Esb0JBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7QUF0RUY7QUF3RUU7RUFDRSxlQUFBO0FBdEVKO0FBMEVBO0VBQ0UsNkJBQUE7RUFDQSw0RUVoMkJVO0VGaTJCVixjQUFBO0FBdkVGO0FBMEVBO0VBQ0UsZUFBQTtBQXZFRjtBQTBFQTtFQUVFLDRFRTEyQlU7RUYyMkJWLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUF4RUY7QUE0RUE7RUFDRSxzQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBekVGO0FBaUZFO0VBQ0UsbUJBQUE7QUE5RUo7QUErRUk7RUFDRSxnQ0FBQTtBQTdFTjtBQWtGQTtFQUNFLGlCQUFBO0FBL0VGO0FBa0ZBO0VBQ0UsOEJBQUE7RUFDQSw0RUUxNEJVO0VGMjRCViw2QkFBQTtBQS9FRjtBQWtGQTtFQUVFLDhCQUFBO0VBRUEsNEVFbDVCVTtBRmkwQlo7QUFvRkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7QUFqRkY7QUFvRkE7RUFDRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0Esd0NBQUE7QUFqRkY7QUFvRkE7RUFDRSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBakZGO0FBb0ZBO0VBRUUsV0FBQTtFQUNBLGtCQUFBO0FBbEZGO0FBcUZBO0VBQ0UsbUJBQUE7QUFsRkY7QUFxRkE7RUFDRSxjQUFBO0FBbEZGO0FBcUZBO0VBQ0UsbUJHdDhCUTtBSG8zQlY7QUFxRkE7RUFDRSxjRzE4QlE7QUh3M0JWO0FBcUZBO0VBQ0UsbUJBQUE7QUFsRkY7QUFxRkE7RUFDRSxjQUFBO0FBbEZGO0FBcUZBO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsK0VFcDlCSztFRnE5QkwsU0FBQTtFQUNBLG1CQUFBO0FBbEZGO0FBb0ZFO0VBQ0UsY0FBQTtBQWxGSjtBQXNGQTtFQUNFLHFCQUFBO0FBbkZGO0FBd0ZJO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSwrRUV0K0JDO0VGdStCRCxnQkFBQTtBQXJGTjtBQXdGSTtFQUNFLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FBdEZOO0FBeUZJO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSwrRUVuL0JDO0FGNDVCUDtBQTRGSTtFQUNFLCtCQUFBO0FBMUZOO0FBNEZNO0VBQ0UsY0FBQTtFQUNBLCtFRTcvQkQ7RUY4L0JDLGlCQUFBO0FBMUZSO0FBNkZNO0VBQ0UsY0FBQTtFQUNBLCtFRW5nQ0Q7RUZvZ0NDLGlCQUFBO0FBM0ZSO0FBZ0dNO0VBQ0UsY0FBQTtFQUNBLCtFRTNnQ0Q7RUY0Z0NDLGlCQUFBO0FBOUZSO0FBaUdNO0VBQ0UsY0FBQTtFQUNBLCtFRWpoQ0Q7RUZraENDLGlCQUFBO0FBL0ZSO0FBbUdJO0VBQ0UsK0JBQUE7QUFqR047QUFtR007RUFDRSxjQUFBO0VBQ0EsK0VFM2hDRDtFRjRoQ0MsaUJBQUE7QUFqR1I7QUFvR007RUFDRSxjQUFBO0VBQ0EsK0VFamlDRDtFRmtpQ0MsaUJBQUE7QUFsR1I7QUF1R007RUFDRSxjQUFBO0VBQ0EsK0VFemlDRDtFRjBpQ0MsaUJBQUE7QUFyR1I7QUF3R007RUFDRSxjQUFBO0VBQ0EsK0VFL2lDRDtFRmdqQ0MsZUFBQTtBQXRHUjtBQThHRTtFQUNFLGNBQUE7QUEzR0o7QUE2R0k7RUFDRSxpQkFBQTtFQUNBLGNBQUE7RUFDQSwrRUU5akNDO0VGK2pDRCxnQkFBQTtBQTNHTjtBQThHSTtFQUNFLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FBNUdOO0FBK0dJO0VBQ0UsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsK0VFM2tDQztBRjg5QlA7QUFxSEk7RUFDRSwrQkFBQTtBQW5ITjtBQXFITTtFQUNFLGNBQUE7RUFDQSw0RUV2bENJO0VGd2xDSixpQkFBQTtBQW5IUjtBQXNITTtFQUNFLGNBQUE7RUFDQSwrRUU5bENEO0VGK2xDQyxpQkFBQTtBQXBIUjtBQXlITTtFQUNFLGNBQUE7RUFDQSw0RUVybUNJO0VGc21DSixpQkFBQTtBQXZIUjtBQTBITTtFQUNFLGNBQUE7RUFDQSwrRUU1bUNEO0VGNm1DQyxpQkFBQTtBQXhIUjtBQTRISTtFQUNFLCtCQUFBO0FBMUhOO0FBNEhNO0VBQ0UsY0FBQTtFQUNBLDRFRXJuQ0k7RUZzbkNKLGlCQUFBO0FBMUhSO0FBNkhNO0VBQ0UsY0FBQTtFQUNBLCtFRTVuQ0Q7RUY2bkNDLGlCQUFBO0FBM0hSO0FBZ0lNO0VBQ0UsY0FBQTtFQUNBLDRFRW5vQ0k7RUZvb0NKLGlCQUFBO0FBOUhSO0FBaUlNO0VBQ0UsY0FBQTtFQUNBLCtFRTFvQ0Q7RUYyb0NDLGlCQUFBO0FBL0hSO0FBdUlBO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSx3Q0FBQTtFQUNBLE1BQUE7RUFDQSxlQUFBO0FBcElGO0FBdUlBO0VBQ0UsOEJBQUE7RUFDQSxnQ0FBQTtFQUNBLGFBQUE7QUFwSUY7QUF5SUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSwrRUV2cUNLO0VGd3FDTCxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7QUF0SUY7QUF5SUE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFFQSxvQkFBQTtFQUNBLG1CQUFBO0FBdklGO0FBeUlFO0VBQ0UsbUJBQUE7QUF2SUo7QUEySUE7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSwrRUUvckNLO0VGZ3NDTCxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0FBeElGO0FBMklBO0VBQ0UsV0dyc0NLO0VIc3NDTCxlQUFBO0VBQ0EsOEVFcnNDWTtFRnNzQ1osU0FBQTtBQXhJRjtBQWdKQTtFQUNFLGtCQUFBO0FBN0lGO0FBK0lFO0VBRUUscUJBQUE7QUE5SUo7QUFrSkE7RUFDRSxtQkFBQTtBQS9JRjtBQWtKQTtFQUNFLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0FBL0lGO0FBaUpFO0VBQ0UsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQS9JSjtBQW1KQTtFQUNFLDZCQUFBO0FBaEpGO0FBbUpBO0VBQ0UsZ0NBQUE7QUFoSkY7QUFtSkE7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBQWhKRjtBQWtKRTtFQUNFLFlBQUE7RUFDQSxxQkFBQTtFQUNBLHNCQUFBO0VBQ0EscUJBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7QUFoSko7QUFvSkE7RUFDRSxjR3h3Q0k7RUh5d0NKLGlCQUFBO0VBQ0EsK0VFeHdDSztFRnl3Q0wsU0FBQTtBQWpKRjtBQW9KQTtFQUNFLHlCQUFBO0VBQ0EsNEJBQUE7RUFDQSwwRkFBQTtFQUNBLG9CQUFBO0VBQ0EsNkJBQUE7QUFqSkY7QUFvSkE7RUFDRSxjR3h4Q0s7RUh5eENMLGlCQUFBO0VBQ0EsNEVFdHhDVTtFRnV4Q1YsU0FBQTtBQWpKRjtBQW9KQTtFQUNFLDZCQUFBO0VBQ0EsOEJBQUE7QUFqSkY7QUFvSkE7RUFDRSxnQ0FBQTtBQWpKRjtBQW9KQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0FBakpGO0FBb0pBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsK0VFL3lDSztFRmd6Q0wsU0FBQTtBQWpKRjtBQW9KQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLCtFRXR6Q0s7QUZxcUNQO0FBb0pBO0VBQ0UsaUJBQUE7RUFDQSxvQkFBQTtBQWpKRjtBQW9KQTtFQUNFLFdBQUE7QUFqSkY7QUFvSkE7RUFDRSxvQkFBQTtFQUNBLHFCQUFBO0VBQ0EsNEJBQUE7RUFDQSxnQkFBQTtBQWpKRjtBQW9KQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSw0RUU1MENVO0VGNjBDVixrQkFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQWpKRjtBQW9KQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSw2QkFBQTtFQUNBLCtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUNBQUE7RUFDQSxjQUFBO0VBQ0EsNEVFNzFDVTtBRjRzQ1o7QUFvSkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLHNCQUFBO0VBQ0EsNEJBQUE7RUFDQSxnQ0FBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLHlDQUFBO0VBQ0EsY0FBQTtFQUNBLDRFRTMyQ1U7QUYwdENaO0FBMEpBO0VBQ0UsaUJBQUE7RUFDQSwrRUV2M0NLO0VGdzNDTCxZQUFBO0VBQ0EsV0FBQTtBQXZKRjtBQTJKRTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSw0RUVoNENRO0FGd3VDWjtBQTBKSTtFQUNFLFdBQUE7RUFDQSxjQUFBO0FBeEpOO0FBOEpFO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxpQkFBQTtBQTVKSjtBQThKSTtFQUNFLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxhQUFBO0FBNUpOO0FBZ0tFO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtBQTlKSjtBQWdLSTtFQUNFLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0FBOUpOO0FBaUtJO0VBQ0UsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUEvSk47QUFrS0k7RUFDRSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtBQWhLTjtBQXNLSTtFQUNFLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0FBcEtOO0FBeUtJO0VBQ0UsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUF2S047QUEwS0k7RUFDRSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsV0FBQTtBQXhLTjtBQTRLRTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSwrRUVoOUNHO0FGc3lDUDtBQTZLRTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSwrRUV2OUNHO0FGNHlDUDtBQThLRTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSw0RUU3OUNRO0FGaXpDWjtBQStLRTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtFQUNBLCtFRXArQ0c7RUZxK0NILGNBQUE7QUE3S0o7QUFpTEU7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsK0VFNytDRztBRjh6Q1A7QUFrTEU7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsK0VFcC9DRztBRm8wQ1A7QUFvTEE7RUFDRSxrQkFBQTtFQUNBLG1CQUFBO0FBakxGO0FBb0xBO0VBQ0UsVUFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtBQWpMRjtBQW1MRTtFQUNFLG1CQUFBO0VBQ0EsNEVFeGdEUTtBRnUxQ1o7QUFtTEk7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0FBakxOO0FBbUxNO0VBQ0Usc0JBQUE7RUFDQSxxQkFBQTtBQWpMUjtBQW9MTTtFQUVFLGlCQUFBO0VBQ0EsMEJBQUE7RUFFQSxzQkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFFQSwwQkFBQTtBQXJMUjtBQXdMTTtFQUNFLGNBQUE7QUF0TFI7QUEyTEU7RUFDRSxtQkFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQXpMSjtBQTJMSTtFQUNFLG1CQUFBO0FBekxOO0FBNkxFO0VBQ0Usb0JBQUE7QUEzTEo7QUErTEE7RUFDRSx3Q0FBQTtBQTVMRjtBQThMRTtFQUNFLDBCQUFBO0VBQ0EsMkJBQUE7QUE1TEo7QUFnTUE7RUFDRSxtQkczakRRO0VINGpEUixrQkFBQTtFQUNBLG1CQUFBO0FBN0xGO0FBZ01JO0VBQ0UsbUJBQUE7RUFDQSw0RUV0a0RNO0VGdWtETixjRzFrREE7RUgya0RBLGdCQUFBO0FBOUxOO0FBaU1JO0VBQ0UsbUJBQUE7RUFDQSwrRUU5a0RDO0VGK2tERCxjQUFBO0FBL0xOO0FBbU1FO0VBQ0UsWUFBQTtFQUNBLHFCQUFBO0FBak1KO0FBcU1BO0VBQ0UsY0FBQTtBQWxNRjtBQXVNQTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsTUFBQTtBQXBNRjtBQXVNQTtFQUNFLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFFBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7QUFwTUY7QUEwTUU7O0VBQ0Usa0JBQUE7RUFDQSxhQUFBO0FBdE1KO0FBeU1FOztFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxjQUFBO0VBQ0EsTUFBQTtBQXRNSjtBQXlNRTs7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSwrRUUzb0RHO0FGcThDUDtBQXlNRTs7RUFDRSxxQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxRQUFBO0VBQ0EsaUJBQUE7RUFDQSxhQUFBO0FBdE1KO0FBME1FOztFQUNFLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0FBdk1KO0FBa05FOztFQUNFLGtCQUFBO0VBQ0EsYUFBQTtBQTlNSjtBQWlORTs7RUFDRSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0FBOU1KO0FBaU5FOztFQUNFLGNBQUE7RUFDQSxpQkFBQTtFQUNBLCtFRTlyREc7QUZnL0NQO0FBaU5FOztFQUNFLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFFBQUE7RUFDQSxpQkFBQTtFQUNBLGFBQUE7QUE5TUo7QUFrTkU7O0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FBL01KO0FBbU5BO0VBQ0Usb0NBQUE7RUFDQSxnQ0FBQTtBQWhORjtBQW1OQTtFQUNFLGNBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QUFoTkY7QUFtTkE7RUFDRSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLDJCQUFBO0VBQ0EsYUFBQTtBQWhORjtBQW1OQTtFQUlFLHNCQUFBO0VBQ0EsYUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLDJCQUFBO0VBQ0EsU0FBQTtBQWpORjtBQW9OQTtFQUNFLGdCQUFBO0VBQ0EsMkJBQUE7QUFqTkY7QUFvTkE7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSw0REFBQTtFQUNBLE9BQUE7QUFqTkY7QUFtTkU7RUFFRSxnQ0FBQTtBQWxOSjtBQXFORTtFQUNFLGVBQUE7QUFuTko7QUF1TkE7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0Esd0JBQUE7RUFDQSxnQ0FBQTtFQUNBLE9BQUE7QUFwTkY7QUF1TkE7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0EsMkJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsZ0NBQUE7QUFwTkY7QUF1TkE7RUFDRSw0REFBQTtBQXBORjtBQXNORTtFQUVFLDhCQUFBO0FBck5KO0FBeU5JO0VBQ0UsV0FBQTtBQXZOTjtBQTBOSTtFQUNFLGVBQUE7QUF4Tk47QUE2TkE7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxlQUFBO0FBMU5GO0FBOE5BLHNCQUFBO0FBQ0E7RUFDRSwyQkFBQTtFQUNBLDREQUFBO0FBM05GO0FBOE5BLGNBQUE7QUFDQTtFQUNFLGNBQUE7QUEzTkY7QUE4TkEsb0JBQUE7QUFpREE7RUFDRSxvQkFBQTtBQTNRRjtBQThRQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtBQTNRRjtBQThRQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtBQTNRRjtBQThRQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtBQTNRRjtBQThRQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0FBM1FGO0FBOFFBO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQTNRRjtBQThRQTtFQUNFLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQTNRRjtBQThRQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFFBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBM1FGO0FBOFFBO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtBQTNRRjtBQThRQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLDJCQUFBO0FBM1FGO0FBOFFBO0VBQ0Usa0JBQUE7QUEzUUY7QUE4UUE7RUFDRSx5QkFBQTtBQTNRRjtBQThRQTtFQUNFO0lBRUUsVUFBQTtJQUNBLGtCQUFBO0VBNVFGO0VBK1FBO0lBRUUsVUFBQTtJQUNBLGlCQUFBO0lBQ0Esa0JBQUE7RUE5UUY7QUFDRjtBQWlSQTtFQUNFLHNCQUFBO0FBL1FGO0FBa1JBO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsU0FBQTtFQUNBLDJCQUFBO0VBQ0EsZUFBQTtFQUNBLFVBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBL1FGO0FBa1JBO0VBQ0UsMEJBQUE7RUFDQSxrQ0FBQTtFQUNBLCtCQUFBO0VBQ0EsNkJBQUE7RUFDQSw4QkFBQTtBQS9RRjtBQWtSQTtFQUNFO0lBQ0UsVUFBQTtFQS9RRjtFQWtSQTtJQUNFLFVBQUE7RUFoUkY7QUFDRjtBQTJUQTtFQUNFLFlBQUE7RUFDQSxtQkFBQTtBQXpSRjtBQTRSQTs7RUFFRSxpQkFBQTtBQXpSRjtBQTRSQTs7RUFFRSxlQUFBO0FBelJGO0FBb1NBO0VBQ0UsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7QUFqU0Y7QUF3U0E7RUFDRSx5QkFBQTtFQUNBLGVBQUE7QUFyU0Y7QUF5U0E7RUFDRSxnQkFBQTtBQXRTRjtBQXlTQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7QUF0U0Y7QUE0U0E7RUFDRSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtFQUNBLFlBQUE7QUF6U0Y7QUE0U0E7RUFDRSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBelNGO0FBNFNBO0VBQ0UsY0FBQTtBQXpTRjtBQTRTQTtFQUNFLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUF6U0Y7QUE0U0E7RUFDRSxnQkFBQTtFQUNBLFlBQUE7RUFDQSw0QkFBQTtFQUNBLG9DQUFBO0VBQ0EsMEJBQUE7RUFDQSxpQkFBQTtBQXpTRjtBQTRTQTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0EsUUFBQTtFQUNBLGtCQUFBO0FBelNGO0FBNFNBO0VBQ0UsV0FBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQXpTRjtBQTRTQTtFQUNFLFlBQUE7QUF6U0Y7QUE0U0E7RUFDRSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FBelNGO0FBNFNBO0VBQ0UsdUJBQUE7RUFDQSxpQ0FBQTtBQXpTRjtBQTRTQTtFQUNFLHlCQUFBO0VBQ0EsaUNBQUE7QUF6U0Y7QUE0U0E7RUFDRSxZQUFBO0VBQ0EsWUFBQTtBQXpTRjtBQTRTQTtFQUNFLFdBQUE7RUFDQSxlQUFBO0FBelNGO0FBNFNBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0EseUJBQUE7QUF6U0Y7QUE0U0E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBelNGO0FBNFNBO0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0FBelNGO0FBNFNBO0VBQ0UsZ0JBQUE7QUF6U0Y7QUE0U0E7RUFDRSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FBelNGO0FBNFNBO0VBQ0UseUJBQUE7RUFDQSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtBQXpTRjtBQTRTQTtFQUNFLDZCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtBQXpTRjtBQTRTQTtFQUNFLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUF6U0Y7QUE0U0E7RUFDRSxnQkFBQTtBQXpTRjtBQTRTQTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0FBelNGO0FBNFNBO0VBQ0UsaUJBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtBQXpTRjtBQTRTQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUF6U0Y7QUE0U0E7RUFDRSxhQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0VBQ0EsVUFBQTtFQUNBLHdDQUFBO0FBelNGO0FBNFNBO0VBQ0UsYUFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7RUFDQSxpQkFBQTtFQUNBLFVBQUE7QUF6U0Y7QUE0U0E7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUF6U0Y7QUE0U0E7RUFDRSxjQUFBO0FBelNGO0FBNFNBO0VBQ0UsZUFBQTtBQXpTRjtBQTRTQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtFQUNBLGlDQUFBO0FBelNGO0FBNFNBO0VBQ0UsZ0NBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUNBQUE7QUF6U0Y7QUE0U0E7RUFDRSw4QkFBQTtBQXpTRjtBQTRTQTtFQUNFLGNBQUE7QUF6U0Y7QUE0U0E7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtBQXpTRjtBQTRTQTtFQUNFLGVBQUE7RUFDQSxtQkFBQTtBQXpTRjtBQTRTQTtFQUNFLDBCQUFBO0VBQ0EsdUJBQUE7QUF6U0Y7QUE4U0U7RUFDRSxjQUFBO0FBM1NKO0FBOFNFO0VBQ0UsbUJBQUE7RUFDQSxvQkFBQTtBQTVTSjtBQWtUQTtFQUNFLHlCQUFBO0FBL1NGO0FBaVRFO0VBRUUsK0VFdjBFRztFRncwRUgsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUFoVEo7QUFtVEU7RUFDRSxpQkFBQTtFQUNBLCtFRS8wRUc7RUZnMUVILGNBQUE7QUFqVEo7QUFvVEU7RUFDRSxpQkFBQTtFQUNBLCtFRXIxRUc7RUZzMUVILGNBQUE7QUFsVEo7QUFxVEU7RUFDRSxpQkFBQTtFQUNBLCtFRTMxRUc7RUY0MUVILGNBQUE7QUFuVEo7QUFzVEU7RUFDRSxpQkFBQTtFQUNBLCtFRWoyRUc7RUZrMkVILGNBQUE7QUFwVEo7QUF1VEU7RUFDRSxpQkFBQTtFQUNBLCtFRXYyRUc7RUZ3MkVILGNBQUE7QUFyVEo7QUF3VEU7RUFDRSxpQkFBQTtFQUNBLCtFRTcyRUc7RUY4MkVILGNBQUE7QUF0VEo7QUE0VEU7OztFQUdFLHNCQUFBO0VBQ0EsYUFBQTtBQXpUSjtBQWlVRTtFQUNFLG1CQU5PO0VBT1AsK0NBQUE7RUFDQSxtQ0FBQTtBQS9USjtBQWtVRTtFQUNFLG1CQUFBO0FBaFVKO0FBbVVFO0VBQ0UsbUJBQUE7QUFqVUo7QUFxVUU7RUFZRSxZQUFBO0VBQ0EsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0FBOVVKO0FBZ1ZJO0VBQ0Usa0JBQUE7RUFDQSxPQUFBO0VBQ0EsTUFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0VBQ0EsZUFBQTtBQTlVTjtBQWlWSTtFQUNFLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLE1BQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLFlBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7QUEvVU47QUFpVk07RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FBL1VSO0FBaVZRO0VBQ0UsUUFBQTtFQUNBLGFBQUE7RUFDQSx5Q0FBQTtBQS9VVjtBQWtWUTtFQUNFLE9BQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSwwQ0FBQTtBQWhWVjtBQXVWRTtFQUNFO0lBQ0UsNkJBQUE7RUFyVko7QUFDRjtBQXdWRTtFQUNFO0lBQ0UsNEJBQUE7RUF0Vko7QUFDRjtBQThWRTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUEzVko7QUErVkE7RUFFRSw0Q0FBQTtBQTdWRjtBQStWRTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQTdWSjtBQWdXRTtFQUNFLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FBOVZKO0FBa1dBO0VBQ0UsdUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLCtFQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLHFCQUFBO0VBQ0EsOEJBQUE7RUFDQSxvQ0FBQTtFQUNBLGtCQUFBO0VBRUMsZ0JBQUE7RUFDRCxnQkFBQTtFQUNBLDRCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0FBaFdGO0FBbVdBO0VBQ0UsY0FBQTtBQWhXRjtBQW1XQTtFQUNFLGFBQUE7QUFoV0Y7QUFtV0E7RUFDRSxVQUFBO0VBQ0EsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLDBCQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw0Q0FBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtBQWhXRjtBQW1XQTtFQUNFLGtCQUFBO0VBQ0EsMEJBQUE7RUFDQSxVQUFBO0VBQ0EsZUFBQTtFQUNBLFlBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSw0Q0FBQTtFQUNBLGVBQUE7RUFDQSx5QkFBQTtBQWhXRjtBQW1XQTtFQUNFLGNBQUE7RUFDQSx3QkFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FBaFdGO0FBbVdBO0VBQ0Usd0JBQUE7RUFDQSxnQkFBQTtBQWhXRjtBQWtXRTtFQUNFLHlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtBQWhXSjtBQW1XRTtFQUNFLDRCQUFBO0VBQ0EsMEJBQUE7RUFDQSx1QkFBQTtFQUNBLG9FQUFBO0VBQ0EseUJBQUE7QUFqV0o7QUFtV0k7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0FBaldOO0FBc1dBO0VBQ0UsY0FBQTtFQUNBLHdCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxZQUFBO0FBbldGO0FBdVdBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUFwV0Y7QUF1V0E7RUFDRSxpQkFBQTtFQUNBLGlCQUFBO0FBcFdGO0FBNFdBO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUF6V0Y7QUE0V0E7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUVBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7RUFDQSx5Q0FBQTtBQTFXRjtBQTZXQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFHQSxrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsNEVFL29GVTtBRm15RVo7QUErV0E7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFDQSwrQkFBQTtBQTVXRjtBQThXRTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFFBQUE7RUFDQSwyQkFBQTtBQTVXSjtBQWlYQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFNBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7QUE5V0Y7QUFnWEU7RUFDRSxhQUFBO0FBOVdKO0FBa1hBO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0FBL1dGO0FBa1hBO0VBQ0UsWUFBQTtFQUNBLFlBQUE7QUEvV0Y7QUFrWEE7RUFDRSxRQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQS9XRjtBQWtYQTtFQUNFLHFCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZUFBQTtBQS9XRjtBQW1YQTtFQUNFLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtBQWhYRjtBQW9YQTtFQUNFLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtBQWpYRjtBQW9YQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxjQUFBO0FBalhGO0FBb1hBO0VBQ0UsaUJBQUE7RUFDQSwrRUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0FBalhGO0FBb1hBO0VBQ0UsaUJBQUE7RUFDQSwrRUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSxTQUFBO0FBalhGO0FBb1hBO0VBQ0UsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUFqWEY7QUFvWEE7RUFDRSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esd0JBQUE7RUFDQSxxQkFBQTtFQUNBLFFBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBQWpYRjtBQXFYQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGNBQUE7QUFsWEY7QUFxWEE7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0FBbFhGO0FBcVhBO0VBSUUsY0FBQTtBQXJYRjtBQXVYRTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxRQUFBO0VBQ0EsaUJBQUE7QUFyWEo7QUF3WEU7RUFDRSxpQkFBQTtBQXRYSjtBQTBYQTtFQUdFLGVBQUE7RUFDQSx5QkFBQTtBQXpYRjtBQTJYRTtFQUNFLHFCQUFBO0VBQ0Esc0JBQUE7QUF6WEo7QUE2WEE7RUFTRSx5QkFBQTtFQUVBLHlCQUFBO0VBQ0EsMEJBQUE7RUFDQSxZQUFBO0FBbllGO0FBcVlFO0VBQ0UsdUJBQUE7QUFuWUo7QUFzWUU7RUFVRSxjQUFBO0VBRUEsUUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFVBQUE7QUE5WUo7QUFpWkU7RUFDRSxlQUFBO0VBQ0EsV0FBQTtBQS9ZSjtBQWtaRTtFQUNFLGNBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7QUFoWko7QUF1WkU7RUFDRSxlQUFBO0VBQ0EsZUFBQTtBQXJaSjtBQXdaRTtFQUNFLGlCQUFBO0FBdFpKO0FBd1pJO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBdFpOO0FBd1pNO0VBQ0UsaUJBQUE7QUF0WlI7QUEwWkk7RUFDRSxjQUFBO0VBQ0EsZ0JBQUE7QUF4Wk47QUE0WkU7RUFDRSxpQkFBQTtBQTFaSjtBQTZaRTtFQUNFLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSx1QkFBQTtBQTNaSjtBQTZaSTtFQUNFLFdBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7QUEzWk47QUFrYUE7RUFDRSxZQUFBO0FBL1pGO0FBa2FBO0VBQ0U7SUFDRSxZQUFBO0VBL1pGOztFQWlhQTtJQUNFLFNBQUE7RUE5WkY7O0VBZ2FBO0lBQ0UsZUFBQTtJQUNBLGlCQUFBO0VBN1pGOztFQWdhQTtJQUVFLFdBQUE7SUFDQSxpQkFBQTtFQTlaRjs7RUFpYUU7SUFDRSxRQUFBO0VBOVpKOztFQWthQTtJQUNFLHFCQUFBO0VBL1pGO0FBQ0Y7QUFnYUM7RUFHQyxjQUFBO0VBS0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsY0FBQTtBQXBhRjtBQXNhRTtFQUdFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLGlCQUFBO0FBdGFKO0FBMGFFO0VBQ0UsaUJBQUE7QUF4YUo7QUE0YUE7RUFDRSxjQUFBO0VBQ0EsMEJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0FBemFGO0FBNmFBO0VBRUUsUUFBQTtFQUVBLGlCQUFBO0VBQ0Esa0JBQUE7QUE1YUY7QUErYUE7RUFDRSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSwwQkFBQTtBQTVhRjtBQSthQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUVBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUE3YUY7QUFnYkE7RUFDRSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxTQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBN2FGO0FBZ2JBO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtBQTdhRjtBQWdiQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsYUFBQTtBQTdhRjtBQSthRTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0FBN2FKO0FBa2JBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7QUEvYUY7QUFtYkE7RUFDRSwyQkFBQTtFQUNBLHFCQUFBO0FBaGJGO0FBbWJBO0VBR0Usa0JBQUE7RUFDQSxVQUFBO0VBQ0EsUUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0FBbGJGO0FBcWJBO0VBQ0UsV0FBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLGtCQUFBO0FBbGJGO0FBcWJBO0VBRUUsV0FBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtBQW5iRjtBQXNiQTtFQUNFLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFFQSxtQkFBQTtBQXBiRjtBQXViQTtFQUNFLHFCQUFBO0VBQ0EsbUJBQUE7RUFFQSxxQkFBQTtFQUNBLDJCQUFBO0FBcmJGO0FBd2JBO0VBSUUseUJBQUE7QUF4YkY7QUFxYkU7RUFDRSw2QkFBQTtBQW5iSjtBQXViRTtFQUNFLG1CQUFBO0FBcmJKO0FBdWJJO0VBQ0UsVUFBQTtFQUNBLHNCQUFBO0FBcmJOO0FBdWJNO0VBQ0UsV0FBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FBcmJSO0FBeWJFO0VBQ0UsMkJBQUE7QUF2Yko7QUEyYkE7RUFDRSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsNEJBQUE7QUF4YkY7QUEyYkE7RUFDRSw2QkFBQTtFQUNBLGlCQUFBO0VBQ0EscUJBQUE7QUF4YkY7QUEyYkE7RUFDRSx5QkFBQTtBQXhiRjtBQTJiQTtFQUNFLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLFNBQUE7RUFDQSxnQ0FBQTtBQXhiRjtBQTJiQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7QUF4YkY7QUEyYkE7RUFHRSxjQUFBO0VBR0EsNEJBQUE7RUFDQSwyQkFBQTtBQTViRjtBQWtjRTtFQUNFLHdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUdFLGVBQUE7RUFDQSxpQkFBQTtBQWxjTjtBQXNjRTtFQUNFLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FBcGNKO0FBc2NJO0VBQ0UsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpREFBQTtBQXBjTjtBQTBjRTtFQUNFLGlCQUFBO0VBQ0EseUJBQUE7RUFDQSxZQUFBO0FBdmNKO0FBeWNJO0VBQ0UsV0FBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpREFBQTtBQXZjTjtBQTRjQTtFQUNFLHFCQUFBO0VBQ0EsbUNBQUE7QUF6Y0Y7QUE2Y0E7RUFDRSwwQkFBQTtFQUNBLG1DQUFBO0FBMWNGO0FBOGNBO0VBQ0UseUJBQUE7RUFDQSxtQ0FBQTtBQTNjRjtBQStjQTtFQUVFO0lBQ0UsYUFBQTtFQTdjRjtBQUNGO0FBaWRBO0VBT0U7SUFBTSxxQ0FBQTtFQXBkTjtBQUNGO0FBc2RBO0VBT0U7SUFBTSxvQ0FBQTtFQXpkTjtBQUNGO0FBMmRBO0VBT0U7SUFBTSw0Q0FBQTtFQTlkTjtBQUNGO0FBZ2VBO0VBQ0UsY0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBQTlkRjtBQWtlQTtFQUNFLHVCQUFBO0FBL2RGO0FBa2VBO0VBQ0UsdUJBQUE7QUEvZEY7QUFnZUU7RUFDRSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0JBQUE7QUE5ZEo7QUFrZUE7RUFDRSxxQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBS0EsZ0JBQUE7QUFuZUY7QUFzZUE7RUFDRSxtQkFBQTtBQW5lRjtBQXNlQTtFQUNFLHFCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQW5lRjtBQXNlQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLGFBQUE7QUFuZUY7QUFzZUE7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsYUFBQTtBQW5lRjtBQXNlQTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLGFBQUE7QUFuZUY7QUFzZUE7RUFFRSw0QkFBQTtFQUNBLDJCQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7QUFwZUY7QUF1ZUE7RUFDRSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUFwZUY7QUF1ZUE7RUFDRSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBQ0EsT0FBQTtFQUNBLFlBQUE7QUFwZUY7QUF1ZUE7RUFDRSwrRUV0MEdLO0VGdTBHTCxpQkFBQTtFQUNBLGNBQUE7QUFwZUY7QUF5ZUU7RUFDRSxVQUFBO0VBQ0Esa0JBQUE7RUFFQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUF2ZUo7QUE2ZUU7RUFDRSxxQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUExZUo7QUFnZkE7RUFDRSxnRUFBQTtFQUNBLDhCQUFBO0FBN2VGO0FBZ2ZBO0VBQ0Usb0JBQUE7RUFDQSw4QkFBQTtFQUNBLDJCQUFBO0VBQ0EsaUNBQUE7RUFDQSxnRUFBQTtFQUNBLGtCQUFBO0VBQ0EsOEJBQUE7RUFDQSxhQUFBO0FBN2VGO0FBaWZFO0VBQ0UscUJBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxnRUFBQTtFQUNBLGFBQUE7QUE5ZUo7QUFtZkU7RUFDRSxxQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUFoZko7QUFzZkE7RUFDSSxxQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUFuZko7QUF3ZkE7RUFFRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdFQUFBO0FBdGZGO0FBMmZFO0VBQ0UscUJBQUE7RUFDQSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdFQUFBO0FBeGZKO0FBK2ZFO0VBQ0UscUJBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxnRUFBQTtBQTVmSjtBQW1nQkU7RUFDRSxxQkFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGdFQUFBO0FBaGdCSjtBQXVnQkU7RUFDRSxxQkFBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUFwZ0JKO0FBMGdCQTtFQUVFLGtGQUFBO0VBQ0EsNkJBQUE7RUFDQSx5QkFBQTtBQXhnQkY7QUE0Z0JBO0VBQ0UsZ0JBQUE7RUFDQSxnRUFBQTtFQWVFLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBdmhCSjtBQXNnQkU7RUFDRSw0QkFBQTtBQXBnQko7QUF1Z0JFO0VBQ0UsNkJBQUE7RUFDQSxhQUFBO0FBcmdCSjtBQW1oQkk7RUFDRSw0RUU5K0dNO0VGKytHTixlQUFBO0VBQ0EsMEJBQUE7RUFDQSx1QkFBQTtFQUNBLGNBQUE7QUFqaEJOO0FBb2hCSTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBQWxoQk47QUFxaEJJO0VBQ0UsV0FBQTtFQUNBLFVBQUE7RUFDQSxrQkFBQTtFQUNBLFFBQUE7RUFDQSxTQUFBO0FBbmhCTjtBQXNpQkk7RUFDTSwyQkFBQTtBQXBpQlY7QUF3aUJBO0VBQ0UsZ0JBQUE7RUFDQSxnRUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQXJpQkY7QUF1aUJFO0VBQ0Usc0JBQUE7RUFDQSxtQkFBQTtBQXJpQko7QUF5aUJNO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUF2aUJSO0FBMGlCTTtFQUNFLGlCQUFBO0VBQ0Esb0JBQUE7QUF4aUJSO0FBMmlCTTtFQUNFLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7RUFDQSxpQkFBQTtBQXppQlI7QUE4aUJNO0VBRUUscUJBQUE7RUFDQSxzQkFBQTtFQUVBLGlCQUFBO0VBQ0EsZ0JBQUE7QUE5aUJSO0FBaWpCTTtFQUNFLGdCQUFBO0VBQ0EsZUFBQTtBQS9pQlI7QUFxakJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxnQkFBQTtBQWxqQkY7QUFxakJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0FBbGpCRjtBQXFqQkE7RUFDRSwyQkFBQTtFQUNBLFlBQUE7QUFsakJGO0FBcWpCQTtFQUNFLGFBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUFsakJGO0FBcWpCQTtFQUNFLGFBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUFsakJGO0FBcWpCQTtFQUNFLGFBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUFsakJGO0FBcWpCQTtFQUNFLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLHFCQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUFsakJGO0FBcWpCQTtFQUVFLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5QkFBQTtBQW5qQkY7QUEyakJBO0VBQ0UsY0FBQTtFQUNFLDRCQUFBO0VBQ0Ysb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FBeGpCRjtBQTJqQkE7RUFDRSxjQUFBO0VBQ0UsNEJBQUE7RUFDRixvQkFBQTtFQUVBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBQXpqQkY7QUE0akJBO0VBQ0UsY0FBQTtFQUNBLG9CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUF6akJGO0FBNGpCQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUVBLFNBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0FBMWpCRjtBQTZqQkE7RUFDRSxpQkFBQTtFQUNBLHVGQUFBO0VBQ0EsNEJBQUE7RUFDQSwwQkFBQTtBQTFqQkY7QUE2akJBO0VBQ0UsY0FBQTtFQUNBLDBCQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtFQUNBLFlBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0VBQ0EsVUFBQTtBQTFqQkY7QUE2akJBO0VBQ0Usa0JBQUE7RUFDQSwrRUV2dEhLO0FGNnBHUDtBQTRqQkE7RUFDRSxpQ0FBQTtBQXpqQkY7QUEwakJFO0VBQ0ksZ0JBQUE7QUF4akJOO0FBMmpCQTtFQUNHLGlCQUFBO0FBeGpCSDtBQTBqQkE7RUFDRSxrQkFBQTtBQXZqQkY7QUF5akJBO0VBQ0UscUJBQUE7RUFDQSx5QkFBQTtBQXRqQkY7QUF5akJBO0VBQ0UscUJBQUE7RUFDQSx5QkFBQTtBQXRqQkY7QUF5akJBO0VBQ0UscUJBQUE7RUFDQSx5QkFBQTtBQXRqQkY7QUF5akJBO0VBQ0UscUJBQUE7RUFDQSx5QkFBQTtBQXRqQkY7QUF5akJBO0VBQ0UsdUJBQUE7QUF0akJGO0FBeWpCQTtFQUNFLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0FBdGpCRjtBQXlqQkE7RUFDRSw0RUVyd0hVO0VGc3dIVixlQUFBO0VBQ0EsY0FBQTtBQXRqQkY7QUF5akJBO0VBQ0Usa0ZBQUE7QUF0akJGO0FBeWpCQTtFQUNFLGFBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7QUF0akJGO0FBdWpCRTtFQUNJLGdDQUFBO0VBQ0EsMkJBQUE7RUFDQSxzQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtBQXJqQk47QUF5akJBO0VBQ0Usc0JBQUE7RUFDQSxnQ0FBQTtBQXRqQkY7QUEwakJHO0VBQ0Usd0JBQUE7QUF2akJMO0FBeWpCRztFQUNDLHlCQUFBO0VBQ0EsNEJBQUE7QUF2akJKO0FBMmpCQTtFQUNFLHFCQUFBO0FBeGpCRjtBQTRqQkE7RUFDRSxxQkFBQTtBQXpqQkY7QUE2akJBO0VBQ0UscUJBQUE7QUExakJGO0FBOGpCQTtFQUNFLHFCQUFBO0FBM2pCRjtBQStqQkE7RUFDRSx1QkFBQTtBQTVqQkY7QUErakJBO0VBQ0Usc0JBQUE7RUFDQSx3QkFBQTtBQTVqQkY7QUFra0JBO0VBQ0UsaUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7QUEvakJGO0FBa2tCQTtFQUNFLGtCQUFBO0FBL2pCRjtBQWtrQkE7RUFDRSxzQkFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EscUJBQUE7RUFDQSxxQkFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7QUEvakJGO0FBa2tCQTtFQUNFLHNCQUFBO0VBQ0EseUJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNBLHFCQUFBO0VBQ0EsMEJBQUE7QUEvakJGO0FBaWtCQTtFQUNFLGdCQUFBO0VBQ0EsZ0VBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUE5akJGO0FBZ2tCRTtFQUNFLHNCQUFBO0VBQ0EsbUJBQUE7QUE5akJKO0FBa2tCTTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FBaGtCUjtBQW1rQk07RUFDRSxpQkFBQTtFQUNBLG9CQUFBO0FBamtCUjtBQW9rQk07RUFDRSxrQkFBQTtFQUNBLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUFsa0JSO0FBdWtCTTtFQUNFLHdCQUFBO0VBQ0Esa0JBQUE7QUFya0JSO0FBMGtCTTtFQUVFLHFCQUFBO0VBQ0Esc0JBQUE7RUFFQSxpQkFBQTtFQUNBLGdCQUFBO0FBMWtCUjtBQTZrQk07RUFDRSxnQkFBQTtFQUNBLGVBQUE7QUEza0JSO0FBaWxCQTtFQUVFLHdCQUFBO0VBQ0EsNEJBQUE7RUFDQSwwQkFBQTtBQS9rQkY7QUFrbEJBO0VBQ0UsNEVFOTZIVTtFRis2SFYsZUFBQTtFQUNBLGNBQUE7QUEva0JGO0FBaWxCQTtFQUNFLDhFRWo3SFk7RUZrN0haLGVBQUE7RUFDQSxjQUFBO0FBOWtCRjtBQWdsQkE7RUFDRSxpQkFBQTtBQTdrQkY7QUEra0JBO0VBQ0UsaUJBQUE7QUE1a0JGO0FBNmtCRTtFQUNJLGVBQUE7RUFDQSwrRUUvN0hDO0VGZzhIRCxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUEza0JOO0FBOGtCQTtFQUNFLCtFRXg4SEs7RUZ5OEhMLGlCQUFBO0VBRUEsaUJBQUE7RUFDQSwyQkFBQTtBQTVrQkY7QUE4a0JFO0VBQ0ksMkJBQUE7RUFDQSxpQkFBQTtBQTVrQk47QUFnbEJBO0VBQ0Usb0JBQUE7QUE3a0JGO0FBOGtCRTtFQUNJLHlCQUFBO0FBNWtCTjtBQWdsQkE7RUFDRSxpQkFBQTtBQTdrQkY7QUEra0JBO0VBQ0UsbUJBQUE7QUE1a0JGO0FBOGtCQTtFQUNFLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLCtFRXArSEs7RUZxK0hMLHlCQUFBO0VBQ0Esa0JBQUE7QUEza0JGO0FBOGtCQTtFQUVFLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSwrRUFBQTtFQUNBLFNBQUE7RUFDQSxxQkFBQTtFQUNBLGVBQUE7QUE1a0JGO0FBZ2xCQTtFQUNFLGtCQUFBO0VBQ0EsNEJBQUE7RUFDQSxzQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0FBN2tCRjtBQWdsQkE7RUFDRSxjQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBRUEscUJBQUE7RUFDQSw0QkFBQTtFQUNBLGdCQUFBO0VBQ0EsdUJBQUE7QUE5a0JGO0FBaWxCQTtFQUNFLGNBQUE7RUFDQSxvQkFBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFFQSxxQkFBQTtFQUNBLDRCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx1QkFBQTtBQS9rQkY7QUFrbEJBO0VBQ0UsdUJBQUE7QUEva0JGO0FBa2xCQTtFQUVFLHVCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtFQUNBLHlCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtFQUNBLHlCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtFQUNBLHlCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtFQUNBLHlCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtFQUNBLHlCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtFQUNBLHlCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtFQUNBLHlCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtFQUNBLHlCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtFQUNBLHlCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtFQUNBLHlCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7QUFobEJGO0FBbWxCQTtFQUNFLGdDQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtBQWhsQkY7QUFtbEJBO0VBQ0UsZ0NBQUE7QUFobEJGO0FBbWxCQTtFQUNFLGdDQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtBQWhsQkY7QUFtbEJBO0VBQ0UsZ0NBQUE7QUFobEJGO0FBbWxCQTtFQUNFLGdDQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxnQ0FBQTtBQWhsQkY7QUFtbEJBO0VBQ0UsMEJBQUE7QUFobEJGO0FBbWxCQTtFQUNFLDBCQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSwwQkFBQTtBQWhsQkY7QUFtbEJBO0VBQ0UsMEJBQUE7QUFobEJGO0FBbWxCQTtFQUNFLG9DQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxvQ0FBQTtBQWhsQkY7QUFtbEJBO0VBQ0Usb0NBQUE7QUFobEJGO0FBbWxCQTtFQUNFLG9DQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxvQ0FBQTtBQWhsQkY7QUFtbEJBO0VBQ0Usb0NBQUE7QUFobEJGO0FBbWxCQTtFQUNFLG9DQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxvQ0FBQTtBQWhsQkY7QUFtbEJBO0VBQ0Usb0NBQUE7QUFobEJGO0FBbWxCQTtFQUNFLG9DQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxvQ0FBQTtBQWhsQkY7QUFtbEJBO0VBQ0Usb0NBQUE7QUFobEJGO0FBbWxCQTtFQUNFLG9DQUFBO0FBaGxCRjtBQW1sQkE7RUFDRSxvQ0FBQTtBQWhsQkY7QUFrbEJBO0VBQ0UsYUFBQTtBQS9rQkY7QUFrbEJBO0VBQ0Usa0JBQUE7RUFDQSxRQUFBO0VBQ0EsU0FBQTtFQUNBLDJDQUFBO0VBQ0EseUJBQUE7QUEva0JGO0FBb2xCRTtFQUNFLGdFQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7QUFqbEJKO0FBbWxCSTtFQUNFLFlBQUE7RUFDQSxtQkFBQTtFQUNBLFlBQUE7RUFDQSxVQUFBO0VBQ0EscUJBQUE7RUFDQSxjQUFBO0FBamxCTjtBQW1sQk07O0VBRUUscUJBQUE7RUFDQSxzQkFBQTtBQWpsQlI7QUFxbEJJO0VBRUUsZ0VBQUE7QUFwbEJOO0FBdWxCSTtFQUVFLGlCQUFBO0FBdGxCTjtBQTJsQkE7RUFDRSx5QkFBQTtFQUNBLDBCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFlBQUE7QUF4bEJGO0FBMmxCQTtFQUNFLFdBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUF4bEJGO0FBMmxCQTtFQUNFLFdBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FBeGxCRjtBQTJsQkE7RUFDRSx1QkFBQTtFQUNBLHNCQUFBO0FBeGxCRjtBQTJsQkE7RUFDRSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSw0QkFBQTtFQUNBLDhCQUFBO0FBeGxCRjtBQWdtQkU7RUFDRSxtQkFBQTtBQTdsQko7QUFnbUJFO0VBQ0UsY0FBQTtFQUNBLDRCQUFBO0VBQ0EsK0VFaHpJRztFRml6SUgsa0JBQUE7RUFDQSxPQUFBO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0FBOWxCSjtBQStsQkk7RUFDRSxlQUFBO0FBN2xCTjtBQW9tQkE7RUFDRSxnQ0FBQTtFQUNBLHNCQUFBO0VBQ0Esb0NBQUE7QUFqbUJGO0FBb21CQTtFQUNFLGtCQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLG9CQUFBO0VBQ0EsY0FBQTtFQUNBLDBCQUFBO0FBam1CRjtBQW9tQkE7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7QUFqbUJGO0FBb21CQTtFQUNFLDBDQUFBO0VBQ0Esd0NBQUE7RUFDQSwwQ0FBQTtBQWptQkY7QUFvbUJBO0VBQ0UscUJBQUE7RUFDQSxhQUFBO0VBQ0EsdUJBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxVQUFBO0VBQ0EseUJBQUE7QUFqbUJGO0FBb21CQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtBQWptQko7QUFtbUJJO0VBQ0Usa0JBQUE7QUFqbUJOO0FBb21CSTtFQUNFLGtCQUFBO0FBbG1CTjtBQXNtQkE7RUFDRSxnRUFBQTtFQUNBLDhCQUFBO0FBbm1CRjtBQXNtQkE7RUFDRSw0QkFBQTtFQUNBLHlCQUFBO0FBbm1CRjtBQXNtQkE7RUFFRSxjQUFBO0FBcG1CRjtBQXdtQkU7RUFDRSxxQkFBQTtFQUNBLFVBQUE7RUFDQSw0QkFBQTtFQUNBLHVCQUFBO0VBQ0EsaUJBQUE7RUFDQSw2QkFBQTtBQXJtQko7QUF5bUJBO0VBQ0Usa0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUF0bUJGO0FBeW1CQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUF0bUJGO0FBd21CQTtFQUNFLG9CQUFBO0FBcm1CRjtBQXdtQkE7RUFDRSxvQkFBQTtBQXJtQkY7QUF3bUJBO0VBQ0UsbUJBQUE7QUFybUJGO0FBdW1CQTtFQUNFLHlCQUFBO0FBcG1CRjtBQXVtQkE7RUFDRSx1QkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0FBcG1CRjtBQXVtQkE7RUFDRSxxQkFBQTtFQUNBLHlCQUFBO0FBcG1CRjtBQXVtQkE7RUFDRSx5QkFBQTtBQXBtQkY7QUF1bUJBO0VBQ0UsNkJBQUE7QUFwbUJGO0FBdW1CQTtFQUNFLGlCQUFBO0FBcG1CRjtBQXVtQkE7RUFDRSw2QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQXBtQkY7QUF1bUJBO0VBQ0UsK0NBQUE7RUFDQSxnREFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7RUFDQSxpQkFBQTtFQUNBLHFDQUFBO0FBcG1CRjtBQXVtQkE7RUFDRSxrQkFBQTtFQUNBLFVBQUE7QUFwbUJGO0FBdW1CQTtFQUNFLGNBQUE7QUFwbUJGO0FBMm1CQTtFQUNFLG1DQUFBO0FBeG1CRjtBQTJtQkE7RUFDRTtJQUNFLHFCQUFBO0VBeG1CRjtFQTBtQkE7SUFDRSxxQkFBQTtFQXhtQkY7RUEwbUJBO0lBQ0UscUJBQUE7RUF4bUJGO0VBMG1CQTtJQUNFLG1CQUFBO0VBeG1CRjtFQTBtQkE7SUFDRSxxQkFBQTtFQXhtQkY7RUEwbUJBO0lBQ0UscUJBQUE7RUF4bUJGO0FBQ0Y7QUEybUJBO0VBQ0UsbUNBQUE7QUF6bUJGO0FBNG1CQTtFQUNFO0lBQ0UscUJBQUE7RUF6bUJGO0VBMm1CQTtJQUNFLG1CQUFBO0VBem1CRjtBQUNGO0FBNG1CQTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBRUEsa0JBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7RUFDQSxXQUFBO0FBM21CRjtBQThtQkE7RUFDRSxZQUFBO0FBM21CRjtBQThtQkE7RUFFRSx3Q0FBQTtBQTNtQkY7QUFzb0JBO0VBQ0U7SUFDRSwyQkFBQTtFQWxuQkY7RUF5bkJBO0lBQ0UsMEJBQUE7RUF2bkJGO0VBMG5CQTtJQUNFLDJCQUFBO0VBeG5CRjtFQTJuQkE7SUFDRSwwQkFBQTtFQXpuQkY7RUE0bkJBO0lBQ0Usd0JBQUE7RUExbkJGO0FBQ0Y7QUE0bkJBO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSxvQkFBQTtFQUNBLFlBQUE7QUExbkJGO0FBNm5CQTtFQUNFLHdCQUFBO0FBMW5CRjtBQXFvQkE7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBRUEsZ0JBQUE7QUFub0JGO0FBcW9CRTtFQUNFLGNBQUE7QUFub0JKO0FBc29CRTtFQUNFLGNBQUE7QUFwb0JKO0FBdW9CRTtFQUNFLGNBQUE7QUFyb0JKO0FBeW9CQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUF0b0JGO0FBeW9CQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUF0b0JGO0FBeW9CQTtFQUNFLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxVQUFBO0VBQ0EsZ0JBQUE7QUF0b0JGO0FBeW9CQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtBQXRvQkY7QUF5b0JBO0VBRUUsWUFBQTtFQUNBLFdBQUE7RUFDQSw0QkFBQTtBQXZvQkY7QUEwb0JBO0VBRUUsWUFBQTtFQUNBLFdBQUE7RUFDQSw0QkFBQTtFQUNBLHNCQUFBO0FBeG9CRjtBQTJvQkE7RUFFRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDRCQUFBO0FBem9CRjtBQTRvQkE7RUFFRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHdCQUFBO0VBQ0EsNEJBQUE7RUFDQSxnQkFBQTtBQTFvQkY7QUE2b0JBO0VBRUUsWUFBQTtFQUNBLDRCQUFBO0VBQ0Esd0JBQUE7RUFDQSxZQUFBO0VBQ0Esb0JBQUE7RUFDQSwyQkFBQTtBQTNvQkY7QUE4b0JBO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLDBCQUFBO0FBM29CRjtBQThvQkE7RUFDRSxnQkFBQTtFQUNBLG1CQUFBO0FBM29CRjtBQThvQkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsYUFBQTtFQUNBLFNBQUE7RUFDQSxZQUFBO0VBRUEsd0JBQUE7RUFDQSw0QkFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSx3QkFBQTtBQTVvQkY7QUErb0JBO0VBQ0Usd0JBQUE7QUE1b0JGO0FBK29CQTtFQUNFLGdCQUFBO0FBNW9CRiIsImZpbGUiOiJob21lLXZlcnRpY2FsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCBcIi4uLy4uLy4uL3RoZW1lL2ZvbnRzLnNjc3NcIjtcblxuLmJsdXItaXR7XG4gIGZpbHRlcjpibHVyKDRweCkgIWltcG9ydGFudDtcbn1cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRjZGN0ZDICFpbXBvcnRhbnQ7XG4gIC8vIC0tYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCAjMDFBM0E0IDUwJSwgI0Y2RjdGQyA1MCUpIWltcG9ydGFudDtcbiAgXG4gIC4tLXRyYW5zZm9ybXtcbiAgICBtaW4taGVpZ2h0OiAyNTBweDtcbiAgICBib3JkZXItcmFkaXVzOiAxO1xuICAgIGJvcmRlci1yYWRpdXM6IDEyJTtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiA4cHg7XG4gIH1cbiAgLm1lZGl1bS10ZXh0LWFsbCB7XG4gICAgY29sb3I6ICMzMzM7XG4gICAgZm9udC1zaXplOiAuOXJlbSAhaW1wb3J0YW50O1xuICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgICBtYXJnaW46IDA7XG4gICAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbiAgICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBcbiAgfVxuICBcbiAgLmdldC1idXR0b24taWNvbntcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDFhM2E0O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICNmZmY7XG4gICAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHJpZ2h0OiAxLjJyZW07XG4gICAgaGVpZ2h0OiAzNXB4O1xuICAgIHdpZHRoOiAzNXB4O1xuICAgIHotaW5kZXg6IDExO1xuICAgIHRvcDogNHJlbTtcbiAgfVxufVxuXG4uLS1tYW5qaGFyaS12aWR7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgLmNhcmR7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgbWluLWhlaWdodDogMTIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgIGJveC1zaGFkb3c6IDBweCA1cHggMTBweCAwcHggcmdiKDEyOSAxMzMgMTMxIC8gMTAlKTtcbiAgICAubGVmdHtcbiAgICAgIC5jb250ZW50e1xuICAgICAgICBjb2xvcjojNTY1NjU2O1xuICAgICAgICBmb250LXNpemU6IC44N3JlbTtcbiAgICAgICAgbGVmdDogMXJlbTtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBsZWZ0O1xuICAgICAgICBwYWRkaW5nLXRvcDogMnB4O1xuICAgICAgIFxuICAgICAgfVxuICAgIH1cbiAgICAucmlnaHR7XG4gICAgICBpbWd7XG4gICAgICAgIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAyNXB4O1xuICAgICAgICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMjVweDtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIG1pbi1oZWlnaHQ6IDIwMHB4O1xuICAgICAgICBtYXJnaW4tbGVmdDogMXB4O1xuICAgICAgICBtYXJnaW4tYm90dG9tOiAtNHB4O1xuICAgICAgICBoZWlnaHQ6IDIwMHB4O1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4uY2VudGVyLXBlcmNlbnQtZmFzdGluZyB7XG4gIHdpZHRoOiA1MCU7XG4gIGhlaWdodDogNTAlO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlcjogMXggc29saWQgI2IzYjNiMztcbiAgYm94LXNoYWRvdzogMCAzcHggNnB4IHJnYmEoMCwgMCwgMCwgMTYlKTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB0b3A6IDI2JTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAyNCU7XG4gIHBhZGRpbmctdG9wOiA4JTtcbn1cblxuLnJlZnJlc2gtbG9jayB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDFweDtcbiAgd2lkdGg6IDE0cHg7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBvcGFjaXR5OiAwLjc7XG59XG5cbmEubW9yZWxlc3Mge1xuICBjb2xvcjogIzE0OTJFNiAhaW1wb3J0YW50O1xufVxuXG4uY3VzdG9tLXJlZCB7XG4gIGNvbG9yOiAjRTc0QzNDO1xufVxuXG4uY3VzdG9tLW9yYW5nZSB7XG4gIGNvbG9yOiBvcmFuZ2U7XG59XG4uY3VzdG9tLWJhY2tncm91bmQtZmZmIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgLS1ib3JkZXI6ICNmZmY7XG4gIC0tYm9yZGVyLWNvbG9yOiAjZmZmO1xufVxuXG4uY3VzdG9tLWNvbG9yLW90aGVyIHtcbiAgIGNvbG9yOiAjMDA3ODgxICAhaW1wb3J0YW50O1xufVxuXG46Om5nLWRlZXAgLmN1c3RvbS1saW5rLWNvbG9yIHtcbiAgY29sb3I6ICMxNDkyRTYgIWltcG9ydGFudDtcbn1cblxuOjpuZy1kZWVwIC5sb2FkaW5nLW5vdGktbXNne1xuICAvLyBoZWlnaHQ6IDQ1cHg7XG4gIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgLy8gbGVmdDogLTVweDtcbiAgLy8gdG9wOiA1cHg7XG4gIGhlaWdodDogMzBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwcHg7XG4gIHRvcDogMTJweDtcbn1cblxuLmxvY2stcHJlbWl1bS1jb2xvciB7XG4gIGNvbG9yOiAjREVCMjIxICFpbXBvcnRhbnQ7XG59XG5cbi5jdXN0b20tY29sb3ItYnRuLXdoaXRlIHtcbiAgY29sb3I6ICNGRkYgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxcmVtO1xufVxuXG4ud2F0ZXItY2VudGVyLXBlcmNlbnQge1xuICB3aWR0aDogNzUlO1xuICBoZWlnaHQ6IDc1JTtcbiAgei1pbmRleDogMTQ7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGxlZnQ6IDEyJTtcbiAgdG9wOiAxNCU7XG4gIHBhZGRpbmctdG9wOiAyNSU7XG4gIC8qIGJveC1zaGFkb3c6IDAgM3B4IDZweCByZ2IoMCAwIDAgLyAxNiUpOyAqL1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBjb2xvcjogIzE0OTJFNjsgXG59XG5cbi5jZW50ZXItcGVyY2VudCB7XG4gIHdpZHRoOiA0MiU7XG4gIGhlaWdodDogNDIlO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlcjogMXggc29saWQgI2IzYjNiMztcbiAgYm94LXNoYWRvdzogMCAzcHggNnB4IHJnYmEoMCwgMCwgMCwgMTYlKTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB0b3A6IDI3JTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAyOSU7XG4gIHBhZGRpbmctdG9wOiA4JTtcbn1cblxuLmJveC1hY3Rpdml0eSB7XG4gIG1heC13aWR0aDogMjYwcHg7XG4gIHdpZHRoOiAxNTRweDtcbiAgaGVpZ2h0OiAxMjVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBtYXJnaW4tdG9wOiAzMCU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5ib3gtZmFzdGluZyB7XG4gIG1heC13aWR0aDogMTUwcHg7XG4gIHdpZHRoOiAxMTJweDtcbiAgLyogaGVpZ2h0OiAxMTVweDsgKi9cbiAgLyogcG9zaXRpb246IGFic29sdXRlOyAqL1xuICAvKiBtYXJnaW4tdG9wOiAxMiU7ICovXG4gIC8qIGxlZnQ6IDUwJTsgKi9cbiAgLyogdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7ICovXG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7XG4gIG1hcmdpbjogYXV0bztcbiAgLyogb3ZlcmZsb3c6IGhpZGRlbjsgKi9cbn1cblxuLmJveC13ZWlnaHQge1xuICBtYXgtd2lkdGg6IDE1MHB4O1xuICB3aWR0aDogMTE1cHg7XG4gIGhlaWdodDogMTE1cHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbWFyZ2luLXRvcDogMTIlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xufVxuXG4uYWJzb2x1dGUtcG9zIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMTAwJTtcbiAgYm90dG9tOiAxMHB4O1xufVxuXG4ud2VpZ2h0LXNtYWxsIHtcbiAgY29sb3I6ICM3MDcwNzA7XG4gIGZvbnQtc2l6ZTogLjhyZW07XG4gIGZvbnQtZmFtaWx5OiAkZm9udDtcbn1cblxuLndlaWdodC1zbWFsbC1kaWZmIHtcbiAgY29sb3I6ICMwMWEzYTQ7XG4gIGZvbnQtc2l6ZTogLjg3NXJlbTtcbiAgZm9udC1mYW1pbHk6ICRmb250O1xufVxuXG4ubWFyZ2luLXRvcC0yMXBlciB7XG4gIG1hcmdpbi10b3A6IDQyJSAhaW1wb3J0YW50O1xufVxuXG4ucGFkZGluZy0wIHtcbiAgcGFkZGluZzogMDtcbn1cblxuLnBhZGRpbmctMTAge1xuICBwYWRkaW5nOiAxMHB4O1xufVxuXG4uY3VzdG9tLWhlaWdodC13aWR0aCB7XG4gIGhlaWdodDogNDgwcHggIWltcG9ydGFudDtcbiAgd2lkdGg6IDEwMCU7XG4gIG1heC1oZWlnaHQ6IDEwMDBweDtcbn1cblxuLmN1c3RvbS1oZWlnaHQtd2lkdGgtNDMwIHtcbiAgaGVpZ2h0OiA1MDhweCAhaW1wb3J0YW50O1xuICB3aWR0aDogMTAwJTtcbiAgbWF4LWhlaWdodDogMTAwMHB4O1xufVxuXG4ucmVzcG9uc2l2ZS1ib3gge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ucmVzcG9uc2l2ZS1ib3g6YWZ0ZXIge1xuICBjb250ZW50OiAnJztcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmctYm90dG9tOiAxMDAlO1xufVxuXG4uaW9uLW5vLXBhZGRpbmcge1xuICBwYWRkaW5nLWxlZnQ6IDA7XG59XG5cbi5jb250ZW50IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogOTglO1xuICBoZWlnaHQ6IDk2JTtcbiAgYm90dG9tOiAwO1xufVxuXG4udGl0bGUge1xuICBmb250LXNpemU6IC44NzVyZW0gIWltcG9ydGFudDtcbiAgY29sb3I6ICMzNTgwRjI7XG4gIGZvbnQtZmFtaWx5OiAkZm9udCAhaW1wb3J0YW50O1xufVxuXG4udGl0bGUtcmVjaXBle1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgZm9udC1mYW1pbHk6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1ib2xkXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWYgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAwLjk1cmVtICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50OyAgXG59XG5cbi50aXRsZS1iaWcge1xuICBmb250LXNpemU6IDEuMnJlbSAhaW1wb3J0YW50O1xuICBjb2xvcjogIzM1ODBGMjtcbiAgZm9udC1mYW1pbHk6ICRmb250ICFpbXBvcnRhbnQ7XG59XG5cbi5jdXN0b20taGVpZ2h0LXdpZHRoLTE1NSB7XG4gIGhlaWdodDogMTU1cHggIWltcG9ydGFudDtcbiAgd2lkdGg6IDE1NXB4ICFpbXBvcnRhbnQ7XG4gIG1heC1oZWlnaHQ6IDEwMDBweDtcbn1cblxuLmZpcmUge1xuICB3aWR0aDogMTAlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGhlaWdodDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiAuMnJlbTtcbn1cblxuLmhlaWdodC00MTAge1xuICBoZWlnaHQ6IDQxMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5tYXgtd2lkdGgtMjAwIHtcbiAgd2lkdGg6IDI0OHB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogMjQ4cHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogMzUlICFpbXBvcnRhbnQ7XG59XG5cbi5waWMtZm9vZC1oZWFydCB7XG4gIHdpZHRoOiAxNXB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogYXV0byAhaW1wb3J0YW50O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDYwcHg7XG4gIHRvcDogNHB4O1xufVxuXG46Om5nLWRlZXAgLnBpYy1mb29kLWhlYXJ0LW1zZyB7XG4gIHdpZHRoOiAxNXB4ICFpbXBvcnRhbnQ7XG4gIGhlaWdodDogYXV0byAhaW1wb3J0YW50O1xuICBsZWZ0OiA2MHB4O1xuICB0b3A6IDRweDtcbn1cblxuLnBpYy1mb29kLWhlYXJ0LWhlYWx0aHkge1xuICB3aWR0aDogMTZweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tcmlnaHQ6IDVweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgaGVpZ2h0OiAxNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi5oZWlnaHQtMzQ1IHtcbiAgaGVpZ2h0OiA1MDhweDtcbiAgbWF4LWhlaWdodDogMTAwMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5ib3R0b20tMjUge1xuICBib3R0b206IDI1cHggIWltcG9ydGFudDtcbiAgd2lkdGg6IDk1JTtcbn1cblxuLnRvcC0xNiB7XG4gIHRvcDogMTYlO1xufVxuXG4udG9wLTE5IHtcbiAgdG9wOiAxOSU7XG59XG5cbi5oZWlnaHQtNDUzIHtcbiAgaGVpZ2h0OiA1MzVweDtcbn1cblxuLmhlaWdodC0zNjkge1xuICBoZWlnaHQ6IDUzNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi5oZWlnaHQtNDUyIHtcbiAgaGVpZ2h0OiA1MzVweCAhaW1wb3J0YW50O1xufVxuXG4uYWN0aXZpdHktZmFzdGluZy1zZWN0aW9uIHtcbiAgYmFja2dyb3VuZDogI0Y2RjdGQztcbiAgcGFkZGluZzogMCAuNXJlbSAwIC41cmVtO1xuXG4gIC5hY3Rpdml0eSB7XG4gICAgLy8gaGVpZ2h0OiAyNjhweDtcbiAgICBib3gtc2hhZG93OiAwIDVweCA1cHggcmdiYSgwLCAwLCAwLCAxNiUpO1xuXG4gICAgLmFjdGl2aXR5LWhlYWRlciB7XG4gICAgICBwYWRkaW5nLXRvcDogOHB4O1xuXG4gICAgICBoMiB7XG4gICAgICAgIG1hcmdpbjogMCAhaW1wb3J0YW50O1xuICAgICAgICBjb2xvcjogIzE0Q0VCNztcbiAgICAgICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICAgICAgZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG4gICAgICB9XG5cbiAgICAgIC5zdGVwcyB7XG4gICAgICAgIHNwYW4ge1xuICAgICAgICAgIGNvbG9yOiAjNzc4Q0EzO1xuICAgICAgICAgIGZvbnQtc2l6ZTogLjhyZW07XG4gICAgICAgICAgZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG4gICAgICAgIH1cblxuICAgICAgICBpb24taWNvbiB7XG4gICAgICAgICAgY29sb3I6ICM3NzhDQTM7XG4gICAgICAgICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlvbi1jYXJkLWNvbnRlbnQge1xuICAgICAgLmZvb3RlciB7XG4gICAgICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGJvdHRvbTogMDtcbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICAuZmFzdGluZyB7XG4gICAgLy8gaGVpZ2h0OiAyNjhweDtcbiAgICB3aWR0aDogOTklO1xuICAgIG1hcmdpbi1sZWZ0OiAxJTtcbiAgICBib3gtc2hhZG93OiAwIDVweCA1cHggcmdiYSgwLCAwLCAwLCAxNiUpO1xuXG4gICAgLmZhc3RpbmctaGVhZGVyIHtcbiAgICAgIHBhZGRpbmctdG9wOiA4cHg7XG5cbiAgICAgIGgyIHtcbiAgICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG4gICAgICAgIC8vICBjb2xvcjojZmZhNTAwO1xuICAgICAgICBmb250LXNpemU6IC44cmVtO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgICAgIH1cblxuICAgICAgLnJlbWFpbmluZyB7XG4gICAgICAgIC8vICAgY29sb3I6I2ZmYTUwMDtcbiAgICAgICAgZm9udC1zaXplOiAuN3JlbTtcbiAgICAgICAgZm9udC1mYW1pbHk6ICRmb250O1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4uZGVmaWNpdC1zZWN0aW9uIHtcbiAgYmFja2dyb3VuZDogI0Y2RjdGQztcbiAgcGFkZGluZy10b3A6IDIwcHg7XG59XG5cbi5rZXltYW50cmEtc2VjdGlvbiB7XG4gIGJhY2tncm91bmQ6ICNGNkY3RkM7XG59XG5cbi53YXRlci13ZWlnaHQtc2VjdGlvbiB7XG4gIGJhY2tncm91bmQ6ICNGNkY3RkM7XG4gIHBhZGRpbmc6IC41cmVtIC41cmVtIDAgLjVyZW07XG5cbiAgLy8gaGVpZ2h0OiAyMzBweDtcbiAgLndhdGVyIHtcbiAgICAvLyBoZWlnaHQ6IDI2OHB4O1xuICAgIGJveC1zaGFkb3c6IDAgNXB4IDVweCByZ2JhKDAsIDAsIDAsIDE2JSk7XG5cbiAgICAud2F0ZXItaGVhZGVyIHtcbiAgICAgIHBhZGRpbmctdG9wOiA3cHg7XG5cbiAgICAgIGgyIHtcbiAgICAgICAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG4gICAgICAgIGNvbG9yOiAjMzQ5OERCO1xuICAgICAgICBmb250LXNpemU6IC44cmVtO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgICAgIH1cblxuICAgICAgLnN0ZXBzIHtcbiAgICAgICAgc3BhbiB7XG4gICAgICAgICAgY29sb3I6ICM3NzhDQTM7XG4gICAgICAgICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICAgICAgICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlvbi1pY29uIHtcbiAgICAgICAgICBjb2xvcjogIzc3OENBMztcbiAgICAgICAgICBmb250LXNpemU6IC44cmVtO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaW9uLWNhcmQtY29udGVudCB7XG4gICAgICAuZm9vdGVyIHtcbiAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgYm90dG9tOiAwO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIC53ZWlnaHQge1xuICAgIC8vIGhlaWdodDogMjY4cHg7XG4gICAgd2lkdGg6IDk5JTtcbiAgICBtYXJnaW4tbGVmdDogMSU7XG4gICAgYm94LXNoYWRvdzogMCA1cHggNXB4IHJnYmEoMCwgMCwgMCwgMTYlKTtcblxuICAgIC53ZWlnaHQtaGVhZGVyIHtcbiAgICAgIHBhZGRpbmctdG9wOiA4cHg7XG4gICAgICBwYWRkaW5nLWxlZnQ6IDA7XG4gICAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xuXG4gICAgICBoMiB7XG4gICAgICAgIG1hcmdpbjogMCAhaW1wb3J0YW50O1xuICAgICAgICBjb2xvcjogIzAxQTNBNDtcbiAgICAgICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICAgICAgZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG4gICAgICB9XG5cbiAgICAgIC53ZWlnaHQtY29sb3Ige1xuICAgICAgICBjb2xvcjogIzAxQTNBNDtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuXG5cblxuLmdvb2dsZS1maXQge1xuXG4gIC5yb3VuZF9wcm9ncmVzc19iYXIge1xuICAgIG1hcmdpbjogYXV0bztcbiAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICB9XG5cbiAgLmhlYXJ0X3BvaW50cyAuY2Fsc190eHQsXG4gIC5zdGVwcyAuc3RlcHNfdHh0IHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgfVxuXG4gIC5oZWFydF9wb2ludHMgc3BhbixcbiAgLnN0ZXBzIHNwYW4ge1xuICAgIGZvbnQtc2l6ZTogMThweDtcbiAgfVxuXG4gIC8vIC5oZWFydF9wb2ludHMgc3BhbiB7ZmxvYXQ6IHJpZ2h0O31cbiAgLy8gLmhlYXJ0X3BvaW50cyBpb24taWNvbiB7XG4gIC8vICAgcG9zaXRpb246IGFic29sdXRlO1xuICAvLyAgIGZvbnQtc2l6ZTogMjBweDtcbiAgLy8gICB0b3A6IDdweDtcbiAgLy8gICBsZWZ0OiAyN3B4O1xuICAvLyAgIGNvbG9yOiAjZmJjMDMwO1xuICAvLyB9XG4gIC5zdGVwcyBpb24taWNvbiB7XG4gICAgZm9udC1zaXplOiAyMHB4O1xuICAgIGNvbG9yOiAjMTY0RUE4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IDNweDtcbiAgfVxuXG4gIC8vIC5zdGF0c19yb3cgc3BhbiB7XG4gIC8vICAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcbiAgLy8gICBmb250LXNpemU6IDE2cHggIWltcG9ydGFudDtcbiAgLy8gfVxuICAuc3RhdHNfcm93IGg1IHtcbiAgICBjb2xvcjogIzFBNzNGNyAhaW1wb3J0YW50O1xuICAgIGZvbnQtc2l6ZTogMjdweDtcbiAgICAvLyBmb250LXdlaWdodDogYm9sZDtcbiAgfVxuXG4gIC5zdGF0c19yb3cge1xuICAgIG1hcmdpbi10b3A6IDE1cHg7XG4gIH1cblxuICAuaW5uZXJfcnBfYmFyIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiAtMjAwcHg7XG4gICAgbWFyZ2luOiBhdXRvO1xuICB9XG5cbiAgLmNvdW50c19ibGsge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDIyJTtcbiAgICBsZWZ0OiAwO1xuICAgIHJpZ2h0OiAwO1xuICAgIHotaW5kZXg6IDk5OTtcbiAgfVxuXG4gIC5jb3VudHNfYmxrIGgzIHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgY29sb3I6ICNmYmMwMzA7XG4gIH1cblxuICAucmVzcG9uc2VfdHh0IHtcbiAgICBjb2xvcjogI2ZiYzAzMDtcbiAgfVxuXG4gIC5jb3VudHNfYmxrIGg0IHtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgY29sb3I6ICMxNjRFQTg7XG4gIH1cblxuICAuYWN0aXZhdGVfZml0X2J0biB7XG4gICAgbWFyZ2luLXRvcDogMzVweDtcbiAgICAtLWNvbG9yOiByZWQ7XG4gICAgLS1iYWNrZ3JvdW5kLWFjdGl2YXRlZDogbm9uZTtcbiAgICAtLWJhY2tncm91bmQtYWN0aXZhdGVkLW9wYWNpdHk6IG5vbmU7XG4gICAgLS1iYWNrZ3JvdW5kLWZvY3VzZWQ6IG5vbmU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIH1cblxuICAuY2FsX2ltYWdlIHtcbiAgICB3aWR0aDogMjBweDtcbiAgICBtYXJnaW46IDBweCA2cHg7XG4gICAgdG9wOiAzcHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG5cbiAgLmNvdW50c19ibGsgLmNhbF9pbWFnZSB7XG4gICAgd2lkdGg6IDIwcHg7XG4gICAgdG9wOiAwcHg7XG4gICAgbWFyZ2luOiAwcHg7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIH1cblxuICBpb24tdGV4dCB7XG4gICAgbWFyZ2luOiBhdXRvO1xuICB9XG5cbiAgLnJlZnJlc2hfYnRuIHtcbiAgICAtLXBhZGRpbmctdG9wOiAyN3B4O1xuICAgIC0tcGFkZGluZy1ib3R0b206IDI3cHg7XG4gICAgLS1wYWRkaW5nLXN0YXJ0OiAxOHB4O1xuICAgIC0tcGFkZGluZy1lbmQ6IDE3cHg7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgfVxuXG4gIGlvbi1idXR0b24gLnJlbG9hZF9pbWFnZSB7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMGRlZyk7XG4gICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuMnMgbGluZWFyO1xuICB9XG5cbiAgaW9uLWJ1dHRvbi5pb24tYWN0aXZhdGVkIC5yZWxvYWRfaW1hZ2Uge1xuICAgIHRyYW5zZm9ybTogcm90YXRlKDM2MGRlZyk7XG4gICAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuMnMgbGluZWFyO1xuICB9XG5cbiAgLmdvb2dsZWZpdF9pbWcge1xuICAgIHdpZHRoOiAxMDBweDtcbiAgICBtYXJnaW46IDkwcHg7XG4gIH1cblxuICAucmVsb2FkX2ltYWdlIHtcbiAgICB3aWR0aDogNDBweDtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gIH1cbn1cblxuLmN1c3Rvbi1oZWlnaHQge1xuICBoZWlnaHQ6IDQ1MHB4O1xufVxuXG4uaGVpZ2h0LTY2MCB7XG4gIGhlaWdodDogNTcwcHg7XG4gIG1hcmdpbi1ib3R0b206IC0xLjJyZW07XG59XG5cbi5oZWlnaHQtMjY2IHtcbiAgaGVpZ2h0OiAyNjZweCAhaW1wb3J0YW50O1xufVxuXG4uaGVpZ2h0LTM4NSB7XG4gIGhlaWdodDogMzg1cHg7XG59XG5cbi5oZWlnaHQtNTE2IHtcbiAgaGVpZ2h0OiA1MTZweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAtLjdyZW07XG59XG5cbi5oZWlnaHQtNjE0IHtcbiAgaGVpZ2h0OiA2NzVweCAhaW1wb3J0YW50XG59XG5cbi53aWR0aC0xMDAge1xuICB3aWR0aDogMTAwJSAhaW1wb3J0YW50O1xufVxuXG4uaGVpZ2h0LTUwMCB7XG4gIGhlaWdodDogNTE2cHggIWltcG9ydGFudDtcbn1cblxuLmhlaWdodC0zNTAge1xuICBoZWlnaHQ6IDM1MHB4ICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW4tYm90dG9tOiAuNXJlbTtcbn1cblxuLmdyYXBoLWNvbG9yLWdyZWVuIHtcbiAgY29sb3I6IHJnYigxNjEsIDE5MSwgMzgpICFpbXBvcnRhbnQ7XG59XG5cbi5ibHVlLWNvbG9yIHtcbiAgY29sb3I6ICMwMEE4NkI7XG59XG5cbi5waW5rLWNvbG9yIHtcbiAgY29sb3I6ICNGODcxOUY7XG59XG5cbi8vIGlvbi1zZWdtZW50IHtcbi8vICAgaW9uLXNlZ21lbnQtYnV0dG9uIHtcbi8vICAgICAtLWluZGljYXRvci1jb2xvcjogcmdiKDE2MSwgMTkxLCAzOCkgIWltcG9ydGFudDtcbi8vICAgLS1jb2xvcjogI2ZmZjtcbi8vICAgfVxuLy8gfVxuXG5cblxuLm1lc3NhZ2UtcmVkIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGNvbG9yOiAjRTc0QzNDO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMXJlbTtcblxuICBwYWRkaW5nOiA2cHg7XG4gIGZvbnQtZmFtaWx5OiAkZm9udC1tZWRpdW07XG4gIGJveC1zaGFkb3c6IDAgNXB4IDVweCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xufVxuXG4ubWVzc2FnZSB7XG4gIGZvbnQtc2l6ZTogMS4zcmVtO1xuICBmb250LWZhbWlseTogJGZvbnQ7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4ub3ZlcmxheSB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgY29udGVudDogJyc7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG4gIG9wYWNpdHk6IC44O1xuICB6LWluZGV4OiA5OTk5MDtcbn1cblxuLmV2ZW50LWNvbnRhaW5lciB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgd2lkdGg6IDgwJTtcbiAgYm90dG9tOiA2MHB4O1xufVxuXG4uY3VzdG9tLWluZm8tbWFyZ2luIHtcbiAgbWFyZ2luLWxlZnQ6IC41cmVtICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiByZ2IoMTgzLCA5LCA0Nik7XG59XG5cbi5pbm5lci1tZXNzYWdlIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB3aWR0aDogODMlO1xuICBoZWlnaHQ6IDcwJTtcbiAgei1pbmRleDogOTk5OTA7XG4gIGJvdHRvbTogNSU7XG4gIHBhZGRpbmctYm90dG9tOiAxMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbGVmdDogMTAlO1xufVxuXG4uY2lyY2xlLXdoaXRlIHtcbiAgYm9yZGVyOiAxcHggc29saWQgI0U3NEMzQztcbiAgcGFkZGluZzogNnB4O1xuICBib3JkZXItcmFkaXVzOiAyMnB4O1xuICBtYXJnaW4tbGVmdDogLjVyZW07XG4gIHRvcDogLThweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uZm9vZCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5jaXJjbGUtaGVhZCB7XG4gIGJhY2tncm91bmQ6IHJnYigyNTUsIDAsIDU0KTtcbiAgcGFkZGluZzogNnB4O1xuICBib3JkZXItcmFkaXVzOiAyMnB4O1xufVxuXG4uY2lyY2xlLWFtYmVyLWhlYWQge1xuICBiYWNrZ3JvdW5kOiAjRkZDMjAwO1xuICBwYWRkaW5nOiA0cHggN3B4O1xuICBib3JkZXItcmFkaXVzOiAyMnB4O1xuICBtYXJnaW4tbGVmdDogMXJlbTtcbiAgdG9wOiAtOHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5saWtlLXN0cmFpdCB7XG4gIHdpZHRoOiAxNXB4O1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICB0b3A6IC0ycHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmNpcmNsZS1hbWJlciB7XG4gIGJhY2tncm91bmQ6ICNGRkMyMDA7XG4gIHBhZGRpbmc6IDZweDtcbiAgYm9yZGVyLXJhZGl1czogMjJweDtcbn1cblxuLmNpcmNsZS1ncmVlbiB7XG4gIGJhY2tncm91bmQ6IHJnYigxNjEsIDE5MSwgMzgpO1xuICBwYWRkaW5nOiA2cHg7XG4gIGJvcmRlci1yYWRpdXM6IDIycHg7XG59XG5cbi5kaXNsaWtlIHtcbiAgd2lkdGg6IDE4cHg7XG4gIC8qIGRpc3BsYXk6IGlubGluZS1ibG9jazsgKi9cbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgdG9wOiAwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmxpa2Uge1xuICB3aWR0aDogMThweDtcbiAgLyogZGlzcGxheTogaW5saW5lLWJsb2NrOyAqL1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICB0b3A6IC0ycHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuaW9uLW1lbnUtYnV0dG9uIHtcbiAgLS1jb2xvcjogI2ZmZjtcbn1cblxuLmNsb3NlLWNpcmNsZSB7XG4gIHdpZHRoOiAxLjRyZW07XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgbWFyZ2luOiAwO1xuICBjb2xvcjogcmdiKDE4MywgOSwgNDYpO1xufVxuXG4uaGFiYml0LWxhYmVsIHtcbiAgZm9udC1zaXplOiAxLjFyZW07XG4gIGNvbG9yOiAjMzMzICFpbXBvcnRhbnQ7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gIGZvbnQtZmFtaWx5OiAkZm9udDtcbn1cblxuLmhhYmJpdC1hcnJvdyB7XG4gIHdpZHRoOiAyLjVyZW07XG4gIGhlaWdodDogYXV0bztcbiAgbGVmdDogLTI3cHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgcGFkZGluZzogMTBweDtcbn1cblxuLmhhYmJpdC1wcm9nLWNvbnRhaW5lciB7XG4gIHdpZHRoOiA4MyU7XG4gIGhlaWdodDogNXB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG4gIGJhY2tncm91bmQ6IGdyYXk7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAyNXB4O1xuICBsZWZ0OiAwO1xufVxuXG4uaGFiYml0LXByb2ctbGFiZWwge1xuICBjb2xvcjogIzVENUQ1RDtcbiAgZm9udC1zaXplOiAwLjlyZW0gIWltcG9ydGFudDtcbiAgZm9udC1mYW1pbHk6ICRmb250O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGxlZnQ6IDA7XG4gIHBhZGRpbmctYm90dG9tOiAzcmVtO1xuICBwYWRkaW5nLXRvcDogMXJlbTtcbiAgd2lkdGg6IDk4JTtcblxuICBkaXYge1xuICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgfVxufVxuXG4ua2V5LW1hbnRyYSB7XG4gIGZvbnQtc2l6ZTogMS4yNXJlbSAhaW1wb3J0YW50O1xuICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgY29sb3I6ICMwMUEzQTQ7XG59XG5cbi5vdGhlci1tZWFsIHtcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4uYWRkLWhhYmJpdHMge1xuICAvLyBjb2xvcjogJGxpcHN0aWM7XG4gIGZvbnQtZmFtaWx5OiAkZm9udC1ib2xkO1xuICBmb250LXNpemU6IDAuNzVyZW07XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgY29sb3I6ICM3MDcwNzA7XG4gIC8vIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xufVxuXG4uYWRkLW1vcmUge1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICB3aWR0aDogYXV0bztcbiAgaGVpZ2h0OiAxcmVtO1xufVxuXG4uY3VzdG9tLWhlaWdodC1zY3JvbGwge1xuICAvLyBtYXgtaGVpZ2h0OiAxNzBweDtcbiAgLy8gbWF4LWhlaWdodDogMjAwcHg7XG4gIC8vIG92ZXJmbG93LXk6IHNjcm9sbDtcblxuICBpb24taXRlbSB7XG4gICAgbWFyZ2luLXRvcDogLTAuMnJlbTtcbiAgICAuaXRlbS1pbm5lcntcbiAgICAgIHBhZGRpbmctaW5saW5lLWVuZDogMCAhaW1wb3J0YW50O1xuICAgIH1cbiAgfVxufVxuXG4uY3VzdG9tLW1pbi1oZWlnaHQge1xuICBtaW4taGVpZ2h0OiAxMjlweDtcbn1cblxuLmJ1dHRvbi1jYW5jZWwge1xuICBmb250LXNpemU6IDAuODc1cmVtICFpbXBvcnRhbnQ7XG4gIGZvbnQtZmFtaWx5OiAkZm9udC1ib2xkO1xuICBib3JkZXItY29sb3I6ICNmZmYgIWltcG9ydGFudDtcbn1cblxuLmJ1dHRvbi1maXQge1xuXG4gIGZvbnQtc2l6ZTogMC44NzVyZW0gIWltcG9ydGFudDtcbiAgLy9ib3JkZXItY29sb3I6ICNmZmY7XG4gIGZvbnQtZmFtaWx5OiAkZm9udC1ib2xkO1xufVxuXG4ubG9jayB7XG4gIHdpZHRoOiAxNXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMnB4O1xuICByaWdodDogMTQlO1xuICB0b3A6IDEwcHg7XG59XG5cbi5oYWJiaXQge1xuICB3aWR0aDogMTAwJTtcbiAgbWFyZ2luLWxlZnQ6IDA7XG4gIG1hcmdpbi1yaWdodDogMDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgYm94LXNoYWRvdzogMC4ycmVtIDAuMnJlbSAwLjVyZW0gI2IzYjNiMztcbn1cblxuLmhhYmJpdC1wcm9nLXBlcmNlbnQge1xuICBmb250LXNpemU6IDAuNzVyZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogcmdiKDIwLCAyMDYsIDE4Myk7XG4gIGJvdHRvbTogMjVweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTdweDtcbiAgYm90dG9tOiAyMnB4O1xufVxuXG4uaGFiYml0LXByb2dyZXNzIHtcbiAgLy8gYmFja2dyb3VuZDogcmdiKDE2MSwgMTkxLCAzOCk7XG4gIGhlaWdodDogNHB4O1xuICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbi5jb2xvci1ncmVlbiB7XG4gIGJhY2tncm91bmQ6IHJnYigxNjEsIDE5MSwgMzgpO1xufVxuXG4uY29sb3ItZ3JlZW4tdGV4dCB7XG4gIGNvbG9yOiByZ2IoMTYxLCAxOTEsIDM4KTtcbn1cblxuLmNvbG9yLXJlZCB7XG4gIGJhY2tncm91bmQ6ICRsaXBzdGljO1xufVxuXG4uY29sb3ItcmVkLXRleHQge1xuICBjb2xvcjogJGxpcHN0aWM7XG59XG5cbi5jb2xvci15ZWxsb3cge1xuICBiYWNrZ3JvdW5kOiByZ2IoMjU1LCAxNDAsIDcwKTtcbn1cblxuLmNvbG9yLXllbGxvdy10ZXh0IHtcbiAgY29sb3I6IHJnYigyNTUsIDE0MCwgNzApO1xufVxuXG4ubWVkaXVtLXRleHQge1xuICBjb2xvcjogIzMzMztcbiAgZm9udC1zaXplOiAuOXJlbTtcbiAgZm9udC1mYW1pbHk6ICRmb250O1xuICBtYXJnaW46IDA7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG5cbiAgLmxpZ2h0LXRleHQge1xuICAgIGNvbG9yOiByZ2IoMTUxLCAxNTEsIDE1MSk7XG4gIH1cbn1cblxuLmN1c3RvbS1wYWRkaW5nLXJpZ2h0IHtcbiAgcGFkZGluZy1yaWdodDogMS4ycmVtO1xufVxuXG4uZmFzdGluZyB7XG4gIC5jb2xsYXBzLXZpZXcge1xuICAgIGgzIHtcbiAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgIGNvbG9yOiAjRjg3MTlGO1xuICAgICAgZm9udC1mYW1pbHk6ICRmb250O1xuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB9XG5cbiAgICBoMiB7XG4gICAgICBmb250LXNpemU6IDEuNHJlbTtcbiAgICAgIGNvbG9yOiAjRjg3MTlGO1xuICAgICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgICB9XG5cbiAgICBoNSB7XG4gICAgICBmb250LXNpemU6IDFyZW07XG4gICAgICBjb2xvcjogIzcwNzA3MDtcbiAgICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgICB9XG4gIH1cblxuICAuZXhwZW5kZWQtdmlldyB7XG4gICAgLnN0YXJ0LXRpbWUge1xuICAgICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2IzYjNiMztcblxuICAgICAgc3BhbjpmaXJzdC1jaGlsZCB7XG4gICAgICAgIGNvbG9yOiAjMzMzMzMzO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMS4zcmVtO1xuICAgICAgfVxuXG4gICAgICBzcGFuOmxhc3QtY2hpbGQge1xuICAgICAgICBjb2xvcjogIzMzMzMzMztcbiAgICAgICAgZm9udC1mYW1pbHk6ICRmb250O1xuICAgICAgICBmb250LXNpemU6IC44cmVtO1xuICAgICAgfVxuICAgIH1cblxuICAgIC5lbmQtdGltZSB7XG4gICAgICBzcGFuOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgY29sb3I6ICMzMzMzMzM7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgICAgICAgZm9udC1zaXplOiAxLjNyZW07XG4gICAgICB9XG5cbiAgICAgIHNwYW46bGFzdC1jaGlsZCB7XG4gICAgICAgIGNvbG9yOiAjMzMzMzMzO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogLjhyZW07XG4gICAgICB9XG4gICAgfVxuXG4gICAgLm1pZGRsZS10aW1lIHtcbiAgICAgIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNiM2IzYjM7XG5cbiAgICAgIHNwYW46Zmlyc3QtY2hpbGQge1xuICAgICAgICBjb2xvcjogI0Y4NzE5RjtcbiAgICAgICAgZm9udC1mYW1pbHk6ICRmb250O1xuICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcbiAgICAgIH1cblxuICAgICAgc3BhbjpsYXN0LWNoaWxkIHtcbiAgICAgICAgY29sb3I6ICMzMzMzMzM7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgICAgICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAuZmFzdGluZy1ib3R0b20ge1xuICAgICAgc3BhbjpmaXJzdC1jaGlsZCB7XG4gICAgICAgIGNvbG9yOiAjMzMzMzMzO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogLjhyZW07XG4gICAgICB9XG5cbiAgICAgIHNwYW46bGFzdC1jaGlsZCB7XG4gICAgICAgIGNvbG9yOiAjRjg3MTlGO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMXJlbTtcblxuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4ud2F0ZXIge1xuICAuY29sbGFwcy12aWV3IHtcbiAgICBwYWRkaW5nLXRvcDogMDtcblxuICAgIGgzIHtcbiAgICAgIGZvbnQtc2l6ZTogMC45cmVtO1xuICAgICAgY29sb3I6ICMzNDk4REI7XG4gICAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgICBmb250LXdlaWdodDogNTAwO1xuICAgIH1cblxuICAgIGgyIHtcbiAgICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xuICAgICAgY29sb3I6ICNGODcxOUY7XG4gICAgICBmb250LXdlaWdodDogNTAwO1xuICAgIH1cblxuICAgIGg1IHtcbiAgICAgIGZvbnQtc2l6ZTogLjZyZW07XG4gICAgICBjb2xvcjogIzcwNzA3MDtcbiAgICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgICB9XG5cblxuXG4gIH1cblxuICAuZXhwZW5kZWQtdmlldyB7XG4gICAgLnN0YXJ0LXRpbWUge1xuICAgICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI2IzYjNiMztcblxuICAgICAgc3BhbjpmaXJzdC1jaGlsZCB7XG4gICAgICAgIGNvbG9yOiAjNzA3MDcwO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XG4gICAgICB9XG5cbiAgICAgIHNwYW46bGFzdC1jaGlsZCB7XG4gICAgICAgIGNvbG9yOiAjMzMzMzMzO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogLjhyZW07XG4gICAgICB9XG4gICAgfVxuXG4gICAgLmVuZC10aW1lIHtcbiAgICAgIHNwYW46Zmlyc3QtY2hpbGQge1xuICAgICAgICBjb2xvcjogIzcwNzA3MDtcbiAgICAgICAgZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG4gICAgICAgIGZvbnQtc2l6ZTogMS41cmVtO1xuICAgICAgfVxuXG4gICAgICBzcGFuOmxhc3QtY2hpbGQge1xuICAgICAgICBjb2xvcjogIzMzMzMzMztcbiAgICAgICAgZm9udC1mYW1pbHk6ICRmb250O1xuICAgICAgICBmb250LXNpemU6IC44cmVtO1xuICAgICAgfVxuICAgIH1cblxuICAgIC5taWRkbGUtdGltZSB7XG4gICAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjYjNiM2IzO1xuXG4gICAgICBzcGFuOmZpcnN0LWNoaWxkIHtcbiAgICAgICAgY29sb3I6ICNGODcxOUY7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAkZm9udC1ib2xkO1xuICAgICAgICBmb250LXNpemU6IDEuNXJlbTtcbiAgICAgIH1cblxuICAgICAgc3BhbjpsYXN0LWNoaWxkIHtcbiAgICAgICAgY29sb3I6ICM1ZjU2NTY7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgICAgICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAuZmFzdGluZy1ib3R0b20ge1xuICAgICAgc3BhbjpmaXJzdC1jaGlsZCB7XG4gICAgICAgIGNvbG9yOiAjNzA3MDcwO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XG4gICAgICB9XG5cbiAgICAgIHNwYW46bGFzdC1jaGlsZCB7XG4gICAgICAgIGNvbG9yOiAjRjg3MTlGO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgICAgIGZvbnQtc2l6ZTogLjhyZW07XG5cbiAgICAgIH1cbiAgICB9XG4gIH1cblxufVxuXG4uc21hbGwtYm94IHtcbiAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gIG1hcmdpbi1yaWdodDogMXJlbTtcbiAgd2lkdGg6IDEwMCU7XG4gIGJveC1zaGFkb3c6IDAuMnJlbSAwLjJyZW0gMC41cmVtICNiM2IzYjM7XG4gIHRvcDogMDtcbiAgcGFkZGluZzogLjVyZW07XG59XG5cbi5wYXJlbnQtY29sb3Ige1xuICBiYWNrZ3JvdW5kOiAjRjZGN0ZDICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiA3cmVtO1xuICBoZWlnaHQ6IDQwMHB4O1xuICAvLyBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxcmVtO1xuICAvLyBib3JkZXItdG9wLXJpZ2h0LXJhZGl1czogMXJlbTtcbn1cblxuLml0ZW0tdGV4dCB7XG4gIGNvbG9yOiAjNTU1NTU1O1xuICBmb250LXNpemU6IDAuNnJlbTtcbiAgZm9udC1mYW1pbHk6ICRmb250O1xuICBtYXJnaW46IDA7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gIHdpZHRoOiA1OHB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xufVxuXG4ucHJvZmlsZSB7XG4gIHdpZHRoOiAzcmVtO1xuICBoZWlnaHQ6IDNyZW07XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgLy8gcGFkZGluZzogMTBweDtcbiAgbWFyZ2luLXJpZ2h0OiAwLjVyZW07XG4gIG1hcmdpbi1sZWZ0OiAwLjVyZW07XG5cbiAgaW1nIHtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICB9XG59XG5cbi5wcm9maWxlLXNtYWxsIHtcbiAgY29sb3I6ICNmZmY7IC8vcmdiKDI1MiwgMTgwLCAxOTQpO1xuICBmb250LXNpemU6IC45cmVtO1xuICBmb250LWZhbWlseTogJGZvbnQ7XG4gIG1hcmdpbjogMDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDNweDtcbn1cblxuLnByb2ZpbGUtYmlnIHtcbiAgY29sb3I6ICR3aGl0ZTtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LWZhbWlseTogJGZvbnQtbWVkaXVtO1xuICBtYXJnaW46IDA7XG59XG5cbi5jb21wbGV0ZWQge1xuICAvLyBtYXJnaW4tcmlnaHQ6IDAuNXJlbTtcbiAgLy8gbWFyZ2luLWxlZnQ6IDAuNXJlbTtcbn1cblxuaW9uLWxpc3Qge1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07XG5cbiAgaW9uLWl0ZW0ge1xuICAgIC8vIG1hcmdpbi10b3A6IDAuNXJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiAwLjVyZW07XG4gIH1cbn1cblxuaW9uLWxhYmVsIHtcbiAgd2hpdGUtc3BhY2U6IG5vcm1hbDtcbn1cblxuaW9uLXRodW1ibmFpbCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgbWFyZ2luOiAwLjVyZW0gMDtcbiAgbWFyZ2luLXJpZ2h0OiAwLjZyZW07XG4gIHdpZHRoOiA2OHB4O1xuICBoZWlnaHQ6IDU4cHg7XG5cbiAgaW1nIHtcbiAgICBib3JkZXItcmFkaXVzOiAxMnB4Oy8vMXJlbTtcbiAgICBoZWlnaHQ6IDEwMCU7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbn1cblxuLm1hcmdpbi1yaWdodC0xIHtcbiAgbWFyZ2luLXJpZ2h0OiAxcmVtICFpbXBvcnRhbnRcbn1cblxuLmN1c3RvbS1ib3JkZXItYm90dG9tIHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNiNWI1YjU7XG59XG5cbi5jaXJjbGUge1xuICBoZWlnaHQ6IDRyZW07XG4gIHdpZHRoOiA0cmVtO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcblxuICBpbWcge1xuICAgIGhlaWdodDogYXV0bztcbiAgICB3aWR0aDogMzUlICFpbXBvcnRhbnQ7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgdG9wOiAzMiU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG59XG5cbi5zbWFsbC10ZXh0IHtcbiAgY29sb3I6ICRncmF5O1xuICBmb250LXNpemU6IDAuOHJlbTtcbiAgZm9udC1mYW1pbHk6ICRmb250O1xuICBtYXJnaW46IDA7XG59XG5cbi5zbWFsbC10ZXh0MSB7XG4gIGNvbG9yOiAjNTY1NjU2ICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMC44cmVtICFpbXBvcnRhbnQ7XG4gIGZvbnQtZmFtaWx5OiAkZm9udCAhaW1wb3J0YW50O1xuICBtYXJnaW46IDAgIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogMC41cmVtICFpbXBvcnRhbnQ7XG59XG5cbi5iaWctdGV4dCB7XG4gIGNvbG9yOiAkYmxhY2s7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgbWFyZ2luOiAwO1xufVxuXG4ucGFkZGluZy1sZWZ0LXJpZ2h0IHtcbiAgcGFkZGluZy1sZWZ0OiAxcmVtICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctcmlnaHQ6IDFyZW0gIWltcG9ydGFudDtcbn1cblxuLmJvcmRlci1ib3R0b20ge1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2YxZjFmMTtcbn1cblxuLmdyYXBoIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGxlZnQ6IDJweDtcbiAgd2lkdGg6IDk4JTtcbiAgbWFyZ2luLXRvcDogMC41cmVtO1xufVxuXG4uZ3JhcGgtbGFiZWwge1xuICBjb2xvcjogcmdiKDg1LCA4NSwgODUpO1xuICBmb250LXNpemU6IDAuNzVyZW07XG4gIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgbWFyZ2luOiAwO1xufVxuXG4uZ3JhcGgtbnVtYmVyIHtcbiAgY29sb3I6IHJnYig4NSwgODUsIDg1KTtcbiAgZm9udC1zaXplOiAxLjFyZW07XG4gIGZvbnQtZmFtaWx5OiAkZm9udDtcbn1cblxuLmN1c3RvbS1tYXJnaW4tbGVmdC1yaWdodCB7XG4gIG1hcmdpbi1sZWZ0OiAxcmVtO1xuICBtYXJnaW4tcmlnaHQ6IDAuNnJlbTtcbn1cblxuLmZvb2QtbmFtZSB7XG4gIGNvbG9yOiAjMzMzO1xufVxuXG4uZm9vZC1uYW1lLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IC13ZWJraXQtYm94O1xuICAtd2Via2l0LWxpbmUtY2xhbXA6IDI7XG4gIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi54LWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDMzcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzAxQTNBNDtcbiAgcGFkZGluZy10b3A6IDVweDtcbn1cblxuLngtYnV0dG9uLWFjdGl2ZS0yIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMzNweDtcbiAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDFyZW07XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDFyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgYmFja2dyb3VuZDogI0Y2RjdGQztcbiAgYm94LXNoYWRvdzogMCA1cHggNXB4IHJnYmEoMCwgMCwgMCwgMTYlKTtcbiAgY29sb3I6ICMxNDE0MTQ7XG4gIGZvbnQtZmFtaWx5OiAkZm9udC1ib2xkO1xufVxuXG4ueC1idXR0b24tYWN0aXZlLTEge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAzM3B4O1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxcmVtO1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBiYWNrZ3JvdW5kOiAjRjZGN0ZDO1xuICBib3gtc2hhZG93OiAwIDVweCA1cHggcmdiYSgwLCAwLCAwLCAxNiUpO1xuICBjb2xvcjogIzE0MTQxNDtcbiAgZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG59XG5cbi5idXR0b24tcG9zIHtcbiAgLy8gbWFyZ2luLWxlZnQ6IDFyZW07XG4gIC8vIG1hcmdpbi1yaWdodDogMXJlbTtcbiAgLy8gcGFkZGluZy10b3A6IDFyZW07XG59XG5cbi5iYXItMiB7XG4gIGZvbnQtc2l6ZTogLjhyZW07XG4gIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgZmxvYXQ6IHJpZ2h0O1xuICBjb2xvcjogZ3JheTtcbn1cblxuLmJhci1iYXIge1xuICAuYmFyLXBhcmVudCB7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgZm9udC1zaXplOiAuN3JlbTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcblxuICAgIC5iYXItMSB7XG4gICAgICBmbG9hdDogbGVmdDtcbiAgICAgIGNvbG9yOiAjNzA3MDcwO1xuICAgIH1cblxuICB9XG5cblxuICAuaGFiYml0LXByb2ctY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG4gICAgYmFja2dyb3VuZDogYW50aXF1ZXdoaXRlO1xuICAgIG1hcmdpbi1sZWZ0OiAycmVtO1xuXG4gICAgaWZyYW1lIHtcbiAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgbGVmdDogLTMycHg7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBoZWlnaHQ6IDIzNXB4O1xuICAgIH1cbiAgfVxuXG4gIC5iYXItY2hpbGQtY29udGFpbmVyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiBncmF5O1xuICAgIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gICAgaGVpZ2h0OiA1cHg7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xuICAgIG1hcmdpbi1ib3R0b206IDVweDtcblxuICAgIC5maWxsLWFtYmVyIHtcbiAgICAgIGJhY2tncm91bmQ6ICNGRkMyMDA7XG4gICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgICAgaGVpZ2h0OiA1cHg7XG4gICAgfVxuXG4gICAgLmZpbGwtcmVkIHtcbiAgICAgIGJhY2tncm91bmQ6ICNFNzRDM0M7XG4gICAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICAgICAgaGVpZ2h0OiA1cHg7XG4gICAgfVxuXG4gICAgLmZpbGwtZ3JlZW4ge1xuICAgICAgYmFja2dyb3VuZDogIzAxQTNBNDtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgICBoZWlnaHQ6IDVweDtcblxuICAgIH1cblxuXG5cbiAgICAuYmFyLWZpbGwtYyB7XG4gICAgICBiYWNrZ3JvdW5kOiByZ2IoMjQyLCAyNDgsIDg3KTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgICBoZWlnaHQ6IDVweDtcbiAgICB9XG5cblxuXG4gICAgLmJhci1maWxsLXAge1xuICAgICAgYmFja2dyb3VuZDogcmdiKDEyNSwgMjI2LCAyMzkpO1xuICAgICAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgICAgIGhlaWdodDogNXB4O1xuICAgIH1cblxuICAgIC5iYXItZmlsbC1mIHtcbiAgICAgIGJhY2tncm91bmQ6IHJnYigxNzMsIDE0MiwgMjUzKTtcbiAgICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgICBoZWlnaHQ6IDVweDtcbiAgICB9XG4gIH1cblxuICAuZmlsbC1yZWQtcCB7XG4gICAgY29sb3I6ICNFNzRDM0M7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gIH1cblxuICAuZmlsbC1hbWJlci1wIHtcbiAgICBjb2xvcjogI0ZGQzIwMDtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBmb250LXNpemU6IC44cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgfVxuXG4gIC5maWxsLWdyZWVuLXAge1xuICAgIGNvbG9yOiAjMDFBM0E0O1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgIGZvbnQtc2l6ZTogLjhyZW07XG4gICAgZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG4gIH1cblxuICAuYmFyLXBlcmNlbnQtYyB7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgY29sb3I6IHJnYigyNDIsIDI0OCwgODcpO1xuXG4gIH1cblxuICAuYmFyLXBlcmNlbnQtcCB7XG4gICAgZmxvYXQ6IGxlZnQ7XG4gICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICBjb2xvcjogcmdiKDEyNSwgMjI2LCAyMzkpO1xuICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgfVxuXG4gIC5iYXItcGVyY2VudC1mIHtcbiAgICBmbG9hdDogbGVmdDtcbiAgICBmb250LXNpemU6IC44cmVtO1xuICAgIGNvbG9yOiByZ2IoMTczLCAxNDIsIDI1Myk7XG4gICAgZm9udC1mYW1pbHk6ICRmb250O1xuICB9XG59XG5cbi5jYWxvcmllcy10b3RhbCB7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbn1cblxuaW9uLWNhcmQge1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW4tbGVmdDogNSU7XG4gIG1hcmdpbi1yaWdodDogMDtcbiAgYm94LXNoYWRvdzogbm9uZTtcbiAgbWluLWhlaWdodDogMTAwJTtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBib3JkZXItcmFkaXVzOiAxNXB4O1xuXG4gIGlvbi1jYXJkLWhlYWRlciB7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcblxuICAgIGlvbi1yb3d7XG4gICAgICBtaW4taGVpZ2h0OiAzcmVtO1xuICAgICAgbWF4LWhlaWdodDogM3JlbTtcblxuICAgICAgaW1ne1xuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICB9XG5cbiAgICAgIHNwYW4udGl0bGV7XG4gICAgICAgIC8vIGNvbG9yOiAjMDFBM0E0O1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG5cbiAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDVweDtcblxuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcbiAgICAgIH1cblxuICAgICAgc3Bhbi50aXRsZS1hY3Rpdml0eXtcbiAgICAgICAgY29sb3I6ICMxNENFQjc7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaW9uLWNhcmQtY29udGVudCB7XG4gICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICBwYWRkaW5nLWxlZnQ6IDBweDtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gICAgcGFkZGluZy1ib3R0b206IDVweDtcblxuICAgIGgyIHtcbiAgICAgIG1hcmdpbi1sZWZ0OiAwLjVyZW07XG4gICAgfVxuICB9XG5cbiAgLmNhcmQtY29udGVudCB7XG4gICAgcGFkZGluZy1ib3R0b206IDI4cHg7XG4gIH1cbn1cblxuaW9uLWNhcmQud2VpZ2h0LWdyYXBoIHtcbiAgYm94LXNoYWRvdzogMC4ycmVtIDAuMnJlbSAwLjVyZW0gI2IzYjNiMztcblxuICBpb24tY2FyZC1jb250ZW50IHtcbiAgICBtYXJnaW4tcmlnaHQ6IDAgIWltcG9ydGFudDtcbiAgICBwYWRkaW5nLXJpZ2h0OiAwICFpbXBvcnRhbnQ7XG4gIH1cbn1cblxuLnNlY29uZC1zZWN0aW9uIHtcbiAgYmFja2dyb3VuZDogJHllbGxvdy0yO1xuICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gIHBhZGRpbmctcmlnaHQ6IDFyZW07XG5cbiAgZGl2IHtcbiAgICBoMiB7XG4gICAgICBmb250LXNpemU6IDAuODc1cmVtO1xuICAgICAgZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG4gICAgICBjb2xvcjogJGdyYXk7XG4gICAgICBtYXJnaW4tYm90dG9tOiAwO1xuICAgIH1cblxuICAgIHAge1xuICAgICAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgICAgIGNvbG9yOiAjYjNiM2IzO1xuICAgIH1cbiAgfVxuXG4gIGltZyB7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIHdpZHRoOiA1MCUgIWltcG9ydGFudDtcbiAgfVxufVxuXG4uY3VzdG9tLWNvbG9yIHtcbiAgY29sb3I6IHJnYigyNTUsIDE2NywgMTkpO1xuICAvLyBmb250LWZhbWlseTogJGZvbnQ7XG59XG5cblxuLm92ZXJsYXkge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGNvbnRlbnQ6ICcnO1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xuICBvcGFjaXR5OiAuODtcbiAgei1pbmRleDogOTk5OTA7XG4gIHRvcDogMDtcbn1cblxuLnBhcmVudCB7XG4gIG1hcmdpbjogMS41cmVtIDEuNXJlbTtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiAxMDAwMDA7XG4gIHRvcDogN3B4O1xuICBtaW4taGVpZ2h0OiA1MTZweDtcbiAgcGFkZGluZzogMTVweDtcblxufVxuXG4ua2V5LW1hbnRyYS1pbmZvLFxuLm90aGVyLW1lYWwtaW5mbyB7XG4gIC5jdXN0b20tc2Nyb2xsIHtcbiAgICBvdmVyZmxvdy15OiBzY3JvbGw7XG4gICAgaGVpZ2h0OiA1MjBweDtcbiAgfVxuXG4gIC5vdmVybGF5IHtcbiAgICBwb3NpdGlvbjogZml4ZWQ7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDAlO1xuICAgIGNvbnRlbnQ6ICcnO1xuICAgIGJhY2tncm91bmQ6ICMwMDA7XG4gICAgb3BhY2l0eTogLjg7XG4gICAgei1pbmRleDogOTk5OTA7XG4gICAgdG9wOiAwO1xuICB9XG5cbiAgLnAge1xuICAgIGNvbG9yOiByZ2IoODUsIDg1LCA4NSk7XG4gICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gIH1cblxuICAucGFyZW50IHtcbiAgICBtYXJnaW46IDEuNXJlbSAxLjVyZW07XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB6LWluZGV4OiAxMDAwMDA7XG4gICAgdG9wOiA3cHg7XG4gICAgbWluLWhlaWdodDogNTE2cHg7XG4gICAgcGFkZGluZzogMTVweDtcblxuICB9XG5cbiAgaW9uLWJ1dHRvbiB7XG4gICAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogLTI2cHg7XG4gICAgdG9wOiAtMTFweDtcbiAgfVxuXG59XG5cblxuLy8gcmFqdmVsXG5cblxuLmtleS1tYW50cmEtaW5mbyxcbi5vdGhlci1tZWFsLWluZm8ge1xuICAuY3VzdG9tLXNjcm9sbCB7XG4gICAgb3ZlcmZsb3cteTogc2Nyb2xsO1xuICAgIGhlaWdodDogNTIwcHg7XG4gIH1cblxuICAub3ZlcmxheSB7XG4gICAgcG9zaXRpb246IGZpeGVkO1xuICAgIHdpZHRoOiAxMDAlO1xuICAgIGhlaWdodDogNzhweDtcbiAgICBjb250ZW50OiBcIlwiO1xuICAgIGJhY2tncm91bmQ6ICMwMDA7IFxuICAgIG9wYWNpdHk6IDAuODtcbiAgICB6LWluZGV4OiA5OTk5MDtcbiAgICB0b3A6IGF1dG87XG4gICAgYm90dG9tOiA1NXB4O1xuICB9XG5cbiAgLnAge1xuICAgIGNvbG9yOiByZ2IoODUsIDg1LCA4NSk7XG4gICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gIH1cblxuICAucGFyZW50IHtcbiAgICBtYXJnaW46IDEuNXJlbSAxLjVyZW07XG4gICAgYmFja2dyb3VuZDogI2ZmZjtcbiAgICBib3JkZXItcmFkaXVzOiAxcmVtO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB6LWluZGV4OiAxMDAwMDA7XG4gICAgdG9wOiA3cHg7XG4gICAgbWluLWhlaWdodDogNTE2cHg7XG4gICAgcGFkZGluZzogMTVweDtcblxuICB9XG5cbiAgaW9uLWJ1dHRvbiB7XG4gICAgbWFyZ2luLWxlZnQ6IDFyZW07XG4gICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogLTI2cHg7XG4gICAgdG9wOiAtMTFweDtcbiAgICB3aWR0aDogMzBweDtcbiAgfVxufVxuXG4uYmdfY29sb3Ige1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjlmNmZlICFpbXBvcnRhbnQ7XG4gIC0tYmFja2dyb3VuZDogI2Y5ZjZmZSAhaW1wb3J0YW50O1xufVxuXG4ucHJvZ3Jlc3Nfd3JhcHBlciB7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBoZWlnaHQ6IDIzN3B4O1xuICB3aWR0aDogMjUwcHg7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgbWFyZ2luLXRvcDogMTBweDtcbn1cblxuI3Byb2dyZXNzIHtcbiAgd2lkdGg6IDI3NXB4O1xuICBoZWlnaHQ6IDI3NXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICB6LWluZGV4OiAxMDAwO1xufVxuXG4uY2lyY2xlIHtcbiAvLyBiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvY2lyY2xlLnBuZycpIG5vLXJlcGVhdCBjZW50ZXIgY2VudGVyO1xuICAtd2Via2l0LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgaGVpZ2h0OiAyNDBweDtcbiAgd2lkdGg6IDI0MHB4O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTUwJSk7XG4gIHRvcDogMjBweDtcbn1cblxuLmhvcml6b250YWxfY2VudGVyIHtcbiAgbWFyZ2luLWxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xufVxuXG4uYWxhcm0ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMjVweDtcbiAgZm9udC1zaXplOiAzMHB4O1xuICBjb2xvcjogZ3JleTtcbiAgei1pbmRleDogMTAwMDA7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSBlYXNlLWluIDAuMXMsIGJveC1zaGFkb3cgZWFzZS1pbiAwLjI1cztcbiAgbGVmdDogMDtcblxuICAmOmJlZm9yZSxcbiAgJjphZnRlciB7XG4gICAgdHJhbnNpdGlvbjogYWxsIGVhc2UtaW4tb3V0IDAuNXM7XG4gIH1cblxuICAmOmFjdGl2ZSB7XG4gICAgZm9udC1zaXplOiAyNXB4O1xuICB9XG59XG5cbi5nbGFzcyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm90dG9tOiAwcHg7XG4gIHdpZHRoOiAzNXB4O1xuICBoZWlnaHQ6IDQ1cHg7XG4gIHBhZGRpbmc6IDA7XG4gIHotaW5kZXg6IDEwMDAwO1xuICBvdXRsaW5lOiBub25lICFpbXBvcnRhbnQ7XG4gIHRyYW5zaXRpb246IGFsbCBlYXNlLWluLW91dCAwLjNzO1xuICBsZWZ0OiAwO1xufVxuXG4udm9sdW1lIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDMwcHg7XG4gIGxlZnQ6IDUxJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01MCUpO1xuICB6LWluZGV4OiAxMDAwMDAwO1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiBncmV5O1xuICB0cmFuc2l0aW9uOiBhbGwgZWFzZS1pbi1vdXQgMC4zcztcbn1cblxuLmdsYXNzX2NvbnRhaW5lciB7XG4gIHRyYW5zaXRpb246IHRyYW5zZm9ybSBlYXNlLWluIDAuMXMsIGJveC1zaGFkb3cgZWFzZS1pbiAwLjI1cztcblxuICAmOmJlZm9yZSxcbiAgJjphZnRlciB7XG4gICAgdHJhbnNpdGlvbjogYWxsIGVhc2UtaW4tb3V0IDFzO1xuICB9XG5cbiAgJjphY3RpdmUge1xuICAgIC5nbGFzcyB7XG4gICAgICB3aWR0aDogMzNweDtcbiAgICB9XG5cbiAgICAudm9sdW1lIHtcbiAgICAgIGZvbnQtc2l6ZTogMTBweDtcbiAgICB9XG4gIH1cbn1cblxuLmNlbnRlcl90ZXh0IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDMwJTtcbiAgZm9udC1zaXplOiAzMHB4O1xufVxuXG4vL0J1YmJsZSBDb250YWluZXJcbi8qIEdlbmVyYWwgQ1NTIFNldHVwICovXG5ib2R5IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRibHVlO1xuICBmb250LWZhbWlseTogXCJVYnVudHUtSXRhbGljXCIsIFwiTHVjaWRhIFNhbnNcIiwgaGVsdmV0aWNhLCBzYW5zO1xufVxuXG4vKiBjb250YWluZXIgKi9cbi5jb250YWluZXIge1xuICBwYWRkaW5nOiA1JSA1JTtcbn1cblxuLyogQ1NTIHRhbGsgYnViYmxlICovXG4vLyAudGFsay1idWJibGUge1xuLy8gXHRtYXJnaW4tbGVmdDogN3B4O1xuLy8gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4vLyAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbi8vIFx0d2lkdGg6IDE1MHB4O1xuLy8gICBoZWlnaHQ6IGF1dG87XG4vLyAgIHRleHQtYWxpZ246IGNlbnRlcjtcbi8vICAgYmFja2dyb3VuZC1jb2xvcjogI2Q0ZWJmZDtcbi8vICAgYWxpZ24tc2VsZjogY2VudGVyO1xuLy8gICBwYWRkaW5nLWxlZnQ6IDJweDtcbi8vICAgcGFkZGluZy1yaWdodDogMnB4O1xuLy8gfVxuLy8gLmJvcmRlcntcbi8vICAgYm9yZGVyOiA4cHggc29saWQgIzY2Njtcbi8vIH1cbi8vIC5yb3VuZHtcbi8vICAgYm9yZGVyLXJhZGl1czogMzBweDtcbi8vIFx0LXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzMHB4O1xuLy8gXHQtbW96LWJvcmRlci1yYWRpdXM6IDMwcHg7XG5cbi8vIH1cblxuLy8gLyogUmlnaHQgdHJpYW5nbGUgcGxhY2VkIHRvcCBsZWZ0IGZsdXNoLiAqL1xuLy8gLnRyaS1yaWdodC5ib3JkZXIubGVmdC10b3A6YmVmb3JlIHtcbi8vIFx0Y29udGVudDogJyAnO1xuLy8gXHRwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyBcdHdpZHRoOiAwO1xuLy8gXHRoZWlnaHQ6IDA7XG4vLyAgIGxlZnQ6IC00MHB4O1xuLy8gXHRyaWdodDogYXV0bztcbi8vICAgdG9wOiAtOHB4O1xuLy8gXHRib3R0b206IGF1dG87XG4vLyBcdGJvcmRlcjogMzJweCBzb2xpZDtcbi8vIFx0Ym9yZGVyLWNvbG9yOiAjNjY2IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50IHRyYW5zcGFyZW50O1xuLy8gfVxuLy8gLnRyaS1yaWdodC5sZWZ0LXRvcDphZnRlcntcbi8vIFx0Y29udGVudDogJyAnO1xuLy8gXHRwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyBcdHdpZHRoOiAwO1xuLy8gXHRoZWlnaHQ6IDA7XG4vLyAgIGxlZnQ6IC0yMHB4O1xuLy8gXHRyaWdodDogYXV0bztcbi8vICAgdG9wOiAwcHg7XG4vLyBcdGJvdHRvbTogYXV0bztcbi8vIFx0Ym9yZGVyOiAyMnB4IHNvbGlkO1xuLy8gXHRib3JkZXItY29sb3I6ICNkNGViZmQgdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQgdHJhbnNwYXJlbnQ7XG4vLyB9XG5cbi5tb3JlX3RpcF9jb250YWluZXIge1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbn1cblxuLnRpcCB7XG4gIGhlaWdodDogNjVweDtcbiAgYWxpZ24tc2VsZjogY2VudGVyO1xufVxuXG4ubW9yZV90aXAge1xuICBoZWlnaHQ6IDMzcHg7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbn1cblxuLnRhbGt0ZXh0IHtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBmb250LXdlaWdodDogNDAwO1xufVxuXG4ubW9yZV90ZXh0IHtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM1MDliZWQ7XG4gIG1hcmdpbjogMDtcbn1cblxuLm1vcmVfdGlwX2JveCB7XG4gIGFsaWduLXNlbGY6IGNlbnRlcjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uZHJ5IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAwO1xuICB3aWR0aDogMzJweDtcbiAgdG9wOiA1MSU7XG4gIG1hcmdpbi1sZWZ0OiAtMTVweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMzBweDtcbn1cblxuLmRyb3Age1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAwO1xuICB0b3A6IDUxJTtcbiAgbWFyZ2luLXJpZ2h0OiAtMTVweDtcbiAgaGVpZ2h0OiAzMHB4O1xuICB3aWR0aDogMzBweDtcbn1cblxuLmNvbmZpcm0ge1xuICBmb250LXNpemU6IDEwcHg7XG4gIGNvbG9yOiBncmV5O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogMDtcbiAgbWFyZ2luLXRvcDogLTEwcHg7XG59XG5cbi5pb25fY29udGFpbmVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDcwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xufVxuXG4uaHlkcmF0ZWQge1xuICBhbGlnbi1zZWxmOiBjZW50ZXI7XG59XG5cbi52b2x1bWVfdGV4dCB7XG4gIGZvbnQtc2l6ZTogNXB4ICFpbXBvcnRhbnQ7XG59XG5cbkBrZXlmcmFtZXMgc2xpZGUtdXAge1xuICBmcm9tIHtcblxuICAgIG9wYWNpdHk6IDE7XG4gICAgbGluZS1oZWlnaHQ6IDMwMHB4O1xuICB9XG5cbiAgdG8ge1xuXG4gICAgb3BhY2l0eTogMDtcbiAgICBsaW5lLWhlaWdodDogMjVweDtcbiAgICB2aXNpYmlsaXR5OiBoaWRkZW47XG4gIH1cbn1cblxuLnNsaWRlLXVwIHtcbiAgYW5pbWF0aW9uOiBzbGlkZS11cCA0cztcbn1cblxuLmN1c3RvbV90b2FzdCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAtMzAlO1xuICBsZWZ0OiA1MCU7XG4gIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNTAlKTtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBvcGFjaXR5OiAwO1xuICBjb2xvcjogIzUwOWJlZDtcbiAgd2lkdGg6IDIwMHB4O1xuICBmb250LXdlaWdodDogYm9sZGVyO1xufVxuXG4uZmFkZS1pbiB7XG4gIGFuaW1hdGlvbjogZmFkZUluIGVhc2UgMTBzO1xuICAtd2Via2l0LWFuaW1hdGlvbjogZmFkZUluIGVhc2UgMTBzO1xuICAtbW96LWFuaW1hdGlvbjogZmFkZUluIGVhc2UgMTBzO1xuICAtby1hbmltYXRpb246IGZhZGVJbiBlYXNlIDEwcztcbiAgLW1zLWFuaW1hdGlvbjogZmFkZUluIGVhc2UgMTBzO1xufVxuXG5Aa2V5ZnJhbWVzIGZhZGVJbiB7XG4gIDAlIHtcbiAgICBvcGFjaXR5OiAwO1xuICB9XG5cbiAgMTAwJSB7XG4gICAgb3BhY2l0eTogMTtcbiAgfVxufVxuXG5ALW1vei1rZXlmcmFtZXMgZmFkZUluIHtcbiAgMCUge1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cblxuICAxMDAlIHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG5cbkAtd2Via2l0LWtleWZyYW1lcyBmYWRlSW4ge1xuICAwJSB7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuXG4gIDEwMCUge1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbn1cblxuQC1vLWtleWZyYW1lcyBmYWRlSW4ge1xuICAwJSB7XG4gICAgb3BhY2l0eTogMDtcbiAgfVxuXG4gIDEwMCUge1xuICAgIG9wYWNpdHk6IDE7XG4gIH1cbn1cblxuQC1tcy1rZXlmcmFtZXMgZmFkZUluIHtcbiAgMCUge1xuICAgIG9wYWNpdHk6IDA7XG4gIH1cblxuICAxMDAlIHtcbiAgICBvcGFjaXR5OiAxO1xuICB9XG59XG5cbi5yb3VuZF9wcm9ncmVzc19iYXIge1xuICBtYXJnaW46IGF1dG87XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5oZWFydF9wb2ludHMgLmNhbHNfdHh0LFxuLnN0ZXBzIC5zdGVwc190eHQge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmhlYXJ0X3BvaW50cyBzcGFuLFxuLnN0ZXBzIHNwYW4ge1xuICBmb250LXNpemU6IDE2cHg7XG59XG5cbi8vIC5oZWFydF9wb2ludHMgc3BhbiB7ZmxvYXQ6IHJpZ2h0O31cbi8vIC5oZWFydF9wb2ludHMgaW9uLWljb24ge1xuLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyAgIGZvbnQtc2l6ZTogMjBweDtcbi8vICAgdG9wOiA3cHg7XG4vLyAgIGxlZnQ6IDI3cHg7XG4vLyAgIGNvbG9yOiAjZmJjMDMwO1xuLy8gfVxuLnN0ZXBzIGlvbi1pY29uIHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBjb2xvcjogIzE2NEVBODtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDNweDtcbn1cblxuLy8gLnN0YXRzX3JvdyBzcGFuIHtcbi8vICAgZm9udC13ZWlnaHQ6IGJvbGQgIWltcG9ydGFudDtcbi8vICAgZm9udC1zaXplOiAxNnB4ICFpbXBvcnRhbnQ7XG4vLyB9XG4uc3RhdHNfcm93IGg1IHtcbiAgY29sb3I6ICMxQTczRjcgIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAyN3B4O1xuICAvLyBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLnN0YXRzX3JvdyB7XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi5pbm5lcl9ycF9iYXIge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogLTIwMHB4O1xuICBtYXJnaW46IGF1dG87XG59XG5cbi8vIGlvbi1jYXJkLWNvbnRlbnQge1xuLy8gICBwYWRkaW5nOiAzNXB4O1xuLy8gfVxuLmNvdW50c19ibGsge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTMlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgei1pbmRleDogOTk5O1xufVxuXG4uY291bnRzX2JsayBoMyB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDI1cHg7XG4gIGNvbG9yOiAjZmJjMDMwO1xufVxuXG4ucmVzcG9uc2VfdHh0IHtcbiAgY29sb3I6ICNmYmMwMzA7XG59XG5cbi5jb3VudHNfYmxrIGg0IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgY29sb3I6ICMxNjRFQTg7XG59XG5cbi5hY3RpdmF0ZV9maXRfYnRuIHtcbiAgbWFyZ2luLXRvcDogMzVweDtcbiAgLS1jb2xvcjogcmVkO1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkOiBub25lO1xuICAtLWJhY2tncm91bmQtYWN0aXZhdGVkLW9wYWNpdHk6IG5vbmU7XG4gIC0tYmFja2dyb3VuZC1mb2N1c2VkOiBub25lO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmNhbF9pbWFnZSB7XG4gIHdpZHRoOiAyMHB4O1xuICBtYXJnaW46IDBweCA2cHg7XG4gIHRvcDogM3B4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5jb3VudHNfYmxrIC5jYWxfaW1hZ2Uge1xuICB3aWR0aDogMjBweDtcbiAgdG9wOiAwcHg7XG4gIG1hcmdpbjogMHB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG59XG5cbmlvbi10ZXh0IHtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4ucmVmcmVzaF9idG4ge1xuICAtLXBhZGRpbmctdG9wOiAyN3B4O1xuICAtLXBhZGRpbmctYm90dG9tOiAyN3B4O1xuICAtLXBhZGRpbmctc3RhcnQ6IDE4cHg7XG4gIC0tcGFkZGluZy1lbmQ6IDE3cHg7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbmlvbi1idXR0b24gLnJlbG9hZF9pbWFnZSB7XG4gIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xuICB0cmFuc2l0aW9uOiB0cmFuc2Zvcm0gMC4ycyBsaW5lYXI7XG59XG5cbmlvbi1idXR0b24uaW9uLWFjdGl2YXRlZCAucmVsb2FkX2ltYWdlIHtcbiAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbiAgdHJhbnNpdGlvbjogdHJhbnNmb3JtIDAuMnMgbGluZWFyO1xufVxuXG4uZ29vZ2xlZml0X2ltZyB7XG4gIHdpZHRoOiAxMDBweDtcbiAgbWFyZ2luOiA5MHB4O1xufVxuXG4ucmVsb2FkX2ltYWdlIHtcbiAgd2lkdGg6IDQwcHg7XG4gIG1hcmdpbi10b3A6IDVweDtcbn1cblxuLnN0YXR1c190aXRsZSB7XG4gIGZvbnQtc2l6ZTogMjNweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIHdoaXRlLXNwYWNlOiBicmVhay1zcGFjZXM7XG59XG5cbi5kYXkge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzM1NWJiNztcbn1cblxuLmNvbnN1bWVkIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuXG4uY29uc3VtZWRfY29sIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbn1cblxuLmNvbnN1bWVkX3ZhbHVlIHtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBjb2xvcjogd2hpdGU7XG59XG5cbi5jb25zdW1lZF92YWx1ZV9jb2wge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZTYyZTI1O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBwYWRkaW5nOiAxMHB4O1xuICBhbGlnbi1zZWxmOiBjZW50ZXI7XG59XG5cbi5jb25zdW1lZF9yb3cge1xuICBib3JkZXItdG9wOiAycHggc29saWQgIzU3NzhjNTtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG59XG5cbi5sb3NzX3ZhbHVlX2NvbCB7XG4gIGJhY2tncm91bmQtY29sb3I6ICM0NmE2NDA7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIHBhZGRpbmc6IDEwcHg7XG4gIGFsaWduLXNlbGY6IGNlbnRlclxufVxuXG4ubG9zc19yb3cge1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG4ucmVzdWx0IHtcbiAgbWFyZ2luLXRvcDogMjBweDtcbiAgY29sb3I6ICM5ZjlhOWI7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xufVxuXG4uY2Fsb3JpZXNfY2FyZCB7XG4gIG1heC1oZWlnaHQ6IHVuc2V0O1xuICBwYWRkaW5nOiAxMHB4O1xuICB3aWR0aDogOTAlO1xufVxuXG4udGFyZ2V0IHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM5ZjlhOWI7XG59XG5cbi53YXRlcl9jYXJkIHtcbiAgcGFkZGluZzogMTBweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiA2NXB4O1xuICBtYXgtaGVpZ2h0OiB1bnNldDtcbiAgd2lkdGg6IDk1JTtcbiAgYm94LXNoYWRvdzogMC4ycmVtIDAuMnJlbSAwLjVyZW0gI2IzYjNiMztcbn1cblxuLndlaWdodF9jYXJkIHtcbiAgcGFkZGluZzogMTNweDtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIHBhZGRpbmctYm90dG9tOiAyNXB4O1xuICBtYXgtaGVpZ2h0OiB1bnNldDtcbiAgd2lkdGg6IDkwJTtcbn1cblxuLndlaWdodF90ZXh0IHtcbiAgZm9udC1zaXplOiAyMHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogIzQ1NDU0NTtcbiAgbGluZS1oZWlnaHQ6IDMwcHg7XG59XG5cbi53ZWlnaHRfZGF0ZSB7XG4gIGNvbG9yOiAjODU4NTg1O1xufVxuXG4ua2cge1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi53ZWlnaHRfdmFsdWUge1xuICBmb250LXNpemU6IDMwcHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGNvbG9yOiBibGFjaztcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvIFNsYWJcIiwgc2VyaWY7XG59XG5cbi53ZWlnaHRfcm93IHtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNmNWYwZWM7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICBwYWRkaW5nLXRvcDogMTBweDtcbiAgZm9udC1mYW1pbHk6ICdSb2JvdG8gU2xhYicsIHNlcmlmO1xufVxuXG4ud2VpZ2h0X2NhcmQ+LndlaWdodF9yb3c6bGFzdC1jaGlsZCB7XG4gIGJvcmRlci1ib3R0b206IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLndlaWdodF9pbXByb3ZlIHtcbiAgY29sb3I6ICM4ZmI1MWQ7XG59XG5cbi53ZWlnaHRfaW1nIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbn1cblxuLmNhbCB7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgdmVydGljYWwtYWxpZ246IDNweDtcbn1cblxuLnRleHRfY2VudGVyIHtcbiAgdGV4dC1hbGlnbjogLXdlYmtpdC1jZW50ZXI7XG4gIHRleHQtYWxpZ246IC1tb3otY2VudGVyO1xufVxuXG5cbi5hY3Rpdml0eSB7XG4gIGgzIHtcbiAgICBjb2xvcjogIzE0Q0VCNztcbiAgfVxuXG4gIC5hZGQtbW9yZS1wZXJjZW50YWdlIHtcbiAgICB0b3A6IDEwJSAhaW1wb3J0YW50O1xuICAgIGxlZnQ6IDMwJSAhaW1wb3J0YW50O1xuICB9XG5cbn1cblxuXG4ud2VpZ2h0IHtcbiAgcGFkZGluZy10b3A6IDAgIWltcG9ydGFudDtcblxuICBoMy53ZWlnaHQtY29sb3Ige1xuXG4gICAgZm9udC1mYW1pbHk6ICRmb250O1xuICAgIGZvbnQtc2l6ZTogLjlyZW07XG4gICAgY29sb3I6ICMwMUEzQTQ7XG4gICAgZm9udC13ZWlnaHQ6IDUwMDtcbiAgfVxuXG4gIC5maXJzdC1zZWMge1xuICAgIGZvbnQtc2l6ZTogMS4xcmVtO1xuICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgICBjb2xvcjogIzcwNzA3MDtcbiAgfVxuXG4gIC5maXJzdC1zZWMtMiB7XG4gICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgY29sb3I6ICM3MDcwNzA7XG4gIH1cblxuICAuc2Vjb25kLXNlYyB7XG4gICAgZm9udC1zaXplOiAxLjVyZW07XG4gICAgZm9udC1mYW1pbHk6ICRmb250O1xuICAgIGNvbG9yOiAjMDFBM0E0O1xuICB9XG5cbiAgLnNlY29uZC1zZWMtMiB7XG4gICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgY29sb3I6ICMwMUEzQTQ7XG4gIH1cblxuICAudGhpcmQtc2VjIHtcbiAgICBmb250LXNpemU6IDEuMXJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgY29sb3I6ICM3MDcwNzA7XG4gIH1cblxuICAudGhpcmQtc2VjLTIge1xuICAgIGZvbnQtc2l6ZTogLjhyZW07XG4gICAgZm9udC1mYW1pbHk6ICRmb250O1xuICAgIGNvbG9yOiAjNzA3MDcwO1xuICB9XG59XG5cbi53YXRlci1hbmltYXRpb24ge1xuXG4gICosXG4gICo6YmVmb3JlLFxuICAqOmFmdGVyIHtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIG91dGxpbmU6IG5vbmU7XG4gIH1cblxuICAkYmdDb2xvcjojMDIwNDM4O1xuICAkYm94Q29sb3I6IzAyMDQzODtcbiAgJHdhdGVyRkNvbG9yOiM0RDZERTM7XG4gICR3YXRlckJDb2xvcjojQzdFRUZGO1xuXG4gIGJvZHkge1xuICAgIGJhY2tncm91bmQ6ICRiZ0NvbG9yO1xuICAgIGZvbnQ6IDE0cHgvMSAnT3BlbiBTYW5zJywgaGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xuICAgIC13ZWJraXQtZm9udC1zbW9vdGhpbmc6IGFudGlhbGlhc2VkO1xuICB9XG5cbiAgLnBlcmNlbnROdW0ge1xuICAgIGZvbnQtc2l6ZTogLjg3NXJlbTtcbiAgfVxuXG4gIC5wZXJjZW50QiB7XG4gICAgZm9udC1zaXplOiAuODc1cmVtO1xuICB9XG5cblxuICAuYm94IHtcbiAgICAvLyB3aWR0aDogMTA1cHg7XG4gICAgLy8gaGVpZ2h0OiAxMDVweDtcbiAgICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgLy8gbWFyZ2luLXRvcDogMjUlO1xuICAgIC8vIGxlZnQ6IDUwJTtcbiAgICAvLyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgICAvLyBiYWNrZ3JvdW5kOiAjZDRlZWZmO1xuICAgIC8vIGJvcmRlcjogMnB4IHNvbGlkIHJnYigxMjUsIDE3NCwgMjQ5KTtcbiAgICAvLyBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgIC8vIG92ZXJmbG93OiBoaWRkZW47XG5cbiAgICB3aWR0aDogMTEwcHg7XG4gICAgaGVpZ2h0OiAxMTBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgbWFyZ2luLXRvcDogMjclO1xuICAgIGxlZnQ6IDUwJTtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbiAgICBiYWNrZ3JvdW5kOiAjZDRlZWZmO1xuICAgIGJvcmRlcjogMnB4IHNvbGlkICM3ZGFlZjk7XG4gICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuXG4gICAgLnBlcmNlbnQge1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgbGVmdDogMDtcbiAgICAgIHRvcDogMDtcbiAgICAgIHotaW5kZXg6IDM7XG4gICAgICB3aWR0aDogMTAwJTtcbiAgICAgIGhlaWdodDogMTAwJTtcbiAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICBkaXNwbGF5OiAtd2Via2l0LWZsZXg7XG4gICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICBjb2xvcjogI2ZmZjtcbiAgICAgIGZvbnQtc2l6ZTogNjRweDtcbiAgICB9XG5cbiAgICAud2F0ZXIge1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgbGVmdDogMDtcbiAgICAgIHRvcDogMDtcbiAgICAgIHotaW5kZXg6IC0xO1xuICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwLCAxMDAlKTtcbiAgICAgIGJhY2tncm91bmQ6IHJnYigxMjUsIDE3NCwgMjQ5KTtcbiAgICAgIHRyYW5zaXRpb246IGFsbCAuM3M7XG5cbiAgICAgICZfd2F2ZSB7XG4gICAgICAgIHdpZHRoOiAyMDAlO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGJvdHRvbTogMTAwJTtcblxuICAgICAgICAmX2JhY2sge1xuICAgICAgICAgIHJpZ2h0OiAwO1xuICAgICAgICAgIGZpbGw6IHJnYigxMjUsIDE3NCwgMjQ5KTtcbiAgICAgICAgICBhbmltYXRpb246IHdhdmUtYmFjayAxLjRzIGluZmluaXRlIGxpbmVhcjtcbiAgICAgICAgfVxuXG4gICAgICAgICZfZnJvbnQge1xuICAgICAgICAgIGxlZnQ6IDA7XG4gICAgICAgICAgZmlsbDogcmdiKDEyNSwgMTc0LCAyNDkpO1xuICAgICAgICAgIG1hcmdpbi1ib3R0b206IC0xcHg7XG4gICAgICAgICAgYW5pbWF0aW9uOiB3YXZlLWZyb250IC43cyBpbmZpbml0ZSBsaW5lYXI7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgfVxuXG4gIEBrZXlmcmFtZXMgd2F2ZS1mcm9udCB7XG4gICAgMTAwJSB7XG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAwKTtcbiAgICB9XG4gIH1cblxuICBAa2V5ZnJhbWVzIHdhdmUtYmFjayB7XG4gICAgMTAwJSB7XG4gICAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSg1MCUsIDApO1xuICAgIH1cbiAgfVxuXG5cblxufVxuXG5pb24tc2xpZGUge1xuICBpb24tY2FyZCB7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxMHB4O1xuICAgIG1hcmdpbi1yaWdodDogMjBweDtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGJveC1zaGFkb3c6IDBweCA1cHggMTBweCAwcHggcmdiYSgxMjksIDEzMywgMTMxLCAwLjEwKSAhaW1wb3J0YW50O1xuICB9XG59XG5cbi5kZXRveC1jYXJkIHtcbiAgLy8gYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm94LXNoYWRvdzogMHB4IDEwcHggOHB4IHJnYmEoMCwgMCwgMCwgMC4xNik7XG5cbiAgLm1haW4tdGl0bGUge1xuICAgIGNvbG9yOiAjMkNCMkIzO1xuICAgIGZvbnQtc2l6ZTogMTVweDtcbiAgICBmb250LXdlaWdodDogNzAwO1xuICAgIHBhZGRpbmctbGVmdDogMjBweDtcbiAgfVxuXG4gIC5kZXRveC10aXRsZSB7XG4gICAgZm9udC1zaXplOiAwLjdyZW07XG4gICAgY29sb3I6ICM2NjY2NjY7XG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgfVxufVxuXG4uZGV0b3gtbm9ybWFsLXBsYW4gaW9uLWJ1dHRvbntcbiAgdGV4dC10cmFuc2Zvcm06IGluaGVyaXQ7XG4gIG1hcmdpbjogMHB4O1xuICBoZWlnaHQ6IDIwcHg7XG4gIGZvbnQtZmFtaWx5OiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhclwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuICAtLXBhZGRpbmctc3RhcnQ6IDA7XG4gIC0tcGFkZGluZy1lbmQ6IDA7XG4gIC0tcGFkZGluZy1ib3R0b206IDBweDtcbiAgLS1ib3JkZXItd2lkdGg6IDFweCAhaW1wb3J0YW50O1xuICBib3gtc2hhZG93OiAxcHggMXB4IDEwcHggMnB4ICMwMDgxODI7XG4gIGJvcmRlci1yYWRpdXM6IDZweDtcbiAgXG4gICBiYWNrZ3JvdW5kOiAjRkZGO1xuICAtLWNvbG9yOiAjMDFBM0E0O1xuICBib3JkZXItd2lkdGg6IDBweCAhaW1wb3J0YW50O1xuICB3aWR0aDogN3JlbTtcbiAgZm9udC1zaXplOiAwLjhyZW07XG59XG5cbi50ZXh0LWdyZWVuIHtcbiAgY29sb3I6ICMwMUEzQTQ7XG59XG5cbi50ZXh0LW9yYW5nZSB7XG4gIGNvbG9yOiBvcmFuZ2U7XG59XG5cbi50cmlhbF9leHBpcmUge1xuICBjb2xvcjogcmVkO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiA1cHggMTBweCA1cHggMTBweDtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB6LWluZGV4OiA5OTk7XG4gIHRvcDogNDAlO1xuICBoZWlnaHQ6IDMwcHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjEyKSAwcHggNHB4IDE2cHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2U1ZTVlNTtcbn1cblxuLnRyaWFsIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiA1cHggMTBweCA1cHggMTBweDtcbiAgY29sb3I6IHJlZDtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB6LWluZGV4OiA5OTk7XG4gIHRvcDogNDAlO1xuICBoZWlnaHQ6IDMwcHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgYm94LXNoYWRvdzogcmdiYSgwLCAwLCAwLCAwLjEyKSAwcHggNHB4IDE2cHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm9yZGVyOiBzb2xpZCAxcHggI2U1ZTVlNTtcbn1cblxuaW9uLXNlZ21lbnQtYnV0dG9uIHtcbiAgbWluLXdpZHRoOiAwcHg7XG4gIC0tY29sb3ItY2hlY2tlZDogIzMwMzAzMDtcbiAgLS1jb2xvcjogIzg1ODU4NTtcbiAgaGVpZ2h0OiAzNHB4O1xuICBtaW4taGVpZ2h0OiAyNXB4O1xufVxuXG5pb24tc2VnbWVudC5pb24tY29sb3Itc2VnZW1lbnQge1xuICAtLWNvbG9yLWNoZWNrZWQ6ICMzMDMwMzA7XG4gIC0tY29sb3I6ICM4NTg1ODU7XG5cbiAgaW9uLWxhYmVsIHtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgZm9udC1zaXplOiAwLjY1cmVtO1xuICB9XG5cbiAgLnNlZ21lbnQtYnV0dG9uLWNoZWNrZWQge1xuICAgIC0taW5kaWNhdG9yLWJveC1zaGFkb3c6IG5vbmU7XG4gICAgLS1pbmRpY2F0b3ItY29sb3I6ICMwMUEzQTQ7XG4gICAgLS1pbmRpY2F0b3ItaGVpZ2h0OiAzcHg7XG4gICAgLS1pbmRpY2F0b3ItdHJhbnNpdGlvbjogdHJhbnNmb3JtIDI1MG1zIGN1YmljLWJlemllcigwLjQsIDAsIDAuMiwgMSk7XG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcblxuICAgIGlvbi1sYWJlbCB7XG4gICAgICBmb250LXdlaWdodDogNzAwO1xuICAgICAgZm9udC1zaXplOiAwLjc4cmVtO1xuICAgIH1cbiAgfVxufVxuXG5pb24tc2VnbWVudC1idXR0b24ge1xuICBtaW4td2lkdGg6IDBweDtcbiAgLS1jb2xvci1jaGVja2VkOiAjMzAzMDMwO1xuICAtLWNvbG9yOiAjODU4NTg1O1xuICBoZWlnaHQ6IDM0cHg7XG59XG5cblxuaW9uLWZhYi1idXR0b24ge1xuICB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OiAzMHB4O1xufVxuXG5pb24tZmFiLWxpc3Qge1xuICBtYXJnaW4tbGVmdDogMzVweDtcbiAgbWFyZ2luLXRvcDogLTEzcHg7XG59XG5cbi8vIC50cmlhbC1mYWJ7XG4vLyAgLy8gcG9zaXRpb246IGZpeGVkO1xuLy8gIC8vIHRvcDogNzAlO1xuLy8gIC8vIGxlZnQ6MDtcbi8vIH1cbi5kZXRveC1oZWFkaW5nIHtcbiAgY29sb3I6ICMwMWEzYTQ7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIHBhZGRpbmctbGVmdDogMTBweDtcbn1cblxuLmNhbC1idXR0b24ge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAzM3B4O1xuICAvLyAgZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgcGFkZGluZy10b3A6IDVweDtcbiAgYmFja2dyb3VuZDogI0ZGRjtcbiAgY29sb3I6ICMwMWEzYTQ7XG4gIGJveC1zaGFkb3c6IDAgNXB4IDVweCByZ2JhKDAsIDAsIDAsIDAuMTYpO1xufVxuXG4uY2FsLWJ1dHRvbi1hY3RpdmUge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAzM3B4O1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAvL2JvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxcmVtO1xuICAvLyAgYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogMXJlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogNXB4O1xuICBjb2xvcjogIzE0MTQxNDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJhY2tncm91bmQ6ICMwMWEzYTQ7XG4gIGNvbG9yOiAjRkZGO1xuICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbn1cblxuLmNhbC1oZWFkZXIge1xuICAtLWJhY2tncm91bmQ6ICNGRkY7XG4gIGhlaWdodDogMzVweDtcbiAgLS1taW4taGVpZ2h0OiAyLjZyZW0gIWltcG9ydGFudDtcblxuICBpb24tc2VnbWVudCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHotaW5kZXg6IDEzO1xuICAgIHRvcDogMHB4O1xuICAgIG1pbi1oZWlnaHQ6IDMwcHggIWltcG9ydGFudDtcbiAgfVxuXG59XG5cbi5zYW1wbGUge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogNTAlO1xuICBsZWZ0OiAyNSU7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICBvcGFjaXR5OiAwLjI7XG5cbiAgaW1nIHtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICB9XG59XG5cbi5zYW1wbGUtY2hhcnQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uZGVmaWNpdC1wcmVtaXVtIHtcbiAgaGVpZ2h0OiA1MHB4O1xuICBmbG9hdDogcmlnaHQ7XG59XG5cbi5jZW50ZXItd2VpZ2h0IHtcbiAgdG9wOiAzNCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMiU7XG4gIHdpZHRoOiAxMDZweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nLXRvcDogOCU7XG59XG5cbi5jZW50ZXItbWVhbCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiA0MCU7XG4gIHBhZGRpbmctdG9wOiA4JTtcbn1cblxuXG4ud2F0ZXItcGx1cyB7XG4gIGZvbnQtc2l6ZTogMi41cmVtO1xuICBjb2xvcjogIzM1ODBGMjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDE1cHg7XG5cbn1cblxuLndhdGVyLW1pbnVzIHtcbiAgZm9udC1zaXplOiAyLjVyZW07XG4gIGNvbG9yOiAjMzU4MEYyO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMTVweDtcbn1cblxuLmNhcmJzLXBlciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMzhweDtcbiAgdG9wOiAxOHB4O1xuICBjb2xvcjogIzRjYzJjMztcbn1cblxuLmNhcmJzLWxhYmVsIHtcbiAgZm9udC1zaXplOiAwLjhyZW07XG4gIGZvbnQtZmFtaWx5OiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhclwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGNvbG9yOiBncmF5O1xuICBsZWZ0OiAyOHB4O1xuICB0b3A6IDM1cHg7XG59XG5cbi5idXJuZWQtbGFiZWwge1xuICBmb250LXNpemU6IDAuOHJlbTtcbiAgZm9udC1mYW1pbHk6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1yZWd1bGFyXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgY29sb3I6IGdyYXk7XG4gIGxlZnQ6IDI4cHg7XG4gIHRvcDogMzVweDtcbn1cblxuLmNhcmJzLWwyIHtcbiAgY29sb3I6ICM3MDcwNzA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMjhweDtcbiAgdG9wOiA1NnB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxcmVtO1xufVxuXG4ud2F0ZXItbGFiZWwge1xuICB3aWR0aDogNTIlO1xuICBoZWlnaHQ6IDUyJTtcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xuICBib3JkZXI6IDF4IHNvbGlkICNiM2IzYjM7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgdG9wOiAzMCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMjUlO1xuICBwYWRkaW5nLXRvcDogOCU7XG4gIGNvbG9yOiAjMzQ5OERCO1xufVxuXG5cbi5maXJzdC1wcm9ncmVzcyB7XG4gIG1hcmdpbi10b3A6IDFyZW07XG4gIG1hcmdpbi1sZWZ0OiAuNXJlbTtcbiAgbWFyZ2luLXJpZ2h0OiAuNXJlbTtcbiAgaGVpZ2h0OiA0LjVyZW1cbn1cblxuLnNlY29uZC1wcm9ncmVzcyB7XG4gIGhlaWdodDogNC41cmVtO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICBtYXJnaW4tbGVmdDogLjVyZW07XG4gIG1hcmdpbi1yaWdodDogLjVyZW07XG59XG5cbi5hY3QtY2FsIHtcbiAgLy8gcG9zaXRpb246IHJlbGF0aXZlO1xuICAvLyB0b3A6IDg1cHg7XG4gIC8vcmlnaHQ6IDEwcHg7XG4gIGNvbG9yOiAjMDFBM0E0O1xuXG4gIGltZyB7XG4gICAgaGVpZ2h0OiAxNXB4O1xuICAgIHdpZHRoOiAxMXB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IDJweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcbiAgfVxuXG4gIHNwYW4ge1xuICAgIGZvbnQtc2l6ZTogMC45cmVtO1xuICB9XG59XG5cbjo6bmctZGVlcCAucGxhbntcbiAgLy8gLS1iYWNrZ3JvdW5kOiAjZmZmO1xuICAvLyBjb2xvcjogIzAxQTNBNDtcbiAgZm9udC1zaXplOiAxcmVtO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuXG4gIGlvbi1pY29ue1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICB9XG59XG5cbi5oZWFkZXItcHJvZ3Jlc3MtY2lyY2xle1xuICAvLyAtLWJhY2tncm91bmQ6XCJhc3NldHMvaW1nL2hlYWRlci1yb3VuZC5zdmc7XCJcbiAgLy8gYmFja2dyb3VuZDogdXJsKCdhc3NldHMvaW1nL2hlYWRlci1yb3VuZC5zdmdzJykgbm8tcmVwZWF0IDEwMCUgMTAwJTtcbiAgLy8gYmFja2dyb3VuZDogdXJsKCcuLi8uLi9hc3NldHMvaW1nL2hlYWRlci1yb3VuZC5zdmcnKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlcjtcbiAgLy8gcG9zaXRpb246IHJlbGF0aXZlO1xuICAvLyB0b3A6IDAlO1xuICAvLyBsZWZ0OiA1MCU7XG4gIC8vIHRyYW5zZm9ybTogdHJhbnNsYXRlKC01MCUsIC01MCUpO1xuXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMUEzQTQ7XG4gIC8vIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMDtcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDA7XG4gIGNvbG9yOiB3aGl0ZTtcblxuICAuaGVhZGVyLXByb2dyZXNzLWNpcmNsZS1yb3d7XG4gICAgbWFyZ2luOiAtMTVweCAwIC0yMHB4IDA7XG4gIH1cblxuICAuaGVhZGVyLWNhbG9yaWUtcmluZy10ZXh0e1xuICAgIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAvLyB0b3A6IDM1JTtcbiAgICAvLyBjb2xvcjogYmx1ZTtcbiAgICAvLyBsZWZ0OiAxNyU7XG5cbiAgICAvLyB0b3A6IDRyZW07XG4gICAgLy8gcG9zaXRpb246IGFic29sdXRlO1xuICAgIC8vIGxlZnQ6IDEuM3JlbTtcbiAgICAvLyB3aWR0aDogMTByZW07XG4gICAgY29sb3I6ICMwMUEzQTQ7XG4gICAgLy8gdG9wOiAzMCU7XG4gICAgdG9wOiAyNCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIGxlZnQ6IDEwJTtcbiAgICB3aWR0aDogODAlO1xuICB9XG5cbiAgaW9uLWxhYmVsLmRhdGV7XG4gICAgZm9udC1zaXplOiAxcmVtO1xuICAgIGNvbG9yOmdyZXk7XG4gIH0gIFxuXG4gIGlvbi1sYWJlbC5kYXJrR3JleXtcbiAgICBjb2xvcjpyZ2IoNiwgNiwgNik7XG4gICAgY29sb3I6ICM4ODg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIH1cblxuICAvLyBpb24tbGFiZWwubm9uLWRhcmt7XG4gIC8vICAgYmFja2dyb3VuZC1jb2xvcjogI0NDQztcbiAgLy8gfVxuXG4gIGlvbi1pY29uLmFycm93e1xuICAgIGZvbnQtc2l6ZTogMnJlbTtcbiAgICBvcGFjaXR5OiAwLjhyZW07XG4gIH1cblxuICAudG90YWwtY2Fsb3JpZXN7XG4gICAgbGluZS1oZWlnaHQ6IDEuNzU7XG5cbiAgICAuY3VyLWNhbG9yaWVze1xuICAgICAgZm9udC1zaXplOiAycmVtO1xuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG5cbiAgICAgIHNwYW57XG4gICAgICAgIGZvbnQtc2l6ZTogMC43cmVtO1xuICAgICAgfVxuICAgIH1cblxuICAgIC5rY2FsVGV4dHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgICAgbGluZS1oZWlnaHQ6IDBweDtcbiAgICB9XG4gIH1cblxuICAuZGlldC1wbGFue1xuICAgIGZvbnQtc2l6ZTogMC45cmVtO1xuICB9XG5cbiAgLm51dHJpZW50cy12YWx1ZXtcbiAgICBmb250LXNpemU6IDAuOXJlbTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICAgIFxuICAgIGlvbi1sYWJlbHtcbiAgICAgIHdpZHRoOiA1cmVtO1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICAgIH1cbiAgfVxuXG4gIC8vIHRyYW5zZm9ybTogc2NhbGUoMS4zKVxufVxuXG4ubm8tbWVhbC1jb250YWluZXJ7XG4gIGhlaWdodDogMjB2aDtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3NjhweCkge1xuICAubm8tbWVhbC1jb250YWluZXJ7XG4gICAgaGVpZ2h0OiAxNHZoO1xuICB9XG4gIC50b3RhbC1jYWxvcmllc3tcbiAgICBsZWZ0OiAyNSU7ICAgIFxuICB9XG4gIC5jdXItY2Fsb3JpZXN7XG4gICAgZm9udC1zaXplOiA0cmVtO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB9XG4gIFxuICAucGxhbntcbiAgICAvLyAtLWJhY2tncm91bmQ6ICNmZmY7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgZm9udC1zaXplOiAxLjVyZW07XG4gIH1cbiAgLmhlYWRlci1wcm9ncmVzcy1jaXJjbGV7XG4gICAgLmhlYWRlci1jYWxvcmllLXJpbmctdGV4dHtcbiAgICAgIHRvcDogMzUlO1xuICAgIH1cbiAgfVxuXG4gIC5oZWFkZXItcHJvZ3Jlc3Mtb3ZhbHtcbiAgICBiYWNrZ3JvdW5kLXNpemU6IDQwMCU7XG4gIH1cbn3igItcblxuLmFjdC1zdGVwcyB7XG4gIGNvbG9yOiAjMzQ5OERCO1xuICAvLyB0b3A6IDU4JTtcbiAgLy8gbGVmdDogMzElO1xuICAvLyBwb3NpdGlvbjogYWJzb2x1dGU7XG5cbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDg1cHg7XG4gIGNvbG9yOiAjMDFBM0E0O1xuXG4gIGltZyB7XG4gICAgLy8gaGVpZ2h0OiAxNXB4O1xuICAgIC8vIHdpZHRoOiAxMXB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IDNweDtcbiAgICBtYXJnaW4tcmlnaHQ6IDJweDtcblxuICB9XG5cbiAgc3BhbiB7XG4gICAgZm9udC1zaXplOiAwLjlyZW07XG4gIH1cbn1cblxuLnNvdXJjZSB7XG4gIGNvbG9yOiAjMTQ5MkU2O1xuICB0ZXh0LWRlY29yYXRpb246IHVuZGVybGluZTtcbiAgZm9udC1zaXplOiAwLjhlbTtcbiAgcGFkZGluZy10b3A6IDJweDtcbiAgLy9tYXJnaW46IDEwcHggMHB4IDEwcHggMHB4O1xufVxuXG4ubWVhbC1sYWJlbCB7XG4gIC8vICAgY29sb3I6I2ZmYTUwMDtcbiAgdG9wOiAxMSU7XG4gIC8vIGxlZnQ6IDMlO1xuICBmb250LXNpemU6IDAuOXJlbTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uZGVmLWxhYmVsIHtcbiAgY29sb3I6ICMxNDkyRTY7XG4gIGZvbnQtc2l6ZTogLjhyZW07XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lO1xufVxuXG4uZ2xhc3MtdGV4dCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMyU7XG4gIC8vIHRvcDogLTUwcHg7XG4gIHRvcDogLTEyNXB4O1xuICBjb2xvcjogIzUwOWJlZDtcbiAgei1pbmRleDogOTk5O1xuICB3aWR0aDogMTUycHg7XG59XG5cbi5hZGQge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMzBweDtcbiAgbGVmdDogMnB4O1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgY29sb3I6ICM1MDliZWQ7XG59XG5cbi5taW51cyB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiAzMHB4O1xuICByaWdodDogMnB4O1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgY29sb3I6ICM1MDliZWQ7XG59XG5cbi5zY3JvbGxpbmctd3JhcHBlciB7XG4gIG92ZXJmbG93LXg6IHNjcm9sbDtcbiAgb3ZlcmZsb3cteTogaGlkZGVuO1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBiYWNrZ3JvdW5kOiAjRjZGN0ZDO1xuICBwYWRkaW5nOiAxMHB4O1xuXG4gIGlvbi1pdGVtIHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgcGFkZGluZzogMHB4O1xuICAgIC0tcGFkZGluZy1zdGFydDogMHB4O1xuICAgIG1hcmdpbjogMHB4O1xuICAgIC0tcGFkZGluZy1lbmQ6IDBweDtcblxuICB9XG59XG5cbi5zY3JvbGwtbGlzdCB7XG4gIGhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiAyMDBweDtcblxufVxuXG4ud2hpdGUtc3BhY2Uge1xuICBkaXNwbGF5OiBpbmhlcml0ICFpbXBvcnRhbnQ7XG4gIHdoaXRlLXNwYWNlOiBwcmUtbGluZTtcbn1cblxuLnByZW1pdW0ge1xuICAvLyB3aWR0aDogNTBweDtcblxuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDFyZW07XG4gIHRvcDogMnB4O1xuICBoZWlnaHQ6IDgwJTtcbiAgd2lkdGg6IGF1dG87XG59XG5cbi5wcmVtaXVtLXJpZ2h0e1xuICBoZWlnaHQ6IDN2aDtcbiAgdG9wOiA3cHg7XG4gIHJpZ2h0OiAxcmVtO1xuICB6LWluZGV4OiA5O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5pb24tY2FyZC1mb290ZXIge1xuICAvLyBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3R0b206IDA7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmlvbi1jYXJkLWJ1dHRvbiB7XG4gIC0tY29sb3I6ICM3MDcwNzA7XG4gIHdpZHRoOiA2MCU7XG4gIG1hcmdpbjogYXV0bztcbiAgYm9yZGVyLXJhZGl1czogMXJlbTtcbiAgYmFja2dyb3VuZDogIzQ2QTFBMztcbiAgLS1jb2xvcjogd2hpdGUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAycmVtO1xuICBtYXJnaW4tdG9wOiAxcmVtO1xuICAvLyBtYXJnaW4tYm90dG9tOiAyLjI1cmVtO1xuICBtYXJnaW4tYm90dG9tOiAxcmVtO1xufVxuXG4uaW9uLWNhcmQtaGVhZGVyLWN1c3RvbSB7XG4gIC0tcGFkZGluZy1zdGFydDogMjBweDtcbiAgLS1wYWRkaW5nLWVuZDogMTVweDtcbiAgLy8gLS1ib3JkZXItY29sb3I6ICNFRUU7XG4gIC0tYm9yZGVyLWNvbG9yOiAjZWVlMDtcbiAgLS1pbm5lci1wYWRkaW5nLWJvdHRvbTogNXB4O1xufVxuXG4uc2Vjb25kYXJ5LXNsaWRlIHtcbiAgOjpuZy1kZWVwIGlvbi1zbGlkZXtcbiAgICBtYXJnaW4tcmlnaHQ6IDE1cHggIWltcG9ydGFudDtcbiAgfVxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRjZGN0ZDO1xuXG4gIGlvbi1zbGlkZSBpb24tY2FyZCB7XG4gICAgbWFyZ2luLWJvdHRvbTogLTJweDtcblxuICAgIGlvbi1jYXJkLWhlYWRlciB7XG4gICAgICBwYWRkaW5nOiAwO1xuICAgICAgYmFja2dyb3VuZC1jb2xvcjogI0ZGRjtcblxuICAgICAgaW1nLnByZW1pdW17XG4gICAgICAgIGhlaWdodDogOTAlO1xuICAgICAgICB3aWR0aDogOTAlO1xuICAgICAgICBwb3NpdGlvbjogaW5oZXJpdDtcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDAuNzVyZW07XG4gICAgICB9XG4gICAgfVxuICB9XG4gIGlvbi1jYXJkLWNvbnRlbnQgaW9uLXJvdyB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKDEwMCUpO1xuICB9XG59XG5cbjo6bmctZGVlcCAuc2Vjb25kYXJ5LXNsaWRlIC5zd2lwZXItcGFnaW5hdGlvbntcbiAgcGFkZGluZy10b3A6IDM1cHg7XG4gIG1hcmdpbi1ib3R0b206IC01cHg7XG4gIHBhZGRpbmctcmlnaHQ6IDglICFpbXBvcnRhbnQ7XG59XG5cbjo6bmctZGVlcCAuc3dpcGVyLXBhZ2luYXRpb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmUgIWltcG9ydGFudDtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIHRyYW5zZm9ybTogc2NhbGUoMC41KTtcbn1cblxuOjpuZy1kZWVwIC5tYWluLWNhcmQgLnN3aXBlci1wYWdpbmF0aW9ue1xuICBwYWRkaW5nLXRvcDogMCAhaW1wb3J0YW50O1xufVxuXG4uaGVhZGVyLXJvdW5ke1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogMCU7XG4gIGxlZnQ6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTUwJSwgLTUwJSk7XG59XG5cbi5kYXRlLWFycm93c3tcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDEuNXJlbTtcbn1cblxuLmhlYWRlci1wcm9ncmVzcy1vdmFse1xuICAvLyBiYWNrZ3JvdW5kLWNvbG9yOiAjMDFBM0E0O1xuICAvLyBoZWlnaHQ6IDVyZW07XG4gIGhlaWdodDogMy4zcmVtO1xuXG4gLy9pbWFnZXMvaGVhZGVyLWNpcmNsZS5zdmcnKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogYm90dG9tO1xuXG4gIC8vIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgLy8gYm9yZGVyLXRvcC1sZWZ0LXJhZGl1czogMDtcbiAgLy8gYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDA7XG5cbiAgaW9uLWljb257XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoOTBkZWcpO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICBib3R0b206IC0wLjVyZW07XG4gICAgY29sb3I6IHdoaXRlO1xuXG4gICAgLy8gLmFycm93e1xuICAgICAgZm9udC1zaXplOiAycmVtO1xuICAgICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgLy8gfVxuICB9XG5cbiAgLm51dHJpZW50cy10aXRsZXtcbiAgICBmb250LXNpemU6IDAuOXJlbTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBcbiAgICBpb24tbGFiZWx7XG4gICAgICB3aWR0aDogNHJlbTsvLzVyZW07XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICBwYWRkaW5nLWJvdHRvbTogNXB4O1xuICAgICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYigyNTUsIDI1NSwgMjU1LCA1MCUpO1xuICAgIH1cbiAgfVxufVxuXG4uaGVhZGVyLXByb2dyZXNzLWNpcmNsZXtcbiAgLm51dHJpZW50cy10aXRsZXtcbiAgICBmb250LXNpemU6IDAuOXJlbTtcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICBcbiAgICAjYnVybmVkLWxhYmVse1xuICAgICAgd2lkdGg6IDRyZW07Ly81cmVtO1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCByZ2IoMjU1LCAyNTUsIDI1NSwgNTAlKTtcbiAgICB9XG4gIH1cbn1cblxuLmFsZXJ0LWFibm9ybWFse1xuICBhbmltYXRpb246IHNoYWtlIDAuNXM7XG4gIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IGluZmluaXRlO1xuICAvLyBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XG59XG5cbi5hbGVydC1hYm5vcm1hbC1yaWdodHtcbiAgYW5pbWF0aW9uOiBzaGFrZVJpZ2h0IDAuNXM7XG4gIGFuaW1hdGlvbi1pdGVyYXRpb24tY291bnQ6IGluZmluaXRlO1xuICAvLyBjb2xvcjogcmVkICFpbXBvcnRhbnQ7XG59XG5cbi5hbGVydC1hYm5vcm1hbC1sZWZ0e1xuICBhbmltYXRpb246IHNoYWtlTGVmdCAwLjVzO1xuICBhbmltYXRpb24taXRlcmF0aW9uLWNvdW50OiBpbmZpbml0ZTtcbiAgLy8gY29sb3I6IHJlZCAhaW1wb3J0YW50O1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNzcwcHgpIHtcblxuICAud2F0ZXItd2VpZ2h0LXNlY3Rpb24ge1xuICAgIGhlaWdodDogMzU2cHg7XG4gIH1cblxufVxuXG5Aa2V5ZnJhbWVzIHNoYWtlTGVmdCB7XG4gIC8vIDAlIHsgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMXB4LCAxcHgpIHJvdGF0ZSg5MGRlZyk7IH1cbiAgLy8gMjAlIHsgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTFweCwgLTJweCkgcm90YXRlKDg5ZGVnKTsgfVxuICAvLyA0MCUgeyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtM3B4LCAwcHgpIHJvdGF0ZSg5MWRlZyk7IH1cbiAgLy8gNjAlIHsgdHJhbnNmb3JtOiB0cmFuc2xhdGUoM3B4LCAycHgpIHJvdGF0ZSg5MGRlZyk7IH1cbiAgLy8gODAlIHsgdHJhbnNmb3JtOiB0cmFuc2xhdGUoMXB4LCAtMXB4KSByb3RhdGUoOTFkZWcpOyB9XG4gIC8vIDEwMCUgeyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtMXB4LCAycHgpIHJvdGF0ZSg4OWRlZyk7IH1cbiAgNTAlIHsgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTVweCkgc2NhbGUoMS4yKTsgfVxufVxuXG5Aa2V5ZnJhbWVzIHNoYWtlUmlnaHQge1xuICAvLyAwJSB7IHRyYW5zZm9ybTogdHJhbnNsYXRlKDFweCwgMXB4KSByb3RhdGUoOTBkZWcpOyB9XG4gIC8vIDIwJSB7IHRyYW5zZm9ybTogdHJhbnNsYXRlKC0xcHgsIC0ycHgpIHJvdGF0ZSg4OWRlZyk7IH1cbiAgLy8gNDAlIHsgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTNweCwgMHB4KSByb3RhdGUoOTFkZWcpOyB9XG4gIC8vIDYwJSB7IHRyYW5zZm9ybTogdHJhbnNsYXRlKDNweCwgMnB4KSByb3RhdGUoOTBkZWcpOyB9XG4gIC8vIDgwJSB7IHRyYW5zZm9ybTogdHJhbnNsYXRlKDFweCwgLTFweCkgcm90YXRlKDkxZGVnKTsgfVxuICAvLyAxMDAlIHsgdHJhbnNmb3JtOiB0cmFuc2xhdGUoLTFweCwgMnB4KSByb3RhdGUoODlkZWcpOyB9XG4gIDUwJSB7IHRyYW5zZm9ybTogdHJhbnNsYXRlKDVweCkgc2NhbGUoMS4yKTsgfVxufVxuXG5Aa2V5ZnJhbWVzIHNoYWtlIHtcbiAgLy8gMCUgeyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgxcHgsIDFweCkgcm90YXRlKDkwZGVnKTsgfVxuICAvLyAyMCUgeyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtMXB4LCAtMnB4KSByb3RhdGUoODlkZWcpOyB9XG4gIC8vIDQwJSB7IHRyYW5zZm9ybTogdHJhbnNsYXRlKC0zcHgsIDBweCkgcm90YXRlKDkxZGVnKTsgfVxuICAvLyA2MCUgeyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgzcHgsIDJweCkgcm90YXRlKDkwZGVnKTsgfVxuICAvLyA4MCUgeyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgxcHgsIC0xcHgpIHJvdGF0ZSg5MWRlZyk7IH1cbiAgLy8gMTAwJSB7IHRyYW5zZm9ybTogdHJhbnNsYXRlKC0xcHgsIDJweCkgcm90YXRlKDg5ZGVnKTsgfVxuICA1MCUgeyB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgwcHgsIDNweCkgcm90YXRlKDkwZGVnKTsgfVxufVxuXG4ucmVmcmVzaC1pY29ue1xuICBjb2xvcjojMTQ5MkU2O1xuICBtYXJnaW4tbGVmdDogMDtcbiAgbWFyZ2luLXJpZ2h0OiAtMTBweDtcbn1cblxuXG5pb24taGVhZGVyLmhlYWRlci1pb3N7XG4gIGhlaWdodDogM3JlbSAhaW1wb3J0YW50O1xufVxuXG5pb24taGVhZGVyIHtcbiAgaGVpZ2h0OiAzcmVtICFpbXBvcnRhbnQ7XG4gIGlvbi10b29sYmFyIHtcbiAgICAtLW1pbi1oZWlnaHQ6IDNyZW0gIWltcG9ydGFudDtcbiAgICAtLWJhY2tncm91bmQ6IHdoaXRlO1xuICAgIC0tY29sb3I6ICMwMUEzQTQ7XG4gIH1cbn1cblxuLmN1c3RvbS1idWxsZXQge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGhlaWdodDogMTBweDtcbiAgd2lkdGg6IDMwcHg7XG4gIC8vIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiA1MCUgMTAwJTtcbiAgLy8gYm9yZGVyLWJvdHRvbS1yaWdodC1yYWRpdXM6IDUwJSAxMDAlO1xuICAvLyBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1MCUgMTAwJTtcbiAgLy8gYm9yZGVyLWJvdHRvbS1sZWZ0LXJhZGl1czogNTAlIDEwMCU7XG4gIGJhY2tncm91bmQ6ICNEREQ7XG59XG5cbi5hY3RpdmUtY3VzdG9tLWJ1bGxldHtcbiAgYmFja2dyb3VuZDogIzAxQTNBNFxufVxuXG4uY3VzdG9tLWJ1bGxldC1zcGFjZSB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgaGVpZ2h0OiAxMHB4O1xuICB3aWR0aDogMTBweDtcbiAgYmFja2dyb3VuZDogI0Y2RjdGQztcbn1cblxuLmxheWVyLW9uLWRpZXRze1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6ICM1YTVjNWNjOTtcbiAgei1pbmRleDogMTExMTtcbn1cblxuLmxheWVyLW9uLWRpZXRzLXJhbmRvbS1sb2Nre1xuICBoZWlnaHQ6IDYwdmg7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHotaW5kZXg6IDExMTE7XG59XG5cbi5sYXllci1vbi1kaWV0cy1iYWNre1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICB6LWluZGV4OiAxMTEwO1xufVxuXG4udXBncmFkZS1zbGlkZXItbG9ja3tcbiAvLyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pbWcvTG9jay5zdmcnKTtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogYm90dG9tO1xuICBoZWlnaHQ6IDYwJTtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi51cGdyYWRlLXNsaWRlci1sb2NrLXRleHR7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgbGluZS1oZWlnaHQ6IDM7XG59XG5cbi51cGdyYWRlLXNsaWRlci1yYW5kb20tbG9jay10ZXh0e1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiAxLjJyZW07XG4gIGxpbmUtaGVpZ2h0OiAzO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMzYlO1xuICB3aWR0aDogMTAwJTtcbiAgbGVmdDogMDtcbiAgYm90dG9tOiAxMHB4O1xufVxuXG4uaW1wcm92ZXtcbiAgZm9udC1mYW1pbHk6ICRmb250O1xuICBmb250LXNpemU6IC44cmVtO1xuICBjb2xvcjojNzA3MDcwO1xufVxuXG4ucWEtZGF5e1xuXG4gIGlvbi1jYXJke1xuICAgIHdpZHRoOiA5MCU7XG4gICAgcGFkZGluZzogMTBweCAyMHB4O1xuICAgIC8vIG1hcmdpbjogMjBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtdGFibGU7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYigxMjkgMTMzIDEzMSAvIDEwJSkgIWltcG9ydGFudDtcbiAgfVxuICBcbn1cblxuLm5ld3MtZGF5e1xuICBpb24tY2FyZHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBwYWRkaW5nOiAxMHB4IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYigxMjkgMTMzIDEzMSAvIDEwJSkgIWltcG9ydGFudDtcbiAgICAvLyBtYXJnaW46IDIwcHg7XG4gICAgXG4gIH0gXG59XG5cbi5yZXN1bHQtbWFpbi1jb250YWluZXJ7XG4gIGJveC1zaGFkb3c6IDBweCA1cHggMTBweCAwcHggcmdiKDEyOSAxMzMgMTMxIC8gMTAlKSAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiAzMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5yZXN1bHRzLWNhcmRze1xuICBtYXJnaW46IDAgIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogMzBweCAhaW1wb3J0YW50O1xuICBib3gtc2hhZG93OiBub25lICFpbXBvcnRhbnQ7XG4gIGJhY2tncm91bmQtY29sb3I6I2ZmZiAhaW1wb3J0YW50O1xuICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYigxMjkgMTMzIDEzMSAvIDEwJSkgIWltcG9ydGFudDtcbiAgbWFyZ2luLXJpZ2h0OjFyZW07XG4gIGJvcmRlci1yYWRpdXM6MzBweCAhaW1wb3J0YW50O1xuICBwYWRkaW5nOjFyZW07XG59XG5cbi5nYXVnZS1jaGFydHtcbiAgaW9uLWNhcmR7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgcGFkZGluZzogMTBweCAyMHB4O1xuICAgIHBhZGRpbmctdG9wOiB1bnNldDtcbiAgICBwYWRkaW5nLWxlZnQ6IHVuc2V0O1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDVweCAxMHB4IDBweCByZ2IoMTI5IDEzMyAxMzEgLyAxMCUpICFpbXBvcnRhbnQ7XG4gICAgaGVpZ2h0OiAxMDBweDtcbiAgfSBcbn1cblxuLnJlY2lwZS1kYXl7XG4gIGlvbi1jYXJke1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogOTAlO1xuICAgIHBhZGRpbmc6IDEwcHggMjBweDtcbiAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICAgIGJveC1zaGFkb3c6IDBweCA1cHggMTBweCAwcHggcmdiKDEyOSAxMzMgMTMxIC8gMTAlKSAhaW1wb3J0YW50O1xuICAgIC8vIG1hcmdpbjogMjBweDtcbiAgICBcbiAgfSBcbn1cblxuLmxlc3MtdGhhbi0xMDAtZm9vZC1pdGVtLWNvbnRhaW5lcntcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBwYWRkaW5nOiAxMHB4IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYigxMjkgMTMzIDEzMSAvIDEwJSkgIWltcG9ydGFudDtcbiAgICAvLyBtYXJnaW46IDIwcHg7XG59XG5cblxuLmxlc3MtdGhhbi0xMDAtZm9vZC1pdGVtLWlubmVyLWNvbnRhaW5lcntcbiAgLy8gZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMTBweCAyMHB4O1xuICBib3JkZXItcmFkaXVzOiAzMHB4O1xuICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYigxMjkgMTMzIDEzMSAvIDEwJSkgIWltcG9ydGFudDtcbiAgLy8gbWFyZ2luOiAyMHB4O1xufVxuXG4ucGVyc29uYWxpc2Utbm93e1xuICBpb24tY2FyZHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBwYWRkaW5nOiAxMHB4IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYigxMjkgMTMzIDEzMSAvIDEwJSkgIWltcG9ydGFudDtcbiAgICAvLyBtYXJnaW46IDIwcHg7XG4gICAgXG4gIH0gXG59XG5cbi5wZXJzb25hbGlzZS1jb3ZpZHtcbiAgaW9uLWNhcmR7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYigxMjkgMTMzIDEzMSAvIDEwJSkgIWltcG9ydGFudDtcbiAgICAvLyBtYXJnaW46IDIwcHg7XG4gICAgXG4gIH0gXG59XG5cbi5vdGhlci1mb29kLWl0ZW17XG4gIGlvbi1jYXJke1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogOTAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDVweCAxMHB4IDBweCByZ2IoMTI5IDEzMyAxMzEgLyAxMCUpICFpbXBvcnRhbnQ7XG4gICAgLy8gbWFyZ2luOiAyMHB4O1xuICAgIFxuICB9IFxufVxuXG4ubGVzcy0xMDAtY2Fse1xuICBpb24tY2FyZHtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDkwJTtcbiAgICBwYWRkaW5nOiAxMHB4IDIwcHg7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYigxMjkgMTMzIDEzMSAvIDEwJSkgIWltcG9ydGFudDtcbiAgICAvLyBtYXJnaW46IDIwcHg7XG4gICAgXG4gIH0gXG59XG5cbi50aXRsZS10ZXh0e1xuICAvLyBwYWRkaW5nLXRvcDogMTBweDtcbiAgZm9udC1mYW1pbHk6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZFwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMC45NXJlbSAhaW1wb3J0YW50O1xuICBjb2xvcjogIzM4ODBmZiAhaW1wb3J0YW50O1xuICAvLyBwYWRkaW5nLWxlZnQ6IDAgIWltcG9ydGFudDtcbn1cblxuLmFjdGl2aXR5LXNlY3Rpb257XG4gIG1heC1oZWlnaHQ6IG5vbmU7XG4gIGJveC1zaGFkb3c6MHB4IDVweCAxMHB4IDBweCByZ2IoMTI5IDEzMyAxMzEgLyAxMCUpICFpbXBvcnRhbnQ7XG5cbiAgLnJvdyAuYWN0aXZpdHk6Zmlyc3QtY2hpbGR7XG4gICAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgI0VFRTtcbiAgfVxuXG4gIC5yb3cgLmFjdGl2aXR5e1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjRUVFO1xuICAgIGhlaWdodDogMTg5cHg7XG4gIH1cblxuICAvLyAucm93IC5hY3Rpdml0eTpsYXN0LWNoaWxke1xuICAvLyAgIGJvcmRlci1sZWZ0OiAxcHggc29saWQgI0VFRTtcbiAgLy8gfVxuICAgIFxuICAgIG1hcmdpbi1sZWZ0OiBhdXRvO1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRUU7XG4gICAgYm9yZGVyLXJhZGl1czogMzVweDtcbiAgICBtYXJnaW46IDE2cHg7XG4gICAgd2lkdGg6IGF1dG87XG5cblxuICAgIC5oZWFkZXJ7XG4gICAgICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICAgICAgcGFkZGluZzogMTVweCAxNXB4IDEwcHg7XG4gICAgICBjb2xvcjogIzczNzc3NztcbiAgICB9XG5cbiAgICAucHJlbWl1bXtcbiAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgIGxlZnQ6IDFyZW07XG4gICAgICB0b3A6IDNweDtcbiAgICAgIGhlaWdodDogODAlO1xuICAgICAgd2lkdGg6IDgwJTtcbiAgICB9XG5cbiAgICAudHJhY2tlci1wcmVtaXVte1xuICAgICAgaGVpZ2h0OiAzdmg7XG4gICAgICB3aWR0aDogNHZoO1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgdG9wOiA4MCU7XG4gICAgICBsZWZ0OiA4MCU7XG4gICAgfVxuXG4gICAgLy8gLmhlYWRlci13YXRlcntcbiAgICAvLyAgIGNvbG9yOiAjMzU4MEYyO1xuICAgIC8vIH1cblxuICAgIC8vIC5hY3Rpdml0eS1oZWFkZXJ7XG4gICAgLy8gICBjb2xvcjogIzE0Q0VCNztcbiAgICAvLyB9XG5cbiAgICAvLyAuZGVmaWNpdC1oZWFkZXJ7XG4gICAgLy8gICBjb2xvcjogIzUwQzg3ODtcbiAgICAvLyB9XG5cbiAgICAvLyAuaGVhZGVyLWtleW1hbnRyYXtcbiAgICAvLyAgIGNvbG9yOiAjM0I3QTU3O1xuICAgIC8vIH1cblxuICAgIC5hY3Rpdml0eS1jaXJjbGUtcHJvZ3Jlc3N7XG4gICAgICAgICAgcGFkZGluZzogMHB4IDI1cHggMTBweCAyNXB4O1xuICAgIH1cbn1cblxuLmRpc2NvdmVyLXNlY3Rpb24ge1xuICBtYXgtaGVpZ2h0OiBub25lO1xuICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYmEoMTI5LCAxMzMsIDEzMSwgMC4xKSAhaW1wb3J0YW50O1xuICBib3JkZXI6IDFweCBzb2xpZCAjRUVFO1xuICBib3JkZXItcmFkaXVzOiAzNXB4O1xuICBtYXJnaW46IDE2cHg7XG4gIHdpZHRoOiBhdXRvO1xuXG4gIGlvbi1yb3cuZGlzY292ZXItY29udGVudCB7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0VFRTtcbiAgICBib3JkZXItcmFkaXVzOiAyNXB4O1xuXG4gICAgLmxlZnQtY29udGVudCB7XG5cbiAgICAgIC5oZWFkZXIge1xuICAgICAgICBmb250LXNpemU6IDFyZW07XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgICAgIH1cblxuICAgICAgLmRpc2NvdmVyLWRlc2NyaXB0aW9uIHtcbiAgICAgICAgZm9udC1zaXplOiAwLjhyZW07XG4gICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICAgICAgfVxuXG4gICAgICBpb24tYnV0dG9uIHtcbiAgICAgICAgbWF4LWhlaWdodDogMS41cmVtO1xuICAgICAgICAtLWJhY2tncm91bmQ6ICMwM2EzYTQ7XG4gICAgICAgIC0tY29sb3I6IHdoaXRlO1xuICAgICAgICBmb250LXNpemU6IDAuODByZW07XG4gICAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgICAgfVxuICAgIH1cblxuICAgIC5yaWdodC1jb250ZW50IHtcbiAgICAgIGltZyB7XG4gICAgICAgIC8vIHRyYW5zZm9ybTogc2NhbGUoMS4xNSk7XG4gICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcblxuICAgICAgICBtYXgtaGVpZ2h0OiAxMDBweDtcbiAgICAgICAgbWF4LXdpZHRoOiAxMDBweDtcbiAgICAgIH1cblxuICAgICAgLm5vLW1heC1zaXplIHtcbiAgICAgICAgbWF4LWhlaWdodDogMTAwJTtcbiAgICAgICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgICAgfVxuICAgIH1cbiAgfVxufVxuXG4uZGlldC1wbGFuLXNrZWxldG9ue1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDUlO1xuICBtYXJnaW4tdG9wOiAtNXB4O1xufVxuXG4uYWN0aXZpdHktc2tlbGV0b257XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5cbi5hY3Rpdml0eS1za2VsZXRvbi1yb3d7XG4gIGJvcmRlcjogMXB4IHNvbGlkICM3ZDdkN2Q0YTtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4ucWEtZGF5LXNrZWxldG9ue1xuICBtYXJnaW4tdG9wOiAwO1xuICBwYWRkaW5nOiAwO1xuICBoZWlnaHQ6IDMwMHB4O1xuICB3aWR0aDogOTAlO1xuICBkaXNwbGF5OiBpbmxpbmUtdGFibGU7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGJveC1zaGFkb3c6IDBweCA1cHggMTBweCAwcHggcmdiKDEyOSAxMzMgMTMxIC8gMTAlKSAhaW1wb3J0YW50O1xufVxuXG4ubmV3cy1kYXktc2tlbGV0b257XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmc6IDA7XG4gIGhlaWdodDogMzAwcHg7XG4gIHdpZHRoOiA5MCU7XG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYm94LXNoYWRvdzogMHB4IDVweCAxMHB4IDBweCByZ2IoMTI5IDEzMyAxMzEgLyAxMCUpICFpbXBvcnRhbnQ7XG59XG5cbi5yZWNpcGUtZGF5LXNrZWxldG9ue1xuICBtYXJnaW4tdG9wOiAwO1xuICBwYWRkaW5nOiAwO1xuICBoZWlnaHQ6IDMwMHB4O1xuICB3aWR0aDogOTAlO1xuICBkaXNwbGF5OiBpbmxpbmUtdGFibGU7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGJveC1zaGFkb3c6IDBweCA1cHggMTBweCAwcHggcmdiKDEyOSAxMzMgMTMxIC8gMTAlKSAhaW1wb3J0YW50O1xufVxuXG4ucGVyc29uYWxpc2Utbm93LXNrZWxldG9ue1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBwYWRkaW5nOiAwO1xuICBoZWlnaHQ6IDEwMHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHdpZHRoOiA5MCU7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIGJveC1zaGFkb3c6IDBweCA1cHggMTBweCAwcHggcmdiKDEyOSAxMzMgMTMxIC8gMTAlKSAhaW1wb3J0YW50O1xufVxuXG5pb24tcmVmcmVzaGVyLWNvbnRlbnQge1xuICAvLyBwYWRkaW5nLWJvdHRvbTogMjAwcHg7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gIC0tY29sb3I6ICM3ZDdkN2Q7XG4gIGNvbG9yOiAjN2Q3ZDdkICFpbXBvcnRhbnQ7XG5cbiAgLy8gLnJlZnJlc2hlci1wdWxsaW5nIHtcbiAgLy8gICBwb3NpdGlvbjogZml4ZWQ7XG4gIC8vICAgdG9wOiA1MHB4O1xuICAvLyB9XG59XG5cbi5nZXQtYnV0dG9uLWljb257XG4gIGNvbG9yOiAjN2Q3ZDdkO1xuICAgIC8qIGJhY2tncm91bmQtY29sb3I6ICNmZmY7ICovXG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xuICBsZWZ0OiAxLjJyZW07XG4gIHRvcDogMC43cmVtO1xuICBoZWlnaHQ6IDI1cHg7XG4gIHdpZHRoOiAyNXB4O1xuICBsZWZ0OiA4NSU7XG4gIHotaW5kZXg6IDI7XG59XG5cbi5nZXQtcmVtb3ZlLWljb257XG4gIGNvbG9yOiAjMDFBM0E0O1xuICAgIC8qIGJhY2tncm91bmQtY29sb3I6ICNmZmY7ICovXG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICAvLyBib3JkZXI6IDFweCBzb2xpZDtcbiAgaGVpZ2h0OiAyMHB4O1xuICB3aWR0aDogMjBweDtcbiAgei1pbmRleDogMjtcbiAgbWFyZ2luLWxlZnQ6IDVweDtcbiAgbWFyZ2luLXRvcDogNXB4O1xufVxuXG46Om5nLWRlZXAgLmdldC1yZW1vdmUtaWNvbi1tc2d7XG4gIGNvbG9yOiAjMDFBM0E0O1xuICBib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgaGVpZ2h0OiAxNXB4O1xuICB3aWR0aDogMTVweDtcbn1cblxuLmFkZC1mb29kLWljb257XG4gIGNvbG9yOiAjMDNhM2E0O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8vIGxlZnQ6IDEuMnJlbTtcbiAgdG9wOiAxcmVtO1xuICBoZWlnaHQ6IDJyZW07XG4gIHdpZHRoOiAycmVtO1xuICBsZWZ0OiA4MCU7XG4gIHotaW5kZXg6IDI7XG59XG5cbi5hZGQtZm9vZC10aXRsZXtcbiAgcGFkZGluZy10b3A6IDEwcHg7XG4gIGZvbnQtZmFtaWx5OiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtYm9sZFwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMC44cmVtICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50OyAgXG59XG5cbi5yZXBsYWNlLWZvb2QtaWNvbntcbiAgY29sb3I6ICMwM2EzYTQ7XG4gIC8qIGJvcmRlci1yYWRpdXM6IDEwMHB4OyAqL1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8qIGJvcmRlcjogMXB4IHNvbGlkOyAqL1xuICBsZWZ0OiAxLjJyZW07XG4gIHRvcDogOHJlbTtcbiAgaGVpZ2h0OiAycmVtO1xuICB3aWR0aDogMnJlbTtcbiAgbGVmdDogODAlO1xuICB6LWluZGV4OiAyO1xufVxuXG4uY29udGVudC1zaXple1xuICBmb250LXNpemU6IC43NXJlbTtcbiAgZm9udC1mYW1pbHk6ICRmb250O1xufVxuLmRldGFpbHtcbiAgYm9yZGVyLXJpZ2h0OiAxcHggc29saWQgIzczNzc3NzRhO1xuICBzcGFue1xuICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgfVxufVxuLnBhZGRpbmctbGVmdC01e1xuICAgcGFkZGluZy1sZWZ0OiA1cHg7XG59XG4ucGFkZGluZy1yaWdodC01e1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG59XG4uZGFya2dyZWVuLCAubGVnZW5kLTl7XG4gIGJvcmRlci1jb2xvcjogIzM4QTUzNDtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM4QTUzNDtcbn1cblxuLmxpZ2h0LWdyZWVuLCAubGVnZW5kLTZ7XG4gIGJvcmRlci1jb2xvcjogIzk0RUEwQTtcbiAgYmFja2dyb3VuZC1jb2xvcjojOTRFQTBBO1xufVxuXG4ueWVsbG93LCAubGVnZW5kLTN7XG4gIGJvcmRlci1jb2xvcjogI0VBREMxODtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0VBREMxODtcbn1cblxuLnllbGxvdy1vcmFuZ2UsIC5sZWdlbmQtMXtcbiAgYm9yZGVyLWNvbG9yOiAjZmZhNTAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZhNTAwO1xufVxuXG4uZ3JleSwgLmxlZ2VuZC0we1xuICBib3JkZXItY29sb3I6ICM3ZDdkN2Q1ZTtcbn1cblxuLmxlZ2VuZHtcbiAgYm9yZGVyLXdpZHRoOiAxcHg7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGhlaWdodDogNTZweDtcbiAgd2lkdGg6IDJweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5xYS1kYXl7XG4gIGZvbnQtZmFtaWx5OiAkZm9udC1ib2xkO1xuICBmb250LXNpemU6IDFyZW07XG4gIGNvbG9yOiMwMUEzQTQ7XG59XG5cbi50b2RvLWhlYWRlcntcbiAgZm9udC1mYW1pbHk6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZFwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XG59XG5cbi5zbG90LXNlbGVjdGlvbi1jYXJkIHtcbiAgZGlzcGxheTogZmxleDtcbiAgb3ZlcmZsb3cteDogc2Nyb2xsO1xuICBoZWlnaHQ6IDQwcHg7XG4gIC5zbG90LXNlbGVjdGlvbi1pdGVtIHtcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jayAhaW1wb3J0YW50O1xuICAgICAgbWluLXdpZHRoOiAxMDBweCAhaW1wb3J0YW50O1xuICAgICAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMwM2EzYTQ7XG4gICAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICAgICAgY29sb3I6ICMwM2EzYTQ7XG4gICAgICAtLWJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAtLWJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIH1cbn1cblxuLnNsb3Qtc2VsZWN0aW9uLWJ0bi1hY3RpdmV7XG4gIGNvbG9yOiAjZmZmICFpbXBvcnRhbnQ7XG4gIC0tYmFja2dyb3VuZDogIzAzYTNhNCAhaW1wb3J0YW50O1xufVxuXG46Om5nLWRlZXAgLmZvb2QtbGVzcy0xMDB7XG4gICBpb24tY2FyZCwgaW9uLXNsaWRle1xuICAgICBtYXJnaW4tdG9wOiAwICFpbXBvcnRhbnQ7XG4gICB9XG4gICAuc3dpcGVyLXBhZ2luYXRpb257XG4gICAgcGFkZGluZy10b3A6IDAgIWltcG9ydGFudDtcbiAgICBtYXJnaW4tdG9wOiAtMTBweCAhaW1wb3J0YW50O1xuICAgfVxufVxuXG4uZGFya2dyZWVuLCAubGVnZW5kLTktbG93ZXJ7XG4gIGJvcmRlci1jb2xvcjogIzM4QTUzNDtcbiAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzM4QTUzNDtcbn1cblxuLmxpZ2h0LWdyZWVuLCAubGVnZW5kLTYtbG93ZXJ7XG4gIGJvcmRlci1jb2xvcjogIzk0RUEwQTtcbiAgLy8gYmFja2dyb3VuZC1jb2xvcjogIzk0RUEwQTtcbn1cblxuLnllbGxvdywgLmxlZ2VuZC0zLWxvd2Vye1xuICBib3JkZXItY29sb3I6ICNFQURDMTg7XG4gIC8vIGJhY2tncm91bmQtY29sb3I6ICNFQURDMTg7XG59XG5cbi55ZWxsb3ctb3JhbmdlLCAubGVnZW5kLTEtbG93ZXJ7XG4gIGJvcmRlci1jb2xvcjogI2ZmYTUwMDtcbiAgLy8gYmFja2dyb3VuZC1jb2xvcjogI0VBREMxODtcbn1cblxuLmdyZXksIC5sZWdlbmQtMC1sb3dlcntcbiAgYm9yZGVyLWNvbG9yOiAjN2Q3ZDdkNWU7XG59XG5cbi5sZWdlbmQtbG93ZXJ7XG4gIGJvcmRlci1sZWZ0LXdpZHRoOiA1cHg7XG4gIGJvcmRlci1sZWZ0LXN0eWxlOiBzb2xpZDtcbiAgLy8gaGVpZ2h0OiA1NnB4O1xuICAvLyB3aWR0aDogMnB4O1xuICAvLyBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLnN1Yi1mb29kSXRlbS1oZWFkZXJ7XG4gIGZvbnQtc2l6ZTogMS4ycmVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM0QzUyNjQ7XG59XG5cbi5zdWItZm9vZEl0ZW0tc3ViLWhlYWRlcntcbiAgZm9udC1zaXplOiAwLjc1cmVtO1xufVxuXG4uc3ViLWZvb2RJdGVtLWJ0bntcbiAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAzYTNhNDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgY29sb3I6ICNmZmY7XG4gIC0tYmFja2dyb3VuZDogIzAzYTNhNDtcbiAgLS1ib3JkZXItcmFkaXVzOiAyMHB4O1xuICBtaW4td2lkdGg6IDgwcHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXRvcDogMTVweDtcbn1cblxuLnN1Yi1mYXN0aW5nLWJ0bntcbiAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAzYTNhNDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgY29sb3I6ICNmZmY7XG4gIC0tYmFja2dyb3VuZDogIzAzYTNhNDtcbiAgLS1ib3JkZXItcmFkaXVzOiAyMHB4O1xuICBtaW4td2lkdGg6IDgwcHggIWltcG9ydGFudDtcbn1cbi5kaXNjb3Zlci1zZWN0aW9uIHtcbiAgbWF4LWhlaWdodDogbm9uZTtcbiAgYm94LXNoYWRvdzogMHB4IDVweCAxMHB4IDBweCByZ2JhKDEyOSwgMTMzLCAxMzEsIDAuMSkgIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWQgI0VFRTtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgbWFyZ2luOiAxNnB4O1xuICB3aWR0aDogYXV0bztcblxuICBpb24tcm93LmRpc2NvdmVyLWNvbnRlbnQge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFRUU7XG4gICAgYm9yZGVyLXJhZGl1czogMjVweDtcblxuICAgIC5sZWZ0LWNvbnRlbnQge1xuXG4gICAgICAuaGVhZGVyIHtcbiAgICAgICAgZm9udC1zaXplOiAxcmVtO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgICAgcGFkZGluZy1ib3R0b206IDEwcHg7XG4gICAgICB9XG5cbiAgICAgIC5kaXNjb3Zlci1kZXNjcmlwdGlvbiB7XG4gICAgICAgIGZvbnQtc2l6ZTogMC44cmVtO1xuICAgICAgICBwYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgICAgIH1cblxuICAgICAgaW9uLWJ1dHRvbiB7XG4gICAgICAgIG1heC1oZWlnaHQ6IDEuNXJlbTtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiAjMDNhM2E0O1xuICAgICAgICAtLWNvbG9yOiB3aGl0ZTtcbiAgICAgICAgZm9udC1zaXplOiAwLjgwcmVtO1xuICAgICAgICBmb250LXdlaWdodDogYm9sZDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAuZGlldC1wbGFue1xuICAgICAgaW1ne1xuICAgICAgICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgICAgIH1cbiAgICB9XG4gICAgXG4gICAgLnJpZ2h0LWNvbnRlbnQge1xuICAgICAgaW1nIHtcbiAgICAgICAgLy8gdHJhbnNmb3JtOiBzY2FsZSgxLjE1KTtcbiAgICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuXG4gICAgICAgIG1heC1oZWlnaHQ6IDEwMHB4O1xuICAgICAgICBtYXgtd2lkdGg6IDEwMHB4O1xuICAgICAgfVxuXG4gICAgICAubm8tbWF4LXNpemUge1xuICAgICAgICBtYXgtaGVpZ2h0OiAxMDAlO1xuICAgICAgICBtYXgtd2lkdGg6IDEwMCU7XG4gICAgICB9IFxuICAgIH1cbiAgfSBcbn1cblxuLmltbXVuaXR5LWNvbnRhaW5lcntcbiAvLyBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pbWcvaW1tdW5pdHkuanBlZycpO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtcG9zaXRpb246IHJpZ2h0O1xufVxuXG4ucWEtZGF5e1xuICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBjb2xvcjojMDFBM0E0O1xufVxuLnFhLWRheS1zZWV7XG4gIGZvbnQtZmFtaWx5OiAkZm9udC1tZWRpdW07XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgY29sb3I6IzYwQUZGNTtcbn1cbi5kaXNwbGF5LWNvbnRlbnR7XG4gIGRpc3BsYXk6IGNvbnRlbnRzO1xufVxuLmRlc2NyaXB0aW9uMXtcbiAgcGFkZGluZy10b3A6MTBweDtcbiAgcHtcbiAgICAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgICAgIHRleHQtYWxpZ246IHN0YXJ0O1xuICAgICAgdGV4dC1qdXN0aWZ5OiBhdXRvO1xuICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICBjb2xvcjojNEM1MjY0ICFpbXBvcnRhbnQ7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIH1cbn1cbjo6bmctZGVlcCAuZGVzY3tcbiAgZm9udC1mYW1pbHk6ICRmb250O1xuICBmb250LXNpemU6MC45cmVtO1xuICAvLyBwYWRkaW5nLWxlZnQ6IDVweDtcbiAgZGlzcGxheTogY29udGVudHM7XG4gIG1hcmdpbi10b3A6IDEwcHggIWltcG9ydGFudDtcblxuICBwe1xuICAgICAgbWFyZ2luLWJvdHRvbTogMCAhaW1wb3J0YW50O1xuICAgICAgZGlzcGxheTogY29udGVudHM7XG4gIH0gICAgXG59XG5cbjo6bmctZGVlcCAuZGlzcEJsb2Nre1xuICBtYXJnaW46IDAgIWltcG9ydGFudDtcbiAgcHtcbiAgICAgIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XG4gIH1cbn1cblxuLnBhZGRpbmctdG9wLTF7XG4gIHBhZGRpbmctdG9wOjEwcHg7XG59XG4ucWEtZGF5LWltZ3tcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbn1cbi52aWV3LW1vcmV7XG4gIHRleHQtYWxpZ246bGVmdDtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBmb250LWZhbWlseTogJGZvbnQ7XG4gIGNvbG9yOiM2MEFGRjUgIWltcG9ydGFudDtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xufVxuXG4ucWEtdGl0bGV7XG4gIC8vIHBhZGRpbmctYm90dG9tOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgY29sb3I6ICM0QzUyNjQ7XG4gIGZvbnQtc2l6ZTogMXJlbTtcbiAgZm9udC1mYW1pbHk6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1yZWd1bGFyXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4gIG1hcmdpbjogMDtcbiAgd2hpdGUtc3BhY2U6IHByZS1saW5lO1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIC8vIGNvbG9yOiAjMDFBM0E0O1xufVxuXG4ucWEtZGF5LWltZ3tcbiAgdGV4dC1hbGlnbjpjZW50ZXI7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gIGhlaWdodDoyMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgbWF4LWhlaWdodDogMjAwcHg7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4udHdvLWxpbmVze1xuICBkaXNwbGF5OiBibG9jaztcbiAgZGlzcGxheTogLXdlYmtpdC1ib3g7XG4gIG1heC13aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAzNXB4O1xuICBtYXJnaW46IDAgYXV0bztcbiAgZm9udC1zaXplOiAxNHB4O1xuICAvLyBsaW5lLWhlaWdodDogMjtcbiAgLXdlYmtpdC1saW5lLWNsYW1wOiAyO1xuICAtd2Via2l0LWJveC1vcmllbnQ6IHZlcnRpY2FsO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbn1cblxuOjpuZy1kZWVwIC50d28tbGluZXMtbm90aS1tc2d7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBkaXNwbGF5OiAtd2Via2l0LWJveDtcbiAgbWF4LXdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDM4cHg7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBmb250LXNpemU6IDE0cHg7XG4gIC8vIGxpbmUtaGVpZ2h0OiAyO1xuICAtd2Via2l0LWxpbmUtY2xhbXA6IDI7XG4gIC13ZWJraXQtYm94LW9yaWVudDogdmVydGljYWw7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIHRleHQtb3ZlcmZsb3c6IGVsbGlwc2lzO1xufVxuXG4uaGVpZ2h0MjR7XG4gIGhlaWdodDogMjRweCAhaW1wb3J0YW50O1xufVxuXG46Om5nLWRlZXAgLnNpbmdsZS1saW5lLW5vdGktbXNnLWNlbnRlcntcbiAgLy8gZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cblxuLmltbXVuaXR5LXNlY3Rpb257XG4gIC0tYmFja2dyb3VuZDogI0ZEOUYzMyAhaW1wb3J0YW50O1xuICBjb2xvcjogICNGRDlGMzMgIWltcG9ydGFudDtcbn1cblxuLmRldG94LXNlY3Rpb257XG4gIC0tYmFja2dyb3VuZDogIzRDQjI3MSAhaW1wb3J0YW50O1xuICBjb2xvcjogICM0Q0IyNzEgIWltcG9ydGFudDtcbn1cblxuLndlaWdodC1sb3NzLXBsdXMtc2VjdGlvbntcbiAgLS1iYWNrZ3JvdW5kOiAjMEI5NEMxICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAgIzBCOTRDMSAhaW1wb3J0YW50O1xufVxuXG4ucG9zdC1jb3ZpZC1zZWN0aW9ue1xuICAtLWJhY2tncm91bmQ6ICM3NTRCMjkgIWltcG9ydGFudDtcbiAgY29sb3I6ICAjNzU0QjI5ICFpbXBvcnRhbnQ7XG59XG5cbi5jaG9sZXN0ZXJvbC1zZWN0aW9ue1xuICAtLWJhY2tncm91bmQ6ICNBMzFFNzkgIWltcG9ydGFudDtcbiAgY29sb3I6ICAjQTMxRTc5ICFpbXBvcnRhbnQ7XG59XG5cbi5kaWFiZXRlcy1zZWN0aW9ue1xuICAtLWJhY2tncm91bmQ6ICNEMTQzMjIgIWltcG9ydGFudDtcbiAgY29sb3I6ICAjRDE0MzIyICFpbXBvcnRhbnQ7XG59XG5cbi5oeXBlcnRlbnNpb24tc2VjdGlvbntcbiAgLS1iYWNrZ3JvdW5kOiAjRkY1RDU2ICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAgI0ZGNUQ1NiAhaW1wb3J0YW50O1xufVxuXG4ubXVjc2xlIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMEI5NEMxICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAgIzBCOTRDMSAhaW1wb3J0YW50O1xufVxuXG4uZmF0IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRkQ5ODBGICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAgI0ZEOTgwRiAhaW1wb3J0YW50O1xufVxuXG4ucGNvcy1zZWN0aW9ue1xuICAtLWJhY2tncm91bmQ6ICNGRjVBN0QgIWltcG9ydGFudDtcbiAgY29sb3I6ICAjRkY1QTdEICFpbXBvcnRhbnQ7XG59XG5cbi5oZWFkZXItbWVzc2FnZS10b29se1xuICBjb2xvcjogIzMzMztcbiAgZm9udC1zaXplOiAwLjhyZW07XG59XG5cbi5jaG9sZXN0ZXJvbC1zZWN0aW9uLXN0cm9rZXtcbiAgLS1iYWNrZ3JvdW5kOiAjQTMxRTc5ICFpbXBvcnRhbnQ7XG59XG5cbi5kaWFiZXRlcy1zZWN0aW9uLXN0cm9rZXtcbiAgLS1iYWNrZ3JvdW5kOiAjRDE0MzIyICFpbXBvcnRhbnQ7XG59XG5cbi5oeXBlcnRlbnNpb24tc2VjdGlvbi1zdHJva2V7XG4gIC0tYmFja2dyb3VuZDogI0ZGNUQ1NiAhaW1wb3J0YW50O1xufVxuXG4ucGNvcy1zZWN0aW9uLXN0cm9rZXtcbiAgLS1iYWNrZ3JvdW5kOiAjRkY1QTdEICFpbXBvcnRhbnQ7XG59XG5cbi5tdWNzbGUtc3Ryb2tlIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMEI5NEMxICFpbXBvcnRhbnQ7XG59XG5cbi5mYXQtc3Ryb2tlIHtcbiAgLS1iYWNrZ3JvdW5kOiAjRkQ5ODBGICFpbXBvcnRhbnQ7XG59XG5cbi53ZWlnaHQtbG9zc3tcbiAgLS1iYWNrZ3JvdW5kOiAjMDFBM0E0ICFpbXBvcnRhbnQ7XG59XG5cbi5pbW11bml0eS1zZWN0aW9uLXN0cm9rZXtcbiAgLS1iYWNrZ3JvdW5kOiAjRkQ5RjMzICFpbXBvcnRhbnQ7XG59XG5cbi5kZXRveC1zZWN0aW9uLXN0cm9rZXtcbiAgc3Ryb2tlOiAjNENCMjcxICFpbXBvcnRhbnQ7XG59XG5cbi53ZWlnaHQtbG9zcy1wbHVzLXNlY3Rpb24tc3Ryb2tle1xuICBzdHJva2U6ICMwQjk0QzEgIWltcG9ydGFudDtcbn1cblxuLndlaWdodC1sb3NzLXN0cm9rZXtcbiAgc3Ryb2tlOiAjNzJCRENBICFpbXBvcnRhbnQ7XG59XG5cbi5kZXRveC1zdHJva2V7XG4gIHN0cm9rZTogIzRjYjI3MSAhaW1wb3J0YW50O1xufVxuXG4ubm90aWZpY2F0aW9uLWJvcmRlcntcbiAgYm9yZGVyOjJweCBzb2xpZCAjMDFBM0E0ICFpbXBvcnRhbnQ7XG59XG5cbi5jaG9sZXN0ZXJvbC1zZWN0aW9uLWJvcmRlcntcbiAgYm9yZGVyOiAycHggc29saWQgI0EzMUU3OSAhaW1wb3J0YW50O1xufVxuXG4uZGlhYmV0ZXMtc2VjdGlvbi1ib3JkZXJ7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNEMTQzMjIgIWltcG9ydGFudDtcbn1cblxuLmh5cGVydGVuc2lvbi1zZWN0aW9uLWJvcmRlcntcbiAgYm9yZGVyOiAycHggc29saWQgI0ZGNUQ1NiAhaW1wb3J0YW50O1xufVxuXG4ucGNvcy1zZWN0aW9uLWJvcmRlcntcbiAgYm9yZGVyOiAycHggc29saWQgI0ZGNUE3RCAhaW1wb3J0YW50O1xufVxuXG4ubXVjc2xlLWJvcmRlciB7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMwQjk0QzEgIWltcG9ydGFudDtcbn1cblxuLmZhdC1ib3JkZXIge1xuICBib3JkZXI6IDJweCBzb2xpZCAjRkQ5ODBGICFpbXBvcnRhbnQ7XG59XG5cbi53ZWlnaHQtbG9zcy1ib3JkZXJ7XG4gIGJvcmRlcjogMnB4IHNvbGlkICMwMUEzQTQgIWltcG9ydGFudDtcbn1cblxuLmltbXVuaXR5LXNlY3Rpb24tYm9yZGVye1xuICBib3JkZXI6IDJweCBzb2xpZCAjRkQ5RjMzICFpbXBvcnRhbnQ7XG59XG5cbi5kZXRveC1zZWN0aW9uLWJvcmRlcntcbiAgYm9yZGVyOjJweCBzb2xpZCAjNENCMjcxICFpbXBvcnRhbnQ7XG59XG5cbi53ZWlnaHQtbG9zcy1wbHVzLXNlY3Rpb24tYm9yZGVye1xuICBib3JkZXI6MnB4IHNvbGlkICMwQjk0QzEgIWltcG9ydGFudDtcbn1cblxuLnBvc3QtY292aWQtc2VjdGlvbi1ib3JkZXJ7XG4gIGJvcmRlcjoycHggc29saWQgIzc1NEIyOSAhaW1wb3J0YW50O1xufVxuXG4ud2VpZ2h0LWxvc3MtYm9yZGVye1xuICBib3JkZXI6MnB4IHNvbGlkICM3MkJEQ0EgIWltcG9ydGFudDtcbn1cblxuLmRldG94LWJvcmRlcntcbiAgYm9yZGVyOjJweCBzb2xpZCAjNGNiMjcxICFpbXBvcnRhbnQ7XG59XG4ucGVyc29uYWxpc2UtY2FyZC1pbWd7XG4gIGhlaWdodDogMTUwcHg7XG59XG5cbi5jZW50ZXItY29udGVudHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDUwJTtcbiAgbGVmdDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKSAhaW1wb3J0YW50O1xuICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xufVxuXG5cbi50b2dnbGVzd2l0Y2gge1xuICBpb24tY2FyZCB7XG4gICAgYm94LXNoYWRvdzogMHB4IDVweCAxMHB4IDBweCByZ2IoMTI5IDEzMyAxMzEgLyAxMCUpICFpbXBvcnRhbnQ7XG4gICAgYm9yZGVyOiAxcHggc29saWQgI0VFRTtcbiAgICBib3JkZXItcmFkaXVzOiAzNXB4O1xuICAgIGhlaWdodDogMzJweDtcblxuICAgIGRpdiB7XG4gICAgICBjb2xvcjogd2hpdGU7XG4gICAgICBib3JkZXItcmFkaXVzOiAzNXB4O1xuICAgICAgaGVpZ2h0OiAzMHB4O1xuICAgICAgd2lkdGg6IDUwJTtcbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgIGxpbmUtaGVpZ2h0OiAyO1xuXG4gICAgICBpb24taWNvbixcbiAgICAgIHNwYW4ge1xuICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgLmFjdGl2ZSB7XG4gICAgICAvLyBiYWNrZ3JvdW5kOiAjMDFBM0E0O1xuICAgICAgYm94LXNoYWRvdzogMHB4IDVweCAxMHB4IDBweCByZ2IoMTI5IDEzMyAxMzEgLyAxMCUpICFpbXBvcnRhbnQ7XG4gICAgfVxuXG4gICAgLmRlYWN0aXZlIHtcbiAgICAgIC8vIGJhY2tncm91bmQ6IHdoaXRlO1xuICAgICAgYm94LXNoYWRvdzogdW5zZXQ7XG4gICAgfVxuICB9XG59XG5cbi52aWRlby1pZnJhbWV7XG4gIHRyYW5zZm9ybTogcm90YXRlKC05MGRlZyk7XG4gIHRyYW5zZm9ybS1vcmlnaW46IGxlZnQgdG9wO1xuICB3aWR0aDogMTAwdmg7XG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEwMCU7XG4gIGxlZnQ6IDA7XG4gIGhlaWdodDogNTB2aDtcbn1cblxuLmdldC1idXR0b24taWNvbi1wb3RyYWl0e1xuICBjb2xvcjogI2ZmZjtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAxYTNhNDtcbiAgYm9yZGVyOiAycHggc29saWQgI2ZmZjtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDEuMnJlbTtcbiAgdG9wOiAzcmVtO1xuICBoZWlnaHQ6IDM1cHg7XG4gIHdpZHRoOiAzNXB4O1xufVxuXG4uZ2V0LWJ1dHRvbi1pY29uLWxhbmRzY2FwZXtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMWEzYTQ7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxLjJyZW07XG4gIHRvcDogMnJlbTtcbiAgaGVpZ2h0OiAzNXB4O1xuICB3aWR0aDogMzVweDtcbiAgbGVmdDogMjAlXG59XG5cbi5waWMtdG9kb3tcbiAgaGVpZ2h0OiA1MHB4ICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiA1MHB4ICFpbXBvcnRhbnQ7XG59XG5cbi50b2RvLWljb257XG4gIG1hcmdpbi1sZWZ0OiAwO1xuICBtYXJnaW4tcmlnaHQ6IC0xMHB4O1xuICAtLWJhY2tncm91bmQtY2hlY2tlZDojMDFBM0E0O1xuICAtLWJvcmRlci1jb2xvci1jaGVja2VkOiMwMUEzQTQ7XG59XG5cbi5jdXN0b20tdG9kby1oZWlnaHQtc2Nyb2xsIHtcbiAgLy8gbWF4LWhlaWdodDogMTcwcHg7XG4gIC8vIG1heC1oZWlnaHQ6IDIwMHB4O1xuICAvLyBvdmVyZmxvdy15OiBzY3JvbGw7XG5cbiAgaW9uLWl0ZW0ge1xuICAgIG1hcmdpbi10b3A6IC0wLjJyZW07XG4gIH1cblxuICAud3AtcHJvZy1sYWJlbCB7XG4gICAgY29sb3I6ICM1RDVENUQ7XG4gICAgZm9udC1zaXplOiAwLjlyZW0gIWltcG9ydGFudDtcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIGxlZnQ6MDtcbiAgICBwYWRkaW5nLWJvdHRvbTogMXJlbTtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXZ7XG4gICAgICBmb250LXNpemU6IDFyZW07XG4gICAgICBcbiAgICB9XG4gICBcbiAgfVxufVxuXG4uYWN0aXZlLWVhdGVue1xuICAtLWJhY2tncm91bmQ6ICMwMUEzQTQgIWltcG9ydGFudDtcbiAgY29sb3I6ICNmZmYgIWltcG9ydGFudDtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAxQTNBNCAhaW1wb3J0YW50O1xufVxuXG4uZm9vZC1hY3Rpb24tYnRue1xuICAtLWJhY2tncm91bmQ6ICNmZmY7XG4gIGZvbnQtc2l6ZTogMC42cmVtO1xuICBib3JkZXI6IDFweCBzb2xpZCAjOGM4RTkzO1xuICBib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgY29sb3I6ICM4YzhFOTM7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4uZm9vZC1hY3Rpb24tYnRuLWNvbnRhaW5lcntcbiAgZGlzcGxheTogZmxleDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtYXJnaW4tdG9wOiAtMjBweDtcbiAgbWFyZ2luLWxlZnQ6IDMwJTtcbiAgb3ZlcmZsb3cteDogc2Nyb2xsO1xufVxuXG5pb24taXRlbSB7XG4gIC0taGlnaGxpZ2h0LWNvbG9yLWZvY3VzZWQ6IG5vbmUgIWltcG9ydGFudDtcbiAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6IG5vbmUgIWltcG9ydGFudDtcbiAgLS1oaWdobGlnaHQtY29sb3ItaW52YWxpZDogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4uc3RhcnQtZmFzdGluZ3tcbiAgLS1iYWNrZ3JvdW5kOiAjMDFhM2E0O1xuICAtLWNvbG9yOiAjZmZmO1xuICAtLWJvcmRlci1jb2xvcjogIzAxYTNhNDtcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGhlaWdodDogMS41cmVtO1xuICBmb250LXNpemU6IDAuNzVyZW07XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdG9wOiAtOTBweDtcbiAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcbn1cblxuLmZhc3RpbmctY29udHJvbGxlcntcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiAtOTVweDtcblxuICAgIC5jb3VudC1kb3due1xuICAgICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICAgIH1cbiAgICBcbiAgICBpb24taWNvbntcbiAgICAgIGZvbnQtc2l6ZTogMS43NXJlbTtcbiAgICB9XG59XG5cbi5yZXN1bHQtbWFpbi1jb250YWluZXJ7XG4gIGJveC1zaGFkb3c6IDBweCA1cHggMTBweCAwcHggcmdiKDEyOSAxMzMgMTMxIC8gMTAlKSAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiAzMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5yZXN1bHQtdGl0bGV7XG4gIGZvbnQtc2l6ZTogMS4xcmVtICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjMDFBM0E0ICFpbXBvcnRhbnQ7XG59XG5cbi5hZ2UtY29udGFpbmVye1xuICAvLyBmb250LXNpemU6IDAuOHJlbTtcbiAgY29sb3I6ICM2NjY2NjY7XG59XG5cbi50cmFuc2Zvcm1hdGlvbntcbiAgaW9uLWNhcmR7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiA5NCU7XG4gICAgYm94LXNoYWRvdzogdW5zZXQgIWltcG9ydGFudDtcbiAgICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgICBtaW4taGVpZ2h0OiA0OTBweDtcbiAgICBtYXJnaW4tdG9wOiAwLjdyZW0gIWltcG9ydGFudDtcbiAgfVxufVxuXG4udHJhbnNmb3JtYXRpb24tY2FyZHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBtaW4taGVpZ2h0OnVuc2V0O1xuICBtYXgtaGVpZ2h0OiB1bnNldDtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbn1cblxuLnJlc3VsdC1jb250ZW50e1xuICBjb2xvcjogIzcwNzA3MDtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xuICBmb250LXNpemU6IDAuOXJlbTtcbiAgdGV4dC1hbGlnbjoganVzdGlmeTtcbn1cbi5pb24tcGFkZGluZy1ib3R0b20tMTB7XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuXG4uYm90dG9tLXNwYWNle1xuICBwYWRkaW5nLWJvdHRvbTogMTVweDtcbn1cblxuLmJvcmRlci1yYWRpb3VzMTV7XG4gIGJvcmRlci1yYWRpdXM6IDE1cHg7XG59XG4udGhlbWUtYm9yZGVye1xuICBib3JkZXI6IDFweCBzb2xpZCAjMDNhM2E0O1xufVxuXG4ub2ZmZXItdGV4dHtcbiAgaGVpZ2h0OiBhdXRvICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjRDVBNjIwO1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luLWxlZnQ6IDAgIWltcG9ydGFudDtcbn1cblxuLmdvbGQtYmFja2dyb3VuZHtcbiAgLS1iYWNrZ3JvdW5kOiAjRDVBNjIwO1xuICBib3JkZXI6IDNweCBzb2xpZCAjRDVBNjIwO1xufVxuXG4uZ29sZC1ib3JkZXJ7XG4gIGJvcmRlcjogM3B4IHNvbGlkICNENUE2MjA7XG59XG5cbi5mb250LXNpemU3NXtcbiAgZm9udC1zaXplOiAwLjc1cmVtICFpbXBvcnRhbnQ7XG59XG5cbi5mb250LXNpemUtMS0ye1xuICBmb250LXNpemU6IDEuMnJlbTtcbn1cblxuY2lyY2xlLXByb2dyZXNzIHN2Z3tcbiAgYm94LXNoYWRvdzogMnB4IDhweCAxMHB4ICMwMDA7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgd2lkdGg6IDE0MHB4O1xufVxuXG4uY3VydmV7XG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDUwJSAxNTBweCAhaW1wb3J0YW50O1xuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNTAlIDE1MHB4ICFpbXBvcnRhbnQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgd2lkdGg6IDE2MCU7XG4gIGxlZnQ6IC0zMCU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJlZDtcbiAgYmFja2dyb3VuZC1zaXplOiA0MiUgYXV0bztcbiAgaGVpZ2h0OiAzMDBweDtcbiAgdG9wOiAtNjNweDtcbiAgei1pbmRleDogMDtcbiAgYmFja2dyb3VuZDogd2hpdGU7XG4gIC8qIGJveC1zaGFkb3c6IDBweCAzM3B4IDE5cHggLTM1cHg7ICovXG59XG5cbi5ib3R0b20tYWN0aXZpdGllc3tcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IC0xMHB4O1xufVxuXG4ubGlnaHQtZ3JleXtcbiAgY29sb3I6ICM3ZDdkN2Q7XG59XG5cbi8vIC5hbmltYXRlZC1jaXJjbGV7XG4vLyAgIHRyYW5zZm9ybTogc2NhbGUoMC45LCAwLjkpO1xuLy8gfVxuXG4uaGVhcnQtYmVhdC1hbmltYXRpb24ge1xuICBhbmltYXRpb246IGFuaW1hdGVIZWFydCAycyBpbmZpbml0ZTtcbn1cblxuQGtleWZyYW1lcyBhbmltYXRlSGVhcnQge1xuICAwJSB7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwLjgpO1xuICB9XG4gIDUlIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOSk7XG4gIH1cbiAgMTAlIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOCk7XG4gIH1cbiAgMTUlIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDEpO1xuICB9XG4gIDUwJSB7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgwLjgpO1xuICB9XG4gIDEwMCUge1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMC44KTtcbiAgfVxufVxuXG4ubG93LWhlYXJ0LWJlYXQtYW5pbWF0aW9uIHtcbiAgYW5pbWF0aW9uOiBhbmltYXRlSGVhcnQgMnMgaW5maW5pdGU7XG59XG5cbkBrZXlmcmFtZXMgYW5pbWF0ZUhlYXJ0IHtcbiAgNTAlIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDAuOSk7XG4gIH1cbiAgMTAwJSB7XG4gICAgdHJhbnNmb3JtOiBzY2FsZSgxKTtcbiAgfVxufVxuXG4ub3ZlcmxheS1ib3R0b20tbXNne1xuICBtYXJnaW4tdG9wOi41cmVtO1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3R0b206IDU1cHg7XG4gIC8vIHBhZGRpbmctYm90dG9tOiA3JTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBiYWNrZ3JvdW5kOiAjRDA5RTAzO1xuICB6LWluZGV4OiA5O1xuICBoZWlnaHQ6IDV2aDtcbn1cblxuLmZvbnQtd2hpdGV7XG4gIGNvbG9yOiB3aGl0ZTtcbn1cblxuLmRvdWJsZS1hcnJvdyB7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBzaGFrZSBpbmZpbml0ZSAxcyBlYXNlLWluLW91dDtcbiAgYW5pbWF0aW9uOiBzaGFrZSAxcyBpbmZpbml0ZSBlYXNlLWluLW91dDtcbn1cbkAtd2Via2l0LWtleWZyYW1lcyBzaGFrZSB7XG4gIDAlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgwKTtcbiAgfVxuXG4gIC8vIDIwJSB7XG4gIC8vICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoLTEwcHgpO1xuICAvLyB9XG5cbiAgMjUlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCg1cHgpO1xuICB9XG5cbiAgNTAlIHtcbiAgICAtd2Via2l0LXRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNXB4KTtcbiAgfVxuXG4gIDc1JSB7XG4gICAgLXdlYmtpdC10cmFuc2Zvcm06IHRyYW5zbGF0ZVgoNXB4KTtcbiAgfVxuXG4gIDEwMCUge1xuICAgIC13ZWJraXQtdHJhbnNmb3JtOiB0cmFuc2xhdGVYKDApO1xuICB9XG59XG5Aa2V5ZnJhbWVzIHNoYWtlIHtcbiAgMCUge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgtNXB4KTtcbiAgfVxuXG4gIC8vIDIwJSB7XG4gIC8vICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01cHgpO1xuICAvLyB9XG5cbiAgNDAlIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoNXB4KTtcbiAgfVxuXG4gIDYwJSB7XG4gICAgdHJhbnNmb3JtOiB0cmFuc2xhdGVYKC01cHgpO1xuICB9XG5cbiAgODAlIHtcbiAgICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVgoNXB4KTtcbiAgfVxuXG4gIDEwMCUge1xuICAgIHRyYW5zZm9ybTogdHJhbnNsYXRlWCgwKTtcbiAgfVxufVxuLmxvYWRpbmctcHJvdGllbi10cmFja2Vye1xuICBoZWlnaHQ6IDYwcHg7XG4gIHdpZHRoOiA2MHB4O1xuICBib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgbWFyZ2luOiBhdXRvO1xufVxuXG4ucm90YXRlLTkwe1xuICB0cmFuc2Zvcm06IHJvdGF0ZSg5MGRlZyk7XG59XG5cbi8vIC5yZWQtZGVmaWNpZW50e1xuLy8gICBmb250LXNpemU6IDEycHg7XG4vLyAgIHdpZHRoOiA1MHB4O1xuLy8gICBwb3NpdGlvbjogYWJzb2x1dGU7XG4vLyAgIHRvcDogMTdweDtcbi8vICAgbGVmdDogMTBweDtcbi8vIH1cblxuLmRlZmlpY2llbnQge1xuICBmb250LXNpemU6IDEwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiA3OHB4O1xuICAvLyBsZWZ0OiAxMHB4O1xuICBmb250LXdlaWdodDogOTAwO1xuXG4gICYuZ3JlZW4tZGVmaWljaWVudHtcbiAgICBjb2xvcjogIzAwQjA1RTtcbiAgfVxuICBcbiAgJi5yZWQtZGVmaWNpZW50e1xuICAgIGNvbG9yOiAjQzUzRjNGO1xuICB9XG4gIFxuICAmLmJ1cm50LWRlZmljaWVudHtcbiAgICBjb2xvcjogIzQwN0JGRjtcbiAgfVxufVxuXG4uZGVmaWljaWVudC1kYXRhe1xuICBmb250LXNpemU6IDlweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEwcHg7XG4gIGxlZnQ6IDEwcHg7XG4gIGZvbnQtd2VpZ2h0OiA5MDA7XG59XG5cbi5kZWZpaWNpZW50LWRhdGEtYnVybnR7XG4gIGZvbnQtc2l6ZTogOXB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMTBweDtcbiAgbGVmdDogMThweDtcbiAgZm9udC13ZWlnaHQ6IDkwMDtcbn1cblxuLmRlZmlpY2llbnQtZGF0YS1sb3N0e1xuICBmb250LXNpemU6IDlweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEwcHg7XG4gIGxlZnQ6IDE4cHg7XG4gIGZvbnQtd2VpZ2h0OiA5MDA7XG59XG5cbi5kaXNhYmxlLXRyYWtlci1jb250YWluZXJ7XG4gIGhlaWdodDogNjVweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuXG4uY29uc3VtZWQtY29udGFpbmVye1xuLy8gIGJhY2tncm91bmQ6IHVybCgnLi4vLi4vLi4vYXNzZXRzL2ltZy9kZWZpY2lldC1ncmVlbi1hcnJvdy5wbmcnKTsgXG4gIGhlaWdodDogNzBweDtcbiAgd2lkdGg6IDg1cHg7XG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG59XG5cbi53YWxrLW1hbi1jb250YWluZXJ7XG4vLyAgYmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaW1nL2RlZmljaWV0LXdhbGstbWFuLnBuZycpO1xuICBoZWlnaHQ6IDkzcHg7XG4gIHdpZHRoOiA1MHB4O1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4uZGVmaWNpdC1jb250YWluZXJ7XG4gLy8gYmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaW1nL2RlZmljaWV0LWJyb3duLWFycm93LnBuZycpOyBcbiAgaGVpZ2h0OiA3MHB4O1xuICB3aWR0aDogODVweDtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbn1cblxuLmVxdWFsLWNvbnRhaW5lcntcbiAvLyBiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pbWcvZGVmaWNpZXQtZXF1YWwtYXJyb3cucG5nJyk7ICAgICBcbiAgaGVpZ2h0OiAxMXB4O1xuICB3aWR0aDogMTlweDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICBiYWNrZ3JvdW5kLXJlcGVhdDogbm8tcmVwZWF0O1xuICBtYXJnaW4tbGVmdDogOHB4O1xufVxuXG4ubG9zdC1jb250YWluZXJ7XG4gLy8gYmFja2dyb3VuZDogdXJsKCcuLi8uLi8uLi9hc3NldHMvaW1nL2RlZmljaWV0LWZpbmFsLXNlY3Rpb24ucG5nJyk7XG4gIGhlaWdodDogNzNweDtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuICB3aWR0aDogMTAwcHg7IFxuICBtYXJnaW4tYm90dG9tOiAtMTBweDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogMCAtNXB4O1xufVxuXG4uZGVmaWNpZXQtdmFsdWV7XG4gIGNvbG9yOmJsdWU7IFxuICBkaXNwbGF5OiBibG9jaztcbiAgZm9udC1zaXplOiAwLjcyNXJlbTtcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmU7XG59XG5cbi5kZWZpY2lldC1pbmZve1xuICBtYXJnaW4tdG9wOiAzNXB4O1xuICBmb250LXNpemU6IDAuNzI1cmVtO1xufVxuXG4uZmFzdGluZy1jYXJkOjpiZWZvcmV7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDEyNXB4O1xuICBoZWlnaHQ6IDExNXB4O1xuICB0b3A6IDUwcHg7XG4gIHJpZ2h0OiAtMTBweDtcbiAvLyBiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uL2Fzc2V0cy9pbWcvZmFzdGluZy1tYWluLXRyYWNrZXIuc3ZnJyk7XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogcmlnaHQ7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHRyYW5zZm9ybTogcm90YXRlKDkwZGVnKTtcbn1cblxuLm5vLW1hcmdpbi10b3B7XG4gIG1hcmdpbi10b3A6IDAgIWltcG9ydGFudDtcbn1cblxuLm1yZy10b3AtMTB7XG4gIG1hcmdpbi10b3A6IDEwcHg7XG59IiwiQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLW1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1NZWRpdW0udHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLWJvbGQ7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tQm9sZC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1leHRlbmRlZE1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1saWdodDtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBTb3VyY2UtU2Fucy1Qcm8tcmFndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cbiIsIkBpbXBvcnQgXCIuLi8uLi9hc3NldHMvZm9udHMvZm9udHMuc2Nzc1wiO1xuXG4vLyBGb250c1xuJGZvbnQtc2l6ZTogMTZweDtcblxuJGZvbnQ6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1yZWd1bGFyXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4kZm9udC1ib2xkOiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtYm9sZFwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtbGlnaHQ6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1saWdodFwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtbWVkaXVtOiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtbWVkaXVtXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4kZm9udC1tZWRpdW0tZXh0ZW5kZWQ6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1leHRlbmRlZE1lZGl1bVwiLCBcIkhlbHZldGljYSBOZXVlXCIsXG4gIEFyaWFsLCBzYW5zLXNlcmlmO1xuXG4gICRzYW5zLWZvbnQtcmVndWxhcjogXCJTb3VyY2UtU2Fucy1Qcm8tcmFndWxhclwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmOyIsIi8vIFByaW1hcnkgY29sb3JzXG4kbGlwc3RpYzogIzAxQTNBNDtcbiRibGFjazojMDFBM0E0O1xuJGdyYXk6cmdiKDg1LDg1LDg1KTtcbiRsaWdodEdyZWVuOnJnYigwLDk5LDk5KTtcbiRsaWdodEdyZWVuMTpyZ2IoMTYxLDE5MSwzOCk7XG4kd2hpdGU6I2ZmZjtcbiRsaWdodFllbGxvdzE6I0Y2RjdGQztcbiRncmF5LTIzOnJnYmEoMjUxLDEzMSwzOCwwLjEpO1xuJGdyYXktOTk6cmdiYSgwLDAsMCwwLjYpO1xuJHllbGxvdy0yOiNGNkY3RkM7XG4keWFsbG93LTM6cmdiKDI1NSwxNjcsMTkpO1xuLy8gR3JhZGllbnRcbi8vJGxpbmVhci1ncmFkaWVudDogbGluZWFyLWdyYWRpZW50KDkwZGVnLCAkYmx1ZS0yMyAwJSwgJGJsdWUtNDEgMCUpO1xuIl19 */";

/***/ }),

/***/ 74450:
/*!*****************************************************************!*\
  !*** ./src/app/components/home-water/home-water.component.scss ***!
  \*****************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJob21lLXdhdGVyLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 98788:
/*!*****************************************************************!*\
  !*** ./src/app/components/recipe-day/recipe-day.component.scss ***!
  \*****************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "@font-face {\n  font-family: gt-america-standard-medium;\n  src: url(\"/assets/fonts/Roboto-Medium.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-regular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-bold;\n  src: url(\"/assets/fonts/Roboto-Bold.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-extendedMedium;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-light;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: Source-Sans-Pro-ragular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n.qa-day {\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1rem;\n  color: #01A3A4;\n}\n.description {\n  padding-top: 10px;\n}\n.description p {\n  font-size: 1rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  text-align: left;\n  text-justify: auto;\n  color: #4C5264 !important;\n  position: relative;\n}\n.title {\n  padding-top: 10px;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif !important;\n  font-size: 0.95rem !important;\n  padding-left: 0 !important;\n}\n.padding-top-1 {\n  padding-top: 10px;\n}\n.qa-day-img {\n  border-radius: 10px;\n}\n.view-more {\n  text-align: left;\n  font-size: 1rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #60AFF5 !important;\n  font-style: italic;\n}\n.content-size {\n  font-size: 0.75rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.detail {\n  border-right: 1px solid #7377774a;\n}\n.detail span {\n  font-weight: 700;\n}\n.padding-left-5 {\n  padding-left: 5px;\n}\n.padding-right-5 {\n  padding-right: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2Fzc2V0cy9mb250cy9mb250cy5zY3NzIiwicmVjaXBlLWRheS5jb21wb25lbnQuc2NzcyIsIi4uLy4uLy4uL3RoZW1lL2N1c3RvbS10aGVtZXMvdmFyaWFibGVzLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSx1Q0FBQTtFQUNBLDhEQUFBO0FDQ0Y7QURFQTtFQUNFLHdDQUFBO0VBQ0EsK0RBQUE7QUNBRjtBREdBO0VBQ0UscUNBQUE7RUFDQSw0REFBQTtBQ0RGO0FESUE7RUFDRSwrQ0FBQTtFQUNBLDZEQUFBO0FDRkY7QURLQTtFQUNFLHNDQUFBO0VBQ0EsNkRBQUE7QUNIRjtBRE9BO0VBQ0Usb0NBQUE7RUFDQSwrREFBQTtBQ0xGO0FBckJBO0VBQ0ksNEVDR1E7RURGUixlQUFBO0VBQ0EsY0FBQTtBQXVCSjtBQXBCQTtFQUNJLGlCQUFBO0FBdUJKO0FBdEJJO0VBQ0ksZUFBQTtFQUNBLCtFQ1BEO0VEUUMsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7QUF3QlI7QUFyQkE7RUFDSSxpQkFBQTtFQUNBLHVGQUFBO0VBQ0EsNkJBQUE7RUFDQSwwQkFBQTtBQXdCSjtBQXRCQTtFQUNJLGlCQUFBO0FBeUJKO0FBdkJBO0VBQ0ksbUJBQUE7QUEwQko7QUF4QkM7RUFDRyxnQkFBQTtFQUNBLGVBQUE7RUFDQSwrRUM3Qkc7RUQ4QkgseUJBQUE7RUFDQSxrQkFBQTtBQTJCSjtBQXpCQTtFQUNJLGtCQUFBO0VBQ0EsK0VDbkNHO0FEK0RQO0FBMUJDO0VBQ0csaUNBQUE7QUE2Qko7QUE1Qkk7RUFDSSxnQkFBQTtBQThCUjtBQTNCQztFQUNJLGlCQUFBO0FBOEJMO0FBNUJDO0VBQ0csa0JBQUE7QUErQkoiLCJmaWxlIjoicmVjaXBlLWRheS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1tZWRpdW07XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tTWVkaXVtLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLXJlZ3VsYXI7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tUmVndWxhci50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1ib2xkO1xuICBzcmM6IHVybChcIi9hc3NldHMvZm9udHMvUm9ib3RvLUJvbGQudHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtZXh0ZW5kZWRNZWRpdW07XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tTGlnaHQudHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtbGlnaHQ7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tTGlnaHQudHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogU291cmNlLVNhbnMtUHJvLXJhZ3VsYXI7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tUmVndWxhci50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG4iLCJcbkBpbXBvcnQgJy4uLy4uLy4uL3RoZW1lL2ZvbnRzLnNjc3MnO1xuLnFhLWRheXtcbiAgICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgY29sb3I6IzAxQTNBNDtcbn1cblxuLmRlc2NyaXB0aW9ue1xuICAgIHBhZGRpbmctdG9wOjEwcHg7XG4gICAgcHtcbiAgICAgICAgZm9udC1zaXplOiAxcmVtO1xuICAgICAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIHRleHQtanVzdGlmeTogYXV0bztcbiAgICAgICAgY29sb3I6IzRDNTI2NCAhaW1wb3J0YW50O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgfVxufVxuLnRpdGxle1xuICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgIGZvbnQtZmFtaWx5OiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtYm9sZFwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmICFpbXBvcnRhbnQ7XG4gICAgZm9udC1zaXplOiAwLjk1cmVtICFpbXBvcnRhbnQ7XG4gICAgcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7ICBcbn1cbi5wYWRkaW5nLXRvcC0xe1xuICAgIHBhZGRpbmctdG9wOjEwcHg7XG59XG4ucWEtZGF5LWltZ3tcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuIH1cbiAudmlldy1tb3Jle1xuICAgIHRleHQtYWxpZ246bGVmdDtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgZm9udC1mYW1pbHk6ICRmb250O1xuICAgIGNvbG9yOiM2MEFGRjUgIWltcG9ydGFudDtcbiAgICBmb250LXN0eWxlOiBpdGFsaWM7XG4gfVxuLmNvbnRlbnQtc2l6ZXtcbiAgICBmb250LXNpemU6IC43NXJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG59XG4gLmRldGFpbHtcbiAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjNzM3Nzc3NGE7XG4gICAgc3BhbntcbiAgICAgICAgZm9udC13ZWlnaHQ6IDcwMDtcbiAgICB9XG4gfVxuIC5wYWRkaW5nLWxlZnQtNXtcbiAgICAgcGFkZGluZy1sZWZ0OiA1cHg7XG4gfVxuIC5wYWRkaW5nLXJpZ2h0LTV7XG4gICAgcGFkZGluZy1yaWdodDogNXB4O1xufSIsIkBpbXBvcnQgXCIuLi8uLi9hc3NldHMvZm9udHMvZm9udHMuc2Nzc1wiO1xuXG4vLyBGb250c1xuJGZvbnQtc2l6ZTogMTZweDtcblxuJGZvbnQ6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1yZWd1bGFyXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4kZm9udC1ib2xkOiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtYm9sZFwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtbGlnaHQ6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1saWdodFwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtbWVkaXVtOiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtbWVkaXVtXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4kZm9udC1tZWRpdW0tZXh0ZW5kZWQ6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1leHRlbmRlZE1lZGl1bVwiLCBcIkhlbHZldGljYSBOZXVlXCIsXG4gIEFyaWFsLCBzYW5zLXNlcmlmO1xuXG4gICRzYW5zLWZvbnQtcmVndWxhcjogXCJTb3VyY2UtU2Fucy1Qcm8tcmFndWxhclwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmOyJdfQ== */";

/***/ }),

/***/ 20711:
/*!***************************************************************!*\
  !*** ./src/app/modal/summary-update/summary-update.page.scss ***!
  \***************************************************************/
/***/ ((module) => {

"use strict";
module.exports = ".mainContent {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  background-color: rgba(0, 0, 0, 0.5);\n}\n\n.backgroundContent {\n  /* background: #fff; */\n  width: 100%;\n  /* padding-bottom: 10px; */\n  position: absolute;\n  height: 100%;\n  top: 45%;\n  bottom: 0px;\n  transform: translateY(-30%) translateX(5%);\n}\n\n.textAreaBorder {\n  border: 1px solid #707070;\n  border-radius: 11px;\n  height: 70px;\n  --padding-start: 11px;\n}\n\n.labelFont {\n  font-size: 14px;\n  color: var(--ion-color-theme) !important;\n}\n\n.radioList {\n  color: #707070 !important;\n  font-size: 14px;\n}\n\n.leadRadioPosition {\n  display: flex;\n}\n\n.mr-10 {\n  margin-right: 10px;\n}\n\n.br-23 {\n  border-radius: 23px;\n}\n\n.font-title {\n  font-size: 14px;\n  font-weight: 600;\n}\n\n.close-icon {\n  font-size: 25px;\n  float: right;\n  position: relative;\n  top: -12px;\n}\n\n.radio-group-class {\n  width: 100%;\n}\n\n.radio-group-class ion-item {\n  width: 33%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN1bW1hcnktdXBkYXRlLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGVBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLG9DQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxzQkFBQTtFQUNBLFdBQUE7RUFDQSwwQkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBQ0EsMENBQUE7QUFFRjs7QUFBQTtFQUNFLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EscUJBQUE7QUFHRjs7QUFEQTtFQUNFLGVBQUE7RUFDQSx3Q0FBQTtBQUlGOztBQUZBO0VBQ0UseUJBQUE7RUFDQSxlQUFBO0FBS0Y7O0FBSEE7RUFDRSxhQUFBO0FBTUY7O0FBSEE7RUFDRSxrQkFBQTtBQU1GOztBQUhBO0VBQ0UsbUJBQUE7QUFNRjs7QUFIQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtBQU1GOztBQUhBO0VBQ0UsZUFBQTtFQUNFLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7QUFNSjs7QUFIQTtFQUNFLFdBQUE7QUFNRjs7QUFMRTtFQUNFLFVBQUE7QUFPSiIsImZpbGUiOiJzdW1tYXJ5LXVwZGF0ZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbkNvbnRlbnQge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC41KTtcbn1cbi5iYWNrZ3JvdW5kQ29udGVudCB7XG4gIC8qIGJhY2tncm91bmQ6ICNmZmY7ICovXG4gIHdpZHRoOiAxMDAlO1xuICAvKiBwYWRkaW5nLWJvdHRvbTogMTBweDsgKi9cbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHRvcDogNDUlO1xuICBib3R0b206IDBweDtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC0zMCUpIHRyYW5zbGF0ZVgoNSUpO1xufVxuLnRleHRBcmVhQm9yZGVyIHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzcwNzA3MDtcbiAgYm9yZGVyLXJhZGl1czogMTFweDtcbiAgaGVpZ2h0OiA3MHB4O1xuICAtLXBhZGRpbmctc3RhcnQ6IDExcHg7XG59XG4ubGFiZWxGb250IHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXRoZW1lKSAhaW1wb3J0YW50O1xufVxuLnJhZGlvTGlzdCB7XG4gIGNvbG9yOiAjNzA3MDcwICFpbXBvcnRhbnQ7XG4gIGZvbnQtc2l6ZTogMTRweDtcbn1cbi5sZWFkUmFkaW9Qb3NpdGlvbiB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5tci0xMHtcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xufVxuXG4uYnItMjN7XG4gIGJvcmRlci1yYWRpdXM6IDIzcHg7XG59XG5cbi5mb250LXRpdGxle1xuICBmb250LXNpemU6IDE0cHg7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5jbG9zZS1pY29ue1xuICBmb250LXNpemU6IDI1cHg7XG4gICAgZmxvYXQ6IHJpZ2h0O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IC0xMnB4O1xufVxuXG4ucmFkaW8tZ3JvdXAtY2xhc3N7XG4gIHdpZHRoOiAxMDAlO1xuICBpb24taXRlbXtcbiAgICB3aWR0aDogMzMlO1xuICB9XG59Il19 */";

/***/ }),

/***/ 49930:
/*!********************************************************************!*\
  !*** ./src/app/select-plan-popup/select-plan-popup.component.scss ***!
  \********************************************************************/
/***/ ((module) => {

"use strict";
module.exports = "@font-face {\n  font-family: gt-america-standard-medium;\n  src: url(\"/assets/fonts/Roboto-Medium.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-regular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-bold;\n  src: url(\"/assets/fonts/Roboto-Bold.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-extendedMedium;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-light;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: Source-Sans-Pro-ragular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n.back-overlay {\n  background: #000;\n  opacity: 0.5;\n  height: 100%;\n  width: 100%;\n  z-index: 11;\n}\n.content-back {\n  background: #fff;\n  width: 90%;\n  z-index: 12;\n  display: inline-block;\n  top: 35%;\n  left: 5%;\n  position: absolute;\n  border-radius: 10px;\n}\n.offer-bottom-popup {\n  width: 100%;\n  position: absolute;\n  /* bottom: 36%; */\n  padding: 10%;\n  background: rgba(239, 239, 239, 0);\n  border-top-right-radius: 25px;\n  border-top-left-radius: 25px;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n}\nion-card {\n  margin: 0;\n  border-radius: 20px;\n}\nion-card-header {\n  padding: 0;\n}\nion-card-header ion-card-subtitle {\n  text-transform: none;\n  font-size: 1.5rem;\n  margin: 0;\n}\nion-card-content {\n  padding: 10px;\n}\nion-card-content .logo {\n  margin: auto;\n}\nion-card-content .logo div {\n  padding: 5px;\n  display: inline-block;\n  border-radius: 15px;\n  box-shadow: 0px 0px 10px 0px rgba(129, 133, 131, 0.5);\n}\nion-card-content .logo img {\n  height: 2.2rem;\n  width: 2.2rem;\n  display: inline-block;\n  vertical-align: middle;\n}\nion-card-content .content-header {\n  font-size: 1.5rem;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #595C5C;\n  padding-left: 10px;\n  display: inline-block;\n  vertical-align: middle;\n}\nion-card-content .green-msg {\n  font-size: 0.9rem;\n  color: #01A3A4;\n}\nion-card-content .start-transform-text {\n  font-size: 0.95rem;\n}\nion-card-content .doubt-text {\n  font-size: 0.95rem;\n}\nion-card-content .note-msg {\n  font-size: 0.65rem;\n  margin-bottom: 5px;\n}\nion-card-content .padding-top-bottom {\n  padding: 5px 10px;\n}\nion-card-content .whatsapp {\n  height: 2rem;\n  width: 2rem;\n  display: inline-block;\n  vertical-align: middle;\n}\nion-card-content .subscribe {\n  --background: #01A3A4;\n  color: white;\n  width: 85%;\n  margin: auto;\n  --border-radius: 25px;\n}\nion-card-content .strike {\n  text-decoration: line-through;\n  font-family: \"gt-america-standard\", \"Helvetica Neue\", Arial, sans-serif;\n  font-size: 1rem;\n}\n.color-gold {\n  color: #D5A620;\n}\nion-content {\n  --background: #F6F7FC !important;\n}\n.personalise-covid ion-card {\n  display: inline-block;\n  width: 90%;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.plan-container {\n  background-size: contain;\n  background-repeat: no-repeat;\n  background-position: right;\n  margin-bottom: 0;\n  padding: 0 10px 10px;\n  max-height: 80vh;\n  overflow: scroll;\n}\n.sub-foodItem-header {\n  font-size: 1.2rem;\n  font-weight: bold;\n  color: #4C5264;\n}\n.sub-foodItem-sub-header {\n  font-size: 0.9rem;\n}\n.sub-foodItem-btn {\n  border: none !important;\n  width: auto !important;\n  color: #1492E6;\n  --background: transparent;\n  min-width: 80px !important;\n  margin-top: 5px;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.sub-cards {\n  background: #fff;\n  margin-top: 0;\n}\n.ion-card-footer {\n  width: 100%;\n  bottom: 0;\n  position: absolute;\n}\n.card-footer {\n  width: 100%;\n  bottom: 0;\n  position: absolute;\n  height: 20%;\n  background: #03a3a4;\n}\n.premium {\n  position: absolute;\n  left: 1rem;\n  top: 2px;\n  height: 80%;\n  width: auto;\n}\n.qa-day-skeleton {\n  margin-top: 0;\n  padding: 0;\n  height: 300px;\n  width: 90%;\n  display: inline-table;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n}\n.activity-skeleton {\n  width: 100%;\n  height: 100%;\n  margin-top: 0;\n}\n.weight-loss-button-cancel {\n  --color: #01A3A4;\n  --border-color:#01A3A4;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.weight-loss-button-activate {\n  --background: #01a3a4;\n  --color: #fff;\n  --border-color:#01A3A4;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.immunity-button-cancel {\n  --color: #FD9F33;\n  --border-color:#FD9F33;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.immunity-button-activate {\n  --background: #FD9F33;\n  --color: #fff;\n  --border-color:#FD9F33;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.post-covid-button-cancel {\n  --color: #754B29;\n  --border-color:#754B29;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.post-covid-button-activate {\n  --background: #754B29;\n  --color: #fff;\n  --border-color:#754B29;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.weight-loss-plus-button-cancel {\n  --color: #0B94C1;\n  --border-color:#0B94C1;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.weight-loss-plus-button-activate {\n  --background: #0B94C1;\n  --color: #fff;\n  --border-color:#0B94C1;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.detox-button-cancel {\n  --color: #4CB271;\n  --border-color:#4CB271;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.detox-button-activate {\n  --background: #4CB271;\n  --color: #fff;\n  --border-color:#4CB271;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.diabetes-button-cancel {\n  --color: #D14322;\n  --border-color:#D14322;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.diabetes-button-activate {\n  --background: #D14322;\n  --color: #fff;\n  --border-color:#D14322;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.cholesterol-button-cancel {\n  --color: #A31E79;\n  --border-color:#A31E79;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.cholesterol-button-activate {\n  --background: #A31E79;\n  --color: #fff;\n  --border-color:#A31E79;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.pcos-button-cancel {\n  --color: #FF5A7D;\n  --border-color:#FF5A7D;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.pcos-button-activate {\n  --background: #FF5A7D;\n  --color: #fff;\n  --border-color:#FF5A7D;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.muscle-button-cancel {\n  --color: #0B94C1;\n  --border-color:#0B94C1;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.muscle-button-activate {\n  --background: #0B94C1;\n  --color: #fff;\n  --border-color:#0B94C1;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.fat-button-cancel {\n  --color: #FD980F;\n  --border-color:#FD980F;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.fat-button-activate {\n  --background: #FD980F;\n  --color: #fff;\n  --border-color:#FD980F;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.hypertension-button-cancel {\n  --color: #FF5D56;\n  --border-color:#FF5D56;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.hypertension-button-activate {\n  --background: #FF5D56;\n  --color: #fff;\n  --border-color:#FF5D56;\n  --border-radius: 100px;\n  border-radius: 100px;\n}\n.close {\n  right: 0;\n  position: absolute;\n  font-size: 2rem;\n  color: #7d7d7d;\n  z-index: 100000;\n  top: 0px;\n}\n.text-title {\n  font-size: 0.925rem;\n}\n.text-desc {\n  font-size: 0.925rem;\n}\n.text-quote {\n  font-size: 0.925rem;\n  font-style: italic;\n}\n.text-desc-note {\n  font-size: 0.875rem;\n  font-style: italic;\n  color: darkgray;\n}\n.border-top {\n  border-radius: 100px;\n  width: 100%;\n  height: 7px;\n  display: block;\n  margin-top: -15px;\n}\n.border-section-btm {\n  border-radius: 100px;\n  width: 88%;\n  height: 7px;\n  display: block;\n  margin-left: 6%;\n}\nion-button {\n  max-height: 2.35rem;\n  font-size: 1rem;\n}\n.sub-foodItem-sub-header-custom {\n  font-size: 0.925rem;\n  font-weight: bold;\n}\n.main-sec {\n  max-height: 90vh;\n}\n.main-sec .plan-container .bottom-btn {\n  position: absolute;\n  bottom: 0px;\n  background: white;\n  width: 85%;\n  padding: 10px 0px;\n}\n.main-sec .plan-container::-webkit-scrollbar {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL2Fzc2V0cy9mb250cy9mb250cy5zY3NzIiwic2VsZWN0LXBsYW4tcG9wdXAuY29tcG9uZW50LnNjc3MiLCIuLi8uLi90aGVtZS9jdXN0b20tdGhlbWVzL3ZhcmlhYmxlcy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsdUNBQUE7RUFDQSw4REFBQTtBQ0NGO0FERUE7RUFDRSx3Q0FBQTtFQUNBLCtEQUFBO0FDQUY7QURHQTtFQUNFLHFDQUFBO0VBQ0EsNERBQUE7QUNERjtBRElBO0VBQ0UsK0NBQUE7RUFDQSw2REFBQTtBQ0ZGO0FES0E7RUFDRSxzQ0FBQTtFQUNBLDZEQUFBO0FDSEY7QURPQTtFQUNFLG9DQUFBO0VBQ0EsK0RBQUE7QUNMRjtBQXRCQTtFQUNFLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQXdCRjtBQXJCQTtFQUNFLGdCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxxQkFBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtBQXdCRjtBQXJCQTtFQVNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGtDQUFBO0VBQ0EsNkJBQUE7RUFDQSw0QkFBQTtFQUNBLFNBQUE7RUFDQSxRQUFBO0VBQ0EsZ0NBQUE7QUFnQkY7QUFiQTtFQUNFLFNBQUE7RUFDQSxtQkFBQTtBQWdCRjtBQVhBO0VBQ0UsVUFBQTtBQWNGO0FBWEU7RUFDRSxvQkFBQTtFQUVBLGlCQUFBO0VBQ0EsU0FBQTtBQVlKO0FBUkE7RUFDRSxhQUFBO0FBV0Y7QUFURTtFQUNFLFlBQUE7QUFXSjtBQVRJO0VBQ0UsWUFBQTtFQUNBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxxREFBQTtBQVdOO0FBUkk7RUFDRSxjQUFBO0VBQ0EsYUFBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7QUFVTjtBQU5FO0VBQ0UsaUJBQUE7RUFDQSw0RUM3RVE7RUQ4RVIsY0FBQTtFQUVBLGtCQUFBO0VBQ0EscUJBQUE7RUFDQSxzQkFBQTtBQU9KO0FBSkU7RUFDRSxpQkFBQTtFQUNBLGNBQUE7QUFNSjtBQUhFO0VBRUUsa0JBQUE7QUFJSjtBQURFO0VBRUUsa0JBQUE7QUFFSjtBQUNFO0VBQ0Usa0JBQUE7RUFDQSxrQkFBQTtBQUNKO0FBRUU7RUFDRSxpQkFBQTtBQUFKO0FBR0U7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLHFCQUFBO0VBQ0Esc0JBQUE7QUFESjtBQUlFO0VBQ0UscUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7RUFDQSxxQkFBQTtBQUZKO0FBS0U7RUFDRSw2QkFBQTtFQUNBLHVFQUFBO0VBQ0EsZUFBQTtBQUhKO0FBT0E7RUFDRSxjQUFBO0FBSkY7QUFRQTtFQUNFLGdDQUFBO0FBTEY7QUFXRTtFQUNFLHFCQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUFSSjtBQWNBO0VBQ0Usd0JBQUE7RUFDQSw0QkFBQTtFQUNBLDBCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZ0JBQUE7QUFYRjtBQTBDQTtFQUNFLGlCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0FBdkNGO0FBMENBO0VBQ0UsaUJBQUE7QUF2Q0Y7QUEwQ0E7RUFDRSx1QkFBQTtFQUNBLHNCQUFBO0VBRUEsY0FBQTtFQUNBLHlCQUFBO0VBRUEsMEJBQUE7RUFDQSxlQUFBO0VBRUEsNEVBQUE7QUExQ0Y7QUE2Q0E7RUFDRSxnQkFBQTtFQUVBLGFBQUE7QUEzQ0Y7QUE4Q0E7RUFFRSxXQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0FBNUNGO0FBK0NBO0VBQ0UsV0FBQTtFQUNBLFNBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQTVDRjtBQThDQTtFQUdFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtBQTdDRjtBQWdEQTtFQUNFLGFBQUE7RUFDQSxVQUFBO0VBQ0EsYUFBQTtFQUNBLFVBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7QUE3Q0Y7QUFnREE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGFBQUE7QUE3Q0Y7QUFnREE7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtBQTdDRjtBQWdEQTtFQUNFLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtBQTdDRjtBQWdEQTtFQUNFLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0FBN0NGO0FBZ0RBO0VBQ0UscUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0FBN0NGO0FBZ0RBO0VBQ0UsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7QUE3Q0Y7QUFnREE7RUFDRSxxQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7QUE3Q0Y7QUFnREE7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtBQTdDRjtBQWdEQTtFQUNFLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtBQTdDRjtBQWdEQTtFQUNFLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0FBN0NGO0FBZ0RBO0VBQ0UscUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0FBN0NGO0FBZ0RBO0VBQ0UsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7QUE3Q0Y7QUFnREE7RUFDRSxxQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7QUE3Q0Y7QUFnREE7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtBQTdDRjtBQWdEQTtFQUNFLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtBQTdDRjtBQWdEQTtFQUNFLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0FBN0NGO0FBZ0RBO0VBQ0UscUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0FBN0NGO0FBZ0RBO0VBQ0UsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7QUE3Q0Y7QUFnREE7RUFDRSxxQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7QUE3Q0Y7QUFnREE7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtBQTdDRjtBQWdEQTtFQUNFLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0Esc0JBQUE7RUFDQSxvQkFBQTtBQTdDRjtBQWdEQTtFQUNFLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0FBN0NGO0FBZ0RBO0VBQ0UscUJBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxzQkFBQTtFQUNBLG9CQUFBO0FBN0NGO0FBZ0RBO0VBQ0UsUUFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsUUFBQTtBQTdDRjtBQWdEQTtFQUVFLG1CQUFBO0FBOUNGO0FBaURBO0VBQ0UsbUJBQUE7QUE5Q0Y7QUFpREE7RUFDRSxtQkFBQTtFQUNBLGtCQUFBO0FBOUNGO0FBaURBO0VBQ0UsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUE5Q0Y7QUFpREE7RUFDRSxvQkFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0FBOUNGO0FBaURBO0VBQ0Usb0JBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBOUNGO0FBaURBO0VBQ0UsbUJBQUE7RUFDQSxlQUFBO0FBOUNGO0FBaURBO0VBQ0UsbUJBQUE7RUFDQSxpQkFBQTtBQTlDRjtBQWlEQTtFQUNFLGdCQUFBO0FBOUNGO0FBZ0RJO0VBQ0Usa0JBQUE7RUFDQSxXQUFBO0VBQ0EsaUJBQUE7RUFDQSxVQUFBO0VBQ0EsaUJBQUE7QUE5Q047QUFrREU7RUFDRSxhQUFBO0FBaERKIiwiZmlsZSI6InNlbGVjdC1wbGFuLXBvcHVwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLW1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1NZWRpdW0udHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLWJvbGQ7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tQm9sZC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1leHRlbmRlZE1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1saWdodDtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBTb3VyY2UtU2Fucy1Qcm8tcmFndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cbiIsIkBpbXBvcnQgXCIuLi8uLi90aGVtZS9mb250cy5zY3NzXCI7XG4uYmFjay1vdmVybGF5IHtcbiAgYmFja2dyb3VuZDogIzAwMDtcbiAgb3BhY2l0eTogMC41O1xuICBoZWlnaHQ6IDEwMCU7XG4gIHdpZHRoOiAxMDAlO1xuICB6LWluZGV4OiAxMTtcbn1cblxuLmNvbnRlbnQtYmFjayB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIHdpZHRoOiA5MCU7XG4gIHotaW5kZXg6IDEyO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHRvcDogMzUlO1xuICBsZWZ0OiA1JTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuXG4ub2ZmZXItYm90dG9tLXBvcHVwIHtcbiAgLy8gd2lkdGg6IDEwMCU7XG4gIC8vIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgLy8gYm90dG9tOiAwO1xuICAvLyBwYWRkaW5nOiA1cHggNXB4IDBweDtcbiAgLy8gYmFja2dyb3VuZDogcmdiYSgyMzksIDIzOSwgMjM5LCAwKTtcbiAgLy8gLyogbWFyZ2luOiAxMHB4IDEwcHggMDsgKi9cbiAgLy8gYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDI1cHg7XG4gIC8vIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDI1cHg7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIC8qIGJvdHRvbTogMzYlOyAqL1xuICBwYWRkaW5nOiAxMCU7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjM5LCAyMzksIDIzOSwgMCk7XG4gIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAyNXB4O1xuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAyNXB4O1xuICBsZWZ0OiA1MCU7XG4gIHRvcDogNTAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcbn1cblxuaW9uLWNhcmQge1xuICBtYXJnaW46IDA7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIC8vIGJvcmRlci10b3AtbGVmdC1yYWRpdXM6IDE1cHg7XG4gIC8vIGJvcmRlci10b3AtcmlnaHQtcmFkaXVzOiAxNXB4O1xufVxuXG5pb24tY2FyZC1oZWFkZXJ7XG4gIHBhZGRpbmc6IDA7Ly8xNXB4IDIwcHg7XG4gIC8vIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCAjYjViNWI1O1xuXG4gIGlvbi1jYXJkLXN1YnRpdGxle1xuICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICAgIC8vIGZvbnQtZmFtaWx5OiAkZm9udC1ib2xkO1xuICAgIGZvbnQtc2l6ZTogMS41cmVtO1xuICAgIG1hcmdpbjogMDtcbiAgfVxufVxuXG5pb24tY2FyZC1jb250ZW50e1xuICBwYWRkaW5nOiAxMHB4O1xuXG4gIC5sb2dvIHtcbiAgICBtYXJnaW46IGF1dG87XG5cbiAgICBkaXYge1xuICAgICAgcGFkZGluZzogNXB4O1xuICAgICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICAgIGJveC1zaGFkb3c6IDBweCAwcHggMTBweCAwcHggcmdiYSgxMjksIDEzMywgMTMxLCAwLjUwKTtcbiAgICB9XG5cbiAgICBpbWd7XG4gICAgICBoZWlnaHQ6IDIuMnJlbTtcbiAgICAgIHdpZHRoOiAyLjJyZW07XG4gICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAgIH1cbiAgfVxuXG4gIC5jb250ZW50LWhlYWRlcntcbiAgICBmb250LXNpemU6IDEuNXJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgICBjb2xvcjogIzU5NUM1QztcblxuICAgIHBhZGRpbmctbGVmdDogMTBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgfVxuXG4gIC5ncmVlbi1tc2d7XG4gICAgZm9udC1zaXplOiAwLjlyZW07XG4gICAgY29sb3I6ICMwMUEzQTQ7XG4gIH1cblxuICAuc3RhcnQtdHJhbnNmb3JtLXRleHR7XG4gICAgLy8gZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG4gICAgZm9udC1zaXplOiAwLjk1cmVtO1xuICB9XG5cbiAgLmRvdWJ0LXRleHR7XG4gICAgLy8gZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG4gICAgZm9udC1zaXplOiAwLjk1cmVtO1xuICB9XG5cbiAgLm5vdGUtbXNne1xuICAgIGZvbnQtc2l6ZTogMC42NXJlbTtcbiAgICBtYXJnaW4tYm90dG9tOiA1cHg7XG4gIH1cblxuICAucGFkZGluZy10b3AtYm90dG9te1xuICAgIHBhZGRpbmc6IDVweCAxMHB4O1xuICB9XG5cbiAgLndoYXRzYXBwe1xuICAgIGhlaWdodDogMnJlbTtcbiAgICB3aWR0aDogMnJlbTtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbiAgfVxuXG4gIC5zdWJzY3JpYmV7XG4gICAgLS1iYWNrZ3JvdW5kOiAjMDFBM0E0O1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB3aWR0aDogODUlO1xuICAgIG1hcmdpbjogYXV0bztcbiAgICAtLWJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIH1cblxuICAuc3RyaWtle1xuICAgIHRleHQtZGVjb3JhdGlvbjogbGluZS10aHJvdWdoO1xuICAgIGZvbnQtZmFtaWx5OiBcImd0LWFtZXJpY2Etc3RhbmRhcmRcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiAgICBmb250LXNpemU6IDFyZW07XG4gIH1cbn1cblxuLmNvbG9yLWdvbGR7XG4gIGNvbG9yOiAjRDVBNjIwO1xufVxuXG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRjZGN0ZDICFpbXBvcnRhbnQ7XG4gIC8vIC0tYmFja2dyb3VuZDogLXdlYmtpdC1saW5lYXItZ3JhZGllbnQodG9wLCAjMDFBM0E0IDUwJSwgI0Y2RjdGQyA1MCUpIWltcG9ydGFudDtcblxufVxuXG4ucGVyc29uYWxpc2UtY292aWR7XG4gIGlvbi1jYXJke1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogOTAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDVweCAxMHB4IDBweCByZ2IoMTI5IDEzMyAxMzEgLyAxMCUpICFpbXBvcnRhbnQ7XG4gICAgLy8gbWFyZ2luOiAyMHB4O1xuICAgIFxuICB9IFxufVxuXG4ucGxhbi1jb250YWluZXJ7XG4gIGJhY2tncm91bmQtc2l6ZTogY29udGFpbjtcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbiAgYmFja2dyb3VuZC1wb3NpdGlvbjogcmlnaHQ7XG4gIG1hcmdpbi1ib3R0b206IDA7XG4gIHBhZGRpbmc6IDAgMTBweCAxMHB4O1xuICBtYXgtaGVpZ2h0OiA4MHZoO1xuICBvdmVyZmxvdzogc2Nyb2xsO1xuICAvLyBoZWlnaHQ6IDE3NXB4O1xufVxuXG4vLyAuaW1tdW5pdHktY29udGFpbmVye1xuLy8gICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vYXNzZXRzL2ltZy9pbW11bml0eS5qcGVnJyk7XG4vLyAgICAgYmFja2dyb3VuZC1zaXplOiBjb250YWluO1xuLy8gICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XG4vLyAgICAgYmFja2dyb3VuZC1wb3NpdGlvbjogcmlnaHQ7XG4vLyAgICAgbWFyZ2luLXRvcDogMDtcbi8vICAgICBoZWlnaHQ6IDIwMHB4O1xuLy8gfVxuXG4vLyAud2VpZ2h0LWxvc3MtY29udGFpbmVye1xuLy8gICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnLi4vLi4vYXNzZXRzL2ltZy93ZWlnaHQtbG9zcy5qcGcnKTtcbi8vICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4vLyAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbi8vICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiByaWdodDtcbi8vICAgICBtYXJnaW4tdG9wOiAwO1xuLy8gICAgIGhlaWdodDogMjAwcHg7XG4vLyB9XG5cbi8vIC5wb3N0LWNvdmlkLWNvbnRhaW5lcntcbi8vICAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJy4uLy4uL2Fzc2V0cy9pbWcvcG9zdF9jb3ZpZC5qcGcnKTtcbi8vICAgICBiYWNrZ3JvdW5kLXNpemU6IGNvbnRhaW47XG4vLyAgICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcbi8vICAgICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiByaWdodDtcbi8vICAgICBtYXJnaW4tdG9wOiAwO1xuLy8gICAgIGhlaWdodDogMjAwcHg7XG4vLyB9XG5cbi5zdWItZm9vZEl0ZW0taGVhZGVye1xuICBmb250LXNpemU6IDEuMnJlbTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNEM1MjY0O1xufVxuXG4uc3ViLWZvb2RJdGVtLXN1Yi1oZWFkZXJ7XG4gIGZvbnQtc2l6ZTogMC45cmVtO1xufVxuXG4uc3ViLWZvb2RJdGVtLWJ0bntcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XG4gIC8vIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGNvbG9yOiAjMTQ5MkU2O1xuICAtLWJhY2tncm91bmQ6IHRyYW5zcGFyZW50O1xuICAvLyAtLWJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIG1pbi13aWR0aDogODBweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIC8vIGZvbnQtc2l6ZTogMC44NzVyZW0gIWltcG9ydGFudDtcbiAgZm9udC1mYW1pbHk6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1ib2xkXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG59XG5cbi5zdWItY2FyZHN7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIC8vIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIG1hcmdpbi10b3A6IDA7XG59XG5cbi5pb24tY2FyZC1mb290ZXIge1xuICAvLyBwb3NpdGlvbjogZml4ZWQ7XG4gIHdpZHRoOiAxMDAlO1xuICBib3R0b206IDA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbn1cblxuLmNhcmQtZm9vdGVye1xuICB3aWR0aDogMTAwJTtcbiAgYm90dG9tOiAwO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGhlaWdodDogMjAlO1xuICBiYWNrZ3JvdW5kOiAjMDNhM2E0O1xufVxuLnByZW1pdW0ge1xuICAvLyB3aWR0aDogNTBweDtcblxuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDFyZW07XG4gIHRvcDogMnB4O1xuICBoZWlnaHQ6IDgwJTtcbiAgd2lkdGg6IGF1dG87XG59XG5cbi5xYS1kYXktc2tlbGV0b257XG4gIG1hcmdpbi10b3A6IDA7XG4gIHBhZGRpbmc6IDA7XG4gIGhlaWdodDogMzAwcHg7XG4gIHdpZHRoOiA5MCU7XG4gIGRpc3BsYXk6IGlubGluZS10YWJsZTtcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgYm94LXNoYWRvdzogMHB4IDVweCAxMHB4IDBweCByZ2IoMTI5IDEzMyAxMzEgLyAxMCUpICFpbXBvcnRhbnQ7XG59XG5cbi5hY3Rpdml0eS1za2VsZXRvbntcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgbWFyZ2luLXRvcDogMDtcbn1cblxuLndlaWdodC1sb3NzLWJ1dHRvbi1jYW5jZWx7XG4gIC0tY29sb3I6ICMwMUEzQTQ7XG4gIC0tYm9yZGVyLWNvbG9yOiMwMUEzQTQ7XG4gIC0tYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4ud2VpZ2h0LWxvc3MtYnV0dG9uLWFjdGl2YXRle1xuICAtLWJhY2tncm91bmQ6ICMwMWEzYTQ7XG4gIC0tY29sb3I6ICNmZmY7XG4gIC0tYm9yZGVyLWNvbG9yOiMwMUEzQTQ7XG4gIC0tYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4uaW1tdW5pdHktYnV0dG9uLWNhbmNlbHtcbiAgLS1jb2xvcjogI0ZEOUYzMztcbiAgLS1ib3JkZXItY29sb3I6I0ZEOUYzMztcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi5pbW11bml0eS1idXR0b24tYWN0aXZhdGV7XG4gIC0tYmFja2dyb3VuZDogI0ZEOUYzMztcbiAgLS1jb2xvcjogI2ZmZjtcbiAgLS1ib3JkZXItY29sb3I6I0ZEOUYzMztcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi5wb3N0LWNvdmlkLWJ1dHRvbi1jYW5jZWx7XG4gIC0tY29sb3I6ICM3NTRCMjk7XG4gIC0tYm9yZGVyLWNvbG9yOiM3NTRCMjk7XG4gIC0tYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4ucG9zdC1jb3ZpZC1idXR0b24tYWN0aXZhdGV7XG4gIC0tYmFja2dyb3VuZDogIzc1NEIyOTtcbiAgLS1jb2xvcjogI2ZmZjtcbiAgLS1ib3JkZXItY29sb3I6Izc1NEIyOTtcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi53ZWlnaHQtbG9zcy1wbHVzLWJ1dHRvbi1jYW5jZWx7XG4gIC0tY29sb3I6ICMwQjk0QzE7XG4gIC0tYm9yZGVyLWNvbG9yOiMwQjk0QzE7XG4gIC0tYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4ud2VpZ2h0LWxvc3MtcGx1cy1idXR0b24tYWN0aXZhdGV7XG4gIC0tYmFja2dyb3VuZDogIzBCOTRDMTtcbiAgLS1jb2xvcjogI2ZmZjtcbiAgLS1ib3JkZXItY29sb3I6IzBCOTRDMTtcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi5kZXRveC1idXR0b24tY2FuY2Vse1xuICAtLWNvbG9yOiAjNENCMjcxO1xuICAtLWJvcmRlci1jb2xvcjojNENCMjcxO1xuICAtLWJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiAxMDBweDtcbn1cblxuLmRldG94LWJ1dHRvbi1hY3RpdmF0ZXtcbiAgLS1iYWNrZ3JvdW5kOiAjNENCMjcxO1xuICAtLWNvbG9yOiAjZmZmO1xuICAtLWJvcmRlci1jb2xvcjojNENCMjcxO1xuICAtLWJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiAxMDBweDtcbn1cblxuLmRpYWJldGVzLWJ1dHRvbi1jYW5jZWx7XG4gIC0tY29sb3I6ICNEMTQzMjI7XG4gIC0tYm9yZGVyLWNvbG9yOiNEMTQzMjI7XG4gIC0tYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4uZGlhYmV0ZXMtYnV0dG9uLWFjdGl2YXRle1xuICAtLWJhY2tncm91bmQ6ICNEMTQzMjI7XG4gIC0tY29sb3I6ICNmZmY7XG4gIC0tYm9yZGVyLWNvbG9yOiNEMTQzMjI7XG4gIC0tYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4uY2hvbGVzdGVyb2wtYnV0dG9uLWNhbmNlbHtcbiAgLS1jb2xvcjogI0EzMUU3OTtcbiAgLS1ib3JkZXItY29sb3I6I0EzMUU3OTtcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi5jaG9sZXN0ZXJvbC1idXR0b24tYWN0aXZhdGV7XG4gIC0tYmFja2dyb3VuZDogI0EzMUU3OTtcbiAgLS1jb2xvcjogI2ZmZjtcbiAgLS1ib3JkZXItY29sb3I6I0EzMUU3OTtcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi5wY29zLWJ1dHRvbi1jYW5jZWx7XG4gIC0tY29sb3I6ICNGRjVBN0Q7XG4gIC0tYm9yZGVyLWNvbG9yOiNGRjVBN0Q7XG4gIC0tYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4ucGNvcy1idXR0b24tYWN0aXZhdGV7XG4gIC0tYmFja2dyb3VuZDogI0ZGNUE3RDtcbiAgLS1jb2xvcjogI2ZmZjtcbiAgLS1ib3JkZXItY29sb3I6I0ZGNUE3RDtcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi5tdXNjbGUtYnV0dG9uLWNhbmNlbHtcbiAgLS1jb2xvcjogIzBCOTRDMTtcbiAgLS1ib3JkZXItY29sb3I6IzBCOTRDMTtcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi5tdXNjbGUtYnV0dG9uLWFjdGl2YXRle1xuICAtLWJhY2tncm91bmQ6ICMwQjk0QzE7XG4gIC0tY29sb3I6ICNmZmY7XG4gIC0tYm9yZGVyLWNvbG9yOiMwQjk0QzE7XG4gIC0tYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4uZmF0LWJ1dHRvbi1jYW5jZWx7XG4gIC0tY29sb3I6ICNGRDk4MEY7XG4gIC0tYm9yZGVyLWNvbG9yOiNGRDk4MEY7XG4gIC0tYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xufVxuXG4uZmF0LWJ1dHRvbi1hY3RpdmF0ZXtcbiAgLS1iYWNrZ3JvdW5kOiAjRkQ5ODBGO1xuICAtLWNvbG9yOiAjZmZmO1xuICAtLWJvcmRlci1jb2xvcjojRkQ5ODBGO1xuICAtLWJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiAxMDBweDtcbn1cblxuLmh5cGVydGVuc2lvbi1idXR0b24tY2FuY2Vse1xuICAtLWNvbG9yOiAjRkY1RDU2O1xuICAtLWJvcmRlci1jb2xvcjojRkY1RDU2O1xuICAtLWJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBib3JkZXItcmFkaXVzOiAxMDBweDtcbn1cblxuLmh5cGVydGVuc2lvbi1idXR0b24tYWN0aXZhdGV7XG4gIC0tYmFja2dyb3VuZDogI0ZGNUQ1NjtcbiAgLS1jb2xvcjogI2ZmZjtcbiAgLS1ib3JkZXItY29sb3I6I0ZGNUQ1NjtcbiAgLS1ib3JkZXItcmFkaXVzOiAxMDBweDtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG59XG5cbi5jbG9zZXtcbiAgcmlnaHQ6IDA7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZm9udC1zaXplOiAycmVtO1xuICBjb2xvcjogIzdkN2Q3ZDtcbiAgei1pbmRleDogMTAwMDAwO1xuICB0b3A6IDBweDtcbn1cblxuLnRleHQtdGl0bGV7XG4gIC8vIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBmb250LXNpemU6IDAuOTI1cmVtO1xufVxuXG4udGV4dC1kZXNje1xuICBmb250LXNpemU6IDAuOTI1cmVtO1xufVxuXG4udGV4dC1xdW90ZXtcbiAgZm9udC1zaXplOiAwLjkyNXJlbTtcbiAgZm9udC1zdHlsZTogaXRhbGljO1xufVxuXG4udGV4dC1kZXNjLW5vdGV7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGZvbnQtc3R5bGU6IGl0YWxpYztcbiAgY29sb3I6ZGFya2dyYXlcbn1cblxuLmJvcmRlci10b3B7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiA3cHg7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW4tdG9wOiAtMTVweDtcbn1cblxuLmJvcmRlci1zZWN0aW9uLWJ0bXtcbiAgYm9yZGVyLXJhZGl1czogMTAwcHg7XG4gIHdpZHRoOiA4OCU7XG4gIGhlaWdodDogN3B4O1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLWxlZnQ6IDYlO1xufVxuXG5pb24tYnV0dG9ue1xuICBtYXgtaGVpZ2h0OiAyLjM1cmVtO1xuICBmb250LXNpemU6IDFyZW07XG59XG5cbi5zdWItZm9vZEl0ZW0tc3ViLWhlYWRlci1jdXN0b217XG4gIGZvbnQtc2l6ZTogMC45MjVyZW07XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubWFpbi1zZWMge1xuICBtYXgtaGVpZ2h0OiA5MHZoO1xuICAucGxhbi1jb250YWluZXIge1xuICAgIC5ib3R0b20tYnRue1xuICAgICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgICAgYm90dG9tOiAwcHg7XG4gICAgICBiYWNrZ3JvdW5kOiB3aGl0ZTtcbiAgICAgIHdpZHRoOiA4NSU7XG4gICAgICBwYWRkaW5nOiAxMHB4IDBweDtcbiAgICB9XG4gIH1cblxuICAucGxhbi1jb250YWluZXI6Oi13ZWJraXQtc2Nyb2xsYmFyIHtcbiAgICBkaXNwbGF5OiBub25lO1xuICB9XG59IiwiQGltcG9ydCBcIi4uLy4uL2Fzc2V0cy9mb250cy9mb250cy5zY3NzXCI7XG5cbi8vIEZvbnRzXG4kZm9udC1zaXplOiAxNnB4O1xuXG4kZm9udDogXCJndC1hbWVyaWNhLXN0YW5kYXJkLXJlZ3VsYXJcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiRmb250LWJvbGQ6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1ib2xkXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4kZm9udC1saWdodDogXCJndC1hbWVyaWNhLXN0YW5kYXJkLWxpZ2h0XCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4kZm9udC1tZWRpdW06IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1tZWRpdW1cIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiRmb250LW1lZGl1bS1leHRlbmRlZDogXCJndC1hbWVyaWNhLXN0YW5kYXJkLWV4dGVuZGVkTWVkaXVtXCIsIFwiSGVsdmV0aWNhIE5ldWVcIixcbiAgQXJpYWwsIHNhbnMtc2VyaWY7XG5cbiAgJHNhbnMtZm9udC1yZWd1bGFyOiBcIlNvdXJjZS1TYW5zLVByby1yYWd1bGFyXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7Il19 */";

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(90271)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map