"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_summary-lead_summary-lead_module_ts"],{

/***/ 82159:
/*!*************************************************************!*\
  !*** ./src/app/summary-lead/summary-lead-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SummaryLeadPageRoutingModule": () => (/* binding */ SummaryLeadPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _summary_lead_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./summary-lead.page */ 23297);




const routes = [
    {
        path: '',
        component: _summary_lead_page__WEBPACK_IMPORTED_MODULE_0__.SummaryLeadPage
    }
];
let SummaryLeadPageRoutingModule = class SummaryLeadPageRoutingModule {
};
SummaryLeadPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SummaryLeadPageRoutingModule);



/***/ }),

/***/ 18690:
/*!*****************************************************!*\
  !*** ./src/app/summary-lead/summary-lead.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SummaryLeadPageModule": () => (/* binding */ SummaryLeadPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _summary_lead_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./summary-lead-routing.module */ 82159);
/* harmony import */ var _summary_lead_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./summary-lead.page */ 23297);







let SummaryLeadPageModule = class SummaryLeadPageModule {
};
SummaryLeadPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _summary_lead_routing_module__WEBPACK_IMPORTED_MODULE_0__.SummaryLeadPageRoutingModule
        ],
        declarations: [_summary_lead_page__WEBPACK_IMPORTED_MODULE_1__.SummaryLeadPage]
    })
], SummaryLeadPageModule);



/***/ }),

/***/ 23297:
/*!***************************************************!*\
  !*** ./src/app/summary-lead/summary-lead.page.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SummaryLeadPage": () => (/* binding */ SummaryLeadPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_summary_lead_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./summary-lead.page.html */ 40524);
/* harmony import */ var _summary_lead_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./summary-lead.page.scss */ 82396);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services */ 65190);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 13252);






let SummaryLeadPage = class SummaryLeadPage {
    constructor(router, storageService, appService) {
        this.router = router;
        this.storageService = storageService;
        this.appService = appService;
        this.segment = '0';
        // summaryData = {
        //   subOptionTypes:[],
        //   statusTypes:[],
        //   optionTypes:[]
        // }
        this.optionTypes = [];
        this.totalOptions = 0;
        this.subOptionTypes = [];
        this.totalSubOptions = 0;
        this.statusTypes = [];
        this.totalStatus = 0;
        this.leadTypes = [];
        this.totalLead = 0;
        this.totalMainOptions = [];
    }
    ngOnInit() { }
    ionViewWillEnter() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            console.log("Summary page");
            //  this.storageService.get("refs").then((val) => {
            yield this.appService.refs().subscribe((response) => {
                console.log("test-refs", response);
                if (response) {
                    this.refsData = response;
                }
            }, (error) => {
                // this.loaderService.hideLoader();
                console.log("Category Error", error);
            });
            yield this.getSummaryLeads();
            //  });
        });
    }
    segmentChanged() {
        // this.slider.slideTo(this.segment);
    }
    rersetValues() {
        this.optionTypes = [];
        this.totalOptions = 0;
        this.subOptionTypes = [];
        this.totalSubOptions = 0;
        this.statusTypes = [];
        this.totalStatus = 0;
        this.leadTypes = [];
        this.totalLead = 0;
        this.totalMainOptions = [];
    }
    getSummaryLeads() {
        this.appService.leadSummary().subscribe((response) => {
            if (response) {
                console.log("ref ", this.refsData);
                console.log("leadSummary ", response);
                this.rersetValues();
                this.myLeads =
                    response &&
                        response.myLeads &&
                        response.myLeads[0] &&
                        response.myLeads[0].myLeadsCount
                        ? response.myLeads[0].myLeadsCount
                        : 0;
                this.hotLeads =
                    response &&
                        response.paidConversions &&
                        response.paidConversions[0] &&
                        response.paidConversions[0].paidConversionCount
                        ? response.paidConversions[0].paidConversionCount
                        : 0;
                // this.totalOptions = 0;
                // this.totalSubOptions = 0;
                // this.totalStatus = 0;
                // this.totalLead = 0;
                response.option.forEach((ele) => {
                    this.refsData.optionTypes.forEach((ele1) => {
                        if (ele1.code == ele._id) {
                            let obj = {};
                            obj["name"] = ele1["name"];
                            obj["code"] = ele1["code"];
                            obj["count"] = ele["optionCount"];
                            this.totalOptions = this.totalOptions + ele["optionCount"];
                            this.optionTypes.push(obj);
                        }
                    });
                });
                response.subOption.forEach((ele) => {
                    this.refsData.subOptionTypes.forEach((ele1) => {
                        if (ele1.code == ele._id) {
                            let obj = {};
                            obj["name"] = ele1["name"];
                            obj["code"] = ele1["code"];
                            obj["count"] = ele["subOptionCount"];
                            this.totalSubOptions =
                                this.totalSubOptions + ele["subOptionCount"];
                            this.subOptionTypes.push(obj);
                        }
                    });
                });
                this.optionTypes.forEach((ele, index) => {
                    let obj = {
                        title: ele["name"],
                        count: ele["count"],
                        subOptions: [],
                    };
                    this.subOptionTypes.forEach((ele1, index1) => {
                        let subCode = ele1["code"].split(".")[0];
                        if (subCode == ele["code"]) {
                            obj["subOptions"].push(ele1);
                        }
                    });
                    this.totalMainOptions.push(obj);
                });
                response.status.forEach((ele) => {
                    this.refsData.statusTypes.forEach((ele1) => {
                        if (ele1.code == ele._id) {
                            let obj = {};
                            obj["name"] = ele1["name"];
                            obj["code"] = ele1["code"];
                            obj["count"] = ele["statusCount"];
                            this.totalStatus = this.totalStatus + ele["statusCount"];
                            this.statusTypes.push(obj);
                        }
                    });
                });
                response.leadType.forEach((ele) => {
                    this.refsData.leadTypes.forEach((ele1) => {
                        if (ele1.code == ele._id) {
                            let obj = {};
                            obj["name"] = ele1["name"];
                            obj["code"] = ele1["code"];
                            obj["count"] = ele["leadTypeCount"];
                            this.totalLead = this.totalLead + ele["leadTypeCount"];
                            this.leadTypes.push(obj);
                        }
                    });
                });
            }
        }, (error) => {
            // this.loaderService.hideLoader();
            console.log("Category Error", error);
        });
    }
    goToSearchPage(type, value, code, subOption) {
        let obj = {
            searchQueryType: type,
            searchQueryName: value,
            searchQueryCode: code,
        };
        this.router.navigate(["/search-lead"], {
            queryParams: obj,
        });
    }
};
SummaryLeadPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router },
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.StorageService },
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.AppService }
];
SummaryLeadPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: "app-summary-lead",
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_summary_lead_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_summary_lead_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], SummaryLeadPage);



/***/ }),

/***/ 40524:
/*!********************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/summary-lead/summary-lead.page.html ***!
  \********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-segment\n    scrollable\n    mode=\"md\"\n    (ionChange)=\"segmentChanged()\"\n    [(ngModel)]=\"segment\"\n    color=\"theme\"\n  >\n    <ion-segment-button mode=\"md\" value=\"0\">\n      <p>Leads Summary</p>\n    </ion-segment-button>\n    <ion-segment-button mode=\"md\" value=\"1\">\n      <p>Campaign Data</p>\n    </ion-segment-button>\n  </ion-segment>\n</ion-header>\n<ion-content>\n  <div class=\"ion-padding-20\">\n    <div  *ngIf=\"segment === '0'\">\n      <ion-row class=\"summaryLeadHeaderFont ion-align-items-center\">\n        <ion-col size=\"9\">\n          <ion-label>Leads Types</ion-label>\n        </ion-col>\n        <ion-col class=\"ion-text-center backgroundColor labelBackground\">\n          <ion-label>{{totalLead}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row\n        class=\"ion-padding-top ion-align-items-center\"\n        *ngFor=\"let lead of leadTypes;let ing=index;\"\n        (click)=\"goToSearchPage('leadType',lead.name, lead.code)\"\n      >\n        <ion-col size=\"8.5\" class=\"labelBackground\">\n          <ion-label class=\"labelFont\">{{lead.name}}</ion-label>\n        </ion-col>\n        <ion-col size=\"0.5\"></ion-col>\n        <ion-col class=\"ion-text-center labelBackground\">\n          <ion-label color=\"theme\">{{lead.count}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row class=\"headerFont ion-align-items-center\">\n        <ion-col size=\"9\">\n          <ion-label>Status Types</ion-label>\n        </ion-col>\n        <ion-col class=\"ion-text-center backgroundColor labelBackground\">\n          <ion-label>{{totalStatus}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row\n        class=\"ion-padding-top ion-align-items-center\"\n        *ngFor=\"let lead of statusTypes;let ing=index;\"\n        (click)=\"goToSearchPage('status',lead.name, lead.code)\"\n      >\n        <ion-col size=\"8.5\" class=\"labelBackground\">\n          <ion-label class=\"labelFont\">{{lead.name}}</ion-label>\n        </ion-col>\n        <ion-col size=\"0.5\"></ion-col>\n        <ion-col class=\"ion-text-center labelBackground\">\n          <ion-label color=\"theme\">{{lead.count}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row\n        class=\"headerFont ion-align-items-center\"\n        (click)=\"goToSearchPage('myLeads','myLeads', true)\"\n      >\n        <ion-col size=\"9\">\n          <ion-label>Contacted by me</ion-label>\n        </ion-col>\n        <ion-col class=\"ion-text-center backgroundColor labelBackground\">\n          <ion-label color=\"theme\">{{myLeads}}</ion-label>\n        </ion-col>\n      </ion-row>\n      <ion-row\n        class=\"headerFont ion-align-items-center\"\n        (click)=\"goToSearchPage('paidConversions','paidConversions', true)\"\n      >\n        <ion-col size=\"9\">\n          <ion-label>Hot Leads</ion-label>\n        </ion-col>\n        <ion-col class=\"ion-text-center backgroundColor labelBackground\">\n          <ion-label color=\"theme\">{{hotLeads}}</ion-label>\n        </ion-col>\n      </ion-row>\n    </div>\n    <div *ngIf=\"segment === '1'\">\n      <div *ngFor=\"let leadMain of totalMainOptions;let ing=index;\">\n        <ion-row class=\"summaryLeadHeaderFont ion-align-items-center\">\n          <ion-col size=\"9\">\n            <ion-label>{{leadMain.title}}</ion-label>\n          </ion-col>\n          <ion-col class=\"ion-text-center backgroundColor labelBackground\">\n            <ion-label>{{leadMain.count}}</ion-label>\n          </ion-col>\n        </ion-row>\n        <ion-row\n          class=\"ion-padding-top ion-align-items-center\"\n          *ngFor=\"let lead of leadMain?.subOptions;let ing=index;\"\n          (click)=\"goToSearchPage('subOption',lead.name, lead.code)\"\n        >\n          <ion-col size=\"8.5\" class=\"labelBackground\">\n            <ion-label class=\"labelFont\">{{lead.name}}</ion-label>\n          </ion-col>\n          <ion-col size=\"0.5\"></ion-col>\n          <ion-col class=\"ion-text-center labelBackground\">\n            <ion-label color=\"theme\">{{lead.count}}</ion-label>\n          </ion-col>\n        </ion-row>\n      </div>\n    </div>\n\n    <!-- <ion-row class=\"headerFont ion-align-items-center\">\n      <ion-col size=\"9\">\n        <ion-label >Sub Option Types</ion-label>\n      </ion-col>\n      <ion-col class=\"ion-text-center backgroundColor labelBackground\">\n        <ion-label>{{totalSubOptions}}</ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-padding-top ion-align-items-center\" *ngFor=\"let lead of subOptionTypes;let ing=index;\" (click)=\"goToSearchPage('subOption',lead.name, lead.code)\">\n      <ion-col size=\"8.5\" class=\"labelBackground\">\n        <ion-label class=\"labelFont\">{{lead.name}}</ion-label>\n      </ion-col>\n      <ion-col size=\"0.5\"></ion-col>\n      <ion-col class=\"ion-text-center labelBackground\">\n        <ion-label color=\"theme\">{{lead.count}}</ion-label>\n      </ion-col>\n    </ion-row> -->\n\n    <!-- <ion-row class=\"ion-padding-top ion-align-items-center\">\n      <ion-col size=\"8.5\" class=\"labelBackground\">\n        <ion-label class=\"labelFont\">Need assistance to pay</ion-label>\n      </ion-col>\n      <ion-col size=\"0.5\"></ion-col>\n      <ion-col class=\"ion-text-center labelBackground\">\n        <ion-label color=\"theme\">15</ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-padding-top ion-align-items-center\">\n      <ion-col size=\"8.5\" class=\"labelBackground\">\n        <ion-label class=\"labelFont\" >Please share me QR code to pay by Google pay/Paytm</ion-label>\n      </ion-col>\n      <ion-col size=\"0.5\"></ion-col>\n      <ion-col class=\"ion-text-center labelBackground\">\n        <ion-label color=\"theme\"></ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"headerFont ion-align-items-center\">\n      <ion-col size=\"9\">\n        <ion-label>Not Sure to Pay</ion-label>\n      </ion-col>\n      <ion-col class=\"ion-text-center backgroundColor labelBackground\">\n        <ion-label color=\"theme\">1020</ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-padding-top ion-align-items-center\">\n      <ion-col size=\"8.5\" class=\"labelBackground\">\n        <ion-label class=\"labelFont\">Not sure of results</ion-label>\n      </ion-col>\n      <ion-col size=\"0.5\"></ion-col>\n      <ion-col class=\"ion-text-center labelBackground\">\n        <ion-label color=\"theme\"></ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-padding-top ion-align-items-center\">\n      <ion-col size=\"8.5\" class=\"labelBackground\">\n        <ion-label class=\"labelFont\" >Can not afford</ion-label>\n      </ion-col>\n      <ion-col size=\"0.5\"></ion-col>\n      <ion-col class=\"ion-text-center labelBackground\">\n        <ion-label color=\"theme\"></ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-padding-top ion-align-items-center\">\n      <ion-col size=\"8.5\" class=\"labelBackground\">\n        <ion-label class=\"labelFont\" >Call Me</ion-label>\n      </ion-col>\n      <ion-col size=\"0.5\"></ion-col>\n      <ion-col class=\"ion-text-center labelBackground\">\n        <ion-label color=\"theme\"></ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"headerFont ion-align-items-center\">\n      <ion-col size=\"9\">\n        <ion-label>Not Now</ion-label>\n      </ion-col>\n      <ion-col class=\"ion-text-center backgroundColor labelBackground\">\n        <ion-label color=\"theme\">1020</ion-label>\n      </ion-col>\n    </ion-row>\n    <ion-row class=\"ion-padding-top ion-align-items-center\">\n      <ion-col size=\"8.5\" class=\"labelBackground\">\n        <ion-label class=\"labelFont\" >Not Interested</ion-label>\n      </ion-col>\n      <ion-col size=\"0.5\"></ion-col>\n      <ion-col class=\"ion-text-center labelBackground\">\n        <ion-label color=\"theme\"></ion-label>\n      </ion-col>\n    </ion-row> -->\n  </div>\n</ion-content>\n");

/***/ }),

/***/ 82396:
/*!*****************************************************!*\
  !*** ./src/app/summary-lead/summary-lead.page.scss ***!
  \*****************************************************/
/***/ ((module) => {

module.exports = "ion-content {\n  --background: #F6F7FC;\n}\n\n.labelBackground {\n  background: #FFFFFF;\n  border: #8C8E93;\n  border-radius: 15px;\n  width: 74px;\n  padding: 15px;\n}\n\n.labelFont {\n  font-size: 14px;\n}\n\n.summaryLeadHeaderFont {\n  font-size: 20px;\n  color: var(--ion-color-theme) !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN1bW1hcnktbGVhZC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxxQkFBQTtBQUNKOztBQUNBO0VBRUksbUJBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtBQUNKOztBQUNBO0VBQ0ksZUFBQTtBQUVKOztBQUFBO0VBQ0ksZUFBQTtFQUNBLHdDQUFBO0FBR0oiLCJmaWxlIjoic3VtbWFyeS1sZWFkLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1jb250ZW50IHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNkY3RkM7XG59XG4ubGFiZWxCYWNrZ3JvdW5kIHtcbiAgICAvLyBoZWlnaHQ6IDUzcHg7XG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgICBib3JkZXI6ICM4QzhFOTM7XG4gICAgYm9yZGVyLXJhZGl1czogMTVweDtcbiAgICB3aWR0aDogNzRweDtcbiAgICBwYWRkaW5nOiAxNXB4O1xufVxuLmxhYmVsRm9udHtcbiAgICBmb250LXNpemU6IDE0cHg7XG59XG4uc3VtbWFyeUxlYWRIZWFkZXJGb250IHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci10aGVtZSkgIWltcG9ydGFudDtcbiAgICAvLyBwYWRkaW5nLXRvcDogMThweDtcbiAgfSJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_summary-lead_summary-lead_module_ts.js.map