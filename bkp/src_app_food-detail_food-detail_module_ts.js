"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_food-detail_food-detail_module_ts"],{

/***/ 17482:
/*!***********************************************************!*\
  !*** ./src/app/food-detail/food-detail-routing.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FoodDetailPageRoutingModule": () => (/* binding */ FoodDetailPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _food_detail_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./food-detail.page */ 77041);




const routes = [
    {
        path: '',
        component: _food_detail_page__WEBPACK_IMPORTED_MODULE_0__.FoodDetailPage
    }
];
let FoodDetailPageRoutingModule = class FoodDetailPageRoutingModule {
};
FoodDetailPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], FoodDetailPageRoutingModule);



/***/ }),

/***/ 29972:
/*!***************************************************!*\
  !*** ./src/app/food-detail/food-detail.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FoodDetailPageModule": () => (/* binding */ FoodDetailPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _food_detail_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./food-detail-routing.module */ 17482);
/* harmony import */ var _food_detail_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./food-detail.page */ 77041);







// import { GaugeChartModule } from 'angular-gauge-chart';
let FoodDetailPageModule = class FoodDetailPageModule {
};
FoodDetailPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _food_detail_routing_module__WEBPACK_IMPORTED_MODULE_0__.FoodDetailPageRoutingModule,
        ],
        declarations: [_food_detail_page__WEBPACK_IMPORTED_MODULE_1__.FoodDetailPage],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_3__.NO_ERRORS_SCHEMA, _angular_core__WEBPACK_IMPORTED_MODULE_3__.CUSTOM_ELEMENTS_SCHEMA]
    })
], FoodDetailPageModule);



/***/ }),

/***/ 77041:
/*!*************************************************!*\
  !*** ./src/app/food-detail/food-detail.page.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FoodDetailPage": () => (/* binding */ FoodDetailPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_food_detail_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./food-detail.page.html */ 93388);
/* harmony import */ var _food_detail_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./food-detail.page.scss */ 14199);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ 38198);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/platform-browser */ 86219);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/utility/utilities */ 53533);
/* harmony import */ var _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../core/constants/constants */ 92133);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/storage */ 78713);
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ 3242);











let FoodDetailPage = class FoodDetailPage {
    constructor(appServices, router, route, _sanitizer, utilities, storage, iab) {
        this.appServices = appServices;
        this.router = router;
        this.route = route;
        this._sanitizer = _sanitizer;
        this.utilities = utilities;
        this.storage = storage;
        this.iab = iab;
        this.isV = false;
        this.isNutritionalShow = true;
        this.isopenIngredients = false;
        this.isopenPreparation = false;
        this.isopenOtherOptions = true;
        this.checkLength = 0;
        this.portion = 0.0;
        this.displayNutriValue = 0;
        this.calories = 0;
        this.foodCode = "";
        this.allFood = [];
        this.categoryType = "";
        this.slot = 0;
        this.foodDetails = [];
        this.slideOptsTwo = {
            initialSlide: 1,
            slidesPerView: 2.5,
            loop: false,
            centeredSlides: false,
            spaceBetween: 8
        };
        this.loaded = false;
        this.streamVideo = false;
        this.isOtherOptinsLoaded = false;
        this.filteredItem = [{ portion_unit: '' }];
        this.foodSourceDomain = "";
        this.diseases = [];
        this.recommendedInData = [];
        this.recomendedData = "";
        this.selectedDietPlan = "weightLoss";
        this.canvasWidth = 140;
        // needleValue= 50;
        this.centralLabel = '';
        this.readyForChange = true;
        this.options = {
            hasNeedle: true,
            needleColor: 'black',
            needleUpdateSpeed: 10,
            arcColors: ["#0BB852", "#FFF", "#8BC73C", "#FFF", "#FFAE00", "#FFF", "#FF791F", "#FFF", "#FF2441"],
            arcDelimiters: [14, 15, 29, 30, 49, 50, 74, 75],
            needleStartValue: 0,
        };
        this.fromCalCounter = false;
        this.username = "";
        this.image_URL = '';
        this.copyOfMainData = [];
        this.categoryLength = 0;
        this.segments = [];
        this.activeInd = 0;
        this.xflag = 0;
        this.allOptionData = [];
        this.username = localStorage.getItem("loginEmail");
        this.route.queryParams.subscribe(res => {
            this.foodCode = res.mainCode;
            this.isV = res.isV;
            this.allFood = res.foodCode;
            this.fromCalCounter = res.fromCalCounter;
            this.categoryType = res.category ? res.category : "";
            this.slot = parseInt(res.slot);
            this.portionX = res.portion;
            this.fetchFood(this.foodCode);
            console.log("eeee:- ", res);
        });
    }
    ngOnInit() { }
    ionViewWillEnter() {
        this.selectedDietPlan = _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.selectedDietPlan;
        this.image_URL = _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.image_URL;
        this.isOtherOptinsLoaded = false;
        // this.utilities.logEvent("view_food_details", {})
        //this.utilities.logEvent("DietPlan_06ViewDetails", {})
        this.getOptions();
    }
    openclose() {
        this.isNutritionalShow = !this.isNutritionalShow;
    }
    openIngredients() {
        this.isopenIngredients = !this.isopenIngredients;
    }
    openPreparation() {
        this.isopenPreparation = !this.isopenPreparation;
    }
    openOtherOptions() {
        this.isopenOtherOptions = !this.isopenOtherOptions;
    }
    openLink(url) {
        this.iab.create(url, "_blank", "location=yes");
    }
    fetchFood(foodCode) {
        this.checkLength = JSON.parse(this.allFood).length;
        this.filteredItem = JSON.parse(this.allFood).filter(item => {
            return item.itemCode == this.foodCode;
        });
        if (this.filteredItem.length) {
            this.portion = this.filteredItem[0].portion;
            this.categoryType = this.filteredItem[0].category;
            this.foodSource = this.filteredItem[0].foodSource;
        }
        console.log("filteredItem", this.filteredItem);
        let reqBody = {
            foodId: foodCode,
            country: _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.country,
            date: _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.dietDate
        };
        console.log(_core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.email);
        this.appServices.fetchFood(reqBody, localStorage.getItem('email')).subscribe(res => {
            var _a, _b;
            this.foodDetails = JSON.parse(JSON.stringify(res)).dietItem;
            this.changeMicros();
            if (!this.filteredItem.length) {
                this.portion = this.foodDetails.portion;
                this.foodSource = this.foodDetails.foodSource;
            }
            console.log("fetchFood Response:", res);
            if (this.foodDetails && this.foodDetails.nutriScore) {
                this.displayNutriValue = this.foodDetails.nutriScore;
                let nutriScore = 100 - this.foodDetails.nutriScore;
                if (nutriScore >= 95) {
                    this.nutriScore = 95;
                }
                else if (nutriScore <= 5) {
                    this.nutriScore = 5;
                }
                else {
                    this.nutriScore = nutriScore;
                }
            }
            this.calories = this.foodDetails.Calories;
            if (this.foodDetails && this.foodDetails.source && this.foodDetails.source != "-" && this.foodDetails.source != "--") {
                this.foodSourceDomain = this.foodDetails.source.split("//")[1].split("/")[0] + "/..";
            }
            if (this.foodDetails && this.foodDetails.video && this.foodDetails.video != "-" && this.foodDetails.video != "--") {
                this.foodDetails.video = this.foodDetails.video
                    .toString()
                    .replace('"', "")
                    .replace('"', "");
            }
            this.senitizedData(this.foodDetails.video);
            if (this.foodDetails.steps != null) {
                this.foodDetails.steps = (_a = this.foodDetails.steps) === null || _a === void 0 ? void 0 : _a.replace(/\n/g, '<br>').trim();
            }
            if (this.foodDetails.steps == "") {
                this.foodDetails.steps = "~";
            }
            if (this.foodDetails.recipe != null) {
                this.foodDetails.recipe = (_b = this.foodDetails.recipe) === null || _b === void 0 ? void 0 : _b.replace(/\n/g, '<br>').trim();
            }
            if (this.foodDetails.recipe == "") {
                this.foodDetails.recipe = "~";
            }
            this.checkDieses();
            // this.portion = this.foodDetails.portion;
        }, err => {
            console.log("fetchFood error:", err);
        });
    }
    videoClick(data) {
        this.videoUrl = this._sanitizer.bypassSecurityTrustResourceUrl(data);
        this.streamVideo = true;
    }
    closeVideo() {
        this.streamVideo = false;
    }
    checkDieses() {
        this.storage.get("localData").then(val => {
            console.log("profile pic", val);
            if (val != "") {
                let data = JSON.parse(val);
                this.diseases = data.otherMaster.diseases.filter(item => {
                    return item.isSelected == true;
                });
                this.diseases.forEach((ele) => {
                    ele.value;
                });
                this.recommendedInData = [];
                this.foodDetails.recommendedIn.forEach((ele) => {
                    if (ele && ele != '') {
                        let temp = this.diseases.filter(ele1 => {
                            return ele1.value == ele;
                        });
                        if (temp && temp.length > 0) {
                            this.recommendedInData.push({ "isExistingDieases": true, "value": ele });
                        }
                        else {
                            this.recommendedInData.push({ "isExistingDieases": false, "value": ele });
                        }
                    }
                });
                console.log("recommendedInData ", this.recommendedInData);
            }
        });
    }
    fetchFoodSingleItem(foodCode) {
        this.filteredItem = [];
        this.filteredItem.push(foodCode);
        this.filteredItem[0].portion_unit = "";
        this.portion = parseFloat(this.filteredItem[0].portion);
        this.categoryType = this.filteredItem[0].category;
        console.log("filteredItem", this.filteredItem);
        this.allFood = JSON.parse(this.allFood);
        let newArray = this.allFood.filter(item => {
            return item.itemCode != this.foodCode;
        });
        newArray.push(this.filteredItem[0]);
        this.allFood = JSON.stringify(newArray);
        this.foodCode = this.filteredItem[0].itemCode;
        this.foodSource = this.filteredItem[0].foodSource;
        let reqBody = {
            foodId: this.filteredItem[0].itemCode,
            country: _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.country
        };
        this.appServices.fetchFood(reqBody, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.email).subscribe(res => {
            this.foodDetails = JSON.parse(JSON.stringify(res)).dietItem;
            console.log("fetchFood Response:", res);
            if (this.foodDetails && this.foodDetails.nutriScore) {
                let nutriScore = 100 - this.foodDetails.nutriScore;
                if (nutriScore >= 95) {
                    this.nutriScore = 95;
                }
                else if (nutriScore <= 5) {
                    this.nutriScore = 5;
                }
                else {
                    this.nutriScore = nutriScore;
                }
            }
            this.calories = this.foodDetails.Calories;
            if (this.foodDetails.video && this.foodDetails.video != "-") {
                this.foodDetails.video = this.foodDetails.video
                    .toString()
                    .replace('"', "")
                    .replace('"', "");
            }
            this.senitizedData(this.foodDetails.video);
            if (this.foodDetails.steps != null) {
                this.foodDetails.steps = this.foodDetails.steps.trim();
            }
            if (this.foodDetails.steps == "") {
                this.foodDetails.steps = "~";
            }
            if (this.foodDetails.recipe != null) {
                this.foodDetails.recipe = this.foodDetails.recipe.trim();
            }
            if (this.foodDetails.recipe == "") {
                this.foodDetails.recipe = "~";
            }
            this.checkDieses();
            // this.portion = this.foodDetails.portion;
        }, err => {
            console.log("fetchFood error:", err);
        });
    }
    senitizedData(videoUrl) {
        console.log("videoUrls", videoUrl);
        this.videoUrl = this._sanitizer.bypassSecurityTrustResourceUrl(videoUrl);
    }
    changeMicros() {
        this.totalCarbs = Math.floor(parseFloat(this.foodDetails.Carbs) * this.portion);
        this.totalProtein = Math.floor(parseFloat(this.foodDetails.Protien) * this.portion);
        this.totalFat = Math.floor(parseFloat(this.foodDetails.Fat) * this.portion);
        this.totalFiber = Math.floor(parseFloat(this.foodDetails.Fiber) * this.portion);
    }
    plus() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            if (this.portion <= 20) {
                this.portion = (yield this.portion) + 0.5;
                yield this.changeMicros();
            }
        });
    }
    minus() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            if (this.portion > 0.5) {
                this.portion = (yield this.portion) - 0.5;
                yield this.changeMicros();
            }
        });
    }
    setPortionZeroAndDelete() {
        this.portion = 0;
        this.update();
    }
    update() {
        this.utilities.presentLoading();
        //  this.utilities.logEvent("DietPlan_07aUpdateFromDetails", {})
        let data = JSON.parse(this.allFood);
        let foodCodeList = [];
        let code = "";
        let portions = "";
        console.log("data", data);
        let filteredData = data.filter(item => {
            return item.itemCode == this.foodCode;
        });
        if (filteredData.length) {
            filteredData[0].portion = this.portion;
        }
        for (let index = 0; index < data.length; index++) {
            if (data.length > 1 && index < data.length - 1) {
                if (data[index].foodSource.toLowerCase() == "external" || data[index].foodSource.toLowerCase() == "personal") {
                    foodCodeList.push(data[index]);
                }
                else if (filteredData.length && filteredData[0].itemCode == data[index].code) {
                    // code = code + data[index].code + ",";
                    // portions = portions + this.portion + ",";
                    if (this.portion != 0) {
                        foodCodeList.push({ code: data[index].code, portion: this.portion, "foodSource": data[index]["foodSource"] ? data[index]["foodSource"] : "INTERNAL", "eaten": data[index].eaten ? data[index].eaten : -1 });
                    }
                }
                else {
                    // code = code + data[index].code + ",";
                    // portions = portions + data[index].portion + ",";
                    foodCodeList.push({ code: data[index].code, portion: parseFloat(data[index].portion), "foodSource": data[index]["foodSource"] ? data[index]["foodSource"] : "INTERNAL", "eaten": data[index].eaten ? data[index].eaten : -1 });
                }
            }
            else {
                // code = code + data[index].code;
                if (data[index] && data[index].foodSource && data[index].foodSource.toLowerCase() == "external") {
                    foodCodeList.push(data[index]);
                }
                else if (filteredData.length && filteredData[0].itemCode == data[index].code) {
                    //  portions = portions + this.portion;
                    if (this.portion != 0) {
                        foodCodeList.push({ code: data[index].code, portion: this.portion, "foodSource": data[index]["foodSource"] ? data[index]["foodSource"] : "INTERNAL", "eaten": data[index].eaten ? data[index].eaten : -1 });
                    }
                }
                else {
                    // portions = portions + data[index].portion;
                    foodCodeList.push({ code: data[index].code, portion: parseFloat(data[index].portion), "foodSource": data[index]["foodSource"] ? data[index]["foodSource"] : "INTERNAL", "eaten": data[index].eaten ? data[index].eaten : -1 });
                }
            }
        }
        if (!filteredData.length) {
            foodCodeList.push({ code: this.foodCode, portion: this.portion, "foodSource": this.foodDetails.foodSource ? this.foodDetails.foodSource : "INTERNAL", "eaten": -1 });
        }
        setTimeout(() => {
            console.log("portions", portions);
            console.log("code", code);
            let reqBody = {
                foodCodeList,
                slot: this.slot,
                detox: _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.isDetox,
                date: _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.dietDate,
                country: _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.country
            };
            console.log("reqBody", reqBody);
            //  this.utilities.logEvent("update_food_details", reqBody);
            this.appServices.postOptionFoodList(reqBody, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.email).then(success => {
                console.log("detail", success);
                this.updateCurrentDietPlan();
            }, err => {
                this.utilities.hideLoader();
                console.log("details error", err);
            });
        }, 2000);
    }
    updateCurrentDietPlan() {
        this.utilities.showLoading();
        this.appServices.getDietPlans(_core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.isDetox, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.dietDate, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.country, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.defaultCalories).then(success => {
            this.fetchDiet(_core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.isDetox, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.dietDate, success["dietplan"]);
        });
    }
    fetchDiet(isDetox, date, success) {
        console.log("------------- fetchDietPlan---------", success);
        let self = this;
        self.appServices.getDietPlans(isDetox, date, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.country, "", false).then(success => {
            self.storage.get("dietData").then((res) => {
                if (res) {
                    res[date] = res[date] ? res[date] : {};
                    res[date][_core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.selectedDietPlan] = success;
                    self.storage.set("dietData", res);
                    self.utilities.hideLoader();
                    if (this.isV) {
                        if (this.username !== 'beato') {
                            self.router.navigate(["/tabs/consume"], { queryParams: { params: this.slot } }).then(() => {
                                window.location.reload();
                            });
                        }
                        else {
                            self.router.navigate(["/consume"], { queryParams: { params: this.slot } }).then(() => {
                                window.location.reload();
                            });
                        }
                    }
                    else {
                        if (this.username !== 'beato') {
                            self.router.navigate(["/tabs/consume"], { queryParams: { params: this.slot } }).then(() => {
                                window.location.reload();
                            });
                        }
                        else {
                            self.router.navigate(["/consume"], { queryParams: { params: this.slot } }).then(() => {
                                window.location.reload();
                            });
                        }
                    }
                    console.log("detail", success);
                }
            });
        });
    }
    cancel() {
        if (this.fromCalCounter)
            this.router.navigate(['todays-calorie-count']);
        else {
            if (this.isV) {
                if (this.username !== 'beato') {
                    this.router.navigate(["/tabs/consume"], { queryParams: { params: this.slot } });
                }
                else {
                    this.router.navigate(["consume"], { queryParams: { params: this.slot } });
                }
            }
            else {
                if (this.username !== 'beato') {
                    this.router.navigate(["/tabs/consume"]);
                }
                else {
                    this.router.navigate(["/consume"]);
                }
            }
        }
    }
    slidePrev() {
        this.slides.slidePrev();
    }
    slideNext() {
        this.slides.slideNext();
    }
    refresh() {
        let filter = this.allOptionData.filter(o => o.code != this.foodCode);
        let random = filter[Math.floor(Math.random() * filter.length)];
        this.fetchFoodSingleItem(random);
    }
    getOptions() {
        this.appServices.getOptions(this.slot, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.isDetox, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.country, _core_constants_constants__WEBPACK_IMPORTED_MODULE_4__.CONSTANTS.email).then(success => {
            var _a, _b, _c;
            this.allOptionData = [];
            this.utilities.hideLoader();
            this.optionData = JSON.parse(JSON.stringify(success));
            this.copyOfMainData = this.optionData;
            console.log("optionData:-", this.optionData);
            for (let index = 0; index < ((_b = (_a = this.optionData) === null || _a === void 0 ? void 0 : _a.mealOptions) === null || _b === void 0 ? void 0 : _b.length); index++) {
                for (let j = 0; j < ((_c = this.optionData.mealOptions[index]) === null || _c === void 0 ? void 0 : _c.categories.length); j++) {
                    for (let k = 0; k < this.optionData.mealOptions[index].categories[j].food.length; k++) {
                        console.log("this.catetgoryType", this.categoryType);
                        console.log("this.catetgoryType1", this.optionData.mealOptions[index].categories[j].food[k].Type);
                        if (this.categoryType.charAt(0) == this.optionData.mealOptions[index].categories[j].food[k].Type.charAt(0)) {
                            this.allOptionData.push(this.optionData.mealOptions[index].categories[j].food[k]);
                        }
                    }
                }
            }
            this.isOtherOptinsLoaded = true;
            console.log("allOptionData:-", this.allOptionData);
            //     this.segments = [];
            //     console.log("this.optionData.mealOptions.length",this.optionData.mealOptions.length);
            //     if(this.optionData.mealOptions.length==1){
            //       this.xflag=0;
            //     }
            //     for (
            //       let index = 0;
            //       index < this.optionData.mealOptions.length;
            //       index++
            //     ) {
            //       if (this.optionData.mealOptions[index].isCategory) {
            //         for (
            //           let j = 0;
            //           j < this.optionData.mealOptions[index].categories.length;
            //           j++
            //         ) {
            //           for (
            //             let fk = 0;
            //             fk <
            //             this.optionData.mealOptions[index].categories[j].food.length;
            //             fk++
            //           ) {
            //             this.optionData.mealOptions[index].categories[j].food[
            //               fk
            //             ].selected = "false";
            //           }
            //           if (
            //             this.optionData.mealOptions[index].categories[j].food.length > 0
            //           ) {
            //             this.segments.push({
            //               optionName: this.optionData.mealOptions[index].optionName
            //             });
            //             break;
            //           } else {
            //             continue;
            //           }
            //         }
            //         for (
            //           let j = 0;
            //           j < this.optionData.mealOptions[index].categories.length;
            //           j++
            //         ) {
            //           if (
            //             this.optionData.mealOptions[index].categories[j].food.length > 0
            //           ) {
            //           }
            //         }
            //       } 
            //       else if (!this.optionData.mealOptions[index].isCategory) {
            //         if (this.optionData.mealOptions[index].food.length > 0) {
            //           for (
            //             let fk = 0;
            //             fk < this.optionData.mealOptions[index].food.length;
            //             fk++
            //           ) {
            //             this.optionData.mealOptions[index].food[fk].selected = "false";
            //           }
            //          this.segments.push({
            //             optionName: this.optionData.mealOptions[index].optionName
            //           });
            //         }
            //       }
            //       if (this.optionData.mealOptions[index].isCategory) {
            //         for (
            //           let j = 0;
            //           j < this.optionData.mealOptions[index].categories.length;
            //           j++
            //         ) {
            //           this.optionData.mealOptions[index].categories[j].isOk = true;
            //         }
            //       }
            //     }
            //     for (let index = 0; index < this.optionData.mealOptions[this.activeInd].categories.length; index++) {
            //       this.categoryLength = this.optionData.mealOptions[this.activeInd].categories.length;
            //       let data1 = this.optionData.mealOptions[this.activeInd].categories[index].food.filter(item=>{
            //          return item.recommendedFor!=undefined;
            //        });
            //        if(data1.length>0){
            //          this.flagIndexforHealth=this.flagIndexforHealth+1;
            //          break;
            //        }
            //      }
        }, err => {
            this.utilities.hideLoader();
        });
    }
    imageLoad() {
        this.loaded = true;
    }
    imageError() {
        this.loaded = true;
    }
    getColorAndText() {
        // if ((this.nutriScore >= 0 && this.nutriScore < 25)) {
        //   return { color: "#0BB852", text: "It is an excellent choice", text2: this.calories > 1.5 && this.foodSource.toLowerCase() != "internal" ? "But smaller portion is recommended" : "" };
        // } else if (this.nutriScore >= 25 && this.nutriScore < 50) {
        //   return { color: "#8BC73C", text: "It is a good Choice", text2: this.calories > 1.5 && this.foodSource.toLowerCase() != "internal" ? "But smaller portion is recommended" : "" };
        // } else if (this.nutriScore >= 50 && this.nutriScore < 65) {
        //   return { color: "#FFAE00", text: "it is an average choice.", text2: "Should be taken in moderation" };
        // } else if (this.nutriScore >= 65 && this.nutriScore < 80) {
        //   return { color: "#FF791F", text: "It is not a good choice. ", text2: "Can be taken in small portions" };
        // } else if (this.nutriScore >= 80) {
        //   return { color: "#FF2441", text: "It should be avoided.", text2: "Can be taken in small portions sometimes" };
        // }
        const nutriScore = this.displayNutriValue;
        if ((nutriScore >= 85)) {
            return { color: "#0BB852", text: "It is an excellent choice", text2: this.calories > 1.5 && this.foodSource.toLowerCase().toLowerCase() != "internal" ? "But smaller portion is recommended" : "" };
        }
        else if (nutriScore >= 70 && nutriScore < 85) {
            return { color: "#8BC73C", text: "It is a good Choice", text2: this.calories > 1.5 && this.foodSource.toLowerCase().toLowerCase() != "internal" ? "But smaller portion is recommended" : "" };
        }
        else if (nutriScore >= 50 && nutriScore < 70) {
            return { color: "#FFAE00", text: "it is an average choice.", text2: "Should be taken in moderation" };
        }
        else if (nutriScore >= 25 && nutriScore < 50) {
            return { color: "#FF791F", text: "It is not a good choice. ", text2: "Can be taken in small portions" };
        }
        else if (nutriScore < 25) {
            return { color: "#FF2441", text: "It should be avoided.", text2: "Can be taken in small portions sometimes" };
        }
    }
};
FoodDetailPage.ctorParameters = () => [
    { type: _app_service__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.ActivatedRoute },
    { type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_8__.DomSanitizer },
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_3__.UTILITIES },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_9__.Storage },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_5__.InAppBrowser }
];
FoodDetailPage.propDecorators = {
    slides: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_10__.ViewChild, args: ["Slides", { static: false },] }]
};
FoodDetailPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: "app-food-detail",
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_food_detail_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_food_detail_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], FoodDetailPage);



/***/ }),

/***/ 93388:
/*!******************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/food-detail/food-detail.page.html ***!
  \******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header *ngIf=\"!streamVideo\" style=\"height: 3rem !important;\">\n  <ion-toolbar  style=\"--min-height: 3rem !important;\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-header>\n<ion-content *ngIf=\"!streamVideo\">\n  <div>\n    <ion-card>\n      <div>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <div (click)=\"videoClick(foodDetails.video)\"\n              *ngIf=\"foodDetails.video && foodDetails.video != '-' && foodDetails.video != '--' && foodDetails.video.indexOf('http') > -1\">\n              <iframe [src]='videoUrl' frameborder=\"0\" allowfullscreen\n                style=\"width: 100%;height: 200px;pointer-events: none;\"></iframe>\n            </div>\n\n            <img style=\"height: 200px;width: 100%;object-fit: cover;\"\n              *ngIf=\"foodDetails.imageUrl && !(foodDetails.video && foodDetails.video != '-' && foodDetails.video != '--' && foodDetails.video.indexOf('http') > -1)\"\n              src=\"{{image_URL}}{{foodDetails.foodItem}}.jpg\">\n              <ion-row style=\"position: absolute;\n              bottom: 16px;right: 10px;\" *ngIf=\"foodDetails?.source && foodDetails?.source != ''\">\n                <ion-col>\n                  <div style=\" height: 50px;\n                  width: 50px;\n                  background: rgb(255 255 255 / 80%);\n                  border-radius: 100px;\">\n                    <img src=\"./assets/img/link.svg\" style=\"margin-top: 13px;\n                    opacity: 1;\n                    height: 3vh;\" />\n                  </div>\n                </ion-col>\n              </ion-row>\n          </ion-col>\n        </ion-row>\n       \n        <ion-row *ngIf=\"foodDetails?.source || foodDetails.video && foodDetails.video != '-' && foodDetails.video != '--' && foodDetails.video.indexOf('http') > -1\">\n          <ion-col class=\"ion-text-center\" style=\"font-size:0.6rem;\">\n            Not must to follow  Recipe. Is shared to guide you for better way of cooking\n          </ion-col>\n        </ion-row>\n        <ion-row style=\"bottom: 16px;\">\n          <ion-col size=\"0.4\"></ion-col>\n          <ion-col class=\"ion-text-left ion-align-self-start\">\n            <ion-item lines=\"none\" class=\"legend-top\" [ngClass]=\"{'legend-9':foodDetails.Score =='9', 'legend-6':foodDetails.Score =='6', 'legend-3': foodDetails.Score =='3' , 'legend-1':foodDetails.Score =='1' , 'legend-0': !foodDetails.Score || foodDetails.Score == ''}\">\n              <span *ngIf=\"foodDetails.video!=null && foodDetails.video!=''\"\n              style=\"font-weight: bold;font-size: 1rem;\">{{foodDetails.Name}}\n              ({{foodDetails.Calories}} cal/ {{filteredItem[0]?.portion_unit}})</span>\n            <span *ngIf=\"foodDetails.video==null || foodDetails.video==''\"\n              style=\"font-weight: bold;font-size: 1rem;\">{{foodDetails.Name}}\n              ({{foodDetails.Calories}} cal/ {{filteredItem[0]?.portion_unit}})</span>\n            <span class=\"first-caps\"\n              *ngIf=\"foodDetails.video!=null || foodDetails.video==''\">{{foodDetails.courtesy}}</span>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf=\"recommendedInData && recommendedInData.length > 0\">\n          <ion-col class=\"ion-text-left\" style=\"font-size:0.7rem;margin: 10px;color: #03a5a7;\">\n            <img src=\"./assets/img/health1.svg\" class=\"pic-food-heart-healthy\" /> This is good in <span *ngFor=\"let item of recommendedInData; let i = index;\" [ngStyle]=\"{'font-weight' : item.isExistingDieases ? 'bold' : 'normal'}\">\n              {{item.value}}<span *ngIf=\"i < recommendedInData.length - 1\">, </span>\n            </span>\n            <!-- <span [ngStyle]=\"{'font-weight' : true ? 'bold' : 'bold'}\">{{item}}</span> -->\n            \n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf=\"selectedDietPlan == 'immunity_booster' && foodDetails.Score == '9'\">\n          <ion-col class=\"ion-text-left\" style=\"font-size:0.7rem;margin: 10px;color: #03a5a7;\">\n            <img src=\"./assets/img/immunity.svg\" class=\"pic-food-heart-healthy\" /> This is good in immunity booster\n            <!-- <span [ngStyle]=\"{'font-weight' : true ? 'bold' : 'bold'}\">{{item}}</span> -->\n            \n          </ion-col>\n        </ion-row>\n\n      </div>\n    </ion-card>\n\n    <div class=\"gauge-chart\" *ngIf=\"foodSource && foodSource.toUpperCase() != 'PERSONAL' &&  nutriScore\">\n      <!-- <ion-row>\n        <ion-col class=\"text-center\">\n          NutriScore: {{100 - nutriScore}}\n        </ion-col>\n      </ion-row> -->\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <ion-card style=\"margin-top: 0;\">\n            <ion-row style=\"height:100%\">\n              <ion-col size=\"5\" style=\"height:100%\">\n                <div\n                  style=\"position: relative;top: 60%;left: 50%;transform: translate(-50%, -50%) !important;display: block !important;\">\n                  <rg-gauge-chart style=\"display: inline-block;\"[canvasWidth]=\"canvasWidth\" [needleValue]=\"nutriScore\" [options]=\"options\">\n                  </rg-gauge-chart>\n                </div>\n              </ion-col>\n              <ion-col size=\"7\" style=\"height:100%\">\n                <div\n                  style=\"color: #7d7d7d; padding: 5px 0; border-width: 2px; border-style: dashed;border-radius: 10px; position: relative;top: 50%;left: 50%;transform: translate(-50%, -50%) !important;display: block !important;\"\n                  [ngStyle]=\"{ 'border-color' : true ? getColorAndText()?.color : getColorAndText()?.color}\">\n                  <div style=\"font-size: 0.8rem; margin-bottom: 2px;\"\n                    [ngStyle]=\"{color: true ? getColorAndText()?.color: getColorAndText()?.color}\">{{getColorAndText()?.text}}</div>\n                  <div style=\"font-size: 0.8rem;\">{{getColorAndText()?.text2}} </div>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-card>\n        </ion-col>\n      </ion-row>\n    </div>\n\n    <div>\n      <div>\n        <ion-card class=\"ion-padding\" style=\"margin-top: 0;\">\n          <ion-row>\n            <ion-col  class=\"ion-text-center\">\n              <div (click)=\"refresh()\" *ngIf=\"allOptionData && allOptionData.length > 0\">\n                <img src=\"./assets/images/refreshIcon.svg\"><br>\n              </div>\n              <!-- <span>Refresh</span> -->\n            </ion-col>\n            <ion-col>\n              <div class=\"portion-entry\">\n\n                <ion-row>\n                  <ion-col size=\"4\" class=\"ion-text-center ion-align-self-center\" (click)=\"minus()\">\n                    <span style=\"    font-size: 1.5rem;\n                      top: -2px;\n                      position: relative;color: #01A3A4;\">-</span>\n                    <!-- <img class=\"plus-minus\" (click)=\"minus()\" src=\"./assets/icons/minus.png\" /> -->\n                  </ion-col>\n                  <ion-col size=\"4\" class=\"text-center ion-align-self-center\" style=\"background: #F6F7FC\">\n                    <ion-input type=\"number\" class=\"input-portion\" [(ngModel)]=\"portion\" ng-pattern=\"/^[0-9]{1,2})?$/\"\n                      min=\"1\" max=\"2\" maxlength=\"1\" readonly=\"true\">\n\n                    </ion-input>\n                  </ion-col>\n                  <ion-col size=\"4\" class=\"ion-text-center ion-align-self-center\" (click)=\"plus()\">\n                    <span style=\"    font-size: 1.5rem;\n                    top: -2px;\n                    position: relative;color: #01A3A4;\">+</span>\n                    <!-- <img class=\"plus-minus\" (click)=\"plus()\" src=\"./assets/icons/plus1.png\" /> -->\n                  </ion-col>\n                </ion-row>\n              </div>\n            </ion-col>\n            <!-- <ion-col class=\"ion-text-center\" style=\"display: grid;\">\n              <span style=\"padding-top:10px;\">{{filteredItem[0]?.portion_unit}}</span>\n            </ion-col> -->\n            <ion-col class=\"ion-text-center\" *ngIf=\"checkLength>=1\">\n              <img src=\"./assets/images/deleteIcon.svg\" (click)=\"setPortionZeroAndDelete()\" class=\"trash\"><br>\n              <!-- <span>Delete</span> -->\n              <!-- <div class=\"trash-container\" *ngIf=\"checkLength>1\">\n                <ion-icon name=\"trash\" (click)=\"setPortionZero()\" class=\"trash\"></ion-icon>\n              </div> -->\n            </ion-col>\n          </ion-row>\n          <div>\n            <ion-row *ngIf=\"filteredItem && filteredItem.length > 0 && filteredItem[0] && filteredItem[0]?.portion_unit\">\n              <ion-col>\n                <ion-item (click)=\"openclose()\" lines=\"none\" class=\"ion-no-padding\" mode=\"ios\">\n\n                  <h2 slot=\"start\"> Nutritional Information <span>(per {{filteredItem[0]?.portion_unit}})</span></h2>\n\n                  <img style=\"width: 30px;\" src=\"./assets/icons/down.png\" slot=\"end\" *ngIf=\"!isNutritionalShow\">\n                  <img style=\"width: 30px;\" src=\"./assets/icons/up.png\" slot=\"end\" *ngIf=\"isNutritionalShow\">\n                </ion-item>\n              </ion-col>\n            </ion-row>\n\n        \n\n            <ion-row *ngIf=\"isNutritionalShow\">\n              <ion-col>\n                <ion-row>\n                  <ion-col class=\"ion-text-center detail1 padding-left-5\">\n                    <span class=\"font-weight700 font-size9\">Carbs</span>\n                    <br>\n                    <img class=\"plus-minus pad-top5\" src=\"./assets/img/carbs-mini.svg\" style=\"height: 4vh;\"/>\n                    <br>\n                    <span class=\"font-size9\">{{totalCarbs}} Gm</span>\n                  </ion-col>\n                  <ion-col class=\"ion-text-center detail1 padding-right-5\">\n                    <span class=\"font-weight700 font-size9\">Proteins </span>\n                    <br>\n                    <img class=\"plus-minus pad-top5\" src=\"./assets/img/protien-mini.svg\" style=\"height: 4vh;\"/>\n                    <br>\n                    <span class=\"font-size9\">{{totalProtein}} Gm</span>\n                  </ion-col>\n                  <ion-col class=\"ion-text-center detail1 padding-left-5\">\n                    <span class=\"font-weight700 font-size9\">Fats</span>\n                    <br>\n                    <img class=\"plus-minus pad-top5\" src=\"./assets/img/fats-mini.svg\" style=\"height: 4vh;\"/>\n                    <br>\n                    <span class=\"font-size9\">{{totalFat}} Gm</span>\n                  </ion-col>\n                  <ion-col class=\"ion-text-center padding-left-5\">\n                    <span class=\"font-weight700 font-size9\">Fiber</span>\n                    <br>\n                    <img class=\"plus-minus pad-top5\" src=\"./assets/img/fiber-mini.svg\" style=\"height: 4vh;\"/>\n                    <br>\n                    <span class=\"font-size9\">{{totalFiber}} Gm</span>\n                  </ion-col>\n                  <!-- <ion-col class=\"ion-text-center padding-left-5\">\n                    <span class=\"font-weight700 font-size9\">Fiber</span>\n                    <br>\n                    <img class=\"plus-minus\" src=\"./assets/img/fiber-mini.svg\" />\n                    <br>\n                    <span class=\"font-size9\">{{foodItem?.Fiber}} Gm</span>\n                  </ion-col> -->\n                </ion-row>\n              </ion-col>\n            </ion-row>\n            <!-- <ion-list lines=\"none\" *ngIf=\"isNutritionalShow\">\n              <ion-item class=\"ion-no-padding\">\n                <ion-avatar slot=\"start\">\n                  <img src=\"./assets/icons/protein.jpg\">\n                </ion-avatar>\n                <ion-label>Protein</ion-label>\n                <span slot=\"end\"> {{foodDetails.Protien}}g</span>\n              </ion-item>\n              <ion-item class=\"ion-no-padding\">\n                <ion-avatar slot=\"start\">\n                  <img src=\"./assets/icons/fats.jpg\">\n                </ion-avatar>\n                <ion-label>Fats</ion-label>\n                <span slot=\"end\"> {{foodDetails.Fat}}g</span>\n              </ion-item>\n              <ion-item class=\"ion-no-padding\">\n                <ion-avatar slot=\"start\">\n                  <img src=\"./assets/icons/carbs.jpg\">\n                </ion-avatar>\n                <ion-label>Carbs</ion-label>\n                <span slot=\"end\"> {{foodDetails.Carbs}}g</span>\n              </ion-item>\n              <ion-item class=\"ion-no-padding\">\n                <ion-avatar slot=\"start\">\n                  <img src=\"./assets/icons/fiber.jpeg\">\n                </ion-avatar>\n                <ion-label>Fiber</ion-label>\n                <span slot=\"end\"> {{foodDetails.Fiber}}g</span>\n              </ion-item>\n  \n            </ion-list> -->\n          </div>\n\n\n          <div *ngIf=\"foodDetails && ((foodDetails.recipe && foodDetails.recipe.length >= 5) || (foodDetails.source && foodDetails.source != ''))\">\n            <ion-row class=\"row-hr\">\n              <ion-col>\n                <ion-item (click)=\"openIngredients()\" lines=\"none\" class=\"ion-no-padding\" mode=\"ios\">\n                  <h2 slot=\"start\"> Ingredients</h2>\n                  <img style=\"width: 30px;\" src=\"./assets/icons/down.png\" slot=\"end\" *ngIf=\"!isopenIngredients\">\n                  <img style=\"width: 30px;\" src=\"./assets/icons/up.png\" slot=\"end\" *ngIf=\"isopenIngredients\">\n                </ion-item>\n              </ion-col>\n            </ion-row>\n\n            <ion-list lines=\"none\" *ngIf=\"isopenIngredients\">\n              <ion-item class=\"ion-no-padding\" style=\"--background: white;\">\n                <ion-label style=\"white-space: normal; background: white;\">\n                  <span *ngIf=\"foodDetails.recipe==null || foodDetails.recipe==''\">---</span>\n                  <span *ngIf=\"foodDetails.source && foodDetails.source != ''\">\n                    Source: <span style=\"color: blue;\"\n                      (click)=\"openLink(foodDetails?.source)\">{{foodSourceDomain}}</span>\n                  </span>\n                  <div *ngIf=\"foodDetails.recipe!='~' && foodDetails.recipe!=null &&  foodDetails.recipe!=''\" style=\"    padding: 0 10px;\n                 width: 100%;;border:none;height:100%; background: white; color: black;\"\n                    readonly=\"readonly\">{{foodDetails.recipe}}</div>\n                </ion-label>\n              </ion-item>\n            </ion-list>\n          </div>\n          <div *ngIf=\"foodDetails && ((foodDetails.steps && foodDetails.steps.length >= 5) || (foodDetails.source && foodDetails.source != ''))\">\n            <ion-row class=\"row-hr\">\n              <ion-col>\n                <ion-item (click)=\"openPreparation()\" lines=\"none\" class=\"ion-no-padding\" mode=\"ios\">\n                  <h2 slot=\"start\"> Preparation Method</h2>\n                  <img style=\"width: 30px;\" src=\"./assets/icons/down.png\" slot=\"end\" *ngIf=\"!isopenPreparation\">\n                  <img style=\"width: 30px;\" src=\"./assets/icons/up.png\" slot=\"end\" *ngIf=\"isopenPreparation\">\n                </ion-item>\n              </ion-col>\n            </ion-row>\n\n            <ion-list lines=\"none\" *ngIf=\"isopenPreparation\">\n              <ion-item class=\"ion-no-padding\" style=\"--background: white;\">\n                <ion-label style=\"white-space: normal; background: white;\">\n                  <!-- <span *ngIf=\"foodDetails.steps==null\">---</span>\n                  <span *ngIf=\"foodDetails.steps==''\">---</span> -->\n                  <span *ngIf=\"foodDetails.steps==null || foodDetails.steps==''\">---</span>\n                  <span *ngIf=\"foodDetails.source && foodDetails.source != ''\">\n                    Source: <span style=\"color: blue;\"\n                      (click)=\"openLink(foodDetails?.source)\">{{foodSourceDomain}}</span>\n                  </span>\n                  <div *ngIf=\"foodDetails.steps!='~' && foodDetails.steps!=null && foodDetails.steps!=''\" style=\"    padding: 0 10px;\n                        width: 100%;border:none;height:100%; background: white; color: black;\"\n                    readonly=\"readonly\">{{foodDetails.steps}} </div>\n                </ion-label>\n              </ion-item>\n            </ion-list>\n          </div>\n        </ion-card>\n\n        <!-- <br/>\n<br/> -->\n      </div>\n    </div>\n    <div>\n      <ion-card>\n        <ion-row *ngIf=\"allOptionData && allOptionData.length > 0\">\n          <ion-col class=\"ion-padding\">\n            <ion-item lines=\"none\" class=\"ion-no-padding\" mode=\"ios\">\n              <h2 slot=\"start\"> Choose other option</h2>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row *ngIf=\"allOptionData && allOptionData.length > 0\">\n          <!-- <ion-col class=\"ion-text-center ion-align-self-center\" size=\"1\" (click)=\"slidePrev()\">\n                  <ion-icon name=\"arrow-back\"></ion-icon> \n          </ion-col> -->\n          <ion-col class=\"ion-text-center\" size=\"12\">\n            <ion-slides #Slides [options]=\"slideOptsTwo\">\n\n              <ion-slide *ngFor=\"let item of allOptionData; let i = index;\"\n              class=\"\" >\n                <!-- <img src=\"./assets/img/health1.svg\" class=\"pic-food-heart\" *ngIf=\"item.recommendedFor?.length>0 || item.Score =='9'\" /> -->\n                <img src=\"./assets/img/health1.svg\" class=\"pic-food-heart\"\n                  *ngIf=\"selectedDietPlan != 'immunity_booster' && item.recommendedFor && item.recommendedFor.length>0\" />\n                <img src=\"./assets/img/immunity.svg\" class=\"pic-food-heart\"\n                  *ngIf=\"selectedDietPlan == 'immunity_booster' && item.Score == '9'\" />\n                <img src=\"./assets/img/health1.svg\" class=\"pic-food-heart\"\n                  *ngIf=\"selectedDietPlan == 'immunity_booster' && item.recommendedFor && item.recommendedFor.length> 0 && item.Score != '9'\" />\n                <!-- width: 99%;\n                height: 120px;\n                border-radius: 15px;\n                background: #f3f3f3; -->\n                <div style=\"\n                top: 11px; height: 165px;\n                position: relative;\">\n                  <ion-spinner [ngClass]=\"{'center':true}\" *ngIf=\"!loaded\"></ion-spinner>\n                  <img style=\"height: 100px;width: 100%;border-radius: 15px;\" src=\"{{image_URL}}{{item.code}}.jpg\"\n                    (load)=\"imageLoad()\" (error)=\"imageError()\" (click)=\"fetchFoodSingleItem(item)\" class=\"legend\" [ngClass]=\"{'legend-9':item.Score =='9', 'legend-6':item.Score =='6', 'legend-3': item.Score =='3', 'legend-1':item.Score =='1', 'legend-0': item.Score == '' || !item.Score}\">\n                  <div class=\"food-name\">{{item.Food}}</div>\n                  <div class=\"food-name\">{{item.Calories}} Cal</div>\n                </div>                                   \n             \n              </ion-slide>\n              <!-- <ion-slide>\n                  <div style=\"width:100%; height: 100px;background: #DDDDDD;border-radius: 15px;\">\n                    </div>\n              </ion-slide>\n              <ion-slide>\n                  <div style=\"width:100%; height: 100px;background: #DDDDDD;border-radius: 15px;\">\n                    </div>\n              </ion-slide>\n              <ion-slide>\n                  <div style=\"width:100%; height: 100px;background: #DDDDDD;border-radius: 15px;\">\n                    </div>\n              </ion-slide>\n              <ion-slide>\n                <div style=\"width:100%; height: 100px;background: #DDDDDD;border-radius: 15px;\">\n                </div>\n              </ion-slide> -->\n            </ion-slides>\n          </ion-col>\n        </ion-row>\n\n        <ion-row *ngIf=\"!isOtherOptinsLoaded && allOptionData && allOptionData.length == 0\">\n          <ion-col class=\"ion-text-center\" size=\"12\">\n            <ion-slides #Slides [options]=\"slideOptsTwo\">\n              <ion-slide>\n                <div style=\"top: 11px; height: 150px;position: relative;\">\n                  <ion-skeleton-text animated class=\"diet-plan-skeleton\"\n                    style=\"height: 100px;width: 140px;border-radius: 15px;\"></ion-skeleton-text>\n                </div>\n              </ion-slide>\n              <ion-slide>\n                <div style=\"top: 11px; height: 150px;position: relative;\">\n                  <ion-skeleton-text animated class=\"diet-plan-skeleton\"\n                    style=\"height: 100px;width: 140px;border-radius: 15px;\"></ion-skeleton-text>\n                </div>\n              </ion-slide>\n              <ion-slide>\n                <div style=\"top: 11px; height: 150px;position: relative;\">\n                  <ion-skeleton-text animated class=\"diet-plan-skeleton\"\n                    style=\"height: 100px;width: 140px;border-radius: 15px;\"></ion-skeleton-text>\n                </div>\n              </ion-slide>\n              <ion-slide>\n                <div style=\"top: 11px; height: 150px;position: relative;\">\n                  <ion-skeleton-text animated class=\"diet-plan-skeleton\"\n                    style=\"height: 100px;width: 140px;border-radius: 15px;\"></ion-skeleton-text>\n                </div>\n              </ion-slide>\n              <ion-slide>\n                <div style=\"top: 11px; height: 150px;position: relative;\">\n                  <ion-skeleton-text animated class=\"diet-plan-skeleton\"\n                    style=\"height: 100px;width: 140px;border-radius: 15px;\"></ion-skeleton-text>\n                </div>\n              </ion-slide>\n              <ion-slide>\n                <div style=\"top: 11px; height: 150px;position: relative;\">\n                  <ion-skeleton-text animated class=\"diet-plan-skeleton\"\n                    style=\"height: 100px;width: 140px;border-radius: 15px;\"></ion-skeleton-text>\n                </div>\n              </ion-slide>\n            </ion-slides>\n          </ion-col>\n        </ion-row>\n\n        <!-- <ion-col class=\"ion-text-center  ion-align-self-center\" size=\"1\" (click)=\"slideNext()\">\n                  <ion-icon name=\"arrow-forward\">\n                </ion-icon> \n          </ion-col> -->\n\n      </ion-card>\n\n    </div>\n  </div>\n  <!-- <br><br> -->\n</ion-content>\n<iframe *ngIf=\"streamVideo\" width=\"100%\" height=\"100%\" class=\"video-iframe\" [src]=\"videoUrl\" frameborder=\"0\"\n  allowfullscreen></iframe>\n<ion-icon *ngIf=\"streamVideo\" (click)=\"closeVideo()\" name=\"close\" class=\"get-button-icon\"></ion-icon>\n<!-- <ion-button *ngIf=\"streamVideo\" (click)=\"closeVideo()\" expand=\"full\" class=\"get-button video-iframe\" style=\"height: 5vh; left: 90%\">Close</ion-button> -->\n<ion-footer class=\"btn\" *ngIf=\"!streamVideo\">\n  <ion-toolbar>\n    <!-- <div class=\"width-50\" slot=\"start\">\n      <ion-button mode=\"ios\" class=\"button button-cancel button-full\" shape=\"round\" (click)=\"cancel()\">\n        Cancel\n      </ion-button>\n    </div> -->\n    <!-- <ion-title></ion-title> -->\n    <div>\n      <ion-button mode=\"ios\" class=\"button button-dander button-full\" shape=\"round\" fill=\"outline\" (click)=\"update()\">\n        Update\n      </ion-button>\n    </div>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ 14199:
/*!***************************************************!*\
  !*** ./src/app/food-detail/food-detail.page.scss ***!
  \***************************************************/
/***/ ((module) => {

module.exports = "@font-face {\n  font-family: gt-america-standard-medium;\n  src: url(\"/assets/fonts/Roboto-Medium.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-regular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-bold;\n  src: url(\"/assets/fonts/Roboto-Bold.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-extendedMedium;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-light;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: Source-Sans-Pro-ragular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n.width-50 {\n  width: 50%;\n}\n.parent {\n  margin-left: 1rem;\n  margin-right: 1rem;\n  margin-top: 1rem;\n}\n.trash-container {\n  width: 40px;\n  height: 40px;\n  background: red;\n  border-radius: 25px;\n}\n.trash-container .trash {\n  color: #fff;\n  text-align: center;\n  vertical-align: bottom;\n  font-size: 1.5rem;\n  content: \"\";\n  padding-top: 17%;\n  padding-left: 20%;\n}\nimg {\n  max-width: 100%;\n}\nion-item h2 {\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #4a4949;\n  margin-top: 0.5rem;\n  font-size: 1rem;\n}\nion-item ion-avatar {\n  width: 30px;\n  height: 30px;\n}\nion-item ion-label {\n  font-size: 0.9rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #565656;\n}\nion-item span {\n  font-size: 0.9rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #565656;\n}\nion-item ion-icon {\n  font-size: 1rem;\n}\n.first-caps::first-letter {\n  text-transform: uppercase;\n}\nh2 {\n  font-weight: bold;\n}\n.portion-entry {\n  border: 1px solid #01A3A4;\n  padding: 0 5px;\n  border-radius: 25px;\n  margin-top: 5px;\n}\n.portion-entry ion-row ion-col:first-child {\n  border-right: 1px solid #01A3A4;\n}\n.portion-entry ion-row ion-col:last-child {\n  border-left: 1px solid #01A3A4;\n}\n.portion-entry .input-portion {\n  --padding-top: 5px;\n  --padding-bottom: 5px;\n}\n.pic-food-heart {\n  width: 14px;\n  height: auto;\n  right: 0;\n  top: 0px;\n  position: absolute;\n}\n.pic-food-heart-healthy {\n  width: 16px !important;\n  margin-right: 5px;\n}\n.backBtn {\n  position: absolute;\n  left: 20vw;\n  top: 40vh;\n  width: 15vw;\n  height: 15vw;\n  border-radius: 50%;\n  border: none;\n  background: red;\n}\n.img-quantity {\n  width: 40%;\n  height: auto;\n}\n.font-color {\n  color: #6B6B6B;\n}\n.nextBtn {\n  position: absolute;\n  right: 20vw;\n  top: 40vh;\n  width: 15vw;\n  height: 15vw;\n  border-radius: 50%;\n  border: none;\n  background: green;\n}\n.qty-color {\n  color: #4a49499c;\n  font-size: 16px;\n  font-weight: 700;\n}\n.qty-text {\n  color: #8D8E93;\n  font-size: 14px;\n  font-weight: normal;\n}\n.col-hr {\n  border-right: 1px solid #EBEBEB;\n}\n.row-hr {\n  border-bottom: 1px solid #EBEBEB;\n}\n.food-name {\n  width: 140px;\n  white-space: nowrap;\n  overflow: hidden;\n  font-size: 1rem;\n  text-overflow: ellipsis;\n}\n.img-loaded + ion-spinner {\n  display: none;\n}\nion-card {\n  margin: 17px 25px;\n  border-radius: 25px;\n  box-shadow: none;\n}\n.get-button {\n  --background: #01A3A4;\n  --color: #fff;\n  --border-radius: 0px 0px 10px 10px;\n  height: 50px;\n  width: 100%;\n}\nion-content {\n  --background: #F6F7FC !important;\n}\n.video-iframe {\n  transform: rotate(-90deg);\n  transform-origin: left top;\n  width: 100vh;\n  overflow-x: hidden;\n  position: absolute;\n  top: 100%;\n  left: 0;\n  height: 58vh;\n}\n.get-button-icon {\n  color: #fff;\n  background-color: #01a3a4;\n  border: 2px solid #fff;\n  border-radius: 100px;\n  position: absolute;\n  right: 1.2rem;\n  top: 2rem;\n  height: 35px;\n  width: 35px;\n  left: 20%;\n}\n.darkgreen, .legend-9 {\n  border-color: #38A534;\n}\n.light-green, .legend-6 {\n  border-color: #94EA0A;\n}\n.yellow, .legend-3 {\n  border-color: #EADC18;\n  background-color: #EADC18;\n}\n.yellow-orange, .legend-1 {\n  border-color: #ffa500;\n  background-color: #ffa500;\n}\n.grey, .legend-0 {\n  border-color: #7d7d7d5e;\n}\n.legend-top {\n  border-left-width: 3px;\n  border-left-style: solid;\n}\n.legend {\n  border-left-width: 5px;\n  border-left-style: solid;\n}\n.gauge-chart ion-card {\n  display: inline-block;\n  width: 90%;\n  padding-right: 20px;\n  padding-top: unset;\n  padding-left: unset;\n  padding-bottom: unset;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  height: 90px;\n}\n.detail1 {\n  border-right: 1px solid #7377774a;\n}\n.pad-top5 {\n  padding-top: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL2Fzc2V0cy9mb250cy9mb250cy5zY3NzIiwiZm9vZC1kZXRhaWwucGFnZS5zY3NzIiwiLi4vLi4vdGhlbWUvY3VzdG9tLXRoZW1lcy92YXJpYWJsZXMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVDQUFBO0VBQ0EsOERBQUE7QUNDRjtBREVBO0VBQ0Usd0NBQUE7RUFDQSwrREFBQTtBQ0FGO0FER0E7RUFDRSxxQ0FBQTtFQUNBLDREQUFBO0FDREY7QURJQTtFQUNFLCtDQUFBO0VBQ0EsNkRBQUE7QUNGRjtBREtBO0VBQ0Usc0NBQUE7RUFDQSw2REFBQTtBQ0hGO0FET0E7RUFDRSxvQ0FBQTtFQUNBLCtEQUFBO0FDTEY7QUFyQkE7RUFDSSxVQUFBO0FBdUJKO0FBckJDO0VBQ0UsaUJBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBd0JIO0FBdEJDO0VBQ0MsV0FBQTtFQUFZLFlBQUE7RUFBYyxlQUFBO0VBQzFCLG1CQUFBO0FBMkJGO0FBMUJFO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0FBNEJKO0FBekJBO0VBQ0UsZUFBQTtBQTRCRjtBQXpCRTtFQUNFLCtFQ3ZCRztFRHdCSCxjQUFBO0VBQ0Esa0JBQUE7RUFDRCxlQUFBO0FBNEJIO0FBMUJFO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUE0Qko7QUF6QkU7RUFDRSxpQkFBQTtFQUNBLCtFQ25DRztFRG9DSCxjQUFBO0FBMkJKO0FBekJFO0VBQ0UsaUJBQUE7RUFDQSwrRUN4Q0c7RUR5Q0gsY0FBQTtBQTJCSjtBQXpCRTtFQUNFLGVBQUE7QUEyQko7QUF4QkE7RUFDRSx5QkFBQTtBQTJCRjtBQXpCRTtFQUNGLGlCQUFBO0FBNEJBO0FBekJDO0VBQ0MseUJBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FBNEJGO0FBMUJJO0VBQ0csK0JBQUE7QUE0QlA7QUExQkk7RUFDRSw4QkFBQTtBQTRCTjtBQXhCRTtFQUNFLGtCQUFBO0VBQ0EscUJBQUE7QUEwQko7QUF0QkM7RUFDQyxXQUFBO0VBQ0EsWUFBQTtFQUNBLFFBQUE7RUFDQSxRQUFBO0VBQ0Esa0JBQUE7QUF5QkY7QUF0QkE7RUFDRSxzQkFBQTtFQUNBLGlCQUFBO0FBeUJGO0FBcEJDO0VBQ0Msa0JBQUE7RUFDQSxVQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsZUFBQTtBQXVCRjtBQXJCQTtFQUNFLFVBQUE7RUFDRSxZQUFBO0FBd0JKO0FBdEJBO0VBQ0UsY0FBQTtBQXlCRjtBQXZCQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0FBMEJGO0FBeEJBO0VBRUUsZ0JBQUE7RUFDRSxlQUFBO0VBQ0EsZ0JBQUE7QUEwQko7QUF4QkE7RUFFRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FBMEJGO0FBeEJBO0VBQ0UsK0JBQUE7QUEyQkY7QUF6QkE7RUFDRSxnQ0FBQTtBQTRCRjtBQXpCQTtFQUNFLFlBQUE7RUFDRSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLHVCQUFBO0FBNEJKO0FBMUJBO0VBQ0UsYUFBQTtBQTZCRjtBQTFCQTtFQUVFLGlCQUFBO0VBQ0EsbUJBQUE7RUFFQSxnQkFBQTtBQTJCRjtBQXhCQTtFQUNFLHFCQUFBO0VBQ0EsYUFBQTtFQUNBLGtDQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7QUEyQkY7QUF4QkE7RUFDRSxnQ0FBQTtBQTJCRjtBQXRCQTtFQUNFLHlCQUFBO0VBQ0EsMEJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsWUFBQTtBQXlCRjtBQXRCQTtFQUNFLFdBQUE7RUFDQSx5QkFBQTtFQUNBLHNCQUFBO0VBQ0Esb0JBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0VBQ0EsWUFBQTtFQUNBLFdBQUE7RUFDQSxTQUFBO0FBeUJGO0FBckJBO0VBQ0UscUJBQUE7QUF3QkY7QUFwQkE7RUFDRSxxQkFBQTtBQXVCRjtBQW5CQTtFQUNFLHFCQUFBO0VBQ0EseUJBQUE7QUFzQkY7QUFuQkE7RUFDRSxxQkFBQTtFQUNBLHlCQUFBO0FBc0JGO0FBbkJBO0VBQ0UsdUJBQUE7QUFzQkY7QUFuQkE7RUFDRSxzQkFBQTtFQUNBLHdCQUFBO0FBc0JGO0FBbkJBO0VBQ0Usc0JBQUE7RUFDQSx3QkFBQTtBQXNCRjtBQWZFO0VBQ0UscUJBQUE7RUFDQSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGdFQUFBO0VBQ0EsWUFBQTtBQWtCSjtBQWRBO0VBQ0UsaUNBQUE7QUFpQkY7QUFYQTtFQUNFLGdCQUFBO0FBY0YiLCJmaWxlIjoiZm9vZC1kZXRhaWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLW1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1NZWRpdW0udHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLWJvbGQ7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tQm9sZC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1leHRlbmRlZE1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1saWdodDtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBTb3VyY2UtU2Fucy1Qcm8tcmFndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cbiIsIkBpbXBvcnQgXCIuLi8uLi90aGVtZS9mb250cy5zY3NzXCI7XG5cbi53aWR0aC01MCB7XG4gICAgd2lkdGg6IDUwJTtcbiAgfVxuIC5wYXJlbnR7XG4gICBtYXJnaW4tbGVmdDoxcmVtO1xuICAgbWFyZ2luLXJpZ2h0OiAxcmVtO1xuICAgbWFyZ2luLXRvcDogMXJlbTtcbiB9XG4gLnRyYXNoLWNvbnRhaW5lcntcbiAgd2lkdGg6IDQwcHg7aGVpZ2h0OiA0MHB4OyBiYWNrZ3JvdW5kOiByZWQ7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIC50cmFzaHtcbiAgICBjb2xvcjogI2ZmZjtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgdmVydGljYWwtYWxpZ246IGJvdHRvbTtcbiAgICBmb250LXNpemU6IDEuNXJlbTtcbiAgICBjb250ZW50OiAnJztcbiAgICBwYWRkaW5nLXRvcDogMTclO1xuICAgIHBhZGRpbmctbGVmdDogMjAlO1xuICB9XG59XG5pbWd7XG4gIG1heC13aWR0aDogMTAwJTtcbn1cbmlvbi1pdGVte1xuICBoMntcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgY29sb3I6IzRhNDk0OTtcbiAgICBtYXJnaW4tdG9wOiAuNXJlbTtcbiAgIGZvbnQtc2l6ZTogMXJlbTtcbiAgfVxuICBpb24tYXZhdGFye1xuICAgIHdpZHRoOiAzMHB4O1xuICAgIGhlaWdodDogMzBweDtcbiAgfVxuXG4gIGlvbi1sYWJlbHtcbiAgICBmb250LXNpemU6IC45cmVtO1xuICAgIGZvbnQtZmFtaWx5OiAkZm9udDtcbiAgICBjb2xvcjojNTY1NjU2O1xuICB9XG4gIHNwYW57XG4gICAgZm9udC1zaXplOiAuOXJlbTtcbiAgICBmb250LWZhbWlseTogJGZvbnQ7XG4gICAgY29sb3I6IzU2NTY1NjtcbiAgfVxuICBpb24taWNvbntcbiAgICBmb250LXNpemU6IDFyZW07XG4gIH1cbn1cbi5maXJzdC1jYXBzOjpmaXJzdC1sZXR0ZXJ7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gIH1cbiAgaDJ7XG5mb250LXdlaWdodDogYm9sZDtcbiAgfVxuXG4gLnBvcnRpb24tZW50cnl7XG4gIGJvcmRlcjogMXB4IHNvbGlkICMwMUEzQTQ7XG4gIHBhZGRpbmc6IDAgNXB4O1xuICBib3JkZXItcmFkaXVzOiAyNXB4O1xuICBtYXJnaW4tdG9wOiA1cHg7XG4gIGlvbi1yb3d7XG4gICAgaW9uLWNvbDpmaXJzdC1jaGlsZHtcbiAgICAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjMDFBM0E0O1xuICAgIH1cbiAgICBpb24tY29sOmxhc3QtY2hpbGR7XG4gICAgICBib3JkZXItbGVmdDogMXB4IHNvbGlkICMwMUEzQTQ7XG4gICAgfVxuICB9XG5cbiAgLmlucHV0LXBvcnRpb257XG4gICAgLS1wYWRkaW5nLXRvcDogNXB4O1xuICAgIC0tcGFkZGluZy1ib3R0b206IDVweDtcbiAgfVxuIH1cblxuIC5waWMtZm9vZC1oZWFydHtcbiAgd2lkdGg6IDE0cHg7XG4gIGhlaWdodDogYXV0bztcbiAgcmlnaHQ6IDA7XG4gIHRvcDogMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG5cbi5waWMtZm9vZC1oZWFydC1oZWFsdGh5IHtcbiAgd2lkdGg6IDE2cHggIWltcG9ydGFudDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG4gIC8vIG1hcmdpbi10b3A6IDEwcHg7XG4gIC8vIGhlaWdodDogYXV0byAhaW1wb3J0YW50O1xufVxuXG4gLmJhY2tCdG57XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMjB2dztcbiAgdG9wOiA0MHZoO1xuICB3aWR0aDogMTV2dztcbiAgaGVpZ2h0OiAxNXZ3O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlcjogbm9uZTtcbiAgYmFja2dyb3VuZDogcmVkO1xufVxuLmltZy1xdWFudGl0eXtcbiAgd2lkdGg6IDQwJTtcbiAgICBoZWlnaHQ6IGF1dG87XG59XG4uZm9udC1jb2xvcntcbiAgY29sb3I6ICM2QjZCNkI7XG59XG4ubmV4dEJ0bntcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMjB2dztcbiAgdG9wOiA0MHZoO1xuICB3aWR0aDogMTV2dztcbiAgaGVpZ2h0OiAxNXZ3O1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIGJvcmRlcjogbm9uZTtcbiAgYmFja2dyb3VuZDogZ3JlZW47XG59XG4ucXR5LWNvbG9yXG57XG4gIGNvbG9yOiAjNGE0OTQ5OWM7XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG59XG4ucXR5LXRleHRcbntcbiAgY29sb3I6ICM4RDhFOTM7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbn1cbi5jb2wtaHJ7XG4gIGJvcmRlci1yaWdodDogMXB4IHNvbGlkICNFQkVCRUI7XG59XG4ucm93LWhye1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI0VCRUJFQjtcbn1cblxuLmZvb2QtbmFtZXtcbiAgd2lkdGg6IDE0MHB4O1xuICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBmb250LXNpemU6IDFyZW07XG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG4uaW1nLWxvYWRlZCArIGlvbi1zcGlubmVyIHtcbiAgZGlzcGxheTpub25lO1xufVxuXG5pb24tY2FyZHtcbiAgLy8gbWFyZ2luOiAyNXB4O1xuICBtYXJnaW46IDE3cHggMjVweDtcbiAgYm9yZGVyLXJhZGl1czogMjVweDtcbiAgLy8gcGFkZGluZzogMCAxNXB4O1xuICBib3gtc2hhZG93OiBub25lO1xufVxuXG4uZ2V0LWJ1dHRvbntcbiAgLS1iYWNrZ3JvdW5kOiAjMDFBM0E0O1xuICAtLWNvbG9yOiAjZmZmO1xuICAtLWJvcmRlci1yYWRpdXM6IDBweCAwcHggMTBweCAxMHB4O1xuICBoZWlnaHQ6IDUwcHg7XG4gIHdpZHRoOiAxMDAlO1xufSBcblxuaW9uLWNvbnRlbnQge1xuICAtLWJhY2tncm91bmQ6ICNGNkY3RkMgIWltcG9ydGFudDtcbiAgLy8gLS1iYWNrZ3JvdW5kOiAtd2Via2l0LWxpbmVhci1ncmFkaWVudCh0b3AsICMwMUEzQTQgNTAlLCAjRjZGN0ZDIDUwJSkhaW1wb3J0YW50O1xuXG59XG5cbi52aWRlby1pZnJhbWV7XG4gIHRyYW5zZm9ybTogcm90YXRlKC05MGRlZyk7XG4gIHRyYW5zZm9ybS1vcmlnaW46IGxlZnQgdG9wO1xuICB3aWR0aDogMTAwdmg7XG4gIG92ZXJmbG93LXg6IGhpZGRlbjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDEwMCU7XG4gIGxlZnQ6IDA7XG4gIGhlaWdodDogNTh2aDtcbn1cblxuLmdldC1idXR0b24taWNvbntcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMWEzYTQ7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNmZmY7XG4gIGJvcmRlci1yYWRpdXM6IDEwMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHJpZ2h0OiAxLjJyZW07XG4gIHRvcDogMnJlbTtcbiAgaGVpZ2h0OiAzNXB4O1xuICB3aWR0aDogMzVweDtcbiAgbGVmdDogMjAlXG59XG5cblxuLmRhcmtncmVlbiwgLmxlZ2VuZC05e1xuICBib3JkZXItY29sb3I6ICMzOEE1MzQ7XG4gIC8vIGJhY2tncm91bmQtY29sb3I6ICMzOEE1MzQ7XG59XG5cbi5saWdodC1ncmVlbiwgLmxlZ2VuZC02e1xuICBib3JkZXItY29sb3I6ICM5NEVBMEE7XG4gIC8vIGJhY2tncm91bmQtY29sb3I6ICM5NEVBMEE7XG59XG5cbi55ZWxsb3csIC5sZWdlbmQtM3tcbiAgYm9yZGVyLWNvbG9yOiAjRUFEQzE4O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjRUFEQzE4O1xufVxuXG4ueWVsbG93LW9yYW5nZSwgLmxlZ2VuZC0xe1xuICBib3JkZXItY29sb3I6ICNmZmE1MDA7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZmE1MDA7XG59XG5cbi5ncmV5LCAubGVnZW5kLTB7XG4gIGJvcmRlci1jb2xvcjogIzdkN2Q3ZDVlO1xufVxuXG4ubGVnZW5kLXRvcHtcbiAgYm9yZGVyLWxlZnQtd2lkdGg6IDNweDtcbiAgYm9yZGVyLWxlZnQtc3R5bGU6IHNvbGlkO1xufVxuXG4ubGVnZW5ke1xuICBib3JkZXItbGVmdC13aWR0aDogNXB4O1xuICBib3JkZXItbGVmdC1zdHlsZTogc29saWQ7XG4gIC8vIGhlaWdodDogNTZweDtcbiAgLy8gd2lkdGg6IDJweDtcbiAgLy8gbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbi5nYXVnZS1jaGFydHtcbiAgaW9uLWNhcmR7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiA5MCU7XG4gICAgcGFkZGluZy1yaWdodDogMjBweDtcbiAgICBwYWRkaW5nLXRvcDogdW5zZXQ7XG4gICAgcGFkZGluZy1sZWZ0OiB1bnNldDtcbiAgICBwYWRkaW5nLWJvdHRvbTogdW5zZXQ7XG4gICAgYm9yZGVyLXJhZGl1czogMzBweDtcbiAgICBib3gtc2hhZG93OiAwcHggNXB4IDEwcHggMHB4IHJnYigxMjkgMTMzIDEzMSAvIDEwJSkgIWltcG9ydGFudDtcbiAgICBoZWlnaHQ6IDkwcHg7XG4gIH0gXG59XG5cbi5kZXRhaWwxe1xuICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjNzM3Nzc3NGE7XG4gIC8vIHNwYW57XG4gIC8vICAgICBmb250LXdlaWdodDogNzAwO1xuICAvLyB9XG59XG5cbi5wYWQtdG9wNXtcbiAgcGFkZGluZy10b3A6IDVweDtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vYXNzZXRzL2ZvbnRzL2ZvbnRzLnNjc3NcIjtcblxuLy8gRm9udHNcbiRmb250LXNpemU6IDE2cHg7XG5cbiRmb250OiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhclwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtYm9sZDogXCJndC1hbWVyaWNhLXN0YW5kYXJkLWJvbGRcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiRmb250LWxpZ2h0OiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtbGlnaHRcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiRmb250LW1lZGl1bTogXCJndC1hbWVyaWNhLXN0YW5kYXJkLW1lZGl1bVwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtbWVkaXVtLWV4dGVuZGVkOiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtZXh0ZW5kZWRNZWRpdW1cIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLFxuICBBcmlhbCwgc2Fucy1zZXJpZjtcblxuICAkc2Fucy1mb250LXJlZ3VsYXI6IFwiU291cmNlLVNhbnMtUHJvLXJhZ3VsYXJcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjsiXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_food-detail_food-detail_module_ts.js.map