"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_search-lead_search-lead_module_ts"],{

/***/ 72210:
/*!***********************************************************!*\
  !*** ./src/app/search-lead/search-lead-routing.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SearchLeadPageRoutingModule": () => (/* binding */ SearchLeadPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _search_lead_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./search-lead.page */ 97390);




const routes = [
    {
        path: '',
        component: _search_lead_page__WEBPACK_IMPORTED_MODULE_0__.SearchLeadPage
    }
];
let SearchLeadPageRoutingModule = class SearchLeadPageRoutingModule {
};
SearchLeadPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SearchLeadPageRoutingModule);



/***/ }),

/***/ 56183:
/*!***************************************************!*\
  !*** ./src/app/search-lead/search-lead.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SearchLeadPageModule": () => (/* binding */ SearchLeadPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _search_lead_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./search-lead-routing.module */ 72210);
/* harmony import */ var _search_lead_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./search-lead.page */ 97390);







let SearchLeadPageModule = class SearchLeadPageModule {
};
SearchLeadPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _search_lead_routing_module__WEBPACK_IMPORTED_MODULE_0__.SearchLeadPageRoutingModule
        ],
        declarations: [_search_lead_page__WEBPACK_IMPORTED_MODULE_1__.SearchLeadPage]
    })
], SearchLeadPageModule);



/***/ }),

/***/ 97390:
/*!*************************************************!*\
  !*** ./src/app/search-lead/search-lead.page.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SearchLeadPage": () => (/* binding */ SearchLeadPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_search_lead_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./search-lead.page.html */ 11733);
/* harmony import */ var _search_lead_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./search-lead.page.scss */ 55497);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _modal_summary_update_summary_update_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../modal/summary-update/summary-update.page */ 7217);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services */ 65190);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ 29243);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);









let SearchLeadPage = class SearchLeadPage {
    constructor(modalController, router, route, appService, storageService, loaderService) {
        this.modalController = modalController;
        this.router = router;
        this.route = route;
        this.appService = appService;
        this.storageService = storageService;
        this.loaderService = loaderService;
        this.isMoreLeads = false;
        this.leadsLimit = 4;
        this.searchFilterredList = [];
        this.subOptions = [];
        this.route.queryParams.subscribe((res) => {
            this.searchQueryType = res.searchQueryType;
            this.searchQueryCode = res.searchQueryCode;
            this.dataOnInIt(res.searchQueryType, res.searchQueryCode);
        });
    }
    dateFormat(dt) {
        return moment__WEBPACK_IMPORTED_MODULE_4___default()(dt).format("DD-MM-YYYY");
    }
    subOptionName(id) {
        let subOptionName = this.subOptions.filter((ele) => {
            return ele.code == id;
        });
        if (subOptionName.length)
            return subOptionName[0]["name"];
        else
            return "";
    }
    dataOnInIt(type, code) {
        this.loaderService.presentLoader("Please Wait").then((_) => {
            let search = type + "=" + code;
            this.appService.searchLead(search).subscribe((response) => {
                if (response) {
                    this.loaderService.hideLoader();
                    this.appService.refs().subscribe((val) => {
                        console.log("test-refs", val);
                        if (val) {
                            this.subOptions = val.subOptionTypes;
                            this.searchLeadData = response.content;
                            this.searchLeadData = this.searchLeadData.sort((a, b) => {
                                let date1 = {
                                    year: b.leadInfo.createdAt.split(' ')[0].split('-')[2],
                                    month: b.leadInfo.createdAt.split(' ')[0].split('-')[1],
                                    day: b.leadInfo.createdAt.split(' ')[0].split('-')[0]
                                };
                                let date2 = {
                                    year: a.leadInfo.createdAt.split(' ')[0].split('-')[2],
                                    month: a.leadInfo.createdAt.split(' ')[0].split('-')[1],
                                    day: a.leadInfo.createdAt.split(' ')[0].split('-')[0]
                                };
                                return new Date(date2.year, Number(date2.month) - 1, date2.day) < new Date(date1.year, Number(date1.month) - 1, date1.day) ? 1 : -1;
                            });
                            this.leadSearch(null);
                        }
                    }, (error) => {
                        // this.loaderService.hideLoader();
                        console.log("Category Error", error);
                    });
                    // this.storageService.get("refs").then((val) => {
                    //   this.subOptions = val.subOptionTypes;
                    //   this.searchLeadData = response.content;
                    //   console.log("Response for search ", this.searchLeadData);
                    //   this.leadSearch(null);
                    // });
                }
                (error) => {
                    this.loaderService.hideLoader();
                };
            });
        });
    }
    setFalseForAllList() {
        this.searchFilterredList.filter((ele) => {
            ele.isMoreLeads = false;
            return ele;
        });
    }
    ngOnInit() { }
    leadSearch(searchValue) {
        let val = searchValue && searchValue.target && searchValue.target.value
            ? searchValue.target.value
            : null;
        if (!val) {
            this.searchFilterredList = [...this.searchLeadData];
        }
        else {
            this.searchFilterredList = this.searchLeadData.filter((lead) => {
                if (lead.customerInfo == null) {
                    return lead.leadInfo.id.includes(val);
                }
                else {
                    return (lead.customerInfo.profile.name.includes(val) ||
                        lead.customerInfo._id.includes(val) ||
                        lead.leadInfo.status.includes(val));
                }
            });
        }
        this.setFalseForAllList();
    }
    showLeadsMore(i) {
        this.searchFilterredList[i]["isMoreLeads"] =
            !this.searchFilterredList[i]["isMoreLeads"];
        // this.isMoreLeads = !this.isMoreLeads;
        this.leadsLimit = this.isMoreLeads ? 4 : 8;
    }
    updateSummary(leadItem, i) {
        this.selectedLeadItem = leadItem;
        this.openSummaryUpdate();
    }
    toNormalLabel(text) {
        if (text) {
            const result = text.replace(/([A-Z])/g, " $1");
            return result.charAt(0).toUpperCase() + result.slice(1);
        }
        else
            return "";
    }
    openSummaryUpdate() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            console.log("Lead item ", this.selectedLeadItem);
            const modal = yield this.modalController.create({
                component: _modal_summary_update_summary_update_page__WEBPACK_IMPORTED_MODULE_2__.SummaryUpdatePage,
                cssClass: "open-summary-update-modal",
                backdropDismiss: true,
                componentProps: {
                    leadData: this.selectedLeadItem,
                    searchQueryType: this.searchQueryType,
                    searchQueryCode: this.searchQueryCode
                },
            });
            modal.onDidDismiss().then((data) => {
                if (data && data.data && data.data.searchQueryCode) {
                    this.dataOnInIt(data.data.searchQueryType, data.data.searchQueryCode);
                }
            });
            return yield modal.present();
        });
    }
};
SearchLeadPage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ModalController },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_7__.ActivatedRoute },
    { type: _services__WEBPACK_IMPORTED_MODULE_3__.AppService },
    { type: _services__WEBPACK_IMPORTED_MODULE_3__.StorageService },
    { type: _services__WEBPACK_IMPORTED_MODULE_3__.LoaderService }
];
SearchLeadPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: "app-search-lead",
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_search_lead_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_search_lead_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], SearchLeadPage);



/***/ }),

/***/ 11733:
/*!******************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/search-lead/search-lead.page.html ***!
  \******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header class=\"ion-text-center ion-no-border\">\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button text=\"\" class=\"headerBackBtn\"></ion-back-button>\n    </ion-buttons>\n    <!-- <ion-title>About Us</ion-title> -->\n    <ion-label class=\"headerFont\">Summary of Hot Leads</ion-label>\n  </ion-toolbar>\n</ion-header>\n<ion-content>\n  <div class=\"ion-padding-horizontal-20\">\n      <!-- <ion-label class=\"headerFont\">Summary of Hot Leads</ion-label> -->\n      <ion-searchbar\n        searchIcon=\"none\"\n        placeholder=\"Search for food\"\n        class=\"ion-no-padding mb-20\"\n        (ionChange)=\"leadSearch($event)\"\n      ></ion-searchbar>\n    <div>\n      <ion-card  *ngFor=\"let leadItem of searchFilterredList; let i = index\"  class=\"ion-no-margin ion-padding ion-margin-bottom-20 br-20\">  \n       <div [ngClass]=\"{'showLeedsMore': leadItem.isMoreLeads == true, 'showLess' : leadItem.isMoreLeads != true }\">\n         <ion-icon color=\"theme\" size=\"large\" (click)=\"showLeadsMore(i)\" class=\"ion-float-end\"  [name]=\"leadItem.isMoreLeads ? 'chevron-up-outline' : 'chevron-down-outline' \"></ion-icon>\n         <div *ngFor=\"let item of leadItem.leadInfo | keyvalue \">\n          <ion-row *ngIf=\"item.key != 'remarks'\">\n            <ion-col  size=\"4\" >\n               <ion-label class=\"cardLabel\">{{toNormalLabel(item.key)}}</ion-label>\n            </ion-col>\n            <ion-col>\n              <span class=\"ion-padding-start cardSubLabel\">{{item.key == 'subOption' ? subOptionName(item.value) : item.value}}</span>\n            </ion-col>\n          </ion-row>\n          <ion-row *ngIf=\"item.key == 'remarks'\">\n            <ion-col >\n              <ion-label class=\"cardLabel\">{{toNormalLabel(item.key)}}</ion-label>\n           </ion-col>\n          </ion-row>\n          <ion-row *ngIf=\"item.key == 'remarks'\">\n            <ion-col>\n              <span *ngFor=\"let subItem of item.value\" class=\"cardSubLabel\">\n                <ion-row>\n                  <ion-col>\n                    {{dateFormat(subItem.createdAt)}}\n                  </ion-col>\n                  <ion-col>\n                    {{subItem.createdBy}}\n                  </ion-col>\n                </ion-row>\n                <ion-row class=\"border-bottom-light-grey\">\n                  <ion-col>\n                    {{subItem.remark}}\n                  </ion-col>\n                </ion-row>\n              </span>\n           </ion-col>\n          </ion-row>\n         </div>\n        <!-- <ion-col *ngIf=\"item.key === 'remarks'\" >\n          <span *ngFor=\"let subItem of item.value\" class=\"ion-padding-start cardSubLabel\">\n            <ion-row>\n              <ion-col>\n                {{subItem.createdAt}}\n              </ion-col>\n              <ion-col>\n                {{subItem.createdBy}}\n              </ion-col>\n            </ion-row>\n            <ion-row>\n              <ion-col>\n                {{subItem.remark}}\n              </ion-col>\n            </ion-row>\n          </span>\n        </ion-col> -->\n       </div>\n        <div class=\"ion-card-update-btn ion-text-center\">\n          <ion-button (click)=\"updateSummary(leadItem,i)\" color=\"theme\" shape=\"round\" fill=\"outline\">\n            Update\n          </ion-button>\n          <ion-row>\n           \n        </ion-row>\n        </div>\n        \n      </ion-card>\n    </div>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ 55497:
/*!***************************************************!*\
  !*** ./src/app/search-lead/search-lead.page.scss ***!
  \***************************************************/
/***/ ((module) => {

module.exports = "ion-toolbar {\n  --background: #F6F7FC;\n  --border-color: #F6F7FC;\n}\n\nion-searchbar {\n  --background: #FFFFFF;\n  --border-radius: 7px;\n}\n\nion-content {\n  --background: #F6F7FC;\n}\n\n.showLeedsMore {\n  height: auto;\n  --background: #FFFFFF;\n  border-radius: 23px;\n  border: var(--ion-color-theme);\n}\n\n.showLess {\n  height: 150px;\n  --background: #FFFFFF;\n  border-radius: 23px;\n  border: var(--ion-color-theme);\n}\n\n.cardLabel {\n  font-size: 14px;\n  color: var(--ion-color-theme);\n}\n\n.cardSubLabel {\n  color: #8C8E93;\n}\n\n.buttonBackground {\n  background: #FFFFFF;\n}\n\n.ion-card-update-btn {\n  background-color: #fff;\n  position: relative;\n  top: 16px;\n  bottom: 0px;\n  padding-top: 20px;\n  padding-bottom: 20px;\n}\n\n.br-20 {\n  border-radius: 20px;\n}\n\n.mb-20 {\n  margin-bottom: 20px;\n}\n\n.border-bottom-light-grey {\n  border-bottom: 1px solid lightgrey;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlYXJjaC1sZWFkLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLHFCQUFBO0VBQ0EsdUJBQUE7QUFDSjs7QUFDQTtFQUNJLHFCQUFBO0VBQ0Esb0JBQUE7QUFFSjs7QUFBQTtFQUNJLHFCQUFBO0FBR0o7O0FBREE7RUFDSSxZQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FBSUo7O0FBRkE7RUFDSSxhQUFBO0VBQ0EscUJBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FBS0o7O0FBSEE7RUFDSSxlQUFBO0VBQ0EsNkJBQUE7QUFNSjs7QUFKQTtFQUNJLGNBQUE7QUFPSjs7QUFMQTtFQUNJLG1CQUFBO0FBUUo7O0FBTEE7RUFDSSxzQkFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLG9CQUFBO0FBUUo7O0FBSkU7RUFDSSxtQkFBQTtBQU9OOztBQUhFO0VBQ0ksbUJBQUE7QUFNTjs7QUFIQTtFQUNJLGtDQUFBO0FBTUoiLCJmaWxlIjoic2VhcmNoLWxlYWQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRvb2xiYXIge1xuICAgIC0tYmFja2dyb3VuZDogI0Y2RjdGQztcbiAgICAtLWJvcmRlci1jb2xvcjogI0Y2RjdGQztcbn1cbmlvbi1zZWFyY2hiYXJ7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIC0tYm9yZGVyLXJhZGl1czogN3B4O1xufVxuaW9uLWNvbnRlbnQge1xuICAgIC0tYmFja2dyb3VuZDogI0Y2RjdGQztcbn1cbi5zaG93TGVlZHNNb3Jle1xuICAgIGhlaWdodDogYXV0bztcbiAgICAtLWJhY2tncm91bmQ6ICNGRkZGRkY7XG4gICAgYm9yZGVyLXJhZGl1czogMjNweDtcbiAgICBib3JkZXI6IHZhcigtLWlvbi1jb2xvci10aGVtZSk7XG59XG4uc2hvd0xlc3N7XG4gICAgaGVpZ2h0OiAxNTBweDtcbiAgICAtLWJhY2tncm91bmQ6ICNGRkZGRkY7XG4gICAgYm9yZGVyLXJhZGl1czogMjNweDtcbiAgICBib3JkZXI6IHZhcigtLWlvbi1jb2xvci10aGVtZSk7XG59XG4uY2FyZExhYmVse1xuICAgIGZvbnQtc2l6ZTogMTRweDtcbiAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXRoZW1lKTtcbn1cbi5jYXJkU3ViTGFiZWx7XG4gICAgY29sb3I6ICM4QzhFOTM7XG59XG4uYnV0dG9uQmFja2dyb3VuZHtcbiAgICBiYWNrZ3JvdW5kOiAjRkZGRkZGO1xufVxuXG4uaW9uLWNhcmQtdXBkYXRlLWJ0bntcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IDE2cHg7XG4gICAgYm90dG9tOiAwcHg7XG4gICAgcGFkZGluZy10b3A6IDIwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDIwcHg7XG59XG5cblxuICAuYnItMjB7XG4gICAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICB9XG5cblxuICAubWItMjB7XG4gICAgICBtYXJnaW4tYm90dG9tOiAyMHB4O1xuICB9XG5cbi5ib3JkZXItYm90dG9tLWxpZ2h0LWdyZXl7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIGxpZ2h0Z3JleTtcbn0iXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_search-lead_search-lead_module_ts.js.map