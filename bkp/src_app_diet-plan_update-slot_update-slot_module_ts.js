"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_diet-plan_update-slot_update-slot_module_ts"],{

/***/ 48434:
/*!*********************************************************************!*\
  !*** ./src/app/diet-plan/update-slot/update-slot-routing.module.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateSlotPageRoutingModule": () => (/* binding */ UpdateSlotPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _update_slot_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-slot.page */ 47465);




const routes = [
    {
        path: '',
        component: _update_slot_page__WEBPACK_IMPORTED_MODULE_0__.UpdateSlotPage
    }
];
let UpdateSlotPageRoutingModule = class UpdateSlotPageRoutingModule {
};
UpdateSlotPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], UpdateSlotPageRoutingModule);



/***/ }),

/***/ 47588:
/*!*************************************************************!*\
  !*** ./src/app/diet-plan/update-slot/update-slot.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateSlotPageModule": () => (/* binding */ UpdateSlotPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _update_slot_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-slot-routing.module */ 48434);
/* harmony import */ var _update_slot_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-slot.page */ 47465);







let UpdateSlotPageModule = class UpdateSlotPageModule {
};
UpdateSlotPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _update_slot_routing_module__WEBPACK_IMPORTED_MODULE_0__.UpdateSlotPageRoutingModule
        ],
        declarations: [_update_slot_page__WEBPACK_IMPORTED_MODULE_1__.UpdateSlotPage]
    })
], UpdateSlotPageModule);



/***/ }),

/***/ 47465:
/*!***********************************************************!*\
  !*** ./src/app/diet-plan/update-slot/update-slot.page.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateSlotPage": () => (/* binding */ UpdateSlotPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_update_slot_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./update-slot.page.html */ 29151);
/* harmony import */ var _update_slot_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-slot.page.scss */ 66235);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var src_app_core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/utility/utilities */ 53533);
/* harmony import */ var src_app_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services */ 65190);







let UpdateSlotPage = class UpdateSlotPage {
    constructor(utilities, route, router, appService) {
        this.utilities = utilities;
        this.route = route;
        this.router = router;
        this.appService = appService;
        this.updateSlot = [{ name: 'sabji', count: 14 }, { name: 'Curry', count: 14 }, { name: 'Raita', count: 14 }, { name: 'Bread', count: 14 }];
        this.slot = 1;
        this.slotName = '';
        this.bkpData = [];
        this.isShow = false;
        this.route.queryParams.subscribe(res => {
            console.log("response:-", res);
            this.slot = res.slot;
            this.slotName = res.name;
        });
    }
    ngOnInit() {
        this.fetchSlotsDetails();
    }
    fetchSlotsDetails() {
        const payload = {
            email: localStorage.getItem("email"),
            slot: this.slot,
            dietPlan: this.slotName
        };
        this.appService.fetchAllCategories(payload).subscribe(res => {
            console.log("res", res);
            this.updateSlot = this.utilities.customSort(res);
            this.bkpData = JSON.stringify(this.updateSlot);
            for (let index = 0; index < this.updateSlot.length; index++) {
                this.updateSlot[index].order = 0;
            }
        }, err => {
            console.log("error", err);
        });
    }
    isVisibal(isA, isB) {
        if (!isA && !isB) {
            return false;
        }
        return true;
    }
    gotoUpdate() {
        this.router.navigate(['./confirm-diet']);
    }
    gotoDetail(item) {
        console.log("sdiet", item);
        this.router.navigate(['update-detail-slot'], { queryParams: { name: this.slotName, cat: item.category, slot: this.slot } });
    }
    reset() {
        const tempDa = this.updatePAyload(JSON.parse(this.bkpData));
        console.log("reset", tempDa);
        const reqPayload = {
            email: localStorage.getItem('email'),
            slot: this.slot,
            data: tempDa
        };
        this.appService.updateAllCategories(reqPayload).subscribe(res => {
            console.log("update slot", res);
        }, err => {
            console.log("error", err);
        });
    }
    updatePAyload(data) {
        //  const data=JSON.parse(this.bkpData);
        const tempData = [];
        for (let index = 0; index < data.length; index++) {
            tempData.push({ categories: data[index].category, diet: data[index].diet, option: data[index].option, order: data[index].order });
        }
        return tempData;
    }
    updateDiet() {
        const tempDa = this.updatePAyload(this.updateSlot);
        const reqPayload = {
            email: localStorage.getItem('email'),
            slot: this.slot,
            data: tempDa
        };
        console.log("update slot payload:", tempDa);
        this.appService.updateAllCategories(reqPayload).subscribe(res => {
            console.log("update slot", res);
        }, err => {
            console.log("error", err);
        });
    }
};
UpdateSlotPage.ctorParameters = () => [
    { type: src_app_core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__.UTILITIES },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.ActivatedRoute },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router },
    { type: src_app_services__WEBPACK_IMPORTED_MODULE_3__.AppService }
];
UpdateSlotPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-update-slot',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_update_slot_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_update_slot_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], UpdateSlotPage);



/***/ }),

/***/ 29151:
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/diet-plan/update-slot/update-slot.page.html ***!
  \****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n<ion-content class=\"update-slot-diet\">\n  <ion-row>\n    <ion-col class=\"ion-text-center\">\n      <h2 class=\"choose-diet\"> Update  the categories for a \n        </h2>\n        <h2 style=\"margin-top:0\">\n          “Lunch slot ({{slot}})”\n        </h2>\n    </ion-col>\n  </ion-row>\n  \n  <ion-grid class=\"content\">\n    <ion-card clss=\"card-item\" >\n      <ion-card-header style=\"border-bottom:.5px solid #b3b3b3;padding:10px 0;\">\n        <ion-row style=\"padding:.5rem;\" >\n          <ion-col size=\"5.1\" class=\"ion-text-left\"><span class=\"cat\">Categories</span></ion-col>\n          <ion-col size=\"2\" class=\"ion-text-center\"><span class=\"order\">Order</span></ion-col>\n          <ion-col size=\"2.3\" class=\"ion-text-center\"><span class=\"option\">Option</span></ion-col>\n          <ion-col size=\"2.6\" class=\"ion-text-center\"><span class=\"diet\">Diet Plan</span></ion-col>\n        </ion-row>\n      </ion-card-header>\n      <ion-card-content>\n        <div *ngFor=\"let sdiet of updateSlot;let sdt=index;\" >\n        <ion-row style=\"padding:.5rem;border-bottom:1px solid #b3b3b3;\" *ngIf=\"isVisibal(sdiet.option,sdiet.diet) && !isShow\">\n          <ion-col (click)=\"gotoDetail(sdiet)\" size=\"5.1\" class=\"ion-text-left\"><span>{{sdiet.name}}</span> <br> <span style=\"color:#01A3A4\">{{sdiet.foodCount}}</span> <ion-icon style=\"color:#01A3A4\" name=\"arrow-forward\"></ion-icon></ion-col>\n          <ion-col size=\"2\" class=\"ion-text-center ion-align-self-center\">\n            <input style=\"height:50px;width:40px;border:1px solid #b3b3b3; border-radius:4px;text-align:center;padding:2px;\" type=\"number\" [(ngModel)]=\"sdiet.order\"/> </ion-col>\n          <ion-col size=\"2.3\" class=\"ion-text-center ion-align-self-center\">\n            <ion-checkbox class=\"option\" mode=\"md\" [disabled]=\"sdiet.diet?true:false\" [checked]=\"sdiet.diet?true:sdiet.option\" [(ngModel)]=\"sdiet.option\"></ion-checkbox>\n          </ion-col>\n          <ion-col size=\"2.6\" class=\"ion-text-center ion-align-self-center\">\n            <ion-checkbox class=\"diet\" mode=\"md\" [checked]=\"sdiet.diet\" [(ngModel)]=\"sdiet.diet\"></ion-checkbox>\n          </ion-col>\n        </ion-row>\n\n        <ion-row style=\"padding:.5rem;border-bottom:1px solid #b3b3b3;\" *ngIf=\"isShow\">\n          <ion-col (click)=\"gotoDetail(sdiet)\" size=\"5.1\" class=\"ion-text-left\"><span>{{sdiet.name}}</span> <br> <span style=\"color:#01A3A4\">{{sdiet.foodCount}}</span> <ion-icon style=\"color:#01A3A4\" name=\"arrow-forward\"></ion-icon></ion-col>\n          <ion-col size=\"2\" class=\"ion-text-center ion-align-self-center\">\n            <input style=\"height:50px;width:40px;border:1px solid #b3b3b3; border-radius:4px;text-align:center;padding:2px;\" type=\"number\" [(ngModel)]=\"sdiet.order\"/> </ion-col>\n          <ion-col size=\"2.3\" class=\"ion-text-center ion-align-self-center\">\n            <ion-checkbox class=\"option\" mode=\"md\" [disabled]=\"sdiet.diet?true:false\" [checked]=\"sdiet.diet?true:sdiet.option\" [(ngModel)]=\"sdiet.option\"></ion-checkbox>\n          </ion-col>\n          <ion-col size=\"2.6\" class=\"ion-text-center ion-align-self-center\">\n            <ion-checkbox class=\"diet\" mode=\"md\" [checked]=\"sdiet.diet\" [(ngModel)]=\"sdiet.diet\"></ion-checkbox>\n          </ion-col>\n        </ion-row>\n      \n      </div>\n      <ion-row>\n        <ion-col class=\"ion-text-center\" (click)=\"isShow=!isShow;\">\n          <a style=\"color:#01A3A4; cursor:HAND;\">Show All Categories</a>\n        </ion-col>\n      </ion-row>\n      </ion-card-content>\n    </ion-card>\n  </ion-grid>\n  </ion-content>\n  <ion-footerbar>\n  <ion-row>\n    <ion-col class=\"ion-text-center\">\n      <ion-button shape=\"round\" color=\"theme\" expand=\"block\" fill=\"outline\" (click)=\"updateDiet()\">Update</ion-button>\n    </ion-col>\n    <ion-col>\n      <ion-button shape=\"round\" color=\"theme\" expand=\"block\" (click)=\"reset()\">Reset Default</ion-button>\n    </ion-col>\n  </ion-row>\n  </ion-footerbar>");

/***/ }),

/***/ 66235:
/*!*************************************************************!*\
  !*** ./src/app/diet-plan/update-slot/update-slot.page.scss ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = ".update-slot-diet {\n  --background:#F6F7FC;\n}\n.update-slot-diet h2 {\n  font-size: 1.2rem;\n  color: #263143;\n}\n.update-slot-diet .content {\n  margin: 1rem 0rem;\n}\n.update-slot-diet .content ion-card {\n  box-shadow: none;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col {\n  font-size: 0.8rem;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.cat {\n  color: gray;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.order {\n  color: #00B8F6;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.option {\n  color: #01A3A4;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.diet {\n  color: #E69740;\n}\n.update-slot-diet .content ion-card-content ion-checkbox.option {\n  --border-radius: 100%;\n  --background-checked: #01A3A4;\n  --border-color-checked: #01A3A4;\n  height: 25px;\n  width: 25px;\n}\n.update-slot-diet .content ion-card-content ion-checkbox.diet {\n  --border-radius: 100%;\n  --background-checked: #E69740;\n  --border-color-checked: #E69740;\n  height: 25px;\n  width: 25px;\n}\n.update-slot-diet .active {\n  border: 1px solid #01A3A4;\n  box-shadow: 0px 4px 8px 0px #b3b3b3;\n}\nion-footerbar ion-row ion-col {\n  padding: 1rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVwZGF0ZS1zbG90LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLG9CQUFBO0FBREo7QUFFSTtFQUNJLGlCQUFBO0VBQ0EsY0FBQTtBQUFSO0FBRUk7RUFDQSxpQkFBQTtBQUFKO0FBQ0k7RUFDSSxnQkFBQTtBQUNSO0FBRWU7RUFDSSxpQkFBQTtBQUFuQjtBQUNpQjtFQUNHLFdBQUE7QUFDcEI7QUFDaUI7RUFDSSxjQUFBO0FBQ3JCO0FBQ2lCO0VBQ0csY0FBQTtBQUNwQjtBQUNpQjtFQUNHLGNBQUE7QUFDcEI7QUFPUTtFQUNJLHFCQUFBO0VBQ0EsNkJBQUE7RUFDQSwrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBTFo7QUFPVztFQUNDLHFCQUFBO0VBQ0EsNkJBQUE7RUFDQSwrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBTFo7QUFXSTtFQUNJLHlCQUFBO0VBQ0EsbUNBQUE7QUFUUjtBQWdCSTtFQUNHLGFBQUE7QUFiUCIsImZpbGUiOiJ1cGRhdGUtc2xvdC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuLnVwZGF0ZS1zbG90LWRpZXR7XG4gICAgLS1iYWNrZ3JvdW5kOiNGNkY3RkM7XG4gICAgaDJ7XG4gICAgICAgIGZvbnQtc2l6ZTogMS4ycmVtO1xuICAgICAgICBjb2xvcjojMjYzMTQzO1xuICAgIH1cbiAgICAuY29udGVudHtcbiAgICBtYXJnaW46MXJlbSAwcmVtO1xuICAgIGlvbi1jYXJke1xuICAgICAgICBib3gtc2hhZG93OiBub25lO1xuICAgICAgICBpb24tY2FyZC1oZWFkZXJ7XG4gICAgICAgICAgIGlvbi1yb3d7XG4gICAgICAgICAgICAgICBpb24tY29se1xuICAgICAgICAgICAgICAgICAgIGZvbnQtc2l6ZTogLjhyZW07XG4gICAgICAgICAgICAgICAgIHNwYW4uY2F0e1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjpncmF5O1xuICAgICAgICAgICAgICAgICB9IFxuICAgICAgICAgICAgICAgICBzcGFuLm9yZGVye1xuICAgICAgICAgICAgICAgICAgICAgY29sb3I6IzAwQjhGNjtcbiAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICBzcGFuLm9wdGlvbntcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6IzAxQTNBNDsgIFxuICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgIHNwYW4uZGlldHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6I0U2OTc0MDsgIFxuICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICB9XG4gICAgICAgICAgIFxuICAgICAgICB9XG4gICAgfVxuICAgIGlvbi1jYXJkLWNvbnRlbnR7XG4gICAgICAgIGlvbi1jaGVja2JveC5vcHRpb257XG4gICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDEwMCU7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQtY2hlY2tlZDogIzAxQTNBNDtcbiAgICAgICAgICAgIC0tYm9yZGVyLWNvbG9yLWNoZWNrZWQ6ICMwMUEzQTQ7XG4gICAgICAgICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgICAgICAgICB3aWR0aDogMjVweDtcbiAgICAgICAgICAgfVxuICAgICAgICAgICBpb24tY2hlY2tib3guZGlldHtcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZC1jaGVja2VkOiAjRTY5NzQwO1xuICAgICAgICAgICAgLS1ib3JkZXItY29sb3ItY2hlY2tlZDogI0U2OTc0MDtcbiAgICAgICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgICAgIHdpZHRoOiAyNXB4O1xuICAgICAgICAgICB9XG4gICAgfVxuICAgIH1cbiAgICBcbiAgICBcbiAgICAuYWN0aXZle1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMDFBM0E0O1xuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDhweCAwcHggI2IzYjNiMztcbiAgICB9XG5cbn1cblxuaW9uLWZvb3RlcmJhcntcbiAgICBpb24tcm93e1xuICAgIGlvbi1jb2x7XG4gICAgICAgcGFkZGluZzoxcmVtOyBcbn1cbn1cbn1cbiJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_diet-plan_update-slot_update-slot_module_ts.js.map