"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_admin_admin_module_ts"],{

/***/ 87992:
/*!***********************************************!*\
  !*** ./src/app/admin/admin-routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AdminPageRoutingModule": () => (/* binding */ AdminPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _admin_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./admin.page */ 32615);




const routes = [
    {
        path: '',
        component: _admin_page__WEBPACK_IMPORTED_MODULE_0__.AdminPage
    }
];
let AdminPageRoutingModule = class AdminPageRoutingModule {
};
AdminPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], AdminPageRoutingModule);



/***/ }),

/***/ 26916:
/*!***************************************!*\
  !*** ./src/app/admin/admin.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AdminPageModule": () => (/* binding */ AdminPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _admin_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./admin-routing.module */ 87992);
/* harmony import */ var _admin_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./admin.page */ 32615);







let AdminPageModule = class AdminPageModule {
};
AdminPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _admin_routing_module__WEBPACK_IMPORTED_MODULE_0__.AdminPageRoutingModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule
        ],
        declarations: [_admin_page__WEBPACK_IMPORTED_MODULE_1__.AdminPage]
    })
], AdminPageModule);



/***/ }),

/***/ 32615:
/*!*************************************!*\
  !*** ./src/app/admin/admin.page.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AdminPage": () => (/* binding */ AdminPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_admin_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./admin.page.html */ 16443);
/* harmony import */ var _admin_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./admin.page.scss */ 9879);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services */ 65190);
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ 3242);
/* harmony import */ var _shared_constants_constants__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/constants/constants */ 66239);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! moment */ 29243);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _core_constants_constants__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../core/constants/constants */ 92133);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 13252);












let AdminPage = class AdminPage {
    constructor(appService, iab, loaderService, formBuilder, router) {
        this.appService = appService;
        this.iab = iab;
        this.loaderService = loaderService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.customerId = "";
        this.accountIdRef = "";
        this.customerAmount = "";
        this.customerExpiryDate = "";
        this.refferalCode = "";
        this.current_year = moment__WEBPACK_IMPORTED_MODULE_5___default()().subtract(0, 'months').format('YYYY-MM-DD');
        this.max_year = moment__WEBPACK_IMPORTED_MODULE_5___default()().add(3, 'years').format('YYYY-MM-DD');
        this.username = "";
        this.refferalEmail = "";
        this.validation_messages = {
            email: [
                { type: "required", message: "Email Address is mandatory." },
                {
                    type: "pattern",
                    message: "Please enter valid Email Address",
                },
            ],
            customerId: [{ type: "required", message: "Enter customer id." },],
            accountIdRef: [{ type: "required", message: "Enter Account id." },],
            customerAmount: [{ type: "required", message: "Enter customer amount." },],
            customerDate: [{ type: "required", message: "Select expiry date." },],
        };
        this.dateValue = "";
        this.dateValue2 = "";
        this.navigateEmail = "";
        this.customerEmail = "";
        this.customerDietplanEmail = "";
        this.username = localStorage.getItem("loginEmail");
        this.navigateEmailForm = this.formBuilder.group({
            email: [
                null,
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required,
                ]),
            ],
        });
        this.customerEmailForm = this.formBuilder.group({
            email: [
                null,
                _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.compose([
                    _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required
                ]),
            ],
        });
        this.refferalEmailForm = this.formBuilder.group({
            accountIdRef: [""],
            email: [null],
            refferalCode: [""]
        });
        this.updateExpiryForm = this.formBuilder.group({
            customerId: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
            customerAmount: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
            customerDate: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
        });
    }
    addupdateCust() {
        this.router.navigate(['addupdate']);
    }
    CreateCustomDietPlan() {
        //console.log(this.customerDietplanEmail);
        // localStorage.setItem('email',this.customerDietplanEmail);
        this.router.navigate(['select-diet']);
    }
    ngOnInit() { }
    formatDate(value) {
        return moment__WEBPACK_IMPORTED_MODULE_5___default()(value).format("DD-MMM-YYYY");
    }
    updateUser() {
        localStorage.setItem("get_user_details", "true");
        let obj = {
            amount: this.customerAmount,
            customerEmail: this.customerId,
            expiryDate: this.dateValue2,
        };
        this.loaderService.presentLoader("Please Wait").then((_) => {
            this.appService.updateExpDate(obj).subscribe((response) => {
                this.loaderService.hideLoader();
                console.log(response);
                alert("User Updated");
                this.updateExpiryForm.reset();
            }, (error) => {
                this.loaderService.hideLoader();
                console.log("Category Error", error);
            });
        });
    }
    updateRefferalEmail() {
        if (!this.refferalEmail) {
            alert("Please Enter Customer ID");
        }
        else {
            let obj = {
                email: this.refferalEmail.includes('@') ? this.refferalEmail : "91-" + this.refferalEmail,
                accountId: this.accountIdRef,
                referralCode: this.refferalCode
            };
            console.log("obj", obj);
            this.loaderService.presentLoader("Please Wait").then((_) => {
                this.appService.updatRefferal(obj).subscribe((response) => {
                    this.loaderService.hideLoader();
                    console.log(response);
                    alert("User Updated with Referral Code: " + response.referralCode);
                    this.refferalEmail = "";
                }, (error) => {
                    this.loaderService.hideLoader();
                    console.log("Category Error", error);
                });
            });
        }
    }
    goToClientApp(token) {
        let link = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_4__.APIS.mainUrl + "?token=" + token;
        console.log("link", link);
        // let link = "http://localhost:8101/token="+token;
        this.iab.create(link, "_system", "location=yes");
    }
    updateCustomer() {
        if (!this.customerEmail) {
            alert("Please Enter Customer ID");
        }
        else {
            _core_constants_constants__WEBPACK_IMPORTED_MODULE_6__.CONSTANTS.email = this.customerEmail;
            localStorage.setItem("email", this.customerEmail);
            localStorage.setItem("userid", this.customerEmail);
            if (this.username !== 'beato') {
                this.router.navigate(['/tabs/consume']);
            }
            else {
                this.router.navigate(['/consume']);
            }
        }
    }
    getToken() {
        if (!this.navigateEmail) {
            alert("Please Enter Customer ID");
        }
        else {
            this.appService.getUserToken(this.navigateEmail).subscribe((response) => {
                console.log(response);
                // if (response) {
                //   this.storageService.set('refs',response);
                // }
            }, (error) => {
                // this.loaderService.hideLoader();
                console.log("User token ", error.error.text);
                this.goToClientApp(error.error.text);
                console.log("Category Error", error);
            });
        }
    }
};
AdminPage.ctorParameters = () => [
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_3__.InAppBrowser },
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.LoaderService },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router }
];
AdminPage.propDecorators = {
    datetime: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_9__.ViewChild, args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_10__.IonDatetime, { static: true },] }]
};
AdminPage = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: "app-admin",
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_admin_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_admin_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], AdminPage);



/***/ }),

/***/ 16443:
/*!******************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/admin/admin.page.html ***!
  \******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content>\n  <div class=\"ion-padding mainTopPadding\">\n    <ion-card [formGroup]=\"updateExpiryForm\" class=\"ion-margin-bottom-20 updateUserCard\">\n      <ion-card-header>\n        <ion-card-title color=\"theme\">Update User Expiry date</ion-card-title>\n      </ion-card-header>\n\n      <ion-card-content>\n        <ion-list lines=\"none\" class=\"ion-no-margin\">\n          <ion-item>\n            <ion-input\n            formControlName=\"customerId\"\n              placeholder=\"Enter Customer ID\"\n              [(ngModel)]=\"customerId\"\n            ></ion-input>\n          </ion-item>\n            <ng-container *ngFor=\"let validation of validation_messages.customerId\">\n              <span\n                class=\"ion-no-margin errMessageCss\"\n                *ngIf=\"updateExpiryForm.get('customerId').hasError(validation.type) && (updateExpiryForm.get('customerId').dirty || updateExpiryForm.get('customerId').touched)\"\n              >\n                {{ validation.message }}\n            </span>\n            </ng-container>\n          <ion-item>\n            <ion-input\n            formControlName=\"customerAmount\" placeholder=\"Enter Amount\" [(ngModel)]=\"customerAmount\"></ion-input>\n          </ion-item>\n            <ng-container  *ngFor=\"let validation of validation_messages.customerAmount\">\n              <span\n                class=\"ion-no-margin errMessageCss\"\n                *ngIf=\"updateExpiryForm.get('customerAmount').hasError(validation.type) && (updateExpiryForm.get('customerAmount').dirty || updateExpiryForm.get('customerAmount').touched)\"\n              >\n                {{ validation.message }}\n            </span>\n            </ng-container>\n          <ion-item>\n            <ion-input  id=\"open-modal\" formControlName=\"customerDate\" [value]=\"dateValue2\" placeholder=\"Select Expiry Date\"></ion-input>\n            <ion-modal trigger=\"open-modal\">\n              <ng-template>\n                <ion-content>\n                  <ion-datetime showDefaultButtons=\"true\" [min]=\"current_year\" [max]=\"max_year\" displayFormat=\"D MMM, YY\" pickerFormat=\"DD MMM YYYY\" #dateTime (ionChange)=\"dateValue2 = formatDate(dateTime.value)\" presentation=\"date\" color=\"theme\">\n                  </ion-datetime>\n                </ion-content>\n              </ng-template>\n            </ion-modal>\n          </ion-item>\n            <ng-container *ngFor=\"let validation of validation_messages.customerDate\">\n              <span\n                class=\"ion-no-margin errMessageCss\"\n                *ngIf=\"updateExpiryForm.get('customerDate').hasError(validation.type) && (updateExpiryForm.get('customerDate').dirty || updateExpiryForm.get('customerDate').touched)\"\n              >\n                {{ validation.message }}\n            </span>\n            </ng-container>\n          <ion-item class=\"update-btn\">\n            <ion-button\n            [disabled]=\"!updateExpiryForm.valid\"\n              (click)=\"updateUser()\"\n              shape=\"round\"\n              color=\"theme\"\n              fill=\"outline\"\n            >\n              Update\n            </ion-button>\n          </ion-item>\n        </ion-list>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-card [formGroup]=\"refferalEmailForm\" class=\"ion-margin-bottom-20\">\n      <ion-card-header>\n        <ion-card-title color=\"theme\">Activate Referral</ion-card-title>\n      </ion-card-header>\n\n      <ion-card-content>\n        <ion-list lines=\"none\" class=\"ion-no-margin\">\n          <ion-item style=\"border-bottom: 1px solid;\">\n            <ion-input style=\"border: none;\"\n            formControlName=\"accountIdRef\"\n              placeholder=\"Enter Account ID\"\n              [(ngModel)]=\"accountIdRef\"\n            ></ion-input>\n          </ion-item>\n            <!-- <ng-container *ngFor=\"let validation of validation_messages.accountIdRef\">\n              <span\n                class=\"ion-no-margin errMessageCss\"\n                *ngIf=\"refferalEmailForm.get('accountIdRef').hasError(validation.type) && (refferalEmailForm.get('accountIdRef').dirty || refferalEmailForm.get('accountIdRef').touched)\"\n              >\n                {{ validation.message }}\n            </span>\n            </ng-container> -->\n          <ion-item style=\"border-bottom: 1px solid;\">\n            91- <ion-input style=\"border: none;\"\n              formControlName=\"email\"\n              placeholder=\"Enter customer id\"\n              [(ngModel)]=\"refferalEmail\"\n            ></ion-input>\n          </ion-item>\n\n          <ion-item style=\"border-bottom: 1px solid;\">\n           <ion-input style=\"border: none;\"\n              formControlName=\"refferalCode\"\n              placeholder=\"Refferal Code\"\n              [(ngModel)]=\"refferalCode\"\n            ></ion-input>\n          </ion-item>\n          <!-- <div class=\"errMessageCss\" *ngIf=\"validation_messages\">\n            <ng-container *ngFor=\"let validation of validation_messages.email\">\n              <span\n                class=\"ion-no-margin\"\n                *ngIf=\"refferalEmailForm.get('email').hasError(validation.type) && (refferalEmailForm.get('email').dirty || refferalEmailForm.get('email').touched)\"\n              >\n                {{ validation.message }}\n            </span>\n            </ng-container>\n          </div> -->\n          <ion-item class=\"update-btn\">\n            <ion-button\n              [disabled]=\"!refferalEmailForm.valid\"\n              (click)=\"updateRefferalEmail()\"\n              shape=\"round\"\n              color=\"theme\"\n              fill=\"outline\"\n            >\n              Update\n            </ion-button>\n          </ion-item>\n        </ion-list>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-card [formGroup]=\"navigateEmailForm\" class=\"ion-margin-bottom-20\">\n      <ion-card-header>\n        <ion-card-title color=\"theme\">View Customer Details</ion-card-title>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-list lines=\"none\" class=\"ion-no-margin\">\n          <ion-item>\n            <!-- <ion-label  position=\"stacked\">Email Address :</ion-label> -->\n            <ion-input\n              formControlName=\"email\"\n              placeholder=\"Enter customer id\"\n              [(ngModel)]=\"navigateEmail\"\n            ></ion-input>\n          </ion-item>\n          <div class=\"errMessageCss\" *ngIf=\"validation_messages\">\n            <ng-container *ngFor=\"let validation of validation_messages.email\">\n              <!-- <span\n                class=\"ion-no-margin\"\n                *ngIf=\"navigateEmailForm.get('email').hasError(validation.type) && (navigateEmailForm.get('email').dirty || navigateEmailForm.get('email').touched)\"\n              >\n                {{ validation.message }}\n            </span> -->\n            </ng-container>\n          </div>\n          <ion-item class=\"update-btn\">\n            <ion-button\n              (click)=\"getToken()\"\n              shape=\"round\"\n              color=\"theme\"\n              fill=\"outline\"\n              [disabled]=\"!navigateEmailForm.valid\">\n              Go\n            </ion-button>\n          </ion-item>\n        </ion-list>\n      </ion-card-content>\n    </ion-card>\n    <ion-card [formGroup]=\"customerEmailForm\" class=\"ion-margin-bottom-20\">\n      <ion-card-header>\n        <ion-card-title color=\"theme\">Update Customer Diet Plan</ion-card-title>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-list lines=\"none\" class=\"ion-no-margin\">\n          <ion-item>\n            <!-- <ion-label  position=\"stacked\">Email Address :</ion-label> -->\n            <ion-input\n              formControlName=\"email\"\n              placeholder=\"Enter customer id\"\n              [(ngModel)]=\"customerEmail\"\n            ></ion-input>\n          </ion-item>\n          <div class=\"errMessageCss\" *ngIf=\"validation_messages\">\n            <ng-container *ngFor=\"let validation of validation_messages.email\">\n              <!-- <span\n                class=\"ion-no-margin\"\n                *ngIf=\"navigateEmailForm.get('email').hasError(validation.type) && (navigateEmailForm.get('email').dirty || navigateEmailForm.get('email').touched)\"\n              >\n                {{ validation.message }}\n            </span> -->\n            </ng-container>\n          </div>\n          <ion-item class=\"update-btn\">\n            <ion-button\n              (click)=\"updateCustomer()\"\n              shape=\"round\"\n              color=\"theme\"\n              fill=\"outline\"\n              [disabled]=\"!customerEmailForm.valid\">\n              Go\n            </ion-button>\n          </ion-item>\n        </ion-list>\n      </ion-card-content>\n    </ion-card>\n\n    <ion-card [formGroup]=\"customerEmailForm\" class=\"ion-margin-bottom-20\">\n      <ion-card-header>\n        <ion-card-title color=\"theme\">Create Custom Diet Plan</ion-card-title>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-list lines=\"none\" class=\"ion-no-margin\">\n          <ion-item>\n            <!-- <ion-label  position=\"stacked\">Email Address :</ion-label> -->\n            <ion-input\n              formControlName=\"email\"\n              placeholder=\"Enter customer id or Mobile number with 91-\"\n              [(ngModel)]=\"customerDietplanEmail\"\n            ></ion-input>\n          </ion-item>\n          <div class=\"errMessageCss\" *ngIf=\"validation_messages\">\n            <ng-container *ngFor=\"let validation of validation_messages.email\">\n              <!-- <span\n                class=\"ion-no-margin\"\n                *ngIf=\"navigateEmailForm.get('email').hasError(validation.type) && (navigateEmailForm.get('email').dirty || navigateEmailForm.get('email').touched)\"\n              >\n                {{ validation.message }}\n            </span> -->\n            </ng-container>\n          </div>\n          <ion-item class=\"update-btn\">\n            <ion-button\n              (click)=\"CreateCustomDietPlan()\"\n              shape=\"round\"\n              color=\"theme\"\n              fill=\"outline\"\n              [disabled]=\"!customerEmailForm.valid\">\n              Go\n            </ion-button>\n          </ion-item>\n        </ion-list>\n      </ion-card-content>\n    </ion-card>\n\n\n\n    <ion-card  class=\"ion-margin-bottom-20\">\n      <ion-card-header>\n        <ion-card-title color=\"theme\">Add/Update Customer</ion-card-title>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n<ion-button (click)=\"addupdateCust()\">Add/Update Customer</ion-button>\n          </ion-col>\n        </ion-row>\n      </ion-card-content>\n    </ion-card>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ 9879:
/*!***************************************!*\
  !*** ./src/app/admin/admin.page.scss ***!
  \***************************************/
/***/ ((module) => {

module.exports = ".update-btn {\n  float: right;\n  padding-top: 15px;\n}\n.update-btn ion-button {\n  width: 101px;\n  height: 36px;\n}\nion-content {\n  --background: #F6F7FC;\n}\nion-card {\n  height: 200px auto;\n  --background: #FFFFFF;\n  border-radius: 23px;\n  border: #707070;\n  padding: 10px;\n}\n.mainTopPadding {\n  padding-top: 60px;\n}\n.updateUserCard {\n  height: 290px auto !important;\n}\nion-input {\n  border-bottom: 1px solid #000;\n}\n.errMessageCss {\n  height: 26px;\n  font-size: small;\n  color: var(--ion-color-danger);\n  padding-left: 24px;\n  padding-top: 5px;\n  padding-bottom: 5px;\n}\n.hideError {\n  display: none;\n}\nion-datetime {\n  height: auto;\n  width: auto;\n  max-width: 350px;\n}\n.grid {\n  display: grid;\n  grid-template-columns: repeat(auto-fit, minmax(288px, 1fr));\n  grid-gap: 0 10px;\n}\nion-modal {\n  --width: 290px;\n  --height: 382px;\n  --border-radius: 8px;\n}\nion-modal ion-datetime {\n  height: 382px;\n}\n.modalContent {\n  --background: #f2f2f7;\n}\n.dateInputPlaceholder {\n  --padding-start: 25px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFkbWluLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLFlBQUE7RUFDQSxpQkFBQTtBQUNKO0FBQUk7RUFDSSxZQUFBO0VBQ0osWUFBQTtBQUVKO0FBQ0E7RUFDSSxxQkFBQTtBQUVKO0FBQUE7RUFDSSxrQkFBQTtFQUVBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtBQUVKO0FBQUE7RUFDSSxpQkFBQTtBQUdKO0FBREE7RUFDSSw2QkFBQTtBQUlKO0FBREE7RUFDSSw2QkFBQTtBQUlKO0FBRkE7RUFDSSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUtKO0FBSEE7RUFDSSxhQUFBO0FBTUo7QUFIQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBRUEsZ0JBQUE7QUFLSjtBQUZFO0VBQ0UsYUFBQTtFQUNBLDJEQUFBO0VBQ0EsZ0JBQUE7QUFLSjtBQUZFO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxvQkFBQTtBQUtKO0FBRkU7RUFDRSxhQUFBO0FBS0o7QUFIRTtFQUNFLHFCQUFBO0FBTUo7QUFKRTtFQUNFLGdDQUFBO0FBT0oiLCJmaWxlIjoiYWRtaW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnVwZGF0ZS1idG57XG4gICAgZmxvYXQ6cmlnaHQ7XG4gICAgcGFkZGluZy10b3A6IDE1cHg7XG4gICAgaW9uLWJ1dHRvbntcbiAgICAgICAgd2lkdGg6IDEwMXB4O1xuICAgIGhlaWdodDogMzZweDtcbiAgICB9XG59XG5pb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRjZGN0ZDO1xufVxuaW9uLWNhcmQge1xuICAgIGhlaWdodDogMjAwcHggYXV0bztcbiAgICAvLyB3aWR0aDogMzQxcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJvcmRlci1yYWRpdXM6IDIzcHg7XG4gICAgYm9yZGVyOiAjNzA3MDcwO1xuICAgIHBhZGRpbmc6IDEwcHg7XG59XG4ubWFpblRvcFBhZGRpbmcge1xuICAgIHBhZGRpbmctdG9wOiA2MHB4O1xufVxuLnVwZGF0ZVVzZXJDYXJke1xuICAgIGhlaWdodDogMjkwcHggYXV0byAhaW1wb3J0YW50O1xufVxuXG5pb24taW5wdXR7XG4gICAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICMwMDA7XG59XG4uZXJyTWVzc2FnZUNzcyB7XG4gICAgaGVpZ2h0OiAyNnB4O1xuICAgIGZvbnQtc2l6ZTogc21hbGw7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYW5nZXIpO1xuICAgIHBhZGRpbmctbGVmdDogMjRweDtcbiAgICBwYWRkaW5nLXRvcDogNXB4O1xuICAgIHBhZGRpbmctYm90dG9tOiA1cHg7XG59XG4uaGlkZUVycm9ye1xuICAgIGRpc3BsYXk6IG5vbmU7XG59XG5cbmlvbi1kYXRldGltZSB7XG4gICAgaGVpZ2h0OiBhdXRvO1xuICAgIHdpZHRoOiBhdXRvO1xuXG4gICAgbWF4LXdpZHRoOiAzNTBweDtcbiAgfVxuXG4gIC5ncmlkIHtcbiAgICBkaXNwbGF5OiBncmlkO1xuICAgIGdyaWQtdGVtcGxhdGUtY29sdW1uczogcmVwZWF0KGF1dG8tZml0LCBtaW5tYXgoMjg4cHgsIDFmcikpO1xuICAgIGdyaWQtZ2FwOiAwIDEwcHg7XG4gIH1cblxuICBpb24tbW9kYWwge1xuICAgIC0td2lkdGg6IDI5MHB4O1xuICAgIC0taGVpZ2h0OiAzODJweDtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDhweDtcbiAgfVxuXG4gIGlvbi1tb2RhbCBpb24tZGF0ZXRpbWUge1xuICAgIGhlaWdodDogMzgycHg7XG4gIH1cbiAgLm1vZGFsQ29udGVudHtcbiAgICAtLWJhY2tncm91bmQ6ICNmMmYyZjc7XG4gIH1cbiAgLmRhdGVJbnB1dFBsYWNlaG9sZGVye1xuICAgIC0tcGFkZGluZy1zdGFydDogMjVweCAhaW1wb3J0YW50O1xuICB9XG4iXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_admin_admin_module_ts.js.map