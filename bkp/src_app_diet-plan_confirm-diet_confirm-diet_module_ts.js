"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_diet-plan_confirm-diet_confirm-diet_module_ts"],{

/***/ 79331:
/*!***********************************************************************!*\
  !*** ./src/app/diet-plan/confirm-diet/confirm-diet-routing.module.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ConfirmDietPageRoutingModule": () => (/* binding */ ConfirmDietPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _confirm_diet_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./confirm-diet.page */ 28447);




const routes = [
    {
        path: '',
        component: _confirm_diet_page__WEBPACK_IMPORTED_MODULE_0__.ConfirmDietPage
    }
];
let ConfirmDietPageRoutingModule = class ConfirmDietPageRoutingModule {
};
ConfirmDietPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ConfirmDietPageRoutingModule);



/***/ }),

/***/ 13247:
/*!***************************************************************!*\
  !*** ./src/app/diet-plan/confirm-diet/confirm-diet.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ConfirmDietPageModule": () => (/* binding */ ConfirmDietPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _confirm_diet_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./confirm-diet-routing.module */ 79331);
/* harmony import */ var _confirm_diet_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./confirm-diet.page */ 28447);







let ConfirmDietPageModule = class ConfirmDietPageModule {
};
ConfirmDietPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _confirm_diet_routing_module__WEBPACK_IMPORTED_MODULE_0__.ConfirmDietPageRoutingModule
        ],
        declarations: [_confirm_diet_page__WEBPACK_IMPORTED_MODULE_1__.ConfirmDietPage]
    })
], ConfirmDietPageModule);



/***/ }),

/***/ 28447:
/*!*************************************************************!*\
  !*** ./src/app/diet-plan/confirm-diet/confirm-diet.page.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ConfirmDietPage": () => (/* binding */ ConfirmDietPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_confirm_diet_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./confirm-diet.page.html */ 45581);
/* harmony import */ var _confirm_diet_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./confirm-diet.page.scss */ 88242);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var src_app_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services */ 65190);






let ConfirmDietPage = class ConfirmDietPage {
    constructor(appService, router, route) {
        this.appService = appService;
        this.router = router;
        this.route = route;
        this.confirmDiet = [
            { diet: 'When you Wake up (Slot 0)', name: ['Drinks'] },
            { diet: 'Before Breakfast (slot 1)', name: ['Nuts'] },
            { diet: 'Breakfast (slot 2)', name: ['Snacks / Milk based snacks/ High protein snacks', 'Drink/Milk based drinks'] },
            { diet: 'Mid Day Meal (slot 3)', name: ['Fruits', 'Drink'] }
        ];
        this.isSelected = new Array(20);
        this.selectedName = "";
        this.isActive = -1;
        this.route.queryParams.subscribe(res => {
            this.selectedName = res.name;
        });
    }
    ngOnInit() {
        this.fetchSlotsDetails();
    }
    fetchSlotsDetails() {
        this.appService.fetchSlotsDetails(localStorage.getItem("email"), this.selectedName).subscribe(res => {
            console.log("res", res);
            this.confirmDiet = res;
        }, err => {
            console.log("error", err);
        });
    }
    gotoUpdate() {
        console.log("this.confirmDiet.metaDataDietPlan", this.confirmDiet.metaDataDietPlan);
        this.appService.updateSlotDetail(this.confirmDiet.metaDataDietPlan, localStorage.getItem("email"), this.selectedName).subscribe(res => {
            console.log("res", res);
        }, err => {
            console.log("error", err);
        });
    }
    gotoUpdateCat(index) {
        this.router.navigate(['update-slot'], { queryParams: { name: this.selectedName, slot: index } });
    }
    confirmedDiet(item, index) {
        console.log("item", item);
        this.confirmDiet.metaDataDietPlan[index].selected = item.selected;
        console.log("detail", this.confirmDiet.metaDataDietPlan);
        this.isActive = index;
    }
};
ConfirmDietPage.ctorParameters = () => [
    { type: src_app_services__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute }
];
ConfirmDietPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-confirm-diet',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_confirm_diet_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_confirm_diet_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], ConfirmDietPage);



/***/ }),

/***/ 45581:
/*!******************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/diet-plan/confirm-diet/confirm-diet.page.html ***!
  \******************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n\n<ion-content class=\"confirm-diet\">\n  <ion-row>\n    <ion-col class=\"ion-text-center\">\n      <h2 class=\"choose-diet\">Review and confrm Slots for </h2>\n      <h2 style=\"margin-top:0\">Diabetes Plan</h2>\n    </ion-col>\n  </ion-row>\n  \n  <ion-grid class=\"content\">\n    <ion-card clss=\"card-item\" *ngFor=\"let sdiet of confirmDiet.metaDataDietPlan;let sdt=index;\" >\n      <ion-card-header style=\"border-bottom:.5px solid #b3b3b3;padding:10px 0;\">\n       <ion-item lines=\"none\">\n         <div slot=\"start\" >\n          (Slot {{sdiet.slot}})\n            <ion-icon mode=\"ios\" name=\"arrow-forward\"></ion-icon>\n          <span (click)=\"gotoUpdateCat(sdt);\">  {{sdiet.name}}</span>\n            <br>\n            <ion-input [(ngModel)]=\"sdiet.time\" style=\"margin-top:10px;border:1px solid #b3b3b3;border-radius: 5px; padding:10px;width:60px;text-align: center;\"></ion-input>\n         \n         </div>\n         <ion-checkbox slot=\"end\" mode=\"md\" [(ngModel)]=\"sdiet.selected\" (click)=\"confirmedDiet(sdiet,sdt)\"></ion-checkbox>\n       </ion-item>\n      </ion-card-header>\n      <ion-card-content>\n        <ion-item  *ngFor=\"let name of sdiet.data;let nm=index;\">\n          <ion-label>{{name.name}}</ion-label>\n        </ion-item>\n      </ion-card-content>\n    </ion-card>\n   \n  </ion-grid>\n  </ion-content>\n  <ion-footerbar>\n  <ion-row>\n    <ion-col class=\"ion-text-center\">\n      <ion-button (click)=\"gotoUpdate()\" shape=\"round\" color=\"theme\" expand=\"block\" fill=\"outline\">Update</ion-button>\n    </ion-col>\n  </ion-row>\n  </ion-footerbar>\n");

/***/ }),

/***/ 88242:
/*!***************************************************************!*\
  !*** ./src/app/diet-plan/confirm-diet/confirm-diet.page.scss ***!
  \***************************************************************/
/***/ ((module) => {

module.exports = ".confirm-diet {\n  --background:#F6F7FC;\n}\n.confirm-diet h2 {\n  font-size: 1.2rem;\n  color: #263143;\n}\n.confirm-diet .content {\n  margin: 1rem 0rem;\n}\n.confirm-diet .content ion-card {\n  box-shadow: none;\n}\n.confirm-diet .content ion-card ion-card-header ion-item div {\n  min-width: 65%;\n  color: #01A3A4;\n  font-size: 0.8rem;\n  font-weight: 500;\n}\n.confirm-diet .content ion-card ion-card-header ion-item ion-checkbox {\n  --border-radius: 100%;\n  --background-checked: #01A3A4;\n  --border-color-checked: #01A3A4;\n  height: 25px;\n  width: 25px;\n}\n.confirm-diet .content ion-card ion-card-content {\n  padding-left: 0;\n}\n.confirm-diet .content ion-card ion-card-content ion-item div {\n  min-width: 75%;\n  font-size: 0.8rem;\n  font-weight: 500;\n  color: #363939;\n  white-space: normal;\n}\n.confirm-diet .content .active {\n  border: 1px solid #01A3A4;\n  box-shadow: 0px 4px 8px 0px #b3b3b3;\n}\nion-footerbar ion-row ion-col {\n  padding: 1rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbmZpcm0tZGlldC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxvQkFBQTtBQURKO0FBRUk7RUFDSSxpQkFBQTtFQUNBLGNBQUE7QUFBUjtBQUVJO0VBQ0EsaUJBQUE7QUFBSjtBQUNJO0VBQ0ksZ0JBQUE7QUFDUjtBQUdXO0VBQ0MsY0FBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0FBRFo7QUFHVztFQUNDLHFCQUFBO0VBQ0EsNkJBQUE7RUFDQSwrQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0FBRFo7QUFLSTtFQUNJLGVBQUE7QUFIUjtBQUtZO0VBQ0MsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUFIYjtBQVVJO0VBQ0kseUJBQUE7RUFDQSxtQ0FBQTtBQVJSO0FBZ0JJO0VBQ0csYUFBQTtBQWJQIiwiZmlsZSI6ImNvbmZpcm0tZGlldC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcblxuLmNvbmZpcm0tZGlldHtcbiAgICAtLWJhY2tncm91bmQ6I0Y2RjdGQztcbiAgICBoMntcbiAgICAgICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgICAgIGNvbG9yOiMyNjMxNDM7XG4gICAgfVxuICAgIC5jb250ZW50e1xuICAgIG1hcmdpbjoxcmVtIDByZW07XG4gICAgaW9uLWNhcmR7XG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgICAgIGlvbi1jYXJkLWhlYWRlcntcbiAgICAgICAgIFxuICAgICAgICBpb24taXRlbXtcbiAgICAgICAgICAgZGl2e1xuICAgICAgICAgICAgbWluLXdpZHRoOiA2NSU7XG4gICAgICAgICAgICBjb2xvcjogIzAxQTNBNDtcbiAgICAgICAgICAgIGZvbnQtc2l6ZTogLjhyZW07XG4gICAgICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgICAgICAgICB9XG4gICAgICAgICAgIGlvbi1jaGVja2JveHtcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZC1jaGVja2VkOiAjMDFBM0E0O1xuICAgICAgICAgICAgLS1ib3JkZXItY29sb3ItY2hlY2tlZDogIzAxQTNBNDtcbiAgICAgICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgICAgIHdpZHRoOiAyNXB4O1xuICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG4gICAgaW9uLWNhcmQtY29udGVudHtcbiAgICAgICAgcGFkZGluZy1sZWZ0OiAwO1xuICAgICAgICBpb24taXRlbXtcbiAgICAgICAgICAgIGRpdntcbiAgICAgICAgICAgICBtaW4td2lkdGg6IDc1JTtcbiAgICAgICAgICAgICBmb250LXNpemU6IC44cmVtO1xuICAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgICAgICAgICAgY29sb3I6IzM2MzkzOTtcbiAgICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIH1cbiAgICBcbiAgICBcbiAgICAuYWN0aXZle1xuICAgICAgICBib3JkZXI6IDFweCBzb2xpZCAjMDFBM0E0O1xuICAgICAgICBib3gtc2hhZG93OiAwcHggNHB4IDhweCAwcHggI2IzYjNiMztcbiAgICB9XG5cbn1cbn1cblxuaW9uLWZvb3RlcmJhcntcbiAgICBpb24tcm93e1xuICAgIGlvbi1jb2x7XG4gICAgICAgcGFkZGluZzoxcmVtOyBcbn1cbn1cbn1cbiJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_diet-plan_confirm-diet_confirm-diet_module_ts.js.map