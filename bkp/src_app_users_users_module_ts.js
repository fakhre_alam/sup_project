"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_users_users_module_ts"],{

/***/ 77564:
/*!***********************************************!*\
  !*** ./src/app/users/users-routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UsersPageRoutingModule": () => (/* binding */ UsersPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _users_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./users.page */ 90303);




const routes = [
    {
        path: '',
        component: _users_page__WEBPACK_IMPORTED_MODULE_0__.UsersPage
    }
];
let UsersPageRoutingModule = class UsersPageRoutingModule {
};
UsersPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], UsersPageRoutingModule);



/***/ }),

/***/ 96538:
/*!***************************************!*\
  !*** ./src/app/users/users.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UsersPageModule": () => (/* binding */ UsersPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _users_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./users-routing.module */ 77564);
/* harmony import */ var _users_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users.page */ 90303);







let UsersPageModule = class UsersPageModule {
};
UsersPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _users_routing_module__WEBPACK_IMPORTED_MODULE_0__.UsersPageRoutingModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule
        ],
        declarations: [_users_page__WEBPACK_IMPORTED_MODULE_1__.UsersPage]
    })
], UsersPageModule);



/***/ }),

/***/ 90303:
/*!*************************************!*\
  !*** ./src/app/users/users.page.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UsersPage": () => (/* binding */ UsersPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_users_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./users.page.html */ 97199);
/* harmony import */ var _users_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users.page.scss */ 43243);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services */ 65190);






let UsersPage = class UsersPage {
    constructor(formBuilder, appService) {
        this.formBuilder = formBuilder;
        this.appService = appService;
        this.submitted = false;
        this.userForm = this.formBuilder.group({
            username: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]],
            firstName: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]],
            lastName: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]],
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.email]],
            password: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]],
            phone: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]]
        });
    }
    get f() { return this.userForm.controls; }
    onSubmit() {
        // stop here if form is invalid
        if (this.userForm.invalid) {
            this.submitted = true;
            return;
        }
        this.userForm.value.userStatus = 0;
        this.userForm.value.id = 0;
        console.log("this.userForm.value", this.userForm.value);
        this.appService.userAdd(this.userForm.value).subscribe(success => {
            console.log("success", success);
            this.submitted = false;
        }, error => {
            console.log("error", error);
        });
        // display form values on success
    }
    onReset() {
        this.submitted = false;
        this.userForm.reset();
    }
    ngOnInit() {
    }
};
UsersPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormBuilder },
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.AppService }
];
UsersPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-users',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_users_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_users_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], UsersPage);



/***/ }),

/***/ 97199:
/*!******************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/users/users.page.html ***!
  \******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>Add User</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<ion-card>\n  <form [formGroup]=\"userForm\" >\n    <div class=\"form-group\">\n    <ion-list>\n      <ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&  (f.username.errors && f.username.touched) }\">\n        <ion-label position=\"floating\">Username</ion-label>\n        <ion-input formControlName=\"username\" placeholder=\"Username\"></ion-input>\n      </ion-item>\n      <div *ngIf=\"submitted &&  (f.username.errors && f.username.touched)\" class=\"invalid\">\n        <div *ngIf=\"f.username.errors.required\">Username is required</div>\n     </div>\n\n     <ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&   (f.firstName.errors && f.firstName.touched )}\">\n      <ion-label position=\"floating\">First Name</ion-label>\n      <ion-input formControlName=\"firstName\" placeholder=\"Firstname\"></ion-input>\n    </ion-item>\n    <div *ngIf=\"submitted &&  (f.firstName.errors && f.firstName.touched)\" class=\"invalid\">\n      <div *ngIf=\"f.firstName.errors.required\">Firstname is required</div>\n   </div>\n\n   <ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&   (f.lastName.errors && f.lastName.touched)}\">\n    <ion-label position=\"floating\">Last Name</ion-label>\n    <ion-input formControlName=\"lastName\" placeholder=\"Lastname\"></ion-input>\n  </ion-item>\n  <div *ngIf=\"submitted &&  (f.lastName.errors && f.lastName.touched)\" class=\"invalid\">\n    <div *ngIf=\"f.lastName.errors.required\">Lastname is required</div>\n </div>\n\n <ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&  (f.email.invalid && f.email.touched) }\">\n  <ion-label position=\"floating\">Email</ion-label>\n  <ion-input formControlName=\"email\" placeholder=\"email\"></ion-input>\n</ion-item>\n<div *ngIf=\"submitted &&  (f.email.invalid && f.email.touched)\" class=\"invalid\">\n  <div *ngIf=\"f.email.errors.required\">email is required</div>\n</div>\n\n<ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&   (f.password.errors && f.password.touched)}\">\n  <ion-label position=\"floating\">Password</ion-label>\n  <ion-input formControlName=\"password\" placeholder=\"password\"></ion-input>\n</ion-item>\n<div *ngIf=\"submitted &&  (f.password.errors && f.password.touched)\" class=\"invalid\">\n  <div *ngIf=\"f.password.errors.required\">password is required</div>\n</div>\n<ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&  (f.phone.errors && f.phone.touched)}\">\n  <ion-label position=\"floating\" >Phone</ion-label>\n  <ion-input formControlName=\"phone\" placeholder=\"phone\"></ion-input>\n</ion-item>\n<div *ngIf=\"submitted && (f.phone.errors && f.phone.touched)\" class=\"invalid\">\n  <div *ngIf=\"f.phone.errors.required\">phone is required</div>\n</div>\n\n   <ion-item>\n    <ion-button class=\"btn btn-primary mr-1\" (click)=\"onSubmit()\">Submit</ion-button>\n   </ion-item>  \n  </ion-list>\n</div>\n  </form>\n</ion-card>\n</ion-content>\n");

/***/ }),

/***/ 43243:
/*!***************************************!*\
  !*** ./src/app/users/users.page.scss ***!
  \***************************************/
/***/ ((module) => {

module.exports = ".is-invalid {\n  --border-color: red;\n}\n\n.invalid {\n  color: red;\n  padding-left: 1.2rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVzZXJzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTtFQUNJLG1CQUFBO0FBQUo7O0FBRUE7RUFDSSxVQUFBO0VBQ0Esb0JBQUE7QUFDSiIsImZpbGUiOiJ1c2Vycy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcbi5pcy1pbnZhbGlke1xuICAgIC0tYm9yZGVyLWNvbG9yOiByZWQ7XG59XG4uaW52YWxpZHtcbiAgICBjb2xvcjpyZWQ7XG4gICAgcGFkZGluZy1sZWZ0OiAxLjJyZW07XG4gICBcbn0iXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_users_users_module_ts.js.map