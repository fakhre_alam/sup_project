"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_modal_summary-update_summary-update_module_ts"],{

/***/ 30412:
/*!***********************************************************************!*\
  !*** ./src/app/modal/summary-update/summary-update-routing.module.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SummaryUpdatePageRoutingModule": () => (/* binding */ SummaryUpdatePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _summary_update_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./summary-update.page */ 7217);




const routes = [
    {
        path: '',
        component: _summary_update_page__WEBPACK_IMPORTED_MODULE_0__.SummaryUpdatePage
    }
];
let SummaryUpdatePageRoutingModule = class SummaryUpdatePageRoutingModule {
};
SummaryUpdatePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SummaryUpdatePageRoutingModule);



/***/ }),

/***/ 3573:
/*!***************************************************************!*\
  !*** ./src/app/modal/summary-update/summary-update.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SummaryUpdatePageModule": () => (/* binding */ SummaryUpdatePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _summary_update_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./summary-update-routing.module */ 30412);
/* harmony import */ var _summary_update_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./summary-update.page */ 7217);







let SummaryUpdatePageModule = class SummaryUpdatePageModule {
};
SummaryUpdatePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _summary_update_routing_module__WEBPACK_IMPORTED_MODULE_0__.SummaryUpdatePageRoutingModule
        ],
        declarations: [_summary_update_page__WEBPACK_IMPORTED_MODULE_1__.SummaryUpdatePage]
    })
], SummaryUpdatePageModule);



/***/ })

}]);
//# sourceMappingURL=src_app_modal_summary-update_summary-update_module_ts.js.map