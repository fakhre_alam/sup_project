"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_home_home_module_ts"],{

/***/ 53949:
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageRoutingModule": () => (/* binding */ HomePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.page */ 47464);




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_0__.HomePage
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], HomePageRoutingModule);



/***/ }),

/***/ 28245:
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageModule": () => (/* binding */ HomePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home-routing.module */ 53949);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.page */ 47464);







let HomePageModule = class HomePageModule {
};
HomePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _home_routing_module__WEBPACK_IMPORTED_MODULE_0__.HomePageRoutingModule
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_1__.HomePage]
    })
], HomePageModule);



/***/ }),

/***/ 47464:
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePage": () => (/* binding */ HomePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./home.page.html */ 12056);
/* harmony import */ var _home_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.page.scss */ 60968);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _services_storage_storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/storage/storage.service */ 3789);
/* harmony import */ var _services_app_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/app/app.service */ 61535);
/* harmony import */ var _app_app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../app/app.service */ 38198);








// import { NativeStorage } from '@awesome-cordova-plugins/native-storage/ngx';
let HomePage = class HomePage {
    constructor(router, storageService, appService, appS) {
        this.router = router;
        this.storageService = storageService;
        this.appService = appService;
        this.appS = appS;
    }
    ngAfterViewInit() {
        this.defaultDetail();
    }
    defaultDetail() {
        this.appService.defaultData().subscribe(res => {
            console.log("defaultData::", res);
            localStorage.setItem("defaultData", JSON.stringify(res));
        }, err => {
            console.log("eee", err);
        });
    }
    ngOnInit() {
        if (!localStorage.getItem("acess_token")) {
            this.router.navigate(["./login"]);
        }
        else {
            this.getRefs();
        }
    }
    gotoCustomer() {
        this.router.navigate(['/tabs/admin']);
    }
    getRefs() {
        this.appService.refs().subscribe((response) => {
            console.log("test-refs", response);
            if (response) {
                this.storageService.set('refs', response);
            }
        }, (error) => {
            // this.loaderService.hideLoader();
            console.log("Category Error", error);
        });
    }
    toSummaryLeads() {
        this.router.navigate(['/tabs/summary-lead']);
    }
    toAdmin() {
        this.router.navigate(['/tabs/admin']);
    }
    toSearch() {
        this.router.navigate(['deitician-search']);
    }
};
HomePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _services_storage_storage_service__WEBPACK_IMPORTED_MODULE_2__.StorageService },
    { type: _services_app_app_service__WEBPACK_IMPORTED_MODULE_3__.AppService },
    { type: _app_app_service__WEBPACK_IMPORTED_MODULE_4__.AppService }
];
HomePage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: "app-home",
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_home_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_home_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], HomePage);



/***/ }),

/***/ 12056:
/*!****************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/home/home.page.html ***!
  \****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- <ion-header> </ion-header> -->\n<ion-content scroll-y=\"false\">\n  <div class=\"ion-padding\">\n    <div class=\"mainTopPadding\">\n      <div class=\"ion-padding-top\">\n        <ion-card class=\"ion-no-margin\" button=\"true\" (click)=\"toAdmin()\">\n          <ion-item lines=\"none\">\n            <ion-thumbnail class=\"ion-no-margin\" slot=\"start\">\n              <img\n                class=\"cardImageSize\"\n                src=\"../../assets/images/home/home-admin.png\"\n              />\n            </ion-thumbnail>\n            <ion-label>\n              <h3 class=\"subTitleFont\">01. Administration</h3>\n              <p class=\"cardContentFont\">Your food preferences.</p>\n            </ion-label>\n          </ion-item>\n        </ion-card>\n      </div>\n      <div class=\"ion-padding-top\">\n        <ion-card class=\"ion-no-margin\" button=\"true\" (click)=\"toSearch()\">\n          <ion-item lines=\"none\">\n            <ion-thumbnail class=\"ion-no-margin\" slot=\"start\">\n              <img\n                class=\"cardImageSize\"\n                src=\"../../assets/images/home/home-admin.png\"\n              />\n            </ion-thumbnail>\n            <ion-label>\n              <h3 class=\"subTitleFont\">02. Customer Diet Management </h3>\n              <p class=\"cardContentFont\">User Profile</p>\n            </ion-label>\n          </ion-item>\n        </ion-card>\n      </div><br><br>\n      <div class=\"ion-padding-bottom\">\n        <ion-card (click)=\"toSummaryLeads()\" class=\"ion-no-margin\" button=\"true\">\n          <ion-item lines=\"none\">\n            <ion-thumbnail class=\"ion-no-margin\" slot=\"start\">\n              <img\n                class=\"cardImageSize\"\n                src=\"../../assets/images/home/home-leeds.png\"\n              />\n            </ion-thumbnail>\n            <ion-label>\n              <h3 class=\"subTitleFont\">03. Leads</h3>\n              <p class=\"cardContentFont\">\n                Your age, gender, height and weight.\n              </p>\n            </ion-label>\n          </ion-item>\n        </ion-card>\n      </div>\n      <div class=\"ion-padding-bottom ion-padding-top\">\n        <ion-card class=\"ion-no-margin\" button=\"true\" (click)=\"gotoCustomer()\">\n          <ion-item lines=\"none\">\n            <ion-thumbnail class=\"ion-no-margin\" slot=\"start\">\n              <img\n                class=\"cardImageSize\"\n                src=\"../../assets/images/home/home-customer.png\"\n              />\n            </ion-thumbnail>\n            <ion-label>\n              <h3 class=\"subTitleFont\">04. Customers</h3>\n              <p class=\"cardContentFont\">\n                Your daily routines & activity level.\n              </p>\n            </ion-label>\n          </ion-item>\n        </ion-card>\n      </div>\n    \n    </div>\n  </div>\n</ion-content>\n<!-- <ion-footer collapse=\"fade\">\n  <div>\n     <ion-button size=\"default\" shape=\"round\" color=\"theme\" expand=\"block\">Next</ion-button>\n  </div>\n</ion-footer> -->\n");

/***/ }),

/***/ 60968:
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/***/ ((module) => {

module.exports = "ion-card {\n  height: 120px;\n  --background: #FFFFFF;\n  border-radius: 23px;\n  border: #707070;\n  padding: 10px;\n}\n\nion-content {\n  --background: #F6F7FC;\n}\n\nion-thumbnail {\n  height: auto;\n  width: auto;\n  padding-right: 6px;\n}\n\nion-item {\n  padding: 10px 0px 10px 0px;\n}\n\n.cardImageSize {\n  width: 80.86px;\n  height: 79.87px;\n}\n\n.cardContentFont {\n  color: #737777 !important;\n}\n\n.mainTopPadding {\n  padding-top: 60px;\n}\n\nion-footer {\n  background: #FFFFFF;\n  padding: 16px 55px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUVBLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsYUFBQTtBQUFKOztBQUVBO0VBQ0kscUJBQUE7QUFDSjs7QUFFQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFDQTtFQUNJLDBCQUFBO0FBRUo7O0FBQUE7RUFDRyxjQUFBO0VBQ0EsZUFBQTtBQUdIOztBQURBO0VBQ0kseUJBQUE7QUFJSjs7QUFGQTtFQUNJLGlCQUFBO0FBS0o7O0FBSEE7RUFDSSxtQkFBQTtFQUNBLGtCQUFBO0FBTUoiLCJmaWxlIjoiaG9tZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tY2FyZCB7XG4gICAgaGVpZ2h0OiAxMjBweDtcbiAgICAvLyB3aWR0aDogMzQxcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJvcmRlci1yYWRpdXM6IDIzcHg7XG4gICAgYm9yZGVyOiAjNzA3MDcwO1xuICAgIHBhZGRpbmc6IDEwcHg7XG59XG5pb24tY29udGVudCB7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRjZGN0ZDO1xuXG59XG5pb24tdGh1bWJuYWlsIHtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgd2lkdGg6IGF1dG87XG4gICAgcGFkZGluZy1yaWdodDogNnB4O1xufVxuaW9uLWl0ZW0ge1xuICAgIHBhZGRpbmc6IDEwcHggMHB4IDEwcHggMHB4O1xufVxuLmNhcmRJbWFnZVNpemUge1xuICAgd2lkdGg6IDgwLjg2cHg7XG4gICBoZWlnaHQ6IDc5Ljg3cHg7XG59XG4uY2FyZENvbnRlbnRGb250e1xuICAgIGNvbG9yOiM3Mzc3NzcgIWltcG9ydGFudDtcbn1cbi5tYWluVG9wUGFkZGluZyB7XG4gICAgcGFkZGluZy10b3A6IDYwcHg7XG59XG5pb24tZm9vdGVye1xuICAgIGJhY2tncm91bmQ6ICNGRkZGRkY7XG4gICAgcGFkZGluZzogMTZweCA1NXB4O1xufVxuXG4iXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_home_home_module_ts.js.map