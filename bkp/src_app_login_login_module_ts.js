"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_login_login_module_ts"],{

/***/ 62359:
/*!***********************************************!*\
  !*** ./src/app/login/login-routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageRoutingModule": () => (/* binding */ LoginPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.page */ 60955);




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_0__.LoginPage
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ 69549:
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageModule": () => (/* binding */ LoginPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-routing.module */ 62359);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page */ 60955);







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _login_routing_module__WEBPACK_IMPORTED_MODULE_0__.LoginPageRoutingModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_1__.LoginPage]
    })
], LoginPageModule);



/***/ }),

/***/ 60955:
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPage": () => (/* binding */ LoginPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_login_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./login.page.html */ 99403);
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page.scss */ 6051);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services */ 65190);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 18346);







let LoginPage = class LoginPage {
    constructor(_FB, appService, storageService, router, loaderService) {
        this._FB = _FB;
        this.appService = appService;
        this.storageService = storageService;
        this.router = router;
        this.loaderService = loaderService;
        this.name = "fitrofy";
        this.validation_messages = {
            password: [
                { type: "required", message: "Password is mandatory." },
                {
                    type: "pattern",
                    message: "Use 8 or more characters with a mix of letters, numbers & symbols.",
                },
            ],
            email: [
                { type: "required", message: "Username is mandatory." },
                {
                    type: "pattern",
                    message: "Please enter valid Username",
                },
            ],
        };
        this.loginForm = this._FB.group({
            email: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required],
            password: ["", _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required],
        });
        localStorage.setItem("loginEmail", "");
        localStorage.setItem("acess_token", "");
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        if (localStorage.getItem("loginEmail") === 'fitelo' || localStorage.getItem("loginEmail") === 'traya' || localStorage.getItem("loginEmail") === 'kapiva') {
            this.router.navigate(["deitician-search"]);
        }
        else if (localStorage.getItem("acess_token")) {
            this.router.navigate(["./tabs/home"]);
        }
    }
    logIn() {
        let email = this.loginForm.controls["email"].value;
        localStorage.setItem("loginEmail", email);
        let password = this.loginForm.controls["password"].value;
        let obj = {
            username: email,
            password: password,
        };
        this.loaderService.presentLoader("Please Wait").then((_) => {
            this.appService.signIn(obj).subscribe((response) => {
                if (response) {
                    localStorage.setItem("email", "");
                    console.log("response", response.access_token.toString());
                    localStorage.setItem("acess_token", response.access_token);
                    this.loaderService.hideLoader();
                    if (localStorage.getItem("loginEmail") === 'fitelo' || localStorage.getItem("loginEmail") === 'traya' || localStorage.getItem("loginEmail") === 'kapiva' || localStorage.getItem("loginEmail") === 'beato') {
                        localStorage.setItem("email", "");
                        this.router.navigate(["deitician-search"]);
                    }
                    else {
                        this.router.navigate(["/tabs/home"]);
                    }
                }
                (error) => {
                    this.loaderService.hideLoader();
                };
            }, err => {
                this.loaderService.hideLoader();
            });
        });
    }
    showPassword() {
        this.hide = !this.hide;
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormBuilder },
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.StorageService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router },
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.LoaderService }
];
LoginPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: "app-login",
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_login_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], LoginPage);



/***/ }),

/***/ 99403:
/*!******************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/login/login.page.html ***!
  \******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content padding>\n  <div style=\"background:#0B7141;    border-bottom-left-radius: 1rem;\n  border-bottom-right-radius: 1rem;color:#fff; height: 40%;\n    padding-top: 20%;\">\n  <!-- <img\n    class=\"logo\"\n    src=\"assets/images/newlogo.png\"\n    alt=\"SDP Management logo\"\n  /> -->\n  <h1 class=\"ion-text-center\">Smart Diet Planner</h1>\n  <h1 class=\"ion-text-center ion-no-margin\">Management</h1>\n  </div>\n  <div class=\"login ion-padding-20\" style=\"padding-top: 30%;\">\n    <form [formGroup]=\"loginForm\" (ngSubmit)=\"logIn()\">\n      <div class=\"input-group ion-padding-bottom\">\n        <ion-item mode=\"ios\" class=\"ion-no-padding ion-padding-top\">\n          <ion-input\n            required=\"true\"\n            placeholder=\"Username\"\n            formControlName=\"email\"\n          ></ion-input>\n          <ion-icon\n            class=\"ion-no-margin ion-padding-end-10\"\n            name=\"person-outline\"\n            slot=\"start\"\n          ></ion-icon>\n        </ion-item>\n        <div class=\"errMessageCss\" *ngIf=\"validation_messages\">\n          <ng-container *ngFor=\"let validation of validation_messages.email\">\n            <p\n              class=\"ion-no-margin\"\n              *ngIf=\"loginForm.get('email').hasError(validation.type) && (loginForm.get('email').dirty || loginForm.get('email').touched)\"\n            >\n              {{ validation.message }}\n            </p>\n          </ng-container>\n        </div>\n        <ion-item mode=\"ios\" class=\"ion-no-padding\">\n          <ion-input\n            [type]=\"hide ? 'text' : 'password'\"\n            placeholder=\"Password\"\n            formControlName=\"password\"\n          ></ion-input>\n          <ion-icon\n            class=\"ion-no-margin ion-padding-end-10\"\n            name=\"lock-closed-outline\"\n            slot=\"start\"\n          ></ion-icon>\n          <ion-icon\n            class=\"ion-no-margin\"\n            color=\"medium\"\n            size=\"small\"\n            [name]=\"hide ? 'eye-off' : 'eye'\"\n            slot=\"end\"\n            (click)=\"showPassword()\"\n          ></ion-icon>\n        </ion-item>\n        <div class=\"errMessageCss\" *ngIf=\"validation_messages\">\n          <ng-container *ngFor=\"let validation of validation_messages.password\">\n            <p\n              class=\"ion-no-margin\"\n              *ngIf=\"loginForm.get('password').hasError(validation.type) && (loginForm.get('password').dirty || loginForm.get('password').touched)\"\n            >\n              {{ validation.message }}\n            </p>\n          </ng-container>\n        </div>\n      </div>\n      <ion-button\n        class=\"ion-padding-horizontal\"\n        size=\"default\"\n        shape=\"round\"\n        color=\"theme\"\n        expand=\"block\"\n        type=\"submit\"\n        fill=\"solid\"\n        style=\"--ion-color-base:#B7D340 !important;\"\n        [disabled]=\"!loginForm.valid\"\n        >Log In</ion-button\n      >\n    </form>\n  </div>\n</ion-content>\n");

/***/ }),

/***/ 6051:
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/***/ ((module) => {

module.exports = ".logo {\n  display: block;\n  margin: 5em auto;\n  width: 200px;\n}\n\n.errMessageCss {\n  height: 26px;\n  font-size: small;\n  color: var(--ion-color-danger);\n  padding-left: 40px;\n  padding-top: 5px;\n  padding-bottom: 5px;\n}\n\n.loginButton {\n  --color: #fff;\n  height: 48px;\n  width: 230px;\n  margin: auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGNBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7QUFDSjs7QUFrQkM7RUFDSyxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQWZOOztBQWtCRTtFQUNJLGFBQUE7RUFDQSxZQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7QUFmTiIsImZpbGUiOiJsb2dpbi5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9nbyB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWFyZ2luOiA1ZW0gYXV0bztcbiAgICB3aWR0aDogMjAwcHg7XG4gfVxuXG5cbi8vICAubG9naW4ge1xuLy8gICAgIGJhY2tncm91bmQ6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMSk7XG4vLyAgICAgYm9yZGVyLXJhZGl1czogMTBweDsgXG4vLyAgICAgbWFyZ2luOiAxZW07XG4vLyAgICAgcGFkZGluZzogMmVtIDVlbSA1ZW0gMmVtOyAgIFxuIFxuLy8gICAgIHAge1xuLy8gICAgICAgICAgIG1hcmdpbi1ib3R0b206IDNlbTsgXG4vLyAgICAgfVxuIFxuIFxuLy8gICAgIGJ1dHRvbiB7XG4vLyAgICAgICAgICAgbWFyZ2luOiAwIDAgMS41ZW0gMDtcbi8vICAgICB9XG4vLyAgfVxuIC5lcnJNZXNzYWdlQ3NzIHtcbiAgICAgIGhlaWdodDogMjZweDtcbiAgICAgIGZvbnQtc2l6ZTogc21hbGw7XG4gICAgICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWRhbmdlcik7XG4gICAgICBwYWRkaW5nLWxlZnQ6IDQwcHg7XG4gICAgICBwYWRkaW5nLXRvcDogNXB4O1xuICAgICAgcGFkZGluZy1ib3R0b206IDVweDtcbiAgfVxuXG4gIC5sb2dpbkJ1dHRvbiB7XG4gICAgICAtLWNvbG9yOiAjZmZmO1xuICAgICAgaGVpZ2h0OiA0OHB4O1xuICAgICAgd2lkdGg6IDIzMHB4O1xuICAgICAgbWFyZ2luOiBhdXRvO1xuICB9Il19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_login_login_module_ts.js.map