"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["default-src_app_diet-plan_update-slot-new_update-slot-new-routing_module_ts"],{

/***/ 96490:
/*!*****************************************************************************!*\
  !*** ./src/app/diet-plan/update-slot-new/update-slot-new-routing.module.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateSlotNewPageRoutingModule": () => (/* binding */ UpdateSlotNewPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _update_slot_new_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-slot-new.page */ 69709);




const routes = [
    {
        path: '',
        component: _update_slot_new_page__WEBPACK_IMPORTED_MODULE_0__.UpdateSlotNewPage
    }
];
let UpdateSlotNewPageRoutingModule = class UpdateSlotNewPageRoutingModule {
};
UpdateSlotNewPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], UpdateSlotNewPageRoutingModule);



/***/ }),

/***/ 69709:
/*!*******************************************************************!*\
  !*** ./src/app/diet-plan/update-slot-new/update-slot-new.page.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateSlotNewPage": () => (/* binding */ UpdateSlotNewPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_update_slot_new_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./update-slot-new.page.html */ 26581);
/* harmony import */ var _update_slot_new_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-slot-new.page.scss */ 30350);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var src_app_core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/utility/utilities */ 53533);
/* harmony import */ var src_app_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/app.service */ 38198);
/* harmony import */ var src_app_services__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services */ 65190);
/* harmony import */ var src_app_core_constants_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/core/constants/constants */ 92133);









let UpdateSlotNewPage = class UpdateSlotNewPage {
    constructor(utilities, route, router, appService, mainService) {
        this.utilities = utilities;
        this.route = route;
        this.router = router;
        this.appService = appService;
        this.mainService = mainService;
        this.updateSlot = [{ name: 'sabji', count: 14 }, { name: 'Curry', count: 14 }, { name: 'Raita', count: 14 }, { name: 'Bread', count: 14 }];
        this.slot = 1;
        this.slotName = '';
        this.bkpData = [];
        this.category = "";
        this.isShow = false;
        this.route.queryParams.subscribe(res => {
            console.log("response:-", res);
            this.slot = res.slot;
            this.slotName = res.name;
            this.category = res.category;
        });
    }
    ngOnInit() {
        this.fetchSlotsDetails();
    }
    fetchSlotsDetails() {
        console.log("Const", src_app_core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.dietDate);
        const payload = {
            email: localStorage.getItem("email"),
            slot: this.slot,
            dietPlan: this.slotName
        };
        this.mainService.getOptionsManagementNew(this.slot, 'fakhre.alam101@gmail.com', this.category, src_app_core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.dietDate, 'keto').then(res => {
            this.updateSlot = this.utilities.customSort(res);
            console.log("resmain", this.updateSlot);
            this.bkpData = JSON.stringify(this.updateSlot);
            for (let index = 0; index < this.updateSlot.length; index++) {
                this.updateSlot[index].order = 0;
            }
        }, err => {
            console.log("error", err);
        });
    }
    isVisibal(isA, isB) {
        if (!isA && !isB) {
            return false;
        }
        return true;
    }
    gotoUpdate() {
        this.router.navigate(['./confirm-diet']);
    }
    gotoDetail(item) {
        console.log("sdiet", item);
        this.router.navigate(['update-detail-slot'], { queryParams: { name: this.slotName, cat: item.category, slot: this.slot } });
    }
    reset() {
        const tempDa = this.updatePAyload(JSON.parse(this.bkpData));
        console.log("reset", tempDa);
        this.mainService.updateOptionsManagementNew(this.slot, 'fakhre.alam101@gmail.com', this.category, src_app_core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.dietDate, 'keto', tempDa).then(res => {
            console.log("update slot", res);
        }, err => {
            console.log("error", err);
        });
    }
    updatePAyload(data) {
        const tempData = [];
        console.log("this.updateSlot", this.updateSlot);
        for (let index = 0; index < data.length; index++) {
            if (data[index].option) {
                tempData.push({ "_id": data[index]._id });
            }
        }
        console.log("tempData", tempData);
        return tempData;
    }
    updateDiet() {
        const tempDa = this.updatePAyload(this.updateSlot);
        const reqPayload = {
            email: localStorage.getItem('email'),
            slot: this.slot,
            data: tempDa
        };
        console.log("update slot payload:", tempDa);
        this.mainService.updateOptionsManagementNew(this.slot, 'fakhre.alam101@gmail.com', 'D', '30092022', 'keto', tempDa).then(res => {
            console.log("update slot", res);
        }, err => {
            console.log("error", err);
        });
    }
};
UpdateSlotNewPage.ctorParameters = () => [
    { type: src_app_core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__.UTILITIES },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.ActivatedRoute },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: src_app_services__WEBPACK_IMPORTED_MODULE_4__.AppService },
    { type: src_app_app_service__WEBPACK_IMPORTED_MODULE_3__.AppService }
];
UpdateSlotNewPage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-update-slot-new',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_update_slot_new_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_update_slot_new_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], UpdateSlotNewPage);



/***/ }),

/***/ 26581:
/*!************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/diet-plan/update-slot-new/update-slot-new.page.html ***!
  \************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n<ion-content class=\"update-slot-diet\">\n  <ion-row>\n    <ion-col class=\"ion-text-center\">\n      <h2 class=\"choose-diet\"> Update  the categories for a \n        </h2>\n        <h2 style=\"margin-top:0\">\n          “Lunch slot ({{slot}})”\n        </h2>\n    </ion-col>\n  </ion-row>\n  \n  <ion-grid class=\"content\">\n    <ion-card clss=\"card-item\" >\n      <ion-card-header style=\"border-bottom:.5px solid #b3b3b3;padding:10px 0;\">\n        <ion-row style=\"padding:.5rem;\" >\n          <ion-col size=\"7\" class=\"ion-text-left\"><span class=\"cat\">Categories</span></ion-col>\n          <ion-col size=\"2\" class=\"ion-text-center\"><span class=\"order\">Order</span></ion-col>\n          <ion-col size=\"3\" class=\"ion-text-center\"><span class=\"option\">Select</span></ion-col>\n        </ion-row>\n      </ion-card-header>\n      <ion-card-content>\n        <div *ngFor=\"let sdiet of updateSlot;let sdt=index;\" >\n        <ion-row style=\"padding:.5rem;border-bottom:1px solid #b3b3b3;\" *ngIf=\"isVisibal(sdiet.option,sdiet.diet) && !isShow\">\n          <ion-col (click)=\"gotoDetail(sdiet)\" size=\"7\" class=\"ion-text-left\"><span>{{sdiet.Food}}</span> <br> <span style=\"color:#01A3A4\">{{sdiet.foodCount}}</span> <ion-icon style=\"color:#01A3A4\" name=\"arrow-forward\"></ion-icon></ion-col>\n          <ion-col size=\"2\" class=\"ion-text-center ion-align-self-center\">\n            <input style=\"height:50px;width:40px;border:1px solid #b3b3b3; border-radius:4px;text-align:center;padding:2px;\" type=\"number\" [(ngModel)]=\"sdiet.order\"/> </ion-col>\n          <ion-col size=\"3\" class=\"ion-text-center ion-align-self-center\">\n            <ion-checkbox class=\"option\" mode=\"md\" [disabled]=\"sdiet.diet?true:false\" [checked]=\"sdiet.diet?true:sdiet.option\" [(ngModel)]=\"sdiet.option\"></ion-checkbox>\n          </ion-col>\n       \n        </ion-row>\n\n        <ion-row style=\"padding:.5rem;border-bottom:1px solid #b3b3b3;\" *ngIf=\"isShow\">\n          <ion-col (click)=\"gotoDetail(sdiet)\" size=\"7\" class=\"ion-text-left\"><span>{{sdiet.Food}}</span> <br> <span style=\"color:#01A3A4\">{{sdiet.foodCount}}</span> <ion-icon style=\"color:#01A3A4\" name=\"arrow-forward\"></ion-icon></ion-col>\n          <ion-col size=\"2\" class=\"ion-text-center ion-align-self-center\">\n            <input style=\"height:50px;width:40px;border:1px solid #b3b3b3; border-radius:4px;text-align:center;padding:2px;\" type=\"number\" [(ngModel)]=\"sdiet.order\"/> </ion-col>\n          <ion-col size=\"3\" class=\"ion-text-center ion-align-self-center\">\n            <ion-checkbox class=\"option\" mode=\"md\" [disabled]=\"sdiet.diet?true:false\" [checked]=\"sdiet.diet?true:sdiet.option\" [(ngModel)]=\"sdiet.option\"></ion-checkbox>\n          </ion-col>\n          \n        </ion-row>\n      \n      </div>\n      <ion-row>\n        <ion-col class=\"ion-text-center\" (click)=\"isShow=!isShow;\">\n          <a style=\"color:#01A3A4; cursor:HAND;\">Show All Categories</a>\n        </ion-col>\n      </ion-row>\n      </ion-card-content>\n    </ion-card>\n  </ion-grid>\n  </ion-content>\n  <ion-footerbar>\n  <ion-row>\n    <ion-col class=\"ion-text-center\">\n      <ion-button shape=\"round\" color=\"theme\" expand=\"block\" fill=\"outline\" (click)=\"updateDiet()\">Update</ion-button>\n    </ion-col>\n    <ion-col>\n      <ion-button shape=\"round\" color=\"theme\" expand=\"block\" (click)=\"reset()\">Reset Default</ion-button>\n    </ion-col>\n  </ion-row>\n  </ion-footerbar>");

/***/ }),

/***/ 30350:
/*!*********************************************************************!*\
  !*** ./src/app/diet-plan/update-slot-new/update-slot-new.page.scss ***!
  \*********************************************************************/
/***/ ((module) => {

module.exports = ".update-slot-diet {\n  --background:#F6F7FC;\n}\n.update-slot-diet h2 {\n  font-size: 1.2rem;\n  color: #263143;\n}\n.update-slot-diet .content {\n  margin: 1rem 0rem;\n}\n.update-slot-diet .content ion-card {\n  box-shadow: none;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col {\n  font-size: 0.8rem;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.cat {\n  color: gray;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.order {\n  color: #00B8F6;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.option {\n  color: #01A3A4;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.diet {\n  color: #E69740;\n}\n.update-slot-diet .content ion-card-content {\n  padding-right: 0;\n}\n.update-slot-diet .content ion-card-content ion-checkbox.option {\n  --border-radius: 100%;\n  --background-checked: #01A3A4;\n  --border-color-checked: #01A3A4;\n  height: 25px;\n  width: 25px;\n}\n.update-slot-diet .content ion-card-content ion-checkbox.diet {\n  --border-radius: 100%;\n  --background-checked: #E69740;\n  --border-color-checked: #E69740;\n  height: 25px;\n  width: 25px;\n}\n.update-slot-diet .active {\n  border: 1px solid #01A3A4;\n  box-shadow: 0px 4px 8px 0px #b3b3b3;\n}\nion-footerbar ion-row ion-col {\n  padding: 1rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVwZGF0ZS1zbG90LW5ldy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxvQkFBQTtBQURKO0FBRUk7RUFDSSxpQkFBQTtFQUNBLGNBQUE7QUFBUjtBQUVJO0VBQ0EsaUJBQUE7QUFBSjtBQUNJO0VBQ0ksZ0JBQUE7QUFDUjtBQUVlO0VBQ0ksaUJBQUE7QUFBbkI7QUFDaUI7RUFDRyxXQUFBO0FBQ3BCO0FBQ2lCO0VBQ0ksY0FBQTtBQUNyQjtBQUNpQjtFQUNHLGNBQUE7QUFDcEI7QUFDaUI7RUFDRyxjQUFBO0FBQ3BCO0FBTUk7RUFDSSxnQkFBQTtBQUpSO0FBS1E7RUFDSSxxQkFBQTtFQUNBLDZCQUFBO0VBQ0EsK0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQUhaO0FBS1c7RUFDQyxxQkFBQTtFQUNBLDZCQUFBO0VBQ0EsK0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQUhaO0FBU0k7RUFDSSx5QkFBQTtFQUNBLG1DQUFBO0FBUFI7QUFjSTtFQUNHLGFBQUE7QUFYUCIsImZpbGUiOiJ1cGRhdGUtc2xvdC1uZXcucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG5cbi51cGRhdGUtc2xvdC1kaWV0e1xuICAgIC0tYmFja2dyb3VuZDojRjZGN0ZDO1xuICAgIGgye1xuICAgICAgICBmb250LXNpemU6IDEuMnJlbTtcbiAgICAgICAgY29sb3I6IzI2MzE0MztcbiAgICB9XG4gICAgLmNvbnRlbnR7XG4gICAgbWFyZ2luOjFyZW0gMHJlbTtcbiAgICBpb24tY2FyZHtcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgICAgaW9uLWNhcmQtaGVhZGVye1xuICAgICAgICAgICBpb24tcm93e1xuICAgICAgICAgICAgICAgaW9uLWNvbHtcbiAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IC44cmVtO1xuICAgICAgICAgICAgICAgICBzcGFuLmNhdHtcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6Z3JheTtcbiAgICAgICAgICAgICAgICAgfSBcbiAgICAgICAgICAgICAgICAgc3Bhbi5vcmRlcntcbiAgICAgICAgICAgICAgICAgICAgIGNvbG9yOiMwMEI4RjY7XG4gICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgc3Bhbi5vcHRpb257XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiMwMUEzQTQ7ICBcbiAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICBzcGFuLmRpZXR7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOiNFNjk3NDA7ICBcbiAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgfVxuICAgICAgICAgICBcbiAgICAgICAgfVxuICAgIH1cbiAgICBpb24tY2FyZC1jb250ZW50e1xuICAgICAgICBwYWRkaW5nLXJpZ2h0OiAwO1xuICAgICAgICBpb24tY2hlY2tib3gub3B0aW9ue1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6ICMwMUEzQTQ7XG4gICAgICAgICAgICAtLWJvcmRlci1jb2xvci1jaGVja2VkOiAjMDFBM0E0O1xuICAgICAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgICAgICAgIH1cbiAgICAgICAgICAgaW9uLWNoZWNrYm94LmRpZXR7XG4gICAgICAgICAgICAtLWJvcmRlci1yYWRpdXM6IDEwMCU7XG4gICAgICAgICAgICAtLWJhY2tncm91bmQtY2hlY2tlZDogI0U2OTc0MDtcbiAgICAgICAgICAgIC0tYm9yZGVyLWNvbG9yLWNoZWNrZWQ6ICNFNjk3NDA7XG4gICAgICAgICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgICAgICAgICB3aWR0aDogMjVweDtcbiAgICAgICAgICAgfVxuICAgIH1cbiAgICB9XG4gICAgXG4gICAgXG4gICAgLmFjdGl2ZXtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgIzAxQTNBNDtcbiAgICAgICAgYm94LXNoYWRvdzogMHB4IDRweCA4cHggMHB4ICNiM2IzYjM7XG4gICAgfVxuXG59XG5cbmlvbi1mb290ZXJiYXJ7XG4gICAgaW9uLXJvd3tcbiAgICBpb24tY29se1xuICAgICAgIHBhZGRpbmc6MXJlbTsgXG59XG59XG59XG4iXX0= */";

/***/ })

}]);
//# sourceMappingURL=default-src_app_diet-plan_update-slot-new_update-slot-new-routing_module_ts.js.map