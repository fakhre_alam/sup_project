"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_options_options_module_ts"],{

/***/ 57244:
/*!***************************************************!*\
  !*** ./src/app/options/options-routing.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OptionsPageRoutingModule": () => (/* binding */ OptionsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _options_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./options.page */ 38621);




const routes = [
    {
        path: '',
        component: _options_page__WEBPACK_IMPORTED_MODULE_0__.OptionsPage,
        children: [{
                path: '?/:param',
                component: _options_page__WEBPACK_IMPORTED_MODULE_0__.OptionsPage
            }]
    }
];
let OptionsPageRoutingModule = class OptionsPageRoutingModule {
};
OptionsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], OptionsPageRoutingModule);



/***/ }),

/***/ 55383:
/*!*******************************************!*\
  !*** ./src/app/options/options.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OptionsPageModule": () => (/* binding */ OptionsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _options_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./options-routing.module */ 57244);
/* harmony import */ var _options_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./options.page */ 38621);
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/components.module */ 57693);







// import { TranslateModule } from '@ngx-translate/core';

let OptionsPageModule = class OptionsPageModule {
};
OptionsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _options_routing_module__WEBPACK_IMPORTED_MODULE_0__.OptionsPageRoutingModule,
            // TranslateModule,
            _components_components_module__WEBPACK_IMPORTED_MODULE_2__.ComponentsModule
        ],
        declarations: [_options_page__WEBPACK_IMPORTED_MODULE_1__.OptionsPage]
    })
], OptionsPageModule);



/***/ }),

/***/ 38621:
/*!*****************************************!*\
  !*** ./src/app/options/options.page.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "OptionsPage": () => (/* binding */ OptionsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_options_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./options.page.html */ 23626);
/* harmony import */ var _options_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./options.page.scss */ 99750);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core/utility/utilities */ 53533);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/storage */ 78713);
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../app.service */ 38198);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/keyboard/ngx */ 95689);
/* harmony import */ var _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../core/constants/constants */ 92133);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../services */ 65190);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! moment */ 29243);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_7__);






// import { message, CONSTANTS } from "../core/constants/constants";








let OptionsPage = class OptionsPage {
    constructor(route, router, storage, utilities, appService, aps, alertController, keyboard) {
        this.route = route;
        this.router = router;
        this.storage = storage;
        this.utilities = utilities;
        this.appService = appService;
        this.aps = aps;
        this.alertController = alertController;
        this.keyboard = keyboard;
        this.heading = "Lunch options";
        this.slot = 0;
        this.constantText = "See more...";
        this.isAlertCame = false;
        this.isLoadFirst = false;
        this.requestBody = [];
        this.isMeal = "";
        this.optionData = [];
        this.segments = [];
        this.categories = [];
        this.jsonCreation = [];
        this.isShowName = "";
        this.foodCode = "";
        this.portions = "";
        this.selectedData = [];
        this.isSeeMore = false;
        this.seeMore = new Array(10);
        this.dataWithRef = [];
        this.optionSelection = 0;
        this.selectedCode = new Array(10);
        this.selectPortion = new Array(10);
        this.selectedCode1 = new Array(5);
        this.showHideItem = new Array(10);
        this.showHideItem1 = false;
        this.isCatOrOther = 0;
        this.onlyportion = 1;
        this.isNoDataInSearch = false;
        this.totalDataSearchedData = [];
        this.searchVal = "";
        this.xflag = 0;
        this.isKeyboardHide = false;
        this.isCategorySelected = false;
        this.selectedDietPlan = "weightLoss";
        this.username = "";
        this.isV = false;
        this.activeInd = 0;
        this.isFiterOption = false;
        this.isHeartShow = true;
        this.flagIndexforHealth = 0;
        this.itembyFilter = 0;
        this.mainData = new Array(10);
        this.searchKey = "";
        this.searchedWithGetData = false;
        this.isSearched = false;
        this.searchingData = false;
        this.segmentName = "";
        this.copyOfMainData = [];
        this.categoryLength = 0;
        this.dietPlanSlotData = [];
        this.unvrifiedData = [];
        this.diseases = "";
        this.image_URL = '';
        this.username = "";
        this.username = localStorage.getItem("loginEmail");
        this.storage.set("optionsData", null);
        // this.requestBody = [];
        this.route.queryParams.subscribe(res => {
            this.heading = res.param;
            this.slot = res.slot;
            this.foodCode = res.foodCode ? res.foodCode : "";
            this.portions = res.portion ? res.portion : "";
            this.isV = res.isV;
        });
        for (let index = 0; index < this.seeMore.length; index++) {
            this.seeMore[index] = "See more...";
        }
    }
    caloryChanged(ind, optionName) {
        this.activeInd = ind;
        this.requestBody = [];
        if (this.xflag < 2) {
            this.xflag = this.xflag + 1;
        }
        this.isMeal = optionName;
        this.categoryLength = this.optionData.mealOptions[this.activeInd].categories.length;
    }
    filterOption() {
        this.isFiterOption = !this.isFiterOption;
        if (this.isFiterOption == false) {
            this.selectedItemByFiter(0);
        }
    }
    selectedItemByFiter(ind) {
        this.itembyFilter = ind;
        if (ind == -1) {
            for (let index = 0; index < this.optionData.mealOptions[this.activeInd].categories.length; index++) {
                if (this.mainData[index] != null && this.mainData[index] != "") {
                    this.optionData.mealOptions[this.activeInd].categories[index].food = this.mainData[index];
                }
                else {
                    this.mainData[index] = [...this.optionData.mealOptions[this.activeInd].categories[index].food];
                }
            }
        }
        if (ind == 0) {
            this.getOptions();
            setTimeout(() => { this.caloryChanged(this.activeInd, this.isMeal); }, 1000);
        }
        else if (ind == 1) {
            for (let index = 0; index < this.optionData.mealOptions[this.activeInd].categories.length; index++) {
                if (this.mainData[index] != null && this.mainData[index] != "") {
                    this.optionData.mealOptions[this.activeInd].categories[index].food = this.mainData[index];
                }
                else {
                    this.mainData[index] = [...this.optionData.mealOptions[this.activeInd].categories[index].food];
                }
                this.optionData.mealOptions[this.activeInd].categories[index].food.sort((a, b) => {
                    return a.Calories - b.Calories;
                });
            }
        }
        if (ind == 2) {
            for (let index = 0; index < this.optionData.mealOptions[this.activeInd].categories.length; index++) {
                if (this.mainData[index] != null && this.mainData[index] != "") {
                    this.optionData.mealOptions[this.activeInd].categories[index].food = this.mainData[index];
                }
                else {
                    this.mainData[index] = [...this.optionData.mealOptions[this.activeInd].categories[index].food];
                }
                this.optionData.mealOptions[this.activeInd].categories[index].food.sort((a, b) => {
                    return b.Protien - a.Protien;
                });
            }
        }
        if (ind == 3) {
            for (let index = 0; index < this.optionData.mealOptions[this.activeInd].categories.length; index++) {
                if (this.mainData[index] != null && this.mainData[index] != "") {
                    this.optionData.mealOptions[this.activeInd].categories[index].food = this.mainData[index];
                }
                else {
                    this.mainData[index] = [...this.optionData.mealOptions[this.activeInd].categories[index].food];
                }
                this.optionData.mealOptions[this.activeInd].categories[index].food.sort((a, b) => {
                    return a.Fat - b.Fat;
                });
            }
        }
    }
    searchByInput(evt) {
        this.keyboard.hide();
        let filteredData = [];
        this.searchKey = evt.toLowerCase();
        this.isSearched = true;
        for (let index = 0; index < this.optionData.mealOptions[this.activeInd].categories.length; index++) {
            if (this.mainData[index] != null && this.mainData[index] != "") {
                this.optionData.mealOptions[this.activeInd].categories[index].food = this.mainData[index];
            }
            else {
                this.mainData[index] = [...this.optionData.mealOptions[this.activeInd].categories[index].food];
            }
            let data1 = this.optionData.mealOptions[this.activeInd].categories[index].food.filter(item => {
                return item.Food.toLowerCase().includes(evt.toLowerCase());
            });
            filteredData = [...filteredData, ...data1];
            this.optionData.mealOptions[this.activeInd].categories[index].food = data1;
            this.searchingData = false;
        }
        if (!filteredData.length) {
            this.searchingData = true;
            this.getAllForSearch(evt.toLowerCase(), true);
            this.searchedWithGetData = false;
            this.isNoDataInSearch = true;
        }
        else {
            this.searchingData = false;
            this.getAllForSearch(evt.toLowerCase(), false);
            this.searchedWithGetData = true;
            this.isNoDataInSearch = false;
        }
    }
    getAllForSearch(searchTerm, flag) {
        this.aps.getUserToken("91-9810152559").subscribe((response) => {
            console.log(response);
            // if (response) {
            //   this.storageService.set('refs',response);
            // }
        }, (error) => {
            // this.loaderService.hideLoader();
            console.log("User token ", error.error.text);
            this.appService.searchFoodItemByName(searchTerm, error.error.text).then((resp) => {
                var _a;
                this.totalDataSearchedData = resp["homeBased"];
                for (let index = 0; index < resp["homeBased"].length; index++) {
                    if (((_a = this.optionData.mealOptions[this.activeInd].categories[0].food.filter(item => {
                        return item.code === resp["homeBased"][index]._id;
                    })) === null || _a === void 0 ? void 0 : _a.length) == 0) {
                        this.optionData.mealOptions[this.activeInd].categories[0].food.push(resp["homeBased"][index]);
                    }
                }
                // if(flag) this.searchingData = this.totalDataSearchedData.length == 0 ? true : false;
            });
            console.log("Category Error", error);
        });
    }
    clearSearchAndReset() {
        this.isSearched = false;
        this.searchVal = "";
        this.searchByInput('');
        this.searchingData = false;
        this.searchedWithGetData = false;
        this.totalDataSearchedData = [];
    }
    // filterCodeOnTop(itemCode){
    //   const foodCodeArr = this.foodCode.split(",");
    //   foodCodeArr.forEach((code,ind)=>{
    //     if(itemCode === code){
    //       return code;
    //     }
    //   })
    // }
    // clearFilterForUpdate = false;
    fillDefaultData() {
        // this.clearFilterForUpdate = false;
        for (let index = 0; index < this.optionData.mealOptions[this.activeInd].categories.length; index++) {
            if (this.mainData[index] != null && this.mainData[index] != "") {
                this.optionData.mealOptions[this.activeInd].categories[index].food = this.mainData[index];
            }
            else {
                this.mainData[index] = [...this.optionData.mealOptions[this.activeInd].categories[index].food];
            }
            let data1 = this.optionData.mealOptions[this.activeInd].categories[index].food.filter(item => {
                return item.Food.toLowerCase().includes('');
            });
            this.optionData.mealOptions[this.activeInd].categories[index].food = data1;
        }
    }
    showHide(categoryIndex) {
        this.showHideItem[categoryIndex] = !this.showHideItem[categoryIndex];
        this.isSeeMore = this.showHideItem[categoryIndex];
        if (this.isSeeMore) {
            this.seeMore[categoryIndex] = "See less...";
        }
        else {
            this.fillDefaultData();
            this.seeMore[categoryIndex] = "See more...";
        }
    }
    showHide1() {
        this.showHideItem1 = !this.showHideItem1;
    }
    minusWeight(code) {
        console.log("this.optionSelection", this.optionSelection);
        if (this.optionSelection > 1) {
            this.optionSelection = this.optionSelection - 0.5;
        }
        if (this.requestBody.length == 0) {
            this.requestBody.push({ code });
        }
    }
    plusWeight(code) {
        if (this.optionSelection < 20) {
            this.optionSelection = this.optionSelection + 0.5;
            console.log("this.optionSelection", this.optionSelection);
        }
        if (this.requestBody.length == 0) {
            this.requestBody.push({ code });
        }
    }
    plusUnverified(n) {
        let isPortion = this.unvrifiedData[n].portion_unit.indexOf('portion') > -1;
        let baseUnit = {
            Calories: parseFloat((this.unvrifiedData[n].Calories / this.unvrifiedData[n].portion).toFixed(2)),
            Protien: parseFloat((this.unvrifiedData[n].Protien / this.unvrifiedData[n].portion).toFixed(2)),
            Fat: parseFloat((this.unvrifiedData[n].Fat / this.unvrifiedData[n].portion).toFixed(2)),
            Carbs: parseFloat((this.unvrifiedData[n].Carbs / this.unvrifiedData[n].portion).toFixed(2)),
            Fiber: parseFloat((this.unvrifiedData[n].Fiber / this.unvrifiedData[n].portion).toFixed(2)),
        };
        if (isPortion && this.unvrifiedData[n].portion < 20) {
            this.unvrifiedData[n].portion = this.unvrifiedData[n].portion + 0.5;
        }
        else if (this.unvrifiedData[n].portion < 750) {
            this.unvrifiedData[n].portion = this.unvrifiedData[n].portion + 25;
        }
        this.changeUnverified(this.unvrifiedData[n], n, baseUnit);
        if (this.requestBody.length == 0) {
            this.requestBody.push({ valid: true });
        }
        // if(this.unvrifiedData[n].toLowerCase() == 'external' || this.unvrifiedData[n].toLowerCase() == 'personal'){
        //   if (this.unvrifiedData[n].portion_unit == 0 && this.unvrifiedData[n]["portion"] < 20) {
        //     this.unvrifiedData[n]["portion"] = this.unvrifiedData[n]["portion"] + 0.5;
        //     this.unvrifiedData[n].Protien = parseFloat(Math.round(this.unvrifiedData[n]["Protien"] * this.unvrifiedData[n].portion * this.basePortionUnit).toFixed(2));
        //     this.unvrifiedData[n].Fat = parseFloat(Math.round(this.unvrifiedData[n]["Fat"] * this.unvrifiedData[n].portion * this.basePortionUnit).toFixed(2));
        //     this.unvrifiedData[n].Carbs = parseFloat(Math.round(this.unvrifiedData[n]["Carbs"] * this.unvrifiedData[n].portion * this.basePortionUnit).toFixed(2));
        //     this.unvrifiedData[n].Fiber = parseFloat(Math.round(this.unvrifiedData[n]["Fiber"] * this.unvrifiedData[n].portion * this.basePortionUnit).toFixed(2));
        //     this.unvrifiedData[n].Calories = parseFloat(Math.round(this.unvrifiedData[n]["Calories"] * this.unvrifiedData[n].portion * this.basePortionUnit).toFixed(2));
        //   }else if(this.unvrifiedData[n].portion_unit == 1 && this.unvrifiedData[n]["portion"] < 750) {
        //     this.unvrifiedData[n]["portion"] = this.unvrifiedData[n]["portion"] + 25;
        //     this.updateMeacros(false);
        //   }
        // }else{
        //   if (this.unvrifiedData[n].portion_unit == 0 && this.unvrifiedData[n]["portion"] < 20) {
        //     this.unvrifiedData[n]["portion"] = this.unvrifiedData[n]["portion"] + 0.5;
        //   }else if(this.unvrifiedData[n].portion_unit == 1 && this.unvrifiedData[n]["portion"] < 750) {
        //     this.unvrifiedData[n]["portion"] = this.unvrifiedData[n]["portion"] + 25;
        //   }
        //   this.updateMeacros(false);
        // }
    }
    minusUnverified(n) {
        let isPortion = this.unvrifiedData[n].portion_unit.indexOf('portion') > -1;
        let baseUnit = {
            // portion: this.unvrifiedData[n].portion,
            Calories: parseFloat((this.unvrifiedData[n].Calories / this.unvrifiedData[n].portion).toFixed(2)),
            Protien: parseFloat((this.unvrifiedData[n].Protien / this.unvrifiedData[n].portion).toFixed(2)),
            Fat: parseFloat((this.unvrifiedData[n].Fat / this.unvrifiedData[n].portion).toFixed(2)),
            Carbs: parseFloat((this.unvrifiedData[n].Carbs / this.unvrifiedData[n].portion).toFixed(2)),
            Fiber: parseFloat((this.unvrifiedData[n].Fiber / this.unvrifiedData[n].portion).toFixed(2)),
        };
        if (isPortion && this.unvrifiedData[n].portion > 1) {
            this.unvrifiedData[n].portion = this.unvrifiedData[n].portion - 0.5;
        }
        else if (this.unvrifiedData[n].portion > 50) {
            this.unvrifiedData[n].portion = this.unvrifiedData[n].portion - 25;
        }
        this.changeUnverified(this.unvrifiedData[n], n, baseUnit);
        if (this.requestBody.length == 0) {
            this.requestBody.push({ valid: true });
        }
    }
    delete(i, code, catName, optionName, k) {
        this.isCategorySelected = true;
        if (this.optionData.mealOptions[0].categories[i].food[k]['portion'] > 0) {
            this.optionData.mealOptions[0].categories[i].food[k]['portion'] = 0;
        }
        if (this.selectPortion[i] > 0) {
            this.selectPortion[i] = 0;
        }
        if (this.requestBody.length == 0) {
            this.requestBody.push({
                optionName,
                code,
                category: catName,
                portion: this.selectPortion[i]
            });
        }
        else {
            this.isCategorySelected = true;
            for (let index = 0; index < this.requestBody.length; index++) {
                if (this.requestBody[index].code == code) {
                    this.requestBody[index].portion = this.selectPortion[i];
                }
            }
            for (let index = 0; index < this.jsonCreation.length; index++) {
                if (this.jsonCreation[index].code == code) {
                    this.jsonCreation[index].portion = this.selectPortion[i];
                }
            }
            console.log("Json Creation", this.jsonCreation);
        }
    }
    minusWeight1(i, code, catName, optionName, k) {
        this.isCategorySelected = true;
        if (this.optionData.mealOptions[0].categories[i].food[k]['portion'] > 0) {
            this.optionData.mealOptions[0].categories[i].food[k]['portion'] = this.optionData.mealOptions[0].categories[i].food[k]['portion'] - 0.5;
        }
        if (this.selectPortion[i] > 0) {
            this.selectPortion[i] = this.selectPortion[i] - 0.5;
        }
        if (this.requestBody.length == 0) {
            this.requestBody.push({
                optionName,
                code,
                category: catName,
                portion: this.selectPortion[i]
            });
        }
        else {
            this.isCategorySelected = true;
            for (let index = 0; index < this.requestBody.length; index++) {
                if (this.requestBody[index].code == code) {
                    this.requestBody[index].portion = this.selectPortion[i];
                }
            }
            for (let index = 0; index < this.jsonCreation.length; index++) {
                if (this.jsonCreation[index].code == code) {
                    this.jsonCreation[index].portion = this.selectPortion[i];
                }
            }
            console.log("Json Creation", this.jsonCreation);
        }
    }
    plusWeight1(i, code, catName, optionName, k) {
        this.optionData.mealOptions[0].categories[i].food[k]['portion'] = this.optionData.mealOptions[0].categories[i].food[k]['portion'] ? this.optionData.mealOptions[0].categories[i].food[k]['portion'] : 1;
        if (parseFloat(this.optionData.mealOptions[0].categories[i].food[k]['portion']) < 20) {
            this.optionData.mealOptions[0].categories[i].food[k]['portion'] = parseFloat(this.optionData.mealOptions[0].categories[i].food[k]['portion']) + 0.5;
        }
        if (this.requestBody.length == 0) {
            this.requestBody.push({ valid: true });
        }
        // this.selectPortion[i] = this.selectPortion[i] ? this.selectPortion[i] : 1;
        // if (this.selectPortion[i] < 20) {
        //   this.selectPortion[i] = this.selectPortion[i] + 0.5;
        // }
        // if (this.requestBody.length == 0) {
        //   this.requestBody.push({
        //     optionName,
        //     code,
        //     category: catName,
        //     portion: this.selectPortion[i]
        //   });
        // } else {
        //   this.isCategorySelected = true;
        //   for (let index = 0; index < this.requestBody.length; index++) {
        //     if (this.requestBody[index].code == code) {
        //       this.requestBody[index].portion = this.selectPortion[i];
        //     }
        //   }
        //   for (let index = 0; index < this.jsonCreation.length; index++) {
        //     if (this.jsonCreation[index].code == code) {
        //       this.jsonCreation[index].portion = this.selectPortion[i];
        //     }
        //   }
        //   console.log("Json Creation", this.jsonCreation);
        // }
    }
    changeUnverified(value, n, baseUnit) {
        let limit = this.unvrifiedData[n].portion_unit.indexOf('portion') > -1 ? 20 : 750;
        let portion = this.unvrifiedData[n].portion;
        if (portion <= limit && portion > 0) {
            this.unvrifiedData[n]['portion'] = parseFloat(portion.toFixed(1));
            this.unvrifiedData[n]["Calories"] = this.unvrifiedData[n].portion * baseUnit['Calories'];
            this.unvrifiedData[n]["Protien"] = this.unvrifiedData[n].portion * baseUnit['Protien'];
            this.unvrifiedData[n]["Fat"] = this.unvrifiedData[n].portion * baseUnit['Fat'];
            this.unvrifiedData[n]["Carbs"] = this.unvrifiedData[n].portion * baseUnit['Carbs'];
            this.unvrifiedData[n]["Fiber"] = this.unvrifiedData[n].portion * baseUnit['Fiber'];
            this.requestBody.push(this.unvrifiedData[n]);
            // if(this.unvrifiedData[n].portion_unit.indexOf('portion') > -1){
            //   let basePortionUnit = Number((this.unvrifiedData[n].portion_unit.match(/\d/g)).join(""));
            //   this.unvrifiedData[n]["Protien"] = this.unvrifiedData[n].Protien*this.unvrifiedData[n].portion*basePortionUnit;
            //   this.unvrifiedData[n]["Fat"] = this.unvrifiedData[n].Fat*this.unvrifiedData[n].portion*basePortionUnit;
            //   this.unvrifiedData[n]["Carbs"] = this.unvrifiedData[n].Carbs*this.unvrifiedData[n].portion*basePortionUnit;
            //   this.unvrifiedData[n]["Fiber"] = this.unvrifiedData[n].Fiber*this.unvrifiedData[n].portion*basePortionUnit;
            //   this.unvrifiedData[n]["Calories"] = this.unvrifiedData[n].Calories*this.unvrifiedData[n].portion*basePortionUnit;
            //   this.requestBody.push(this.unvrifiedData[n]);
            // }else{
            //   let basePortionUnit = this.unvrifiedData[n].portion;
            //   this.unvrifiedData[n]["Calories"] = this.unvrifiedData[n].portion*baseUnit['Calories'];
            //   this.unvrifiedData[n]["Protien"] = this.unvrifiedData[n].portion*baseUnit['Protien'];
            //   this.unvrifiedData[n]["Fat"] = this.unvrifiedData[n].portion*baseUnit['Fat'];
            //   this.unvrifiedData[n]["Carbs"] = this.unvrifiedData[n].portion*baseUnit['Carbs'];
            //   this.unvrifiedData[n]["Fiber"] = this.unvrifiedData[n].portion*baseUnit['Fiber'];
            //   this.requestBody.push(this.unvrifiedData[n]);
            // }
        }
        else if (portion <= 0) {
            this.unvrifiedData[n]['portion'] = 0;
        }
    }
    unverifiedChange(value, n) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            // let value = parseFloat(eve.detail.value);
            let limit = this.unvrifiedData[n].portion_unit.indexOf('portion') > -1 ? 20 : 750;
            if (value > limit) {
                const alert = yield this.alertController.create({
                    cssClass: "Alert-class",
                    header: "Alert",
                    backdropDismiss: false,
                    // subHeader: 'Subtitle',
                    message: "Food portion can't be more then " + limit,
                    buttons: [{
                            text: 'Ok',
                            handler: (evt) => {
                                this.unvrifiedData[n]['portion'] = 0;
                            }
                        }]
                });
                yield alert.present();
            }
            else {
                this.changeUnverified(value, n, null);
                if (this.requestBody.length == 0) {
                    this.requestBody.push({ valid: true });
                }
            }
            // this.optionSelection = value.length > 1 ? value.substring(0, 1) : value;
        });
    }
    unverifiedChange1(value, n) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            // let value = parseFloat(eve.detail.value);
            if (value && value.currentTarget && value.currentTarget.value) {
                let speValue = value.currentTarget.value.split(".");
                if (speValue.length == 2) {
                    if (speValue[1].length == 1) {
                        value.preventDefault();
                    }
                }
            }
            // this.optionSelection = value.length > 1 ? value.substring(0, 1) : value;
        });
    }
    change(value, i, k) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            // let value = parseFloat(eve.detail.value);
            if (value > 20) {
                const alert = yield this.alertController.create({
                    cssClass: "Alert-class",
                    header: "Alert",
                    backdropDismiss: false,
                    // subHeader: 'Subtitle',
                    message: "Food portion can't be more then 20",
                    buttons: [{
                            text: 'Ok',
                            handler: (evt) => {
                                this.optionData.mealOptions[0].categories[i].food[k]['portion'] = 0;
                            }
                        }]
                });
                yield alert.present();
            }
            else if (value <= 20 && value > 0) {
                this.optionData.mealOptions[0].categories[i].food[k]['portion'] = parseFloat(value.toFixed(1));
            }
            else if (value <= 0) {
                this.optionData.mealOptions[0].categories[i].food[k]['portion'] = 0;
            }
            if (this.requestBody.length == 0) {
                this.requestBody.push({ valid: true });
            }
            // this.optionSelection = value.length > 1 ? value.substring(0, 1) : value;
        });
    }
    change1(value, i, k) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            // let value = parseFloat(eve.detail.value);
            if (value && value.currentTarget && value.currentTarget.value) {
                let speValue = value.currentTarget.value.split(".");
                if (speValue.length == 2) {
                    if (speValue[1].length == 1) {
                        value.preventDefault();
                    }
                }
            }
            // this.optionSelection = value.length > 1 ? value.substring(0, 1) : value;
        });
    }
    filterMeals(data, type) {
        return data.filter(item => {
            return item.category == type;
        });
    }
    filterdata(data, code) {
        const dataq = data.filter(item => {
            return item.code == code;
        });
    }
    segmentChanged(event) {
        this.requestBody = [];
        if (this.xflag < 2) {
            this.xflag = this.xflag + 1;
        }
        console.log("event:-  ", this.xflag);
    }
    ngOnInit() { }
    gotoSuggestion(name) {
        this.router.navigate(['suggestion'], {
            queryParams: {
                param: name
            }
        });
    }
    // showHideOption(index, name) {
    // console.log(name);
    // if (this.isShowName != name) {
    //   this.isShowName = name;
    // } else {
    //   this.isShowName = "";
    // }
    // }
    goToFoodDetails(food) {
        let portion = "";
        for (let index = 0; index < this.diets[this.slot].data.length; index++) {
            if (this.diets[this.slot].data.length > 1) {
                portion = portion + this.diets[this.slot].data[index].portion + ",";
            }
            else {
                portion = portion + this.diets[this.slot].data[index].portion;
            }
        }
        this.router.navigate(["food-detail"], {
            queryParams: {
                param: this.heading,
                slot: this.slot,
                foodCode: JSON.stringify(this.diets[this.slot].data),
                portion,
                mainCode: food._id,
                category: food.category
            }
        });
    }
    getOptions() {
        // this.appService.getOptions(this.slot,CONSTANTS.isDetox).then(
        //   success => {
        this.storage.get("optionsData").then((res) => {
            let success = res[this.slot];
            this.optionData = JSON.parse(JSON.stringify(success));
            // for(let i = 0; i < this.optionData.mealOptions.length;i++){
            //   for(let j = 0; j < this.optionData.mealOptions[i].categories.length;j++){
            //     this.optionData.mealOptions[i].categories[j]["food"].sort((a, b) =>{ return (a.Score < b.Score ? 1 : -1) })
            //   }
            // }
            this.isNoDataInSearch = false;
            this.searchKey = "";
            this.totalDataSearchedData = [];
            this.searchVal = "";
            const foodCodeArr = this.foodCode.split(",");
            const portions = this.portions.split(",");
            for (let i = 0; i < this.optionData.mealOptions.length; i++) {
                for (let j = 0; j < this.optionData.mealOptions[i].categories.length; j++) {
                    for (let k = 0; k < foodCodeArr.length; k++) {
                        this.optionData.mealOptions[i].categories[j]["food"].filter((ele) => {
                            if (ele.code == foodCodeArr[k]) {
                                ele.portion = portions[k];
                                return ele;
                            }
                        });
                    }
                }
            }
            this.copyOfMainData = this.optionData;
            console.log("optionData:-", this.optionData);
            this.categoryLength = this.optionData.mealOptions[this.activeInd].categories.length;
            this.segments = [];
            console.log("this.optionData.mealOptions.length", this.optionData.mealOptions.length);
            if (this.optionData.mealOptions.length == 1) {
                this.xflag = 0;
            }
            for (let index = 0; index < this.optionData.mealOptions.length; index++) {
                if (this.optionData.mealOptions[index].isCategory) {
                    console.log(" this.isShowName", this.isShowName);
                    this.getSelectedOption(index, this.optionData.mealOptions[index].optionName, this.optionData.mealOptions[index].categories);
                }
                this.isMeal = this.segmentName = this.optionData.mealOptions[this.activeInd]["optionName"];
                this.utilities.hideLoader();
            }
        }, err => {
            this.utilities.hideLoader();
        });
    }
    compare(a, b) {
        // Use toUpperCase() to ignore character casing
        const bandA = a.order;
        const bandB = b.order;
        let comparison = 0;
        if (bandA > bandB) {
            comparison = 1;
        }
        else if (bandA < bandB) {
            comparison = -1;
        }
        return comparison;
    }
    getSelectedOption(optionIndex, optionName, categories) {
        //   this.dataWithRef = data;
        //  this.utilities.logEvent("sort_other_options", {});       
        let roast = 0;
        const foodCodeArr = this.foodCode.split(",");
        const portions = this.portions.split(",");
        for (let k = 0; k < categories.length; k++) {
            for (let j = 0; j < foodCodeArr.length; j++) {
                for (let index = 0; index < categories[k].food.length; index++) {
                    if (categories[k].name != null &&
                        categories[k].food[index].code === foodCodeArr[j]) {
                        console.log("ddddd: ", k);
                        if (this.jsonCreation.length == 0) {
                            if (foodCodeArr.length == 1) {
                                for (let s = 0; s < foodCodeArr.length; s++) {
                                    this.jsonCreation.push({
                                        catName: "",
                                        code: foodCodeArr[s],
                                        optionName: optionName,
                                        portion: parseFloat(portions[s])
                                    });
                                }
                            }
                            else {
                                for (let s = 0; s < foodCodeArr.length - 1; s++) {
                                    this.jsonCreation.push({
                                        catName: "",
                                        code: foodCodeArr[s],
                                        optionName: optionName,
                                        portion: parseFloat(portions[s])
                                    });
                                }
                            }
                        }
                        if (roast == 0) {
                            roast = 1;
                            this.isShowName = categories[k].name;
                        }
                        this.jsonCreation[j].catName = categories[k].name;
                        this.selectedCode[k] = categories[k].food[index].code;
                        this.selectPortion[k] = parseFloat(portions[j]);
                        // new changes alam
                        categories[k].food[index].order = 1;
                        categories[k].food[index].selected = "checked";
                        this.isCatOrOther = 1;
                        if (optionIndex != 0 && this.isLoadFirst == true) {
                            this.isLoadFirst = false;
                            this.xflag = 0;
                        }
                        this.isMeal = optionName;
                        this.caloryChanged(optionIndex, optionName);
                        this.segmentName = optionName;
                        console.log("xxxxx: ", optionName);
                    }
                }
            }
            for (let index = 0; index < categories[k].food.length; index++) {
                if (categories[k].food[index].order == undefined) {
                    categories[k].food[index].order = 2;
                    //  data[index].selected = "false";
                }
            }
            categories[k].food.sort(this.compare);
        }
        console.log("this.selectPortion", this.selectPortion);
        //   return data;
    }
    getSelectedOption1(option) {
        //   this.dataWithRef = data;
        const foodCodeArr = this.foodCode.split(",");
        const portions = this.portions.split(",");
        for (let k = 0; k < option.length; k++) {
            if (option[k].food != undefined) {
                for (let j = 0; j < foodCodeArr.length; j++) {
                    for (let index = 0; index < option[k].food.length; index++) {
                        if (option[k].food[index].code === foodCodeArr[j]) {
                            // ----new change alam
                            this.selectedCode1[k] = option[k].food[index].code;
                            this.optionSelection = Number(portions[j]); // option[k].food[index].portion;
                            // --- new change alam
                            option[k].food[index].order = 1;
                            option[k].food[index].selected = "checked";
                            console.log(option[k].optionName);
                            this.isMeal = option[k].optionName;
                        }
                    }
                }
                for (let index = 0; index < option[k].food.length; index++) {
                    if (option[k].food[index].order == undefined) {
                        option[k].food[index].order = 2;
                    }
                }
                option[k].food.sort(this.compare);
            }
        }
        console.log("this.selectedCode1", this.selectedCode1[0]);
        //   return data;
    }
    firstDateIsPastDayComparedToSecond(firstDate, secondDate) {
        if (firstDate.setHours(0, 0, 0, 0) - secondDate.setHours(0, 0, 0, 0) >= 0) { //first date is in future, or it is today
            return false;
        }
        return true;
    }
    getWordPressOtions() {
        this.utilities.showLoading();
        this.appService.getOptionsManagement(this.slot, _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.email).then(success => {
            this.utilities.hideLoader();
            this.storage.get("optionsData").then((res) => {
                res = res ? res : {};
                res[this.slot] = success;
                this.storage.set("optionsData", res).then(() => {
                    this.getOptions();
                });
            });
        }, err => {
            console.log("Wordpress api Error: ", JSON.stringify(err));
        });
    }
    loadAllData() {
        if (!localStorage.getItem("loadAllOptions")) {
            localStorage.setItem("loadAllOptions", new Date().toString());
            this.getWordPressOtions();
        }
        else if (this.firstDateIsPastDayComparedToSecond(new Date(localStorage.getItem("loadAllOptions")), new Date())) {
            this.storage.set("optionsData", {}).then(() => {
                this.getWordPressOtions();
            });
        }
        else {
            this.storage.get("optionsData").then((res) => {
                if (!res) {
                    this.getWordPressOtions();
                }
                else if (!res[this.slot]) {
                    this.getWordPressOtions();
                }
                else {
                    this.getOptions();
                }
            });
        }
    }
    loadDietPlanSlotData() {
        this.storage.get("dietData").then((res) => {
            let planName = _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.isDetox ? "detox" : _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.selectedDietPlan;
            let dietPlan = res[moment__WEBPACK_IMPORTED_MODULE_7___default()(new Date()).format("DDMMYYYY")][planName];
            this.dietPlanSlotData = dietPlan["diets"][this.slot]["data"];
        });
    }
    loadUnverifiedFoodData() {
        this.storage.get("dietData").then((res) => {
            let planName = _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.isDetox ? "detox" : _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.selectedDietPlan;
            let dietPlan = res[moment__WEBPACK_IMPORTED_MODULE_7___default()(new Date()).format("DDMMYYYY")][planName];
            this.unvrifiedData = dietPlan["diets"][this.slot]["data"].filter((ele) => {
                return ele.foodSource && (ele.foodSource.toLowerCase() == 'personal' || ele.foodSource.toLowerCase() == 'external');
            });
        });
    }
    ionViewWillEnter() {
        this.storage.get("diets").then(diets => {
            this.diets = diets;
        });
        this.searchingData = false;
        this.selectedDietPlan = _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.selectedDietPlan;
        this.image_URL = _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.image_URL;
        // this.utilities.logEvent("DietPlan_05ViewOptions", {});
        // this.utilities.logEvent("search_other_options", {});   
        this.keyboard.onKeyboardWillShow().subscribe(() => {
            this.isKeyboardHide = true;
            // console.log('SHOWK');
        });
        this.keyboard.onKeyboardWillHide().subscribe(() => {
            this.isKeyboardHide = false;
            // console.log('HIDEK');
        });
        this.xflag = 0;
        this.selectedCode = new Array(10);
        this.selectPortion = new Array(10);
        this.selectedCode1 = new Array(5);
        this.showHideItem = new Array(10);
        for (let index = 0; index < this.showHideItem.length; index++) {
            this.showHideItem[index] = false;
        }
        this.isLoadFirst = true;
        this.loadAllData();
        this.loadDietPlanSlotData();
        this.loadUnverifiedFoodData();
        this.storage.get("localData").then(val => {
            var _a;
            console.log("profile pic", val);
            if (val != "") {
                let data = JSON.parse(val);
                let filteredDesease = (_a = data === null || data === void 0 ? void 0 : data.otherMaster) === null || _a === void 0 ? void 0 : _a.diseases.filter(item => {
                    return item.isSelected == true;
                });
                for (let index = 0; index < (filteredDesease === null || filteredDesease === void 0 ? void 0 : filteredDesease.length); index++) {
                    this.diseases = this.diseases + filteredDesease[index].value;
                    if (index < filteredDesease.length - 1) {
                        this.diseases = this.diseases + " or ";
                    }
                }
            }
        });
    }
    filterDring(value) { }
    presentAlertConfirm(message, moptionFood, option, optionName, category, code, portion, i) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: "my-custom-class",
                header: "Confirm!",
                message,
                buttons: [
                    {
                        text: "Close",
                        role: "cancel",
                        cssClass: "secondary",
                        handler: blah => { }
                    },
                    {
                        text: "Continue",
                        handler: () => {
                            this.isCategorySelected = true;
                            this.categoryItems(moptionFood, option, optionName, category, code, portion, i);
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    selectedMealCategory(moptionFood, option, optionName, category, code, portion, i, k) {
        if (this.isMeal != this.segmentName && this.xflag != 3) {
            this.xflag = 3;
            this.presentAlertConfirm("If you select in this option then your selection in the other option will be lost. Please confirm", moptionFood, option, optionName, category, code, portion, i).then(succes => {
                this.isAlertCame = true;
                console.log("success");
            }, err => {
                console.log("err", err);
            });
        }
        else {
            console.log("xflag", this.xflag);
            if (this.xflag == 0) {
                this.xflag = 1;
            }
            this.categoryItems(moptionFood, option, optionName, category, code, portion, i);
        }
        this.optionData.mealOptions[0].categories[i].food[k].selected = "checked";
        console.log("dadad", this.requestBody);
    }
    categoryItems(moptionFood, option, optionName, category, code, portion, i) {
        this.utilities.hideLoader();
        this.isCategorySelected = true;
        this.selectPortion[i] = portion;
        console.log("new datadat:- ", this.jsonCreation);
        if (this.requestBody.length > 0) {
            //    for (let index = 0; index < this.requestBody.length; index++) {
            let bodyData = this.requestBody.filter(item => {
                return item.category == category;
            });
            if (bodyData.length > 0) {
                for (let index = 0; index < this.requestBody.length; index++) {
                    if (bodyData[0].category == this.requestBody[index].category) {
                        this.requestBody[index].code = code;
                        this.requestBody[index].portion = portion;
                    }
                }
            }
            else {
                this.requestBody.push({
                    optionName,
                    code,
                    category,
                    portion
                });
                option.selected = "checked";
            }
            console.log("added Data:- ", this.requestBody);
            // }
        }
        else {
            option.selected = "checked";
            this.selectedCode[i] = code;
            this.requestBody.push({
                optionName,
                code,
                category,
                portion
            });
        }
        console.log("requestBody:-", this.requestBody);
    }
    selectedMeal(moptionFood, option, optionName, category, code, portion) {
        if (this.xflag == 2) {
            this.xflag = 3;
            this.utilities
                .presentAlertConfirm("If you select in this option then your selection in the other option will be lost. Please confirm")
                .then(succes => {
                this.isCategorySelected = false;
                this.fruitssnacks(moptionFood, option, optionName, category, code, portion);
            }, err => {
                console.log("err", err);
            });
        }
        else {
            this.fruitssnacks(moptionFood, option, optionName, category, code, portion);
        }
        console.log("dadad", this.requestBody);
    }
    removeByAttr(arr, attr, value) {
        let i = arr.length;
        while (i--) {
            if (arr[i] &&
                arr[i].hasOwnProperty(attr) &&
                arguments.length > 2 &&
                arr[i][attr] === value) {
                arr.splice(i, 1);
            }
        }
        return arr;
    }
    fruitssnacks(moptionFood, option, optionName, category, code, portion) {
        this.optionSelection = portion;
        console.log("optionName,category ,code:-", this.selectedCode1);
        for (let index = 0; index < moptionFood.length; index++) {
            moptionFood[index].selected = "false";
        }
        if (this.requestBody.length > 0) {
            for (let index = 0; index < this.requestBody.length; index++) {
                if (this.requestBody[index].category == null) {
                    // this.requestBody = [];
                }
                if (this.requestBody.length > 0) {
                    if (this.requestBody[index].code != code) {
                        this.requestBody.push({
                            optionName,
                            code,
                            category
                        });
                        option.selected = "checked";
                    }
                }
                else {
                    this.requestBody.push({
                        optionName,
                        code,
                        category
                    });
                    option.selected = "checked";
                }
            }
        }
        else {
            //  this.requestBody = [];
            //  this.filterdata(this.dataWithRef,code);
            option.selected = "checked";
            this.requestBody.push({
                optionName,
                code,
                category
            });
        }
    }
    back() {
        this.router.navigate(["consume"]);
    }
    gotoCounter() {
        this.router.navigate(['list-todays-calorie-count'], { queryParams: { selectedSlotIndex: Number(this.slot), searchKey: this.searchKey, fromFoodOptions: true, toMainPage: true } });
    }
    gotoCounterAddQuick() {
        this.router.navigate(['list-todays-calorie-count'], { queryParams: { selectedSlotIndex: Number(this.slot), searchKey: this.searchKey, fromFoodOptions: true, toMainPage: true, addQuick: true } });
    }
    goto() {
        let selectedPref = [];
        let selectedFoodItems = [];
        // needed for clear filtered data removed and back to non filter data 
        for (let index = 0; index < this.optionData.mealOptions[this.activeInd].categories.length; index++) {
            if (this.mainData[index] != null && this.mainData[index] != "") {
                this.optionData.mealOptions[this.activeInd].categories[index].food = this.mainData[index];
            }
            else {
                this.mainData[index] = [...this.optionData.mealOptions[this.activeInd].categories[index].food];
            }
            let data1 = this.optionData.mealOptions[this.activeInd].categories[index].food.filter(item => {
                return item.Food.toLowerCase().includes('');
            });
            this.optionData.mealOptions[this.activeInd].categories[index].food = data1;
        }
        let selectedFoodList = [];
        this.optionData.mealOptions[0].categories.forEach((ele) => {
            let foodItmes = ele.food.filter((ele1) => {
                return ele1["selected"] && ele1["selected"] == "checked" && ele1["portion"] != 0;
            });
            selectedFoodList = [...selectedFoodList, ...foodItmes];
        });
        if (selectedFoodList.length)
            selectedFoodItems = selectedFoodList[selectedFoodList.length - 1];
        selectedFoodList.forEach((ele) => {
            selectedPref.push({
                code: ele.code,
                portion: ele.portion != undefined ? parseFloat(ele.portion) : 1,
                eaten: ele.eaten ? ele.eaten : -1,
                foodSource: ele.foodSource ? ele.foodSource : "INTERNAL"
            });
        });
        let unverifiedList = this.unvrifiedData.filter((ele) => {
            if (ele.portion > 0) {
                let obj = {
                    "Food": ele['Food'],
                    "Calories": ele["Calories"],
                    "Carbs": ele["Carbs"],
                    "Fat": ele["Fat"],
                    "Protien": ele["Protien"],
                    "Fiber": ele["Fiber"],
                    "portion": ele["portion"],
                    "portion_unit": ele["portion_unit"],
                    "foodSource": ele["foodSource"],
                    "code": ele["_id"] ? ele["_id"] : ele["code"],
                    "eaten": ele["eaten"]
                };
                return obj;
            }
        });
        selectedPref.forEach((ele, index) => {
            this.dietPlanSlotData.forEach((ele1) => {
                if (ele.code == ele1.code) {
                    selectedPref[index]["eaten"] = ele1["eaten"];
                }
            });
        });
        if (this.isSearched) {
            for (let index = 0; index < this.requestBody.length; index++) {
                selectedPref.push({
                    code: this.requestBody[index].code,
                    portion: this.requestBody[index].portion,
                    eaten: -1,
                    foodSource: 'INTERNAL'
                });
            }
        }
        // need to test -- search data should merge with slot previous data(food item)
        selectedPref = [...selectedPref, ...unverifiedList];
        const itemCode = [...new Map(selectedPref.map(item => [item['code'], item])).values()];
        const reqData = {
            slot: Number(this.slot),
            foodCodeList: itemCode,
            detox: _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.isDetox,
            date: _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.dietDate,
            country: _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.country,
            isUpdateDiet: true
        };
        console.log("reqData Navaid:-", reqData);
        if (reqData.foodCodeList.length > 0) {
            this.utilities.presentLoading();
            this.appService.postOptionFoodList(reqData, _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.email).then(success => {
                this.requestBody = [];
                this.jsonCreation = [];
                console.log("post", success);
                this.storage.set("optionsData", this.optionData);
                this.fetchDiet(_core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.isDetox, _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.dietDate, success["dietplan"]);
            }, err => {
                this.utilities.hideLoader();
                console.log("post", err);
            });
        }
        else {
            this.utilities.presentAlert("Please select item with portion.");
        }
    }
    fetchDiet(isDetox, date, success) {
        console.log("------------- fetchDietPlan---------");
        this.appService.getDietPlans(isDetox, date, _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.country, _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.defaultCalories, true).then(success => {
            this.storage.get("dietData").then((res) => {
                if (res) {
                    res[date] = res[date] ? res[date] : {};
                    let planName = isDetox ? "detox" : _core_constants_constants__WEBPACK_IMPORTED_MODULE_5__.CONSTANTS.selectedDietPlan;
                    res[date][planName] = success;
                    this.storage.set("dietData", res).then(() => {
                    });
                    this.utilities.hideLoader();
                    if (this.username !== 'beato') {
                        this.router.navigate(["/tabs/consume"], { queryParams: { params: this.slot, refersh: new Date().getTime() } });
                    }
                    else {
                        this.router.navigate(["/consume"], { queryParams: { params: this.slot, refersh: new Date().getTime() } });
                    }
                }
            });
        }, error => {
            this.storage.set("localData", "");
            this.storage.set("profileData", "");
            this.utilities.hideLoader();
            console.log("DietPlan Error:", error);
            this.router.navigate(["profile"]);
        });
    }
    ngOnDestroy() {
    }
};
OptionsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.ActivatedRoute },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_10__.Storage },
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__.UTILITIES },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_3__.AppService },
    { type: _services__WEBPACK_IMPORTED_MODULE_6__.AppService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_11__.AlertController },
    { type: _ionic_native_keyboard_ngx__WEBPACK_IMPORTED_MODULE_4__.Keyboard }
];
OptionsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
        selector: "app-options",
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_options_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_options_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], OptionsPage);



/***/ }),

/***/ 23626:
/*!**********************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/options/options.page.html ***!
  \**********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- <ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>\n      {{heading}}\n    </ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <!-- <ion-title>\n      {{heading}}\n    </ion-title> -->\n    <ion-searchbar placeholder=\"Search Food For {{heading}}\" style=\"--background: #FFF;z-index: 13;top: 0px;\" enterkeyhint=\"search\" (search)=\"searchByInput($event.target.value)\" (ionClear)=\"clearSearchAndReset()\" [(ngModel)]=\"searchVal\">\n    </ion-searchbar>\n  </ion-toolbar>\n</ion-header>\n<ion-content style=\"--offset-bottom: 0px !important; background: #FFF !important;\">\n  <div class=\"parent\">\n    <!--   <div class=\"segment-top\">\n      <ion-row class=\"button-pos\">\n        <ion-col size=\"12\">\n          <ion-row class=\"ion-no-padding\">\n            <ion-col class=\"ion-no-padding x-button\" *ngFor=\"let segment of segments; let i=index;\">\n              <div [ngClass]=\"{'x-button-active-1':i!=activeInd}\" (click)=\"caloryChanged(i,segment.optionName)\">\n                {{segment.optionName}}\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row> -->\n\n    <!-- <ion-segment mode=\"md\" class=\"ion-color-segement border-round-corner shadow-segment\" (ionChange)=\"segmentChanged($event.target.value)\" >\n          <ion-segment-button mode=\"md\" *ngFor=\"let segment of segments; let i=index;\">\n            <ion-label>{{segment.optionName}}</ion-label>\n          </ion-segment-button>\n        </ion-segment> -->\n    <!-- </div> -->\n    <ion-row style=\"margin-bottom: 15px;\" *ngIf=\"!isNoDataInSearch\">\n      <ion-col class=\"ion-text-center\">\n        <span class=\"indication darkgreen-top recomended-for\"> Best</span>\n        <span class=\"indication light-green-top recomended-for\"> Good</span>\n        <span class=\"indication yellow-top recomended-for\"> Average</span>\n        <span class=\"indication yellow-orange-top recomended-for\"> Fair</span>\n        <span class=\"indication unverified-top recomended-for\"> Unverified</span>\n      </ion-col>\n      <!-- <ion-col class=\"ion-text-start\">\n        <span class=\"recomended-for\"> (for weight loss)</span>\n      </ion-col> -->\n    </ion-row>\n    <div class=\"recomendedfor-you\" *ngIf=\"!isNoDataInSearch\">\n      <ion-row *ngIf=\"flagIndexforHealth>0\"\n        [ngClass]=\"{'margin-top':categoryLength==1 || (isSeeMore && categoryLength>1) }\">\n        <ion-col class=\"ion-text-center ion-align-self-center\">\n          <img src=\"./assets/img/health1.svg\" style=\"vertical-align: middle;width:15px;margin-right:.2rem;\"> <span\n            class=\"recomended-for\">Recomended for {{diseases}}</span>\n        </ion-col>\n      </ion-row>\n      <ion-row *ngIf=\"selectedDietPlan == 'immunity_booster'\">\n        <ion-col class=\"ion-text-center ion-align-self-center\">\n          <img src=\"./assets/img/immunity.svg\" style=\"vertical-align: middle;width:15px;margin-right:.2rem;\"> <span\n            class=\"recomended-for\">Recomended for Immunity Booster</span>\n        </ion-col>\n      </ion-row>\n    </div>\n    <!-- <div class=\"search\" >\n      <ion-row>\n        <ion-col size=\"1\" class=\"ion-text-left ion-align-self-end\" (click)=\"filterOption()\">\n          <img src=\"./assets/img/filter.svg\">\n        </ion-col>\n        <ion-col size=\"11\" class=\"ion-text-left\" style=\"border-bottom:1px solid #b3b3b3;\">\n          <img src=\"./assets/img/search-icon.svg\" class=\"search-icon\">\n          <ion-input (input)=\"searchByInput($event.target.value)\" style=\"padding-left:2rem !important;\"></ion-input>\n        </ion-col>\n      </ion-row>\n    </div> -->\n    <div class=\"filter-by-selection\" *ngIf=\"!isNoDataInSearch\">\n      <!-- *ngIf=\"!isFiterOption && categoryLength==1 || (!isFiterOption && isSeeMore && categoryLength>1)\"> -->\n      <ion-row>\n        <ion-col class=\"ion-text-cneter ion-align-self-center\" (click)=\"selectedItemByFiter(0)\">\n          <span [ngClass]=\"{'add-color':itembyFilter==0}\">Recomended</span><br>\n          <!-- <div [ngClass]=\"{'select-item-by-filter':itembyFilter==0}\"></div> -->\n        </ion-col>\n        <ion-col class=\"ion-text-center ion-align-self-center\" (click)=\"selectedItemByFiter(1)\">\n          <span [ngClass]=\"{'add-color':itembyFilter==1}\">Low cal</span><br>\n          <!-- <div [ngClass]=\"{'select-item-by-filter':itembyFilter==1}\"></div> -->\n        </ion-col>\n        <ion-col class=\"ion-text-center ion-align-self-center\" (click)=\"selectedItemByFiter(2)\">\n          <span [ngClass]=\"{'add-color':itembyFilter==2}\">High Proteins</span><br>\n          <!-- <div [ngClass]=\"{'select-item-by-filter':itembyFilter==2}\"></div> -->\n        </ion-col>\n        <ion-col class=\"ion-text-center ion-align-self-center\" (click)=\"selectedItemByFiter(3)\">\n          <span [ngClass]=\"{'add-color':itembyFilter==3}\">Low fat</span><br>\n          <!-- <div [ngClass]=\"{'select-item-by-filter':itembyFilter==3}\"></div> -->\n        </ion-col>\n      </ion-row>\n    </div>\n\n    <!-- <div class=\"next-section\">\n      <div *ngIf=\"unvrifiedData && unvrifiedData.length > 0\">\n        <ion-card>\n          <ion-row>\n            <ion-col size=\"12\" class=\"text-left ion-align-self-end\">\n              <h3 class=\"small-heading\">\n                <span style=\"color:#737777;\">Unverified Items</span>\n              </h3>\n            </ion-col>\n          </ion-row>\n          <ion-item class=\"ion-no-padding\" *ngFor=\"let option of unvrifiedData;let n=index\">\n            <div slot=\"start\" class=\"legend legend-0\"></div>\n            <ion-thumbnail slot=\"start\">\n              <img src=\"./assets/img/food-item.png\" class=\"pic-food\" />\n            </ion-thumbnail>\n            <ion-label class=\"medium-text\">\n              <span style=\"color:#4C5264;font-weight: 400;\">{{option.Food}} ({{ onlyportion}}\n                {{option.portion_unit}})</span><br />\n              <span class=\"light-text\">\n                <div style=\"float: left;width:70px; white-space: nowrap;\">\n                  {{option.Calories | number : '1.2-2'}}\n                  cal</div>\n                <div style=\"float: left;width:100px; white-space: nowrap;\" *ngIf=\"itembyFilter==2\">\n                  {{option.Protien| number : '1.2-2' }} gm Protein</div>\n                <div style=\"float: left;width:70px;white-space: nowrap;\" *ngIf=\"itembyFilter==3\">\n                  {{option.Fat | number : '1.2-2'}} gm Fat </div>\n              </span>\n            </ion-label>\n            <div slot=\"end\" class=\"portion-entry\" style=\"margin-left: 22px;\">\n              <ion-row class=\"box-add\">\n                <ion-col size=\"3\" class=\"ion-text-center ion-align-self-center\">\n                  <span class=\"plus-minus\"\n                    (click)=\"minusUnverified(n)\">-</span>\n                </ion-col>\n                <ion-col size=\"6\" class=\"ion-text-center ion-align-self-center\">\n                  <ion-input type=\"number\" [(ngModel)]=\"option.portion\" ng-pattern=\"/^([0-9\\.]{1,2})?([.]?\\d{0,1})?$/)$/\"\n                    min=\"1\" max=\"2\" maxlength=\"1\" (ngModelChange)=\"unverifiedChange($event, n)\"  (keypress)=\"unverifiedChange1($event, n)\"\n                    style=\"background: #F6F7FC; color: black;background-color: white;\">\n                  </ion-input>\n                </ion-col>\n                <ion-col size=\"3\" class=\"ion-text-center ion-align-self-center\">\n                  <span class=\"plus-minus\" (click)=\"plusUnverified(n)\"\n                    style=\"font-size: 1.5rem;\">+</span>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-item>\n        </ion-card>\n      </div>\n    </div> -->\n   \n\n    <div class=\"next-section\">\n      <div *ngFor=\"let moption of optionData?.mealOptions;let j=index\">\n        <div *ngIf=\"moption.isCategory\">\n          <div *ngFor=\"let cat of moption.categories; let i=index;\">\n            <ion-card *ngIf=\"cat.food?.length>0\">\n\n\n              <span *ngIf=\"isMeal==moption.optionName\">\n                <ion-row>\n                  <ion-col size=\"12\" class=\"text-left ion-align-self-end\" style=\"margin-left: 10px;\">\n                    <h3 class=\"small-heading\">\n                      <span style=\"color:#01A3A5;\" *ngIf=\"moption.categories?.length>1\">{{cat.name}}</span>\n                      <span *ngIf=\"seeMore[i] != constantText || moption.categories?.length==1\">&nbsp;&nbsp;<a\n                          class=\"suggest\" (click)=\"gotoSuggestion(cat.name)\">(Suggest us!)</a></span>\n                      <!-- <span *ngIf=\"!cat.isOk\" style=\"color:red;\"\n                      >Please select!\n                    </span> -->\n\n                    </h3>\n                  </ion-col>\n                </ion-row>\n                <!-- <ion-radio-group [(ngModel)]=\"selectedCode[i]\"> -->\n                  <span *ngFor=\"let option of cat.food; let k=index\">\n                    <ion-item class=\"ion-no-padding\" style=\"margin: 10px;\"\n                      [ngClass]=\"{'hide-show':k>=2 && !showHideItem[i] && moption.categories?.length>1, 'custom-border-bottom':k==cat.food?.length-1 || k==1}\">\n                      <!-- <img src=\"./assets/img/health2.svg\" slot=\"start\"  class=\"pic-food-heart\" *ngIf=\"option.recommendedFor==undefined && flagIndexforHealth>0\" /> -->\n                      <div slot=\"start\" class=\"legend\" [ngClass]=\"{'legend-9':option.Score =='9', 'legend-6':option.Score =='6', 'legend-3': option.Score =='3' , 'legend-1':option.Score =='1',  'legend-0': !option.Score || option.Score == ''}\"></div>\n                      <ion-thumbnail slot=\"start\" (click)=\"goToFoodDetails(option)\">\n                        <!-- <img src=\"./assets/img/health1.svg\" class=\"pic-food-heart\"\n                          *ngIf=\"(option.recommendedFor?.length>0 && flagIndexforHealth>0) || option.Score =='9'\" /> -->\n                        <img src=\"./assets/img/health1.svg\" class=\"pic-food-heart\"\n                          *ngIf=\"selectedDietPlan != 'immunity_booster' && option.recommendedFor && option.recommendedFor.length>0\" />\n                        <img src=\"./assets/img/immunity.svg\" class=\"pic-food-heart\"\n                          *ngIf=\"selectedDietPlan == 'immunity_booster' && option.Score == '9'\" />\n                        <img src=\"./assets/img/health1.svg\" class=\"pic-food-heart\"\n                          *ngIf=\"selectedDietPlan == 'immunity_booster' && option.recommendedFor && option.recommendedFor.length> 0 && option.Score != '9'\" />\n                        <img src=\"{{image_URL}}{{option.code?option.code:option._id}}.jpg\" class=\"pic-food\" />\n                      </ion-thumbnail>\n                      <ion-label class=\"medium-text\" (click)=\"goToFoodDetails(option)\">\n                        <span style=\"color:#4C5264;font-weight: 400;\">{{option.Food}} ({{ onlyportion}}\n                          {{option.portion_unit}})</span><br />\n                        <span class=\"light-text\">\n                          <div style=\"float: left;width:70px; white-space: nowrap;\">\n                            {{option.Calories | number : '1.2-2'}}\n                            cal</div>\n                          <div style=\"float: left;width:100px; white-space: nowrap;\" *ngIf=\"itembyFilter==2\">\n                            {{option.Protien| number : '1.2-2' }} gm Protein</div>\n                          <div style=\"float: left;width:70px;white-space: nowrap;\" *ngIf=\"itembyFilter==3\">\n                            {{option.Fat | number : '1.2-2'}} gm Fat </div>\n\n                          <!-- <span *ngIf=\"itembyFilter==2\"> Protein</span>\n                      <span *ngIf=\"itembyFilter==3\">{{option.Fat}}gm Fat</span>  -->\n                        </span>\n                      </ion-label>\n                      <!-- <ion-radio slot=\"end\" value=\"{{option.code}}\" mode=\"md\"\n                        (click)=\"selectedMealCategory(cat.food,option,moption.optionName,cat.name,option.code,option.portion,i)\">\n                      </ion-radio> -->\n                      <ion-icon *ngIf=\"!option.selected\" name=\"ellipse-outline\" slot=\"end\" (click)=\"selectedMealCategory(cat.food,option,moption.optionName,cat.name,option.code?option.code:option._id,option.portion,i,k)\"></ion-icon>\n                      <div slot=\"end\" class=\"portion-entry\" *ngIf=\"option.selected\" style=\"margin-left: 22px;\">\n                        <ion-icon name=\"trash\" color=\"danger\" (click)=\"delete(i,option.code,cat.name,moption.optionName,k)\" style=\"    right: 3px;\n                        position: absolute;\n                        top: -21px;\"></ion-icon>\n                        <ion-row class=\"box-add\">\n                          <ion-col size=\"3\" class=\"ion-text-center ion-align-self-center\">\n                            <span class=\"plus-minus\"\n                              (click)=\"minusWeight1(i,option.code?option.code:option._id,cat.name,moption.optionName,k)\">-</span>\n                          </ion-col>\n                          <ion-col size=\"6\" class=\"ion-text-center ion-align-self-center\">\n                            <ion-input type=\"number\" [(ngModel)]=\"option.portion\" ng-pattern=\"/^([0-9\\.]{1,2})?([.]?\\d{0,1})?$/)$/\"\n                              min=\"1\" max=\"2\" maxlength=\"1\" (ngModelChange)=\"change($event, i, k)\"  (keypress)=\"change1($event, i, k)\"\n                              style=\"background: #F6F7FC; color: black;background-color: white;\">\n                            </ion-input>\n                          </ion-col>\n                          <ion-col size=\"3\" class=\"ion-text-center ion-align-self-center\">\n                            <span class=\"plus-minus\" (click)=\"plusWeight1(i,option.code?option.code:option._id,cat.name,moption.optionName,k)\"\n                              style=\"font-size: 1.5rem;\">+</span>\n                          </ion-col>\n                        </ion-row>\n                      \n                      </div>\n                    </ion-item>\n                    <ion-item lines=\"none\" *ngIf=\"k==2 && moption.categories?.length>1 && !this.showHideItem[i]\"\n                      (click)=\"showHide(i)\">\n                    <!-- class=\"border-bottom\"> -->\n                      <ion-label>\n                        <a style=\"text-align: center;display: block;\">\n                          <span style=\"color:#1492E6;font-size:.751rem;\">{{seeMore[i]}}</span>\n                        </a>\n                      </ion-label>\n                    </ion-item>\n                    <ion-item lines=\"none\"\n                      *ngIf=\"k==cat.food?.length-1 && moption.categories?.length>1 && this.showHideItem[i]\"\n                      (click)=\"showHide(i)\">\n                    <!-- class=\"border-bottom\"> -->\n                      <ion-label>\n                        <a style=\"text-align: center;display: block;\">\n                          <span style=\"color:#1492E6;font-size:.751rem;\">{{seeMore[i]}}</span>\n                        </a>\n                      </ion-label>\n                    </ion-item>\n                  </span>\n\n                <!-- </ion-radio-group> -->\n              </span>\n            </ion-card>\n          </div>\n        </div>\n        <div *ngIf=\"!moption.isCategory\">\n          <span *ngIf=\"isMeal==moption.optionName\">\n            <ion-radio-group [(ngModel)]=\"selectedCode1[j]\">\n              <span *ngFor=\"let option of moption.food let k=index;\">\n                <ion-item class=\"ion-no-padding\" [ngClass]=\"{'hide-show':k>=7  && !showHideItem1}\">\n\n                  <ion-thumbnail slot=\"start\" (click)=\"goToFoodDetails(option)\">\n                    <img src=\"{{image_URL}}{{option.code}}.jpg\" class=\"pic-food\" \n                    onerror=\"this.src='./assets/newImages/null.jpg';this.style='object-fit: contain'\" />\n                  </ion-thumbnail>\n                  <ion-label class=\"medium-text\" [ngClass]=\"{'custom-width':option.code==selectedCode1[j]}\" (click)=\"goToFoodDetails(option)\">\n                    {{option.Food}} <br />\n                    <span class=\"light-text\">\n                      <b>{{option.Calories}} cal </b>\n                      {{ option.portion}}\n                      {{option.portion_unit}}\n                    </span>\n                  </ion-label>\n                  <ion-radio slot=\"end\" value=\"{{option.code}}\" mode=\"md\"\n                    (click)=\"selectedMeal(moption.food,option,moption.optionName,null,option.code,option.portion)\">\n                  </ion-radio>\n\n                  <div slot=\"end\" class=\"portion-entry\" *ngIf=\"option.code==selectedCode1[j]\">\n                    <ion-row>\n                      <ion-col size=\"2\" class=\"text-center ion-align-self-center\">\n                        <img src=\"./assets/icons/minus.png\" (click)=\"minusWeight(option.code)\" />\n                      </ion-col>\n\n\n                      <ion-col size=\"8\" class=\"text-center ion-align-self-center\">\n                        <ion-input type=\"number\" [(ngModel)]=\"optionSelection\" ng-pattern=\"/^[0-9]{1,2})?$/\" min=\"1\"\n                          max=\"2\" maxlength=\"1\" readonly=\"true\" (ngModelChange)=\"change($event, null, k)\">\n                        </ion-input>\n                      </ion-col>\n                      <ion-col size=\"2\" class=\"text-center ion-align-self-center\">\n                        <img src=\"./assets/icons/plus1.png\" (click)=\"plusWeight(option.code)\" />\n\n                      </ion-col>\n                    </ion-row>\n                  </div>\n                </ion-item>\n                <ion-button (click)=\"showHide1()\" *ngIf=\"k==7\" style=\"position: absolute;right:15px;\">\n                  <span><a>More...</a> </span>\n                </ion-button>\n\n              </span>\n            </ion-radio-group>\n          </span>\n        </div>\n      </div>\n    </div>\n\n\n  </div>\n</ion-content>\n\n<ion-footer class=\"btn\" *ngIf=\"!isKeyboardHide\">\n  <ion-toolbar>\n    <!-- <div class=\"width-50\" slot=\"start\">\n      <ion-button mode=\"ios\" class=\"button button-cancel button-full\" shape=\"round\" (click)=\"back()\">\n        {{'button.back' |translate}}\n      </ion-button>\n    </div> -->\n    <!-- <ion-title></ion-title> -->\n    <div>\n      <ion-button mode=\"ios\" class=\"button button-dander button-full\" shape=\"round\" fill=\"outline\"\n        [disabled]=\"requestBody?.length==0\" (click)=\"goto()\">\n        Update\n        <!-- {{'button.next' |translate}} -->\n      </ion-button>\n    </div>\n  </ion-toolbar>\n</ion-footer>");

/***/ }),

/***/ 99750:
/*!*******************************************!*\
  !*** ./src/app/options/options.page.scss ***!
  \*******************************************/
/***/ ((module) => {

module.exports = "@font-face {\n  font-family: gt-america-standard-medium;\n  src: url(\"/assets/fonts/Roboto-Medium.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-regular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-bold;\n  src: url(\"/assets/fonts/Roboto-Bold.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-extendedMedium;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-light;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: Source-Sans-Pro-ragular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\nion-content {\n  --background: #F6F7FC !important;\n}\n.border-bottom {\n  border-bottom: 1px solid #b3b3b3;\n}\n.button-pos {\n  padding-top: 5px;\n}\n.x-button {\n  width: 100%;\n  height: 33px;\n  font-weight: bold;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n  text-align: center;\n  color: #01A3A4;\n  padding-top: 0;\n}\n.filter-by-selection span {\n  font-size: 0.7rem;\n  color: #4C5264;\n  line-height: 2;\n  padding: 12px 5px 5px;\n}\n.filter-by-selection ion-col {\n  height: 40px;\n}\n.custom-border-bottom {\n  border-bottom: none !important;\n  --border-color:transparent;\n}\n.suggest {\n  right: 10px;\n  position: absolute;\n  color: #1492E6;\n  font-size: 0.751rem;\n  top: 15px;\n  margin-right: 1.5rem;\n}\n.search {\n  position: relative;\n  height: 60px;\n  margin-bottom: 1rem;\n}\n.margin-top-3 {\n  margin-top: 3rem !important;\n}\n.recomendedfor-you {\n  margin-top: 1rem;\n  margin-bottom: 1rem;\n}\n.recomended-for {\n  color: #707070;\n  font-size: 0.8rem;\n}\n.add-color {\n  color: #01A3A5 !important;\n  border-bottom: solid 3px #01A3A5 !important;\n}\n.select-item-by-filter {\n  height: 4px;\n  background: #01A3A5;\n  width: 60%;\n  display: inline-block;\n}\n.x-button-active-1 {\n  width: 100%;\n  height: 33px;\n  vertical-align: middle;\n  text-align: center;\n  padding-top: 5px;\n  background: #F6F7FC;\n  box-shadow: 0 5px 5px rgba(0, 0, 0, 0.16);\n  color: #707070;\n  font-family: \"gt-america-standard-bold\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.search-icon {\n  vertical-align: bottom;\n  display: inline-block;\n  position: absolute;\n  top: 10px;\n}\n.box-add {\n  border: 1px solid #01A3A5;\n  border-radius: 5px;\n  height: 30px;\n}\n.pic-food-heart {\n  width: 15px;\n  height: auto;\n  left: 51px;\n  top: 2px;\n  position: absolute;\n}\n.seprate {\n  position: absolute;\n  left: 115px;\n  top: 11px;\n}\n.plus-minus {\n  margin: 0px;\n  color: #01A3A5;\n  font-size: 1.7rem;\n  position: relative;\n  top: -3px;\n  font-weight: 600;\n}\n.more-button {\n  position: absolute;\n  right: 15px;\n  padding: 0;\n  height: 20px;\n  width: 85%;\n}\nion-header ion-toolbar {\n  --background: #01A3A4;\n  --color: #fff;\n  --font-size: 1.5rem;\n  --font-family: $font;\n}\n.small-heading {\n  font-size: 0.875rem;\n  margin: 0.6rem 0;\n  cursor: pointer;\n}\n.small-text {\n  font-size: 0.875rem;\n  color: #b3b3b3;\n}\n.parent {\n  margin: 2.2rem 1rem;\n}\nion-thumbnail {\n  display: inline-block;\n  margin: 0.5rem 0;\n  margin-right: 0.7rem;\n}\nion-thumbnail img {\n  border-radius: 1rem;\n}\nion-radio {\n  --color-checked:transparent;\n  height: 15px;\n  width: 15px;\n}\n.segment-top {\n  position: fixed;\n  top: 55px;\n  margin-bottom: 2rem;\n  z-index: 11;\n  width: 100%;\n  background: #fff;\n  left: 0;\n  margin-top: 1.8rem;\n}\n.next-section {\n  margin-top: 0;\n}\n.medium-text {\n  font-size: 0.875rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #01A3A4;\n  white-space: normal;\n  max-width: 75%;\n  width: 75%;\n}\n.custom-width {\n  width: 125px;\n}\n.portion-entry {\n  background: #fff;\n  position: absolute;\n  width: 85px;\n  right: 0;\n  z-index: 11;\n}\n.portion-entry img {\n  height: auto;\n  width: 20px;\n  /* padding: 5px; */\n  display: inline-block;\n  text-align: center;\n  vertical-align: middle;\n  position: relative;\n  left: -5px;\n  top: 0px;\n}\n.portion-entry ion-input {\n  text-align: center;\n  height: 25px;\n  width: auto;\n  color: #666666;\n  top: -2px;\n}\n.hide-show {\n  display: none;\n}\n.isHide {\n  display: none;\n}\n.isShow {\n  display: \"\";\n  cursor: pointer;\n}\n.light-text {\n  color: #979797;\n  font-size: 0.875rem;\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n}\nion-footer {\n  --background: #F6F7FC;\n}\nion-segment-button {\n  font-size: 0.875rem;\n}\nion-button {\n  margin-left: 1rem;\n  margin-right: 1rem;\n}\n.width-50 {\n  width: 50%;\n}\n.dflex-center {\n  display: flex;\n  align-items: center;\n  justify-content: space-between;\n}\ninput[type=number]::-webkit-inner-spin-button,\ninput[type=number]::-webkit-outer-spin-button {\n  -webkit-appearance: none !important;\n  -moz-appearance: none !important;\n  appearance: none !important;\n  margin: 0 !important;\n}\nsc-ion-input-ios-h {\n  --padding-top: 10px;\n  --padding-end: 0 !important;\n  --padding-bottom: 10px;\n  --padding-start: 0;\n  font-size: 14px !important;\n}\n.d-inlineb {\n  display: inline-block;\n}\n.filter-category {\n  width: 100%;\n  overflow-x: scroll;\n  white-space: nowrap;\n  height: 55px;\n}\n.indication {\n  text-align: center;\n  border-left: 5px solid red;\n  display: inline-block;\n  line-height: 1rem;\n  padding: 0 10px;\n}\n.darkgreen-top {\n  border-color: #38A534;\n}\n.unverified-top {\n  border-color: #7d7d7d5e;\n}\n.light-green-top {\n  border-color: #94EA0A;\n}\n.yellow-top {\n  border-color: #EADC18;\n}\n.yellow-orange-top {\n  border-color: #ffa500;\n}\n.darkgreen, .legend-9 {\n  border-color: #38A534;\n  background-color: #38A534;\n}\n.light-green, .legend-6 {\n  border-color: #94EA0A;\n  background-color: #94EA0A;\n}\n.yellow, .legend-3 {\n  border-color: #EADC18;\n  background-color: #EADC18;\n}\n.yellow-orange, .legend-1 {\n  border-color: #ffa500;\n  background-color: #ffa500;\n}\n.grey, .legend-0 {\n  border-color: #7d7d7d5e;\n}\n.legend {\n  border-width: 1px;\n  border-style: solid;\n  height: 56px;\n  width: 2px;\n  margin-right: 5px;\n}\nion-card {\n  margin: 0;\n  border-radius: 25px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  margin-bottom: 20px;\n}\nion-item {\n  --highlight-color-focused: none !important;\n  --highlight-color-valid: none !important;\n  --highlight-color-invalid: none !important;\n}\n.no-search-item ion-card {\n  display: inline-block;\n  width: 95%;\n  border-radius: 30px;\n  box-shadow: 0px 5px 10px 0px rgba(129, 133, 131, 0.1) !important;\n  margin: 5px 10px;\n}\n.margin-top20 {\n  margin-top: 20px;\n}\n.margin-bottom20 {\n  margin-bottom: 20px;\n}\n.avatar-size {\n  height: 50px;\n  width: 50px;\n}\n.msg-container {\n  color: #01A3A5;\n}\n.sb2:before {\n  content: \"\";\n  width: 0px;\n  height: 0px;\n  position: absolute;\n  border-left: 10px solid transparent;\n  border-right: 10px solid #b3b3b3;\n  border-top: 10px solid #b3b3b3;\n  border-bottom: 10px solid transparent;\n  left: -3px;\n  top: 40px;\n}\n.box2 {\n  border: 2px solid rgba(112, 112, 112, 0.2);\n  padding: 20px 10px;\n  text-align: center;\n  font-weight: 900;\n  color: rgba(112, 112, 112, 0.2);\n  font-family: \"gt-america-standard-regular\", \"Helvetica Neue\", Arial, sans-serif;\n  position: relative;\n  border-radius: 20px;\n}\n.sb11:before {\n  content: \"\";\n  width: 0px;\n  height: 0px;\n  position: absolute;\n  border-left: 10px solid transparent;\n  border-right: 10px solid rgba(112, 112, 112, 0.2);\n  border-top: 10px solid transparent;\n  border-bottom: 10px solid transparent;\n  left: -22px;\n}\n.sb11:after {\n  content: \"\";\n  width: 0px;\n  height: 0px;\n  position: absolute;\n  border-left: 8px solid transparent;\n  border-right: 10px solid #fff;\n  border-top: 10px solid transparent;\n  border-bottom: 10px solid transparent;\n  left: -17px;\n  top: 20px;\n}\n.unveri-font {\n  font-size: 1rem;\n  color: #01A3A5;\n  font-weight: bold;\n}\n.info-text {\n  color: rgba(112, 112, 112, 0.4);\n  font-size: 0.85rem;\n}\n.button-layout {\n  --border-radius: 100px !important;\n  border: 0;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL2Fzc2V0cy9mb250cy9mb250cy5zY3NzIiwib3B0aW9ucy5wYWdlLnNjc3MiLCIuLi8uLi90aGVtZS9jdXN0b20tdGhlbWVzL3ZhcmlhYmxlcy5zY3NzIiwiLi4vLi4vdGhlbWUvY3VzdG9tLXRoZW1lcy9jb2xvcnMuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLHVDQUFBO0VBQ0EsOERBQUE7QUNDRjtBREVBO0VBQ0Usd0NBQUE7RUFDQSwrREFBQTtBQ0FGO0FER0E7RUFDRSxxQ0FBQTtFQUNBLDREQUFBO0FDREY7QURJQTtFQUNFLCtDQUFBO0VBQ0EsNkRBQUE7QUNGRjtBREtBO0VBQ0Usc0NBQUE7RUFDQSw2REFBQTtBQ0hGO0FET0E7RUFDRSxvQ0FBQTtFQUNBLCtEQUFBO0FDTEY7QUFyQkE7RUFDRSxnQ0FBQTtBQXVCRjtBQXBCQTtFQUNFLGdDQUFBO0FBdUJGO0FBckJBO0VBQ0UsZ0JBQUE7QUF3QkY7QUFyQkE7RUFDRSxXQUFBO0VBQVksWUFBQTtFQUNaLGlCQUFBO0VBQ0EsNEVDVlU7RURXVixrQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FBeUJGO0FBdEJFO0VBQ0UsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLHFCQUFBO0FBeUJKO0FBdkJFO0VBQ0UsWUFBQTtBQXlCSjtBQXRCQTtFQUNFLDhCQUFBO0VBQ0EsMEJBQUE7QUF5QkY7QUF2QkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxTQUFBO0VBQ0Esb0JBQUE7QUEwQkY7QUF4QkE7RUFDRSxrQkFBQTtFQUNBLFlBQUE7RUFFQSxtQkFBQTtBQTBCRjtBQXhCQTtFQUNFLDJCQUFBO0FBMkJGO0FBekJBO0VBQ0UsZ0JBQUE7RUFDQSxtQkFBQTtBQTRCRjtBQTFCQTtFQUNFLGNBQUE7RUFDQSxpQkFBQTtBQTZCRjtBQTNCQTtFQUNFLHlCQUFBO0VBQ0EsMkNBQUE7QUE4QkY7QUEzQkE7RUFDRSxXQUFBO0VBQVksbUJBQUE7RUFBbUIsVUFBQTtFQUMvQixxQkFBQTtBQWdDRjtBQTlCQTtFQUNFLFdBQUE7RUFBWSxZQUFBO0VBQ1osc0JBQUE7RUFLQSxrQkFBQTtFQUFvQixnQkFBQTtFQUNwQixtQkFBQTtFQUNBLHlDQUFBO0VBQ0EsY0FBQTtFQUNDLDRFQzNFUztBRDBHWjtBQTdCQTtFQUNFLHNCQUFBO0VBQ0UscUJBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7QUFnQ0o7QUE5QkE7RUFDRSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsWUFBQTtBQWlDRjtBQS9CQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7RUFDQSxrQkFBQTtBQWtDRjtBQWhDQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLFNBQUE7QUFtQ0Y7QUFqQ0E7RUFDRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsZ0JBQUE7QUFvQ0Y7QUE5QkE7RUFDRSxrQkFBQTtFQUNFLFdBQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7QUFpQ0o7QUE1QkU7RUFFRSxxQkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0FBOEJKO0FBM0JBO0VBQ0UsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7QUE4QkY7QUE1QkE7RUFDRSxtQkFBQTtFQUNBLGNBQUE7QUErQkY7QUE3QkE7RUFDRSxtQkFBQTtBQWdDRjtBQTlCQTtFQUNFLHFCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxvQkFBQTtBQWlDRjtBQS9CRTtFQUNFLG1CQUFBO0FBaUNKO0FBN0JBO0VBQ0UsMkJBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQWdDRjtBQTlCQTtFQUNFLGVBQUE7RUFDQSxTQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsT0FBQTtFQUNBLGtCQUFBO0FBaUNGO0FBL0JBO0VBQ0UsYUFBQTtBQWtDRjtBQWhDQTtFQUNFLG1CQUFBO0VBQ0EsK0VDN0tLO0VEOEtMLGNFakxLO0VGa0xMLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLFVBQUE7QUFtQ0Y7QUE3QkE7RUFDRSxZQUFBO0FBZ0NGO0FBOUJBO0VBQ0UsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxRQUFBO0VBQ0EsV0FBQTtBQWlDRjtBQWhDSTtFQUNFLFlBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxxQkFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLFVBQUE7RUFDQSxRQUFBO0FBa0NOO0FBaENJO0VBQ0Usa0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0FBa0NOO0FBL0JBO0VBQ0UsYUFBQTtBQWtDRjtBQWhDQTtFQUNFLGFBQUE7QUFtQ0Y7QUFqQ0E7RUFDRSxXQUFBO0VBQ0EsZUFBQTtBQW9DRjtBQWpDQTtFQUNFLGNBQUE7RUFDQSxtQkFBQTtFQUNBLCtFQ2pPSztBRHFRUDtBQWxDQTtFQUNFLHFCQUFBO0FBcUNGO0FBbkNBO0VBQ0UsbUJBQUE7QUFzQ0Y7QUFwQ0E7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0FBdUNGO0FBakNBO0VBQ0UsVUFBQTtBQW9DRjtBQWxDQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtFQUNBLDhCQUFBO0FBcUNGO0FBbkNBOztFQUVBLG1DQUFBO0VBQ0EsZ0NBQUE7RUFDQSwyQkFBQTtFQUNBLG9CQUFBO0FBc0NBO0FBcENBO0VBQ0UsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSwwQkFBQTtBQXVDRjtBQXJDQztFQUNFLHFCQUFBO0FBd0NIO0FBdENDO0VBQ0MsV0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0FBeUNGO0FBdENBO0VBQ0Usa0JBQUE7RUFDQSwwQkFBQTtFQUNBLHFCQUFBO0VBQ0EsaUJBQUE7RUFDQSxlQUFBO0FBeUNGO0FBdENBO0VBQ0UscUJBQUE7QUF5Q0Y7QUF0Q0E7RUFDRSx1QkFBQTtBQXlDRjtBQXRDQTtFQUNFLHFCQUFBO0FBeUNGO0FBdENBO0VBQ0UscUJBQUE7QUF5Q0Y7QUF0Q0E7RUFDRSxxQkFBQTtBQXlDRjtBQXRDQTtFQUNFLHFCQUFBO0VBQ0EseUJBQUE7QUF5Q0Y7QUF0Q0E7RUFDRSxxQkFBQTtFQUNBLHlCQUFBO0FBeUNGO0FBdENBO0VBQ0UscUJBQUE7RUFDQSx5QkFBQTtBQXlDRjtBQXRDQTtFQUNFLHFCQUFBO0VBQ0EseUJBQUE7QUF5Q0Y7QUF0Q0E7RUFDRSx1QkFBQTtBQXlDRjtBQXRDQTtFQUNFLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGlCQUFBO0FBeUNGO0FBdENBO0VBQ0UsU0FBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7RUFDQSxtQkFBQTtBQXlDRjtBQXRDQTtFQUNFLDBDQUFBO0VBQ0Esd0NBQUE7RUFDQSwwQ0FBQTtBQXlDRjtBQXJDRTtFQUNFLHFCQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsZ0VBQUE7RUFDQSxnQkFBQTtBQXdDSjtBQWxDQTtFQUNFLGdCQUFBO0FBcUNGO0FBbENBO0VBQ0UsbUJBQUE7QUFxQ0Y7QUFsQ0E7RUFDRSxZQUFBO0VBQ0EsV0FBQTtBQXFDRjtBQWxDQTtFQUNFLGNBQUE7QUFxQ0Y7QUFqQ0E7RUFDRSxXQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1DQUFBO0VBQ0EsZ0NBQUE7RUFDQSw4QkFBQTtFQUNBLHFDQUFBO0VBQ0EsVUFBQTtFQUNBLFNBQUE7QUFvQ0Y7QUFqQ0E7RUFHRSwwQ0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNBLCtCQUFBO0VBQ0EsK0VDN1lLO0VEOFlMLGtCQUFBO0VBQ0EsbUJBQUE7QUFrQ0Y7QUEvQkE7RUFDRSxXQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtFQUNBLG1DQUFBO0VBQ0EsaURBQUE7RUFDQSxrQ0FBQTtFQUNBLHFDQUFBO0VBQ0EsV0FBQTtBQWtDRjtBQS9CQTtFQUNFLFdBQUE7RUFDQSxVQUFBO0VBQ0EsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0NBQUE7RUFDQSw2QkFBQTtFQUNBLGtDQUFBO0VBQ0EscUNBQUE7RUFDQSxXQUFBO0VBQ0EsU0FBQTtBQWtDRjtBQS9CQTtFQUNFLGVBQUE7RUFDQSxjQUFBO0VBQ0EsaUJBQUE7QUFrQ0Y7QUEvQkE7RUFDRSwrQkFBQTtFQUNBLGtCQUFBO0FBa0NGO0FBL0JBO0VBQ0UsaUNBQUE7RUFDQSxTQUFBO0FBa0NGIiwiZmlsZSI6Im9wdGlvbnMucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLW1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1NZWRpdW0udHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLWJvbGQ7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tQm9sZC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1leHRlbmRlZE1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1saWdodDtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBTb3VyY2UtU2Fucy1Qcm8tcmFndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cbiIsIkBpbXBvcnQgXCIuLi8uLi90aGVtZS9mb250cy5zY3NzXCI7XG5cbmlvbi1jb250ZW50IHtcbiAgLS1iYWNrZ3JvdW5kOiAjRjZGN0ZDICFpbXBvcnRhbnQ7XG59XG5cbi5ib3JkZXItYm90dG9te1xuICBib3JkZXItYm90dG9tOjFweCBzb2xpZCAjYjNiM2IzO1xufVxuLmJ1dHRvbi1wb3N7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG59XG5cbi54LWJ1dHRvbntcbiAgd2lkdGg6IDEwMCU7aGVpZ2h0OiAzM3B4O1xuICBmb250LXdlaWdodDpib2xkO1xuICBmb250LWZhbWlseTogJGZvbnQtYm9sZDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjojMDFBM0E0O1xuICBwYWRkaW5nLXRvcDowO1xufVxuLmZpbHRlci1ieS1zZWxlY3Rpb257XG4gIHNwYW57XG4gICAgZm9udC1zaXplOiAuN3JlbTtcbiAgICBjb2xvcjojNEM1MjY0O1xuICAgIGxpbmUtaGVpZ2h0OiAyO1xuICAgIHBhZGRpbmc6IDEycHggNXB4IDVweDtcbiAgfVxuICBpb24tY29se1xuICAgIGhlaWdodDogNDBweDtcbiAgfVxufVxuLmN1c3RvbS1ib3JkZXItYm90dG9te1xuICBib3JkZXItYm90dG9tOiBub25lICFpbXBvcnRhbnQ7XG4gIC0tYm9yZGVyLWNvbG9yOnRyYW5zcGFyZW50O1xufVxuLnN1Z2dlc3R7XG4gIHJpZ2h0OiAxMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGNvbG9yOiAjMTQ5MkU2O1xuICBmb250LXNpemU6IC43NTFyZW07XG4gIHRvcDoxNXB4O1xuICBtYXJnaW4tcmlnaHQ6IDEuNXJlbTtcbn1cbi5zZWFyY2h7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgaGVpZ2h0OiA2MHB4O1xuICAvLyB0b3A6IDFyZW07XG4gIG1hcmdpbi1ib3R0b206IDFyZW07XG59XG4ubWFyZ2luLXRvcC0ze1xuICBtYXJnaW4tdG9wOjNyZW0gIWltcG9ydGFudDtcbn1cbi5yZWNvbWVuZGVkZm9yLXlvdXtcbiAgbWFyZ2luLXRvcDogMXJlbTtcbiAgbWFyZ2luLWJvdHRvbTogMXJlbTtcbn1cbi5yZWNvbWVuZGVkLWZvcntcbiAgY29sb3I6IzcwNzA3MDtcbiAgZm9udC1zaXplOi44cmVtO1xufVxuLmFkZC1jb2xvcntcbiAgY29sb3I6IzAxQTNBNSAhaW1wb3J0YW50O1xuICBib3JkZXItYm90dG9tOiBzb2xpZCAzcHggIzAxQTNBNSAhaW1wb3J0YW50O1xuICBcbn1cbi5zZWxlY3QtaXRlbS1ieS1maWx0ZXJ7XG4gIGhlaWdodDo0cHg7IGJhY2tncm91bmQ6IzAxQTNBNTt3aWR0aDo2MCU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbn1cbi54LWJ1dHRvbi1hY3RpdmUtMXtcbiAgd2lkdGg6IDEwMCU7aGVpZ2h0OiAzM3B4O1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICAvLyBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiAxcmVtO1xuICAvLyBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogMXJlbTtcbiAgLy8gYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDFyZW07XG4gIC8vIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6IDFyZW07XG4gIHRleHQtYWxpZ246IGNlbnRlcjsgcGFkZGluZy10b3A6IDVweDsgXG4gIGJhY2tncm91bmQ6ICNGNkY3RkM7XG4gIGJveC1zaGFkb3c6IDAgNXB4IDVweCByZ2JhKDAsMCwwLDE2JSk7XG4gIGNvbG9yOiM3MDcwNzA7IFxuICAgZm9udC1mYW1pbHk6ICRmb250LWJvbGQ7XG59XG4uc2VhcmNoLWljb257XG4gIHZlcnRpY2FsLWFsaWduOiBib3R0b207XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICB0b3A6IDEwcHg7XG59XG4uYm94LWFkZHtcbiAgYm9yZGVyOiAxcHggc29saWQgIzAxQTNBNTtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBoZWlnaHQ6IDMwcHg7XG59XG4ucGljLWZvb2QtaGVhcnR7XG4gIHdpZHRoOiAxNXB4O1xuICBoZWlnaHQ6IGF1dG87XG4gIGxlZnQ6IDUxcHg7XG4gIHRvcDogMnB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG59XG4uc2VwcmF0ZXtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBsZWZ0OiAxMTVweDtcbiAgdG9wOiAxMXB4O1xufVxuLnBsdXMtbWludXN7XG4gIG1hcmdpbjowcHg7XG4gIGNvbG9yOiMwMUEzQTU7XG4gIGZvbnQtc2l6ZTogMS43cmVtO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRvcDogLTNweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbi8vIGlvbi1idXR0b25zIHtcbi8vICAgcGFkZGluZy1sZWZ0OiAxLjEyNXJlbTtcbi8vICAgcGFkZGluZy1yaWdodDogMS4xMjVyZW07XG4vLyB9XG4ubW9yZS1idXR0b257XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogMTVweDtcbiAgICBwYWRkaW5nOiAwO1xuICAgIGhlaWdodDogMjBweDtcbiAgICB3aWR0aDogODUlO1xufVxuXG5pb24taGVhZGVyIHtcbiAgLy8gaGVpZ2h0OiA1LjNyZW07XG4gIGlvbi10b29sYmFyIHtcbiAgICAvLyAtLW1pbi1oZWlnaHQ6IDUuM3JlbSAhaW1wb3J0YW50O1xuICAgIC0tYmFja2dyb3VuZDogIzAxQTNBNDsvLyN7JGxpZ2h0WWVsbG93MX07XG4gICAgLS1jb2xvcjogI2ZmZjtcbiAgICAtLWZvbnQtc2l6ZTogMS41cmVtO1xuICAgIC0tZm9udC1mYW1pbHk6ICRmb250O1xuICB9XG59XG4uc21hbGwtaGVhZGluZyB7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIG1hcmdpbjogMC42cmVtIDA7XG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5zbWFsbC10ZXh0IHtcbiAgZm9udC1zaXplOiAwLjg3NXJlbTtcbiAgY29sb3I6ICNiM2IzYjM7XG59XG4ucGFyZW50IHtcbiAgbWFyZ2luOiAyLjJyZW0gMXJlbTtcbn1cbmlvbi10aHVtYm5haWwge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIG1hcmdpbjogLjVyZW0gMDtcbiAgbWFyZ2luLXJpZ2h0OiAuN3JlbTtcblxuICBpbWcge1xuICAgIGJvcmRlci1yYWRpdXM6IDFyZW07XG4gIH1cbn1cblxuaW9uLXJhZGlvIHtcbiAgLS1jb2xvci1jaGVja2VkOnRyYW5zcGFyZW50O1xuICBoZWlnaHQ6IDE1cHg7XG4gIHdpZHRoOiAxNXB4O1xufVxuLnNlZ21lbnQtdG9wIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6IDU1cHg7XG4gIG1hcmdpbi1ib3R0b206IDJyZW07XG4gIHotaW5kZXg6IDExO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgbGVmdDogMDtcbiAgbWFyZ2luLXRvcDogMS44cmVtO1xufVxuLm5leHQtc2VjdGlvbiB7XG4gIG1hcmdpbi10b3A6IDA7XG59XG4ubWVkaXVtLXRleHQge1xuICBmb250LXNpemU6IDAuODc1cmVtO1xuICBmb250LWZhbWlseTogJGZvbnQ7XG4gIGNvbG9yOiAkYmxhY2s7XG4gIHdoaXRlLXNwYWNlOiBub3JtYWw7XG4gIG1heC13aWR0aDogNzUlO1xuICB3aWR0aDogNzUlO1xufVxuLy8gaW9uLWJhY2stYnV0dG9uIHtcbi8vICAgLS1jb2xvcjogIzAwMDtcbi8vIH1cblxuLmN1c3RvbS13aWR0aHtcbiAgd2lkdGg6MTI1cHg7XG59XG4ucG9ydGlvbi1lbnRyeXtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogODVweDtcbiAgcmlnaHQ6IDA7XG4gIHotaW5kZXg6IDExO1xuICAgIGltZyB7XG4gICAgICBoZWlnaHQ6IGF1dG87XG4gICAgICB3aWR0aDogMjBweDtcbiAgICAgIC8qIHBhZGRpbmc6IDVweDsgKi9cbiAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAgIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICBsZWZ0OiAtNXB4O1xuICAgICAgdG9wOiAwcHg7XG4gICAgfVxuICAgIGlvbi1pbnB1dCB7XG4gICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICBoZWlnaHQ6IDI1cHg7XG4gICAgICB3aWR0aDogYXV0bztcbiAgICAgIGNvbG9yOiAjNjY2NjY2O1xuICAgICAgdG9wOiAtMnB4O1xuICAgICAgIH1cbn1cbi5oaWRlLXNob3d7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG4uaXNIaWRlIHtcbiAgZGlzcGxheTogbm9uZTtcbn1cbi5pc1Nob3cge1xuICBkaXNwbGF5OiAnJztcbiAgY3Vyc29yOiBwb2ludGVyO1xufVxuXG4ubGlnaHQtdGV4dCB7XG4gIGNvbG9yOiByZ2IoMTUxLCAxNTEsIDE1MSk7XG4gIGZvbnQtc2l6ZTogMC44NzVyZW07XG4gIGZvbnQtZmFtaWx5OiAkZm9udDtcbn1cbmlvbi1mb290ZXIge1xuICAtLWJhY2tncm91bmQ6ICN7JGxpZ2h0WWVsbG93MX07XG59XG5pb24tc2VnbWVudC1idXR0b24ge1xuICBmb250LXNpemU6IDAuODc1cmVtO1xufVxuaW9uLWJ1dHRvbiB7XG4gIG1hcmdpbi1sZWZ0OiAxcmVtO1xuICBtYXJnaW4tcmlnaHQ6IDFyZW07XG59XG4vLyA6Om5nLWRlZXAgaW9uLWJ1dHRvbiB7XG4vLyAgIGJvcmRlcjogMXB4IHNvbGlkICFpbXBvcnRhbnQ7XG4vLyAgIGJvcmRlci1yYWRpdXM6IDVweDtcbi8vIH1cbi53aWR0aC01MCB7XG4gIHdpZHRoOiA1MCU7XG59XG4uZGZsZXgtY2VudGVye1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG59XG5pbnB1dFt0eXBlPW51bWJlcl06Oi13ZWJraXQtaW5uZXItc3Bpbi1idXR0b24sXG5pbnB1dFt0eXBlPW51bWJlcl06Oi13ZWJraXQtb3V0ZXItc3Bpbi1idXR0b24ge1xuLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lICFpbXBvcnRhbnQ7XG4tbW96LWFwcGVhcmFuY2U6IG5vbmUgIWltcG9ydGFudDsgXG5hcHBlYXJhbmNlOiBub25lICFpbXBvcnRhbnQ7XG5tYXJnaW46IDAgIWltcG9ydGFudDtcbn1cbnNjLWlvbi1pbnB1dC1pb3MtaCB7XG4gIC0tcGFkZGluZy10b3A6IDEwcHg7XG4gIC0tcGFkZGluZy1lbmQ6IDAgIWltcG9ydGFudDtcbiAgLS1wYWRkaW5nLWJvdHRvbTogMTBweDtcbiAgLS1wYWRkaW5nLXN0YXJ0OiAwO1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbiAgfVxuIC5kLWlubGluZWJ7XG4gICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gfSBcbiAuZmlsdGVyLWNhdGVnb3J5e1xuICB3aWR0aDogMTAwJTtcbiAgb3ZlcmZsb3cteDogc2Nyb2xsO1xuICB3aGl0ZS1zcGFjZTpub3dyYXA7XG4gIGhlaWdodDogNTVweDtcbiB9XG5cbi5pbmRpY2F0aW9ue1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGJvcmRlci1sZWZ0OiA1cHggc29saWQgcmVkO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIGxpbmUtaGVpZ2h0OiAxcmVtO1xuICBwYWRkaW5nOiAwIDEwcHg7XG59XG5cbi5kYXJrZ3JlZW4tdG9we1xuICBib3JkZXItY29sb3I6ICMzOEE1MzQ7XG59XG5cbi51bnZlcmlmaWVkLXRvcHtcbiAgYm9yZGVyLWNvbG9yOiAjN2Q3ZDdkNWU7XG59XG5cbi5saWdodC1ncmVlbi10b3B7XG4gIGJvcmRlci1jb2xvcjogIzk0RUEwQVxufVxuXG4ueWVsbG93LXRvcHtcbiAgYm9yZGVyLWNvbG9yOiAjRUFEQzE4XG59XG5cbi55ZWxsb3ctb3JhbmdlLXRvcHtcbiAgYm9yZGVyLWNvbG9yOiAjZmZhNTAwXG59XG5cbi5kYXJrZ3JlZW4sIC5sZWdlbmQtOXtcbiAgYm9yZGVyLWNvbG9yOiAjMzhBNTM0O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMzhBNTM0O1xufVxuXG4ubGlnaHQtZ3JlZW4sIC5sZWdlbmQtNntcbiAgYm9yZGVyLWNvbG9yOiAjOTRFQTBBO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjOTRFQTBBO1xufVxuXG4ueWVsbG93LCAubGVnZW5kLTN7XG4gIGJvcmRlci1jb2xvcjogI0VBREMxODtcbiAgYmFja2dyb3VuZC1jb2xvcjogI0VBREMxODtcbn1cblxuLnllbGxvdy1vcmFuZ2UsIC5sZWdlbmQtMXtcbiAgYm9yZGVyLWNvbG9yOiAjZmZhNTAwO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZhNTAwO1xufVxuXG4uZ3JleSwgLmxlZ2VuZC0we1xuICBib3JkZXItY29sb3I6ICM3ZDdkN2Q1ZTtcbn1cblxuLmxlZ2VuZHtcbiAgYm9yZGVyLXdpZHRoOiAxcHg7XG4gIGJvcmRlci1zdHlsZTogc29saWQ7XG4gIGhlaWdodDogNTZweDtcbiAgd2lkdGg6IDJweDtcbiAgbWFyZ2luLXJpZ2h0OiA1cHg7XG59XG5cbmlvbi1jYXJke1xuICBtYXJnaW46IDA7XG4gIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIGJveC1zaGFkb3c6IDBweCA1cHggMTBweCAwcHggcmdiKDEyOSAxMzMgMTMxIC8gMTAlKSAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG5pb24taXRlbSB7XG4gIC0taGlnaGxpZ2h0LWNvbG9yLWZvY3VzZWQ6IG5vbmUgIWltcG9ydGFudDtcbiAgLS1oaWdobGlnaHQtY29sb3ItdmFsaWQ6IG5vbmUgIWltcG9ydGFudDtcbiAgLS1oaWdobGlnaHQtY29sb3ItaW52YWxpZDogbm9uZSAhaW1wb3J0YW50O1xufVxuXG4ubm8tc2VhcmNoLWl0ZW17XG4gIGlvbi1jYXJke1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogOTUlO1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gICAgYm94LXNoYWRvdzogMHB4IDVweCAxMHB4IDBweCByZ2IoMTI5IDEzMyAxMzEgLyAxMCUpICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luOiA1cHggMTBweDtcbiAgICAvLyBtYXJnaW46IDIwcHg7XG4gICAgXG4gIH0gXG59XG5cbi5tYXJnaW4tdG9wMjB7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbi5tYXJnaW4tYm90dG9tMjB7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5hdmF0YXItc2l6ZXtcbiAgaGVpZ2h0OiA1MHB4O1xuICB3aWR0aDogNTBweDtcbn1cblxuLm1zZy1jb250YWluZXJ7XG4gIGNvbG9yOiMwMUEzQTU7XG59XG5cblxuLnNiMjpiZWZvcmUge1xuICBjb250ZW50OiBcIlwiO1xuICB3aWR0aDogMHB4O1xuICBoZWlnaHQ6IDBweDtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3JkZXItbGVmdDogMTBweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgYm9yZGVyLXJpZ2h0OiAxMHB4IHNvbGlkICNiM2IzYjM7XG4gIGJvcmRlci10b3A6IDEwcHggc29saWQgI2IzYjNiMztcbiAgYm9yZGVyLWJvdHRvbTogMTBweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgbGVmdDogLTNweDtcbiAgdG9wOiA0MHB4O1xufVxuXG4uYm94MiB7XG4gIC8vIHdpZHRoOiAzMDBweDtcbiAgLy8gbWFyZ2luOiA1MHB4IGF1dG87XG4gIGJvcmRlcjogMnB4IHNvbGlkIHJnYigxMTIgMTEyIDExMiAvIDIwJSk7XG4gIHBhZGRpbmc6IDIwcHggMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXdlaWdodDogOTAwO1xuICBjb2xvcjogcmdiKDExMiAxMTIgMTEyIC8gMjAlKTtcbiAgZm9udC1mYW1pbHk6ICRmb250O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG59XG5cbi5zYjExOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHdpZHRoOiAwcHg7XG4gIGhlaWdodDogMHB4O1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvcmRlci1sZWZ0OiAxMHB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICBib3JkZXItcmlnaHQ6IDEwcHggc29saWQgcmdiKDExMiAxMTIgMTEyIC8gMjAlKTtcbiAgYm9yZGVyLXRvcDogMTBweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgYm9yZGVyLWJvdHRvbTogMTBweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgbGVmdDogLTIycHg7XG59XG5cbi5zYjExOmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgd2lkdGg6IDBweDtcbiAgaGVpZ2h0OiAwcHg7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgYm9yZGVyLWxlZnQ6IDhweCBzb2xpZCB0cmFuc3BhcmVudDtcbiAgYm9yZGVyLXJpZ2h0OiAxMHB4IHNvbGlkICNmZmY7XG4gIGJvcmRlci10b3A6IDEwcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIGJvcmRlci1ib3R0b206IDEwcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIGxlZnQ6IC0xN3B4O1xuICB0b3A6IDIwcHg7XG59XG5cbi51bnZlcmktZm9udHtcbiAgZm9udC1zaXplOiAxcmVtO1xuICBjb2xvcjogIzAxQTNBNTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5pbmZvLXRleHR7XG4gIGNvbG9yOiByZ2IoMTEyIDExMiAxMTIgLyA0MCUpO1xuICBmb250LXNpemU6IDAuODVyZW07XG59XG5cbi5idXR0b24tbGF5b3V0e1xuICAtLWJvcmRlci1yYWRpdXM6IDEwMHB4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlcjogMDtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vYXNzZXRzL2ZvbnRzL2ZvbnRzLnNjc3NcIjtcblxuLy8gRm9udHNcbiRmb250LXNpemU6IDE2cHg7XG5cbiRmb250OiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhclwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtYm9sZDogXCJndC1hbWVyaWNhLXN0YW5kYXJkLWJvbGRcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiRmb250LWxpZ2h0OiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtbGlnaHRcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjtcbiRmb250LW1lZGl1bTogXCJndC1hbWVyaWNhLXN0YW5kYXJkLW1lZGl1bVwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtbWVkaXVtLWV4dGVuZGVkOiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtZXh0ZW5kZWRNZWRpdW1cIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLFxuICBBcmlhbCwgc2Fucy1zZXJpZjtcblxuICAkc2Fucy1mb250LXJlZ3VsYXI6IFwiU291cmNlLVNhbnMtUHJvLXJhZ3VsYXJcIiwgXCJIZWx2ZXRpY2EgTmV1ZVwiLCBBcmlhbCwgc2Fucy1zZXJpZjsiLCIvLyBQcmltYXJ5IGNvbG9yc1xuJGxpcHN0aWM6ICMwMUEzQTQ7XG4kYmxhY2s6IzAxQTNBNDtcbiRncmF5OnJnYig4NSw4NSw4NSk7XG4kbGlnaHRHcmVlbjpyZ2IoMCw5OSw5OSk7XG4kbGlnaHRHcmVlbjE6cmdiKDE2MSwxOTEsMzgpO1xuJHdoaXRlOiNmZmY7XG4kbGlnaHRZZWxsb3cxOiNGNkY3RkM7XG4kZ3JheS0yMzpyZ2JhKDI1MSwxMzEsMzgsMC4xKTtcbiRncmF5LTk5OnJnYmEoMCwwLDAsMC42KTtcbiR5ZWxsb3ctMjojRjZGN0ZDO1xuJHlhbGxvdy0zOnJnYigyNTUsMTY3LDE5KTtcbi8vIEdyYWRpZW50XG4vLyRsaW5lYXItZ3JhZGllbnQ6IGxpbmVhci1ncmFkaWVudCg5MGRlZywgJGJsdWUtMjMgMCUsICRibHVlLTQxIDAlKTtcbiJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_options_options_module_ts.js.map