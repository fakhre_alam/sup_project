"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_diet-plan_update-detail-slot_update-detail-slot_module_ts"],{

/***/ 14259:
/*!***********************************************************************************!*\
  !*** ./src/app/diet-plan/update-detail-slot/update-detail-slot-routing.module.ts ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateDetailSlotPageRoutingModule": () => (/* binding */ UpdateDetailSlotPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _update_detail_slot_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-detail-slot.page */ 52516);




const routes = [
    {
        path: '',
        component: _update_detail_slot_page__WEBPACK_IMPORTED_MODULE_0__.UpdateDetailSlotPage
    }
];
let UpdateDetailSlotPageRoutingModule = class UpdateDetailSlotPageRoutingModule {
};
UpdateDetailSlotPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], UpdateDetailSlotPageRoutingModule);



/***/ }),

/***/ 61406:
/*!***************************************************************************!*\
  !*** ./src/app/diet-plan/update-detail-slot/update-detail-slot.module.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateDetailSlotPageModule": () => (/* binding */ UpdateDetailSlotPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _update_detail_slot_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-detail-slot-routing.module */ 14259);
/* harmony import */ var _update_detail_slot_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-detail-slot.page */ 52516);







let UpdateDetailSlotPageModule = class UpdateDetailSlotPageModule {
};
UpdateDetailSlotPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _update_detail_slot_routing_module__WEBPACK_IMPORTED_MODULE_0__.UpdateDetailSlotPageRoutingModule
        ],
        declarations: [_update_detail_slot_page__WEBPACK_IMPORTED_MODULE_1__.UpdateDetailSlotPage]
    })
], UpdateDetailSlotPageModule);



/***/ }),

/***/ 52516:
/*!*************************************************************************!*\
  !*** ./src/app/diet-plan/update-detail-slot/update-detail-slot.page.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateDetailSlotPage": () => (/* binding */ UpdateDetailSlotPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_update_detail_slot_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./update-detail-slot.page.html */ 387);
/* harmony import */ var _update_detail_slot_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-detail-slot.page.scss */ 550);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var src_app_core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/utility/utilities */ 53533);
/* harmony import */ var src_app_services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services */ 65190);







let UpdateDetailSlotPage = class UpdateDetailSlotPage {
    constructor(utilities, route, router, appService) {
        this.utilities = utilities;
        this.route = route;
        this.router = router;
        this.appService = appService;
        this.updateSlot = [{ name: 'sabji', count: 14 }, { name: 'Curry', count: 14 }, { name: 'Raita', count: 14 }, { name: 'Bread', count: 14 }];
        this.slot = 1;
        this.slotName = '';
        this.cat = '';
        this.isShow = false;
        this.bkpData = [];
        this.route.queryParams.subscribe(res => {
            console.log("response:-", res);
            this.slot = res.slot;
            this.cat = res.cat;
            this.slotName = res.name;
        });
    }
    ngOnInit() {
        this.fetchFoodDetails();
    }
    isVisibal(isA, isB) {
        if (!isA && !isB) {
            return false;
        }
        return true;
    }
    fetchFoodDetails() {
        const payload = {
            email: localStorage.getItem('email'),
            slot: this.slot,
            dietPlan: this.slotName,
            cat: this.cat
        };
        this.appService.fetchFoodDetails(payload).subscribe(res => {
            console.log("res", res);
            this.updateSlot = this.utilities.customSort(res);
            this.bkpData = JSON.stringify(this.updateSlot);
        }, err => {
            console.log("error", err);
        });
    }
    gotoUpdate() {
        this.router.navigate(['./confirm-diet']);
    }
    gotoDetail(item) {
        // this.router.navigate(['update-detail-slot'],{queryParams:{param:item.name}})
    }
    updatePAyload(data) {
        //  const data=JSON.parse(this.bkpData);
        const tempData = [];
        for (let index = 0; index < data.length; index++) {
            tempData.push({ diet: data[index].diet, option: data[index].option, _id: data[index]._id });
        }
        return tempData;
    }
    reset() {
        const tempDa = this.updatePAyload(JSON.parse(this.bkpData));
        // this.updateSlot = JSON.parse(this.bkpData);
        console.log("reset", this.updateSlot);
        const reqPayload = {
            email: localStorage.getItem('email'),
            slot: this.slot,
            cat: this.cat,
            data: tempDa
        };
        this.appService.updateFoodItems(reqPayload).subscribe(res => {
            console.log("update slot", res);
        }, err => {
            console.log("error", err);
        });
    }
    updateFoodItems() {
        const tempDa = this.updatePAyload(this.updateSlot);
        const reqPayload = {
            email: localStorage.getItem('email'),
            slot: this.slot,
            cat: this.cat,
            data: tempDa
        };
        console.log("update slot payload:", tempDa);
        this.appService.updateFoodItems(reqPayload).subscribe(res => {
            console.log("update slot", res);
        }, err => {
            console.log("error", err);
        });
    }
};
UpdateDetailSlotPage.ctorParameters = () => [
    { type: src_app_core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__.UTILITIES },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.ActivatedRoute },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__.Router },
    { type: src_app_services__WEBPACK_IMPORTED_MODULE_3__.AppService }
];
UpdateDetailSlotPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-update-detail-slot',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_update_detail_slot_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_update_detail_slot_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], UpdateDetailSlotPage);



/***/ }),

/***/ 387:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/diet-plan/update-detail-slot/update-detail-slot.page.html ***!
  \******************************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n<ion-content class=\"update-slot-diet\">\n  <ion-row>\n    <ion-col class=\"ion-text-center\">\n      <h2 class=\"choose-diet\">  Update “Fruits” in “Breakfast”\n        </h2>\n    </ion-col>\n  </ion-row>\n  <ion-grid class=\"content\">\n    <ion-card clss=\"card-item\" >\n      <ion-card-header style=\"border-bottom:.5px solid #b3b3b3;padding:10px 0;\">\n        <ion-row style=\"padding:.5rem;\" >\n          <ion-col size=\"7.1\" class=\"ion-text-left\"><span class=\"cat\">Food Items</span></ion-col>\n          <ion-col size=\"2.3\" class=\"ion-text-right\"><span class=\"option\">Option</span></ion-col>\n          <ion-col size=\"2.6\" class=\"ion-text-right\"><span class=\"diet\">Diet Plan</span></ion-col>\n        </ion-row>\n      </ion-card-header>\n      <ion-card-content>\n        <div  *ngFor=\"let sdiet of updateSlot;let sdt=index;\">\n        <ion-row style=\"padding: 0 .2rem;\" *ngIf=\"isVisibal(sdiet.option,sdiet.diet) && !isShow\">\n          <ion-col>\n          <ion-row style=\"padding: .3rem;\">  \n          <ion-col size=\"8\" class=\"ion-text-left\">\n            <div style=\"margin-bottom: 0.4rem;line-height: 1;\"><span class=\"food-cal\">{{sdiet.Food}}</span> &nbsp;<span class=\"--cal --cal\"> {{sdiet.Calories}} Cal</span> </div>\n            <div>\n              <ion-row class=\"pfcf\">\n                <ion-col size=\"3\">Protein <br>\n                  {{sdiet.Protien}}  \n                </ion-col>\n                <ion-col size=\"3\">Fats\n                  <br> {{sdiet.Fat}} \n                </ion-col>\n                <ion-col size=\"3\">Carbs\n                  <br>\n                  {{sdiet.Carbs}} \n                </ion-col>\n                <ion-col size=\"3\">Fiber\n                  <br>\n                  {{sdiet.Fiber}} \n                </ion-col>\n              </ion-row>\n            </div>\n\n            </ion-col>\n          <ion-col size=\"2\" class=\"ion-text-center ion-align-self-center\">\n            <ion-checkbox class=\"option\" mode=\"md\" [disabled]=\"sdiet.diet?true:false\" [checked]=\"sdiet.diet?true:sdiet.option\" [(ngModel)]=\"sdiet.option\"></ion-checkbox>\n          </ion-col>\n          <ion-col size=\"2\" class=\"ion-text-right ion-align-self-center\">\n            <ion-checkbox class=\"diet\" mode=\"md\" [checked]=\"sdiet.diet\" [(ngModel)]=\"sdiet.diet\"></ion-checkbox>\n          </ion-col>\n          </ion-row>\n          <ion-row style=\"padding-bottom: .3rem;\">\n            <ion-col style=\"border-bottom:1px solid #b3b3b3;\"></ion-col>\n          </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row style=\"padding: 0 .2rem;\" *ngIf=\"isShow\">\n          <ion-col>\n          <ion-row style=\"padding: .3rem;\">  \n          <ion-col size=\"8\" class=\"ion-text-left\">\n            <div style=\"margin-bottom: 0.4rem;line-height: 1;\"><span class=\"food-cal\">{{sdiet.Food}}</span> &nbsp;<span class=\"--cal --cal\"> {{sdiet.Calories}} Cal</span> </div>\n            <div>\n              <ion-row class=\"pfcf\">\n                <ion-col size=\"3\">Protein <br>\n                  {{sdiet.Protien}}  \n                </ion-col>\n                <ion-col size=\"3\">Fats\n                  <br> {{sdiet.Fat}} \n                </ion-col>\n                <ion-col size=\"3\">Carbs\n                  <br>\n                  {{sdiet.Carbs}} \n                </ion-col>\n                <ion-col size=\"3\">Fiber\n                  <br>\n                  {{sdiet.Fiber}} \n                </ion-col>\n              </ion-row>\n            </div>\n\n            </ion-col>\n          <ion-col size=\"2\" class=\"ion-text-center ion-align-self-center\">\n            <ion-checkbox class=\"option\" mode=\"md\" [disabled]=\"sdiet.diet?true:false\" [checked]=\"sdiet.diet?true:sdiet.option\" [(ngModel)]=\"sdiet.option\"></ion-checkbox>\n          </ion-col>\n          <ion-col size=\"2\" class=\"ion-text-right ion-align-self-center\">\n            <ion-checkbox class=\"diet\" mode=\"md\" [checked]=\"sdiet.diet\" [(ngModel)]=\"sdiet.diet\"></ion-checkbox>\n          </ion-col>\n          </ion-row>\n          <ion-row style=\"padding-bottom: .3rem;\">\n            <ion-col style=\"border-bottom:1px solid #b3b3b3;\"></ion-col>\n          </ion-row>\n          </ion-col>\n        </ion-row>\n      </div>\n      <ion-row>\n        <ion-col class=\"ion-text-center\" (click)=\"isShow=!isShow;\">\n          <a style=\"color:#01A3A4; cursor:HAND;\">Show All Foods Items</a>\n        </ion-col>\n      </ion-row>\n      </ion-card-content>\n    </ion-card>\n  </ion-grid>\n  </ion-content>\n  <ion-footerbar>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-button shape=\"round\" color=\"theme\" expand=\"block\" fill=\"outline\" (click)=\"updateFoodItems()\">Update</ion-button>\n      </ion-col>\n      <ion-col>\n        <ion-button shape=\"round\" color=\"theme\" expand=\"block\" (click)=\"reset()\">Reset Default</ion-button>\n      </ion-col>\n    </ion-row>\n    </ion-footerbar>\n");

/***/ }),

/***/ 550:
/*!***************************************************************************!*\
  !*** ./src/app/diet-plan/update-detail-slot/update-detail-slot.page.scss ***!
  \***************************************************************************/
/***/ ((module) => {

module.exports = ".update-slot-diet {\n  --background:#F6F7FC;\n}\n.update-slot-diet h2 {\n  font-size: 1.2rem;\n  color: #263143;\n}\n.update-slot-diet .content {\n  margin: 0rem 0rem;\n}\n.update-slot-diet .content .food-cal {\n  font-size: 0.85rem;\n  font-weight: 600;\n}\n.update-slot-diet .content .--cal {\n  font-size: 0.75rem;\n  font-weight: 600;\n}\n.update-slot-diet .content .pfcf ion-col {\n  font-size: 0.65rem;\n  white-space: nowrap;\n  border-right: 1px solid #b3b3b3;\n  text-align: center;\n}\n.update-slot-diet .content ion-card {\n  box-shadow: none;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col {\n  font-size: 0.8rem;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.cat {\n  color: gray;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.order {\n  color: #00B8F6;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.option {\n  color: #01A3A4;\n}\n.update-slot-diet .content ion-card ion-card-header ion-row ion-col span.diet {\n  color: #E69740;\n}\n.update-slot-diet .content ion-card-content {\n  padding: 0;\n}\n.update-slot-diet .content ion-card-content ion-checkbox.option {\n  --border-radius: 100%;\n  --background-checked: #01A3A4;\n  --border-color-checked: #01A3A4;\n  height: 25px;\n  width: 25px;\n}\n.update-slot-diet .content ion-card-content ion-checkbox.diet {\n  --border-radius: 100%;\n  --background-checked: #E69740;\n  --border-color-checked: #E69740;\n  height: 25px;\n  width: 25px;\n}\n.update-slot-diet .active {\n  border: 1px solid #01A3A4;\n  box-shadow: 0px 4px 8px 0px #b3b3b3;\n}\nion-footerbar ion-row ion-col {\n  padding: 1rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVwZGF0ZS1kZXRhaWwtc2xvdC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBRUE7RUFDSSxvQkFBQTtBQURKO0FBRUk7RUFDSSxpQkFBQTtFQUNBLGNBQUE7QUFBUjtBQUVJO0VBQ0EsaUJBQUE7QUFBSjtBQUVJO0VBQ0ksa0JBQUE7RUFDQSxnQkFBQTtBQUFSO0FBRUk7RUFDSSxrQkFBQTtFQUNBLGdCQUFBO0FBQVI7QUFHUTtFQUNBLGtCQUFBO0VBQ0ksbUJBQUE7RUFDSiwrQkFBQTtFQUNBLGtCQUFBO0FBRFI7QUFJSTtFQUNJLGdCQUFBO0FBRlI7QUFNZTtFQUNJLGlCQUFBO0FBSm5CO0FBS2lCO0VBQ0csV0FBQTtBQUhwQjtBQUtpQjtFQUNJLGNBQUE7QUFIckI7QUFLaUI7RUFDRyxjQUFBO0FBSHBCO0FBS2lCO0VBQ0csY0FBQTtBQUhwQjtBQVVJO0VBQ0ksVUFBQTtBQVJSO0FBU1E7RUFDSSxxQkFBQTtFQUNBLDZCQUFBO0VBQ0EsK0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQVBaO0FBU1c7RUFDQyxxQkFBQTtFQUNBLDZCQUFBO0VBQ0EsK0JBQUE7RUFDQSxZQUFBO0VBQ0EsV0FBQTtBQVBaO0FBYUk7RUFDSSx5QkFBQTtFQUNBLG1DQUFBO0FBWFI7QUFrQkk7RUFDRyxhQUFBO0FBZlAiLCJmaWxlIjoidXBkYXRlLWRldGFpbC1zbG90LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuXG4udXBkYXRlLXNsb3QtZGlldHtcbiAgICAtLWJhY2tncm91bmQ6I0Y2RjdGQztcbiAgICBoMntcbiAgICAgICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgICAgIGNvbG9yOiMyNjMxNDM7XG4gICAgfVxuICAgIC5jb250ZW50e1xuICAgIG1hcmdpbjowcmVtIDByZW07XG5cbiAgICAuZm9vZC1jYWx7XG4gICAgICAgIGZvbnQtc2l6ZTogLjg1cmVtO1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgIH1cbiAgICAuLS1jYWx7XG4gICAgICAgIGZvbnQtc2l6ZTogLjc1cmVtO1xuICAgICAgICBmb250LXdlaWdodDogNjAwO1xuICAgIH1cbiAgICAucGZjZntcbiAgICAgICAgaW9uLWNvbHtcbiAgICAgICAgZm9udC1zaXplOiAuNjVyZW07XG4gICAgICAgICAgICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICAgICAgICBib3JkZXItcmlnaHQ6IDFweCBzb2xpZCAjYjNiM2IzO1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIH1cbiAgICB9XG4gICAgaW9uLWNhcmR7XG4gICAgICAgIGJveC1zaGFkb3c6IG5vbmU7XG4gICAgICAgIGlvbi1jYXJkLWhlYWRlcntcbiAgICAgICAgICBcbiAgICAgICAgICAgaW9uLXJvd3tcbiAgICAgICAgICAgICAgIGlvbi1jb2x7XG4gICAgICAgICAgICAgICAgICAgZm9udC1zaXplOiAuOHJlbTtcbiAgICAgICAgICAgICAgICAgc3Bhbi5jYXR7XG4gICAgICAgICAgICAgICAgICAgIGNvbG9yOmdyYXk7XG4gICAgICAgICAgICAgICAgIH0gXG4gICAgICAgICAgICAgICAgIHNwYW4ub3JkZXJ7XG4gICAgICAgICAgICAgICAgICAgICBjb2xvcjojMDBCOEY2O1xuICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgIHNwYW4ub3B0aW9ue1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjojMDFBM0E0OyAgXG4gICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgc3Bhbi5kaWV0e1xuICAgICAgICAgICAgICAgICAgICBjb2xvcjojRTY5NzQwOyAgXG4gICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgIH1cbiAgICAgICAgICAgXG4gICAgICAgIH1cbiAgICB9XG4gICAgaW9uLWNhcmQtY29udGVudHtcbiAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgaW9uLWNoZWNrYm94Lm9wdGlvbntcbiAgICAgICAgICAgIC0tYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICAgICAgICAgIC0tYmFja2dyb3VuZC1jaGVja2VkOiAjMDFBM0E0O1xuICAgICAgICAgICAgLS1ib3JkZXItY29sb3ItY2hlY2tlZDogIzAxQTNBNDtcbiAgICAgICAgICAgIGhlaWdodDogMjVweDtcbiAgICAgICAgICAgIHdpZHRoOiAyNXB4O1xuICAgICAgICAgICB9XG4gICAgICAgICAgIGlvbi1jaGVja2JveC5kaWV0e1xuICAgICAgICAgICAgLS1ib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgICAgICAgICAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6ICNFNjk3NDA7XG4gICAgICAgICAgICAtLWJvcmRlci1jb2xvci1jaGVja2VkOiAjRTY5NzQwO1xuICAgICAgICAgICAgaGVpZ2h0OiAyNXB4O1xuICAgICAgICAgICAgd2lkdGg6IDI1cHg7XG4gICAgICAgICAgIH1cbiAgICB9XG4gICAgfVxuICAgIFxuICAgIFxuICAgIC5hY3RpdmV7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMUEzQTQ7XG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggOHB4IDBweCAjYjNiM2IzO1xuICAgIH1cblxufVxuXG5pb24tZm9vdGVyYmFye1xuICAgIGlvbi1yb3d7XG4gICAgaW9uLWNvbHtcbiAgICAgICBwYWRkaW5nOjFyZW07IFxufVxufVxufVxuIl19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_diet-plan_update-detail-slot_update-detail-slot_module_ts.js.map