"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_diet-plan_select-diet_select-diet_module_ts"],{

/***/ 16520:
/*!*********************************************************************!*\
  !*** ./src/app/diet-plan/select-diet/select-diet-routing.module.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectDietPageRoutingModule": () => (/* binding */ SelectDietPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _select_diet_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./select-diet.page */ 73645);




const routes = [
    {
        path: '',
        component: _select_diet_page__WEBPACK_IMPORTED_MODULE_0__.SelectDietPage
    }
];
let SelectDietPageRoutingModule = class SelectDietPageRoutingModule {
};
SelectDietPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SelectDietPageRoutingModule);



/***/ }),

/***/ 70478:
/*!*************************************************************!*\
  !*** ./src/app/diet-plan/select-diet/select-diet.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectDietPageModule": () => (/* binding */ SelectDietPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _select_diet_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./select-diet-routing.module */ 16520);
/* harmony import */ var _select_diet_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./select-diet.page */ 73645);







let SelectDietPageModule = class SelectDietPageModule {
};
SelectDietPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _select_diet_routing_module__WEBPACK_IMPORTED_MODULE_0__.SelectDietPageRoutingModule
        ],
        declarations: [_select_diet_page__WEBPACK_IMPORTED_MODULE_1__.SelectDietPage]
    })
], SelectDietPageModule);



/***/ }),

/***/ 73645:
/*!***********************************************************!*\
  !*** ./src/app/diet-plan/select-diet/select-diet.page.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SelectDietPage": () => (/* binding */ SelectDietPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_select_diet_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./select-diet.page.html */ 22083);
/* harmony import */ var _select_diet_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./select-diet.page.scss */ 2565);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var src_app_services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services */ 65190);






let SelectDietPage = class SelectDietPage {
    constructor(appService, router) {
        this.appService = appService;
        this.router = router;
        this.selectDiet = [{ diet: 'Weight Loss' }, { diet: 'Diabetes' }, { diet: 'Muscle build' }];
        this.isActive = -1;
        this.dietName = "";
    }
    ngOnInit() {
        this.fetchDietPlanNames();
    }
    fetchDietPlanNames() {
        this.appService.fetchDietPlanNames().subscribe(res => {
            console.log("res", res);
            this.selectDiet = res;
        }, err => {
            console.log("error", err);
        });
    }
    gotoConfirm() {
        this.router.navigate(['./confirm-diet'], { queryParams: { name: this.dietName } });
    }
    selectedDiet(item, index) {
        this.dietName = item.dietPlan;
        this.isActive = index;
    }
};
SelectDietPage.ctorParameters = () => [
    { type: src_app_services__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router }
];
SelectDietPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-select-diet',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_select_diet_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_select_diet_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], SelectDietPage);



/***/ }),

/***/ 22083:
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/diet-plan/select-diet/select-diet.page.html ***!
  \****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n<ion-content class=\"select-diet\">\n<ion-row>\n  <ion-col class=\"ion-text-center\">\n    <h2 class=\"choose-diet\">Choose the most suitable Diet Plan </h2>\n  </ion-col>\n</ion-row>\n\n<ion-grid class=\"content\">\n  <ion-item *ngFor=\"let sdiet of selectDiet;let sdt=index;\" (click)=\"selectedDiet(sdiet,sdt)\" [ngClass]=\"{'active':isActive===sdt}\">\n    <ion-row style=\"width: 100%;\">\n      <ion-col class=\"ion-text-center\" size=\"12\">\n        <div class=\"diet-plan\" >{{sdiet.name}}</div>\n      </ion-col>\n    </ion-row>\n  </ion-item>\n</ion-grid>\n</ion-content>\n<ion-footerbar>\n<ion-row>\n  <ion-col class=\"ion-text-center\">\n    <ion-button shape=\"round\" color=\"theme\" expand=\"block\" (click)=\"gotoConfirm()\">Next</ion-button>\n  </ion-col>\n</ion-row>\n</ion-footerbar>");

/***/ }),

/***/ 2565:
/*!*************************************************************!*\
  !*** ./src/app/diet-plan/select-diet/select-diet.page.scss ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = ".select-diet {\n  --background:#F6F7FC;\n}\n.select-diet h2 {\n  font-size: 1.2rem;\n  color: #263143;\n}\n.select-diet .content {\n  margin: 1rem 1rem;\n}\n.select-diet .content ion-item {\n  margin-top: 1rem;\n  border-radius: 20px;\n}\n.select-diet .content .choose-diet {\n  font-size: 1.2rem;\n  font-weight: 500;\n}\n.select-diet .content .diet-plan {\n  font-size: 1.4rem;\n  padding: 8px;\n  background-color: #fff;\n  border-radius: 15px;\n  color: #263143;\n}\n.select-diet .content .active {\n  border: 1px solid #01A3A4;\n  box-shadow: 0px 4px 8px 0px #b3b3b3;\n}\nion-footerbar ion-row ion-col {\n  padding: 1rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNlbGVjdC1kaWV0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFFQTtFQUNJLG9CQUFBO0FBREo7QUFFSTtFQUNJLGlCQUFBO0VBQ0EsY0FBQTtBQUFSO0FBRUk7RUFDQSxpQkFBQTtBQUFKO0FBQ0k7RUFDSSxnQkFBQTtFQUNBLG1CQUFBO0FBQ1I7QUFDSTtFQUNJLGlCQUFBO0VBQ0EsZ0JBQUE7QUFDUjtBQUNJO0VBQ0ksaUJBQUE7RUFDQSxZQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLGNBQUE7QUFDUjtBQUVJO0VBQ0kseUJBQUE7RUFDQSxtQ0FBQTtBQUFSO0FBUUk7RUFDRyxhQUFBO0FBTFAiLCJmaWxlIjoic2VsZWN0LWRpZXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG5cbi5zZWxlY3QtZGlldHtcbiAgICAtLWJhY2tncm91bmQ6I0Y2RjdGQztcbiAgICBoMntcbiAgICAgICAgZm9udC1zaXplOiAxLjJyZW07XG4gICAgICAgIGNvbG9yOiMyNjMxNDM7XG4gICAgfVxuICAgIC5jb250ZW50e1xuICAgIG1hcmdpbjoxcmVtIDFyZW07XG4gICAgaW9uLWl0ZW17XG4gICAgICAgIG1hcmdpbi10b3A6MXJlbTtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgICB9XG4gICAgLmNob29zZS1kaWV0e1xuICAgICAgICBmb250LXNpemU6MS4ycmVtO1xuICAgICAgICBmb250LXdlaWdodDo1MDA7XG4gICAgfVxuICAgIC5kaWV0LXBsYW57XG4gICAgICAgIGZvbnQtc2l6ZTogMS40cmVtO1xuICAgICAgICBwYWRkaW5nOiA4cHg7XG4gICAgICAgIGJhY2tncm91bmQtY29sb3I6ICNmZmY7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgICAgIGNvbG9yOiAjMjYzMTQzO1xuXG4gICAgfVxuICAgIC5hY3RpdmV7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICMwMUEzQTQ7XG4gICAgICAgIGJveC1zaGFkb3c6IDBweCA0cHggOHB4IDBweCAjYjNiM2IzO1xuICAgIH1cblxufVxufVxuXG5pb24tZm9vdGVyYmFye1xuICAgIGlvbi1yb3d7XG4gICAgaW9uLWNvbHtcbiAgICAgICBwYWRkaW5nOjFyZW07IFxufVxufVxufVxuIl19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_diet-plan_select-diet_select-diet_module_ts.js.map