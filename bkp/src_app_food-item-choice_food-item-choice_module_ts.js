"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_food-item-choice_food-item-choice_module_ts"],{

/***/ 2182:
/*!*********************************************************************!*\
  !*** ./src/app/food-item-choice/food-item-choice-routing.module.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FoodItemChoicePageRoutingModule": () => (/* binding */ FoodItemChoicePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _food_item_choice_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./food-item-choice.page */ 82568);




const routes = [
    {
        path: '',
        component: _food_item_choice_page__WEBPACK_IMPORTED_MODULE_0__.FoodItemChoicePage
    }
];
let FoodItemChoicePageRoutingModule = class FoodItemChoicePageRoutingModule {
};
FoodItemChoicePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], FoodItemChoicePageRoutingModule);



/***/ }),

/***/ 73331:
/*!*************************************************************!*\
  !*** ./src/app/food-item-choice/food-item-choice.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FoodItemChoicePageModule": () => (/* binding */ FoodItemChoicePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _food_item_choice_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./food-item-choice-routing.module */ 2182);
/* harmony import */ var ng_circle_progress__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ng-circle-progress */ 91756);
/* harmony import */ var _food_item_choice_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./food-item-choice.page */ 82568);








let FoodItemChoicePageModule = class FoodItemChoicePageModule {
};
FoodItemChoicePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            ng_circle_progress__WEBPACK_IMPORTED_MODULE_7__.NgCircleProgressModule.forRoot({
                animation: false,
                radius: 49,
                innerStrokeWidth: 6,
                outerStrokeWidth: 6,
                space: -6,
                responsive: false,
                showTitle: true,
                titleFontSize: "15",
                subtitleFontSize: "15",
                unitsFontSize: "15",
                renderOnClick: false
            }),
            _food_item_choice_routing_module__WEBPACK_IMPORTED_MODULE_0__.FoodItemChoicePageRoutingModule
        ],
        declarations: [_food_item_choice_page__WEBPACK_IMPORTED_MODULE_1__.FoodItemChoicePage]
    })
], FoodItemChoicePageModule);



/***/ }),

/***/ 82568:
/*!***********************************************************!*\
  !*** ./src/app/food-item-choice/food-item-choice.page.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FoodItemChoicePage": () => (/* binding */ FoodItemChoicePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_food_item_choice_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./food-item-choice.page.html */ 17121);
/* harmony import */ var _food_item_choice_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./food-item-choice.page.scss */ 68038);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ 38198);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services */ 65190);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../core/utility/utilities */ 53533);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 13252);








let FoodItemChoicePage = class FoodItemChoicePage {
    constructor(router, utilities, cdr, aps, appService) {
        this.router = router;
        this.utilities = utilities;
        this.cdr = cdr;
        this.aps = aps;
        this.appService = appService;
        this.itemOp = [];
        this.itemOptions = [];
        this.totalLength = new Array(9).fill(0);
        this.avgSlotTotalCal = new Array(9).fill(0);
        this.protien = new Array(9).fill(0);
        this.sumProtien = 0;
        this.fat = new Array(9).fill(0);
        this.sumFat = 0;
        this.carbs = new Array(9).fill(0);
        this.sumCarbs = 0;
        this.calTotal = 0;
        this.foodItemsArray = {
            slots: [],
            "total_macros": {
                "calories": 0,
                "fat": 0,
                "protein": 0,
                "carbs": 0,
                "fiber": 0 // Sum of Breakfast (25) and Lunch (8)
            }
        };
        this.slotNames = ['When You Wake up', 'Before Breakfast', 'Breakfast', 'Mid Day Meal', 'Lunch', 'Post Lunch', 'Evening Snack', 'Dinner', 'Before Sleep'];
        this.foodItemSlots = [];
        this.isOpen = false;
        this.isOpenSlotIndex = -1;
        this.isOpenItemIndex = -1;
        this.addItem = new Array(9);
        this.timeout = null;
        this.searchVisible = false;
        this.foodItemAdd = "";
    }
    defaultStructureOfJson() {
        for (let index = 0; index < 9; index++) {
            this.foodItemsArray.slots.push({
                slotIndex: index,
                name: this.slotNames[index],
                items: [],
                avg_macros: {
                    calories: 0,
                    protein: 0,
                    fat: 0,
                    carbs: 0
                }
            });
        }
        this.foodItemSlots = this.foodItemsArray.slots;
    }
    averageSlotTotalCal() {
        let tempAvgCal = 0;
        let protien = 0;
        let fat = 0;
        let carbs = 0;
        let diviser = 0;
        for (let index = 0; index < this.foodItemSlots.length; index++) {
            tempAvgCal = 0;
            protien = 0;
            fat = 0;
            carbs = 0;
            diviser = 0;
            for (let j = 0; j < this.foodItemSlots[index].items.length; j++) {
                diviser = this.foodItemSlots[index].items[j].options.length > 0 ? this.foodItemSlots[index].items[j].options.length : 1;
                tempAvgCal += (this.foodItemSlots[index].items[j].itemsCalories / diviser);
                protien += (this.foodItemSlots[index].items[j].itemsProtien / diviser);
                fat += (this.foodItemSlots[index].items[j].itemsFat / diviser);
                carbs += (this.foodItemSlots[index].items[j].itemsCarbs / diviser);
            }
            this.avgSlotTotalCal[index] = tempAvgCal;
            this.protien[index] = protien;
            this.fat[index] = fat;
            this.carbs[index] = carbs;
        }
        this.calTotal = this.avgSlotTotalCal.reduce((cummulat, a) => cummulat + a, 0);
        console.log(this.protien, this.fat, this.carbs);
    }
    sumTotalCalprotienfatcarbs(item) {
        return item.reduce((partialSum, a) => partialSum + a, 0);
    }
    ngOnInit() {
        const userId = localStorage.getItem('email');
        this.appService.getDietRecall(userId).then(res => {
            console.log("res", res);
            if (res == null) {
                this.defaultStructureOfJson();
                console.log("this.foodItemSlots", this.foodItemsArray, this.foodItemSlots);
            }
            else {
                this.foodItemsArray = res;
                this.foodItemSlots = this.foodItemsArray.slots;
                this.bindData(); // we will use this method when API will return response
                this.averageSlotTotalCal();
            }
        }, err => {
            console.log("err", err);
        });
    }
    bindData() {
        let tcount = 0;
        for (let index = 0; index < this.foodItemSlots.length; index++) {
            tcount = 0;
            for (let j = 0; j < this.foodItemSlots[index].items.length; j++) {
                tcount = tcount + this.foodItemSlots[index].items[j].options.length;
            }
            this.totalLength[index] = tcount;
        }
        console.log("totalLength", this.totalLength);
    }
    openPopup(slotIndex, itemIndex) {
        this.isOpen = true;
        this.isOpenSlotIndex = slotIndex;
        this.isOpenItemIndex = itemIndex;
    }
    closePopup() {
        this.isOpen = false;
        this.isOpenSlotIndex = -1;
        this.isOpenItemIndex = -1;
    }
    ngOnChanges() {
        this.cdr.detectChanges();
    }
    removeItems(index, itemIndex, name) {
        // debugger;
        const item = this.foodItemSlots[index].items.filter(item => {
            return item.name === name;
        });
        this.foodItemSlots[index].items = this.foodItemSlots[index].items.filter(item => {
            return item.name !== name;
        });
        this.averageSlotTotalCal();
    }
    updateCaloriesOndemand(event, itemName, unit) {
        console.log(event, itemName);
        // debugger;
        if (event.data != null && !isNaN(parseInt(event.data))) {
            let optionFood = '';
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray =
                this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.filter(item => {
                    return itemName !== item;
                });
            let temp = itemName.split('~(')[1].split(')')[0];
            let tempName = itemName.split('[')[0];
            //debugger;
            // itemName.split('[')[0]+`[${parseInt(event.data)}~${unit})];
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.push(tempName + `[${event.data}~(${temp})]`);
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArrayJoin = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.join(',');
            for (let index = 0; index < this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options.length; index++) {
                optionFood = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options[index].Food;
                if (itemName.split('[')[0] == optionFood) {
                    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options[index].portion = parseInt(event.data);
                    break;
                }
            }
            this.setCaloriesByPortionupdate();
        }
        // 
    }
    removeMoreItems(food) {
        // debugger;
        const searchItem = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options.filter(item => {
            return (item === null || item === void 0 ? void 0 : item.name) + `[${item === null || item === void 0 ? void 0 : item.portion}~(${item === null || item === void 0 ? void 0 : item.portionUnit})]` === food || (item === null || item === void 0 ? void 0 : item.Food) + `[${item === null || item === void 0 ? void 0 : item.portion}~(${item === null || item === void 0 ? void 0 : item.portionUnit})]` === food;
        });
        console.log("searchItemsearchItem:", searchItem);
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray =
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.filter(item => {
                return item !== food;
            });
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArrayJoin = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.join(',');
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options =
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options.filter(item => {
                return item.Food + `[${item === null || item === void 0 ? void 0 : item.portion}~(${item === null || item === void 0 ? void 0 : item.portionUnit})]` !== food;
            });
        if (searchItem.length > 0 && searchItem[0].macros !== undefined) {
            this.foodItemSlots[this.isOpenSlotIndex].avg_macros.calories = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.calories - (searchItem[0].macros.calories * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].avg_macros.protein = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.protein - (searchItem[0].macros.protein * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].avg_macros.fat = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.fat - (searchItem[0].macros.fat * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].avg_macros.carbs = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.carbs - (searchItem[0].macros.carbs * searchItem[0].portion);
            //  this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portion = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portion - (searchItem[0].portion);
            this.foodItemsArray.total_macros.calories = this.foodItemsArray.total_macros.calories - (searchItem[0].macros.calories * searchItem[0].portion);
            this.foodItemsArray.total_macros.protein = this.foodItemsArray.total_macros.protein - (searchItem[0].macros.protein * searchItem[0].portion);
            this.foodItemsArray.total_macros.fat = this.foodItemsArray.total_macros.fat - (searchItem[0].macros.fat * searchItem[0].portion);
            this.foodItemsArray.total_macros.carbs = this.foodItemsArray.total_macros.carbs - (searchItem[0].macros.carbs * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCalories = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCalories - (searchItem[0].macros.calories * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsProtien = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsProtien - (searchItem[0].macros.protein * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsFat = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsFat - (searchItem[0].macros.fat * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCarbs = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCarbs - (searchItem[0].macros.carbs * searchItem[0].portion);
        }
        else if (searchItem.length > 0 && searchItem[0].macros === undefined) {
            this.foodItemSlots[this.isOpenSlotIndex].avg_macros.calories = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.calories - (searchItem[0].Calories * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].avg_macros.protein = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.protein - (searchItem[0].Protein * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].avg_macros.fat = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.fat - (searchItem[0].Fat * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].avg_macros.carbs = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.carbs - (searchItem[0].Carbs * searchItem[0].portion);
            //  this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portion = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portion - (searchItem[0].portion);
            this.foodItemsArray.total_macros.calories = this.foodItemsArray.total_macros.calories - (searchItem[0].Calories * searchItem[0].portion);
            this.foodItemsArray.total_macros.protein = this.foodItemsArray.total_macros.protein - (searchItem[0].Protein * searchItem[0].portion);
            this.foodItemsArray.total_macros.fat = this.foodItemsArray.total_macros.fat - (searchItem[0].Fat * searchItem[0].portion);
            this.foodItemsArray.total_macros.carbs = this.foodItemsArray.total_macros.carbs - (searchItem[0].Carbs * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCalories = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCalories - (searchItem[0].Calories * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsProtien = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsProtien - (searchItem[0].Protein * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsFat = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsFat - (searchItem[0].Fat * searchItem[0].portion);
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCarbs = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCarbs - (searchItem[0].Carbs * searchItem[0].portion);
        }
        this.bindData();
        setTimeout(() => {
            this.averageSlotTotalCal();
        }, 100);
    }
    addNewItem(event, index) {
        if (event.target.value.trim() === "") {
            return this.utilities.presentAlert("Field should not be blank!");
        }
        const newItem = {
            "id": index,
            "name": event.target.value,
            "itemsArray": [],
            "itemsArrayJoin": '',
            "itemsCalories": 0,
            "itemsProtien": 0,
            "itemsFat": 0,
            "itemsCarbs": 0,
            "portion": 0,
            "portionUinit": "",
            "options": []
        };
        this.foodItemSlots[index].items.push(newItem);
        this.addItem[index] = "";
    }
    clearSearch() {
        console.log("Clear search called");
    }
    searchAllApiData(event) {
        if (event.target.value.trim() === "") {
            return this.utilities.presentAlert("Field should not be blank!");
        }
        if (this.timeout) {
            clearTimeout(this.timeout);
        }
        //var $this = this;
        this.timeout = setTimeout(() => {
            if (event.keyCode != 13) {
                console.log(event.target.value);
                this.aps.getUserToken("91-9810152559").subscribe((response) => {
                    console.log(response);
                }, (error) => {
                    console.log("User token ", error.error.text);
                    this.appService.searchFoodItemByName(event.target.value, error.error.text).then((resp) => {
                        this.searchItems = resp;
                        this.searchVisible = true;
                        this.foodItemAdd = "";
                    });
                    console.log("Category Error", error);
                });
            }
        }, 1500);
    }
    selectedItem(searchItem) {
        this.searchVisible = false;
        console.log("searchItem", searchItem);
        searchItem.macros = {
            "calories": searchItem.Calories,
            "fat": searchItem.Fat,
            "protein": (searchItem.Protien == undefined ? searchItem.Protein : searchItem.Protien),
            "carbs": searchItem.Carbs,
            "fiber": searchItem.Fiber
        };
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.push(searchItem.Food + `[${searchItem.portion}~(${searchItem.portion_unit})]`);
        // this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portion += searchItem.portion;
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portionUnit = searchItem.portion_unit;
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArrayJoin =
            this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.join(',');
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options.push(searchItem);
        if (this.totalLength[this.isOpenSlotIndex] >= 0) {
            this.totalLength[this.isOpenSlotIndex] = this.totalLength[this.isOpenSlotIndex] + 1;
        }
        else {
            this.totalLength[this.isOpenSlotIndex] = 0;
            this.totalLength[this.isOpenSlotIndex] = this.totalLength[this.isOpenSlotIndex] + 1;
        }
        console.log("this.totalLength", this.totalLength);
        this.foodItemSlots[this.isOpenSlotIndex].avg_macros.calories += (searchItem.Calories * searchItem.portion);
        this.foodItemSlots[this.isOpenSlotIndex].avg_macros.protein += ((searchItem.Protien == undefined ? searchItem.Protein : searchItem.Protien) * searchItem.portion);
        this.foodItemSlots[this.isOpenSlotIndex].avg_macros.fat += (searchItem.Fat * searchItem.portion);
        this.foodItemSlots[this.isOpenSlotIndex].avg_macros.carbs += (searchItem.Carbs * searchItem.portion);
        this.foodItemsArray.total_macros.calories += (searchItem.Calories * searchItem.portion);
        this.foodItemsArray.total_macros.protein += ((searchItem.Protien == undefined ? searchItem.Protein : searchItem.Protien) * searchItem.portion);
        this.foodItemsArray.total_macros.fat += (searchItem.Fat * searchItem.portion);
        this.foodItemsArray.total_macros.carbs += (searchItem.Carbs * searchItem.portion);
        //debugger;
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCalories += (searchItem.Calories * searchItem.portion);
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsProtien += ((searchItem.Protien == undefined ? searchItem.Protein : searchItem.Protien) * searchItem.portion);
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsFat += (searchItem.Fat * searchItem.portion);
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCarbs += (searchItem.Carbs * searchItem.portion);
        this.averageSlotTotalCal();
        this.cdr.detectChanges();
    }
    setCaloriesByPortionupdate() {
        let calories = 0;
        let protein = 0;
        let fat = 0;
        let carbs = 0;
        let slotCalory = 0;
        let slotProtein = 0;
        let slotFat = 0;
        let slotCarbs = 0;
        for (let index = 0; index < this.foodItemSlots.length; index++) {
            for (let j = 0; j < this.foodItemSlots[index].items.length; j++) {
                calories = 0;
                protein = 0;
                fat = 0;
                carbs = 0;
                for (let k = 0; k < this.foodItemSlots[index].items[j].options.length; k++) {
                    calories += (this.foodItemSlots[index].items[j].options[k].macros.calories * this.foodItemSlots[index].items[j].options[k].portion);
                    protein += (this.foodItemSlots[index].items[j].options[k].macros.protein * this.foodItemSlots[index].items[j].options[k].portion);
                    fat += (this.foodItemSlots[index].items[j].options[k].macros.fat * this.foodItemSlots[index].items[j].options[k].portion);
                    carbs += (this.foodItemSlots[index].items[j].options[k].macros.carbs * this.foodItemSlots[index].items[j].options[k].portion);
                }
                slotCalory = calories;
                slotProtein = protein;
                slotFat = fat;
                slotCarbs = carbs;
                this.foodItemSlots[index].items[j].itemsCalories = slotCalory;
                this.foodItemSlots[index].items[j].itemsProtien = slotProtein;
                this.foodItemSlots[index].items[j].itemsFat = slotFat;
                this.foodItemSlots[index].items[j].itemsCarbs = slotCarbs;
                slotCalory = 0;
                slotProtein = 0;
                slotFat = 0;
                slotCarbs = 0;
            }
            this.foodItemSlots[index].avg_macros.calories = calories;
            this.foodItemSlots[index].avg_macros.protein = protein;
            this.foodItemSlots[index].avg_macros.fat = fat;
            this.foodItemSlots[index].avg_macros.carbs = carbs;
        }
        this.foodItemsArray.total_macros.calories = calories;
        this.foodItemsArray.total_macros.protein = protein;
        this.foodItemsArray.total_macros.fat = fat;
        this.foodItemsArray.total_macros.carbs = carbs;
        this.averageSlotTotalCal();
    }
    goback() {
        this.router.navigate(['deitician-search']);
    }
    saveRecall() {
        const userId = localStorage.getItem('email');
        this.appService.saveDietRecall(userId, this.foodItemsArray).then(res => {
            console.log("test", res);
            this.utilities.presentAlert("Record saved successfully!");
        }, err => {
            console.log("err", err);
        });
    }
};
FoodItemChoicePage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_4__.UTILITIES },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ChangeDetectorRef },
    { type: _services__WEBPACK_IMPORTED_MODULE_3__.AppService },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_2__.AppService }
];
FoodItemChoicePage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-food-item-choice',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_food_item_choice_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_food_item_choice_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], FoodItemChoicePage);



/***/ }),

/***/ 17121:
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/food-item-choice/food-item-choice.page.html ***!
  \****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-icon name=\"arrow-back-outline\" color=\"#fff\" style=\"font-size: 2rem;\" (click)=\"goback()\"></ion-icon>\n    </ion-buttons>\n    <ion-title>Recall Choice</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  <ion-card style=\"text-align: center;margin-left: 10%; width: 80%;    box-shadow: 0 2px 7px 3px #f3f3f3b5;\">\n    <circle-progress radius=\"55\"\n  percent=\"100\"\n  [outerStrokeGradientStopColor]=\"'#fff'\"\n  [innerStrokeColor]=\"'#b3b3b3'\"\n  [outerStrokeColor]=\"'#bb0000'\"\n  outerStrokeWidth=\"5\" innerStrokeWidth=\"5\" [title]=\"calTotal\"\n  [titleFontSize]=\"22\"\n  [titleFontWeight]=\"500\" subtitle=\"Kcal\"\n  [subtitleFontWeight]=\"400\"\n  [subtitleFontSize]=\"15\" units=\"\" [unitsFontSize]=\"10\" [unitsFontWeight]=\"400\">\n </circle-progress>\n <ion-row>\n  <ion-col class=\"ion-text-left\">\n    <div>\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <img src=\"./assets/img/protein-new.png\">\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"ion-text-center\" style=\"padding-bottom: 5px;font-size: .8rem;color:#000\">\n          Protein\n        </ion-col>\n      </ion-row>\n     \n      <ion-row>\n        <ion-col class=\"ion-text-center\" *ngIf=\"sumTotalCalprotienfatcarbs(protien)<=0\">\n          <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n            0 gm (0%)\n          </span>\n        </ion-col>\n        <ion-col class=\"ion-text-center\" *ngIf=\"sumTotalCalprotienfatcarbs(protien)>0\">\n          <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n            {{sumTotalCalprotienfatcarbs(protien).toFixed(1)}} gm ({{(sumTotalCalprotienfatcarbs(protien)*100*4/sumTotalCalprotienfatcarbs(avgSlotTotalCal)).toFixed(0)}}%)\n       </span>\n          </ion-col>\n      </ion-row>\n    </div>\n  </ion-col>\n  <ion-col class=\"ion-text-center\">\n    <div>\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <img src=\"./assets/img/fat-new.png\">\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"ion-text-center\" style=\"padding-bottom: 5px;font-size: .8rem;color:#000\">\n          Fat\n        </ion-col>\n      </ion-row>\n     \n      <ion-row>\n        <ion-col class=\"ion-text-center\" *ngIf=\"sumTotalCalprotienfatcarbs(fat)<=0\">\n          <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n          0 gm (0%)\n          </span>\n        </ion-col>\n        <ion-col class=\"ion-text-center\" *ngIf=\"sumTotalCalprotienfatcarbs(fat)>0\">\n          <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n          {{sumTotalCalprotienfatcarbs(fat).toFixed(1)}} gm ({{(sumTotalCalprotienfatcarbs(fat)*100*9/sumTotalCalprotienfatcarbs(avgSlotTotalCal)).toFixed(0)}}%)\n        </span>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-col>\n  <ion-col class=\"ion-text-right\">\n    <div>\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <img src=\"./assets/img/carbs-new.png\">\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"ion-text-center\" style=\"padding-bottom: 5px;font-size: .8rem;color:#000\">\n          Carbs\n        </ion-col>\n      </ion-row>\n     \n      <ion-row>\n        <ion-col class=\"ion-text-center\" *ngIf=\"sumTotalCalprotienfatcarbs(carbs)<=0\">\n          <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n          0 gm (0%)\n          </span>\n        </ion-col>\n        <ion-col class=\"ion-text-center\" *ngIf=\"sumTotalCalprotienfatcarbs(carbs)>0\">\n          <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n          {{sumTotalCalprotienfatcarbs(carbs).toFixed(1)}} gm ({{(sumTotalCalprotienfatcarbs(carbs)*100*4/sumTotalCalprotienfatcarbs(avgSlotTotalCal)).toFixed(0)}}%)\n        </span>\n        </ion-col>\n      </ion-row>\n    </div>\n  </ion-col>\n</ion-row>\n<br>\n  </ion-card>\n<br>\n<div *ngFor=\"let items of foodItemSlots; let ind=index;\">\n  <ion-row style=\"margin-left: .8rem;margin-right: .8rem;color:#0F0F0F;font-size: .8rem;\">\n    <ion-col class=\"ion-text-left\">\n      <span style=\"font-weight:500;\">{{items?.name}}</span>\n    </ion-col>\n    <ion-col class=\"ion-text-right\">\n      <span style=\"font-weight:500;\">{{(avgSlotTotalCal[ind]).toFixed(1)}} Kcal</span>\n    </ion-col>\n  </ion-row>\n <ion-card style=\"box-shadow: none;background: #FAFAFA;border:1px solid #f3f3f3;border-radius: 5px;\">\n  <ion-card-header>\n    <ion-row>\n      <ion-col class=\"ion-text-left\">\n        <div>\n          <ion-row>\n            <ion-col class=\"ion-text-center\">\n              <img src=\"./assets/img/protein-new.png\">\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"ion-text-center\" style=\"color:#000;font-size: .8rem;\">\n              Protein\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"ion-text-center\" *ngIf=\"protien[ind]<=0\">\n              <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n              0 gm (0%)\n              </span>\n            </ion-col>\n            <ion-col class=\"ion-text-center\" *ngIf=\"protien[ind]>0\">\n              <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n              {{protien[ind]?.toFixed(1)}} gm ({{(protien[ind]*100*4/avgSlotTotalCal[ind]).toFixed(0)}}%)\n              </span>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n      <ion-col class=\"ion-text-center\">\n        <div>\n          <ion-row>\n            <ion-col class=\"ion-text-center\">\n              <img src=\"./assets/img/fat-new.png\">\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"ion-text-center\" style=\"color:#000;font-size: .8rem;\">\n              Fat\n            </ion-col>\n          </ion-row>\n          \n          <ion-row>\n            <ion-col class=\"ion-text-center\" *ngIf=\"fat[ind]<=0\">\n              <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n              0 gm (0%)\n              </span>\n            </ion-col>\n            <ion-col class=\"ion-text-center\" *ngIf=\"fat[ind]>0\">\n              <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n              {{fat[ind]?.toFixed(1)}} gm ({{(fat[ind]*100*9/avgSlotTotalCal[ind]).toFixed(0)}}%)\n              </span>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n      <ion-col class=\"ion-text-right\">\n        <div>\n          <ion-row>\n            <ion-col class=\"ion-text-center\">\n              <img src=\"./assets/img/carbs-new.png\">\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col class=\"ion-text-center\" style=\"color:#000;font-size: .8rem;\">\n              Carbs\n            </ion-col>\n          </ion-row>\n          \n          <ion-row>\n            <ion-col class=\"ion-text-center\" *ngIf=\"carbs[ind]>0\">\n              <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n              {{carbs[ind]?.toFixed(1)}} gm ({{(carbs[ind]*100*4/avgSlotTotalCal[ind]).toFixed(0)}}%)\n              </span>\n            </ion-col>\n            <ion-col class=\"ion-text-center\" *ngIf=\"carbs[ind]<=0\">\n              <span style=\"font-size: .8rem;color:#000;font-weight: 500;\">\n              0 gm (0%)\n              </span>\n            </ion-col>\n          </ion-row>\n        </div>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col size=\"12\">\n        <ion-searchbar\n        style=\"margin-right: 0;width: 100%;\"\n        search-icon=\"none\"\n        mode=\"md\" \n        (search)=\"addNewItem($event,ind)\"\n        placeholder=\"Enter item name\"\n        [(ngModel)]=\"addItem[ind]\"\n      ></ion-searchbar>\n      </ion-col>\n    </ion-row>\n  </ion-card-header>\n  <ion-card-content>\n    <div>\n      <ion-row>\n        <ion-col class=\"ion-align-self-center\" size=\"12\">\n         <div *ngFor=\"let item of items.items; let itemIndex=index;\" style=\"background: #08c2c36e;padding:1rem;border-radius: 10px;margin-top:1rem\">\n        <ion-row  style=\"margin-bottom: .2rem;\">  \n          <ion-col class=\"ion-text-left ion-align-self-center\" size=\"6\">\n            <span style=\"color: #000;\"> <b>{{item?.name}}</b><br>{{(item?.itemsCalories>0?(item?.itemsCalories/item?.itemsArray?.length):0)?.toFixed(1)}} Kcal</span>\n           </ion-col>\n           <ion-col class=\"ion-text-center ion-align-self-center\" size=\"5\" (click)=\"openPopup(ind,itemIndex)\" \n           style=\"font-size:.6rem;padding:5px;border: 1px solid #01A3A4;border-radius: 5px;background:#01A3A4;color: #fff;\">\n            <span>Manage Options</span>\n           </ion-col>\n          <ion-col class=\"ion-text-right ion-align-self-center\" size=\"1\" (click)=\"removeItems(ind,itemIndex,item?.name)\">\n            <ion-icon name=\"trash\" style=\"color: #ca3838;font-size: 1.1rem;\"></ion-icon>\n         </ion-col>\n         </ion-row>\n         <ion-row *ngIf=\"item.itemsArrayJoin?.length>0\">\n         <ion-col class=\"ion-text-left\" size=\"12\" >\n          <div style=\"height:60px;background: #fff; padding:10px;font-size: .75rem; white-space: normal;border:1px solid #b3b3b3;border-radius: 5px;font-weight: 500;\">\n            {{item.itemsArrayJoin}}\n          </div>\n        </ion-col>\n       \n      </ion-row>\n    </div> \n    <!--move from here-->\n      </ion-col>\n      </ion-row>\n    </div>\n  </ion-card-content>\n </ion-card>\n </div>\n<br>\n <ion-card style=\"padding: 1rem;\">\n  <div>\n    <ion-row>\n      <ion-col size=\"5.5\" class=\"ion-text-center\">\n        <ion-button class=\"button\" shape=\"round\"  expand=\"block\" class=\"button button-full button-outline\" (click)=\"goback()\">Cancel</ion-button>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n      <ion-col size=\"5.5\" class=\"ion-text-center\">\n        <ion-button class=\"button button-full\" expand=\"block\" shape=\"round\" (click)=\"saveRecall()\" color=\"primary\">Save</ion-button>\n      </ion-col>\n    </ion-row>\n  </div>\n </ion-card>\n\n</ion-content>\n\n <div *ngIf=\"isOpen\">\n \n<div style=\"background: #000;\nopacity: .7;\nwidth: 100%;\nheight: 100%;\nposition: absolute;\nz-index: 12;\npadding: 20% 5%;\ntop: 0;\"></div>\n<div style=\"background: #fff;\nwidth: 80%;\nheight: 60%;\nposition: absolute;\nz-index: 13;\ntop: 30%;\npadding: 1rem;\nleft: 10%;\">\n<ion-button class=\"button button-full\" expand=\"block\" style=\"float: right;font-size: .8rem;\" shape=\"round\" (click)=\"closePopup()\" color=\"primary\">Done</ion-button>\n <br><br><br>\n <div *ngFor=\"let foodItem of foodItemSlots[isOpenSlotIndex].items[isOpenItemIndex].itemsArray let foodInd=index;\"\n style=\"background: #08c2c36e;padding:1rem;border-radius: 10px;margin-top:1rem;\">\n  <ion-row>\n    <ion-col class=\"ion-text-left\" size=\"8\">\n\n      <!-- <ion-searchbar\n      search-icon=\"none\"\n      mode=\"md\" \n      disabled=\"true\"\n      cancel-button-icon=\"false\"\n      placeholder=\"Search for Food\"\n      value=\"{{foodItem}}\"\n      style=\"min-width: 95%;background: #fff;\"\n    >\n    </ion-searchbar> -->\n    <ion-input disabled=\"true\" style=\"min-width: 60px;background: #fff;\" type=\"text\" \n    value=\"{{foodItem}}\"></ion-input>  \n    </ion-col>\n    <ion-col size=\"2\" class=\"ion-text-center ion-align-self-center\">\n      <ion-input style=\"min-width: 60px;background: #fff;\" type=\"number\" value=\"{{foodItem.split('[')[1]?.split('~')[0]}}\"  \n      (input)=\"updateCaloriesOndemand($event,foodItem,foodItem?.split('[')[1].split('~')[1])\"></ion-input>     \n    </ion-col>\n    <ion-col class=\"ion-text-center ion-align-self-center\" size=\"2\">\n      <ion-icon name=\"close\" (click)=\"removeMoreItems(foodItem)\" style=\"font-size: 1.3rem;color:hsl(0, 89%, 34%)\"></ion-icon>\n    </ion-col>\n  </ion-row>\n</div>\n  <br>\n  <ion-row>\n    <ion-col size=\"12\">\n     <div style=\"font-size: 1rem;color: #707070;\">\n      Search Options:\n     </div>\n      <div>\n    <ion-input (keyup)=\"searchAllApiData($event)\" [(ngModel)]=\"foodItemAdd\" placeholder=\"Search for Food\" style=\"min-width: 100%;\"></ion-input>\n    <div *ngIf=\"searchVisible\" style=\"height: 283px; overflow-y: scroll; border: 1px solid #b3b3b3;padding: 1rem;border-radius: 8px;box-shadow: 5px 5px 5px #b3b3b3;\">\n      <div style=\"float: left;margin:.3rem 0;width: 100%;font-size: 1.2rem;\n      border-bottom: 1px solid #b3b3b3;\n      padding: 10px;\n      background: #f3f3f3;\">\n       Home Based\n      </div><br>\n      <ion-row *ngFor=\"let searchItem of searchItems.homeBased; let searchIndex=index;\" style=\"padding: 10px; border-bottom: 1px solid #b3b3b3;\">\n      <ion-col (click)=\"selectedItem(searchItem)\">\n        {{searchItem?.Food}}\n      </ion-col>\n    </ion-row>\n    <div style=\"float: left;margin:.3rem 0rem;width: 100%;font-size: 1.2rem;\n    border-bottom: 1px solid #b3b3b3;\n    padding: 10px;\n    background: #f3f3f3;\">\n      packaged\n     </div><br>\n     <ion-row *ngFor=\"let searchItem of searchItems.packaged; let searchIndex=index;\" style=\"padding: 10px; border-bottom: 1px solid #b3b3b3;\">\n     <ion-col (click)=\"selectedItem(searchItem)\">\n       {{searchItem?.Food}}\n     </ion-col>\n   </ion-row>\n   <div style=\"float: left;margin:.3rem 0rem;width: 100%;font-size: 1.2rem;\n   border-bottom: 1px solid #b3b3b3;\n   padding: 10px;\n   background: #f3f3f3;\">\n    Restaurant\n   </div><br>\n   <ion-row *ngFor=\"let searchItem of searchItems.restaurant; let searchIndex=index;\" style=\"padding: 10px; border-bottom: 1px solid #b3b3b3;\">\n   <ion-col (click)=\"selectedItem(searchItem)\">\n     {{searchItem?.Food}}\n   </ion-col>\n </ion-row>\n  </div>\n  </div>\n    </ion-col>\n \n  </ion-row>\n</div>\n\n</div> ");

/***/ }),

/***/ 68038:
/*!*************************************************************!*\
  !*** ./src/app/food-item-choice/food-item-choice.page.scss ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = "ion-searchbar {\n  padding: 0 10px !important;\n  margin: 0;\n  margin-top: 0.5rem;\n  --box-shadow: none;\n  border: 1px solid #b3b3b3;\n  border-radius: 5px;\n  min-width: 120px;\n  width: 72%;\n}\n\nion-input {\n  padding: 0 10px !important;\n  margin: 0;\n  margin-top: 0.5rem;\n  --box-shadow: none;\n  border: 1px solid #b3b3b3;\n  border-radius: 5px;\n  min-width: 120px;\n  width: 77%;\n}\n\nion-searchbar.searchbar-input {\n  padding-left: 0 !important;\n  padding-right: 0 !important;\n}\n\nion-searchbar ::ng-deep .searchbar-clear-icon {\n  display: none !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZvb2QtaXRlbS1jaG9pY2UucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsMEJBQUE7RUFDQSxTQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxnQkFBQTtFQUNFLFVBQUE7QUFDSjs7QUFDQTtFQUNJLDBCQUFBO0VBQ0EsU0FBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7RUFDRSxVQUFBO0FBRU47O0FBQUE7RUFDQywwQkFBQTtFQUNBLDJCQUFBO0FBR0Q7O0FBRVU7RUFDRSx3QkFBQTtBQUNaIiwiZmlsZSI6ImZvb2QtaXRlbS1jaG9pY2UucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXNlYXJjaGJhcntcbiAgcGFkZGluZzogMCAxMHB4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMDtcbiAgbWFyZ2luLXRvcDogLjVyZW07XG4gIC0tYm94LXNoYWRvdzogbm9uZTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2IzYjNiMztcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBtaW4td2lkdGg6IDEyMHB4O1xuICAgIHdpZHRoOiA3MiU7XG59XG5pb24taW5wdXR7XG4gICAgcGFkZGluZzogMCAxMHB4ICFpbXBvcnRhbnQ7XG4gICAgbWFyZ2luOiAwO1xuICAgIG1hcmdpbi10b3A6IC41cmVtO1xuICAgIC0tYm94LXNoYWRvdzogbm9uZTtcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjYjNiM2IzO1xuICAgIGJvcmRlci1yYWRpdXM6IDVweDtcbiAgICBtaW4td2lkdGg6IDEyMHB4O1xuICAgICAgd2lkdGg6IDc3JTtcbiAgfVxuaW9uLXNlYXJjaGJhci5zZWFyY2hiYXItaW5wdXR7XG4gcGFkZGluZy1sZWZ0OiAwICFpbXBvcnRhbnQ7XG4gcGFkZGluZy1yaWdodDogMCAhaW1wb3J0YW50O1xufVxuXG4gICAgaW9uLXNlYXJjaGJhciB7XG4gICAgICAgOjpuZy1kZWVwIHtcbiAgICAgICAgICAuc2VhcmNoYmFyLWNsZWFyLWljb24ge1xuICAgICAgICAgICAgZGlzcGxheTogbm9uZSAhaW1wb3J0YW50O1xuICAgICAgICAgIH1cbiAgICAgIFxuICAgICAgICB9XG4gICAgICBcbiAgICAgIH0iXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_food-item-choice_food-item-choice_module_ts.js.map