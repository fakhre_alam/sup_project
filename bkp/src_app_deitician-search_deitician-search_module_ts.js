"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_deitician-search_deitician-search_module_ts"],{

/***/ 98867:
/*!*********************************************************************!*\
  !*** ./src/app/deitician-search/deitician-search-routing.module.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DeiticianSearchPageRoutingModule": () => (/* binding */ DeiticianSearchPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _deitician_search_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./deitician-search.page */ 18062);




const routes = [
    {
        path: '',
        component: _deitician_search_page__WEBPACK_IMPORTED_MODULE_0__.DeiticianSearchPage
    }
];
let DeiticianSearchPageRoutingModule = class DeiticianSearchPageRoutingModule {
};
DeiticianSearchPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], DeiticianSearchPageRoutingModule);



/***/ }),

/***/ 10415:
/*!*************************************************************!*\
  !*** ./src/app/deitician-search/deitician-search.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DeiticianSearchPageModule": () => (/* binding */ DeiticianSearchPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _deitician_search_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./deitician-search-routing.module */ 98867);
/* harmony import */ var _deitician_search_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./deitician-search.page */ 18062);
/* harmony import */ var _fulldayname_pipe__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../fulldayname.pipe */ 53530);








let DeiticianSearchPageModule = class DeiticianSearchPageModule {
};
DeiticianSearchPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _deitician_search_routing_module__WEBPACK_IMPORTED_MODULE_0__.DeiticianSearchPageRoutingModule
        ],
        declarations: [_deitician_search_page__WEBPACK_IMPORTED_MODULE_1__.DeiticianSearchPage, _fulldayname_pipe__WEBPACK_IMPORTED_MODULE_2__.FulldaynamePipe]
    })
], DeiticianSearchPageModule);



/***/ }),

/***/ 18062:
/*!***********************************************************!*\
  !*** ./src/app/deitician-search/deitician-search.page.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DeiticianSearchPage": () => (/* binding */ DeiticianSearchPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_deitician_search_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./deitician-search.page.html */ 52447);
/* harmony import */ var _deitician_search_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./deitician-search.page.scss */ 37304);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _app_app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ 38198);
/* harmony import */ var _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../core/constants/constants */ 92133);
/* harmony import */ var _services_app_app_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/app/app.service */ 61535);
/* harmony import */ var _shared_constants_constants__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../shared/constants/constants */ 66239);
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ 3242);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../core/utility/utilities */ 53533);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! moment */ 29243);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_8__);













let DeiticianSearchPage = class DeiticianSearchPage {
    constructor(ngzone, utilities, appservice, iab, router, appS, appServe) {
        this.ngzone = ngzone;
        this.utilities = utilities;
        this.appservice = appservice;
        this.iab = iab;
        this.router = router;
        this.appS = appS;
        this.appServe = appServe;
        this.isHindi = true;
        this.current_year = new Date().getFullYear();
        this.d = new Date();
        this.dateValue = "";
        this.dateValue2 = "";
        this.dietPlan = [
            { id: 'fiteloWaterRetention', name: 'Water retention' },
            { id: 'fiteloWeightLoss', name: 'High Protein fiber' },
            { id: 'weightLoss', name: 'Weight Loss' },
            { id: 'muscleGain_morning', name: 'Muscle Gain Morning' },
            { id: 'muscleGain_evening', name: 'Muscle Gain Evening' },
            { id: 'fatShredding_morning', name: 'Fat Shredding Morning' },
            { id: 'fatShredding_evening', name: 'Fat Shredding Evening' },
            { id: 'diabetes', name: 'Diabetes' },
            { id: 'pcos', name: 'PCOS' },
            { id: 'cholesterol', name: 'Cholesterol' },
            { id: 'hypertension', name: 'Hypertension' }
        ];
        this.search = localStorage.getItem('email');
        this.isOpenProfile = true;
        this.isOpenProfile1 = true;
        this.isSearch = false;
        this.username = "";
        this.dietData = [
            { value: '0', name: 'When You Wake up' },
            { value: '1', name: 'Before Breakfast' },
            { value: '2', name: 'Breakfast' },
            { value: '3', name: 'Mid Day Meal' },
            { value: '4', name: 'Lunch' },
            { value: '5', name: 'Post Lunch' },
            { value: '6', name: 'Evening Snack' },
            { value: '7', name: 'Dinner' },
            { value: '8', name: 'Before Sleep' },
        ];
        this.profileData = {};
        this.suggestedWeightRange = 0;
        this.isOpen1 = false;
        this.isOpen2 = false;
        this.isOpen3 = false;
        this.isOpen4 = false;
        this.isOpen5 = false;
        this.isOpen6 = false;
        this.profileInititals = "";
        this.checkedDetox = 0;
        this.diseases = [];
        this.diseases1 = [];
        this.alergies = ['SF,SO', 'ML', 'F', 'E', 'N', 'G'];
        this.community = [];
        this.username = "";
        this.isSearch = false;
        this.username = localStorage.getItem("loginEmail");
        this.search = localStorage.getItem('email');
        // window.location.reload();
    }
    checkPlan() {
        this.appservice.getOnePlan().then(res => {
            this.plandata = res;
            let exp = new Date(this.plandata.planExpiryDate).getTime();
            let newDate = new Date().getTime();
            console.log(exp, newDate);
            if (exp > newDate) {
                this.plandata.isPlanActive = true;
            }
            console.log("plan==========>", res);
        });
    }
    formatDate(value) {
        return moment__WEBPACK_IMPORTED_MODULE_8___default()(value).format("DD-MMM-YYYY");
    }
    isOpen1click() {
        this.isOpen1 = !this.isOpen1;
    }
    isOpen2click() {
        this.isOpen2 = !this.isOpen2;
    }
    isOpen3click() {
        this.isOpen3 = !this.isOpen3;
    }
    isOpen4click() {
        this.isOpen4 = !this.isOpen4;
    }
    isOpen5click() {
        this.isOpen5 = !this.isOpen5;
    }
    isOpen6click() {
        this.isOpen6 = !this.isOpen6;
    }
    submitCopyDietPlan() {
        this.appS.copyDietPlan(this.search, moment__WEBPACK_IMPORTED_MODULE_8___default()(this.dateValue).format("DDMMYYYY"), moment__WEBPACK_IMPORTED_MODULE_8___default()(this.dateValue2).format("DDMMYYYY")).then(res => {
            console.log("res", res);
            this.utilities.presentAlert("Diet Plan Copied successfully!");
        }, err => {
            console.log("error", err);
        });
    }
    getProfile() {
        this.profileData = [];
        this.appservice.getProfile().then((res) => {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v;
            this.profileData = res;
            if (((_b = (_a = this.profileData) === null || _a === void 0 ? void 0 : _a.profile) === null || _b === void 0 ? void 0 : _b.subCategory) === "weightloss") {
                this.profileData.profile.subCategory = "Weight Loss";
            }
            if (((_d = (_c = this.profileData) === null || _c === void 0 ? void 0 : _c.profile) === null || _d === void 0 ? void 0 : _d.subCategory) === "weightmaintenance") {
                this.profileData.profile.subCategory = "Weight Maintenance";
            }
            if (((_f = (_e = this.profileData) === null || _e === void 0 ? void 0 : _e.profile) === null || _f === void 0 ? void 0 : _f.subCategory) === "musclebuilding") {
                this.profileData.profile.subCategory = "Muscle Building";
            }
            if (((_h = (_g = this.profileData) === null || _g === void 0 ? void 0 : _g.profile) === null || _h === void 0 ? void 0 : _h.subCategory) === "leanbody") {
                this.profileData.profile.subCategory = "Lean Body";
            }
            // this.storage.get("localData").then((val) => {
            //   this.localData = JSON.parse(val);
            //   console.log(this.localData);
            //   this.profileData.lifeStyle.foodType = this.localData?.otherMaster?.foodPref.find(
            //     (f) => f.code === this.profileData.lifeStyle.foodType
            //   );
            //   this.profileData.lifeStyle.country = this.localData?.countries.find(
            //     (f) => f._id === this.profileData.lifeStyle.country
            //   );
            //   this.profileData.lifeStyle.activities = this.localData?.otherMaster?.activities.find(
            //     (item) => this.profileData?.lifeStyle?.activities.code === item.code
            //   );
            //   this.profileData.lifeStyle.communities = this.localData?.otherMaster?.community.filter(
            //     (item) =>
            //       this.profileData?.lifeStyle?.communities?.includes(item.code)
            //   );
            //   this.profileData.lifeStyle.diseases = this.localData?.otherMaster?.diseases.filter(
            //     (item) => this.profileData?.lifeStyle?.diseases?.includes(item.code)
            //   );
            // });
            let h = ((_l = (_k = (_j = this.profileData) === null || _j === void 0 ? void 0 : _j.demographic) === null || _k === void 0 ? void 0 : _k.height) === null || _l === void 0 ? void 0 : _l.unit) === "in"
                ? ((_p = (_o = (_m = this.profileData) === null || _m === void 0 ? void 0 : _m.demographic) === null || _o === void 0 ? void 0 : _o.height) === null || _p === void 0 ? void 0 : _p.value) / 12
                : (_s = (_r = (_q = this.profileData) === null || _q === void 0 ? void 0 : _q.demographic) === null || _r === void 0 ? void 0 : _r.height) === null || _s === void 0 ? void 0 : _s.value;
            if (((_v = (_u = (_t = this.profileData) === null || _t === void 0 ? void 0 : _t.demographic) === null || _u === void 0 ? void 0 : _u.height) === null || _v === void 0 ? void 0 : _v.unit) === "in") {
                console.log(h);
                h = h.toString().split(".");
                console.log(h);
                const h1 = (h[1] / 0.0833333).toString().split("0")[0];
                console.log(h1);
                this.profileData.demographic.height.value = `${h[0]}' ${h1}"`;
            }
            else {
                this.profileData.demographic.height.value =
                    this.profileData.demographic.height.value + " cm";
            }
            console.log(this.profileData);
        });
    }
    languagePref(event) {
        this.appServe.languagePref(event.detail.checked ? 'English' : 'Hindi', this.search).subscribe(res => {
            console.log("sucess", res);
        }, err => {
            console.log("err", err);
        });
    }
    logout() {
        localStorage.clear();
        localStorage.setItem('email', "");
        this.searchDetails = undefined;
        this.search = '';
        this.router.navigate(['login']);
    }
    ngOnInit() {
        this.username = "";
        this.username = localStorage.getItem("loginEmail");
        this.search = localStorage.getItem('email');
    }
    fetchProfile() {
        var _a;
        if (!((_a = this.search) === null || _a === void 0 ? void 0 : _a.trim())) {
            return;
        }
        _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.email = this.search;
        localStorage.setItem("email", this.search);
        this.isSearch = true;
        this.appS.searchProfile(this.search).then((profileData) => {
            var _a, _b, _c, _d, _e, _f;
            console.log("profileData:-", profileData);
            this.additionalPref(profileData["additionalPref"]);
            const data = profileData['profile'];
            if (data) {
                this.searchDetails = JSON.parse(JSON.stringify(profileData));
                const planC = this.dietPlan.filter(item => {
                    var _a, _b;
                    return item.id === ((_b = (_a = this.searchDetails) === null || _a === void 0 ? void 0 : _a.additionalPref) === null || _b === void 0 ? void 0 : _b.planChosen);
                });
                this.planChoosen = (_a = planC[0]) === null || _a === void 0 ? void 0 : _a.name;
                this.updateTargetCal = (_c = (_b = this.searchDetails) === null || _b === void 0 ? void 0 : _b.lifeStyle) === null || _c === void 0 ? void 0 : _c.calories;
                this.recommendedTargetCal = (_e = (_d = this.searchDetails) === null || _d === void 0 ? void 0 : _d.lifeStyle) === null || _e === void 0 ? void 0 : _e.calories;
                const dd = JSON.parse(JSON.stringify(profileData));
                this.profileInititals = dd === null || dd === void 0 ? void 0 : dd.profile.name;
                // dd?.profile.name?.replace("  "," ")?.split(' ')[0][0].toUpperCase() + 
                // (dd?.profile?.name?.replace("  "," ")?.split(' ')?.length>=2?dd?.profile?.name?.replace("  "," ")?.split(' ')[1][0].toUpperCase():dd?.profile?.name?.replace("  "," ")?.split(' ')[0][1].toUpperCase());
                this.isHindi = ((_f = profileData['profile']) === null || _f === void 0 ? void 0 : _f.languagePref) == undefined ? false : true;
                // this.search ='';
                this.appServe.getUserToken(this.search).subscribe((response) => {
                    console.log(response);
                }, (error) => {
                    console.log("User token ", error.error.text);
                    localStorage.setItem('personal_token', error.error.text);
                    console.log("Category Error", error);
                });
                this.getProfile();
                this.getToken1();
            }
            else {
                this.searchDetails = undefined;
            }
            this.appS.getBeatoData(this.search).then(res => {
                console.log("response", res);
            }, err => {
                console.log("error", err);
            });
        });
    }
    additionalPref(additionalPref) {
        this.additionPreferences = additionalPref;
    }
    viewProfile() {
        this.router.navigate(['new-profile']);
    }
    ngAfterViewInit() {
        this.username = "";
        this.username = localStorage.getItem("loginEmail");
        this.search = localStorage.getItem('email');
        if (localStorage.getItem('email')) {
            this.fetchProfile();
            this.defaultDetail = JSON.parse(localStorage.getItem("defaultData"));
            console.log("this.defaultDetail", this.defaultDetail);
            this.appS.searchProfile(this.search).then((profileData) => {
                var _a, _b, _c;
                console.log("profileData:-", profileData);
                localStorage.setItem("email", this.search);
                _core_constants_constants__WEBPACK_IMPORTED_MODULE_3__.CONSTANTS.email = localStorage.getItem('email');
                if (profileData.demographic != undefined) {
                    this.searchDetails = profileData;
                }
                else {
                    this.searchDetails = undefined;
                }
                console.log("searchDetails?.demographic?.height?.value", (_c = (_b = (_a = this.searchDetails) === null || _a === void 0 ? void 0 : _a.demographic) === null || _b === void 0 ? void 0 : _b.height) === null || _c === void 0 ? void 0 : _c.value);
            });
        }
    }
    gotoBeato() {
        const start_date = moment__WEBPACK_IMPORTED_MODULE_8___default()(new Date()).format("YYYY-MM-DD");
        const end_date = moment__WEBPACK_IMPORTED_MODULE_8___default()(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 7)).format("YYYY-MM-DD");
        console.log(`https://care.internal.beatoapp.com/diet-chart-listing?userid=${this.search}&flag=0&start=${start_date}&end=${end_date}`);
        window.open(`https://care.internal.beatoapp.com/diet-chart-listing?userid=${this.search}&flag=0&start=${start_date}&end=${end_date}`, "_blank");
    }
    goBack() {
        this.router.navigate(['tabs/home']);
    }
    updateCustomer() {
        this.router.navigate(['addupdate']);
    }
    updateRefferal() {
        this.router.navigate(['/tabs/admin']);
    }
    viewUserDetail() {
        this.router.navigate(['/tabs/admin']);
    }
    updateDietplan() {
        if (this.username !== 'beato') {
            this.router.navigate(['/tabs/consume']);
        }
        else {
            this.router.navigate(['/consume']);
        }
    }
    gotoPreferences() {
        this.router.navigate(['preference-diet']);
    }
    gotoDietPlan() {
        if (this.username !== 'beato') {
            this.router.navigate(['/tabs/consume']);
        }
        else {
            this.router.navigate(['/consume']);
        }
    }
    updateFoodChoices() {
        this.router.navigate(['personalise']);
    }
    goToClientApp(token) {
        let link = _shared_constants_constants__WEBPACK_IMPORTED_MODULE_5__.APIS.mainUrl + "?token=" + token;
        console.log("link", link);
        this.iab.create(link, "_system", "location=yes");
    }
    gotoupdateRemarks() {
        // goto slot remarks update
        this.router.navigate(["update-slot-remarks"], { queryParams: { params: this.search } });
    }
    getTokenUpdateTargetCal() {
        if (!localStorage.getItem('email')) {
            // alert("Please Enter Customer ID");
            return;
        }
        else {
            this.appServe.getUserToken(localStorage.getItem('email')).subscribe((response) => {
                console.log(response);
            }, (error) => {
                console.log("User token ", error.error.text);
                this.appS.updateTargetCal(this.updateTargetCal, this.search, error.error.text);
                this.recommendedTargetCal = Object.assign(this.updateTargetCal);
                localStorage.setItem("tknupdatetarget", error.error.text);
                console.log("Category Error", error);
            });
        }
    }
    getToken() {
        if (!localStorage.getItem('email')) {
            // alert("Please Enter Customer ID");
            return;
        }
        else {
            this.appServe.getUserToken(localStorage.getItem('email')).subscribe((response) => {
                console.log(response);
            }, (error) => {
                console.log("User token ", error.error.text);
                this.goToClientApp(error.error.text);
                localStorage.setItem("tknupdatetarget", error.error.text);
                console.log("Category Error", error);
            });
        }
    }
    getToken1() {
        if (!localStorage.getItem('email')) {
            this.utilities.presentAlert("Please enter email");
        }
        else {
            this.appServe.getUserToken(localStorage.getItem('email')).subscribe((response) => {
                console.log(response);
            }, (error) => {
                console.log("User token ", error.error.text);
                this.appS.getDefaultData(error.error.text).then(res => {
                    var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y, _z, _0, _1, _2, _3, _4, _5;
                    this.diseases = [];
                    this.diseases1 = [];
                    this.alergies = ['SF,SO', 'ML', 'F', 'E', 'N', 'G'];
                    this.mealPref;
                    this.community = [];
                    this.defaultData = res;
                    this.activityName = this.defaultData.otherMaster.activities.filter(item => {
                        var _a, _b;
                        return item.code === ((_b = (_a = this.searchDetails) === null || _a === void 0 ? void 0 : _a.lifeStyle) === null || _b === void 0 ? void 0 : _b.activities["code"]);
                    });
                    this.age = (_c = (_b = (_a = this.defaultData) === null || _a === void 0 ? void 0 : _a.otherMaster) === null || _b === void 0 ? void 0 : _b.ageMaster) === null || _c === void 0 ? void 0 : _c.data.filter(item => {
                        var _a, _b;
                        return item.code === ((_b = (_a = this.searchDetails) === null || _a === void 0 ? void 0 : _a.demographic) === null || _b === void 0 ? void 0 : _b.age["code"]);
                    });
                    for (let index = 0; index < ((_e = (_d = this.searchDetails) === null || _d === void 0 ? void 0 : _d.lifeStyle) === null || _e === void 0 ? void 0 : _e.diseases.length); index++) {
                        for (let j = 0; j < ((_g = (_f = this.defaultData) === null || _f === void 0 ? void 0 : _f.otherMaster) === null || _g === void 0 ? void 0 : _g.diseases.length); j++) {
                            if (((_j = (_h = this.defaultData) === null || _h === void 0 ? void 0 : _h.otherMaster) === null || _j === void 0 ? void 0 : _j.diseases[j].code) === ((_l = (_k = this.searchDetails) === null || _k === void 0 ? void 0 : _k.lifeStyle) === null || _l === void 0 ? void 0 : _l.diseases[index])) {
                                if (this.alergies.includes((_o = (_m = this.defaultData) === null || _m === void 0 ? void 0 : _m.otherMaster) === null || _o === void 0 ? void 0 : _o.diseases[j].code)) {
                                    this.diseases1.push((_q = (_p = this.defaultData) === null || _p === void 0 ? void 0 : _p.otherMaster) === null || _q === void 0 ? void 0 : _q.diseases[j].value);
                                }
                                else {
                                    this.diseases.push((_s = (_r = this.defaultData) === null || _r === void 0 ? void 0 : _r.otherMaster) === null || _s === void 0 ? void 0 : _s.diseases[j].value);
                                }
                            }
                        }
                    }
                    this.mealPref = (_v = (_u = (_t = this.defaultData) === null || _t === void 0 ? void 0 : _t.otherMaster) === null || _u === void 0 ? void 0 : _u.foodPref) === null || _v === void 0 ? void 0 : _v.filter(item => {
                        var _a, _b;
                        return item.code === ((_b = (_a = this.searchDetails) === null || _a === void 0 ? void 0 : _a.lifeStyle) === null || _b === void 0 ? void 0 : _b.foodType);
                    });
                    for (let index = 0; index < ((_x = (_w = this.searchDetails) === null || _w === void 0 ? void 0 : _w.lifeStyle) === null || _x === void 0 ? void 0 : _x.communities.length); index++) {
                        for (let j = 0; j < ((_z = (_y = this.defaultData) === null || _y === void 0 ? void 0 : _y.otherMaster) === null || _z === void 0 ? void 0 : _z.community.length); j++) {
                            if (((_1 = (_0 = this.defaultData) === null || _0 === void 0 ? void 0 : _0.otherMaster) === null || _1 === void 0 ? void 0 : _1.community[j].code) === ((_3 = (_2 = this.searchDetails) === null || _2 === void 0 ? void 0 : _2.lifeStyle) === null || _3 === void 0 ? void 0 : _3.communities[index])) {
                                this.community.push((_5 = (_4 = this.defaultData) === null || _4 === void 0 ? void 0 : _4.otherMaster) === null || _5 === void 0 ? void 0 : _5.community[j].value);
                            }
                        }
                    }
                });
                this.appS.getOnePlan1(error.error.text).then(res => {
                    console.log("getOnePlan", res);
                });
                console.log("Category Error", error);
            });
        }
    }
    gotoRecall() {
        this.router.navigate(['food-item-choice']);
    }
};
DeiticianSearchPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_9__.NgZone },
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_7__.UTILITIES },
    { type: _app_app_service__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_6__.InAppBrowser },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_10__.Router },
    { type: _app_app_service__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _services_app_app_service__WEBPACK_IMPORTED_MODULE_4__.AppService }
];
DeiticianSearchPage = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-deitician-search',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_deitician_search_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_deitician_search_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], DeiticianSearchPage);



/***/ }),

/***/ 53530:
/*!*************************************!*\
  !*** ./src/app/fulldayname.pipe.ts ***!
  \*************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FulldaynamePipe": () => (/* binding */ FulldaynamePipe)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 14001);


let FulldaynamePipe = class FulldaynamePipe {
    transform(value) {
        switch (value) {
            case 'SUN': {
                return 'Sunday';
                break;
            }
            case 'MON': {
                return 'Monday';
                break;
            }
            case 'TUE': {
                return 'Tuesday';
                break;
            }
            case 'WED': {
                return 'Wednesday';
                break;
            }
            case 'THU': {
                return 'Thursday';
                break;
            }
            case 'FRI': {
                return 'Friday';
                break;
            }
            case 'SAT': {
                return 'Saturday';
                break;
            }
            default:
                '';
                break;
        }
    }
};
FulldaynamePipe = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Pipe)({
        name: 'fulldayname'
    })
], FulldaynamePipe);



/***/ }),

/***/ 52447:
/*!****************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/deitician-search/deitician-search.page.html ***!
  \****************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n<ion-content>\n  <div style=\"    background: #0B713F;\n  height: 12rem;\n  width: 100%;\n  padding: 2rem;\n  border-bottom-left-radius: 1rem;\n  border-bottom-right-radius: 1rem;\">\n   \n    <ion-row>\n      <ion-col class=\"ion-text-left\" size=\"1.5\" *ngIf=\"username==='manasvi'\" style=\"    color: #fff;\">\n        <ion-icon name=\"arrow-back-outline\" color=\"#fff\" style=\"font-size: 2rem;\" (click)=\"goBack()\"></ion-icon> \n      </ion-col>\n      <ion-col class=\"ion-text-left\" style=\"color:#fff; font-size:1.2rem;margin-bottom:1rem\">\n       <b> Welcome: {{username}} </b>\n      </ion-col>\n      <ion-col class=\"ion-text-right ion-align-self-start\">\n        <ion-button (click)=\"logout()\" style=\"--background:#B7D340;--border-color: #f3f3f3;\n        height: 36px;\" shape=\"round\">Logout</ion-button>\n      </ion-col>\n    </ion-row><br>\n  <ion-row>\n    <ion-col class=\"ion-text-left\" style=\"font-size: .8rem;color:#fff;\">\n      Create daily diet plan to make people healthier!\n    </ion-col>\n  </ion-row><br>\n    <ion-row>\n      <ion-col size=\"10\" class=\"ion-text-left\" style=\"padding-left:1rem;color:#333; font-size:1.2rem;background: #fff;border: 1px solid #b3b3b3;border-radius: 1rem;\">\n        <ion-input [(ngModel)]=\"search\"  placeholder=\"search profile\" style=\"padding-left:2rem;\"></ion-input>\n      </ion-col>\n     <ion-col size=\"2\" class=\"ion-text-right ion-align-self-center\">\n      <ion-icon name=\"search-outline\" style=\"font-size: 2rem;color:#fff\" (click)=\"fetchProfile()\"></ion-icon> \n     </ion-col>\n    </ion-row>\n  </div>\n  \n<ion-card style=\"background-color: #fff;height: 800px; margin:0\" *ngIf=\"!searchDetails\">\n  <div class=\"search-detail\" style=\"margin: 2rem;\" >\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <div style=\"height: 325px;\n        background: #F7F7F7;\n        display: grid;\n        vertical-align: middle;\n        padding-top: 80px;\n        border-radius: 1rem;\">\n         <img src=\"./assets/img/nothing.png\" style=\"margin: 0 auto;\n         text-align: center;\n         width: 50%;\n         height: auto;\n         vertical-align: middle;\n         position: relative;\n         top: -3rem;\">\n          <span style=\"color:#000;font-size: .8rem;\" *ngIf=\"!searchDetails && isSearch\">\n            Sorry, we couldn’t find the  \n            user {{search}} \n         </span> \n         <span style=\"color:#565656;font-size: .8rem;\" *ngIf=\"!isSearch\">\n          Search a user by using the \n          exact ID of the user. \n         </span> \n        </div>\n      </ion-col>\n    </ion-row>\n  </div>\n</ion-card>\n\n<div *ngIf=\"searchDetails\">\n<ion-card style=\"padding: .5rem;\">\n  <ion-card-header (click)=\"isOpen1click()\" style=\"padding: 0;\">\n    <ion-row>\n      <ion-col size=\"11\" class=\"ion-text-center\">\n        <span style=\"font-size: 1.3rem;font-weight: 600;color:#000\"> {{profileInititals}}</span>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n    </ion-row>\n    <ion-icon name=\"chevron-down-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"isOpen1\"></ion-icon>\n    <ion-icon name=\"chevron-up-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"!isOpen1\"></ion-icon>\n  </ion-card-header>\n  <ion-card-content *ngIf=\"isOpen1\" style=\"padding-top: 1rem;\">\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Goal</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.profile?.subCategory}}</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Age</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.demographic?.age.avg_age}}</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Gender</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.demographic?.gender['gender']}}</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Curr. Weight</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.demographic?.currentWeight}} Kg.</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Height</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.demographic?.heightInCM.toFixed(0)}} Cm.</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">BMI</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.demographic?.bmi}}</span>\n    </ion-col>\n  </ion-row>\n</ion-card-content>\n</ion-card>\n\n<ion-card style=\"padding: .5rem;\">\n  <ion-card-header (click)=\"isOpen2click()\" style=\"padding: 0;\">\n    <ion-row>\n      <ion-col size=\"11\" class=\"ion-text-left\">\n        <span style=\"color:#565656; \">\n          PROFILE\n        </span>\n      </ion-col>\n      <ion-col size=\"1\">\n    <ion-icon name=\"chevron-down-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"isOpen2\"></ion-icon>\n    <ion-icon name=\"chevron-up-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"!isOpen2\"></ion-icon>\n    </ion-col>\n</ion-row>  \n</ion-card-header>\n\n  <ion-card-content *ngIf=\"isOpen2\" style=\"padding-top: 1rem;\">\n  <ion-row style=\"padding-bottom: 1rem;\" *ngIf=\"activityName?.length>0\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Activity Level</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{activityName?.length>0?activityName[0].value:''}}</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Community</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span> {{searchDetails?.profile?.country}},  ({{community?.join(', ')}})</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\" *ngIf=\"diseases?.length>0\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Health Conditions</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{diseases.join(', ')}}</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\" *ngIf=\"mealPref?.length>0\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Meal Preferences</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{mealPref?.length>0? mealPref[0].value:''}}</span>\n    </ion-col>\n  </ion-row>\n  </ion-card-content>\n</ion-card>\n<ion-card style=\"padding: .5rem;background:#FBEEFC;\">\n  <ion-card-header (click)=\"isOpen3click()\" style=\"padding: 0;\">\n    <ion-row>\n      <ion-col size=\"11\" class=\"ion-text-left\">\n        <span style=\"color:#565656; \">\n          TARGET\n        </span>\n      </ion-col>\n      <ion-col size=\"1\">\n    \n    <ion-icon name=\"chevron-down-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"isOpen3\"></ion-icon>\n    <ion-icon name=\"chevron-up-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"!isOpen3\"></ion-icon>\n    </ion-col>\n    </ion-row>\n  </ion-card-header>\n  <ion-card-content *ngIf=\"isOpen3\" style=\"padding-top: 1rem;\">\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Target Weight</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.demographic?.suggestedWeight}} Kg.</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Target Calories</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left ion-align-self-start\">\n      <ion-row>\n        <ion-col size=\"7\">\n          <ion-input  [(ngModel)]=\"updateTargetCal\" style=\"width: 100%;\n          border: 1px solid #b3b3b3;\n          display: inline-block;\n          height: 28px;\n          border-radius: 5px;\n          --padding-start: .5rem;\"></ion-input>\n        </ion-col>\n        <ion-col size=\"5\">\n          <ion-button style=\"width: 70px;\n          height: 26px;\" (click)=\"getTokenUpdateTargetCal()\" expand=\"block\" \n          style=\"--border-color: #f3f3f3;\n          --background: #b3b3b3;\n          color:#fff;\n          height: 30px;font-size: .8rem;\"\n          fill=\"outline\">Update</ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Recommended Calories</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.demographic?.bmi}} Kcal</span>\n    </ion-col>\n  </ion-row>\n  </ion-card-content>\n</ion-card>\n<!-- <ion-row style=\"margin-left:1rem;font-weight: 600;margin-top: 1rem;\">\n  <ion-col class=\"ion-text-left\">\n    <span style=\"color:#838383\">\n      CURRENT  INTAKE\n    </span>\n  </ion-col>\n</ion-row> -->\n<ion-card style=\"padding: .5rem;background: #E2F0F7;margin-top: .5rem;\">\n  <ion-card-header (click)=\"isOpen4click()\" style=\"padding: 0;\">\n    <ion-row>\n      <ion-col size=\"11\" class=\"ion-align-self-start\"><span style=\"color:#838383\">\n        <span style=\"color:#565656\">\n          CURRENT  INTAKE\n        </span>\n      </span></ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"chevron-down-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"isOpen4\"></ion-icon>\n    <ion-icon name=\"chevron-up-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"!isOpen4\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    \n  </ion-card-header>\n  <ion-card-content *ngIf=\"isOpen4\" style=\"padding-top: 1rem;\">\n  <ion-row style=\"padding-bottom: 1rem;\">\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Calories</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.lifeStyle?.actual_calories==undefined?'-':searchDetails?.lifeStyle?.actual_calories}}</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\" >\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Carbs</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.lifeStyle?.actual_carbs==undefined?'-':searchDetails?.lifeStyle?.actual_carbs}}</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\" >\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Fat</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.lifeStyle?.actual_fats==undefined?'-':searchDetails?.lifeStyle?.actual_fats}}</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\" >\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Fiber</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.lifeStyle?.actual_fiber==undefined?'-':searchDetails?.lifeStyle?.actual_fiber}}</span>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\" >\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Protein</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{searchDetails?.lifeStyle?.actual_protein==undefined?'-':searchDetails?.lifeStyle?.actual_protein}}</span>\n    </ion-col>\n  </ion-row>\n  </ion-card-content>\n</ion-card>\n<!-- <ion-row style=\"margin-left:1rem;font-weight: 600;margin-top: 1rem;\">\n  <ion-col class=\"ion-text-left\">\n    <span style=\"color:#838383\">\n      SPECIAL PREFERENCES\n    </span>\n  </ion-col>\n</ion-row> -->\n<ion-card style=\"padding: .5rem;background: #E4F3EC;margin-top: .5rem;\">\n  <ion-card-header (click)=\"isOpen5click()\" style=\"padding: 0;\">\n    <ion-row>\n      <ion-col size=\"11\" class=\"ion-align-self-start\"><span style=\"color:#565656\">\n        SPECIAL PREFERENCES\n      </span></ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"chevron-down-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"isOpen5\"></ion-icon>\n    <ion-icon name=\"chevron-up-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"!isOpen5\"></ion-icon>\n      </ion-col>\n    </ion-row>\n  </ion-card-header>\n  <ion-card-content *ngIf=\"isOpen5\" style=\"padding-top: 1rem;\">\n  <ion-row style=\"padding-bottom: 1rem;\" >\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Diet Plan</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <span>{{ planChoosen}}</span>\n    </ion-col>\n  </ion-row>\n    <ion-row style=\"padding-bottom: 1rem;\" >\n      <ion-col size=\"5\" class=\"ion-text-left\">\n        <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Cooking Expertise</span>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n      <ion-col size=\"6\" class=\"ion-text-left\">\n        <span>{{\n          searchDetails?.additionalPref?.cookingProficiency===3?'Low - self but amateur':\n          searchDetails?.additionalPref?.cookingProficiency===6?'Medium - self but amateur': 'High - amateur' \n        }}</span>\n      </ion-col>\n    </ion-row>\n    <ion-row style=\"padding-bottom: 1rem;\" >\n      <ion-col size=\"5\" class=\"ion-text-left\">\n        <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Routine</span>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n      <ion-col size=\"6\" class=\"ion-text-left\">\n        <span>\n          {{\n            searchDetails?.additionalPref?.cookingTimeAvailability\n            ===3?'Idle - e.g housewife':\n            searchDetails?.additionalPref?.cookingTimeAvailability\n            ===6?'Medium - e.g housewife': 'High - e.g housewife' \n          }}\n        </span>\n      </ion-col>\n    </ion-row>\n    <ion-row style=\"padding-bottom: 1rem;\" >\n      <ion-col size=\"5\" class=\"ion-text-left\">\n        <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Current Fruits</span>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n      <ion-col size=\"6\" class=\"ion-text-left\">\n        <span>{{searchDetails?.additionalPref?.fruitsLiking===true?'High':'Low'}}</span>\n      </ion-col>\n    </ion-row>\n    <ion-row style=\"padding-bottom: 1rem;\" >\n      <ion-col size=\"5\" class=\"ion-text-left\">\n        <span style=\"font-size: 1rem;font-weight: 600;color:#000\">Craving for milk</span>\n      </ion-col>\n      <ion-col size=\"1\"></ion-col>\n      <ion-col size=\"6\" class=\"ion-text-left\">\n        <span>{{searchDetails?.additionalPref?.teaLiking===true?'High':'Low'}}</span>\n      </ion-col>\n    </ion-row>\n    </ion-card-content>\n</ion-card>\n<!-- <ion-row style=\"margin-left:1rem;font-weight: 600;margin-top: 1rem;\">\n  <ion-col class=\"ion-text-left\">\n    <span style=\"color:#838383\">\n      COPY WEEKLY DIET PLAN\n    </span>\n  </ion-col>\n</ion-row> -->\n<ion-card style=\"padding:.5rem;background: #F2E5D8;margin-top: .5rem;\">\n  <ion-card-header (click)=\"isOpen6click()\" style=\"padding: 0;\">\n    <ion-row>\n      <ion-col size=\"11\" class=\"ion-align-self-start\"><span style=\"color:#565656\">\n        COPY WEEKLY DIET PLAN\n      </span></ion-col>\n      <ion-col size=\"1\">\n        <ion-icon name=\"chevron-down-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"isOpen6\"></ion-icon>\n    <ion-icon name=\"chevron-up-outline\" style=\"color: #0B713F;\n    position: absolute;\n    right: 0;\n    font-size: 2rem;\n    top: -5px;\" *ngIf=\"!isOpen6\"></ion-icon>\n      </ion-col>\n    </ion-row>\n    \n  </ion-card-header>\n  <ion-card-content *ngIf=\"isOpen6\" style=\"padding-top: 1rem;\">\n  <ion-row style=\"padding-bottom: 1rem;\" >\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">From Date</span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <ion-input  id=\"open-modal\"  [value]=\"dateValue\" placeholder=\"Select Date\" \n            style=\"border:1px solid #f3f3f3; border-radius: 5px;padding:10px;\"></ion-input>\n            <ion-modal trigger=\"open-modal\">\n              <ng-template>\n                <ion-content style=\"position: absolute;\n                left:0%;\n                right:0%;\n                width: 100%;\n                top: 0; --background:#fff\">\n                  <ion-datetime style=\"width: auto;\n                  margin: 0 auto;\" showDefaultButtons=\"true\" [min]=\"current_year\" [max]=\"current_year\" displayFormat=\"DD-MMM-YYYY\" \n                  pickerFormat=\"DD MMM YYYY\" #dateTime (ionChange)=\"dateValue = formatDate(dateTime.value)\" \n                  presentation=\"date\" color=\"theme\"></ion-datetime>\n                </ion-content>\n              </ng-template>\n            </ion-modal>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"padding-bottom: 1rem;\" >\n    <ion-col size=\"5\" class=\"ion-text-left\">\n      <span style=\"font-size: 1rem;font-weight: 600;color:#000\">To Date </span>\n    </ion-col>\n    <ion-col size=\"1\"></ion-col>\n    <ion-col size=\"6\" class=\"ion-text-left\">\n      <ion-input  id=\"open-modal1\"  [value]=\"dateValue2\" placeholder=\"Select Date\" \n      style=\"border:1px solid #f3f3f3; border-radius: 5px;padding:10px;\"></ion-input>\n      <ion-modal trigger=\"open-modal1\">\n        <ng-template>\n          <ion-content style=\"position: absolute;\n          left:0%;\n          right:0%;\n          width: 100%;\n          top: 0; --background:#fff\">\n            <ion-datetime style=\"width: auto;\n            margin: 0 auto;\" showDefaultButtons=\"true\" [min]=\"current_year\" [max]=\"current_year\" displayFormat=\"DD-MMM-YYYY\" \n            pickerFormat=\"DD MMM YYYY\" #dateTime2 (ionChange)=\"dateValue2 = formatDate(dateTime2.value)\" \n            presentation=\"date\" color=\"theme\"></ion-datetime> \n          </ion-content>\n        </ng-template>\n      </ion-modal>\n    </ion-col>\n  </ion-row>\n  <ion-row style=\"margin-bottom:.5rem\">\n    <ion-col class=\"ion-text-center\">\n      <ion-button (click)=\"submitCopyDietPlan()\" expand=\"block\" \n      style=\"--border-color: #f3f3f3;\n      color:#fff;\n      --background: #b3b3b3;\n      height: 36px;\"\n      fill=\"outline\">Copy Plan</ion-button>\n    </ion-col>\n  </ion-row>\n</ion-card-content>\n</ion-card>\n    <ion-card class=\"card\" style=\"background-color: #fff; margin:1.2rem;border-radius: 15px;padding:1.5rem\">\n      <ion-row style=\"margin-bottom:.5rem\">\n        <ion-col class=\"ion-text-center\">\n          <ion-button (click)=\"updateFoodChoices()\" expand=\"block\" \n          style=\"color:#0B713F;--border-color:#0B713F;\"\n          fill=\"outline\">Update Food choices</ion-button>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row style=\"margin-bottom:.5rem\">\n        <ion-col class=\"ion-text-center\">\n          <ion-button (click)=\"gotoPreferences()\" expand=\"block\" \n          style=\"color:#0B713F;--border-color:#0B713F;\"\n          fill=\"outline\">Update Special preferences</ion-button>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row style=\"margin-bottom:.5rem\">\n        <ion-col class=\"ion-text-center\">\n          <ion-button (click)=\"gotoDietPlan()\"  expand=\"block\" \n          style=\"color:#0B713F;--border-color:#0B713F;\"\n          fill=\"outline\">Customise  Diet plan</ion-button>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row style=\"margin-bottom:.5rem\" *ngIf=\"username!=='beato'\">\n        <ion-col class=\"ion-text-center\">\n          <ion-button (click)=\"getToken()\" expand=\"block\" \n          style=\"color:#0B713F;--border-color:#0B713F;\"\n          fill=\"outline\">View User in the SDP App</ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row style=\"margin-bottom:.5rem\" >\n        <ion-col class=\"ion-text-center\">\n          <ion-button (click)=\"gotoupdateRemarks()\" expand=\"block\" \n          style=\"color:#0B713F;--border-color:#0B713F;\"\n          fill=\"outline\">Update Slot Remarks</ion-button>\n        </ion-col>\n      </ion-row>\n      <ion-row style=\"margin-bottom:.5rem\" *ngIf=\"username==='beato'\">\n        <ion-col class=\"ion-text-center\">\n          <ion-button (click)=\"gotoBeato()\" expand=\"block\" \n          style=\"--background:#0B713F;\" \n          >Goto Beato</ion-button>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row style=\"margin-bottom:.5rem\" *ngIf=\"username!=='beato'\">\n        <ion-col class=\"ion-text-center\">\n          <ion-button (click)=\"gotoRecall()\" expand=\"block\" \n          style=\"color:#0B713F;\"  fill=\"outline\"\n          >Diet Recall</ion-button>\n        </ion-col>\n      </ion-row>\n    </ion-card>\n\n  </div>\n\n\n</ion-content>\n");

/***/ }),

/***/ 37304:
/*!*************************************************************!*\
  !*** ./src/app/deitician-search/deitician-search.page.scss ***!
  \*************************************************************/
/***/ ((module) => {

module.exports = "@font-face {\n  font-family: gt-america-standard-medium;\n  src: url(\"/assets/fonts/Roboto-Medium.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-regular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-bold;\n  src: url(\"/assets/fonts/Roboto-Bold.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-extendedMedium;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-light;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: Source-Sans-Pro-ragular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n.searchbar-search-icon {\n  display: none;\n  width: 0;\n  height: 0;\n}\nion-content {\n  --background: #F6F7FC;\n}\nion-content .modal-default {\n  width: 80%;\n  height: 100%;\n}\nion-toggle {\n  padding: 1rem 2rem 0.5rem 0;\n  --track-background: #ddd;\n  --track-background-checked: #ddd;\n  --handle-background: #eb7769;\n  --handle-background-checked: #95c34e;\n  --handle-width: 25px;\n  --handle-height: 27px;\n  --handle-max-height: auto;\n  --handle-spacing: 6px;\n  --handle-border-radius: 4px;\n  --handle-box-shadow: none;\n}\nion-toggle::part(track) {\n  height: 10px;\n  width: 65px;\n  /* Required for iOS handle to overflow the height of the track */\n  overflow: visible;\n}\n.header-sec {\n  margin: 1rem 1rem;\n  line-height: 1.5;\n}\n.profile-detail {\n  font-size: 0.9rem;\n  margin: 3rem 2rem;\n}\n.profile-detail .item-value {\n  font-family: \"gt-america-standard-medium\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #171717;\n  white-space: nowrap;\n}\n.profile-detail .item-value1 {\n  font-family: \"gt-america-standard-medium\", \"Helvetica Neue\", Arial, sans-serif;\n  color: #171717;\n}\n.search-sec {\n  margin: 1rem 1rem;\n}\n.search-sec ion-button {\n  width: 86%;\n  height: 35px;\n  position: relative;\n  top: 10px;\n  margin-left: 0.2rem;\n}\n.search-sec .label {\n  color: #01A3A4;\n  font-size: 1.2rem;\n}\n.search-sec ion-input {\n  --background: #fff;\n  --padding-start: .5rem;\n  margin-top: 0.5rem;\n  border-radius: 10px;\n}\n.search-detail .card {\n  background: #fff;\n  border-radius: 10px;\n  box-shadow: none;\n  padding-top: 1rem;\n  padding-bottom: 1rem;\n  padding-left: 1rem;\n  padding-right: 1rem;\n  line-height: 1.8;\n}\n.search-detail .card .label {\n  color: #01A3A4;\n  font-size: 0.752rem;\n  font-family: \"gt-america-standard-medium\", \"Helvetica Neue\", Arial, sans-serif;\n}\n.search-detail .card .desc {\n  color: #8C8E93;\n  font-size: 0.752rem;\n}\n.search-detail .card ion-button {\n  width: 100%;\n  height: 40px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL2Fzc2V0cy9mb250cy9mb250cy5zY3NzIiwiZGVpdGljaWFuLXNlYXJjaC5wYWdlLnNjc3MiLCIuLi8uLi90aGVtZS9jdXN0b20tdGhlbWVzL3ZhcmlhYmxlcy5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsdUNBQUE7RUFDQSw4REFBQTtBQ0NGO0FERUE7RUFDRSx3Q0FBQTtFQUNBLCtEQUFBO0FDQUY7QURHQTtFQUNFLHFDQUFBO0VBQ0EsNERBQUE7QUNERjtBRElBO0VBQ0UsK0NBQUE7RUFDQSw2REFBQTtBQ0ZGO0FES0E7RUFDRSxzQ0FBQTtFQUNBLDZEQUFBO0FDSEY7QURPQTtFQUNFLG9DQUFBO0VBQ0EsK0RBQUE7QUNMRjtBQXBCQTtFQUF5QixhQUFBO0VBQWMsUUFBQTtFQUFTLFNBQUE7QUF5QmhEO0FBdkJBO0VBQ0kscUJBQUE7QUEwQko7QUF4Qkk7RUFDSSxVQUFBO0VBQ0EsWUFBQTtBQTBCUjtBQXZCQTtFQUNJLDJCQUFBO0VBRUEsd0JBQUE7RUFDQSxnQ0FBQTtFQUVBLDRCQUFBO0VBQ0Esb0NBQUE7RUFFQSxvQkFBQTtFQUNBLHFCQUFBO0VBQ0EseUJBQUE7RUFDQSxxQkFBQTtFQUVBLDJCQUFBO0VBQ0EseUJBQUE7QUFzQko7QUFuQkU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUVBLGdFQUFBO0VBQ0EsaUJBQUE7QUFxQko7QUFuQkE7RUFDSSxpQkFBQTtFQUNBLGdCQUFBO0FBc0JKO0FBcEJBO0VBQ0ksaUJBQUE7RUFDQSxpQkFBQTtBQXVCSjtBQXRCSTtFQUNJLDhFQ3RDTTtFRHVDTixjQUFBO0VBQ0QsbUJBQUE7QUF3QlA7QUF0Qkk7RUFDSSw4RUMzQ007RUQ0Q04sY0FBQTtBQXdCUjtBQXBCQTtFQUNJLGlCQUFBO0FBdUJKO0FBdEJJO0VBQ0ksVUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxtQkFBQTtBQXdCUjtBQXRCSTtFQUNJLGNBQUE7RUFDQSxpQkFBQTtBQXdCUjtBQXRCSTtFQUNJLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0FBd0JSO0FBcEJJO0VBQ0ksZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtBQXVCUjtBQXRCUTtFQUNJLGNBQUE7RUFDQSxtQkFBQTtFQUNBLDhFQ2pGRTtBRHlHZDtBQXRCUTtFQUNJLGNBQUE7RUFDQSxtQkFBQTtBQXdCWjtBQXJCUTtFQUNJLFdBQUE7RUFDQSxZQUFBO0FBdUJaIiwiZmlsZSI6ImRlaXRpY2lhbi1zZWFyY2gucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLW1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1NZWRpdW0udHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLWJvbGQ7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tQm9sZC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1leHRlbmRlZE1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1saWdodDtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBTb3VyY2UtU2Fucy1Qcm8tcmFndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cbiIsIlxuQGltcG9ydCAnLi4vLi4vdGhlbWUvZm9udHMuc2Nzcyc7XG5cbi5zZWFyY2hiYXItc2VhcmNoLWljb24geyBkaXNwbGF5Om5vbmU7IHdpZHRoOjA7IGhlaWdodDowOyB9XG4gIFxuaW9uLWNvbnRlbnR7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRjZGN0ZDO1xuXG4gICAgLm1vZGFsLWRlZmF1bHR7XG4gICAgICAgIHdpZHRoOiA4MCU7XG4gICAgICAgIGhlaWdodDogMTAwJTtcbiAgICB9XG59XG5pb24tdG9nZ2xlIHtcbiAgICBwYWRkaW5nOiAxcmVtIDJyZW0gLjVyZW0gMDtcbiAgXG4gICAgLS10cmFjay1iYWNrZ3JvdW5kOiAjZGRkO1xuICAgIC0tdHJhY2stYmFja2dyb3VuZC1jaGVja2VkOiAjZGRkO1xuICBcbiAgICAtLWhhbmRsZS1iYWNrZ3JvdW5kOiAjZWI3NzY5O1xuICAgIC0taGFuZGxlLWJhY2tncm91bmQtY2hlY2tlZDogIzk1YzM0ZTtcbiAgXG4gICAgLS1oYW5kbGUtd2lkdGg6IDI1cHg7XG4gICAgLS1oYW5kbGUtaGVpZ2h0OiAyN3B4O1xuICAgIC0taGFuZGxlLW1heC1oZWlnaHQ6IGF1dG87XG4gICAgLS1oYW5kbGUtc3BhY2luZzogNnB4O1xuICBcbiAgICAtLWhhbmRsZS1ib3JkZXItcmFkaXVzOiA0cHg7XG4gICAgLS1oYW5kbGUtYm94LXNoYWRvdzogbm9uZTtcbiAgfVxuICBcbiAgaW9uLXRvZ2dsZTo6cGFydCh0cmFjaykge1xuICAgIGhlaWdodDogMTBweDtcbiAgICB3aWR0aDogNjVweDtcbiAgXG4gICAgLyogUmVxdWlyZWQgZm9yIGlPUyBoYW5kbGUgdG8gb3ZlcmZsb3cgdGhlIGhlaWdodCBvZiB0aGUgdHJhY2sgKi9cbiAgICBvdmVyZmxvdzogdmlzaWJsZTtcbiAgfVxuLmhlYWRlci1zZWN7XG4gICAgbWFyZ2luOjFyZW0gMXJlbTtcbiAgICBsaW5lLWhlaWdodDogMS41O1xufVxuLnByb2ZpbGUtZGV0YWlse1xuICAgIGZvbnQtc2l6ZTogLjlyZW07XG4gICAgbWFyZ2luOiAzcmVtIDJyZW07XG4gICAgLml0ZW0tdmFsdWV7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAkZm9udC1tZWRpdW07XG4gICAgICAgIGNvbG9yOiAjMTcxNzE3O1xuICAgICAgIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gICAgfVxuICAgIC5pdGVtLXZhbHVlMXtcbiAgICAgICAgZm9udC1mYW1pbHk6ICRmb250LW1lZGl1bTtcbiAgICAgICAgY29sb3I6ICMxNzE3MTc7XG4gICAgICAgXG4gICAgfVxufVxuLnNlYXJjaC1zZWN7XG4gICAgbWFyZ2luOjFyZW0gMXJlbTtcbiAgICBpb24tYnV0dG9ue1xuICAgICAgICB3aWR0aDogODYlO1xuICAgICAgICBoZWlnaHQ6IDM1cHg7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgdG9wOjEwcHg7XG4gICAgICAgIG1hcmdpbi1sZWZ0Oi4ycmVtO1xuICAgIH1cbiAgICAubGFiZWx7XG4gICAgICAgIGNvbG9yOiAjMDFBM0E0O1xuICAgICAgICBmb250LXNpemU6IDEuMnJlbTtcbiAgICB9XG4gICAgaW9uLWlucHV0e1xuICAgICAgICAtLWJhY2tncm91bmQ6ICNmZmY7XG4gICAgICAgIC0tcGFkZGluZy1zdGFydDogLjVyZW07XG4gICAgICAgIG1hcmdpbi10b3A6IC41cmVtO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIH1cbn1cbi5zZWFyY2gtZGV0YWlse1xuICAgIC5jYXJke1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAgICAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgICAgICBib3gtc2hhZG93OiBub25lO1xuICAgICAgICBwYWRkaW5nLXRvcDoxcmVtO1xuICAgICAgICBwYWRkaW5nLWJvdHRvbToxcmVtO1xuICAgICAgICBwYWRkaW5nLWxlZnQ6IDFyZW07XG4gICAgICAgIHBhZGRpbmctcmlnaHQ6IDFyZW07XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjg7XG4gICAgICAgIC5sYWJlbHtcbiAgICAgICAgICAgIGNvbG9yOiAjMDFBM0E0O1xuICAgICAgICAgICAgZm9udC1zaXplOiAuNzUycmVtO1xuICAgICAgICAgICAgZm9udC1mYW1pbHk6ICRmb250LW1lZGl1bTtcbiAgICAgICAgfVxuICAgICAgICAuZGVzY3tcbiAgICAgICAgICAgIGNvbG9yOiAjOEM4RTkzO1xuICAgICAgICAgICAgZm9udC1zaXplOiAuNzUycmVtO1xuICAgICAgICB9XG5cbiAgICAgICAgaW9uLWJ1dHRvbntcbiAgICAgICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICAgICAgaGVpZ2h0OiA0MHB4O1xuICAgICAgICB9XG4gICAgfVxufSIsIkBpbXBvcnQgXCIuLi8uLi9hc3NldHMvZm9udHMvZm9udHMuc2Nzc1wiO1xuXG4vLyBGb250c1xuJGZvbnQtc2l6ZTogMTZweDtcblxuJGZvbnQ6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1yZWd1bGFyXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4kZm9udC1ib2xkOiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtYm9sZFwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtbGlnaHQ6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1saWdodFwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmO1xuJGZvbnQtbWVkaXVtOiBcImd0LWFtZXJpY2Etc3RhbmRhcmQtbWVkaXVtXCIsIFwiSGVsdmV0aWNhIE5ldWVcIiwgQXJpYWwsIHNhbnMtc2VyaWY7XG4kZm9udC1tZWRpdW0tZXh0ZW5kZWQ6IFwiZ3QtYW1lcmljYS1zdGFuZGFyZC1leHRlbmRlZE1lZGl1bVwiLCBcIkhlbHZldGljYSBOZXVlXCIsXG4gIEFyaWFsLCBzYW5zLXNlcmlmO1xuXG4gICRzYW5zLWZvbnQtcmVndWxhcjogXCJTb3VyY2UtU2Fucy1Qcm8tcmFndWxhclwiLCBcIkhlbHZldGljYSBOZXVlXCIsIEFyaWFsLCBzYW5zLXNlcmlmOyJdfQ== */";

/***/ })

}]);
//# sourceMappingURL=src_app_deitician-search_deitician-search_module_ts.js.map