"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_diet-plan_update-slot-new_update-slot-new_module_ts"],{

/***/ 41150:
/*!*********************************************************************!*\
  !*** ./src/app/diet-plan/update-slot-new/update-slot-new.module.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateSlotNewPageModule": () => (/* binding */ UpdateSlotNewPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _update_slot_new_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-slot-new-routing.module */ 96490);
/* harmony import */ var _update_slot_new_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-slot-new.page */ 69709);







let UpdateSlotNewPageModule = class UpdateSlotNewPageModule {
};
UpdateSlotNewPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _update_slot_new_routing_module__WEBPACK_IMPORTED_MODULE_0__.UpdateSlotNewPageRoutingModule
        ],
        declarations: [_update_slot_new_page__WEBPACK_IMPORTED_MODULE_1__.UpdateSlotNewPage]
    })
], UpdateSlotNewPageModule);



/***/ })

}]);
//# sourceMappingURL=src_app_diet-plan_update-slot-new_update-slot-new_module_ts.js.map