"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_update-slot-remarks_update-slot-remarks_module_ts"],{

/***/ 23035:
/*!***************************************************************************!*\
  !*** ./src/app/update-slot-remarks/update-slot-remarks-routing.module.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateSlotRemarksPageRoutingModule": () => (/* binding */ UpdateSlotRemarksPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _update_slot_remarks_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-slot-remarks.page */ 63455);




const routes = [
    {
        path: '',
        component: _update_slot_remarks_page__WEBPACK_IMPORTED_MODULE_0__.UpdateSlotRemarksPage
    }
];
let UpdateSlotRemarksPageRoutingModule = class UpdateSlotRemarksPageRoutingModule {
};
UpdateSlotRemarksPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], UpdateSlotRemarksPageRoutingModule);



/***/ }),

/***/ 4938:
/*!*******************************************************************!*\
  !*** ./src/app/update-slot-remarks/update-slot-remarks.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateSlotRemarksPageModule": () => (/* binding */ UpdateSlotRemarksPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _update_slot_remarks_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./update-slot-remarks-routing.module */ 23035);
/* harmony import */ var _update_slot_remarks_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-slot-remarks.page */ 63455);







let UpdateSlotRemarksPageModule = class UpdateSlotRemarksPageModule {
};
UpdateSlotRemarksPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _update_slot_remarks_routing_module__WEBPACK_IMPORTED_MODULE_0__.UpdateSlotRemarksPageRoutingModule
        ],
        declarations: [_update_slot_remarks_page__WEBPACK_IMPORTED_MODULE_1__.UpdateSlotRemarksPage]
    })
], UpdateSlotRemarksPageModule);



/***/ }),

/***/ 63455:
/*!*****************************************************************!*\
  !*** ./src/app/update-slot-remarks/update-slot-remarks.page.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UpdateSlotRemarksPage": () => (/* binding */ UpdateSlotRemarksPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_update_slot_remarks_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./update-slot-remarks.page.html */ 17893);
/* harmony import */ var _update_slot_remarks_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./update-slot-remarks.page.scss */ 47428);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ 38198);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _services_app_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/app/app.service */ 61535);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../core/utility/utilities */ 53533);








let UpdateSlotRemarksPage = class UpdateSlotRemarksPage {
    constructor(router, utilities, appService, queryParams, appServ) {
        this.router = router;
        this.utilities = utilities;
        this.appService = appService;
        this.queryParams = queryParams;
        this.appServ = appServ;
        this.slot0 = "";
        this.slot1 = "";
        this.slot2 = "";
        this.slot3 = "";
        this.slot4 = "";
        this.slot5 = "";
        this.slot6 = "";
        this.slot7 = "";
        this.slot8 = "";
        this.queryParams.queryParams.subscribe(res => {
            this.search = res["params"];
        });
    }
    goback() {
        this.router.navigate(["/deitician-search"]);
    }
    ngOnInit() {
        this.appServ.getUserToken(this.search).subscribe((response) => {
            console.log(response);
        }, (error) => {
            console.log("User token ", error.error.text);
            this.appService.getSlotRemarks({}, this.search, error.error.text).then(res => {
                var _a, _b, _c, _d, _e, _f, _g, _h, _j;
                this.slot0 = (_a = res["remarks"][0]) === null || _a === void 0 ? void 0 : _a.remark;
                this.slot1 = (_b = res["remarks"][1]) === null || _b === void 0 ? void 0 : _b.remark;
                this.slot2 = (_c = res["remarks"][2]) === null || _c === void 0 ? void 0 : _c.remark;
                this.slot3 = (_d = res["remarks"][3]) === null || _d === void 0 ? void 0 : _d.remark;
                this.slot4 = (_e = res["remarks"][4]) === null || _e === void 0 ? void 0 : _e.remark;
                this.slot5 = (_f = res["remarks"][5]) === null || _f === void 0 ? void 0 : _f.remark;
                this.slot6 = (_g = res["remarks"][6]) === null || _g === void 0 ? void 0 : _g.remark;
                this.slot7 = (_h = res["remarks"][7]) === null || _h === void 0 ? void 0 : _h.remark;
                this.slot8 = (_j = res["remarks"][8]) === null || _j === void 0 ? void 0 : _j.remark;
            });
            console.log("Category Error", error);
        });
    }
    getToken(payload, search) {
        this.appServ.getUserToken(search).subscribe((response) => {
            console.log(response);
        }, (error) => {
            console.log("User token ", error.error.text);
            this.appService.updateSlotRemarks(payload, search, error.error.text);
            this.utilities.presentAlert("Remarks saved successfully!");
            console.log("Category Error", error);
        });
    }
    updateRemarks() {
        const payload = {
            "remarks": [
                { "slot": "0", "remark": this.slot0 },
                { "slot": "1", "remark": this.slot1 },
                { "slot": "2", "remark": this.slot2 },
                { "slot": "3", "remark": this.slot3 },
                { "slot": "4", "remark": this.slot4 },
                { "slot": "5", "remark": this.slot5 },
                { "slot": "6", "remark": this.slot6 },
                { "slot": "7", "remark": this.slot7 },
                { "slot": "8", "remark": this.slot8 }
            ]
        };
        console.log("payload", payload, this.search);
        this.getToken(payload, this.search);
    }
};
UpdateSlotRemarksPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_4__.UTILITIES },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.ActivatedRoute },
    { type: _services_app_app_service__WEBPACK_IMPORTED_MODULE_3__.AppService }
];
UpdateSlotRemarksPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-update-slot-remarks',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_update_slot_remarks_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_update_slot_remarks_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], UpdateSlotRemarksPage);



/***/ }),

/***/ 17893:
/*!**********************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/update-slot-remarks/update-slot-remarks.page.html ***!
  \**********************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content>\n  <div style=\"background:#0B7141;    border-bottom-left-radius: 1rem;\n  border-bottom-right-radius: 1rem;color:#fff; height: 15%;\n  padding-top: 10%; padding-left:2rem;\">\n  <ion-row>\n    <ion-col size=\"1\" class=\"ion-text-left ion-align-self-center\">\n        <ion-icon name=\"arrow-back-outline\" (click)=\"goback()\" style=\"font-size: 2rem;\"></ion-icon>\n    </ion-col>\n    <ion-col size=\"2\"></ion-col>\n    <ion-col size=\"9\" class=\"ion-text-left ion-align-self-center\">\n      Update Slot Remarks\n    </ion-col>\n  </ion-row>\n  </div>\n  <ion-card>\n    <div>\n    <ion-card-header style=\"background: #E4F3EC;margin-bottom: .5rem;\">\n      When You Wake up\n    </ion-card-header>\n    <ion-card-content>\n     <ion-textarea placeholder=\"Remarks for When You Wake up\" [(ngModel)]=\"slot0\" style=\"border: 1px solid #bebebe;border-radius: 10px; height: 150px;width: 100%;padding: 10px;\" rows=\"5\"></ion-textarea>\n    </ion-card-content>\n  </div>\n  </ion-card>\n  <ion-card>\n  <div>\n   <ion-card-header style=\"background: #F2E5D8;margin-bottom: .5rem;\">\n      Before Breakfast\n    </ion-card-header>\n    <ion-card-content>\n     <ion-textarea placeholder=\"Remarks for Before Breakfast\" [(ngModel)]=\"slot1\" style=\"border: 1px solid #bebebe;border-radius: 10px; height: 150px;width: 100%;padding: 10px;\" rows=\"5\"></ion-textarea>\n    </ion-card-content>\n  </div>\n  </ion-card>\n  <ion-card>\n    <div>\n    <ion-card-header style=\"background: #FBEEFC;margin-bottom: .5rem;\">\n        Breakfast\n      </ion-card-header>\n      <ion-card-content>\n      <ion-textarea placeholder=\"Remarks for Breakfast\" [(ngModel)]=\"slot2\" style=\"border: 1px solid #bebebe;border-radius: 10px; height: 150px;width: 100%;padding: 10px;\" rows=\"5\"></ion-textarea>\n      </ion-card-content>\n    </div>\n  </ion-card>\n  <ion-card>\n  <div>\n   <ion-card-header style=\"background: #E2F0F7;margin-bottom: .5rem;\">\n      Mid Day Meal\n    </ion-card-header>\n    <ion-card-content>\n     <ion-textarea placeholder=\"Remarks for Mid Day Meal\"  [(ngModel)]=\"slot3\" style=\"border: 1px solid #bebebe;border-radius: 10px; height: 150px;width: 100%;padding: 10px;\" rows=\"5\"></ion-textarea>\n    </ion-card-content>\n  </div>\n  </ion-card>\n  <ion-card>\n  <div>\n   <ion-card-header style=\"background: #E4F3EC;margin-bottom: .5rem;\">\n      Lunch\n    </ion-card-header>\n    <ion-card-content>\n     <ion-textarea placeholder=\"Remarks for Lunch\" [(ngModel)]=\"slot4\" style=\"border: 1px solid #bebebe;border-radius: 10px; height: 150px;width: 100%;padding: 10px;\" rows=\"5\"></ion-textarea>\n    </ion-card-content>\n  </div>\n  </ion-card>\n  <ion-card>\n  <div>\n   <ion-card-header style=\"background: #F2E5D8;margin-bottom: .5rem;\">\n      Post Lunch\n    </ion-card-header>\n    <ion-card-content>\n     <ion-textarea placeholder=\"Remarks for Post Lunch\" [(ngModel)]=\"slot5\" style=\"border: 1px solid #bebebe;border-radius: 10px; height: 150px;width: 100%;padding: 10px;\" rows=\"5\"></ion-textarea>\n    </ion-card-content>\n  </div>\n  </ion-card>\n  <ion-card>\n  <div>\n   <ion-card-header style=\"background: #FBEEFC;margin-bottom: .5rem;\">\n      Evening Snack\n    </ion-card-header>\n    <ion-card-content>\n     <ion-textarea placeholder=\"Remarks for Evening Snack\"  [(ngModel)]=\"slot6\" style=\"border: 1px solid #bebebe;border-radius: 10px; height: 150px;width: 100%;padding: 10px;\" rows=\"5\"></ion-textarea>\n    </ion-card-content>\n  </div>\n  </ion-card>\n  <ion-card>\n  <div>\n   <ion-card-header style=\"background: #E2F0F7;margin-bottom: .5rem;\">\n      Dinner\n    </ion-card-header>\n    <ion-card-content>\n     <ion-textarea placeholder=\"Remarks for Dinner\" [(ngModel)]=\"slot7\" style=\"border: 1px solid #bebebe;border-radius: 10px; height: 150px;width: 100%;padding: 10px;\" rows=\"5\"></ion-textarea>\n    </ion-card-content>\n  </div>\n  </ion-card>\n  <ion-card>\n  <div>\n   <ion-card-header style=\"background: #E4F3EC;margin-bottom: .5rem;\">\n      Before Sleep\n    </ion-card-header>\n    <ion-card-content>\n     <ion-textarea placeholder=\"Remarks for Before Sleep\" [(ngModel)]=\"slot8\" style=\"border: 1px solid #bebebe;border-radius: 10px; height: 150px;width: 100%;padding: 10px;\" rows=\"5\"></ion-textarea>\n    </ion-card-content>\n  </div>\n  </ion-card>\n  <div>\n    <ion-row style=\"margin-bottom:.5rem\">\n      <ion-col class=\"ion-text-center\">\n        <ion-button (click)=\"updateRemarks()\" expand=\"block\" \n        color=\"theme\"\n        >Update Remarks</ion-button>\n      </ion-col>\n    </ion-row>\n  </div>\n\n</ion-content>\n");

/***/ }),

/***/ 47428:
/*!*******************************************************************!*\
  !*** ./src/app/update-slot-remarks/update-slot-remarks.page.scss ***!
  \*******************************************************************/
/***/ ((module) => {

module.exports = "ion-text-area div textarea {\n  height: 150px;\n}\n\nion-card-header {\n  padding: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVwZGF0ZS1zbG90LXJlbWFya3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVRO0VBQ0EsYUFBQTtBQURSOztBQU1BO0VBQ0ksYUFBQTtBQUhKIiwiZmlsZSI6InVwZGF0ZS1zbG90LXJlbWFya3MucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLXRleHQtYXJlYXtcbiAgICBkaXZ7XG4gICAgICAgIHRleHRhcmVhe1xuICAgICAgICBoZWlnaHQ6IDE1MHB4O1xuICAgIH1cbn1cbn1cblxuaW9uLWNhcmQtaGVhZGVye1xuICAgIHBhZGRpbmc6IDEwcHg7XG59Il19 */";

/***/ })

}]);
//# sourceMappingURL=src_app_update-slot-remarks_update-slot-remarks_module_ts.js.map