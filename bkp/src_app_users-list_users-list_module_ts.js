"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_users-list_users-list_module_ts"],{

/***/ 73371:
/*!*********************************************************!*\
  !*** ./src/app/users-list/users-list-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UsersListPageRoutingModule": () => (/* binding */ UsersListPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _users_list_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./users-list.page */ 68755);




const routes = [
    {
        path: '',
        component: _users_list_page__WEBPACK_IMPORTED_MODULE_0__.UsersListPage
    }
];
let UsersListPageRoutingModule = class UsersListPageRoutingModule {
};
UsersListPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], UsersListPageRoutingModule);



/***/ }),

/***/ 83615:
/*!*************************************************!*\
  !*** ./src/app/users-list/users-list.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UsersListPageModule": () => (/* binding */ UsersListPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _users_list_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./users-list-routing.module */ 73371);
/* harmony import */ var _users_list_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users-list.page */ 68755);







let UsersListPageModule = class UsersListPageModule {
};
UsersListPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _users_list_routing_module__WEBPACK_IMPORTED_MODULE_0__.UsersListPageRoutingModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule
        ],
        declarations: [_users_list_page__WEBPACK_IMPORTED_MODULE_1__.UsersListPage]
    })
], UsersListPageModule);



/***/ }),

/***/ 68755:
/*!***********************************************!*\
  !*** ./src/app/users-list/users-list.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UsersListPage": () => (/* binding */ UsersListPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_users_list_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./users-list.page.html */ 72387);
/* harmony import */ var _users_list_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./users-list.page.scss */ 23983);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _services__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services */ 65190);






let UsersListPage = class UsersListPage {
    constructor(formBuilder, appService) {
        this.formBuilder = formBuilder;
        this.appService = appService;
        this.submitted = true;
        this.userForm = this.formBuilder.group({
            username: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]],
            firstName: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]],
            lastName: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]],
            email: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.email]],
            password: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]],
            phone: [null, [_angular_forms__WEBPACK_IMPORTED_MODULE_3__.Validators.required]]
        });
    }
    get f() { return this.userForm.controls; }
    update() {
        // stop here if form is invalid
        if (this.userForm.invalid) {
            this.submitted = true;
            return;
        }
        this.userForm.value.userStatus = 0;
        this.userForm.value.id = 0;
        console.log("this.userForm.value", this.userForm.value);
        this.appService.userUpdate(this.userForm.value.username, this.userForm.value).subscribe(success => {
            console.log("success", success);
            this.submitted = false;
            this.onReset();
        }, error => {
            console.log("error", error);
        });
    }
    onReset() {
        this.submitted = false;
        this.userForm.reset();
    }
    delete() {
        if (this.userForm.value.username) {
            this.appService.userDelete(this.userForm.value.username).subscribe(success => {
                console.log("success", success);
                this.submitted = false;
                this.onReset();
            }, error => {
                console.log("error", error);
            });
        }
    }
    ngOnInit() {
    }
};
UsersListPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_3__.FormBuilder },
    { type: _services__WEBPACK_IMPORTED_MODULE_2__.AppService }
];
UsersListPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-users-list',
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_users_list_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_users_list_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], UsersListPage);



/***/ }),

/***/ 72387:
/*!****************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/users-list/users-list.page.html ***!
  \****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-title>User Details</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n<ion-card>\n  <ion-card-header>\n    <form [formGroup]=\"userForm\" >\n      <div class=\"form-group\">\n      <ion-list>\n        <ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&  (f.username.errors && f.username.touched) }\">\n          <ion-label position=\"floating\">Username</ion-label>\n          <ion-input formControlName=\"username\" placeholder=\"Username\"></ion-input>\n        </ion-item>\n        <div *ngIf=\"submitted &&  (f.username.errors && f.username.touched)\" class=\"invalid\">\n          <div *ngIf=\"f.username.errors.required\">Username is required</div>\n       </div>\n  \n       <ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&   (f.firstName.errors && f.firstName.touched )}\">\n        <ion-label position=\"floating\">First Name</ion-label>\n        <ion-input formControlName=\"firstName\" placeholder=\"Firstname\"></ion-input>\n      </ion-item>\n      <div *ngIf=\"submitted &&  (f.firstName.errors && f.firstName.touched)\" class=\"invalid\">\n        <div *ngIf=\"f.firstName.errors.required\">Firstname is required</div>\n     </div>\n  \n     <ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&   (f.lastName.errors && f.lastName.touched)}\">\n      <ion-label position=\"floating\">Last Name</ion-label>\n      <ion-input formControlName=\"lastName\" placeholder=\"Lastname\"></ion-input>\n    </ion-item>\n    <div *ngIf=\"submitted &&  (f.lastName.errors && f.lastName.touched)\" class=\"invalid\">\n      <div *ngIf=\"f.lastName.errors.required\">Lastname is required</div>\n   </div>\n  \n   <ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&  (f.email.invalid && f.email.touched) }\">\n    <ion-label position=\"floating\">Email</ion-label>\n    <ion-input type=\"text\" formControlName=\"email\" placeholder=\"email\"></ion-input>\n  </ion-item>\n  <div *ngIf=\"submitted &&  (f.email.invalid && f.email.touched)\" class=\"invalid\">\n    <div *ngIf=\"f.email.errors.required\">email is required</div>\n  </div>\n  \n  <ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&   (f.password.errors && f.password.touched)}\">\n    <ion-label position=\"floating\">Password</ion-label>\n    <ion-input type=\"password\" formControlName=\"password\" placeholder=\"password\"></ion-input>\n  </ion-item>\n  <div *ngIf=\"submitted &&  (f.password.errors && f.password.touched)\" class=\"invalid\">\n    <div *ngIf=\"f.password.errors.required\">password is required</div>\n  </div>\n  <ion-item class=\"form-control\" [ngClass]=\"{ 'is-invalid': submitted &&  (f.phone.errors && f.phone.touched)}\">\n    <ion-label position=\"floating\" >Phone</ion-label>\n    <ion-input type=\"tel\" formControlName=\"phone\" placeholder=\"phone\"></ion-input>\n  </ion-item>\n  <div *ngIf=\"submitted && (f.phone.errors && f.phone.touched)\" class=\"invalid\">\n    <div *ngIf=\"f.phone.errors.required\">phone is required</div>\n  </div>\n  \n    </ion-list>\n  </div>\n    </form>\n   \n  </ion-card-header>\n  <ion-card-content>\n    <ion-row>\n      <ion-col class=\"ion-text-right\" size=\"8\" (click)=\"update()\">Update</ion-col>\n      <ion-col class=\"ion-text-right\" size=\"4\" (click)=\"delete()\">Delete</ion-col>\n    </ion-row>\n</ion-card-content>\n</ion-card>\n</ion-content>\n");

/***/ }),

/***/ 23983:
/*!*************************************************!*\
  !*** ./src/app/users-list/users-list.page.scss ***!
  \*************************************************/
/***/ ((module) => {

module.exports = "ion-toolbar {\n  --background: #F6F7FC;\n  --border-color: #F6F7FC;\n}\n\nion-searchbar {\n  --background: #FFFFFF;\n  --border-radius: 7px;\n}\n\nion-content {\n  --background: #F6F7FC;\n}\n\n.showLeedsMore {\n  height: auto;\n  --background: #FFFFFF;\n  border-radius: 23px;\n  border: var(--ion-color-theme);\n}\n\n.showLess {\n  height: 150px;\n  --background: #FFFFFF;\n  border-radius: 23px;\n  border: var(--ion-color-theme);\n}\n\n.cardLabel {\n  font-size: 14px;\n  color: var(--ion-color-theme);\n}\n\n.cardSubLabel {\n  color: #8C8E93;\n}\n\n.buttonBackground {\n  background: #FFFFFF;\n}\n\n.ion-card-update-btn {\n  background-color: #fff;\n  position: relative;\n  top: 16px;\n  bottom: 0px;\n  padding-top: 20px;\n  padding-bottom: 20px;\n}\n\n.br-20 {\n  border-radius: 20px;\n}\n\n.mb-20 {\n  margin-bottom: 20px;\n}\n\n.border-bottom-light-grey {\n  border-bottom: 1px solid lightgrey;\n}\n\n.is-invalid {\n  --border-color: red;\n}\n\n.invalid {\n  color: red;\n  padding-left: 1.2rem;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInVzZXJzLWxpc3QucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kscUJBQUE7RUFDQSx1QkFBQTtBQUNKOztBQUNBO0VBQ0kscUJBQUE7RUFDQSxvQkFBQTtBQUVKOztBQUFBO0VBQ0kscUJBQUE7QUFHSjs7QUFEQTtFQUNJLFlBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUFJSjs7QUFGQTtFQUNJLGFBQUE7RUFDQSxxQkFBQTtFQUNBLG1CQUFBO0VBQ0EsOEJBQUE7QUFLSjs7QUFIQTtFQUNJLGVBQUE7RUFDQSw2QkFBQTtBQU1KOztBQUpBO0VBQ0ksY0FBQTtBQU9KOztBQUxBO0VBQ0ksbUJBQUE7QUFRSjs7QUFMQTtFQUNJLHNCQUFBO0VBQ0Esa0JBQUE7RUFDQSxTQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0Esb0JBQUE7QUFRSjs7QUFKRTtFQUNJLG1CQUFBO0FBT047O0FBSEU7RUFDSSxtQkFBQTtBQU1OOztBQUhBO0VBQ0ksa0NBQUE7QUFNSjs7QUFIQTtFQUNJLG1CQUFBO0FBTUo7O0FBSkE7RUFDSSxVQUFBO0VBQ0Esb0JBQUE7QUFPSiIsImZpbGUiOiJ1c2Vycy1saXN0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi10b29sYmFyIHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNkY3RkM7XG4gICAgLS1ib3JkZXItY29sb3I6ICNGNkY3RkM7XG59XG5pb24tc2VhcmNoYmFye1xuICAgIC0tYmFja2dyb3VuZDogI0ZGRkZGRjtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDdweDtcbn1cbmlvbi1jb250ZW50IHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNkY3RkM7XG59XG4uc2hvd0xlZWRzTW9yZXtcbiAgICBoZWlnaHQ6IGF1dG87XG4gICAgLS1iYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJvcmRlci1yYWRpdXM6IDIzcHg7XG4gICAgYm9yZGVyOiB2YXIoLS1pb24tY29sb3ItdGhlbWUpO1xufVxuLnNob3dMZXNze1xuICAgIGhlaWdodDogMTUwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRkZGRkZGO1xuICAgIGJvcmRlci1yYWRpdXM6IDIzcHg7XG4gICAgYm9yZGVyOiB2YXIoLS1pb24tY29sb3ItdGhlbWUpO1xufVxuLmNhcmRMYWJlbHtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgY29sb3I6IHZhcigtLWlvbi1jb2xvci10aGVtZSk7XG59XG4uY2FyZFN1YkxhYmVse1xuICAgIGNvbG9yOiAjOEM4RTkzO1xufVxuLmJ1dHRvbkJhY2tncm91bmR7XG4gICAgYmFja2dyb3VuZDogI0ZGRkZGRjtcbn1cblxuLmlvbi1jYXJkLXVwZGF0ZS1idG57XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgdG9wOiAxNnB4O1xuICAgIGJvdHRvbTogMHB4O1xuICAgIHBhZGRpbmctdG9wOiAyMHB4O1xuICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xufVxuXG5cbiAgLmJyLTIwe1xuICAgICAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgfVxuXG5cbiAgLm1iLTIwe1xuICAgICAgbWFyZ2luLWJvdHRvbTogMjBweDtcbiAgfVxuXG4uYm9yZGVyLWJvdHRvbS1saWdodC1ncmV5e1xuICAgIGJvcmRlci1ib3R0b206IDFweCBzb2xpZCBsaWdodGdyZXk7XG59XG5cbi5pcy1pbnZhbGlke1xuICAgIC0tYm9yZGVyLWNvbG9yOiByZWQ7XG59XG4uaW52YWxpZHtcbiAgICBjb2xvcjpyZWQ7XG4gICAgcGFkZGluZy1sZWZ0OiAxLjJyZW07XG4gICBcbn0iXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_users-list_users-list_module_ts.js.map