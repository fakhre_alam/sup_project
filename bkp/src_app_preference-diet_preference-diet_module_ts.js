"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_preference-diet_preference-diet_module_ts"],{

/***/ 90223:
/*!*******************************************************************!*\
  !*** ./src/app/preference-diet/preference-diet-routing.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PreferenceDietPageRoutingModule": () => (/* binding */ PreferenceDietPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _preference_diet_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./preference-diet.page */ 82284);




const routes = [
    {
        path: '',
        component: _preference_diet_page__WEBPACK_IMPORTED_MODULE_0__.PreferenceDietPage
    }
];
let PreferenceDietPageRoutingModule = class PreferenceDietPageRoutingModule {
};
PreferenceDietPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], PreferenceDietPageRoutingModule);



/***/ }),

/***/ 97470:
/*!***********************************************************!*\
  !*** ./src/app/preference-diet/preference-diet.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PreferenceDietPageModule": () => (/* binding */ PreferenceDietPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _preference_diet_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./preference-diet-routing.module */ 90223);
/* harmony import */ var _preference_diet_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./preference-diet.page */ 82284);







let PreferenceDietPageModule = class PreferenceDietPageModule {
};
PreferenceDietPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _preference_diet_routing_module__WEBPACK_IMPORTED_MODULE_0__.PreferenceDietPageRoutingModule
        ],
        declarations: [_preference_diet_page__WEBPACK_IMPORTED_MODULE_1__.PreferenceDietPage]
    })
], PreferenceDietPageModule);



/***/ }),

/***/ 82284:
/*!*********************************************************!*\
  !*** ./src/app/preference-diet/preference-diet.page.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

var _shared_constants_nuts_json__WEBPACK_IMPORTED_MODULE_4___namespace_cache;
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PreferenceDietPage": () => (/* binding */ PreferenceDietPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_preference_diet_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./preference-diet.page.html */ 51939);
/* harmony import */ var _preference_diet_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./preference-diet.page.scss */ 75404);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../core/utility/utilities */ 53533);
/* harmony import */ var _services_app_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/app/app.service */ 61535);
/* harmony import */ var _shared_constants_nuts_json__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../shared/constants/nuts.json */ 69245);








let PreferenceDietPage = class PreferenceDietPage {
    constructor(cdr, router, utils, appService) {
        this.cdr = cdr;
        this.router = router;
        this.utils = utils;
        this.appService = appService;
        this.username = localStorage.getItem("loginEmail");
        this.nonVegWeek = new Array(7);
        this.fruitsWeek = new Array(7);
        this.desertWeek = new Array(7);
        this.nonVegDays = new Array(7);
        this.nonEggDays = new Array(7);
        this.detoxDays = new Array(7).fill(false);
        this.dinnerPref = new Array(3).fill(false);
        this.cheatDays = new Array(7);
        this.cheatTime = new Array(7);
        this.desertDays = new Array(7);
        this.desertTime = new Array(7);
        this.fruitsDays = new Array(7);
        this.fruitsTime = new Array(7);
        this.waterProtein = "";
        this.cookingExpeties = "";
        this.routine = "";
        this.fruitesCon = "";
        this.milkTea = "3";
        this.desert = true;
        this.midDayMeal = "";
        this.eatOutside = "";
        this.beatoDietPlan = [{ id: 'weightLoss', name: 'Weight Loss' }, { id: 'diabetes', name: 'Diabetes' }];
        this.dietPlan = [
            { id: 'fiteloWaterRetention', name: 'Water retention' },
            { id: 'fiteloWeightLoss', name: 'High Protein fiber' },
            { id: 'weightLoss', name: 'Weight Loss' },
            { id: 'muscleGain_morning', name: 'Muscle Gain Morning' },
            { id: 'muscleGain_evening', name: 'Muscle Gain Evening' },
            { id: 'fatShredding_morning', name: 'Fat Shredding Morning' },
            { id: 'fatShredding_evening', name: 'Fat Shredding Evening' },
            { id: 'diabetes', name: 'Diabetes' },
            { id: 'pcos', name: 'PCOS' },
            { id: 'cholesterol', name: 'Cholesterol' },
            { id: 'hypertension', name: 'Hypertension' }
        ];
        this.week = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
        this.dietData = [
            { value: '0', name: 'When You Wake up' },
            { value: '1', name: 'Before Breakfast' },
            { value: '2', name: 'Breakfast' },
            { value: '3', name: 'Mid Day Meal' },
            { value: '4', name: 'Lunch' },
            { value: '5', name: 'Post Lunch' },
            { value: '6', name: 'Evening Snack' },
            { value: '7', name: 'Dinner' },
            { value: '8', name: 'Before Sleep' },
        ];
        this.dietData1 = [
            { value: '1', name: 'Before Breakfast' },
            { value: '2', name: 'Breakfast' },
            { value: '3', name: 'Mid Day Meal' },
            { value: '4', name: 'Lunch' },
            { value: '5', name: 'Post Lunch' },
            { value: '6', name: 'Evening Snack' },
            { value: '7', name: 'Dinner' }
        ];
        this.dietData2 = [
            { value: '4', name: 'Lunch' }
        ];
        this.nonvegupto = [];
        this.defaultslot1 = [];
        this.defaultslot2 = [];
        this.defaultslot3 = [];
        this.defaultslot4 = [];
        this.defaultslot5 = [];
        this.defaultslot6 = [];
        this.actualLength = 2;
        this.isShow = [];
        this.preferNonVegTime = new Array(7).fill("");
        this.preferNonVegDays = new Array(this.actualLength);
        this.checkedDetox = 0;
        this.detoxLimit = 2;
        this.cheatLength = 2;
        this.isShow1 = [];
        this.isShow2 = [];
        this.isShow3 = [];
        this.nonvegupto1 = [];
        this.nonvegupto2 = [];
        this.nonvegupto3 = [];
        this.isT1 = [];
        this.isT2 = [];
        this.isT3 = [];
        this.isT4 = [];
        this.removedSlot = [];
        this.dinnerOptions = [];
        this.removeSlot = [];
        this.allnuts = [];
        this.dietitianPrefItems = [];
        this.slot3 = "0";
        this.slot6 = "8";
        if (this.username === 'beato') {
            this.dietPlan = [{ id: 'weightLoss', name: 'Weight Loss' }, { id: 'diabetes', name: 'Diabetes' }];
        }
        else {
            this.dietPlan = [
                { id: 'fiteloWaterRetention', name: 'Water retention' },
                { id: 'fiteloWeightLoss', name: 'High Protein fiber' },
                { id: 'weightLoss', name: 'Weight Loss' },
                { id: 'muscleGain_morning', name: 'Muscle Gain Morning' },
                { id: 'muscleGain_evening', name: 'Muscle Gain Evening' },
                { id: 'fatShredding_morning', name: 'Fat Shredding Morning' },
                { id: 'fatShredding_evening', name: 'Fat Shredding Evening' },
                { id: 'diabetes', name: 'Diabetes' },
                { id: 'pcos', name: 'PCOS' },
                { id: 'cholesterol', name: 'Cholesterol' },
                { id: 'hypertension', name: 'Hypertension' }
            ];
        }
    }
    ngOnInit() {
        this.getNutsValue();
        this.getPreferences();
        setTimeout(() => {
            var _a, _b, _c, _d;
            if (((_a = this.isShow) === null || _a === void 0 ? void 0 : _a.length) == 0) {
                this.isShow = [...this.isT1];
            }
            if (((_b = this.isShow2) === null || _b === void 0 ? void 0 : _b.length) == 0) {
                this.isShow2 = [...this.isT2];
            }
            if (((_c = this.isShow3) === null || _c === void 0 ? void 0 : _c.length) == 0) {
                this.isShow3 = [...this.isT3];
            }
            if (((_d = this.isShow1) === null || _d === void 0 ? void 0 : _d.length) == 0) {
                this.isShow1 = [...this.isT4];
            }
            console.log(this.detoxDays, this.checkedDetox);
            this.checkedDetox = this.detoxDays.filter(item => {
                return item === true;
            }).length;
        }, 500);
    }
    isShowItem(index, event) {
        this.preferNonVegDays.push(index);
        if (this.nonvegupto.length < this.actualLength && event.detail.checked) {
            this.nonvegupto.push(index);
            this.isShow.push(index);
        }
        else if (event.detail.checked == false) {
            this.preferNonVegTime[index - 1] = "";
            if (this.nonvegupto.includes(index)) {
                this.nonvegupto = this.nonvegupto.filter((item) => {
                    return item != index;
                });
            }
            this.isShow = this.isShow.filter((item) => {
                return item != index;
            });
        }
    }
    onCheckedDetox(event) {
        if (event.detail.checked === true) {
            this.checkedDetox = this.checkedDetox + 1;
        }
        else {
            this.checkedDetox = this.checkedDetox - 1;
        }
    }
    isShowDesertItem(index, event) {
        if (this.nonvegupto2.length < this.cheatLength && event.detail.checked) {
            this.nonvegupto2.push(index);
            this.isShow2.push(index);
        }
        else if (event.detail.checked == false) {
            this.desertTime[index - 1] = "";
            if (this.nonvegupto2.includes(index)) {
                this.nonvegupto2 = this.nonvegupto2.filter((item) => {
                    return item != index;
                });
            }
            this.isShow2 = this.isShow2.filter((item) => {
                return item != index;
            });
        }
    }
    isShowFruitsItem(index, event) {
        if (event.detail.checked) {
            this.nonvegupto3.push(index);
            this.isShow3.push(index);
        }
        else if (event.detail.checked == false) {
            if (this.nonvegupto3.includes(index)) {
                this.nonvegupto3 = this.nonvegupto3.filter((item) => {
                    return item != index;
                });
            }
            this.isShow3 = this.isShow3.filter((item) => {
                return item != index;
            });
        }
    }
    isShowCheatItem(index, event) {
        if (this.nonvegupto1.length < this.cheatLength && event.detail.checked) {
            this.nonvegupto1.push(index);
            this.isShow1.push(index);
        }
        else if (event.detail.checked == false) {
            this.cheatTime[index - 1] = '';
            if (this.nonvegupto1.includes(index)) {
                this.nonvegupto1 = this.nonvegupto1.filter((item) => {
                    return item != index;
                });
            }
            this.isShow1 = this.isShow1.filter((item) => {
                return item != index;
            });
        }
    }
    getPreferences() {
        this.dinnerOptions = new Array(3).fill('');
        const payload = {
            email: localStorage.getItem("email")
        };
        this.appService.getPreferences(payload).subscribe((res) => {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v, _w, _x, _y, _z, _0, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10, _11, _12, _13, _14;
            console.log("res", res);
            if ((res === null || res === void 0 ? void 0 : res.lifeStyle) != undefined) {
                this.waterProtein = res === null || res === void 0 ? void 0 : res.lifeStyle.dietPlanName;
                this.defaultIncomingPlan = res === null || res === void 0 ? void 0 : res.lifeStyle.dietPlanName;
            }
            if ((res === null || res === void 0 ? void 0 : res.additionalPref) != undefined && ((_b = (_a = res === null || res === void 0 ? void 0 : res.additionalPref) === null || _a === void 0 ? void 0 : _a.dietitianPrefItems) === null || _b === void 0 ? void 0 : _b.length) > 0) {
                (_c = res === null || res === void 0 ? void 0 : res.additionalPref) === null || _c === void 0 ? void 0 : _c.dietitianPrefItems.filter((item, index) => {
                    debugger;
                    if (item.category === "N") {
                        this.defaultslot1 = item === null || item === void 0 ? void 0 : item.foodId;
                        this.slot1 = item === null || item === void 0 ? void 0 : item.slot.toString();
                    }
                    else if (item.category === "NS") {
                        this.defaultslot2 = item === null || item === void 0 ? void 0 : item.foodId;
                        this.slot2 = item === null || item === void 0 ? void 0 : item.slot.toString();
                    }
                    else if (item.category === "DZ" && item.slot === 0) {
                        this.defaultslot3 = item === null || item === void 0 ? void 0 : item.foodId;
                        this.slot3 = "0";
                    }
                    else if (item.category === "DZ" && (item === null || item === void 0 ? void 0 : item.slot) === 8) {
                        this.defaultslot6 = item === null || item === void 0 ? void 0 : item.foodId;
                        this.slot6 = "8";
                    }
                    this.cdr.detectChanges();
                });
                this.dietitianPrefItems = (_d = res === null || res === void 0 ? void 0 : res.additionalPref) === null || _d === void 0 ? void 0 : _d.dietitianPrefItems.filter(item => {
                    return item.category !== "DM" && item.category !== "B";
                });
                const tempDMItem = (_e = res === null || res === void 0 ? void 0 : res.additionalPref) === null || _e === void 0 ? void 0 : _e.dietitianPrefItems.filter(item => {
                    return item.category === "DM";
                });
                let slot4Data = [];
                for (let index = 0; index < tempDMItem.length; index++) {
                    slot4Data.push(tempDMItem[index].slot + "");
                }
                const data4 = { slot: slot4Data, category: "DM", foodId: (_f = tempDMItem[0]) === null || _f === void 0 ? void 0 : _f.foodId };
                this.slot4 = slot4Data;
                this.defaultslot4 = (_g = tempDMItem[0]) === null || _g === void 0 ? void 0 : _g.foodId;
                const tempBItem = (_h = res === null || res === void 0 ? void 0 : res.additionalPref) === null || _h === void 0 ? void 0 : _h.dietitianPrefItems.filter(item => {
                    return item.category === "B";
                });
                let slot5Data = [];
                for (let index = 0; index < tempBItem.length; index++) {
                    slot5Data.push(tempBItem[index].slot + "");
                }
                const data5 = { slot: slot5Data, category: "B", foodId: (_j = tempBItem[0]) === null || _j === void 0 ? void 0 : _j.foodId };
                this.slot5 = slot5Data;
                this.defaultslot5 = (_k = tempBItem[0]) === null || _k === void 0 ? void 0 : _k.foodId;
                this.dietitianPrefItems.push(data4);
                this.dietitianPrefItems.push(data5);
                console.log("this.dietitianPrefItems", this.dietitianPrefItems);
            }
            if (((_m = (_l = res === null || res === void 0 ? void 0 : res.additionalPref) === null || _l === void 0 ? void 0 : _l.dinnerOptions) === null || _m === void 0 ? void 0 : _m.length) > 0) {
                (_p = (_o = res === null || res === void 0 ? void 0 : res.additionalPref) === null || _o === void 0 ? void 0 : _o.dinnerOptions) === null || _p === void 0 ? void 0 : _p.filter((itm, index) => {
                    if (itm === 'A') {
                        this.dinnerPref[0] = true;
                        this.dinnerOptions[0] = 'A';
                    }
                    if (itm === 'C') {
                        this.dinnerPref[1] = true;
                        this.dinnerOptions[1] = 'C';
                    }
                    if (itm === 'W') {
                        this.dinnerPref[2] = true;
                        this.dinnerOptions[2] = 'W';
                    }
                });
            }
            if ((res === null || res === void 0 ? void 0 : res.additionalPref) != undefined) {
                this.additionalPref1 = res === null || res === void 0 ? void 0 : res.additionalPref;
                this.removedSlot = (_q = res === null || res === void 0 ? void 0 : res.additionalPref) === null || _q === void 0 ? void 0 : _q.removeSlot.toString().split(',');
                this.removeSlot = this.removedSlot;
                this.week.filter((item, ind) => {
                    var _a, _b, _c;
                    for (let index = 0; index < ((_b = (_a = this.additionalPref1) === null || _a === void 0 ? void 0 : _a.notNonVegDay) === null || _b === void 0 ? void 0 : _b.length); index++) {
                        if (item === ((_c = this.additionalPref1) === null || _c === void 0 ? void 0 : _c.notNonVegDay[index])) {
                            this.nonVegWeek[ind] = true;
                        }
                    }
                });
                for (let index = 0; index < ((_s = (_r = this.additionalPref1) === null || _r === void 0 ? void 0 : _r.notNonEggDay) === null || _s === void 0 ? void 0 : _s.length); index++) {
                    this.week.filter((item, ind) => {
                        var _a;
                        if (item === ((_a = this.additionalPref1) === null || _a === void 0 ? void 0 : _a.notNonEggDay[index])) {
                            this.nonEggDays[ind] = true;
                        }
                    });
                }
                if (((_u = (_t = this.additionalPref1) === null || _t === void 0 ? void 0 : _t.nonVegLunch) === null || _u === void 0 ? void 0 : _u.length) > 0) {
                    for (let index = 0; index < ((_w = (_v = this.additionalPref1) === null || _v === void 0 ? void 0 : _v.nonVegLunch) === null || _w === void 0 ? void 0 : _w.length); index++) {
                        this.week.filter((item, ind) => {
                            var _a;
                            if (item === ((_a = this.additionalPref1) === null || _a === void 0 ? void 0 : _a.nonVegLunch[index])) {
                                this.nonVegDays[ind] = true;
                                this.isT1.push(ind + 1);
                                this.preferNonVegTime[ind] = 'Lunch';
                            }
                        });
                    }
                }
                if (((_y = (_x = this.additionalPref1) === null || _x === void 0 ? void 0 : _x.nonVegDinner) === null || _y === void 0 ? void 0 : _y.length) > 0) {
                    for (let index = 0; index < ((_0 = (_z = this.additionalPref1) === null || _z === void 0 ? void 0 : _z.nonVegDinner) === null || _0 === void 0 ? void 0 : _0.length); index++) {
                        this.week.filter((item, ind) => {
                            var _a;
                            if (item === ((_a = this.additionalPref1) === null || _a === void 0 ? void 0 : _a.nonVegDinner[index])) {
                                this.nonVegDays[ind] = true;
                                this.isT1.push(ind + 1);
                                this.preferNonVegTime[ind] = 'Dinner';
                            }
                        });
                    }
                }
                if (((_2 = (_1 = this.additionalPref1) === null || _1 === void 0 ? void 0 : _1.cheatDaysDinner) === null || _2 === void 0 ? void 0 : _2.length) > 0) {
                    for (let index = 0; index < ((_4 = (_3 = this.additionalPref1) === null || _3 === void 0 ? void 0 : _3.cheatDaysDinner) === null || _4 === void 0 ? void 0 : _4.length); index++) {
                        this.week.filter((item, ind) => {
                            var _a;
                            if (item === ((_a = this.additionalPref1) === null || _a === void 0 ? void 0 : _a.cheatDaysDinner[index])) {
                                this.cheatDays[ind] = true;
                                this.isT4.push(ind + 1);
                                this.cheatTime[ind] = 'Dinner';
                            }
                        });
                    }
                }
                if (((_6 = (_5 = this.additionalPref1) === null || _5 === void 0 ? void 0 : _5.cheatDaysLunch) === null || _6 === void 0 ? void 0 : _6.length) > 0) {
                    for (let index = 0; index < ((_8 = (_7 = this.additionalPref1) === null || _7 === void 0 ? void 0 : _7.cheatDaysLunch) === null || _8 === void 0 ? void 0 : _8.length); index++) {
                        this.week.filter((item, ind) => {
                            var _a;
                            if (item === ((_a = this.additionalPref1) === null || _a === void 0 ? void 0 : _a.cheatDaysLunch[index])) {
                                this.cheatDays[ind] = true;
                                this.isT4.push(ind + 1);
                                this.cheatTime[ind] = 'Lunch';
                            }
                        });
                    }
                }
                if (((_10 = (_9 = this.additionalPref1) === null || _9 === void 0 ? void 0 : _9.fruitsDaysEveMeal) === null || _10 === void 0 ? void 0 : _10.length) > 0) {
                    for (let index = 0; index < ((_12 = (_11 = this.additionalPref1) === null || _11 === void 0 ? void 0 : _11.fruitsDaysEveMeal) === null || _12 === void 0 ? void 0 : _12.length); index++) {
                        this.week.filter((item, ind) => {
                            if (item === this.additionalPref1.fruitsDaysEveMeal[index]) {
                                this.fruitsDays[ind] = true;
                                this.isT3.push(ind + 1);
                                this.fruitsTime[ind] = 'Dinner';
                            }
                        });
                    }
                }
                if (this.additionalPref1.fruitsDaysMidDay.length > 0) {
                    for (let index = 0; index < this.additionalPref1.fruitsDaysMidDay.length; index++) {
                        this.week.filter((item, ind) => {
                            if (item === this.additionalPref1.fruitsDaysMidDay[index]) {
                                this.fruitsDays[ind] = true;
                                this.isT3.push(ind + 1);
                                this.fruitsTime[ind] = 'Lunch';
                            }
                        });
                    }
                }
                if (this.additionalPref1.dessertDaysEveMeal.length > 0) {
                    for (let index = 0; index < this.additionalPref1.dessertDaysEveMeal.length; index++) {
                        this.week.filter((item, ind) => {
                            if (item === this.additionalPref1.dessertDaysEveMeal[index]) {
                                this.desertDays[ind] = true;
                                this.isT2.push(ind + 1);
                                this.desertTime[ind] = 'Dinner';
                            }
                        });
                    }
                }
                if (this.additionalPref1.dessertDaysMidDay.length > 0) {
                    for (let index = 0; index < this.additionalPref1.dessertDaysMidDay.length; index++) {
                        this.week.filter((item, ind) => {
                            if (item === this.additionalPref1.dessertDaysMidDay[index]) {
                                this.desertDays[ind] = true;
                                this.isT2.push(ind + 1);
                                this.desertTime[ind] = 'Lunch';
                            }
                        });
                    }
                }
                this.nonvegupto = [...this.isShow];
                this.nonvegupto1 = [...this.isShow1];
                this.nonvegupto2 = [...this.isShow2];
                this.nonvegupto3 = [...this.isShow3];
                if (((_14 = (_13 = this.additionalPref1) === null || _13 === void 0 ? void 0 : _13.detoxDay) === null || _14 === void 0 ? void 0 : _14.length) > 0) {
                    for (let index = 0; index < this.additionalPref1.detoxDay.length; index++) {
                        this.week.filter((item, ind) => {
                            if (item === this.additionalPref1.detoxDay[index]) {
                                this.detoxDays[ind] = true;
                                this.checkedDetox = this.checkedDetox + 1;
                                // this.isT3[]
                            }
                        });
                    }
                }
                this.cookingExpeties = this.additionalPref1.cookingProficiency + '';
                this.routine = this.additionalPref1.cookingTimeAvailability + '';
                this.fruitesCon = this.additionalPref1.fruitsLiking + '';
                this.milkTea = (this.additionalPref1.teaLiking == false && this.additionalPref1.coffeeLiking == false) ? '3' : this.additionalPref1.teaLiking == true ? '2' : '1';
                this.midDayMeal = this.additionalPref1.midDayMeals + '';
            }
        });
    }
    goback() {
        this.router.navigate(['deitician-search'])
            .then(() => {
            window.location.reload();
        });
    }
    updatePreferences() {
        var _a, _b, _c;
        const fruitsWeekDays = [];
        const desertsWeekDays = [];
        const notNonVegDay = [];
        const notNonEggDay = [];
        const nonVegLunch = [];
        const nonVegDinner = [];
        const cheatLunch = [];
        const cheatDinner = [];
        const desertLunch = [];
        const desertDinner = [];
        const fruitsLunch = [];
        const fruitsDinner = [];
        const detoxDaysdetail = [];
        for (let index = 0; index < this.fruitsWeek.length; index++) {
            if (this.fruitsWeek[index]) {
                fruitsWeekDays.push(this.week[index]);
            }
        }
        for (let index = 0; index < this.desertWeek.length; index++) {
            if (this.desertWeek[index]) {
                desertsWeekDays.push(this.week[index]);
            }
        }
        for (let index = 0; index < this.nonVegWeek.length; index++) {
            if (this.nonVegWeek[index]) {
                notNonVegDay.push(this.week[index]);
            }
        }
        for (let index = 0; index < this.nonEggDays.length; index++) {
            if (this.nonEggDays[index]) {
                notNonEggDay.push(this.week[index]);
            }
        }
        for (let index = 0; index < this.nonVegDays.length; index++) {
            if (this.nonVegDays[index]) {
                if (this.preferNonVegTime[index] === "Lunch") {
                    nonVegLunch.push(this.week[index]);
                }
                if (this.preferNonVegTime[index] === "Dinner") {
                    nonVegDinner.push(this.week[index]);
                }
            }
        }
        for (let index = 0; index < this.cheatDays.length; index++) {
            if (this.cheatDays[index]) {
                if (this.cheatTime[index] === "Lunch") {
                    cheatLunch.push(this.week[index]);
                }
                if (this.cheatTime[index] === "Dinner") {
                    cheatDinner.push(this.week[index]);
                }
            }
        }
        for (let index = 0; index < this.desertDays.length; index++) {
            if (this.desertDays[index]) {
                if (this.desertTime[index] === "Lunch") {
                    desertLunch.push(this.week[index]);
                }
                if (this.desertTime[index] === "Dinner") {
                    desertDinner.push(this.week[index]);
                }
            }
        }
        for (let index = 0; index < this.fruitsDays.length; index++) {
            if (this.fruitsDays[index]) {
                if (this.fruitsTime[index] === "Lunch") {
                    fruitsLunch.push(this.week[index]);
                }
                if (this.fruitsTime[index] === "Dinner") {
                    fruitsDinner.push(this.week[index]);
                }
            }
        }
        for (let index = 0; index < this.detoxDays.length; index++) {
            if (this.detoxDays[index]) {
                detoxDaysdetail.push(this.week[index]);
            }
        }
        this.dietitianPrefItems = this.dietitianPrefItems.filter(item => {
            return item.category !== "DM" && item.category !== "B";
        });
        if (this.slot4 != undefined && ((_a = this.defaultslot4) === null || _a === void 0 ? void 0 : _a.length) > 0) {
            for (let index = 0; index < this.slot4.length; index++) {
                this.dietitianPrefItems.push({ slot: this.slot4[index], category: "DM", foodId: this.defaultslot4 });
            }
        }
        if (this.slot5 != undefined && ((_b = this.defaultslot5) === null || _b === void 0 ? void 0 : _b.length) > 0) {
            for (let index = 0; index < this.slot5.length; index++) {
                this.dietitianPrefItems.push({ slot: this.slot5[index], category: "B", foodId: this.defaultslot5 });
            }
        }
        const payload = {
            _id: localStorage.getItem("email"),
            additionalPref: {
                planChosen: this.waterProtein,
                notNonVegDay: notNonVegDay,
                notNonEggDay: notNonEggDay,
                nonVegLunch: nonVegLunch,
                nonVegDinner: nonVegDinner,
                teaLiking: this.milkTea === "3" ? false : this.milkTea == '1' ? false : true,
                coffeeLiking: this.milkTea === "3" ? false : this.milkTea == '2' ? false : true,
                fruitsLiking: this.fruitesCon !== "" ? true : false,
                cookingProficiency: this.cookingExpeties != "undefined" ? this.cookingExpeties : "",
                cookingTimeAvailability: this.routine != "undefined" ? this.routine : "",
                cheatDaysLunch: cheatLunch,
                cheatDaysDinner: cheatDinner,
                normalDetoxDay: detoxDaysdetail,
                caloriesDeficit: 500,
                midDayMeals: this.midDayMeal === "" ? [] : (_c = this.midDayMeal) === null || _c === void 0 ? void 0 : _c.split(','),
                fruitsDays: fruitsWeekDays,
                dessertDaysMidDay: desertLunch,
                dessertDaysEveMeal: desertDinner,
                fruitsDaysMidDay: fruitsLunch,
                fruitsDaysEveMeal: fruitsDinner,
                removeSlot: this.removeSlot.length > 0 && this.removeSlot[0] != "" && this.removeSlot != null ? this.removeSlot : [],
                dietitianPrefItems: this.dietitianPrefItems,
                dinnerOptions: this.dinnerOptions.filter(item => {
                    return item !== "";
                })
            },
        };
        // if(this.defaultIncomingPlan !==this.waterProtein){
        //   payload.additionalPref["planChosen"]= this.waterProtein;
        // }
        // else{
        //   delete payload.additionalPref["planChosen"];
        // }
        console.log("payload", payload);
        this.appService.updateDietPref(payload).subscribe((res) => {
            console.log("res", res);
            this.utils.presentAlert('Record Saved successfully!');
            //  this.router.navigate(["deitician-search"]);
            //  this.router.navigate(['deitician-search'])
            //  .then(() => {
            //    window.location.reload();
            //  });
        });
    }
    onCheckedDinnerPref(e, item) {
        console.log(e.detail.checked);
        if (e.detail.checked && item.itm === 'A') {
            this.dinnerPref[0] = true;
            this.dinnerOptions[0] = 'A';
        }
        else if (!e.detail.checked && item.itm === 'A') {
            this.dinnerPref[0] = false;
            this.dinnerOptions[0] = '';
        }
        if (e.detail.checked && item.itm === 'C') {
            this.dinnerPref[1] = true;
            this.dinnerOptions[1] = 'C';
        }
        else if (!e.detail.checked && item.itm === 'C') {
            this.dinnerPref[1] = false;
            this.dinnerOptions[1] = '';
        }
        if (e.detail.checked && item.itm === 'W') {
            this.dinnerPref[2] = true;
            this.dinnerOptions[2] = 'W';
        }
        else if (!e.detail.checked && item.itm === 'W') {
            this.dinnerPref[2] = false;
            this.dinnerOptions[2] = '';
        }
        this.dinnerOptions = this.dinnerOptions.filter(item => {
            return item !== '';
        });
    }
    selectedSlotForRemove(e) {
        this.removeSlot = e.detail.value.toString().split(',');
        console.log('ionChange fired with value: ' + this.removeSlot);
    }
    getNutsValue() {
        var _a;
        this.allnuts = (_a = JSON.parse(JSON.stringify(/*#__PURE__*/ (_shared_constants_nuts_json__WEBPACK_IMPORTED_MODULE_4___namespace_cache || (_shared_constants_nuts_json__WEBPACK_IMPORTED_MODULE_4___namespace_cache = __webpack_require__.t(_shared_constants_nuts_json__WEBPACK_IMPORTED_MODULE_4__, 2)))))) === null || _a === void 0 ? void 0 : _a.nuts;
        console.log("nuts", this.allnuts);
    }
    selecteSlot1(e) {
        this.slot1 = e.detail.value;
        this.defaultslot1 = [];
        this.dietitianPrefItems.find((item, index) => {
            if ((item === null || item === void 0 ? void 0 : item.category) === "N") {
                this.dietitianPrefItems.splice(index, 1);
            }
        });
    }
    selecteSlot2(e) {
        this.slot2 = e.detail.value;
        this.defaultslot2 = [];
        this.dietitianPrefItems.find((item, index) => {
            if ((item === null || item === void 0 ? void 0 : item.category) === "NS") {
                this.dietitianPrefItems.splice(index, 1);
            }
        });
    }
    selecteSlot3(e) {
        this.slot3 = e.detail.value;
        this.defaultslot3 = [];
        this.dietitianPrefItems.find((item, index) => {
            if ((item === null || item === void 0 ? void 0 : item.category) === "DZ") {
                this.dietitianPrefItems.splice(index, 1);
            }
        });
    }
    selecteSlot4(e) {
        this.slot4 = e.detail.value;
        this.defaultslot4 = [];
        // this.dietitianPrefItems.find((item, index)=>{
        //   if(item?.category==="DM"){
        //     this.dietitianPrefItems.splice(index,1);
        //   }
        //   });
    }
    selecteSlot5(e) {
        this.slot5 = e.detail.value;
        this.defaultslot5 = [];
        // this.dietitianPrefItems.find((item, index)=>{
        //   if(item?.category==="B"){
        //     this.dietitianPrefItems.splice(index,1);
        //   }
        //   });
    }
    selecteSlot6(e) {
        this.slot6 = e.detail.value;
        this.defaultslot6 = [];
        this.dietitianPrefItems.find((item, index) => {
            if ((item === null || item === void 0 ? void 0 : item.category) === "DZ") {
                this.dietitianPrefItems.splice(index, 1);
            }
        });
    }
    selectedNutsinSlot1(e) {
        if (e.detail.value != "" && e.detail.value != undefined) {
            this.dietitianPrefItems.find((item, index) => {
                if (item.category === "N") {
                    this.dietitianPrefItems[index].slot = this.slot1;
                    this.dietitianPrefItems[index].category = "N";
                    this.dietitianPrefItems[index].foodId = e.detail.value.toString().split(',');
                    return;
                }
            });
            if (this.dietitianPrefItems.filter(item => item.category === "N").length === 0) {
                this.dietitianPrefItems.push({ slot: this.slot1, category: "N", foodId: e.detail.value.toString().split(',') });
            }
            if (this.dietitianPrefItems.length == 0) {
                this.dietitianPrefItems.push({ slot: this.slot1, category: "N", foodId: e.detail.value.toString().split(',') });
            }
        }
        else {
            this.dietitianPrefItems.find((item, index) => {
                if (item.category === "N") {
                    this.dietitianPrefItems.splice(index, 1);
                }
            });
        }
    }
    selectedNutsinSlot2(e) {
        if (e.detail.value != "" && e.detail.value != undefined) {
            this.dietitianPrefItems.find((item, index) => {
                if (item.category === "NS") {
                    this.dietitianPrefItems[index].slot = this.slot2;
                    this.dietitianPrefItems[index].category = "NS";
                    this.dietitianPrefItems[index].foodId = e.detail.value.toString().split(',');
                    return;
                }
            });
            if (this.dietitianPrefItems.filter(item => item.category === "NS").length === 0) {
                this.dietitianPrefItems.push({ slot: this.slot2, category: "NS", foodId: e.detail.value.toString().split(',') });
            }
            if (this.dietitianPrefItems.length == 0) {
                this.dietitianPrefItems.push({ slot: this.slot2, category: "NS", foodId: e.detail.value.toString().split(',') });
            }
        }
        else {
            this.dietitianPrefItems.find((item, index) => {
                if (item.category === "NS") {
                    this.dietitianPrefItems.splice(index, 1);
                }
            });
        }
    }
    selectedNutsinSlot3(e) {
        if (e.detail.value != "" && e.detail.value != undefined) {
            this.dietitianPrefItems.find((item, index) => {
                if (item.category === "DZ" && item.slot === 0) {
                    this.dietitianPrefItems[index].slot = 0;
                    this.dietitianPrefItems[index].category = "DZ";
                    this.dietitianPrefItems[index].foodId = e.detail.value.toString().split(',');
                    return;
                }
            });
            if (this.dietitianPrefItems.filter(item => item.category === "DZ" && item.slot === 0).length === 0) {
                this.dietitianPrefItems.push({ slot: 0, category: "DZ", foodId: e.detail.value.toString().split(',') });
            }
            if (this.dietitianPrefItems.length == 0) {
                this.dietitianPrefItems.push({ slot: 0, category: "DZ", foodId: e.detail.value.toString().split(',') });
            }
        }
        else {
            this.dietitianPrefItems.find((item, index) => {
                if (item.category === "DZ" && this.dietitianPrefItems[index].slot === 0) {
                    this.dietitianPrefItems.splice(index, 1);
                }
            });
        }
    }
    selectedNutsinSlot4(e) {
        this.dietitianPrefItems = this.dietitianPrefItems.filter(item => {
            return item.category !== "DM" && item.category !== "B";
        });
        //if(e.detail.value!="" && e.detail.value!=undefined){
        //   this.dietitianPrefItems.find((item, index)=>{
        //     if(item.category ==="DM"){
        //      this.dietitianPrefItems[index].slot=this.slot4;
        //      this.dietitianPrefItems[index].category="DM";
        //      this.dietitianPrefItems[index].foodId = e.detail.value.toString().split(',');
        //      return;
        //     }
        //  })
        //  if(this.dietitianPrefItems.filter(item=> item.category==="DM").length===0){
        //   this.dietitianPrefItems.push({slot:this.slot4,category:"DM",foodId:e.detail.value.toString().split(',')});
        // }
        //  if(this.dietitianPrefItems.length==0){
        //    this.dietitianPrefItems.push({slot:this.slot4,category:"DM",foodId:e.detail.value.toString().split(',')});
        //  }
        // }else{
        //   this.dietitianPrefItems.find((item, index)=>{
        //     if(item.category ==="DM" ){
        //      this.dietitianPrefItems.splice(index,1);
        //     }
        //  })
        // }
    }
    selectedNutsinSlot5(e) {
        this.dietitianPrefItems = this.dietitianPrefItems.filter(item => {
            return item.category !== "B" && item.category !== "DM";
        });
        // if(e.detail.value!="" && e.detail.value!=undefined){
        //   this.dietitianPrefItems.find((item, index)=>{
        //     if(item.category ==="B"){
        //      this.dietitianPrefItems[index].slot=this.slot5;
        //      this.dietitianPrefItems[index].category="B";
        //      this.dietitianPrefItems[index].foodId = e.detail.value.toString().split(',');
        //      return;
        //     }
        //  })
        //  if(this.dietitianPrefItems.filter(item=> item.category==="B").length===0){
        //   this.dietitianPrefItems.push({slot:this.slot5,category:"B",foodId:e.detail.value.toString().split(',')});
        // }
        //  if(this.dietitianPrefItems.length==0){
        //    this.dietitianPrefItems.push({slot:this.slot5,category:"B",foodId:e.detail.value.toString().split(',')});
        //  }
        // }
        // else{
        //   this.dietitianPrefItems.find((item, index)=>{
        //     if(item.category ==="B"){
        //      this.dietitianPrefItems.splice(index,1);
        //     }
        //  })
        // }
    }
    selectedNutsinSlot6(e) {
        if (e.detail.value != "" && e.detail.value != undefined) {
            this.dietitianPrefItems.find((item, index) => {
                if (item.category === "DZ" && this.dietitianPrefItems[index].slot === 8) {
                    this.dietitianPrefItems[index].slot = 8;
                    this.dietitianPrefItems[index].category = "DZ";
                    this.dietitianPrefItems[index].foodId = e.detail.value.toString().split(',');
                    return;
                }
            });
            if (this.dietitianPrefItems.filter(item => item.category === "DZ" && item.slot === 8).length === 0) {
                this.dietitianPrefItems.push({ slot: 8, category: "DZ", foodId: e.detail.value.toString().split(',') });
            }
            if (this.dietitianPrefItems.length == 0) {
                this.dietitianPrefItems.push({ slot: 8, category: "DZ", foodId: e.detail.value.toString().split(',') });
            }
        }
        else {
            this.dietitianPrefItems.find((item, index) => {
                if (item.category === "DZ" && this.dietitianPrefItems[index].slot === 8) {
                    this.dietitianPrefItems.splice(index, 1);
                }
            });
        }
    }
    save() { }
};
PreferenceDietPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ChangeDetectorRef },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_6__.Router },
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_2__.UTILITIES },
    { type: _services_app_app_service__WEBPACK_IMPORTED_MODULE_3__.AppService }
];
PreferenceDietPage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: "app-preference-diet",
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_preference_diet_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_preference_diet_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], PreferenceDietPage);



/***/ }),

/***/ 51939:
/*!**************************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/preference-diet/preference-diet.page.html ***!
  \**************************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-icon name=\"arrow-back-outline\" color=\"#fff\" style=\"font-size: 2rem;\" (click)=\"goback()\"></ion-icon>\n    </ion-buttons>\n    <ion-title>Diet Preferences</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  \n  <ion-card>\n    <ion-row style=\"margin-bottom: .5rem;\">\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:1.1rem;color:#01A3A4;\">\n        <b>Please choose the Diet Plan</b>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-list lines=\"none\"> \n        \n          <ion-radio-group mode=\"md\"  [value]=\"waterProtein\" [(ngModel)]=\"waterProtein\">\n            <!-- *ngIf=\"username!=='fitelo'\" -->\n            <ion-item class=\"diet-name\" *ngFor=\"let item of dietPlan\">\n              <ion-radio   color=\"primary\" value=\"{{item.id}}\"></ion-radio>\n              <ion-label>{{item.name}}</ion-label>\n            </ion-item>\n          </ion-radio-group>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n  </ion-card>\n  \n  <ion-card>\n    <ion-row style=\"margin-bottom: .5rem;\">\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:1.1rem;color:#01A3A4;\">\n        <b>Please set customer preferences</b>\n      </ion-col>\n    </ion-row>\n    \n    <div>\n    <ion-row>\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:.9rem;color:#000000;\">\n        <b>Days on which does NOT take Non Veg</b><br><br>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonVegWeek[0]\"></ion-checkbox>\n              <ion-label>Sunday</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonVegWeek[1]\"></ion-checkbox>\n              <ion-label>Monday</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonVegWeek[2]\"></ion-checkbox>\n              <ion-label>Tuesday</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonVegWeek[3]\"></ion-checkbox>\n              <ion-label>Wednesday</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonVegWeek[4]\"></ion-checkbox>\n              <ion-label>Thursday</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonVegWeek[5]\"></ion-checkbox>\n              <ion-label>Friday</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonVegWeek[6]\"></ion-checkbox>\n              <ion-label>Saturday</ion-label>\n            </ion-item>\n          </ion-col>\n\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    \n    <ion-row>\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:.9rem;color:#000000;\"><br>\n        <b style=\"font-size:.9rem;\">Preferred days of Non Veg </b> <span style=\"font-size:.8rem;\">(upto {{actualLength}}\n          days)</span><br>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\" > \n              <ion-checkbox [disabled]=\"(isShow.length==actualLength) && !isShow.includes(1)?true:false\" mode=\"md\" [(ngModel)]=\"nonVegDays[0]\" (ionChange)=\"isShowItem(1,$event)\"></ion-checkbox>\n              <ion-label>Sunday</ion-label>\n            </ion-item>\n            \n            <div *ngIf=\"isShow.includes(1)\" style=\"margin-left:2.7rem\">\n              <ion-row>\n                <ion-col class=\"ion-text-left\">\n                  <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"preferNonVegTime[0]\">\n                    <ion-item lines=\"none\">\n                     \n                      <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                      <ion-label>Lunch</ion-label>\n                    </ion-item>\n\n                    <ion-item lines=\"none\">\n                     \n                      <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                      <ion-label>Dinner</ion-label>\n                    </ion-item>\n\n                  </ion-radio-group>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox [disabled]=\"(isShow.length==actualLength) && !isShow.includes(2)?true:false\" mode=\"md\" [(ngModel)]=\"nonVegDays[1]\" (ionChange)=\"isShowItem(2,$event)\"></ion-checkbox>\n              <ion-label>Monday</ion-label>\n            </ion-item>\n            \n            <div *ngIf=\"isShow.includes(2)\" style=\"margin-left:2.7rem\">\n              <ion-row>\n                <ion-col class=\"ion-text-left\">\n                  <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"preferNonVegTime[1]\">\n                    <ion-item lines=\"none\">\n                     \n                      <ion-radio color=\"primary\" value=\"Lunch\"></ion-radio>\n                      <ion-label>Lunch</ion-label>\n                    </ion-item>\n\n                    <ion-item lines=\"none\">\n                     \n                      <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                      <ion-label>Dinner</ion-label>\n                    </ion-item>\n\n                  </ion-radio-group>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\" >\n              <ion-checkbox [disabled]=\"(isShow.length==actualLength) && !isShow.includes(3)?true:false\" mode=\"md\" [(ngModel)]=\"nonVegDays[2]\" (ionChange)=\"isShowItem(3,$event)\"></ion-checkbox>\n              <ion-label>Tuesday</ion-label>\n            </ion-item>\n            <div *ngIf=\"isShow.includes(3)\"  style=\"margin-left:2.7rem\">\n              <ion-row>\n                <ion-col class=\"ion-text-left\">\n                  <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"preferNonVegTime[2]\">\n                    <ion-item lines=\"none\">\n                     \n                      <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                      <ion-label>Lunch</ion-label>\n                    </ion-item>\n\n                    <ion-item lines=\"none\">\n                     \n                      <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                      <ion-label>Dinner</ion-label>\n                    </ion-item>\n\n                  </ion-radio-group>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox [disabled]=\"(isShow.length==actualLength) && !isShow.includes(4) ?true:false\" mode=\"md\" [(ngModel)]=\"nonVegDays[3]\" (ionChange)=\"isShowItem(4,$event)\"></ion-checkbox>\n              <ion-label>Wednesday</ion-label>\n            </ion-item>\n            <div *ngIf=\"isShow.includes(4)\"  style=\"margin-left:2.7rem\">\n              <ion-row>\n                <ion-col class=\"ion-text-left\">\n                  <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"preferNonVegTime[3]\">\n                    <ion-item lines=\"none\">\n                      \n                      <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                      <ion-label>Lunch</ion-label>\n                    </ion-item>\n\n                    <ion-item lines=\"none\">\n                      \n                      <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                      <ion-label>Dinner</ion-label>\n                    </ion-item>\n\n                  </ion-radio-group>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\" >\n              <ion-checkbox [disabled]=\"(isShow.length==actualLength) && !isShow.includes(5)?true:false\" mode=\"md\" [(ngModel)]=\"nonVegDays[4]\" (ionChange)=\"isShowItem(5,$event)\"></ion-checkbox>\n              <ion-label>Thursday</ion-label>\n            </ion-item>\n            \n            <div *ngIf=\"isShow.includes(5)\"  style=\"margin-left:2.7rem\">\n              <ion-row>\n                <ion-col class=\"ion-text-left\">\n                  <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"preferNonVegTime[4]\">\n                    <ion-item lines=\"none\">\n                      \n                      <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                      <ion-label>Lunch</ion-label>\n                    </ion-item>\n\n                    <ion-item lines=\"none\">\n                      \n                      <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                      <ion-label>Dinner</ion-label>\n                    </ion-item>\n\n                  </ion-radio-group>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\" >\n              <ion-checkbox [disabled]=\"(isShow.length==actualLength) && !isShow.includes(6)?true:false\" mode=\"md\" [(ngModel)]=\"nonVegDays[5]\" (ionChange)=\"isShowItem(6,$event)\"></ion-checkbox>\n              <ion-label>Friday</ion-label>\n            </ion-item>\n            \n            <div *ngIf=\"isShow.includes(6)\"  style=\"margin-left:2.7rem\">\n              <ion-row>\n                <ion-col class=\"ion-text-left\">\n                  <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"preferNonVegTime[5]\">\n                    <ion-item lines=\"none\">\n                      \n                      <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                      <ion-label>Lunch</ion-label>\n                    </ion-item>\n\n                    <ion-item lines=\"none\">\n                      \n                      <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                      <ion-label>Dinner</ion-label>\n                    </ion-item>\n\n                  </ion-radio-group>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\" >\n              <ion-checkbox mode=\"md\" [disabled]=\"(isShow.length==actualLength) && !isShow.includes(7)?true:false\" [(ngModel)]=\"nonVegDays[6]\" (ionChange)=\"isShowItem(7,$event)\"></ion-checkbox>\n              <ion-label>Saturday</ion-label>\n            </ion-item>\n            \n            <div *ngIf=\"isShow.includes(7)\"   style=\"margin-left:2.7rem\">\n              <ion-row>\n                <ion-col class=\"ion-text-left\">\n                  <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"preferNonVegTime[6]\">\n                    <ion-item lines=\"none\">\n                     \n                      <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                      <ion-label>Lunch</ion-label>\n                    </ion-item>\n\n                    <ion-item lines=\"none\">\n                     \n                      <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                      <ion-label>Dinner</ion-label>\n                    </ion-item>\n\n                  </ion-radio-group>\n                </ion-col>\n              </ion-row>\n            </div>\n          </ion-col>\n        </ion-row>\n      </ion-col>\n    </ion-row>\n  </div>\n    \n    <ion-row>\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:.9rem;color:#000000;\"><br>\n        <b>Days on which does NOT take Egg</b><br>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonEggDays[0]\"></ion-checkbox>\n              <ion-label>Sunday</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonEggDays[1]\"></ion-checkbox>\n              <ion-label>Monday</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonEggDays[2]\"></ion-checkbox>\n              <ion-label>Tuesday</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonEggDays[3]\"></ion-checkbox>\n              <ion-label>Wednesday</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonEggDays[4]\"></ion-checkbox>\n              <ion-label>Thursday</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonEggDays[5]\"></ion-checkbox>\n              <ion-label>Friday</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"nonEggDays[6]\"></ion-checkbox>\n              <ion-label>Saturday</ion-label>\n            </ion-item>\n          </ion-col>\n\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    \n    <ion-row style=\"margin-bottom: .5rem;\">\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:1.1rem;color:#01A3A4;\">\n        <b>Cooking Expertise</b>\n      </ion-col>\n    </ion-row>\n   \n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-list lines=\"none\">\n          <ion-radio-group mode=\"md\" value=\"cooking\" [(ngModel)]=\"cookingExpeties\">\n            <ion-item>\n              <ion-radio  color=\"primary\" value=\"9\"></ion-radio>\n              <ion-label>High - have a cook</ion-label>\n            </ion-item>\n\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"6\"></ion-radio>\n              <ion-label>Medium - family member/myself can cook</ion-label>\n            </ion-item>\n\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"3\"></ion-radio>\n              <ion-label>Low - self but amateur cook</ion-label>\n            </ion-item>\n\n          </ion-radio-group>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n\n<!--     \n    <ion-row style=\"margin-bottom: .5rem;\">\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:1.1rem;color:#01A3A4;\">\n        <b>Routine</b>\n      </ion-col>\n    </ion-row>\n   \n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-list lines=\"none\">\n          <ion-radio-group mode=\"md\" value=\"cooking\" [(ngModel)]=\"routine\">\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"9\"></ion-radio>\n              <ion-label>Idle (e.g housewife)</ion-label>\n            </ion-item>\n\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"6\"></ion-radio>\n              <ion-label>Busy (e.g IT person)</ion-label>\n            </ion-item>\n\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"3\"></ion-radio>\n              <ion-label>Super Busy</ion-label>\n            </ion-item>\n\n          </ion-radio-group>\n        </ion-list>\n      </ion-col>\n    </ion-row> -->\n\n    \n    <!-- <ion-row style=\"margin-bottom: .5rem;\">\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:1.1rem;color:#01A3A4;\">\n        <b>Current fruits consumption</b>\n      </ion-col>\n    </ion-row>\n   \n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-list lines=\"none\">\n          <ion-radio-group mode=\"md\" value=\"cooking\" [(ngModel)]=\"fruitesCon\">\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"false\"></ion-radio>\n              <ion-label>Low</ion-label>\n            </ion-item>\n\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"true\"></ion-radio>\n              <ion-label>High</ion-label>\n            </ion-item>\n\n         \n\n          </ion-radio-group>\n        </ion-list>\n      </ion-col>\n    </ion-row> -->\n    \n  \n  \n    <!-- <div>\n    \n     <ion-row>\n        <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:.9rem;color:#000000;\">\n          <b>Preferred days of Fruits </b>\n          <br><br> \n        </ion-col>\n      </ion-row>\n    \n      <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\" >\n                  <ion-checkbox mode=\"md\" [(ngModel)]=\"fruitsDays[0]\" (ionChange)=\"isShowFruitsItem(1,$event)\"></ion-checkbox>\n                  <ion-label>Sunday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow3.includes(1)\" style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"fruitsTime[0]\">\n                        <ion-item lines=\"none\">\n                         \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                         \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n    \n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\">\n                  <ion-checkbox  mode=\"md\" [(ngModel)]=\"fruitsDays[1]\" (ionChange)=\"isShowFruitsItem(2,$event)\"></ion-checkbox>\n                  <ion-label>Monday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow3.includes(2)\" style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"fruitsTime[1]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\" >\n                  <ion-checkbox  mode=\"md\" [(ngModel)]=\"fruitsDays[2]\" (ionChange)=\"isShowFruitsItem(3,$event)\"></ion-checkbox>\n                  <ion-label>Tuesday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow3.includes(3)\"  style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"fruitsTime[2]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\" ></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\">\n                  <ion-checkbox  mode=\"md\" [(ngModel)]=\"fruitsDays[3]\" (ionChange)=\"isShowFruitsItem(4,$event)\"></ion-checkbox>\n                  <ion-label>Wednesday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow3.includes(4)\"  style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"fruitsTime[3]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\" >\n                  <ion-checkbox  mode=\"md\" [(ngModel)]=\"fruitsDays[4]\" (ionChange)=\"isShowFruitsItem(5,$event)\"></ion-checkbox>\n                  <ion-label>Thursday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow3.includes(5)\"  style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"fruitsTime[4]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\" >\n                  <ion-checkbox  mode=\"md\" [(ngModel)]=\"fruitsDays[5]\" (ionChange)=\"isShowFruitsItem(6,$event)\"></ion-checkbox>\n                  <ion-label>Friday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow3.includes(6)\"  style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"fruitsTime[5]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\" >\n                  <ion-checkbox mode=\"md\"  [(ngModel)]=\"fruitsDays[6]\" (ionChange)=\"isShowFruitsItem(7,$event)\"></ion-checkbox>\n                  <ion-label>Saturday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow3.includes(7)\"   style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"fruitsTime[6]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                         \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n    </div>\n -->\n\n\n\n    <!-- <ion-row style=\"margin-bottom: .5rem;\">\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:1.1rem;color:#01A3A4;\">\n        <b>Craving for milk tea or coffee</b>\n      </ion-col>\n    </ion-row>\n   \n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-list lines=\"none\">\n          <ion-radio-group mode=\"md\" value=\"cooking\" [(ngModel)]=\"milkTea\">\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"3\" ></ion-radio>\n              <ion-label>Vegetable juice</ion-label>\n            </ion-item>\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"2\"></ion-radio>\n              <ion-label>Need Tea</ion-label>\n            </ion-item>\n\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"1\"></ion-radio>\n              <ion-label>Need Coffee</ion-label>\n            </ion-item>\n\n         \n\n          </ion-radio-group>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n     -->\n    <!-- <ion-row style=\"margin-bottom: .5rem;\">\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:1.1rem;color:#01A3A4;\">\n        <b>Desserts craving</b>\n      </ion-col>\n    </ion-row>\n    \n    <div>\n      <ion-row>\n        <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:.9rem;color:#000000;\">\n          <b>Preferred days of Deserts </b><span style=\"font-size:.8rem;\">(upto {{cheatLength}}\n            days)</span><br><br>\n        </ion-col>\n      </ion-row>\n    \n      <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\" >\n                  <ion-checkbox [disabled]=\"(isShow2.length==cheatLength) && !isShow2.includes(1)?true:false\" mode=\"md\" [(ngModel)]=\"desertDays[0]\" (ionChange)=\"isShowDesertItem(1,$event)\"></ion-checkbox>\n                  <ion-label>Sunday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow2.includes(1)\" style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"desertTime[0]\">\n                        <ion-item lines=\"none\">\n                         \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                         \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n    \n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\">\n                  <ion-checkbox [disabled]=\"(isShow2.length==cheatLength) && !isShow2.includes(2)?true:false\" mode=\"md\" [(ngModel)]=\"desertDays[1]\" (ionChange)=\"isShowDesertItem(2,$event)\"></ion-checkbox>\n                  <ion-label>Monday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow2.includes(2)\" style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"desertTime[1]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\" >\n                  <ion-checkbox [disabled]=\"(isShow2.length==cheatLength) && !isShow2.includes(3)?true:false\" mode=\"md\" [(ngModel)]=\"desertDays[2]\" (ionChange)=\"isShowDesertItem(3,$event)\"></ion-checkbox>\n                  <ion-label>Tuesday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow2.includes(3)\"  style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"desertTime[2]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\" ></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\">\n                  <ion-checkbox [disabled]=\"(isShow2.length==cheatLength) && !isShow2.includes(4) ?true:false\" mode=\"md\" [(ngModel)]=\"desertDays[3]\" (ionChange)=\"isShowDesertItem(4,$event)\"></ion-checkbox>\n                  <ion-label>Wednesday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow2.includes(4)\"  style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"desertTime[3]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\" >\n                  <ion-checkbox [disabled]=\"isShow2.length==actualLength && !isShow2.includes(5)?true:false\" mode=\"md\" [(ngModel)]=\"desertDays[4]\" (ionChange)=\"isShowDesertItem(5,$event)\"></ion-checkbox>\n                  <ion-label>Thursday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow2.includes(5)\"  style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"desertTime[4]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\" >\n                  <ion-checkbox [disabled]=\"(isShow2.length==cheatLength) && !isShow2.includes(6)?true:false\" mode=\"md\" [(ngModel)]=\"desertDays[5]\" (ionChange)=\"isShowDesertItem(6,$event)\"></ion-checkbox>\n                  <ion-label>Friday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow2.includes(6)\"  style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"desertTime[5]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col class=\"ion-text-center\">\n            <ion-row>\n              <ion-col>\n                <ion-item lines=\"none\" >\n                  <ion-checkbox mode=\"md\" [disabled]=\"isShow2.length==actualLength && !isShow2.includes(7)?true:false\" [(ngModel)]=\"desertDays[6]\" (ionChange)=\"isShowDesertItem(7,$event)\"></ion-checkbox>\n                  <ion-label>Saturday</ion-label>\n                </ion-item>\n                \n                <div *ngIf=\"isShow2.includes(7)\"   style=\"margin-left:2.7rem\">\n                  <ion-row>\n                    <ion-col class=\"ion-text-left\">\n                      <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                    </ion-col>\n                  </ion-row>\n                  <ion-row>\n                    <ion-col>\n                      <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"desertTime[6]\">\n                        <ion-item lines=\"none\">\n                          \n                          <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                          <ion-label>Morning Mid Day</ion-label>\n                        </ion-item>\n    \n                        <ion-item lines=\"none\">\n                         \n                          <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                          <ion-label>Evening Mid Day</ion-label>\n                        </ion-item>\n    \n                      </ion-radio-group>\n                    </ion-col>\n                  </ion-row>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-col>\n        </ion-row>\n     \n    </div> -->\n\n\n\n\n\n    <!-- <ion-row style=\"margin-bottom: .5rem;\">\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:1.1rem;color:#01A3A4;\">\n        <b>Mid day meals</b>\n      </ion-col>\n    </ion-row>\n   \n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-list lines=\"none\">\n          <ion-radio-group mode=\"md\" value=\"cooking\" [(ngModel)]=\"midDayMeal\">\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"3\"></ion-radio>\n              <ion-label>Morning</ion-label>\n            </ion-item>\n\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"6\"></ion-radio>\n              <ion-label>Evening</ion-label>\n            </ion-item>\n\n            <ion-item>\n             \n              <ion-radio  color=\"primary\" value=\"3,6\"></ion-radio>\n              <ion-label>Both</ion-label>\n            </ion-item>\n         \n\n          </ion-radio-group>\n        </ion-list>\n      </ion-col>\n    </ion-row>\n     -->\n  \n    <div>\n      <ion-row>\n        <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:1.3rem;color:#01A3A4;\">\n          <b>Choose cheat days</b> <span style=\"font-size:.8rem;\">(upto {{cheatLength}}\n            days)</span>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <ion-row>\n            <ion-col>\n              <ion-item lines=\"none\" >\n                <ion-checkbox [disabled]=\"(isShow1.length==cheatLength) && !isShow1.includes(1)?true:false\" mode=\"md\" [(ngModel)]=\"cheatDays[0]\" (ionChange)=\"isShowCheatItem(1,$event)\"></ion-checkbox>\n                <ion-label>Sunday</ion-label>\n              </ion-item>\n              \n              <div *ngIf=\"isShow1.includes(1)\" style=\"margin-left:2.7rem\">\n                <ion-row>\n                  <ion-col class=\"ion-text-left\">\n                    <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"cheatTime[0]\">\n                      <ion-item lines=\"none\">\n                       \n                        <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                        <ion-label>Lunch</ion-label>\n                      </ion-item>\n  \n                      <ion-item lines=\"none\">\n                       \n                        <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                        <ion-label>Dinner</ion-label>\n                      </ion-item>\n  \n                    </ion-radio-group>\n                  </ion-col>\n                </ion-row>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n  \n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <ion-row>\n            <ion-col>\n              <ion-item lines=\"none\">\n                <ion-checkbox [disabled]=\"(isShow1.length==cheatLength) && !isShow1.includes(2)?true:false\" mode=\"md\" [(ngModel)]=\"cheatDays[1]\" (ionChange)=\"isShowCheatItem(2,$event)\"></ion-checkbox>\n                <ion-label>Monday</ion-label>\n              </ion-item>\n              \n              <div *ngIf=\"isShow1.includes(2)\" style=\"margin-left:2.7rem\">\n                <ion-row>\n                  <ion-col class=\"ion-text-left\">\n                    <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"cheatTime[1]\">\n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                        <ion-label>Lunch</ion-label>\n                      </ion-item>\n  \n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                        <ion-label>Dinner</ion-label>\n                      </ion-item>\n  \n                    </ion-radio-group>\n                  </ion-col>\n                </ion-row>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <ion-row>\n            <ion-col>\n              <ion-item lines=\"none\" >\n                <ion-checkbox [disabled]=\"(isShow1.length==cheatLength) && !isShow1.includes(3)?true:false\" mode=\"md\" [(ngModel)]=\"cheatDays[2]\" (ionChange)=\"isShowCheatItem(3,$event)\"></ion-checkbox>\n                <ion-label>Tuesday</ion-label>\n              </ion-item>\n              \n              <div *ngIf=\"isShow1.includes(3)\"  style=\"margin-left:2.7rem\">\n                <ion-row>\n                  <ion-col class=\"ion-text-left\">\n                    <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"cheatTime[2]\">\n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Lunch\" ></ion-radio>\n                        <ion-label>Lunch</ion-label>\n                      </ion-item>\n  \n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                        <ion-label>Dinner</ion-label>\n                      </ion-item>\n  \n                    </ion-radio-group>\n                  </ion-col>\n                </ion-row>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <ion-row>\n            <ion-col>\n              <ion-item lines=\"none\">\n                <ion-checkbox [disabled]=\"(isShow1.length==cheatLength) && !isShow1.includes(4) ?true:false\" mode=\"md\" [(ngModel)]=\"cheatDays[3]\" (ionChange)=\"isShowCheatItem(4,$event)\"></ion-checkbox>\n                <ion-label>Wednesday</ion-label>\n              </ion-item>\n              \n              <div *ngIf=\"isShow1.includes(4)\"  style=\"margin-left:2.7rem\">\n                <ion-row>\n                  <ion-col class=\"ion-text-left\">\n                    <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"cheatTime[3]\">\n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                        <ion-label>Lunch</ion-label>\n                      </ion-item>\n  \n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                        <ion-label>Dinner</ion-label>\n                      </ion-item>\n  \n                    </ion-radio-group>\n                  </ion-col>\n                </ion-row>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <ion-row>\n            <ion-col>\n              <ion-item lines=\"none\" >\n                <ion-checkbox [disabled]=\"(isShow1.length==cheatLength) && !isShow1.includes(5)?true:false\" mode=\"md\" [(ngModel)]=\"cheatDays[4]\" (ionChange)=\"isShowCheatItem(5,$event)\"></ion-checkbox>\n                <ion-label>Thursday</ion-label>\n              </ion-item>\n              \n              <div *ngIf=\"isShow1.includes(5)\"  style=\"margin-left:2.7rem\">\n                <ion-row>\n                  <ion-col class=\"ion-text-left\">\n                    <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"cheatTime[4]\">\n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                        <ion-label>Lunch</ion-label>\n                      </ion-item>\n  \n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                        <ion-label>Dinner</ion-label>\n                      </ion-item>\n  \n                    </ion-radio-group>\n                  </ion-col>\n                </ion-row>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <ion-row>\n            <ion-col>\n              <ion-item lines=\"none\" >\n                <ion-checkbox [disabled]=\"(isShow1.length==cheatLength) && !isShow1.includes(6)?true:false\" mode=\"md\" [(ngModel)]=\"cheatDays[5]\" (ionChange)=\"isShowCheatItem(6,$event)\"></ion-checkbox>\n                <ion-label>Friday</ion-label>\n              </ion-item>\n              \n              <div *ngIf=\"isShow1.includes(6)\"  style=\"margin-left:2.7rem\">\n                <ion-row>\n                  <ion-col class=\"ion-text-left\">\n                    <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"cheatTime[5]\">\n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                        <ion-label>Lunch</ion-label>\n                      </ion-item>\n  \n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                        <ion-label>Dinner</ion-label>\n                      </ion-item>\n  \n                    </ion-radio-group>\n                  </ion-col>\n                </ion-row>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col class=\"ion-text-center\">\n          <ion-row>\n            <ion-col>\n              <ion-item lines=\"none\" >\n                <ion-checkbox mode=\"md\" [disabled]=\"(isShow1.length==cheatLength) && !isShow1.includes(7)?true:false\" [(ngModel)]=\"cheatDays[6]\" (ionChange)=\"isShowCheatItem(7,$event)\"></ion-checkbox>\n                <ion-label>Saturday</ion-label>\n              </ion-item>\n              \n              <div *ngIf=\"isShow1.includes(7)\"   style=\"margin-left:2.7rem\">\n                <ion-row>\n                  <ion-col class=\"ion-text-left\">\n                    <span style=\"font-size:.8rem;\" class=\"ion-text-left\">Choose Preferred timing</span>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <ion-radio-group mode=\"md\" value=\"lunch-dinner\" [(ngModel)]=\"cheatTime[6]\">\n                      <ion-item lines=\"none\">\n                        \n                        <ion-radio  color=\"primary\" value=\"Lunch\"></ion-radio>\n                        <ion-label>Lunch</ion-label>\n                      </ion-item>\n  \n                      <ion-item lines=\"none\">\n                       \n                        <ion-radio  color=\"primary\" value=\"Dinner\"></ion-radio>\n                        <ion-label>Dinner</ion-label>\n                      </ion-item>\n  \n                    </ion-radio-group>\n                  </ion-col>\n                </ion-row>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n      </ion-row>\n    </div>\n\n    <ion-row>\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size:1.1rem;color:#01A3A4;\"><br>\n        <b>Detox Day</b><br>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"detoxDays[0]\" [disabled]=\"detoxLimit==checkedDetox && detoxDays[0]===false\" (ionChange)=\"onCheckedDetox($event)\"></ion-checkbox>\n              <ion-label>Sunday</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"detoxDays[1]\" [disabled]=\"detoxLimit==checkedDetox && detoxDays[1]===false\" (ionChange)=\"onCheckedDetox($event)\"></ion-checkbox>\n              <ion-label>Monday</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"detoxDays[2]\" [disabled]=\"detoxLimit==checkedDetox && detoxDays[2]==false\" (ionChange)=\"onCheckedDetox($event)\"></ion-checkbox>\n              <ion-label>Tuesday</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"detoxDays[3]\" [disabled]=\"detoxLimit==checkedDetox && detoxDays[3]==false\" (ionChange)=\"onCheckedDetox($event)\"></ion-checkbox>\n              <ion-label>Wednesday</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"detoxDays[4]\" [disabled]=\"detoxLimit==checkedDetox && detoxDays[4]==false\" (ionChange)=\"onCheckedDetox($event)\"></ion-checkbox>\n              <ion-label>Thursday</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\"> \n              <ion-checkbox mode=\"md\" [(ngModel)]=\"detoxDays[5]\" [disabled]=\"detoxLimit==checkedDetox && detoxDays[5]==false\" (click)=\"onCheckedDetox($event)\"></ion-checkbox>\n              <ion-label>Friday</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"detoxDays[6]\" [disabled]=\"detoxLimit==checkedDetox && detoxDays[6]==false\"  (click)=\"onCheckedDetox($event)\"></ion-checkbox>\n              <ion-label>Saturday</ion-label>\n            </ion-item>\n          </ion-col>\n\n        </ion-row>\n      </ion-col>\n    </ion-row>\n    \n  </ion-card>\n  <br>\n  <ion-card>\n    <ion-card-header style=\"margin-left:1rem;font-size:1.3rem;color:#01A3A4;\">Which Slot to be remove</ion-card-header>\n    <ion-card-content style=\"background: #fff;\">\n      <ion-list>\n        <ion-item>\n          <ion-select [(ngModel)]=\"removedSlot\" aria-label=\"slots\" placeholder=\"Select slots to remove\" [multiple]=\"true\" (ionChange)=\"selectedSlotForRemove($event)\">\n            <ion-select-option value=\"{{diet?.value}}\" *ngFor=\"let diet of dietData1\">{{diet?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n\n  <br>\n  <ion-row style=\"margin-left: 1rem;\">\n    <ion-col>\n      <b style=\"font-size: 1.3rem;color:#01A3A4; font-weight: bold;\">Special adjustments</b>\n    </ion-col>\n  </ion-row>\n  <br>\n  <ion-row style=\"margin-left: 1rem;\">\n    <ion-col>\n      <b style=\"margin-left: 1rem; font-size: .9rem; color: #000000;\">Which NUTS to be added in which slot -</b>\n    </ion-col>\n  </ion-row>\n  <ion-card>\n    <ion-card-header style=\"font-size:1.2rem;color:#000000\">\n      <ion-list>\n        <ion-item>\n          <ion-select [(ngModel)]=\"slot1\" aria-label=\"slots\" placeholder=\"Select slot\" [multiple]=\"false\" (ionChange)=\"selecteSlot1($event)\">\n            <ion-select-option value=\"{{diet?.value}}\" *ngFor=\"let diet of dietData1\">{{diet?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-header>\n    <ion-card-content style=\"background: #fff;\">\n      <ion-list>\n        <ion-item>\n          <ion-select [disabled]=\"slot1===''\" [(ngModel)]=\"defaultslot1\" aria-label=\"slots\" placeholder=\"Select item\" [multiple]=\"true\" (ionChange)=\"selectedNutsinSlot1($event)\">\n            <ion-select-option *ngFor=\"let nut of allnuts[0]?.value\" selected=\"true\"  value=\"{{nut?.code}}\">{{nut?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n\n  <br>\n  <ion-row style=\"margin-left: 1rem;\">\n    <ion-col>\n      <b style=\"margin-left: 1rem; font-size: .9rem; color: #000000;font-weight: bold;\">Which SEEDS to be added in which slot -</b>\n    </ion-col>\n  </ion-row>\n  <ion-card>\n    <ion-card-header style=\"font-size:1.2rem;color:#000000\">\n      <ion-list>\n        <ion-item>\n          <ion-select [(ngModel)]=\"slot2\" aria-label=\"slots\" placeholder=\"Select slot\" [multiple]=\"false\" (ionChange)=\"selecteSlot2($event)\">\n            <ion-select-option value=\"{{diet?.value}}\" *ngFor=\"let diet of dietData1\">{{diet?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-header>\n    <ion-card-content style=\"background: #fff;\">\n      <ion-list>\n        <ion-item>\n          <ion-select [disabled]=\"slot2===''\" [(ngModel)]=\"defaultslot2\" aria-label=\"slots\" placeholder=\"Select Item\" [multiple]=\"true\" (ionChange)=\"selectedNutsinSlot2($event)\">\n            <ion-select-option *ngFor=\"let nut of allnuts[1]?.value\" value=\"{{nut?.code}}\">{{nut?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n\n  <br>\n  <ion-row style=\"margin-left: 1rem;\">\n    <ion-col>\n      <b style=\"margin-left: 1rem; font-size: .9rem; color: #000000;font-weight: bold;\">Which activated drink to be given early morning</b>\n    </ion-col>\n  </ion-row>\n  <ion-card>\n    <ion-card-header style=\"font-size:1.2rem;color:#000000\">\n      <ion-list>\n        <ion-item>\n          <ion-select [(ngModel)]=\"slot3\" aria-label=\"slots\" placeholder=\"Select slot\" [multiple]=\"false\" (ionChange)=\"selecteSlot3($event)\">\n            <ion-select-option value=\"0\">{{dietData[0]?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-header>\n    <ion-card-content style=\"background: #fff;\">\n      <ion-list>\n        <ion-item>\n          <ion-select [disabled]=\"slot3===''\" [(ngModel)]=\"defaultslot3\" aria-label=\"slots\" placeholder=\"Select item\" [multiple]=\"true\" (ionChange)=\"selectedNutsinSlot3($event)\">\n            <ion-select-option *ngFor=\"let nut of allnuts[2]?.value\" value=\"{{nut?.code}}\">{{nut?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n\n  <br>\n  <ion-row style=\"margin-left: 1rem;\">\n    <ion-col>\n      <b style=\"margin-left: 1rem; font-size: .9rem; color: #000000;font-weight: bold;\">Which Tea/Coffee to be added in which slot -</b>\n    </ion-col>\n  </ion-row>\n  <ion-card>\n    <ion-card-header style=\"font-size:1.2rem;color:#000000\">\n      <ion-list>\n        <ion-item>\n          <ion-select [(ngModel)]=\"slot4\" aria-label=\"slots\" placeholder=\"Select slot\" [multiple]=\"true\" (ionChange)=\"selecteSlot4($event)\">\n            <ion-select-option value=\"{{diet?.value}}\" *ngFor=\"let diet of dietData1\">{{diet?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-header>\n    <ion-card-content style=\"background: #fff;\">\n      <ion-list>\n        <ion-item>\n          <ion-select [disabled]=\"slot4===''\" [(ngModel)]=\"defaultslot4\" aria-label=\"slots\" placeholder=\"Select Item\" [multiple]=\"true\" (ionChange)=\"selectedNutsinSlot4($event)\">\n            <ion-select-option *ngFor=\"let nut of allnuts[3]?.value\" value=\"{{nut?.code}}\">{{nut?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <br>\n  <ion-row style=\"margin-left: 1rem;\">\n    <ion-col>\n      <b style=\"margin-left: 1rem; font-size: .9rem; color: #000000;font-weight: bold;\">Which Rice/Chapati to be updated in lunch - -</b>\n    </ion-col>\n  </ion-row>\n  <ion-card>\n    <ion-card-header style=\"font-size:1.2rem;color:#000000\">\n      <ion-list>\n        <ion-item>\n          <ion-select [(ngModel)]=\"slot5\" aria-label=\"slots\" placeholder=\"Select slot\" [multiple]=\"true\" (ionChange)=\"selecteSlot5($event)\">\n            <ion-select-option value=\"{{diet.value}}\" *ngFor=\"let diet of dietData2\">{{diet?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-header>\n    <ion-card-content style=\"background: #fff;\">\n      <ion-list>\n        <ion-item>\n          <ion-select [disabled]=\"slot5===''\" [(ngModel)]=\"defaultslot5\" aria-label=\"slots\" placeholder=\"Select Item\" [multiple]=\"true\" (ionChange)=\"selectedNutsinSlot5($event)\">\n            <ion-select-option *ngFor=\"let nut of allnuts[4]?.value\" value=\"{{nut?.code}}\">{{nut?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n\n  <br>\n  <ion-row style=\"margin-left: 1rem;\">\n    <ion-col>\n      <b style=\"margin-left: 1rem; font-size: .9rem; color: #000000;font-weight: bold;\">Which drinks to recommend for better sleep</b>\n    </ion-col>\n  </ion-row>\n  <ion-card>\n    <ion-card-header style=\"font-size:1.2rem;color:#000000\">\n      <ion-list>\n        <ion-item>\n          <ion-select [(ngModel)]=\"slot6\" aria-label=\"slots\" placeholder=\"Select slot\" [multiple]=\"false\" (ionChange)=\"selecteSlot6($event)\">\n            <ion-select-option value=\"8\" >{{dietData[8]?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-header>\n    <ion-card-content style=\"background: #fff;\">\n      <ion-list>\n        <ion-item>\n          <ion-select [disabled]=\"slot6===''\" [(ngModel)]=\"defaultslot6\" aria-label=\"slots\" placeholder=\"Select Item\" [multiple]=\"true\" (ionChange)=\"selectedNutsinSlot6($event)\">\n            <ion-select-option *ngFor=\"let nut of allnuts[5]?.value\" value=\"{{nut?.code}}\">{{nut?.name}}</ion-select-option>\n          </ion-select>\n        </ion-item>\n      </ion-list>\n    </ion-card-content>\n  </ion-card>\n  <br>\n  <ion-card>\n    <ion-row>\n      <ion-col class=\"ion-text-left\" style=\"margin-left:1rem;font-size: 1.3rem;color:#01A3A4;\"><br>\n        <b>Dinner Preferences</b><br>\n      </ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col class=\"ion-text-center\">\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"dinnerPref[0]\"  (ionChange)=\"onCheckedDinnerPref($event,{itm:'A'})\"></ion-checkbox>\n              <ion-label>Sabzi</ion-label>\n            </ion-item>\n          </ion-col>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"dinnerPref[1]\"  (ionChange)=\"onCheckedDinnerPref($event,{itm:'C'})\"></ion-checkbox>\n              <ion-label>Curry</ion-label>\n            </ion-item>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col>\n            <ion-item lines=\"none\">\n              <ion-checkbox mode=\"md\" [(ngModel)]=\"dinnerPref[2]\"  (ionChange)=\"onCheckedDinnerPref($event,{itm:'W'})\"></ion-checkbox>\n              <ion-label>Snack</ion-label>\n            </ion-item>\n          </ion-col>\n         \n        </ion-row>\n    \n      </ion-col>\n    </ion-row>\n  </ion-card>\n\n<ion-row style=\"margin: 0 1.5rem;\">\n  <ion-col size=\"5.5\" class=\"ion-text-center\">\n    <ion-button class=\"button\" shape=\"round\"  expand=\"block\" class=\"button button-full button-outline\" (click)=\"goback()\">Cancel</ion-button>\n  </ion-col>\n  <ion-col size=\"1\"></ion-col>\n  <ion-col size=\"5.5\" class=\"ion-text-center\">\n    <ion-button class=\"button button-full\" expand=\"block\" shape=\"round\" (click)=\"updatePreferences()\" color=\"primary\">Save</ion-button>\n  </ion-col>\n</ion-row>\n\n</ion-content>");

/***/ }),

/***/ 75404:
/*!***********************************************************!*\
  !*** ./src/app/preference-diet/preference-diet.page.scss ***!
  \***********************************************************/
/***/ ((module) => {

module.exports = "ion-content ion-card {\n  margin: 0;\n  padding: 1rem;\n  box-shadow: none;\n  background: #FAFAFA;\n  --border-color: #F4F4F4;\n}\nion-content ion-card ion-item {\n  --background:#FAFAFA;\n}\nion-content ion-card ion-item ion-checkbox {\n  margin: 0.5rem 0.5rem 0.5rem 0;\n}\nion-content .diet-name {\n  float: left;\n}\nion-content ion-radio {\n  margin-right: 0.5rem;\n}\nion-content ion-radio, ion-content ion-checkbox {\n  --color: #1EABAC;\n  --border-color:#1EABAC;\n}\n.sc-ion-alert-md-h {\n  --min-width: 400px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByZWZlcmVuY2UtZGlldC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0c7RUFDSSxTQUFBO0VBQ0gsYUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtBQUFKO0FBQ1E7RUFDQSxvQkFBQTtBQUNSO0FBRVE7RUFDSSw4QkFBQTtBQUFaO0FBSUc7RUFDQyxXQUFBO0FBRko7QUFJRztFQUNJLG9CQUFBO0FBRlA7QUFJRztFQUNJLGdCQUFBO0VBQ0Esc0JBQUE7QUFGUDtBQU9BO0VBQ0ksNkJBQUE7QUFKSiIsImZpbGUiOiJwcmVmZXJlbmNlLWRpZXQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XG4gICBpb24tY2FyZHtcbiAgICAgICBtYXJnaW46IDA7XG4gICAgcGFkZGluZzogMXJlbTtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICAgIGJhY2tncm91bmQ6ICNGQUZBRkE7XG4gICAgLS1ib3JkZXItY29sb3I6ICNGNEY0RjQ7XG4gICAgICAgIGlvbi1pdGVte1xuICAgICAgICAtLWJhY2tncm91bmQ6I0ZBRkFGQTtcbiAgICAgICAgfVxuICAgIGlvbi1pdGVte1xuICAgICAgICBpb24tY2hlY2tib3h7XG4gICAgICAgICAgICBtYXJnaW46IC41cmVtIC41cmVtIC41cmVtIDA7XG4gICAgICAgIH1cbiAgICB9XG4gICB9XG4gICAuZGlldC1uYW1le1xuICAgIGZsb2F0OiBsZWZ0O1xuICAgfVxuICAgaW9uLXJhZGlve1xuICAgICAgIG1hcmdpbi1yaWdodDogLjVyZW07XG4gICB9XG4gICBpb24tcmFkaW8saW9uLWNoZWNrYm94e1xuICAgICAgIC0tY29sb3I6ICMxRUFCQUM7XG4gICAgICAgLS1ib3JkZXItY29sb3I6IzFFQUJBQztcbiAgIH1cbiAgXG59XG5cbi5zYy1pb24tYWxlcnQtbWQtaCB7XG4gICAgLS1taW4td2lkdGg6IDQwMHB4ICFpbXBvcnRhbnQ7XG4gICAgfSJdfQ== */";

/***/ }),

/***/ 69245:
/*!********************************************!*\
  !*** ./src/app/shared/constants/nuts.json ***!
  \********************************************/
/***/ ((module) => {

module.exports = JSON.parse('{"nuts":[{"name":"N","value":[{"code":"382","name":"Overnight Soaked Walnuts"},{"code":"383","name":"Over Night Soaked Almonds"},{"code":"384","name":"Cashews -Plain/ Roasted(Non Salty/Non Fried)"},{"code":"425","name":"Handful Almonds / Walnuts/ Cashews"}]},{"name":"NS","value":[{"code":"385","name":"Handful Seeds mix (flax seeds/pumpkin/sunflower )"},{"code":"1190","name":"Pumpkin Seed"},{"code":"1191","name":"Flaxseeds"},{"code":"1192","name":"Sunflower Seeds"},{"code":"1595","name":"Chia seeds"}]},{"name":"DZ","value":[{"code":"154","name":"Overnight Soaked Methi Wate"},{"code":"155","name":"Dhaniya Seeds Water/ Coriander Seeds Water"},{"code":"156","name":"Lukewarm Water With Lemon"},{"code":"157","name":"Jira Water/ Jeera/ Cumin Seeds Water"},{"code":"158","name":"Cucumber And Lime Water"},{"code":"161","name":"Ajwain Water/ Carom Seeds Water"},{"code":"171","name":"Lemon Tea"},{"code":"172","name":"Ginger Turmeric Tea"},{"code":"376","name":"Ginger Cinnamon Tea"},{"code":"424","name":"Apple Cider Vinegar with Water"}]},{"name":"DM","value":[{"code":"060","name":"Tea With Milk And 1 Tsp Sugar"},{"code":"061","name":"Coffee With Milk & 1 Tsp Sugar (Unrefined)"},{"code":"063","name":"Filter Coffee"},{"code":"407","name":"Tea With Milk and Stevia"},{"code":"408","name":"Coffee with Milk and Stevia"},{"code":"1054","name":"Coffee with milk, without sugar"},{"code":"1635","name":"Tea with skimmed milk without sugar"},{"code":"064","name":"Black tea"},{"code":"062","name":"Black coffee"},{"code":"1636","name":"Tea with low fat milk, with 1 tsp sugar"}]},{"name":"B","value":[{"code":"005","name":"Boiled Rice/ White/ Plain Rice"},{"code":"007","name":"Whole Wheat Bread"},{"code":"008","name":"Whole Wheat Chapati/ Roti"},{"code":"009","name":"Brown Rice"},{"code":"077","name":"Jowar Bakhri/ Sorghum Roti"},{"code":"087","name":"Methi Thepla"},{"code":"216","name":"Missi Roti"},{"code":"219","name":"Multigrain Roti"},{"code":"301","name":"Steamed Vegetable Rice"},{"code":"390","name":"Jowar/Bajra Roti"},{"code":"405","name":"Makki Ki Roti/ Maize Flour Chapati"}]},{"name":"DZ_1","value":[{"code":"155","name":"Dhaniya Seeds Water/ Coriander Seeds Water"},{"code":"157","name":"Jira Water/ Jeera/ Cumin Seeds Water"},{"code":"161","name":"Ajwain Water/ Carom Seeds Water"},{"code":"172","name":"Ginger Turmeric Tea"},{"code":"375","name":"Mint tea"}]}]}');

/***/ })

}]);
//# sourceMappingURL=src_app_preference-diet_preference-diet_module_ts.js.map