"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_new-profile_new-profile_module_ts"],{

/***/ 93034:
/*!***********************************************************!*\
  !*** ./src/app/new-profile/new-profile-routing.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewProfilePageRoutingModule": () => (/* binding */ NewProfilePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _new_profile_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./new-profile.page */ 88395);




const routes = [
    {
        path: '',
        component: _new_profile_page__WEBPACK_IMPORTED_MODULE_0__.NewProfilePage
    }
];
let NewProfilePageRoutingModule = class NewProfilePageRoutingModule {
};
NewProfilePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], NewProfilePageRoutingModule);



/***/ }),

/***/ 34719:
/*!***************************************************!*\
  !*** ./src/app/new-profile/new-profile.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewProfilePageModule": () => (/* binding */ NewProfilePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 28267);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 18346);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _new_profile_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./new-profile-routing.module */ 93034);
/* harmony import */ var _new_profile_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./new-profile.page */ 88395);







let NewProfilePageModule = class NewProfilePageModule {
};
NewProfilePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _new_profile_routing_module__WEBPACK_IMPORTED_MODULE_0__.NewProfilePageRoutingModule
        ],
        declarations: [_new_profile_page__WEBPACK_IMPORTED_MODULE_1__.NewProfilePage]
    })
], NewProfilePageModule);



/***/ }),

/***/ 88395:
/*!*************************************************!*\
  !*** ./src/app/new-profile/new-profile.page.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NewProfilePage": () => (/* binding */ NewProfilePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 98806);
/* harmony import */ var _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_new_profile_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./new-profile.page.html */ 57278);
/* harmony import */ var _new_profile_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./new-profile.page.scss */ 27189);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 14001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 78099);
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ 78713);
/* harmony import */ var _app_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../app.service */ 38198);
/* harmony import */ var _services_app_app_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/app/app.service */ 61535);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 13252);
/* harmony import */ var _core_utility_utilities__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../core/utility/utilities */ 53533);










let NewProfilePage = class NewProfilePage {
    constructor(modalCtrl, storage, appservice, appService, router, utilities) {
        this.modalCtrl = modalCtrl;
        this.storage = storage;
        this.appservice = appservice;
        this.appService = appService;
        this.router = router;
        this.utilities = utilities;
        this.segment = "profile";
        this.profileData = {};
        this.localData = {};
        this.suggestedWeightRange = 0;
        this.additionalPref1 = { NonVegLunch: [], nonVegDinner: [], cheatDaysLunch: [], cheatDaysDinner: [], notNonEggDay: [], notNonVegDay: [],
            midDayMeals: [],
            detoxDay: [], coffeeLiking: true, teaLiking: true, fruitsLiking: true
        };
        this.getPreferences();
    }
    ngOnInit() {
    }
    ngAfterViewInit() {
        this.getProfile();
        this.checkPlan();
        this.gotoDemographic();
    }
    gotoDemographic() {
        this.storage.get("localData").then((local) => {
            var _a;
            const data = this.utilities.parseJSON(local);
            if (((_a = data.otherMaster.height[0]) === null || _a === void 0 ? void 0 : _a.param) == "in") {
                this.suggestedWeightRange = Math.ceil(parseInt(data.otherMaster.height[0].value) * 2.54 - 100);
            }
            else if (data.otherMaster.height[0].unit == "cm") {
                this.suggestedWeightRange = Math.ceil(parseInt(data.otherMaster.height[0].value) - 100);
            }
        });
    }
    returnWeek() {
        var _a, _b;
        //let createdDate = new Date("9-25-2022"); // for month view
        let createdDate = new Date((_b = (_a = this.profileData) === null || _a === void 0 ? void 0 : _a.profile) === null || _b === void 0 ? void 0 : _b.createdDate);
        const diff = new Date().valueOf() - (createdDate === null || createdDate === void 0 ? void 0 : createdDate.valueOf());
        const seconds = Math.floor(diff / 1000);
        interval = seconds / 2592000;
        if (interval > 2) {
            return {
                type: "M",
                count: Math.floor(interval),
            };
        }
        var interval = seconds / 604800;
        if (interval > 1) {
            return {
                type: "W",
                count: Math.floor(interval),
            };
        }
        else {
            return {
                type: "W",
                count: 0,
            };
        }
    }
    checkPlan() {
        this.appservice.getOnePlan().then(res => {
            this.plandata = res;
            let exp = new Date(this.plandata.planExpiryDate).getTime();
            let newDate = new Date().getTime();
            console.log(exp, newDate);
            if (exp > newDate) {
                this.plandata.isPlanActive = true;
            }
            console.log("plan==========>", res);
        });
    }
    getFlag(type) {
        switch (type) {
            case "IND":
                return "india-flag.png";
            case "USA":
                return "USA-flag.png";
            case "CAN":
                return "canada-flag.png";
            case "AUS":
                return "Australia-flag.png";
            case "EGY":
                return "Egypt-flag.png";
        }
    }
    modalClose() {
        this.router.navigate(['deitician-search']);
    }
    getPreferences() {
        const payload = {
            email: localStorage.getItem("email")
        };
        this.appService.getPreferences(payload).subscribe((res) => {
            if ((res === null || res === void 0 ? void 0 : res.additionalPref) != undefined) {
                this.additionalPref1 = res === null || res === void 0 ? void 0 : res.additionalPref;
                console.log("getPreferences", this.additionalPref1);
            }
        });
    }
    getImage(type) {
        console.log(type);
        switch (type) {
            case "AC1":
                return "sedentary.png";
            case "AC2":
                return "lightly-active.png";
            case "AC3":
                return "moderatly-active.png";
            case "AC4":
                return "super-active.png";
            case "AC5":
                return "extrimaly-active.png";
            case "weightLoss":
                return "weightmanagement.png";
            case "maintenance":
                return "weightmanagement.png";
            case "muscleGain":
                return "fitness.png";
            case "fatShredding":
                return "fitness.png";
            case "diseseManagement":
                return "deseasemanagement.png";
            case "V":
                return "broccoli@3x.png";
            case "NV":
                return "meat@3x.png";
            case "E":
                return "brocolly-egg.png";
            case "Ve":
                return "leaf@3x.png";
            default:
                return "weightmanagement.png";
        }
    }
    openModel(component) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.router.navigate([component], { queryParams: { from: 'editProfile' } });
            this.getProfile();
        });
    }
    getProfile() {
        this.profileData = [];
        this.appservice.getProfile().then((res) => {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p, _q, _r, _s, _t, _u, _v;
            this.profileData = res;
            if (((_b = (_a = this.profileData) === null || _a === void 0 ? void 0 : _a.profile) === null || _b === void 0 ? void 0 : _b.subCategory) === "weightloss") {
                this.profileData.profile.subCategory = "Weight Loss";
            }
            if (((_d = (_c = this.profileData) === null || _c === void 0 ? void 0 : _c.profile) === null || _d === void 0 ? void 0 : _d.subCategory) === "weightmaintenance") {
                this.profileData.profile.subCategory = "Weight Maintenance";
            }
            if (((_f = (_e = this.profileData) === null || _e === void 0 ? void 0 : _e.profile) === null || _f === void 0 ? void 0 : _f.subCategory) === "musclebuilding") {
                this.profileData.profile.subCategory = "Muscle Building";
            }
            if (((_h = (_g = this.profileData) === null || _g === void 0 ? void 0 : _g.profile) === null || _h === void 0 ? void 0 : _h.subCategory) === "leanbody") {
                this.profileData.profile.subCategory = "Lean Body";
            }
            this.storage.get("localData").then((val) => {
                var _a, _b, _c, _d, _e, _f, _g, _h, _j;
                this.localData = JSON.parse(val);
                console.log(this.localData);
                this.profileData.lifeStyle.foodType = (_b = (_a = this.localData) === null || _a === void 0 ? void 0 : _a.otherMaster) === null || _b === void 0 ? void 0 : _b.foodPref.find((f) => f.code === this.profileData.lifeStyle.foodType);
                this.profileData.lifeStyle.country = (_c = this.localData) === null || _c === void 0 ? void 0 : _c.countries.find((f) => f._id === this.profileData.lifeStyle.country);
                this.profileData.lifeStyle.activities = (_e = (_d = this.localData) === null || _d === void 0 ? void 0 : _d.otherMaster) === null || _e === void 0 ? void 0 : _e.activities.find((item) => { var _a, _b; return ((_b = (_a = this.profileData) === null || _a === void 0 ? void 0 : _a.lifeStyle) === null || _b === void 0 ? void 0 : _b.activities.code) === item.code; });
                this.profileData.lifeStyle.communities = (_g = (_f = this.localData) === null || _f === void 0 ? void 0 : _f.otherMaster) === null || _g === void 0 ? void 0 : _g.community.filter((item) => { var _a, _b, _c; return (_c = (_b = (_a = this.profileData) === null || _a === void 0 ? void 0 : _a.lifeStyle) === null || _b === void 0 ? void 0 : _b.communities) === null || _c === void 0 ? void 0 : _c.includes(item.code); });
                this.profileData.lifeStyle.diseases = (_j = (_h = this.localData) === null || _h === void 0 ? void 0 : _h.otherMaster) === null || _j === void 0 ? void 0 : _j.diseases.filter((item) => { var _a, _b, _c; return (_c = (_b = (_a = this.profileData) === null || _a === void 0 ? void 0 : _a.lifeStyle) === null || _b === void 0 ? void 0 : _b.diseases) === null || _c === void 0 ? void 0 : _c.includes(item.code); });
            });
            let h = ((_l = (_k = (_j = this.profileData) === null || _j === void 0 ? void 0 : _j.demographic) === null || _k === void 0 ? void 0 : _k.height) === null || _l === void 0 ? void 0 : _l.unit) === "in"
                ? ((_p = (_o = (_m = this.profileData) === null || _m === void 0 ? void 0 : _m.demographic) === null || _o === void 0 ? void 0 : _o.height) === null || _p === void 0 ? void 0 : _p.value) / 12
                : (_s = (_r = (_q = this.profileData) === null || _q === void 0 ? void 0 : _q.demographic) === null || _r === void 0 ? void 0 : _r.height) === null || _s === void 0 ? void 0 : _s.value;
            if (((_v = (_u = (_t = this.profileData) === null || _t === void 0 ? void 0 : _t.demographic) === null || _u === void 0 ? void 0 : _u.height) === null || _v === void 0 ? void 0 : _v.unit) === "in") {
                console.log(h);
                h = h.toString().split(".");
                console.log(h);
                const h1 = (h[1] / 0.0833333).toString().split("0")[0];
                console.log(h1);
                this.profileData.demographic.height.value = `${h[0]}' ${h1}"`;
            }
            else {
                this.profileData.demographic.height.value =
                    this.profileData.demographic.height.value + " cm";
            }
            console.log(this.profileData);
        });
    }
};
NewProfilePage.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.ModalController },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_7__.Storage },
    { type: _app_service__WEBPACK_IMPORTED_MODULE_2__.AppService },
    { type: _services_app_app_service__WEBPACK_IMPORTED_MODULE_3__.AppService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: _core_utility_utilities__WEBPACK_IMPORTED_MODULE_4__.UTILITIES }
];
NewProfilePage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: "app-new-profile",
        template: _Users_fakharealam_sup_project_node_modules_ngtools_webpack_src_loaders_direct_resource_js_new_profile_page_html__WEBPACK_IMPORTED_MODULE_0__["default"],
        styles: [_new_profile_page_scss__WEBPACK_IMPORTED_MODULE_1__]
    })
], NewProfilePage);



/***/ }),

/***/ 57278:
/*!******************************************************************************************************************!*\
  !*** ./node_modules/@ngtools/webpack/src/loaders/direct-resource.js!./src/app/new-profile/new-profile.page.html ***!
  \******************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!-- <ion-header>\n  <ion-toolbar>\n    <ion-title>new-profile</ion-title>\n  </ion-toolbar>\n</ion-header> -->\n\n<ion-content>\n  <div class=\"main_div\">\n   \n    <div class=\"bottom_shadow\"></div>\n    <div>\n     \n      <ion-segment [value]=\"segment\" [(ngModel)]=\"segment\">\n        <ion-segment-button value=\"profile\" class=\"profileBtn\" layout=\"icon-start\">\n          <ion-icon src=\"../../../../assets/newImages/user_icon_outline.svg\"></ion-icon>\n          <ion-label>My Profile</ion-label>\n        </ion-segment-button>\n        <!-- <ion-segment-button value=\"insight\" class=\"insightBtn\" layout=\"icon-start\">\n          <ion-icon src=\"'../../../../../../assets/newImages/insight_icon.svg\"></ion-icon>\n          <ion-label>My Insights</ion-label>\n        </ion-segment-button> -->\n      </ion-segment>\n      <div class=\"w_100 right\" style=\"    top: -3rem;\n      position: relative;\n      right: 1rem;\">\n        <ion-icon\n          class=\"close_modal_icon\"\n          name=\"close-circle\"\n          (click)=\"modalClose()\"\n        ></ion-icon>\n      </div>\n    </div>\n\n    <div *ngIf=\"segment === 'profile' \">\n      <div class=\"section-card\" style=\"margin-top: 40px\">\n        <div class=\"avatar_div\">\n          <ion-avatar class=\"avatar\" style=\"margin-top: -40px\">\n            <img src=\"https://ionicframework.com/docs/demos/api/avatar/avatar.svg\" />\n          </ion-avatar>\n        </div>\n        <div class=\"edit_icon_div\">\n          <!-- <ion-icon (click)=\"openModel('boarding2')\" class=\"edit_icon\"\n            src=\"../../../../assets/newImages/icon-edit.svg\"></ion-icon> -->\n        </div>\n\n        <p class=\"username\">{{profileData?.profile?.name}}</p>\n        <ion-grid class=\"w_100\">\n          <ion-row>\n            <ion-col size=\"5\">\n              <p class=\"commonText\">\n                Age:<span>{{profileData?.demographic?.age?.avg_age}}</span>\n              </p>\n              <p class=\"commonText\">\n                Height:<span>{{profileData?.demographic?.height?.value}} </span>\n              </p>\n            </ion-col>\n            <ion-col size=\"7\">\n              <p class=\"commonText\">\n                Gender:<span>{{profileData?.demographic?.gender?.gender}}\n                </span>\n              </p>\n              <p class=\"commonText\">\n                Starting Weight:<span>{{profileData?.demographic?.weight?.value}}\n                  {{profileData?.demographic?.weight?.unit ===\n                  'pound'?'lbs':'kg'}}</span>\n              </p>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div>\n\n      <div class=\"section-card left\" style=\"flex-direction: row; padding: 5px 15px\">\n        <div class=\"edit_icon_div\">\n          <!-- <ion-icon (click)=\"openModel('boarding')\" class=\"edit_icon\"\n            src=\"../../../../assets/newImages/icon-edit.svg\"></ion-icon> -->\n        </div>\n        <ion-img [src]=\"'../../../../assets/newImages/'+getImage(profileData?.profile?.category)\" class=\"fitness_image\">\n        </ion-img>\n        <p class=\"commonText upperCase\">\n          My Goal: <br /><span style=\"margin-left: 0%\">\n            <!-- {{profileData?.profile?.category}} with -->\n            {{profileData?.profile?.subCategory}}</span>\n        </p>\n      </div>\n\n      <div class=\"section-card\">\n        <p class=\"commonText\" style=\"color: var(--black); margin-top: 0%\">\n          <ion-icon class=\"calendar_icon\" src=\"../../../../assets/newImages/caender_icon.svg\"></ion-icon>\n          Your Fitness Journey\n        </p>\n\n        <div class=\"condition_card\" style=\"padding: 10px 15px 0px\" *ngIf=\"returnWeek()\">\n          <div class=\"slider_div\">\n            <div class=\"lines\"></div>\n            <!-- <div class=\"lines2\">\n              <div [ngStyle]=\"{'width':returnWeek()*12.5  }\"></div>\n            </div> -->\n            <div class=\"slider_page\">\n              <div *ngFor=\"let w of [1,2,3,4,5,6,7,8]\" style=\"width: 12.5%\">\n                <div class=\"center flex_col\">\n                  <p class=\"mar_0 ion-text-center week\" *ngIf=\"returnWeek().count >= w\">\n                    ✓\n                  </p>\n                  <ion-icon *ngIf=\"returnWeek().count === w-1\" class=\"man_icon\"\n                    src=\"../../../../assets/newImages/man_icon.svg\"></ion-icon>\n                  <p class=\"mar_0\" [ngClass]=\"returnWeek().count >= 1 ? 'page_Count' : 'page_Count_active' \"></p>\n                  <p class=\"week\">{{returnWeek().type}}{{w}}</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n\n        <div style=\"margin-top: 30px\">\n          <ion-grid class=\"pad_0\">\n            <ion-row>\n              <ion-col size=\"6\" style=\"padding-right: 5px\">\n                <div class=\"condition_card center h_100\">\n                  <div class=\"card_div h_100\">\n                    <ion-img src=\"../../../../assets/newImages/bmi.png\" class=\"card_image\"></ion-img>\n                    <p class=\"title\">Current BMI:</p>\n                    <p class=\"main_title\">{{profileData?.demographic?.bmi}}</p>\n                    <p class=\"sub_title\">Range 18-25 (Recommended)</p>\n                  </div>\n                </div>\n              </ion-col>\n              <ion-col size=\"6\" style=\"padding-left: 5px\">\n                <div class=\"condition_card h_100\">\n                  <div class=\"card_div\">\n                    <ion-img src=\"../../../../assets/newImages/sugg_cal_icon.png\" class=\"card_image\"></ion-img>\n                    <p class=\"title\" style=\"margin-top: 10px\">\n                      Suggested calories :<br />\n                    </p>\n                    <p class=\"main_title\">\n                      {{profileData?.lifeStyle?.calories}} Kcal/day\n                    </p>\n                  </div>\n                </div>\n              </ion-col>\n            </ion-row>\n          </ion-grid>\n        </div>\n\n        <div class=\"condition_card\" style=\"margin-top: 20px\">\n          <div class=\"card_div\">\n            <ion-img src=\"../../../../assets/newImages/weight_machine.png\" class=\"card_image\"></ion-img>\n            <ion-grid class=\"w_100 pad_0\">\n              <ion-row>\n                <ion-col size=\"6\">\n                  <p class=\"title\" style=\"margin-top: 10px\">Current weight:</p>\n                  <p class=\"main_title\">\n                    {{profileData?.demographic?.weight?.value}} kg\n                  </p>\n                </ion-col>\n                <ion-col size=\"6\">\n                  <p class=\"title\" style=\"margin-top: 10px\">Desired weight:</p>\n                  <p class=\"main_title\">\n                    {{profileData?.demographic?.suggestedWeight}} kg\n                  </p>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n            <p class=\"sub_title\">\n              Target/Ideal weight recommended: : {{suggestedWeightRange-2}}kg - :\n            {{suggestedWeightRange+2}}kg\n            </p>\n          </div>\n        </div>\n        <div class=\"condition_card\" style=\"margin-top: 20px\">\n          <div class=\"card_div\">\n            <ion-img src=\"../../../../assets/newImages/nutritional.png\" class=\"card_image\"></ion-img>\n            <p class=\"title\">\n              Nutritional <br />\n              Requirement <span class=\"sub_title\">(On daily basis)</span>\n            </p>\n            <div class=\"cal_calculation_div\">\n              <div class=\"cal_main_div\">\n                <div class=\"cal_col\">\n                  <ion-img src=\"../../../../assets/newImages/calorie.png\" class=\"card_cal_image\"></ion-img>\n                  <p class=\"cal_sub_title\">Fiber</p>\n                  <p class=\"cal_title\">{{profileData?.lifeStyle?.fiber}}g</p>\n                </div>\n                <div class=\"cal_col\">\n                  <ion-img src=\"../../../../assets/newImages/carbs.png\" class=\"card_cal_image\"></ion-img>\n                  <p class=\"cal_sub_title\">Carbs</p>\n                  <p class=\"cal_title\">{{profileData?.lifeStyle?.carb}}g</p>\n                </div>\n\n                <div class=\"cal_col\">\n                  <ion-img src=\"../../../../assets/newImages/protein.png\" class=\"card_cal_image\"></ion-img>\n                  <p class=\"cal_sub_title\">Protein</p>\n                  <p class=\"cal_title\">{{profileData?.lifeStyle?.protien}}g</p>\n                </div>\n\n                <div class=\"cal_col\">\n                  <ion-img src=\"../../../../assets/newImages/fats.png\" class=\"card_cal_image\"></ion-img>\n                  <p class=\"cal_sub_title\">Fats</p>\n                  <p class=\"cal_title\">{{profileData?.lifeStyle?.fat}}g</p>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n\n      <!-- <div class=\"section-card pad_0\">\n        <ion-grid class=\"w_100\">\n          <ion-row>\n            <ion-col size=\"6\">\n              <div class=\"center flex_col currunt_plan_div\">\n                <p class=\"main_title\">Current Plan</p>\n                <ion-icon class=\"star_icon\" src=\"../../../../assets/newImages/star_icon.svg\"></ion-icon>\n                <p class=\"main_title\">Free</p>\n                <p class=\"sub_title ion-text-center\">\n                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n                </p>\n              </div>\n            </ion-col>\n            <ion-col size=\"6\">\n              <div class=\"center flex_col h_100\">\n                <ion-icon class=\"star_icon\" src=\"../../../../assets/newImages/upgrade_plan_icon.svg\"></ion-icon>\n                <ion-button class=\"go_btn\" shape=\"round\">Upgrade plan</ion-button>\n                <p class=\"commonText\" style=\"color: var(--theme-color)\">\n                  See benefits\n                </p>\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </div> -->\n\n      <div class=\"section-card left\" style=\"flex-direction: row; padding: 5px 15px\">\n        <div class=\"edit_icon_div\">\n          <!-- <ion-icon (click)=\"openModel('boarding3')\" class=\"edit_icon\"\n            src=\"../../../../assets/newImages/icon-edit.svg\"></ion-icon> -->\n        </div>\n        <div style=\"width: 50\">\n          <ion-img [src]=\"'../../../../assets/newImages/'+getImage(profileData?.lifeStyle?.activities?.code)\"\n            class=\"Activity_image\"></ion-img>\n        </div>\n        <p class=\"commonText\">\n          Physical Activity: <br /><span\n            style=\"margin-left: 0%\">{{profileData?.lifeStyle?.activities?.value?.split('(')[0]}}</span>\n        </p>\n      </div>\n\n      <div class=\"section-card left flex_col\" style=\"padding: 5px 15px\">\n        <div class=\"edit_icon_div\">\n          <!-- <ion-icon (click)=\"openModel('boarding4')\" class=\"edit_icon\"\n            src=\"../../../../assets/newImages/icon-edit.svg\"></ion-icon> -->\n        </div>\n        <div class=\"left w_100\">\n          <div style=\"width: 50\">\n            <ion-img src=\"../../../../assets/newImages/healthCondition.png\" class=\"Activity_image\"></ion-img>\n          </div>\n          <p class=\"commonText\">\n            Health Conditions: <br />\n            <span style=\"margin-left: 0%\" *ngFor=\" let d of profileData?.lifeStyle?.diseases\">\n              <span style=\"margin-left: 0%\"\n                *ngIf=\"!d?.value?.includes('Allergy') && !d?.value?.includes('allergy') \">{{d.value}},{{' '}}</span>\n            </span>\n            <!-- <span>None</span> -->\n          </p>\n        </div>\n\n        <div class=\"left w_100\">\n          <div style=\"width: 50\">\n            <ion-img src=\"../../../../assets/newImages/alergies.png\" class=\"Activity_image\"></ion-img>\n          </div>\n          <p class=\"commonText\">\n            Allergies:<br /><span *ngFor=\" let d of profileData?.lifeStyle?.diseases\" style=\"margin-left: 0%\"><span\n                style=\"margin-left: 0%\"\n                *ngIf=\"d?.value?.includes('Allergy') || d?.value?.includes('allergy') \">{{d.value}},{{' '}}</span></span>\n          </p>\n        </div>\n      </div>\n\n\n      <div class=\"section-card left flex_col\" style=\"flex-direction: row; padding: 5px 15px\">\n        <div class=\"left w_100\">\n          <ion-img [src]=\"'../../../../assets/newImages/'+getImage(profileData?.lifeStyle?.foodType?.code)\"\n            class=\"Activity_image\"></ion-img>\n          <p class=\"commonText\">\n            Meal Preferences:<br /><span style=\"margin-left: 0%\">{{profileData?.lifeStyle?.foodType?.value}}</span>\n          </p>\n        </div>\n        <div class=\"left w_100\">\n          <div style=\"width: 50\">\n            <ion-img [src]=\"'../../../../assets/newImages/'+getFlag(profileData?.lifeStyle?.country?._id)\"\n              class=\"Activity_image\"></ion-img>\n          </div>\n          <p class=\"commonText\">\n            Regional Preferences in Food: <br /><span\n              style=\"margin-left: 0%\">{{profileData?.lifeStyle?.country?.name}},</span><span\n              *ngFor=\"let c of this.profileData?.lifeStyle?.communities\">{{c?.value}},</span>\n          </p>\n        </div>\n      </div>\n\n\n      <div class=\"section-card left flex_col\" style=\"flex-direction: row; padding: 5px 15px\">\n        <!-- <div class=\"left w_100\">\n          <ion-img [src]=\"'../../../../assets/newImages/'+getImage(profileData?.lifeStyle?.foodType?.code)\"\n            class=\"Activity_image\"></ion-img>\n          <p class=\"commonText\">\n            Preferences:<br />\n          </p>\n        </div> -->\n        <div class=\"w_100 section-pref\">\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Non Veg Lunch</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\" *ngFor=\"let item of additionalPref1?.NonVegLunch\">\n                {{item}}\n              </div>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Non Veg Dinner</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\" *ngFor=\"let item of additionalPref1?.nonVegDinner\">\n                {{item}}\n              </div>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Cheat Days Lunch</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\" *ngFor=\"let item of additionalPref1?.cheatDaysLunch\">\n                {{item}}\n              </div>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Cheat Days Dinner</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\" *ngFor=\"let item of additionalPref1?.cheatDaysDinner\">\n                {{item}}\n              </div>\n            </ion-col>\n          </ion-row>\n\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Nob Eating Egg Days</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\" *ngFor=\"let item of additionalPref1?.notNonEggDay\">\n                {{item}}\n              </div>\n            </ion-col>\n          </ion-row>\n\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Non Eating Non Veg Days</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\" *ngFor=\"let item of additionalPref1?.notNonVegDay\">\n                {{item}}\n              </div>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Mid Day Meals</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\">\n                {{additionalPref1?.midDayMeals?.length!=0?additionalPref1?.midDayMeals?.length>1?'Both':additionalPref1?.midDayMeals[0]=='3'?'Morning':'Evening':'No Item Available'}}\n              </div>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Detox Day</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\">\n                {{additionalPref1?.detoxDay[0]}}\n              </div>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Coffee Likes</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\">\n                {{additionalPref1?.coffeeLiking==true?'High':'Low'}}\n              </div>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Tea Likes</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\">\n                {{additionalPref1?.teaLiking==true?'High':'Low'}}\n              </div>\n            </ion-col>\n          </ion-row>\n          <ion-row style=\"margin-bottom: 1.2rem;\">\n            <ion-col size=\"5\">\n              <span>Fruits Like</span>\n            </ion-col>\n            <ion-col size=\"7\">\n              <div class=\"name-item\">\n                {{additionalPref1?.fruitsLiking==true?'High':'Low'}}\n              </div>\n            </ion-col>\n          </ion-row>\n\n\n\n\n\n\n\n\n\n\n\n\n\n        </div>\n      </div>\n\n\n     </div>\n\n    <div *ngIf=\"segment === 'insight' \">\n      <div class=\"center\">\n        <ion-icon class=\"pre_nxt_btn\" name=\"arrow-back-outline\"></ion-icon>\n        <p class=\"date\">Last Week</p>\n        <ion-icon class=\"pre_nxt_btn\" name=\"arrow-forward-outline\"></ion-icon>\n      </div>\n    </div>\n  </div>\n</ion-content>");

/***/ }),

/***/ 27189:
/*!***************************************************!*\
  !*** ./src/app/new-profile/new-profile.page.scss ***!
  \***************************************************/
/***/ ((module) => {

module.exports = "@font-face {\n  font-family: gt-america-standard-medium;\n  src: url(\"/assets/fonts/Roboto-Medium.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-regular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-bold;\n  src: url(\"/assets/fonts/Roboto-Bold.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-extendedMedium;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: gt-america-standard-light;\n  src: url(\"/assets/fonts/Roboto-Light.ttf\") format(\"TrueType\");\n}\n@font-face {\n  font-family: Source-Sans-Pro-ragular;\n  src: url(\"/assets/fonts/Roboto-Regular.ttf\") format(\"TrueType\");\n}\nion-content {\n  --background: #F6F7FC;\n}\n.main_div {\n  min-height: 100%;\n  background: var(--white);\n  border-radius: 40px 40px 0px 0px;\n  padding-bottom: 30px;\n  --background: #F6F7FC;\n}\n.main_div .w_100 {\n  width: 100%;\n  padding: 0.5rem;\n}\n.main_div .close_modal_icon {\n  right: 1rem;\n  position: absolute;\n  font-size: 2rem;\n}\n.main_div .name-item {\n  border: 1px solid #f2f2f2;\n  padding: 0.5rem;\n  background: #f2f2f2;\n  margin: 0.2rem;\n  width: 80px;\n  float: left;\n  border-radius: 5px;\n}\n.main_div .section-pref span {\n  font-size: 1rem;\n  color: #b3b3b3;\n}\n.profileBtn {\n  --background-checked:var(--white);\n  --background:var(--theme-newHeader);\n  --border-radius: 30px 30px 30px 0px;\n  text-transform: none;\n  font-size: var(--small-font);\n  color: var(--black);\n  font-family: var(--theme-newFont);\n}\n.profileBtn::part(indicator) {\n  display: none;\n}\n.profileBtn ion-icon {\n  font-size: 20px;\n}\n.insightBtn {\n  --background-checked:var(--white);\n  --background:var(--theme-newHeader);\n  --border-radius: 30px 30px 0px 30px;\n  text-transform: none;\n  font-size: var(--small-font);\n  color: var(--black);\n  font-family: var(--theme-newFont);\n}\n.insightBtn::part(indicator) {\n  display: none;\n}\n.insightBtn ion-icon {\n  font-size: 15px;\n}\nion-segment-button[aria-selected=true] {\n  border-radius: 30px 30px 0px 0px;\n  width: 50%;\n  font-size: var(--medium-font);\n}\nion-segment-button[aria-selected=true] ion-icon {\n  font-size: 25px;\n}\nion-segment {\n  height: 60px;\n  border-radius: 40px 40px 0px 0px;\n  --ripple-color:none !important;\n}\n.pre_nxt_btn {\n  color: var(--theme-color);\n}\n.date {\n  margin: 20px 30px;\n  font-size: var(--regularM-font);\n  font-family: var(--theme-newFont);\n  font-weight: 500;\n  color: var(--black);\n}\n.avatar_div {\n  width: 45px;\n  z-index: 1;\n}\n.avatar {\n  height: 50px;\n  width: 50px;\n  border: 2px solid var(--theme-color);\n}\n.section-card {\n  margin: 20px;\n  padding: 15px 20px;\n  box-shadow: var(--boxshadow);\n  border-radius: 10px;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  position: relative;\n}\n.edit_icon {\n  font-size: 40px;\n  color: var(--theme-color);\n}\n.edit_icon_div {\n  position: absolute;\n  right: 5px;\n  top: 5px;\n}\n.username {\n  font-size: var(--regularM-font);\n  font-family: var(--theme-newFont);\n  font-weight: 500;\n  color: var(--black);\n  margin-bottom: 0%;\n}\n.main_title {\n  font-size: var(--medium-font);\n  font-family: var(--theme-newFont);\n  font-weight: 500;\n  color: var(--black);\n  margin: 0%;\n}\n.commonText {\n  font-size: var(--regular-font);\n  font-family: var(--theme-newFont);\n  font-weight: 500;\n  color: var(--black-light);\n}\n.commonText span {\n  color: var(--black);\n  margin-left: 5px;\n}\n.fitness_image {\n  width: 90px;\n  object-fit: contain;\n  margin-right: 10px;\n}\n.calendar_icon {\n  color: var(--black-light);\n  margin-right: 5px;\n}\n.condition_card {\n  background: var(--lightGrey);\n  border: 1.5px solid var(--card-border);\n  border-radius: 10px;\n  padding: 20px;\n  flex-direction: column;\n}\n.slider_div {\n  width: 100%;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  position: relative;\n}\n.slider_page {\n  width: 100%;\n  display: flex;\n  justify-content: space-between;\n  align-items: flex-end;\n  padding-bottom: 13px;\n}\n.lines {\n  border: 1px solid #E3E3E3;\n  width: 100%;\n  position: absolute;\n}\n.lines2 {\n  width: 100%;\n  position: absolute;\n}\n.lines2 div {\n  border: 1px solid var(--black);\n}\n.page_Count {\n  height: 10px;\n  width: 10px;\n  border: 2px solid var(--white);\n  border-radius: 100%;\n  font-size: var(--xsmall-font);\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  color: var(--white);\n  font-weight: bold;\n  z-index: 10;\n  background: var(--theme-newHeader);\n  font-family: var(--theme-newFont);\n}\n.page_Count_active {\n  height: 10px;\n  width: 10px;\n  border: 2px solid var(--white);\n  border-radius: 100%;\n  font-size: var(--xsmall-font);\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  color: var(--theme-newHeader);\n  font-weight: bold;\n  z-index: 10;\n  background: #F2903C;\n  font-family: var(--theme-newFont);\n}\n.complete {\n  border: 2px solid var(--black) !important;\n}\n.week {\n  font-size: var(--xsmall-font);\n  font-family: var(--theme-newFont);\n  font-weight: 500;\n  color: var(--black-light);\n  margin: 0%;\n  margin-top: 5px;\n  margin-bottom: 0;\n}\n.man_icon {\n  font-size: 25px;\n  margin-bottom: 5px;\n}\n.line_icon {\n  color: var(--theme-color);\n  stroke: var(--theme-color);\n  font-size: 25px;\n  margin-top: 5px;\n}\n.card_div {\n  display: flex;\n  justify-content: center;\n  align-items: flex-start;\n  flex-direction: column;\n  position: relative;\n}\n.card_image {\n  height: 30px;\n  position: absolute;\n  right: -15px;\n  top: -30px;\n}\n.title {\n  font-size: var(--small-font);\n  font-family: var(--theme-newFont);\n  color: var(--black);\n  font-weight: 700;\n  margin: 0px;\n}\n.sub_title {\n  font-size: var(--xsmall-font);\n  font-family: var(--theme-newFont);\n  color: var(--black);\n  margin: 10px 0px 0px;\n  font-weight: 400;\n}\n.star_icon {\n  font-size: 30px;\n  margin-top: 10px;\n}\n.currunt_plan_div {\n  background: #FFF9F2;\n  border-radius: 8px;\n  padding: 10px;\n}\n.upperCase {\n  text-transform: capitalize;\n}\n.go_btn {\n  height: 40px;\n  margin-top: 10px;\n}\n.go_btn::part(native) {\n  --background: var(--theme-newButton);\n  text-transform: none;\n  box-shadow: var(--btnShaddow);\n  color: var(--white);\n  font-size: var(--small-font);\n  font-family: var(--theme-newFont);\n}\n.flag_img {\n  margin: 20px;\n}\n.flag_img::part(image) {\n  height: 40px;\n  width: 40px;\n}\n.Activity_image {\n  width: 65px;\n  object-fit: contain;\n  margin: 10px 20px 10px 10px;\n}\n.protien_progress {\n  position: absolute;\n  --progress-background:var(--black);\n  border-radius: 25px;\n  width: 100%;\n  margin: 2px 0px;\n}\n.cal_calculation_div {\n  width: 100%;\n}\n.cal_main_div {\n  display: flex;\n  justify-content: space-between;\n  margin-top: 20px;\n}\n.cal_col {\n  display: flex;\n  flex-direction: column;\n  align-items: flex-start;\n}\n.card_cal_image {\n  height: 15px;\n  object-fit: contain;\n}\n.cal_title {\n  font-size: var(--regularM-font);\n  font-family: var(--theme-newFont);\n  color: var(--black);\n  font-weight: 700;\n  margin: 0px;\n}\n.cal_sub_title {\n  font-size: var(--small-font);\n  font-family: var(--theme-newFont);\n  color: var(--black);\n  margin: 10px 0px 5px;\n  font-weight: 400;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uL2Fzc2V0cy9mb250cy9mb250cy5zY3NzIiwibmV3LXByb2ZpbGUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsdUNBQUE7RUFDQSw4REFBQTtBQ0NGO0FERUE7RUFDRSx3Q0FBQTtFQUNBLCtEQUFBO0FDQUY7QURHQTtFQUNFLHFDQUFBO0VBQ0EsNERBQUE7QUNERjtBRElBO0VBQ0UsK0NBQUE7RUFDQSw2REFBQTtBQ0ZGO0FES0E7RUFDRSxzQ0FBQTtFQUNBLDZEQUFBO0FDSEY7QURPQTtFQUNFLG9DQUFBO0VBQ0EsK0RBQUE7QUNMRjtBQXRCQTtFQUNJLHFCQUFBO0FBd0JKO0FBdEJBO0VBQ0ksZ0JBQUE7RUFDQSx3QkFBQTtFQUNBLGdDQUFBO0VBQ0Esb0JBQUE7RUFDQSxxQkFBQTtBQXlCSjtBQXhCSTtFQUNJLFdBQUE7RUFDQSxlQUFBO0FBMEJSO0FBeEJJO0VBQ0ksV0FBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtBQTBCUjtBQXhCSTtFQUNJLHlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7QUEwQlI7QUF2QlE7RUFDSSxlQUFBO0VBQ0EsY0FBQTtBQXlCWjtBQVpBO0VBSUksaUNBQUE7RUFDQSxtQ0FBQTtFQUNBLG1DQUFBO0VBQ0Esb0JBQUE7RUFDQSw0QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUNBQUE7QUFZSjtBQXJCSTtFQUNJLGFBQUE7QUF1QlI7QUFaSTtFQUNJLGVBQUE7QUFjUjtBQVRBO0VBSUksaUNBQUE7RUFDQSxtQ0FBQTtFQUNBLG1DQUFBO0VBQ0Esb0JBQUE7RUFDQSw0QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUNBQUE7QUFTSjtBQWxCSTtFQUNJLGFBQUE7QUFvQlI7QUFWSTtFQUNJLGVBQUE7QUFZUjtBQVJBO0VBQ0ksZ0NBQUE7RUFDQSxVQUFBO0VBQ0EsNkJBQUE7QUFXSjtBQVZJO0VBQ0ksZUFBQTtBQVlSO0FBVEE7RUFDSSxZQUFBO0VBRUEsZ0NBQUE7RUFDQSw4QkFBQTtBQVdKO0FBUkE7RUFDSSx5QkFBQTtBQVdKO0FBUkE7RUFDSSxpQkFBQTtFQUNBLCtCQUFBO0VBQ0EsaUNBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0FBV0o7QUFSQTtFQUNJLFdBQUE7RUFDQSxVQUFBO0FBV0o7QUFSQTtFQUNJLFlBQUE7RUFDQSxXQUFBO0VBQ0Esb0NBQUE7QUFXSjtBQVJBO0VBQ0ksWUFBQTtFQUNBLGtCQUFBO0VBQ0EsNEJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0Esa0JBQUE7QUFXSjtBQVJBO0VBQ0ksZUFBQTtFQUNBLHlCQUFBO0FBV0o7QUFSQTtFQUNJLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFFBQUE7QUFXSjtBQVJBO0VBQ0ksK0JBQUE7RUFDQSxpQ0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtBQVdKO0FBUkE7RUFDSSw2QkFBQTtFQUNBLGlDQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLFVBQUE7QUFXSjtBQVJBO0VBQ0ksOEJBQUE7RUFDQSxpQ0FBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7QUFXSjtBQVRJO0VBQ0ksbUJBQUE7RUFDQSxnQkFBQTtBQVdSO0FBUEE7RUFDSSxXQUFBO0VBQ0EsbUJBQUE7RUFDRCxrQkFBQTtBQVVIO0FBUEE7RUFDSSx5QkFBQTtFQUNBLGlCQUFBO0FBVUo7QUFQQTtFQUNJLDRCQUFBO0VBQ0Esc0NBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQVVKO0FBTkE7RUFDSSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFFQSxrQkFBQTtBQVFKO0FBTEU7RUFDRSxXQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0VBQ0EscUJBQUE7RUFDQSxvQkFBQTtBQVFKO0FBTEU7RUFDRSx5QkFBQTtFQUNBLFdBQUE7RUFDQSxrQkFBQTtBQVFKO0FBTEU7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7QUFRSjtBQU5JO0VBRUUsOEJBQUE7QUFPTjtBQUhFO0VBQ0UsWUFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0VBQ0EsNkJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0Esa0NBQUE7RUFDQSxpQ0FBQTtBQU1KO0FBSEU7RUFDRSxZQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSw2QkFBQTtFQUNBLGFBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsNkJBQUE7RUFDQSxpQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGlDQUFBO0FBTUo7QUFIRTtFQUNFLHlDQUFBO0FBTUo7QUFIRTtFQUNFLDZCQUFBO0VBQ0EsaUNBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EsVUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQU1KO0FBREU7RUFDRSxlQUFBO0VBQ0Esa0JBQUE7QUFJSjtBQURFO0VBQ0UseUJBQUE7RUFDQSwwQkFBQTtFQUNBLGVBQUE7RUFDQSxlQUFBO0FBSUo7QUFERTtFQUVFLGFBQUE7RUFDQSx1QkFBQTtFQUNBLHVCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtBQUdKO0FBQUU7RUFDRSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtBQUdKO0FBQUE7RUFDSSw0QkFBQTtFQUNBLGlDQUFBO0VBQ0EsbUJBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7QUFHSjtBQUFBO0VBQ0ksNkJBQUE7RUFDQSxpQ0FBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxnQkFBQTtBQUdKO0FBQ0E7RUFDSSxlQUFBO0VBQ0EsZ0JBQUE7QUFFSjtBQUNBO0VBQ0ksbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGFBQUE7QUFFSjtBQUNBO0VBQ0ksMEJBQUE7QUFFSjtBQUNBO0VBQ0ksWUFBQTtFQUNBLGdCQUFBO0FBRUo7QUFESTtFQUNJLG9DQUFBO0VBQ0Esb0JBQUE7RUFDQSw2QkFBQTtFQUNBLG1CQUFBO0VBQ0EsNEJBQUE7RUFDQSxpQ0FBQTtBQUdSO0FBQ0E7RUFDRyxZQUFBO0FBRUg7QUFESTtFQUNJLFlBQUE7RUFDQSxXQUFBO0FBR1I7QUFDQTtFQUNJLFdBQUE7RUFDQSxtQkFBQTtFQUNELDJCQUFBO0FBRUg7QUFDQTtFQUNJLGtCQUFBO0VBQ0Esa0NBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0FBRUo7QUFRQTtFQUNJLFdBQUE7QUFMSjtBQVFBO0VBQ0ksYUFBQTtFQUNBLDhCQUFBO0VBQ0EsZ0JBQUE7QUFMSjtBQVFBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsdUJBQUE7QUFMSjtBQVFBO0VBQ0ksWUFBQTtFQUNBLG1CQUFBO0FBTEo7QUFRQTtFQUNJLCtCQUFBO0VBQ0EsaUNBQUE7RUFDQSxtQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtBQUxKO0FBUUE7RUFDSSw0QkFBQTtFQUNBLGlDQUFBO0VBQ0EsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLGdCQUFBO0FBTEoiLCJmaWxlIjoibmV3LXByb2ZpbGUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLW1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1NZWRpdW0udHRmXCIpIGZvcm1hdChcIlRydWVUeXBlXCIpO1xufVxuXG5AZm9udC1mYWNlIHtcbiAgZm9udC1mYW1pbHk6IGd0LWFtZXJpY2Etc3RhbmRhcmQtcmVndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBndC1hbWVyaWNhLXN0YW5kYXJkLWJvbGQ7XG4gIHNyYzogdXJsKFwiL2Fzc2V0cy9mb250cy9Sb2JvdG8tQm9sZC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1leHRlbmRlZE1lZGl1bTtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cbkBmb250LWZhY2Uge1xuICBmb250LWZhbWlseTogZ3QtYW1lcmljYS1zdGFuZGFyZC1saWdodDtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1MaWdodC50dGZcIikgZm9ybWF0KFwiVHJ1ZVR5cGVcIik7XG59XG5cblxuQGZvbnQtZmFjZSB7XG4gIGZvbnQtZmFtaWx5OiBTb3VyY2UtU2Fucy1Qcm8tcmFndWxhcjtcbiAgc3JjOiB1cmwoXCIvYXNzZXRzL2ZvbnRzL1JvYm90by1SZWd1bGFyLnR0ZlwiKSBmb3JtYXQoXCJUcnVlVHlwZVwiKTtcbn1cbiIsIkBpbXBvcnQgXCIuLi8uLi90aGVtZS9mb250cy5zY3NzXCI7XG5pb24tY29udGVudHtcbiAgICAtLWJhY2tncm91bmQ6ICNGNkY3RkM7XG59XG4ubWFpbl9kaXZ7XG4gICAgbWluLWhlaWdodDogMTAwJTtcbiAgICBiYWNrZ3JvdW5kOiB2YXIoLS13aGl0ZSk7XG4gICAgYm9yZGVyLXJhZGl1czogNDBweCA0MHB4IDBweCAwcHg7XG4gICAgcGFkZGluZy1ib3R0b206IDMwcHg7XG4gICAgLS1iYWNrZ3JvdW5kOiAjRjZGN0ZDO1xuICAgIC53XzEwMHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIHBhZGRpbmc6IDAuNXJlbTtcbiAgICB9XG4gICAgLmNsb3NlX21vZGFsX2ljb257XG4gICAgICAgIHJpZ2h0OiAxcmVtO1xuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgICAgIGZvbnQtc2l6ZTogMnJlbTtcbiAgICB9XG4gICAgLm5hbWUtaXRlbXtcbiAgICAgICAgYm9yZGVyOiAxcHggc29saWQgI2YyZjJmMjtcbiAgICAgICAgcGFkZGluZzogLjVyZW07XG4gICAgICAgIGJhY2tncm91bmQ6ICNmMmYyZjI7XG4gICAgICAgIG1hcmdpbjogLjJyZW07XG4gICAgICAgIHdpZHRoOiA4MHB4O1xuICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgYm9yZGVyLXJhZGl1czogNXB4O1xuICAgIH1cbiAgICAuc2VjdGlvbi1wcmVme1xuICAgICAgICBzcGFue1xuICAgICAgICAgICAgZm9udC1zaXplOiAxcmVtO1xuICAgICAgICAgICAgY29sb3I6ICNiM2IzYjM7XG4gICAgICAgIH1cbiAgICB9XG59XG5cbmlvbi1jb250ZW50e1xuICAgICY6OnBhcnQoYmFja2dyb3VuZCl7XG4gICAgIC8vICAgLS1iYWNrZ3JvdW5kOnZhcigtLXRoZW1lLW5ld0hlYWRlcik7XG4gICAgfVxufVxuXG5cblxuLnByb2ZpbGVCdG57XG4gICAgJjo6cGFydChpbmRpY2F0b3Ipe1xuICAgICAgICBkaXNwbGF5Om5vbmU7XG4gICAgfVxuICAgIC0tYmFja2dyb3VuZC1jaGVja2VkOnZhcigtLXdoaXRlKTtcbiAgICAtLWJhY2tncm91bmQ6dmFyKC0tdGhlbWUtbmV3SGVhZGVyKTtcbiAgICAtLWJvcmRlci1yYWRpdXM6IDMwcHggMzBweCAzMHB4IDBweDtcbiAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgICBmb250LXNpemU6IHZhcigtLXNtYWxsLWZvbnQpO1xuICAgIGNvbG9yOiB2YXIoLS1ibGFjayk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLXRoZW1lLW5ld0ZvbnQpO1xuXG5cbiAgICBpb24taWNvbntcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xuICAgIH1cblxufVxuXG4uaW5zaWdodEJ0bntcbiAgICAmOjpwYXJ0KGluZGljYXRvcil7XG4gICAgICAgIGRpc3BsYXk6bm9uZTtcbiAgICB9XG4gICAgLS1iYWNrZ3JvdW5kLWNoZWNrZWQ6dmFyKC0td2hpdGUpO1xuICAgIC0tYmFja2dyb3VuZDp2YXIoLS10aGVtZS1uZXdIZWFkZXIpO1xuICAgIC0tYm9yZGVyLXJhZGl1czogMzBweCAzMHB4IDBweCAzMHB4O1xuICAgIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICAgIGZvbnQtc2l6ZTogdmFyKC0tc21hbGwtZm9udCk7XG4gICAgY29sb3I6IHZhcigtLWJsYWNrKTtcbiAgICBmb250LWZhbWlseTogdmFyKC0tdGhlbWUtbmV3Rm9udCk7XG5cbiAgICBpb24taWNvbntcbiAgICAgICAgZm9udC1zaXplOiAxNXB4O1xuICAgIH1cbn1cblxuaW9uLXNlZ21lbnQtYnV0dG9uW2FyaWEtc2VsZWN0ZWQ9XCJ0cnVlXCJde1xuICAgIGJvcmRlci1yYWRpdXM6IDMwcHggMzBweCAwcHggMHB4O1xuICAgIHdpZHRoOiA1MCU7XG4gICAgZm9udC1zaXplOiB2YXIoLS1tZWRpdW0tZm9udCk7XG4gICAgaW9uLWljb257XG4gICAgICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICB9XG59XG5pb24tc2VnbWVudCB7XG4gICAgaGVpZ2h0OiA2MHB4O1xuICAvLyAgLS1iYWNrZ3JvdW5kOiAgbGluZWFyLWdyYWRpZW50KHZhcigtLXRoZW1lLW5ld0hlYWRlciksdmFyKC0tdGhlbWUtbmV3SGVhZGVyKSwjZmZmZmZmLCAjZmZmZmZmKTs7XG4gICAgYm9yZGVyLXJhZGl1czo0MHB4IDQwcHggMHB4IDBweDtcbiAgICAtLXJpcHBsZS1jb2xvcjpub25lICFpbXBvcnRhbnQ7XG59XG5cbi5wcmVfbnh0X2J0bntcbiAgICBjb2xvcjogdmFyKC0tdGhlbWUtY29sb3IpO1xufVxuXG4uZGF0ZXtcbiAgICBtYXJnaW46IDIwcHggMzBweDtcbiAgICBmb250LXNpemU6IHZhcigtLXJlZ3VsYXJNLWZvbnQpO1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS10aGVtZS1uZXdGb250KTtcbiAgICBmb250LXdlaWdodDogNTAwO1xuICAgIGNvbG9yOiB2YXIoLS1ibGFjayk7XG59XG5cbi5hdmF0YXJfZGl2e1xuICAgIHdpZHRoOiA0NXB4O1xuICAgIHotaW5kZXg6IDE7XG59XG5cbi5hdmF0YXJ7XG4gICAgaGVpZ2h0OiA1MHB4O1xuICAgIHdpZHRoOiA1MHB4O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLXRoZW1lLWNvbG9yKTtcbn1cblxuLnNlY3Rpb24tY2FyZHtcbiAgICBtYXJnaW46MjBweDtcbiAgICBwYWRkaW5nOiAxNXB4IDIwcHg7XG4gICAgYm94LXNoYWRvdzogdmFyKC0tYm94c2hhZG93KTtcbiAgICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5lZGl0X2ljb257XG4gICAgZm9udC1zaXplOiA0MHB4O1xuICAgIGNvbG9yOiB2YXIoLS10aGVtZS1jb2xvcik7XG59XG5cbi5lZGl0X2ljb25fZGl2e1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICByaWdodDogNXB4O1xuICAgIHRvcDo1cHhcbn1cblxuLnVzZXJuYW1le1xuICAgIGZvbnQtc2l6ZTogdmFyKC0tcmVndWxhck0tZm9udCk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLXRoZW1lLW5ld0ZvbnQpO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgY29sb3I6IHZhcigtLWJsYWNrKTtcbiAgICBtYXJnaW4tYm90dG9tOiAwJTtcbn1cblxuLm1haW5fdGl0bGV7XG4gICAgZm9udC1zaXplOiB2YXIoLS1tZWRpdW0tZm9udCk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLXRoZW1lLW5ld0ZvbnQpO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgY29sb3I6IHZhcigtLWJsYWNrKTtcbiAgICBtYXJnaW46IDAlO1xufVxuXG4uY29tbW9uVGV4dHtcbiAgICBmb250LXNpemU6IHZhcigtLXJlZ3VsYXItZm9udCk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLXRoZW1lLW5ld0ZvbnQpO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgY29sb3I6IHZhcigtLWJsYWNrLWxpZ2h0KTtcblxuICAgIHNwYW57XG4gICAgICAgIGNvbG9yOiB2YXIoLS1ibGFjayk7XG4gICAgICAgIG1hcmdpbi1sZWZ0OiA1cHg7IFxuICAgIH1cbn1cblxuLmZpdG5lc3NfaW1hZ2V7XG4gICAgd2lkdGg6IDkwcHg7XG4gICAgb2JqZWN0LWZpdDogY29udGFpbjtcbiAgIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLmNhbGVuZGFyX2ljb257XG4gICAgY29sb3I6IHZhcigtLWJsYWNrLWxpZ2h0KTtcbiAgICBtYXJnaW4tcmlnaHQ6IDVweDtcbn1cblxuLmNvbmRpdGlvbl9jYXJke1xuICAgIGJhY2tncm91bmQ6IHZhcigtLWxpZ2h0R3JleSk7XG4gICAgYm9yZGVyOiAxLjVweCBzb2xpZCB2YXIoLS1jYXJkLWJvcmRlcik7XG4gICAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgICBwYWRkaW5nOiAyMHB4IDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIFxufVxuXG4uc2xpZGVyX2RpdiB7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgZGlzcGxheTogZmxleDtcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIC8vIHBhZGRpbmctbGVmdDogNSU7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB9XG5cbiAgLnNsaWRlcl9wYWdlIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1lbmQ7XG4gICAgcGFkZGluZy1ib3R0b206IDEzcHg7XG4gIH1cblxuICAubGluZXMge1xuICAgIGJvcmRlcjogMXB4IHNvbGlkICNFM0UzRTM7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICB9XG4gIFxuICAubGluZXMyIHtcbiAgICB3aWR0aDogMTAwJTtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIFxuICAgIGRpdiB7XG4gICBcbiAgICAgIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWJsYWNrKTtcbiAgICB9XG4gIH1cblxuICAucGFnZV9Db3VudCB7XG4gICAgaGVpZ2h0OiAxMHB4O1xuICAgIHdpZHRoOiAxMHB4O1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLXdoaXRlKTtcbiAgICBib3JkZXItcmFkaXVzOiAxMDAlO1xuICAgIGZvbnQtc2l6ZTogdmFyKC0teHNtYWxsLWZvbnQpO1xuICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBjb2xvcjogdmFyKC0td2hpdGUpO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHotaW5kZXg6IDEwO1xuICAgIGJhY2tncm91bmQ6IHZhcigtLXRoZW1lLW5ld0hlYWRlcik7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLXRoZW1lLW5ld0ZvbnQpO1xuICB9XG4gIFxuICAucGFnZV9Db3VudF9hY3RpdmUge1xuICAgIGhlaWdodDogMTBweDtcbiAgICB3aWR0aDogMTBweDtcbiAgICBib3JkZXI6IDJweCBzb2xpZCB2YXIoLS13aGl0ZSk7XG4gICAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgICBmb250LXNpemU6IHZhcigtLXhzbWFsbC1mb250KTtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgY29sb3I6IHZhcigtLXRoZW1lLW5ld0hlYWRlcikgO1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIHotaW5kZXg6IDEwO1xuICAgIGJhY2tncm91bmQ6ICNGMjkwM0M7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLXRoZW1lLW5ld0ZvbnQpO1xuICB9XG4gIFxuICAuY29tcGxldGUge1xuICAgIGJvcmRlcjogMnB4IHNvbGlkIHZhcigtLWJsYWNrKSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLndlZWt7XG4gICAgZm9udC1zaXplOiB2YXIoLS14c21hbGwtZm9udCk7XG4gICAgZm9udC1mYW1pbHk6IHZhcigtLXRoZW1lLW5ld0ZvbnQpO1xuICAgIGZvbnQtd2VpZ2h0OiA1MDA7XG4gICAgY29sb3I6IHZhcigtLWJsYWNrLWxpZ2h0KTtcbiAgICBtYXJnaW46IDAlO1xuICAgIG1hcmdpbi10b3A6IDVweDtcbiAgICBtYXJnaW4tYm90dG9tOiAwO1xuICB9XG5cblxuXG4gIC5tYW5faWNvbntcbiAgICBmb250LXNpemU6IDI1cHg7XG4gICAgbWFyZ2luLWJvdHRvbTogNXB4O1xuICB9XG5cbiAgLmxpbmVfaWNvbntcbiAgICBjb2xvcjogdmFyKC0tdGhlbWUtY29sb3IpO1xuICAgIHN0cm9rZTogdmFyKC0tdGhlbWUtY29sb3IpO1xuICAgIGZvbnQtc2l6ZTogMjVweDtcbiAgICBtYXJnaW4tdG9wOiA1cHg7XG4gIH1cblxuICAuY2FyZF9kaXZ7XG4gICAgXG4gICAgZGlzcGxheTogZmxleDtcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICBhbGlnbi1pdGVtczogZmxleC1zdGFydDtcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgfVxuXG4gIC5jYXJkX2ltYWdle1xuICAgIGhlaWdodDogMzBweDtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgcmlnaHQ6IC0xNXB4O1xuICAgIHRvcDogLTMwcHg7XG59XG5cbi50aXRsZXtcbiAgICBmb250LXNpemU6IHZhcigtLXNtYWxsLWZvbnQpO1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS10aGVtZS1uZXdGb250KTtcbiAgICBjb2xvcjogdmFyKC0tYmxhY2spO1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgbWFyZ2luOiAwcHggIDtcbn1cblxuLnN1Yl90aXRsZXtcbiAgICBmb250LXNpemU6IHZhcigtLXhzbWFsbC1mb250KTtcbiAgICBmb250LWZhbWlseTogdmFyKC0tdGhlbWUtbmV3Rm9udCk7XG4gICAgY29sb3I6IHZhcigtLWJsYWNrKTtcbiAgICBtYXJnaW46IDEwcHggMHB4IDBweCA7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgIFxufVxuXG4uc3Rhcl9pY29ue1xuICAgIGZvbnQtc2l6ZTogMzBweDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xufVxuXG4uY3VycnVudF9wbGFuX2RpdntcbiAgICBiYWNrZ3JvdW5kOiAjRkZGOUYyO1xuICAgIGJvcmRlci1yYWRpdXM6IDhweDtcbiAgICBwYWRkaW5nOiAxMHB4O1xufVxuXG4udXBwZXJDYXNle1xuICAgIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xufVxuXG4uZ29fYnRue1xuICAgIGhlaWdodDogNDBweDtcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xuICAgICY6OnBhcnQobmF0aXZlKXtcbiAgICAgICAgLS1iYWNrZ3JvdW5kOiB2YXIoLS10aGVtZS1uZXdCdXR0b24pO1xuICAgICAgICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgICAgICAgYm94LXNoYWRvdzogdmFyKC0tYnRuU2hhZGRvdyk7XG4gICAgICAgIGNvbG9yOiB2YXIoLS13aGl0ZSk7XG4gICAgICAgIGZvbnQtc2l6ZTogdmFyKC0tc21hbGwtZm9udCk7XG4gICAgICAgIGZvbnQtZmFtaWx5OiB2YXIoLS10aGVtZS1uZXdGb250KTtcbiAgICB9XG59XG5cbi5mbGFnX2ltZ3tcbiAgIG1hcmdpbjogMjBweDtcbiAgICAmOjpwYXJ0KGltYWdlKXtcbiAgICAgICAgaGVpZ2h0OjQwcHg7XG4gICAgICAgIHdpZHRoOiA0MHB4O1xuICAgIH1cbn1cblxuLkFjdGl2aXR5X2ltYWdle1xuICAgIHdpZHRoOiA2NXB4O1xuICAgIG9iamVjdC1maXQ6IGNvbnRhaW47XG4gICBtYXJnaW46IDEwcHggMjBweCAxMHB4IDEwcHg7XG59XG5cbi5wcm90aWVuX3Byb2dyZXNze1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICAtLXByb2dyZXNzLWJhY2tncm91bmQ6dmFyKC0tYmxhY2spO1xuICAgIGJvcmRlci1yYWRpdXM6IDI1cHg7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgbWFyZ2luOiAycHggMHB4O1xufVxuXG4vLyAuY2FyZF9pbWFnZXtcbi8vICAgICBoZWlnaHQ6IDY1cHg7XG4vLyAgICAgcG9zaXRpb246IGFic29sdXRlO1xuLy8gICAgIHJpZ2h0OiAtMTBweDtcbi8vICAgICB0b3A6IC00MHB4O1xuLy8gfVxuXG4uY2FsX2NhbGN1bGF0aW9uX2RpdntcbiAgICB3aWR0aDogMTAwJTtcbn1cblxuLmNhbF9tYWluX2RpdntcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG4uY2FsX2NvbHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gICAgYWxpZ24taXRlbXM6IGZsZXgtc3RhcnQ7XG59XG5cbi5jYXJkX2NhbF9pbWFnZXtcbiAgICBoZWlnaHQ6IDE1cHg7XG4gICAgb2JqZWN0LWZpdDogY29udGFpbjtcbn1cblxuLmNhbF90aXRsZXtcbiAgICBmb250LXNpemU6IHZhcigtLXJlZ3VsYXJNLWZvbnQpO1xuICAgIGZvbnQtZmFtaWx5OiB2YXIoLS10aGVtZS1uZXdGb250KTtcbiAgICBjb2xvcjogdmFyKC0tYmxhY2spO1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgbWFyZ2luOiAwcHggIDtcbn1cblxuLmNhbF9zdWJfdGl0bGV7XG4gICAgZm9udC1zaXplOiB2YXIoLS1zbWFsbC1mb250KTtcbiAgICBmb250LWZhbWlseTogdmFyKC0tdGhlbWUtbmV3Rm9udCk7XG4gICAgY29sb3I6IHZhcigtLWJsYWNrKTtcbiAgICBtYXJnaW46IDEwcHggMHB4IDVweCA7XG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgIFxufVxuXG4gIFxuICAiXX0= */";

/***/ })

}]);
//# sourceMappingURL=src_app_new-profile_new-profile_module_ts.js.map