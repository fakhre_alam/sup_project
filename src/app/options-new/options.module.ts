import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OptionsNewPageRoutingModule } from './options-routing.module';

import { OptionsNewPage } from './options.page';
// import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OptionsNewPageRoutingModule,
    // TranslateModule,
    ComponentsModule
  ],
  declarations: [OptionsNewPage]
})
export class OptionsNewPageModule {}
