import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OptionsNewPage } from './options.page';

const routes: Routes = [
  {
    path: '',
    component: OptionsNewPage,
    children:[{
      path: '?/:param',
      component: OptionsNewPage
    }]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OptionsNewPageRoutingModule {}
