import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OptionsNewPage } from './options.page';

describe('OptionsPage', () => {
  let component: OptionsNewPage;
  let fixture: ComponentFixture<OptionsNewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OptionsNewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OptionsNewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
