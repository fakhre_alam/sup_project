import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HealthParametersPageRoutingModule } from './health-parameters-routing.module';

import { HealthParametersPage } from './health-parameters.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HealthParametersPageRoutingModule
  ],
  declarations: [HealthParametersPage]
})
export class HealthParametersPageModule {}
