import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { HealthParametersPage } from "./health-parameters.page";

const routes: Routes = [
  {
    path: "",
    component: HealthParametersPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HealthParametersPageRoutingModule {}
