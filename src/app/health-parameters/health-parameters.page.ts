import {
  Component,
  ElementRef,
  OnInit,
  Renderer2,
  ViewChild,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import moment from "moment";
import { LoaderService, TemplateService } from "../services";
import { UTILITIES } from "../core/utility/utilities";
import { IonContent } from "@ionic/angular";

@Component({
  selector: "app-health-parameters",
  templateUrl: "./health-parameters.page.html",
  styleUrls: ["./health-parameters.page.scss"],
})
export class HealthParametersPage implements OnInit {
  formattedDate: string;
  rawDate: Date;
  current_year = moment().subtract(0, "months").format("YYYY-MM-DD");
  max_year: any = moment().add(3, "years").format("YYYY-MM-DD");
  weight: number;
  systolic: number;
  diastolic: number;
  bg_Fasting: number;
  bg_Random: number;
  bg_unitIs_mg: boolean = false;
  cholestol_total: number;
  cholestol_ldl: number;
  cholestol_hdl: number;
  cholestol_triglycerides: number;
  isActive: boolean = true;
  healthRecords: any;
  userId: string = localStorage.getItem("email");
  // Reference the target element
  // @ViewChild('targetElement', { static: false }) targetElement: ElementRef;
  fetchedData: any;
  @ViewChild(IonContent, { static: false }) ionContent: IonContent;

  constructor(
    private formBuilder: FormBuilder,
    private templateService: TemplateService,
    private loaderService: LoaderService,
    private utilities: UTILITIES,
    private renderer: Renderer2
  ) {}
  // Weight Kg:
  // Bp : 		Systolic /   Diastolic
  // Blood Glucose (mmol/l or mg/dl) : 	Fasting / Random     mmol/l × 18.018 = mg/dl.
  // Cholesterol: 		Total / LDL / HDL / Triglycerides

  // 1 Add for a date
  // 2 Show summary in table form- Creator, Date, Weight, BP, Glucose. Cholesterol, with option to edit or disable a record

  ngOnInit() {
    this.rawDate = new Date();
    this.formattedDate = this.formatDate(this.rawDate);
    this.loaderService.presentLoader("Fetching Data....").then((_) => {
      this.templateService.getHealthParams(this.userId).subscribe(
        (res) => {
          this.fetchedData = res;
          this.getActive();
          console.log("Fetched Data-->> ", res);
          this.loaderService.hideLoader();
        },
        (error) => {
          this.loaderService.hideLoader();
          console.log("Category Error", error);
          this.utilities.presentAlert("Something went wrong!!!");
        }
      );
    });
  }
  onDateChange(date: string) {
    this.rawDate = new Date(date);
    this.formattedDate = this.formatDate(this.rawDate);
  }

  formatDate(date: Date): string {
    const day = ("0" + date.getDate()).slice(-2);
    const month = ("0" + (date.getMonth() + 1)).slice(-2);
    const year = date.getFullYear();

    return `${day}-${month}-${year}`;
  }

  getFormattedDate(date: Date): string {
    date = new Date(date);
    const day = ("0" + date.getDate()).slice(-2);
    const month = ("0" + (date.getMonth() + 1)).slice(-2);
    const year = date.getFullYear();

    return `${day}-${month}-${year}`;
  }

  isDisabled() {
    if (
      this.weight === undefined &&
      this.systolic === undefined &&
      this.diastolic === undefined &&
      this.bg_Fasting === undefined &&
      this.bg_Random === undefined &&
      this.cholestol_total === undefined &&
      this.cholestol_ldl === undefined &&
      this.cholestol_hdl === undefined &&
      this.cholestol_triglycerides === undefined
    ) {
      return true;
    }
    return false;
  }

  submitBtnHandler() {
    console.log("Weight:: ", this.weight);
    console.log("Date::>> ", new Date(this.rawDate));
    const obj = {
      date: this.rawDate,
      userId: this.userId,
      createdBy:
        localStorage.getItem("dietitianName") ||
        localStorage.getItem("loginEmail"),
      active: this.isActive,
      weightKg: this.weight,
      bp_systolic: this.systolic,
      bp_diastolic: this.diastolic,
      bloodGlucose_fasting: this.bg_Fasting,
      bloodGlucose_random: this.bg_Random,
      bloodGlucose_unit: this.bg_unitIs_mg ? "mg/dl" : "mmol/l",
      cholesterol_total: this.cholestol_total,
      cholesterol_ldl: this.cholestol_ldl,
      cholesterol_hdl: this.cholestol_hdl,
      cholesterol_triglycerides: this.cholestol_triglycerides,
    };
    this.loaderService.presentLoader("Saving...").then((_) => {
      this.templateService.addHealthParams(obj).subscribe(
        (response) => {
          this.loaderService.hideLoader();
          console.log("Response AddHealthParams:: ", response);
          this.utilities.presentAlert("Record Added Successfully!");
          window.location.reload();
        },
        (error) => {
          this.loaderService.hideLoader();
          console.log("Category Error", error);
          this.utilities.presentAlert("Something went wrong!!!");
        }
      );
    });
  }

  doUpdate: boolean = false;

  editFxn(record: Object) {
    this.doUpdate = true;
    this.ionContent.scrollToTop(0);
    console.log("Edit--Record:: -> ", record);
    // const targetElement = this.renderer.selectRootElement('#targetElement', true);
    //   targetElement.scrollIntoView({ behavior: 'smooth' });
    this.rawDate = new Date(record["date"]);
    this.formattedDate = this.formatDate(this.rawDate);
    this.isActive = record["ative"];
    this.weight = record["healthData"]["weightKg"];
    this.systolic = record["healthData"]["bloodPressure"]["systolic"];
    this.diastolic = record["healthData"]["bloodPressure"]["diastolic"];
    this.bg_Fasting = record["healthData"]["bloodGlucose"]["fasting"];
    this.bg_Random = record["healthData"]["bloodGlucose"]["random"];
    this.bg_unitIs_mg = record["healthData"]["bloodGlucose"]["unit"] == "mg/dl";
    this.cholestol_total = record["healthData"]["cholesterol"]["total"];
    this.cholestol_ldl = record["healthData"]["cholesterol"]["ldl"];
    this.cholestol_hdl = record["healthData"]["cholesterol"]["hdl"];
    this.cholestol_triglycerides =
      record["healthData"]["cholesterol"]["triglycerides"];
  }

  getActive() {
    this.healthRecords = this.fetchedData.filter((record) => record.active);
    console.log("HealthRecords::>> ", this.healthRecords);
  }

  updateBtnHandler() {
    console.log("Update Btn Clicked");
    const obj = {
      date: this.rawDate,
      userId: this.userId,
      createdBy:
        localStorage.getItem("dietitianName") ||
        localStorage.getItem("loginEmail"),
      active: this.isActive,
      weightKg: this.weight,
      bp_systolic: this.systolic,
      bp_diastolic: this.diastolic,
      bloodGlucose_fasting: this.bg_Fasting,
      bloodGlucose_random: this.bg_Random,
      bloodGlucose_unit: this.bg_unitIs_mg ? "mg/dl" : "mmol/l",
      cholesterol_total: this.cholestol_total,
      cholesterol_ldl: this.cholestol_ldl,
      cholesterol_hdl: this.cholestol_hdl,
      cholesterol_triglycerides: this.cholestol_triglycerides,
    };
    this.loaderService.presentLoader("Updating...").then((_) => {
      this.templateService.updateHealthParams(obj).subscribe(
        (response) => {
          this.loaderService.hideLoader();
          console.log("Response AddHealthParams:: ", response);
          this.utilities.presentAlert("Record Updated Successfully!");
          window.location.reload();
        },
        (error) => {
          this.loaderService.hideLoader();
          console.log("Category Error", error);
          this.utilities.presentAlert("Something went wrong!!!");
        }
      );
    });
  }

  deleteFxn(id: string, datePassed: Date) {
    console.log("IDDD ::>> ", id);
    const payload = {
      userId: this.userId,
      active: false,
      date: datePassed,
    };

    this.templateService.updateHealthParams(payload).subscribe(
      (response) => {
        // this.loaderService.hideLoader();
        console.log("Response AddHealthParams:: ", response);
        this.utilities.presentAlert("Record Deleted Successfully!");
        // window.location.reload();
        const record = this.fetchedData.find((record) => record._id === id);
        if (record) {
          record.active = false;
          // return record;
        } else {
          // return null; // Return null if no record is found with the given id
        }
        this.getActive();
      },
      (error) => {
        this.loaderService.hideLoader();
        console.log("Category Error", error);
        this.utilities.presentAlert("Something went wrong!!!");
      }
    );
  }
}
