import { AfterViewInit, Component, OnInit, NgZone } from "@angular/core";
import { Router } from "@angular/router";
import { AppService as appSer } from "../../app/app.service";
import { CONSTANTS } from "../core/constants/constants";
import { AppService } from "../services/app/app.service";
import { AppService as appS } from "../app.service";
import { APIS } from "../shared/constants/constants";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { UTILITIES } from "../core/utility/utilities";
import moment from "moment";
import { NgTinyUrlService } from "ng-tiny-url";

@Component({
  selector: "app-deitician-search",
  templateUrl: "./deitician-search.page.html",
  styleUrls: ["./deitician-search.page.scss"],
})
export class DeiticianSearchPage implements OnInit, AfterViewInit {
  isHindi: boolean = true;
  current_year = new Date().getFullYear();
  d = new Date();
  dateValue = "";
  dateValue2 = "";
  dietPlan = [
    { id: "fiteloWaterRetention", name: "Water retention" },
    { id: "fiteloWeightLoss", name: "High Protein fiber" },
    { id: "weightLoss", name: "Weight Loss" },
    { id: "muscleGain_morning", name: "Muscle Gain Morning" },
    { id: "muscleGain_evening", name: "Muscle Gain Evening" },
    { id: "fatShredding_morning", name: "Fat Shredding Morning" },
    { id: "fatShredding_evening", name: "Fat Shredding Evening" },
    { id: "diabetes", name: "Diabetes" },
    { id: "pcos", name: "PCOS" },
    { id: "cholesterol", name: "Cholesterol" },
    { id: "hypertension", name: "Hypertension" },
  ];
  search: any = localStorage.getItem("email");
  searchDetails: any;
  isOpenProfile = true;
  isOpenProfile1 = true;
  defaultDetail: any;
  plandata: any;
  isSearch = false;
  username = "";
  dietData = [
    { value: "0", name: "When You Wake up" },
    { value: "1", name: "Before Breakfast" },
    { value: "2", name: "Breakfast" },
    { value: "3", name: "Mid Day Meal" },
    { value: "4", name: "Lunch" },
    { value: "5", name: "Post Lunch" },
    { value: "6", name: "Evening Snack" },
    { value: "7", name: "Dinner" },
    { value: "8", name: "Before Sleep" },
  ];
  profileData: any = {};
  link1 = "";
  suggestedWeightRange: any = 0;
  constructor(
    private tinyUrl: NgTinyUrlService,
    private ngzone: NgZone,
    private utilities: UTILITIES,
    private appservice: appS,
    private iab: InAppBrowser,
    private router: Router,
    private appS: appSer,
    private appServe: AppService
  ) {}
  getTinyUrl(url) {
    this.tinyUrl.shorten(url).subscribe(
      (res) => {
        this.link = res;
        this.link1 = res;
        this.utilities.hideLoader();
      },
      (err) => {
        this.utilities.hideLoader();
        this.utilities.presentAlert("Something went wrong. Please try again!");
        console.log("error", err);
      }
    );
  }
  copyMessage(val: string) {
    const selBox = document.createElement("textarea");
    selBox.style.position = "fixed";
    selBox.style.left = "0";
    selBox.style.top = "0";
    selBox.style.opacity = "0";
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand("copy");
    document.body.removeChild(selBox);
    this.link = "copied";
  }
  checkPlan() {
    // this.appservice.getOnePlan().then((res) => {
    //   this.plandata = res;
    //   let exp: any = new Date(this.plandata.planExpiryDate).getTime();
    //   let newDate: any = new Date().getTime();
    //   console.log(exp, newDate);
    //   if (exp > newDate) {
    //     this.plandata.isPlanActive = true;
    //   }
    //   console.log("plan==========>", res);
    // });
  }
  formatDate(value: string) {
    return moment(value).format("DD-MMM-YYYY");
  }
  isOpen1 = true;
  isOpen2 = true;
  isOpen3 = true;
  isOpen4 = true;
  isOpen5 = true;
  isOpen6 = true;
  isOpen1click() {
    this.isOpen1 = !this.isOpen1;
  }
  isOpen2click() {
    this.isOpen2 = !this.isOpen2;
  }
  isOpen3click() {
    this.isOpen3 = !this.isOpen3;
  }
  isOpen4click() {
    this.isOpen4 = !this.isOpen4;
  }
  isOpen5click() {
    this.isOpen5 = !this.isOpen5;
  }
  isOpen6click() {
    this.isOpen6 = !this.isOpen6;
  }
  submitCopyDietPlan() {
    this.appS
      .copyDietPlan(
        this.search,
        moment(this.dateValue).format("DDMMYYYY"),
        moment(this.dateValue2).format("DDMMYYYY")
      )
      .then(
        (res) => {
          console.log("res", res);
          this.utilities.presentAlert("Diet Plan Copied successfully!");
        },
        (err) => {
          console.log("error", err);
        }
      );
  }
  getProfile() {
    this.profileData = [];
    this.appservice.getProfile().then((res) => {
      this.profileData = res;

      if (this.profileData?.profile?.subCategory === "weightloss") {
        this.profileData.profile.subCategory = "Weight Loss";
      }
      if (this.profileData?.profile?.subCategory === "weightmaintenance") {
        this.profileData.profile.subCategory = "Weight Maintenance";
      }
      if (this.profileData?.profile?.subCategory === "musclebuilding") {
        this.profileData.profile.subCategory = "Muscle Building";
      }
      if (this.profileData?.profile?.subCategory === "leanbody") {
        this.profileData.profile.subCategory = "Lean Body";
      }

      let h: any =
        this.profileData?.demographic?.height?.unit === "in"
          ? this.profileData?.demographic?.height?.value / 12
          : this.profileData?.demographic?.height?.value;
      if (this.profileData?.demographic?.height?.unit === "in") {
        console.log(h);
        h = h.toString().split(".");
        console.log(h);
        const h1: any = (h[1] / 0.0833333).toString().split("0")[0];
        console.log(h1);
        this.profileData.demographic.height.value = `${h[0]}' ${h1}"`;
      } else {
        this.profileData.demographic.height.value =
          this.profileData.demographic.height.value + " cm";
      }
      console.log(this.profileData);
    });
  }

  languagePref(event) {
    this.appServe
      .languagePref(event.detail.checked ? "English" : "Hindi", this.search)
      .subscribe(
        (res) => {
          console.log("sucess", res);
        },
        (err) => {
          console.log("err", err);
        }
      );
  }
  logout() {
    localStorage.clear();
    localStorage.setItem("email", "");
    this.searchDetails = undefined;
    this.search = "";
    this.username = "";
    this.router.navigate(["login"]);
    this.searchData = "";
  }
  ngOnInit() {
    this.searchData = "";
    this.username = "";
    this.username = localStorage.getItem("loginEmail");
    this.search = localStorage.getItem("email");
  }

  planChoosen: any;
  profileInititals = "";
  dinnerPref = [{ A: "Subzi" }, { C: "Curry" }, { W: "Snack" }];
  fullMonthName = [
    { SUN: "Sunday" },
    { MON: "Monday" },
    { TUE: "Tuesday" },
    { WED: "Wednesday" },
    { THU: "Thursday" },
    { FRI: "Friday" },
    { SAT: "Saturday" },
  ];
  returnMonth(monthArr) {
    let TempArr = [];
    for (let index = 0; index < monthArr?.length; index++) {
      let Temp = this.fullMonthName.filter((item) => {
        return item[monthArr[index]];
      });
      TempArr.push(Temp[0][monthArr[index]]);
    }
    return TempArr.join(",");
  }
  returnDinner(monthArr) {
    let TempArr = [];
    for (let index = 0; index < monthArr?.length; index++) {
      let Temp = this.dinnerPref.filter((item) => {
        return item[monthArr[index]];
      });
      TempArr.push(Temp[0][monthArr[index]]);
    }
    return TempArr.join(",");
  }
  returnRemovedSlot(monthArr) {
    let TempArr = [];
    for (let index = 0; index < monthArr?.length; index++) {
      let Temp = this.dietData.filter((item) => {
        return item.value == monthArr[index]?.toString();
      });
      TempArr.push(Temp[0].name);
    }
    return TempArr.join(",");
  }
  searchData: any = "";
  lifeStyle: any;
  handleSearchEvent($event) {
    this.search = $event;
    console.log("Inside Parent Page:: ", this.search);
    this.fetchProfile();
  }
  fetchProfile() {
    if (!this.search?.trim()) {
      return;
    }
    CONSTANTS.email = this.search;
    localStorage.setItem("email", this.search);
    this.isSearch = true;
    this.appS.searchProfile(this.search).then((profileData: any) => {
      this.searchData = profileData["_id"];
      console.log("profileData:-", profileData);
      this.lifeStyle = profileData?.lifeStyle;
      this.additionalPref(profileData["additionalPref"]);
      const data = profileData["profile"];
      if (data) {
        this.searchDetails = JSON.parse(JSON.stringify(profileData));
        console.log("this.searchDetails", this.searchDetails);

        const planC = this.dietPlan.filter((item) => {
          return item.id === this.searchDetails?.additionalPref?.planChosen;
        });
        this.planChoosen = planC[0]?.name;
        this.updateTargetCal = this.searchDetails?.lifeStyle?.calories;
        this.recommendedTargetCal = this.searchDetails?.lifeStyle?.calories;
        const dd = JSON.parse(JSON.stringify(profileData));
        this.profileInititals = dd?.profile.name;
        // dd?.profile.name?.replace("  "," ")?.split(' ')[0][0].toUpperCase() +
        // (dd?.profile?.name?.replace("  "," ")?.split(' ')?.length>=2?dd?.profile?.name?.replace("  "," ")?.split(' ')[1][0].toUpperCase():dd?.profile?.name?.replace("  "," ")?.split(' ')[0][1].toUpperCase());

        this.isHindi =
          profileData["profile"]?.languagePref == undefined ? false : true;
        // this.search ='';
        this.appServe.getUserToken(this.search).subscribe(
          (response) => {
            console.log(response);
          },
          (error) => {
            console.log("User token ", error.error.text);
            localStorage.setItem("personal_token", error.error.text);
            console.log("Category Error", error);
          }
        );
        this.getProfile();
        this.getToken1();
      } else {
        this.searchDetails = undefined;
      }

      this.appS.getBeatoData(this.search).then(
        (res) => {
          console.log("response", res);
        },
        (err) => {
          console.log("error", err);
        }
      );
    });
  }

  additionPreferences: any;
  additionalPref(additionalPref) {
    this.additionPreferences = additionalPref;
  }
  viewProfile() {
    this.router.navigate(["new-profile"]);
  }
  ionViewWillEnter() {
    // debugger;
    this.username = "";
    this.isSearch = false;
    this.username = this.getUserName();
    this.search = localStorage.getItem("email");
    this.searchData = "";
    if (localStorage.getItem("email")) {
      this.fetchProfile();
      this.defaultDetail = JSON.parse(localStorage.getItem("defaultData"));
      console.log("this.defaultDetail", this.defaultDetail);
      this.appS.searchProfile(this.search).then((profileData: any) => {
        console.log("profileData:-", profileData);

        localStorage.setItem("email", this.search);
        CONSTANTS.email = localStorage.getItem("email");

        if (profileData.demographic != undefined) {
          this.searchDetails = profileData;
        } else {
          this.searchDetails = undefined;
        }

        console.log(
          "searchDetails?.demographic?.height?.value",
          this.searchDetails?.demographic?.height?.value
        );
      });
    }
  }

  getUserName() {
    const email = localStorage.getItem("loginEmail");
    return email.toLowerCase().includes("beatoapp.com".toLowerCase())
      ? "beato"
      : email;
  }
  ngAfterViewInit() {}
  gotoBeato() {
    const start_date = moment(new Date()).format("YYYY-MM-DD");
    const end_date = moment(
      new Date(
        new Date().getFullYear(),
        new Date().getMonth(),
        new Date().getDate() + 7
      )
    ).format("YYYY-MM-DD");
    console.log(
      `https://care.internal.beatoapp.com/diet-chart-listing?userid=${this.search}&flag=0&start=${start_date}&end=${end_date}`
    );

    window.open(
      `https://care.internal.beatoapp.com/diet-chart-listing?userid=${this.search}&flag=0&start=${start_date}&end=${end_date}`,
      "_blank"
    );
  }
  goBack() {
    this.router.navigate(["tabs/home"]);
  }
  updateCustomer() {
    this.router.navigate(["addupdate"]);
  }
  updateRefferal() {
    this.router.navigate(["/tabs/admin"]);
  }
  viewUserDetail() {
    this.router.navigate(["/tabs/admin"]);
  }
  updateDietplan() {
    if (this.username !== "beato") {
      this.router.navigate(["/tabs/consume"]);
    } else {
      this.router.navigate(["/consume"]);
    }
  }

  gotoPreferences() {
    this.router.navigate(["/preference-diet"]).then((res) => {
      location.reload();
    });
  }
  gotoDietPlan() {
    if (this.username !== "beato") {
      this.router.navigate(["/tabs/consume"]);
    } else {
      this.router.navigate(["/consume"]);
    }
  }
  updateFoodChoices() {
    this.router.navigate(["personalise"]);
  }
  goToClientApp(token) {
    let link = APIS.mainUrl + "?token=" + token;
    console.log("link", link);
    this.iab.create(link, "_system", "location=yes");
  }

  gotoupdateRemarks() {
    // goto slot remarks update
    this.router.navigate(["update-slot-remarks"], {
      queryParams: { params: this.search },
    });
  }
  updateTargetCal: any;
  recommendedTargetCal: any;

  getTokenUpdateTargetCal() {
    if (!localStorage.getItem("email")) {
      // alert("Please Enter Customer ID");
      return;
    } else {
      this.appServe.getUserToken(localStorage.getItem("email")).subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log("User token ", error.error.text);
          this.appS.updateTargetCal(
            this.updateTargetCal,
            this.search,
            error.error.text
          );
          this.recommendedTargetCal = Object.assign(this.updateTargetCal);
          localStorage.setItem("tknupdatetarget", error.error.text);
          console.log("Category Error", error);
        }
      );
    }
  }

  getToken() {
    if (!localStorage.getItem("email")) {
      // alert("Please Enter Customer ID");
      return;
    } else {
      this.appServe.getUserToken(localStorage.getItem("email")).subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log("User token ", error.error.text);
          this.goToClientApp(error.error.text);
          localStorage.setItem("tknupdatetarget", error.error.text);
          console.log("Category Error", error);
        }
      );
    }
  }

  updateProfile(isupdate) {
    this.appServe.getUserToken(this.search).subscribe(
      (response) => {
        console.log(response);
      },
      (error) => {
        this.lifeStyle.firstConsult = isupdate;
        const lifeStyle = this.lifeStyle;
        console.log("User token ", lifeStyle, error.error.text);
        this.appS.postLifeStyle(lifeStyle, error.error.text).then(
          (res) => {
            console.log("profile updated");
            this.utilities.presentAlert("Updated Successfully!");
          },
          (err) => {
            console.log("failed to updated profile");
          }
        );
      }
    );
  }

  checkedDetox = 0;
  defaultData: any;
  activityName;
  age;
  height;
  gender;
  diseases = [];
  diseases1 = [];
  alergies = ["SF,SO", "ML", "F", "E", "N", "G"];
  mealPref;
  community = [];
  getToken1() {
    if (!localStorage.getItem("email")) {
      this.utilities.presentAlert("Please enter email");
    } else {
      this.appServe.getUserToken(localStorage.getItem("email")).subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log("User token ", error.error.text);
          this.appS.getDefaultData(error.error.text).then((res) => {
            this.diseases = [];
            this.diseases1 = [];
            this.alergies = ["SF,SO", "ML", "F", "E", "N", "G"];
            this.mealPref;
            this.community = [];
            this.defaultData = res;
            this.activityName = this.defaultData.otherMaster.activities.filter(
              (item) => {
                return (
                  item.code ===
                  this.searchDetails?.lifeStyle?.activities["code"]
                );
              }
            );

            console.log("Activity Response:: ", this.defaultData);

            for (
              let index = 0;
              index < this.searchDetails?.lifeStyle?.diseases.length;
              index++
            ) {
              for (
                let j = 0;
                j < this.defaultData?.otherMaster?.diseases.length;
                j++
              ) {
                if (
                  this.defaultData?.otherMaster?.diseases[j].code ===
                  this.searchDetails?.lifeStyle?.diseases[index]
                ) {
                  if (
                    this.alergies.includes(
                      this.defaultData?.otherMaster?.diseases[j].code
                    )
                  ) {
                    this.diseases1.push(
                      this.defaultData?.otherMaster?.diseases[j].value
                    );
                  } else {
                    this.diseases.push(
                      this.defaultData?.otherMaster?.diseases[j].value
                    );
                  }
                }
              }
            }
            this.mealPref = this.defaultData?.otherMaster?.foodPref?.filter(
              (item) => {
                return item.code === this.searchDetails?.lifeStyle?.foodType;
              }
            );
            for (
              let index = 0;
              index < this.searchDetails?.lifeStyle?.communities.length;
              index++
            ) {
              for (
                let j = 0;
                j < this.defaultData?.otherMaster?.community.length;
                j++
              ) {
                if (
                  this.defaultData?.otherMaster?.community[j].code ===
                  this.searchDetails?.lifeStyle?.communities[index]
                ) {
                  this.community.push(
                    this.defaultData?.otherMaster?.community[j].value
                  );
                }
              }
            }
          });

          this.appS.getOnePlan1(error.error.text).then((res) => {
            console.log("getOnePlan", res);
          });
          console.log("Category Error", error);
        }
      );
    }
  }

  gotoRecall() {
    this.router.navigate(["food-item-choice"]);
  }
  userIdInput = "";
  key = "";
  isOpenPopup = false;
  closePopup() {
    this.isOpenPopup = false;
  }
  clearMessage() {
    this.link = "";
    this.link1 = "";
    this.key = "";
    this.userIdInput = "";
  }
  registerUser() {
    this.isOpenPopup = true;
    this.link = "";
    this.link1 = "";
    this.key = "";
    this.userIdInput = "";
  }
  customUrl: string = "";
  link = "";
  isRegisteredUser() {
    let clientIdSystem = "";
    if (localStorage.getItem("loginEmail") === "orthocure") {
      this.key = "orthocure2024";
      clientIdSystem = "orthocure";
    } else if (localStorage.getItem("loginEmail") === "fitelo") {
      this.key = "FITELO2023";
      clientIdSystem = "fitelo";
    }
    if (this.key === "" || this.userIdInput === "") {
      this.utilities.presentAlert("Key and Profile id is mandatory!");
      return;
    }
    //this.utilities.showLoading();
    console.log(this.userIdInput);
    const payload = {
      profile: {
        email: this.userIdInput,
      },
    };
    this.appS.getTokenExternal(this.key, this.userIdInput).subscribe((res1) => {
      if (res1["code"] === "0000") {
        // const lnk = `https://testonboarding.smartdietplanner.com/read?token=${res1["accessToken"]}&clientId=${clientIdSystem}&type=1`;
        const lnk = `https://onboarding.smartdietplanner.com/read?token=${res1["accessToken"]}&clientId=${clientIdSystem}&type=1`;

        this.getTinyUrl(lnk);
      } else {
        this.appS.externalRegistration(this.key, payload).subscribe(
          (res) => {
            console.log("success", res["access_token"]);
            if (res["code"] !== "0000") {
              this.utilities.hideLoader();
              this.utilities.presentAlert(res["message"]);
            } else {
              //  const lnk = `https://testonboarding.smartdietplanner.com/read?token=${res["access_token"]}&clientId=${clientIdSystem}&type=1`;
              const lnk = `https://onboarding.smartdietplanner.com/read?token=${res["access_token"]}&clientId=${clientIdSystem}&type=1`;

              this.getTinyUrl(lnk);
            }
          },
          (err) => {
            this.utilities.hideLoader();
            console.log("error", err);
          }
        );
      }
    });

    // this.isOpenPopup = false;
  }

  AddInstructions() {
    this.router.navigate(["/add-instructions"]);
  }
  consultQA() {
    this.router.navigate(["/consult-qa"]);
  }
}
