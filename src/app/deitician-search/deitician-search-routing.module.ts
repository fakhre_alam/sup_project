import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeiticianSearchPage } from './deitician-search.page';

const routes: Routes = [
  {
    path: '',
    component: DeiticianSearchPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DeiticianSearchPageRoutingModule {}
