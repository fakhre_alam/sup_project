import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { DeiticianSearchPageRoutingModule } from "./deitician-search-routing.module";

import { DeiticianSearchPage } from "./deitician-search.page";
import { FulldaynamePipe } from "../fulldayname.pipe";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DeiticianSearchPageRoutingModule,
  ],
  declarations: [
    DeiticianSearchPage,
    FulldaynamePipe,
     ],
})
export class DeiticianSearchPageModule {}
