import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { AddInstructionsPageRoutingModule } from "./add-instructions-routing.module";

import { AddInstructionsPage } from "./add-instructions.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddInstructionsPageRoutingModule,
  ],
  declarations: [
    AddInstructionsPage,
    
  ],
})
export class AddInstructionsPageModule {}
