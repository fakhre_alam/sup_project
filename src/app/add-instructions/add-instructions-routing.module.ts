import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddInstructionsPage } from './add-instructions.page';

const routes: Routes = [
  {
    path: '',
    component: AddInstructionsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddInstructionsPageRoutingModule {}
