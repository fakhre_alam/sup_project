import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";
import { AppService as appS } from "../services/app/app.service";
import { UTILITIES } from "../core/utility/utilities";
import { TemplateService } from "../services";
@Component({
  selector: "app-add-instructions",
  templateUrl: "./add-instructions.page.html",
  styleUrls: ["./add-instructions.page.scss"],
})
export class AddInstructionsPage implements OnInit {
  normalDay = "";
  cheatDay = "";
  detoxDay = "";
  lifeStyle: any;
  userId = localStorage.getItem("email");
  instructionId = "";

  constructor(
    private appServices: AppService,
    private appServe: appS,
    private utilities: UTILITIES,
    private templateService: TemplateService
  ) {
   
  }

  bindInstructions(){
    this.templateService.getInstructions(this.userId).subscribe(
      (response) => {
        console.log("Insrtutions-Result >> ", response);
        if (response[0]) {
          console.log("$$$$$$$$$$$$$$$$$$ ", response[0]);
          this.normalDay = response[0]?.normalDay;
          this.cheatDay = response[0]?.cheatDay;
          this.detoxDay = response[0]?.detoxDay;
          this.instructionId = response[0]?._id;
          console.log("Id:: ", this.instructionId);
        }
      },
      (error) => {
        console.error("------------>> Error Getting instrutions", error.error);
      }
    );
  }
  ionViewWillEnter(){
    this.bindInstructions();
    // this.appServices.getProfile().then(
    //   (res: any) => {
    //     this.lifeStyle = res.lifeStyle;
    //     this.normalDay =
    //       res.lifeStyle?.instructions?.nornalDay == undefined
    //         ? ""
    //         : res.lifeStyle?.instructions?.nornalDay;
    //     this.cheatDay =
    //       res.lifeStyle?.instructions?.cheatDay == undefined
    //         ? ""
    //         : res.lifeStyle?.instructions?.cheatDay;
    //     this.detoxDay =
    //       res.lifeStyle?.instructions?.detoxDay == undefined
    //         ? ""
    //         : res.lifeStyle?.instructions?.detoxDay;
    //   },
    //   (err) => {
    //     console.log("profile error", err);
    //   }
    // );
  }
  ngOnInit() {
  
  }
  submitInstructions() {
    // if (this.normalDay == "" || this.cheatDay == "" || this.detoxDay == "") {
    //   alert("Fill All Fields");
    //   return;
    // }
    if (this.instructionId != "") {
      this.templateService
        .updateInstructions(
          this.instructionId,
          this.userId,
          this.normalDay,
          this.cheatDay,
          this.detoxDay,
          "createrId:string"
        )
        .subscribe(
          (response) => {
            this.utilities.presentAlert("Instructions Updated Successfully.");
            if (localStorage.getItem("dietitianId") != null) {
              // dietitianId, actionId, userId,  actionName,  companyId
              this.templateService
                .handleDietitianAction(
                  localStorage.getItem("dietitianId"),
                  "668d72381073b512dd2b9405",
                  localStorage.getItem("email"),
                  "Update Instructions",
                  localStorage.getItem("companyId")
                )
                .subscribe((dRespondata) => {
                  console.log("Saved Insructions>> ", response);
                });
            }
          },
          (error) => {
            alert("ewrror");
            console.error("Error Updating instructions", error.error);
          }
        );
    } else {
      this.templateService
        .addInstructions(
          this.userId,
          this.normalDay,
          this.cheatDay,
          this.detoxDay,
          "createrId:string"
        )
        .subscribe(
          (response) => {
            console.log("Saved Insructions>> ", response);
            this.utilities.presentAlert("Instructions saved successfully.");
          },
          (error) => {
            alert("ewrror");
            console.error("Error Adding instrutions", error.error);
          }
        );
    }
    // this.appServe.getUserToken(localStorage.getItem("email")).subscribe(
    //   (response) => {
    //     console.log("local Storage-- ",response);
    //   },
    //   (error) => {
    //   this.lifeStyle.instructions ={
    //     nornalDay:this.normalDay,
    //     cheatDay:this.cheatDay,
    //     detoxDay:this.detoxDay
    //   };
    //  this.appServices.postLifeStyle(this.lifeStyle,error.error.text).then(res=>{
    //   console.log("instructions response:- ",res);
    //   this.utilities.presentAlert("Instructions saved successfully.");
    //   },err=>{
    //   console.log("instructions error:-",err);

    //   })
    // });
  }
  back() {
    window.history.back();
  }
}
