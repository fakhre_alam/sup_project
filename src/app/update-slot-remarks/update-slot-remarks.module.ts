import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { UpdateSlotRemarksPageRoutingModule } from "./update-slot-remarks-routing.module";

import { UpdateSlotRemarksPage } from "./update-slot-remarks.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateSlotRemarksPageRoutingModule,
  ],
  declarations: [
    UpdateSlotRemarksPage,

  ],
})
export class UpdateSlotRemarksPageModule {}
