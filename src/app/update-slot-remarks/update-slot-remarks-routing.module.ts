import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateSlotRemarksPage } from './update-slot-remarks.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateSlotRemarksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateSlotRemarksPageRoutingModule {}
