import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService as AppS } from '../services/app/app.service';
import moment from 'moment';
import { UTILITIES } from '../core/utility/utilities';
@Component({
  selector: 'app-update-slot-remarks',
  templateUrl: './update-slot-remarks.page.html',
  styleUrls: ['./update-slot-remarks.page.scss'],
})
export class UpdateSlotRemarksPage implements OnInit {
  slot0="";
  slot1="";
  slot2="";
  slot3="";
  slot4="";
  slot5="";
  slot6="";
  slot7="";
  slot8="";
  search;
  constructor(private router: Router,private utilities:UTILITIES, private appService: AppService, private queryParams: ActivatedRoute,private appServ:AppS) { 
    this.queryParams.queryParams.subscribe(res=>{
      this.search = res["params"];
    });
  }
  goback(){
    this.router.navigate(["/deitician-search"]);
  }
  ngOnInit() {
    this.appServ.getUserToken(this.search).subscribe(
      (response) => {
        console.log(response);
       },
      (error) => {
        console.log("User token ", error.error.text);
        this.appService.getSlotRemarks({},this.search,error.error.text).then(res=>{
          this.slot0= res["remarks"][0]?.remark;
          this.slot1= res["remarks"][1]?.remark;
          this.slot2= res["remarks"][2]?.remark;
          this.slot3= res["remarks"][3]?.remark;
          this.slot4= res["remarks"][4]?.remark;
          this.slot5= res["remarks"][5]?.remark;
          this.slot6= res["remarks"][6]?.remark;
          this.slot7= res["remarks"][7]?.remark;
          this.slot8= res["remarks"][8]?.remark;
        })
        console.log("Category Error", error);
      }
    );
  }
  getToken(payload,search) {
   
      this.appServ.getUserToken(search).subscribe(
        (response) => {
          console.log(response);
         },
        (error) => {
          console.log("User token ", error.error.text);
          this.appService.updateSlotRemarks(payload,search,error.error.text);
          this.utilities.presentAlert("Remarks saved successfully!");
          console.log("Category Error", error);
        }
      );
    
  }

  updateRemarks(){
    const payload={
      "remarks":[
        {"slot":"0", "remark":this.slot0},
        {"slot":"1", "remark":this.slot1},
        {"slot":"2", "remark":this.slot2},
        {"slot":"3", "remark":this.slot3},
        {"slot":"4", "remark":this.slot4},
        {"slot":"5", "remark":this.slot5},
        {"slot":"6", "remark":this.slot6},
        {"slot":"7", "remark":this.slot7},
        {"slot":"8", "remark":this.slot8}
      ]
    }
console.log("payload", payload,this.search);
     this.getToken(payload,this.search); 
  }
}
