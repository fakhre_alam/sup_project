import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { NwDietPlanPageRoutingModule } from "./suggest-recipe-routing.module";

import { NwDietPlanPage } from "./suggest-recipe.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NwDietPlanPageRoutingModule,
  ],
  declarations: [NwDietPlanPage],
})
export class NwDietPlanPageModule {}
