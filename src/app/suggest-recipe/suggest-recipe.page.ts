import { Component, OnInit } from "@angular/core";
import { LoaderService, TemplateService } from "../services";
import { UTILITIES } from "../core/utility/utilities";
import { ActivatedRoute, Router } from "@angular/router";
import { IonLabel, IonSelect } from "@ionic/angular";

@Component({
  selector: "app-suggest-recipe",
  templateUrl: "./suggest-recipe.page.html",
  styleUrls: ["./suggest-recipe.page.scss"],
})
export class NwDietPlanPage implements OnInit {
  _id: string;
  Food: string = "";
  foodType: string;
  itemCode: string = "";
  hindiName: string = "";
  Type: string;
  Slots: string[] = [];
  status: string = "submitted";
  Calories: string;
  Carbs: string;
  Fat: string;
  Protien: string;
  Fiber: string;
  season: string[] = [""];
  Special_slot: number = -1;
  Community: string[];
  AvoidIn: string[];
  RecommendedIn: string[];
  Remark: string;
  Detox: string = "N";
  Detox_rank: string = "3";
  Special_diet: string = "-1";
  code: string = "001";
  Ultra_special: number = -1;
  immunity: number = -1;
  immunity_special_slot: number = -1;
  during_covid: number = -1;
  during_covid_special_slot: number = -1;
  post_covid: number = -1;
  post_covid_special_slot: number = -1;
  courtesy: string = "By FitrofyRecipes";
  recipe: string;
  steps: string;
  stepsHindi: string = "";
  recipeHindi: string;
  video: string;
  gluten_free: string = "";
  source: string = "";
  portion: number;
  portion_unit: string = "Katori- 150 ml";
  country: string[] = ["IND"];
  brand: string = "";
  Special_slot_canada: string = "-1";
  Special_slot_australia: string = "-1";
  Score: number = 3;
  Diabetes_score: string = "3";
  Diabetes_special: string = "-1";
  BP_Score: string = "3";
  BP_special: string = "-1";
  Cholestrol_score: string = "3";
  Cholestrol_special: string = "-1";
  PCOS_score: string = "3";
  autism_slot: string = "-1";
  adhd_slot: string = "-1";
  autism_score: string = "3";
  adhd_score: string = "3";
  PCOS_special: string = "-1";
  net_carbs: string = "0";
  companyName: string = localStorage.getItem("companyId");
  "WeightlossPlus_score": string = "3";
  "WeightlossPlus_special": string = "-1";
  "recipeFlag": string = "-1";
  "trendingRecipe": string = "-1";
  muscle_building_score: string = "3";
  "special_slot_muscle_building_morning": string = "3";

  "special_slot_muscle_building_evening": string = "-1";

  "Protein_percentage": string = "0";
  "Keto_Score": string = "3";
  "Keto_Slot": string = "-1";
  "Hypothyroid_Score": string = "3";
  "Hypothyroid_Slot": string = "-1";
  "Anti_Inflammatory_Score": string = "3";
  "Anti_Inflammatory_Slot": string = "-1";
  "Fatty_Liver_Score": string = "3";
  "Fatty_Liver_Slot": string = "-1";
  "Functional_Food_flag": string = "";
  "Functional_foods": string[] = [];
  "Anemia_Score": string = "3";
  "Acidity_Score": string = "3";
  "Sleep_Score": string = "3";
  "Uric_Acid_Score": string = "3";
  "Calcium": string = "3";
  "Vitamin_D": string = "3";
  "Vitamin_B12_Score": string = "3";
  "Egypt_Plan": string = "-1";
  "Alkaline_Score": string = "3";
  "Alkaline_Slot": string = "-1";
  "Prediabetes_Score": string = "3";
  "Prediabetes_Slot": string = "-1";
  "High_Protein_Score": string = "3";
  "High_Protein_Slot": string = "-1";
  "T1_Pregnancy_score": string = "3";
  "T1_Pregnancy_slot": string = "-1";
  "T2_Pregnancy_slot": string = "-1";
  "T2_Pregnancy_score": string = "3";
  "T3_Pregnancy_slot": string = "-1";
  "T3_Pregnancy_score": string = "3";
  "Postpartum_score": string = "3";
  "Postpartum_slot": string = "-1";
  "Lactation_score_0_6": string = "3";
  "Lactation_slot_0_6": string = "-1";
  "Lactation_score_7_12": string = "3";
  "Lactation_slot_7_12": string = "-1";
  "waterRetentionScore": string = "3";
  "waterRetentionSlot": string = "-1";
  "fiteloWeightLoss_Score": string = "3";
  "fiteloWeightLossSlot": string = "-1";
  "lunchCheatDayRank": string = "3";
  "lunchCheatDaySlot": string = "-1";
  "dinnerCheatDayRank": string = "3";
  "dinnerCheatDaySlot": string = "-1";
  "cookingProficiency": string = "3";
  "cookingTime": string = "3";
  "detoxFiteloRank": string = "3";
  "detoxFiteloSlot": string = "-1";
  "travel_rank": string = "3";
  "travel_slot": string = "-1";
  "Stress_rank": string = "3";
  "Stress_slot": string = "-1";
  "Skincare_rank": string = "3";
  "Skincare_slot": string = "-1";
  "gutelimination_rank": string = "3";
  "gutelimination_slot": string = "-1";
  "Fodmap_rank": string = "3";
  "Fodmap_slot": string = "-1";
  "kidsbelow3_rank": string = "3";
  "kidsbelow3_slot": string = "-1";
  "kidsabove3_rank": string = "3";
  "kidsabove3_slot": string = "-1";
  "SmartCalories": number = 4;
  "BuyUrl": string = "";
  dietitianId: string;
  dietitianName: string;
  myId: string = localStorage.getItem("dietitianId");

  id: string;
  isOrigin: boolean = false;

  typeOptions: Object[] = [
    { label: "Dry Vegetables", value: "A" },
    { value: "B", label: "Bread/ Roti/ Rice" },
    { value: "C", label: "Curry" },
    { value: "D", label: "Non-dairy drinks/ Plant milk drinks" },
    { value: "DM", label: "Milk based drinks" },
    { value: "WS", label: "Smoothies" },
    { value: "DZ", label: "Drinks with zero calories" },
    { value: "F", label: "Fruits" },
    { value: "M", label: "Meals" },
    { value: "N", label: "Nuts" },
    { value: "NS", label: "Nut Seeds" },
    { value: "PW", label: "Post Workout Snacks" },
    { value: "R", label: "Raita" },
    { value: "S", label: "Side Salad" },
    { value: "SO", label: "Soup" },
    { value: "W", label: "Snacks" },
    { value: "WD", label: "Desserts" },
    { value: "WP", label: "Parantha" },
    { value: "Y", label: "Yogurt" },
  ];

  portionUnitOptions: string[] = [
    "Bowl- 200 ml",
    // "pc- (mention the quantity of 1 pc in grams)",
    "pc",
    "Katori- 150 ml",
    "Glass- 250ml",
    "quarter plate",
    "tsp- 5ml",
    "Cup- 200 ml",
    "tbsp- 15ml",
    "serving",
    "Portion",
  ];

  scoreOptions = [
    { label: "Bad", value: "-1" },
    { label: "Below Average", value: "1" },
    { label: "Average", value: "3" },
    { label: "Good", value: "6" },
    { label: "Excellent", value: "9" },
  ];
  scoreOptionsNum = [
    { label: "Bad", value: -1 },
    { label: "Below Average", value: 1 },
    { label: "Average", value: 3 },
    { label: "Good", value: 6 },
    { label: "Excellent", value: 9 },
  ];
  countryOptions = [
    { code: "IND", label: "India" },
    { code: "USA", label: "USA" },
    { code: "UAE", label: "Dubai" },
    { code: "UK", label: "UK" },
    { code: "IR", label: "Ireland" },
    { code: "EGY", label: "Egypt" },
    { code: "CAN", label: "Canada" },
    { code: "AUS", label: "Australia" },
    { code: "MAL", label: "Malaysia" },
  ];
  avoidInOptions = [
    { code: "A", value: "ACIDITY" },
    { code: "AN", value: "ANEMIA" },
    { code: "AI", value: "ANTI-INFLAMMATORY" },
    { code: "B", value: "BLOOD PRESSURE" },
    { code: "C", value: "CHOLESTEROL" },
    { code: "CR", value: "CALCIUM-RICH" },
    { code: "D", value: "DIABETES" },
    { code: "PD", value: "PRE-DIABETES" },
    { code: "GD", value: "GESTATIONAL DIABETES" },
    // { code: "E", value: "EGESTION/ CONSTIPATION" },
    { code: "FL", value: "FATTY LIVER" },
    { code: "HP", value: "HIGH PROTEIN" },
    { code: "IR", value: "IRON-RICH" },
    { code: "LP", value: "LOW PROTEIN" },
    { code: "P", value: "PCOS" },
    { code: "T", value: "HYPOTHYROID" },
    { code: "S", value: "SLEEP DISORDER" },
    { code: "UA", value: "URIC ACID" },
    { code: "VB", value: "VITAMIN B12" },
    { code: "VD", value: "VITAMIN D" },
    { code: "W", value: "WIND/ FLATULENCE/ BLOATING" },
    { code: "N", value: "NUTS Allergy" },
    { code: "E", value: "EGGS Allergy" },
    { code: "F", value: "FISH Allergy" },
    { code: "ML", value: "MILK/ LACTOSE Allergy" },
    { code: "SO", value: "SOYA Allergy" },
    { code: "SF", value: "SEAFOOD Allergy" },
    { code: "G", value: "GLUTEN Allergy" },
  ];
  communityOptions = [
    { code: "U", label: "Universal" },
    { code: "P", label: "North India" },
    { code: "S", label: "South India" },
    { code: "M", label: "Maharashtra" },
    { code: "G", label: "Gujarat" },
    { code: "B", label: "Bengali" },
  ];
  seasonOptions: Object[] = [
    { label: "All", value: "" },
    { value: "January", label: "January" },
    { value: "February", label: "February" },
    { value: "March", label: "March" },
    { value: "April", label: "April" },
    { value: "May", label: "May" },
    { value: "June", label: "June" },
    { value: "July", label: "July" },
    { value: "August", label: "August" },
    { value: "September", label: "September" },
    { value: "October", label: "October" },
    { value: "November", label: "November" },
    { value: "December", label: "December" },
  ];

  slotOptions: Object[] = [
    { slot: 0, message: "When You Wake up", data: [{}] },
    { slot: 1, message: "Before Breakfast", data: [{}] },
    { slot: 2, message: "Breakfast", data: [{}] },
    { slot: 3, message: "Mid Day Meal", data: [{}] },
    { slot: 4, message: "Lunch", data: [{}] },
    { slot: 5, message: "Post Lunch", data: [{}] },
    { slot: 6, message: "Evening Snack", data: [{}] },
    { slot: 7, message: "Dinner", data: [{}] },
    { slot: 8, message: "Before Sleep", data: [{}] },
  ];

  foodTypeOptions = [
    {
      code: "V",
      value: "Vegetarian",
      isSelected: false,
    },
    {
      code: "NV",
      value: "Non-Vegetarian",
      isSelected: false,
    },
    {
      code: "E",
      value: "Vegetarian + Egg",
      isSelected: false,
    },
    {
      code: "Ve",
      value: "Vegan",
      isSelected: false,
    },
  ];

  isManisha: Boolean =
    localStorage.getItem("loginEmail") === "manisha@smartdietplanner.com";

  constructor(
    private loaderService: LoaderService,
    private utilities: UTILITIES,
    private templateService: TemplateService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.route.queryParamMap.subscribe((params) => {
      this.id = params.get("id");
      const designed = params.get("designed");
      const type = params.get("type");

      if (this.id) {
        // alert(this.id);
        this.loaderService.presentLoader("Fetching details...").then((_) => {
          if (designed === "true") {
            this.templateService
              .getIngredientsRecipeRecords("?id=" + this.id)
              .subscribe(
                (response) => {
                  this.loaderService.hideLoader();
                  this.id = null;

                  const data = response["records"][0];
                  // Assuming `response` contains the API response object
                  Object.assign(this, data);

                  // alert(typeof data["total"] + " || "  + JSON.stringify(data["total"]));

                  this.Calories = data["total"][0];
                  this.Carbs = data["total"][1];
                  this.Protien = data["total"][2];
                  this.Fat = data["total"][3];
                  this.Fiber = data["total"][4];
                  this.Calcium = data["total"][5];

                  this.recipe = this.getRecipeStr(
                    data["countFood"],
                    data["keys"],
                    data["food"]
                  );

                  // this.dietRecords = response;
                  console.log(":: Get Diet Plan Record ", data);
                },
                (err) => {
                  console.log("Error Response>> ", err);
                  this.loaderService.hideLoader();
                }
              );
          } else if (type === "origin") {
            this.isOrigin = true;
            this.templateService.getRecordFromOrigin(this.id).subscribe(
              (response) => {
                this.loaderService.hideLoader();

                const data = response;
                // Assuming `response` contains the API response object
                Object.assign(this, data);

                this.Lactation_score_0_6 = data["0-6_Lactation_score"];
                this.Lactation_score_7_12 = data["7-12_Lactation_score"];
                this.Keto_Score = this.convertAndRound(this.Keto_Score);
                this.travel_rank = this.convertAndRound(this.travel_rank);
                this.gutelimination_rank = this.convertAndRound(
                  this.gutelimination_rank
                );
                this.Protein_percentage = data["Protein_percentage %"];

                // Check if the Type value exists in the typeOptions array
                console.log("ITem in Tpye:: ", data["Type"]);
                const exists = this.typeOptions.some(
                  (option) => option["value"] === data["Type"]
                );
                if (!exists) {
                  this.typeOptions.push({
                    label: data["Type"],
                    value: data["Type"],
                  });
                  console.log(`Added new type: ${data["Type"]}`);
                }

                const portionUnitExists = this.portionUnitOptions.some(
                  (option) => option["value"] === this.portion_unit
                );
                if (!portionUnitExists) {
                  this.portionUnitOptions.push(this.portion_unit);
                  console.log(`Added new type: ${this.portion_unit}`);
                }

                // this.dietRecords = response;
                console.log(":: Get Diet Plan Record ", data);
              },
              (err) => {
                console.log("Error Response>> ", err);
                this.loaderService.hideLoader();
              }
            );
          } else {
            this.templateService
              .getFoodDietPlanRecords(`?id=${this.id}`)
              .subscribe(
                (response) => {
                  this.loaderService.hideLoader();

                  const data = response[0];
                  // Assuming `response` contains the API response object
                  Object.assign(this, data);

                  this.Lactation_score_0_6 = data["0-6_Lactation_score"];
                  this.Lactation_score_7_12 = data["7-12_Lactation_score"];
                  this.Protein_percentage = data["Protein_percentage %"];

                  console.log("ITem in Tpye:: ", data["Type"]);
                  const exists = this.typeOptions.some(
                    (option) => option["value"] === data["Type"]
                  );
                  if (!exists) {
                    this.typeOptions.push({
                      label: data["Type"],
                      value: data["Type"],
                    });
                    console.log(`Added new type: ${data["Type"]}`);
                  }

                  const portionUnitExists = this.portionUnitOptions.some(
                    (option) => option["value"] === this.portion_unit
                  );
                  if (!portionUnitExists) {
                    this.portionUnitOptions.push(this.portion_unit);
                    console.log(`Added new type: ${this.portion_unit}`);
                  }

                  // this.dietRecords = response;
                  console.log(":: Get Diet Plan Record ", data);
                },
                (err) => {
                  console.log("Error Response>> ", err);
                  this.loaderService.hideLoader();
                }
              );
          }
        });
      }
    });
  }

  getRecipeStr(countFood, keys, food) {
    let recipeStr = "";

    for (let index = 0; index < countFood.length - 1; index++) {
      const foodElement = countFood[index];
      recipeStr +=
        index +
        1 +
        ". " +
        foodElement["name"] +
        " " +
        foodElement["qnty"] +
        " " +
        foodElement["sUnit"] +
        " \n";

      for (let yIndex = 0; yIndex < 8; yIndex++) {
        recipeStr += keys[yIndex] + ": " + food[index][yIndex] + "\n";
      }
      recipeStr += "\n";
    }

    return recipeStr;
  }

  createProfile() {
    const body = {
      companyName: this.companyName,
      dietitianId: localStorage.getItem("dietitianId"),
      dietitianName: localStorage.getItem("dietitianName"),
      Food: this.Food,
      foodType: this.foodType,
      status: this.isManisha ? "In Database" : this.status,
      Calories: this.Calories,
      itemCode: this.itemCode,
      hindiName: this.hindiName,
      recipeHindi: this.recipeHindi,
      Type: this.Type,
      Score: this.Score,
      Slots: this.Slots,
      Carbs: this.Carbs,
      Fat: this.Fat,
      Protien: this.Protien,
      Fiber: this.Fiber,
      season: this.season,
      Special_slot: this.Special_slot,
      Community: this.Community,
      AvoidIn: this.AvoidIn,
      RecommendedIn: this.RecommendedIn,
      Remark: this.Remark,
      Detox: this.Detox,
      Detox_rank: this.Detox_rank,
      Special_diet: this.Special_diet,
      code: this.code,
      Ultra_special: this.Ultra_special,
      immunity: this.immunity,
      immunity_special_slot: this.immunity_special_slot,
      during_covid: this.during_covid,
      during_covid_special_slot: this.during_covid_special_slot,
      post_covid: this.post_covid,
      post_covid_special_slot: this.post_covid_special_slot,
      courtesy: this.courtesy,
      recipe: this.recipe,
      steps: this.steps,
      stepsHindi: this.stepsHindi,
      video: this.video,
      gluten_free: this.gluten_free,
      source: this.source,
      portion: this.portion,
      portion_unit: this.portion_unit,
      country: this.country,
      brand: this.brand,
      Special_slot_canada: this.Special_slot_canada,
      Special_slot_australia: this.Special_slot_australia,
      Diabetes_score: this.Diabetes_score,
      Diabetes_special: this.Diabetes_special,
      BP_Score: this.BP_Score,
      BP_special: this.BP_special,
      Cholestrol_score: this.Cholestrol_score,
      Cholestrol_special: this.Cholestrol_special,
      PCOS_score: this.PCOS_score,
      PCOS_special: this.PCOS_special,
      net_carbs: this.net_carbs,
      WeightlossPlus_score: this.WeightlossPlus_score,
      WeightlossPlus_special: this.WeightlossPlus_special,
      recipeFlag: this.recipeFlag,
      trendingRecipe: this.trendingRecipe,
      muscle_building_score: this.muscle_building_score,
      special_slot_muscle_building_morning:
        this.special_slot_muscle_building_morning,
      special_slot_muscle_building_evening:
        this.special_slot_muscle_building_evening,
      Protein_percentage: this.Protein_percentage,
      Keto_Score: this.Keto_Score,
      Keto_Slot: this.Keto_Slot,
      Hypothyroid_Score: this.Hypothyroid_Score,
      Hypothyroid_Slot: this.Hypothyroid_Slot,
  autism_slot: this.autism_slot,
  adhd_slot:this.adhd_slot,
  autism_score: this.autism_score,
  adhd_score: this.adhd_score,
      Anti_Inflammatory_Score: this.Anti_Inflammatory_Score,
      Anti_Inflammatory_Slot: this.Anti_Inflammatory_Slot,
      Fatty_Liver_Score: this.Fatty_Liver_Score,
      Fatty_Liver_Slot: this.Fatty_Liver_Slot,
      Functional_Food_flag: this.Functional_Food_flag,
      Functional_foods: this.Functional_foods,
      Anemia_Score: this.Anemia_Score,
      Acidity_Score: this.Acidity_Score,
      Sleep_Score: this.Sleep_Score,
      Uric_Acid_Score: this.Uric_Acid_Score,
      Calcium: this.Calcium,
      Vitamin_D: this.Vitamin_D,
      Vitamin_B12_Score: this.Vitamin_B12_Score,
      Egypt_Plan: this.Egypt_Plan,
      Alkaline_Score: this.Alkaline_Score,
      Alkaline_Slot: this.Alkaline_Slot,
      Prediabetes_Score: this.Prediabetes_Score,
      Prediabetes_Slot: this.Prediabetes_Slot,
      High_Protein_Score: this.High_Protein_Score,
      High_Protein_Slot: this.High_Protein_Slot,
      T1_Pregnancy_score: this.T1_Pregnancy_score,
      T1_Pregnancy_slot: this.T1_Pregnancy_slot,
      T2_Pregnancy_slot: this.T2_Pregnancy_slot,
      T2_Pregnancy_score: this.T2_Pregnancy_score,
      T3_Pregnancy_slot: this.T3_Pregnancy_slot,
      T3_Pregnancy_score: this.T3_Pregnancy_score,
      Postpartum_score: this.Postpartum_score,
      Postpartum_slot: this.Postpartum_slot,
      "0-6_Lactation_score": this.Lactation_score_0_6,
      "0-6_Lactation_slot": this.Lactation_slot_0_6,
      "7-12_Lactation_score": this.Lactation_score_7_12,
      "7-12_Lactation_slot": this.Lactation_slot_7_12,
      waterRetentionScore: this.waterRetentionScore,
      waterRetentionSlot: this.waterRetentionSlot,
      fiteloWeightLoss_Score: this.fiteloWeightLoss_Score,
      fiteloWeightLossSlot: this.fiteloWeightLossSlot,
      lunchCheatDayRank: this.lunchCheatDayRank,
      lunchCheatDaySlot: this.lunchCheatDaySlot,
      dinnerCheatDayRank: this.dinnerCheatDayRank,
      dinnerCheatDaySlot: this.dinnerCheatDaySlot,
      cookingProficiency: this.cookingProficiency,
      cookingTime: this.cookingTime,
      detoxFiteloRank: this.detoxFiteloRank,
      detoxFiteloSlot: this.detoxFiteloSlot,
      travel_rank: this.travel_rank,
      travel_slot: this.travel_slot,
      Stress_rank: this.Stress_rank,
      Stress_slot: this.Stress_slot,
      Skincare_rank: this.Skincare_rank,
      Skincare_slot: this.Skincare_slot,
      gutelimination_rank: this.gutelimination_rank,
      gutelimination_slot: this.gutelimination_slot,
      Fodmap_rank: this.Fodmap_rank,
      Fodmap_slot: this.Fodmap_slot,
      kidsbelow3_rank: this.kidsbelow3_rank,
      kidsbelow3_slot: this.kidsbelow3_slot,
      kidsabove3_rank: this.kidsabove3_rank,
      kidsabove3_slot: this.kidsabove3_slot,
      SmartCalories: this.SmartCalories,
      BuyUrl: this.BuyUrl,
    };

    this.loaderService.presentLoader("Creating Plan...").then((_) => {
      this.templateService.createFoodDietRecord(body).subscribe(
        (response) => {
          console.log("Created FoodPlan :: ", response);
          this._id = response["_id"];

          if (this.isManisha) {
            this.handleSaveToOrigin(response);
          } else {
            this.loaderService.hideLoader();
            this.utilities.presentAlert("Record Created");
            this.router.navigate(["food-search"]);
          }
        },
        (error) => {
          console.log("Create Food DietPlan Error: ", error);
          this.utilities.presentAlert("Some Error Occured!");
          this.loaderService.hideLoader();
        }
      );
    });
  }

  updateOriginRecipe() {
    const body = {
      companyName: this.companyName,
      dietitianId: localStorage.getItem("dietitianId"),
      dietitianName: localStorage.getItem("dietitianName"),
      Food: this.Food,
      foodType: this.foodType,
      status: this.status,
      Calories: this.Calories,
      itemCode: this.itemCode,
      hindiName: this.hindiName,
      recipeHindi: this.recipeHindi,
      Type: this.Type,
      Score: this.Score,
      Slots: this.Slots,
      Carbs: this.Carbs,
      Fat: this.Fat,
      Protien: this.Protien,
      Fiber: this.Fiber,
      season: this.season,
      Special_slot: this.Special_slot,
      Community: this.Community,
      AvoidIn: this.AvoidIn,
      RecommendedIn: this.RecommendedIn,
      Remark: this.Remark,
      Detox: this.Detox,
      Detox_rank: this.Detox_rank,
      Special_diet: this.Special_diet,
      code: this.code,
      Ultra_special: this.Ultra_special,
      immunity: this.immunity,
      immunity_special_slot: this.immunity_special_slot,
      during_covid: this.during_covid,
      during_covid_special_slot: this.during_covid_special_slot,
      post_covid: this.post_covid,
      post_covid_special_slot: this.post_covid_special_slot,
      courtesy: this.courtesy,
      recipe: this.recipe,
      steps: this.steps,
      stepsHindi: this.stepsHindi,
      video: this.video,
      gluten_free: this.gluten_free,
      source: this.source,
      portion: this.portion,
      portion_unit: this.portion_unit,
      country: this.country,
      brand: this.brand,
      Special_slot_canada: this.Special_slot_canada,
      Special_slot_australia: this.Special_slot_australia,
      Diabetes_score: this.Diabetes_score,
      Diabetes_special: this.Diabetes_special,
      BP_Score: this.BP_Score,
      BP_special: this.BP_special,
      Cholestrol_score: this.Cholestrol_score,
      Cholestrol_special: this.Cholestrol_special,
      PCOS_score: this.PCOS_score,
      PCOS_special: this.PCOS_special,
      net_carbs: this.net_carbs,
      WeightlossPlus_score: this.WeightlossPlus_score,
      WeightlossPlus_special: this.WeightlossPlus_special,
      recipeFlag: this.recipeFlag,
      trendingRecipe: this.trendingRecipe,
      muscle_building_score: this.muscle_building_score,
      special_slot_muscle_building_morning:
        this.special_slot_muscle_building_morning,
      special_slot_muscle_building_evening:
        this.special_slot_muscle_building_evening,
      "Protein_percentage %": this.Protein_percentage,
      Keto_Score: this.Keto_Score,
      Keto_Slot: this.Keto_Slot,
      Hypothyroid_Score: this.Hypothyroid_Score,
      Hypothyroid_Slot: this.Hypothyroid_Slot,
      Anti_Inflammatory_Score: this.Anti_Inflammatory_Score,
    autism_slot: this.autism_slot,
    adhd_slot:this.adhd_slot,
    autism_score: this.autism_score,
    adhd_score: this.adhd_score,
      Anti_Inflammatory_Slot: this.Anti_Inflammatory_Slot,
      Fatty_Liver_Score: this.Fatty_Liver_Score,
      Fatty_Liver_Slot: this.Fatty_Liver_Slot,
      Functional_Food_flag: this.Functional_Food_flag,
      Functional_foods: this.Functional_foods,
      Anemia_Score: this.Anemia_Score,
      Acidity_Score: this.Acidity_Score,
      Sleep_Score: this.Sleep_Score,
      Uric_Acid_Score: this.Uric_Acid_Score,
      Calcium: this.Calcium,
      Vitamin_D: this.Vitamin_D,
      Vitamin_B12_Score: this.Vitamin_B12_Score,
      Egypt_Plan: this.Egypt_Plan,
      Alkaline_Score: this.Alkaline_Score,
      Alkaline_Slot: this.Alkaline_Slot,
      Prediabetes_Score: this.Prediabetes_Score,
      Prediabetes_Slot: this.Prediabetes_Slot,
      High_Protein_Score: this.High_Protein_Score,
      High_Protein_Slot: this.High_Protein_Slot,
      T1_Pregnancy_score: this.T1_Pregnancy_score,
      T1_Pregnancy_slot: this.T1_Pregnancy_slot,
      T2_Pregnancy_slot: this.T2_Pregnancy_slot,
      T2_Pregnancy_score: this.T2_Pregnancy_score,
      T3_Pregnancy_slot: this.T3_Pregnancy_slot,
      T3_Pregnancy_score: this.T3_Pregnancy_score,
      Postpartum_score: this.Postpartum_score,
      Postpartum_slot: this.Postpartum_slot,
      "0-6_Lactation_score": this.Lactation_score_0_6,
      "0-6_Lactation_slot": this.Lactation_slot_0_6,
      "7-12_Lactation_score": this.Lactation_score_7_12,
      "7-12_Lactation_slot": this.Lactation_slot_7_12,
      waterRetentionScore: this.waterRetentionScore,
      waterRetentionSlot: this.waterRetentionSlot,
      fiteloWeightLoss_Score: this.fiteloWeightLoss_Score,
      fiteloWeightLossSlot: this.fiteloWeightLossSlot,
      lunchCheatDayRank: this.lunchCheatDayRank,
      lunchCheatDaySlot: this.lunchCheatDaySlot,
      dinnerCheatDayRank: this.dinnerCheatDayRank,
      dinnerCheatDaySlot: this.dinnerCheatDaySlot,
      cookingProficiency: this.cookingProficiency,
      cookingTime: this.cookingTime,
      detoxFiteloRank: this.detoxFiteloRank,
      detoxFiteloSlot: this.detoxFiteloSlot,
      travel_rank: this.travel_rank,
      travel_slot: this.travel_slot,
      Stress_rank: this.Stress_rank,
      Stress_slot: this.Stress_slot,
      Skincare_rank: this.Skincare_rank,
      Skincare_slot: this.Skincare_slot,
      gutelimination_rank: this.gutelimination_rank,
      gutelimination_slot: this.gutelimination_slot,
      Fodmap_rank: this.Fodmap_rank,
      Fodmap_slot: this.Fodmap_slot,
      kidsbelow3_rank: this.kidsbelow3_rank,
      kidsbelow3_slot: this.kidsbelow3_slot,
      kidsabove3_rank: this.kidsabove3_rank,
      kidsabove3_slot: this.kidsabove3_slot,
      SmartCalories: this.SmartCalories,
      BuyUrl: this.BuyUrl,
    };

    this.loaderService.presentLoader("Updating Recipe...").then((_) => {
      this.templateService
        .updateDietPlanRecordToOrigin(this.id, body)
        .subscribe(
          (response) => {
            console.log("Updated Origin FoodPlan :: ", response);

            this.loaderService.hideLoader();
            this.utilities.presentAlert("Record Updated");
            // food-search
            // this.router.navigate(["food-search"]);
          },
          (error) => {
            console.log("Update Food DietPlan Error: ", error);
            this.utilities.presentAlert("Some Error Occured!");
            this.loaderService.hideLoader();
          }
        );
    });
  }

  updateProfile() {
    const body = {
      dietitianId: this.dietitianId,
      dietitianName: this.dietitianName,
      Food: this.Food,
      foodType: this.foodType,
      status: this.status,
      Calories: this.Calories,
      itemCode: this.itemCode,
      hindiName: this.hindiName,
      recipeHindi: this.recipeHindi,
      Score: this.Score,
      Type: this.Type,
      Slots: this.Slots,
      Carbs: this.Carbs,
      Fat: this.Fat,
      Protien: this.Protien,
      Fiber: this.Fiber,
      season: this.season,
      Special_slot: this.Special_slot,
      Community: this.Community,
      AvoidIn: this.AvoidIn,
      RecommendedIn: this.RecommendedIn,
      Remark: this.Remark,
      Detox: this.Detox,
      Detox_rank: this.Detox_rank,
      Special_diet: this.Special_diet,
      code: this.code,
      Ultra_special: this.Ultra_special,
      updated_by: localStorage.getItem("dietitianId"),
      updated_by_name: localStorage.getItem("dietitianName"),
      immunity: this.immunity,
      immunity_special_slot: this.immunity_special_slot,
      during_covid: this.during_covid,
      during_covid_special_slot: this.during_covid_special_slot,
      post_covid: this.post_covid,
      post_covid_special_slot: this.post_covid_special_slot,
      courtesy: this.courtesy,
      recipe: this.recipe,
      steps: this.steps,
      stepsHindi: this.stepsHindi,
      video: this.video,
      gluten_free: this.gluten_free,
      source: this.source,
      portion: this.portion,
      portion_unit: this.portion_unit,
      country: this.country,
      brand: this.brand,
      Special_slot_canada: this.Special_slot_canada,
      Special_slot_australia: this.Special_slot_australia,
      Diabetes_score: this.Diabetes_score,
      Diabetes_special: this.Diabetes_special,
      BP_Score: this.BP_Score,
      BP_special: this.BP_special,
      Cholestrol_score: this.Cholestrol_score,
      Cholestrol_special: this.Cholestrol_special,
      PCOS_score: this.PCOS_score,
      PCOS_special: this.PCOS_special,
      net_carbs: this.net_carbs,
      WeightlossPlus_score: this.WeightlossPlus_score,
      WeightlossPlus_special: this.WeightlossPlus_special,
      recipeFlag: this.recipeFlag,
      trendingRecipe: this.trendingRecipe,
      muscle_building_score: this.muscle_building_score,
      special_slot_muscle_building_morning:
        this.special_slot_muscle_building_morning,
      special_slot_muscle_building_evening:
        this.special_slot_muscle_building_evening,
      "Protein_percentage %": this["Protein_percentage"],
      Keto_Score: this.Keto_Score,
      Keto_Slot: this.Keto_Slot,
      Hypothyroid_Score: this.Hypothyroid_Score,
      Hypothyroid_Slot: this.Hypothyroid_Slot,
    autism_slot: this.autism_slot,
    adhd_slot:this.adhd_slot,
    autism_score: this.autism_score,
    adhd_score: this.adhd_score,
      Anti_Inflammatory_Score: this.Anti_Inflammatory_Score,
      Anti_Inflammatory_Slot: this.Anti_Inflammatory_Slot,
      Fatty_Liver_Score: this.Fatty_Liver_Score,
      Fatty_Liver_Slot: this.Fatty_Liver_Slot,
      Functional_Food_flag: this.Functional_Food_flag,
      Functional_foods: this.Functional_foods,
      Anemia_Score: this.Anemia_Score,
      Acidity_Score: this.Acidity_Score,
      Sleep_Score: this.Sleep_Score,
      Uric_Acid_Score: this.Uric_Acid_Score,
      Calcium: this.Calcium,
      Vitamin_D: this.Vitamin_D,
      Vitamin_B12_Score: this.Vitamin_B12_Score,
      Egypt_Plan: this.Egypt_Plan,
      Alkaline_Score: this.Alkaline_Score,
      Alkaline_Slot: this.Alkaline_Slot,
      Prediabetes_Score: this.Prediabetes_Score,
      Prediabetes_Slot: this.Prediabetes_Slot,
      High_Protein_Score: this.High_Protein_Score,
      High_Protein_Slot: this.High_Protein_Slot,
      T1_Pregnancy_score: this.T1_Pregnancy_score,
      T1_Pregnancy_slot: this.T1_Pregnancy_slot,
      T2_Pregnancy_slot: this.T2_Pregnancy_slot,
      T2_Pregnancy_score: this.T2_Pregnancy_score,
      T3_Pregnancy_slot: this.T3_Pregnancy_slot,
      T3_Pregnancy_score: this.T3_Pregnancy_score,
      Postpartum_score: this.Postpartum_score,
      Postpartum_slot: this.Postpartum_slot,
      "0-6_Lactation_score": this.Lactation_score_0_6,
      "0-6_Lactation_slot": this.Lactation_slot_0_6,
      "7-12_Lactation_score": this.Lactation_score_7_12,
      "7-12_Lactation_slot": this.Lactation_slot_7_12,
      waterRetentionScore: this.waterRetentionScore,
      waterRetentionSlot: this.waterRetentionSlot,
      fiteloWeightLoss_Score: this.fiteloWeightLoss_Score,
      fiteloWeightLossSlot: this.fiteloWeightLossSlot,
      lunchCheatDayRank: this.lunchCheatDayRank,
      lunchCheatDaySlot: this.lunchCheatDaySlot,
      dinnerCheatDayRank: this.dinnerCheatDayRank,
      dinnerCheatDaySlot: this.dinnerCheatDaySlot,
      cookingProficiency: this.cookingProficiency,
      cookingTime: this.cookingTime,
      detoxFiteloRank: this.detoxFiteloRank,
      detoxFiteloSlot: this.detoxFiteloSlot,
      travel_rank: this.travel_rank,
      travel_slot: this.travel_slot,
      Stress_rank: this.Stress_rank,
      Stress_slot: this.Stress_slot,
      Skincare_rank: this.Skincare_rank,
      Skincare_slot: this.Skincare_slot,
      gutelimination_rank: this.gutelimination_rank,
      gutelimination_slot: this.gutelimination_slot,
      Fodmap_rank: this.Fodmap_rank,
      Fodmap_slot: this.Fodmap_slot,
      kidsbelow3_rank: this.kidsbelow3_rank,
      kidsbelow3_slot: this.kidsbelow3_slot,
      kidsabove3_rank: this.kidsabove3_rank,
      kidsabove3_slot: this.kidsabove3_slot,
      SmartCalories: this.SmartCalories,
      BuyUrl: this.BuyUrl,
    };

    this.loaderService.presentLoader("Updating Plan...").then((_) => {
      this.templateService.updateFoodDietRecord(body, this.id).subscribe(
        (response) => {
          console.log("Created FoodPlan :: ", response);

          this.loaderService.hideLoader();
          this.utilities.presentAlert("Record Updated");
          this.router.navigate(["food-search"]);
        },
        (error) => {
          console.log("Create Food DietPlan Error: ", error);
          this.loaderService.hideLoader();
        }
      );
    });
  }

  handleAcceptClick() {
    // alert("Acccept: "+ id)
    // rejected/approved
    const body = {
      status: "approved",
    };

    this.loaderService.presentLoader("Accepting Plan...").then((_) => {
      this.templateService.updateFoodDietRecord(body, this.id).subscribe(
        (response) => {
          console.log("Created FoodPlan :: ", response);

          this.loaderService.hideLoader();
          this.status = "approved";

          this.utilities.presentAlert("Recipe Accepted");
          this.router.navigate(["food-search"]);
        },
        (error) => {
          console.log("Accept Food DietPlan Error: ", error);
          this.loaderService.hideLoader();
        }
      );
    });
  }

  dietPlanId: string;
  reason: string;
  rejectPopup: Boolean = false;
  resubmitPopup: Boolean = false;
  resubmissionReason: string;

  showRejectPopup() {
    this.rejectPopup = true;
    this.dietPlanId = this.id;
  }

  closePopup() {
    this.rejectPopup = false;
  }

  handleRejectClick() {
    const body = {
      status: "rejected",
      rejectionReason: this.reason,
    };
    const id = this.dietPlanId;

    this.loaderService.presentLoader("Rejecting Plan...").then((_) => {
      this.templateService.updateFoodDietRecord(body, id).subscribe(
        (response) => {
          console.log("Rejected FoodPlan :: ", response);

          this.loaderService.hideLoader();
          this.status = "rejected";
          this.closePopup();
          this.utilities.presentAlert("Recipe Rejected");
          this.router.navigate(["food-search"]);
        },
        (error) => {
          console.log("Reject Food DietPlan Error: ", error);
          this.loaderService.hideLoader();
          this.closePopup();
        }
      );
    });
  }

  showResubmitReasonPopup() {
    this.resubmitPopup = true;
    this.dietPlanId = this.id;
  }

  closeResumitReasonPopup() {
    this.resubmitPopup = false;
  }

  handleResubmittedClick() {
    const body = {
      status: "resubmitted",
      resubmissionReason: this.resubmissionReason,
      dietitianId: this.dietitianId,
      dietitianName: this.dietitianName,
      Food: this.Food,
      foodType: this.foodType,
      Calories: this.Calories,
      itemCode: this.itemCode,
      hindiName: this.hindiName,
      recipeHindi: this.recipeHindi,
      Score: this.Score,
      Type: this.Type,
      Slots: this.Slots,
      Carbs: this.Carbs,
      Fat: this.Fat,
      Protien: this.Protien,
      Fiber: this.Fiber,
      season: this.season,
      Special_slot: this.Special_slot,
      Community: this.Community,
      AvoidIn: this.AvoidIn,
      RecommendedIn: this.RecommendedIn,
      Remark: this.Remark,
      Detox: this.Detox,
      Detox_rank: this.Detox_rank,
      Special_diet: this.Special_diet,
      code: this.code,
      Ultra_special: this.Ultra_special,
      updated_by: localStorage.getItem("dietitianId"),
      updated_by_name: localStorage.getItem("dietitianName"),
      immunity: this.immunity,
      immunity_special_slot: this.immunity_special_slot,
      during_covid: this.during_covid,
      during_covid_special_slot: this.during_covid_special_slot,
      post_covid: this.post_covid,
      post_covid_special_slot: this.post_covid_special_slot,
      courtesy: this.courtesy,
      recipe: this.recipe,
      steps: this.steps,
      stepsHindi: this.stepsHindi,
      video: this.video,
      gluten_free: this.gluten_free,
      source: this.source,
      portion: this.portion,
      portion_unit: this.portion_unit,
      country: this.country,
      brand: this.brand,
      Special_slot_canada: this.Special_slot_canada,
      Special_slot_australia: this.Special_slot_australia,
      Diabetes_score: this.Diabetes_score,
      Diabetes_special: this.Diabetes_special,
      BP_Score: this.BP_Score,
      BP_special: this.BP_special,
      Cholestrol_score: this.Cholestrol_score,
      Cholestrol_special: this.Cholestrol_special,
      PCOS_score: this.PCOS_score,
      PCOS_special: this.PCOS_special,
      net_carbs: this.net_carbs,
      WeightlossPlus_score: this.WeightlossPlus_score,
      WeightlossPlus_special: this.WeightlossPlus_special,
      recipeFlag: this.recipeFlag,
      trendingRecipe: this.trendingRecipe,
      muscle_building_score: this.muscle_building_score,
      special_slot_muscle_building_morning:
        this.special_slot_muscle_building_morning,
      special_slot_muscle_building_evening:
        this.special_slot_muscle_building_evening,
      "Protein_percentage %": this["Protein_percentage"],
      Keto_Score: this.Keto_Score,
      Keto_Slot: this.Keto_Slot,
      Hypothyroid_Score: this.Hypothyroid_Score,
      Hypothyroid_Slot: this.Hypothyroid_Slot,
    autism_slot: this.autism_slot,
    adhd_slot:this.adhd_slot,
    autism_score: this.autism_score,
    adhd_score: this.adhd_score,
      Anti_Inflammatory_Score: this.Anti_Inflammatory_Score,
      Anti_Inflammatory_Slot: this.Anti_Inflammatory_Slot,
      Fatty_Liver_Score: this.Fatty_Liver_Score,
      Fatty_Liver_Slot: this.Fatty_Liver_Slot,
      Functional_Food_flag: this.Functional_Food_flag,
      Functional_foods: this.Functional_foods,
      Anemia_Score: this.Anemia_Score,
      Acidity_Score: this.Acidity_Score,
      Sleep_Score: this.Sleep_Score,
      Uric_Acid_Score: this.Uric_Acid_Score,
      Calcium: this.Calcium,
      Vitamin_D: this.Vitamin_D,
      Vitamin_B12_Score: this.Vitamin_B12_Score,
      Egypt_Plan: this.Egypt_Plan,
      Alkaline_Score: this.Alkaline_Score,
      Alkaline_Slot: this.Alkaline_Slot,
      Prediabetes_Score: this.Prediabetes_Score,
      Prediabetes_Slot: this.Prediabetes_Slot,
      High_Protein_Score: this.High_Protein_Score,
      High_Protein_Slot: this.High_Protein_Slot,
      T1_Pregnancy_score: this.T1_Pregnancy_score,
      T1_Pregnancy_slot: this.T1_Pregnancy_slot,
      T2_Pregnancy_slot: this.T2_Pregnancy_slot,
      T2_Pregnancy_score: this.T2_Pregnancy_score,
      T3_Pregnancy_slot: this.T3_Pregnancy_slot,
      T3_Pregnancy_score: this.T3_Pregnancy_score,
      Postpartum_score: this.Postpartum_score,
      Postpartum_slot: this.Postpartum_slot,
      "0-6_Lactation_score": this.Lactation_score_0_6,
      "0-6_Lactation_slot": this.Lactation_slot_0_6,
      "7-12_Lactation_score": this.Lactation_score_7_12,
      "7-12_Lactation_slot": this.Lactation_slot_7_12,
      waterRetentionScore: this.waterRetentionScore,
      waterRetentionSlot: this.waterRetentionSlot,
      fiteloWeightLoss_Score: this.fiteloWeightLoss_Score,
      fiteloWeightLossSlot: this.fiteloWeightLossSlot,
      lunchCheatDayRank: this.lunchCheatDayRank,
      lunchCheatDaySlot: this.lunchCheatDaySlot,
      dinnerCheatDayRank: this.dinnerCheatDayRank,
      dinnerCheatDaySlot: this.dinnerCheatDaySlot,
      cookingProficiency: this.cookingProficiency,
      cookingTime: this.cookingTime,
      detoxFiteloRank: this.detoxFiteloRank,
      detoxFiteloSlot: this.detoxFiteloSlot,
      travel_rank: this.travel_rank,
      travel_slot: this.travel_slot,
      Stress_rank: this.Stress_rank,
      Stress_slot: this.Stress_slot,
      Skincare_rank: this.Skincare_rank,
      Skincare_slot: this.Skincare_slot,
      gutelimination_rank: this.gutelimination_rank,
      gutelimination_slot: this.gutelimination_slot,
      Fodmap_rank: this.Fodmap_rank,
      Fodmap_slot: this.Fodmap_slot,
      kidsbelow3_rank: this.kidsbelow3_rank,
      kidsbelow3_slot: this.kidsbelow3_slot,
      kidsabove3_rank: this.kidsabove3_rank,
      kidsabove3_slot: this.kidsabove3_slot,
      SmartCalories: this.SmartCalories,
      BuyUrl: this.BuyUrl,
    };
    const id = this.dietPlanId;

    this.loaderService.presentLoader("Submitting Receipe...").then((_) => {
      this.templateService.updateFoodDietRecord(body, id).subscribe(
        (response) => {
          console.log("Resubmitted FoodPlan :: ", response);

          this.loaderService.hideLoader();
          this.status = "resubmitted";
          this.closePopup();
          this.utilities.presentAlert("Recipe Submitted");
          this.router.navigate(["food-search"]);
        },
        (error) => {
          console.log("Resubmitted Food DietPlan Error: ", error);
          this.loaderService.hideLoader();
          this.closePopup();
        }
      );
    });
  }

  handleUploadToDietPlanClick() {
    const body = {
      _id: this._id,
      status: "resubmitted",
      resubmissionReason: this.resubmissionReason,
      dietitianId: this.dietitianId || this.myId,
      dietitianName:
        this.dietitianName || localStorage.getItem("dietitianName"),
      Food: this.Food,
      foodType: this.foodType,
      Calories: this.Calories,
      itemCode: this.itemCode,
      hindiName: this.hindiName,
      recipeHindi: this.recipeHindi,
      Score: this.Score,
      Type: this.Type,
      Slots: this.Slots,
      Carbs: this.Carbs,
      Fat: this.Fat,
      Protien: this.Protien,
      Fiber: this.Fiber,
      season: this.season,
      Special_slot: this.Special_slot,
      Community: this.Community,
      AvoidIn: this.AvoidIn,
      RecommendedIn: this.RecommendedIn,
      Remark: this.Remark,
      Detox: this.Detox,
      Detox_rank: this.Detox_rank,
      Special_diet: this.Special_diet,
      code: this.code,
      Ultra_special: this.Ultra_special,
      updated_by: localStorage.getItem("dietitianId"),
      updated_by_name: localStorage.getItem("dietitianName"),
      immunity: this.immunity,
      immunity_special_slot: this.immunity_special_slot,
      during_covid: this.during_covid,
      during_covid_special_slot: this.during_covid_special_slot,
      post_covid: this.post_covid,
      post_covid_special_slot: this.post_covid_special_slot,
      courtesy: this.courtesy,
      recipe: this.recipe,
      steps: this.steps,
      stepsHindi: this.stepsHindi,
      video: this.video,
      gluten_free: this.gluten_free,
      source: this.source,
      portion: this.portion,
      portion_unit: this.portion_unit,
      country: this.country,
      brand: this.brand,
      Special_slot_canada: this.Special_slot_canada,
      Special_slot_australia: this.Special_slot_australia,
      Diabetes_score: this.Diabetes_score,
      Diabetes_special: this.Diabetes_special,
      BP_Score: this.BP_Score,
      BP_special: this.BP_special,
      Cholestrol_score: this.Cholestrol_score,
      Cholestrol_special: this.Cholestrol_special,
      PCOS_score: this.PCOS_score,
      PCOS_special: this.PCOS_special,
      net_carbs: this.net_carbs,
      WeightlossPlus_score: this.WeightlossPlus_score,
      WeightlossPlus_special: this.WeightlossPlus_special,
      recipeFlag: this.recipeFlag,
      trendingRecipe: this.trendingRecipe,
      muscle_building_score: this.muscle_building_score,
      special_slot_muscle_building_morning:
        this.special_slot_muscle_building_morning,
      special_slot_muscle_building_evening:
        this.special_slot_muscle_building_evening,
      "Protein_percentage %": this["Protein_percentage"],
      Keto_Score: this.Keto_Score,
      Keto_Slot: this.Keto_Slot,
      Hypothyroid_Score: this.Hypothyroid_Score,
      Hypothyroid_Slot: this.Hypothyroid_Slot,
    autism_slot: this.autism_slot,
    adhd_slot:this.adhd_slot,
    autism_score: this.autism_score,
    adhd_score: this.adhd_score,
      Anti_Inflammatory_Score: this.Anti_Inflammatory_Score,
      Anti_Inflammatory_Slot: this.Anti_Inflammatory_Slot,
      Fatty_Liver_Score: this.Fatty_Liver_Score,
      Fatty_Liver_Slot: this.Fatty_Liver_Slot,
      Functional_Food_flag: this.Functional_Food_flag,
      Functional_foods: this.Functional_foods,
      Anemia_Score: this.Anemia_Score,
      Acidity_Score: this.Acidity_Score,
      Sleep_Score: this.Sleep_Score,
      Uric_Acid_Score: this.Uric_Acid_Score,
      Calcium: this.Calcium,
      Vitamin_D: this.Vitamin_D,
      Vitamin_B12_Score: this.Vitamin_B12_Score,
      Egypt_Plan: this.Egypt_Plan,
      Alkaline_Score: this.Alkaline_Score,
      Alkaline_Slot: this.Alkaline_Slot,
      Prediabetes_Score: this.Prediabetes_Score,
      Prediabetes_Slot: this.Prediabetes_Slot,
      High_Protein_Score: this.High_Protein_Score,
      High_Protein_Slot: this.High_Protein_Slot,
      T1_Pregnancy_score: this.T1_Pregnancy_score,
      T1_Pregnancy_slot: this.T1_Pregnancy_slot,
      T2_Pregnancy_slot: this.T2_Pregnancy_slot,
      T2_Pregnancy_score: this.T2_Pregnancy_score,
      T3_Pregnancy_slot: this.T3_Pregnancy_slot,
      T3_Pregnancy_score: this.T3_Pregnancy_score,
      Postpartum_score: this.Postpartum_score,
      Postpartum_slot: this.Postpartum_slot,
      "0-6_Lactation_score": this.Lactation_score_0_6,
      "0-6_Lactation_slot": this.Lactation_slot_0_6,
      "7-12_Lactation_score": this.Lactation_score_7_12,
      "7-12_Lactation_slot": this.Lactation_slot_7_12,
      waterRetentionScore: this.waterRetentionScore,
      waterRetentionSlot: this.waterRetentionSlot,
      fiteloWeightLoss_Score: this.fiteloWeightLoss_Score,
      fiteloWeightLossSlot: this.fiteloWeightLossSlot,
      lunchCheatDayRank: this.lunchCheatDayRank,
      lunchCheatDaySlot: this.lunchCheatDaySlot,
      dinnerCheatDayRank: this.dinnerCheatDayRank,
      dinnerCheatDaySlot: this.dinnerCheatDaySlot,
      cookingProficiency: this.cookingProficiency,
      cookingTime: this.cookingTime,
      detoxFiteloRank: this.detoxFiteloRank,
      detoxFiteloSlot: this.detoxFiteloSlot,
      travel_rank: this.travel_rank,
      travel_slot: this.travel_slot,
      Stress_rank: this.Stress_rank,
      Stress_slot: this.Stress_slot,
      Skincare_rank: this.Skincare_rank,
      Skincare_slot: this.Skincare_slot,
      gutelimination_rank: this.gutelimination_rank,
      gutelimination_slot: this.gutelimination_slot,
      Fodmap_rank: this.Fodmap_rank,
      Fodmap_slot: this.Fodmap_slot,
      kidsbelow3_rank: this.kidsbelow3_rank,
      kidsbelow3_slot: this.kidsbelow3_slot,
      kidsabove3_rank: this.kidsabove3_rank,
      kidsabove3_slot: this.kidsabove3_slot,
      SmartCalories: this.SmartCalories,
      BuyUrl: this.BuyUrl,
    };

    this.loaderService
      .presentLoader("Uploading data to Diet plan...")
      .then((_) => {
        this.templateService.saveDietPlanRecordToOrigin(body).subscribe(
          (response) => {
            this.loaderService.hideLoader();
            response["status"] = "submitted";
            console.log(
              "Get getRecordsaveDietPlanRecordToOriginFromOrigin API Response:: ",
              response
            );
            this.utilities.presentAlert("Added Record to Collection");
          },
          (error) => {
            this.loaderService.hideLoader();
            console.log("saveDietPlanRecordToOrigin Error: ", error);
            this.utilities.presentAlert("Something went wrong");
          }
        );
      });
  }

  handleSaveToOrigin(body: Object) {
    this.templateService.saveDietPlanRecordToOrigin(body).subscribe(
      (response) => {
        this.loaderService.hideLoader();
        this.utilities.presentAlert("Record Created");
        this.router.navigate(["food-search"]);
      },
      (error) => {
        this.loaderService.hideLoader();
        console.log("saveDietPlanRecordToOrigin Error: ", error);
        this.utilities.presentAlert("Something went wrong");
      }
    );
  }

  showTypeSelector: boolean = false;
  typeEntered: string;

  closeTypeSelectorPopup() {
    this.showTypeSelector = false;
  }

  async onTypeChange(event: any) {
    const selectedValue = event.detail.value;
    console.log("Selected value:", selectedValue); // Debugging

    if (selectedValue === "add-new") {
      this.Type = null;

      this.showTypeSelector = true;

      // await this.showAddNewTypeAlert();
    }
  }

  handleSaveNewOption() {
    const newOption = {
      label: this.typeEntered,
      value: this.typeEntered,
    };
    this.typeOptions.push(newOption);
    // Optionally set the newly added option as selected
    this.Type = newOption.value;
    this.showTypeSelector = false;
  }

  convertAndRound(value: string): string {
    // Parse the string into a float
    const floatValue = parseFloat(value);

    // Round it to the nearest whole number
    const roundedValue = Math.round(floatValue);

    return roundedValue.toString();
  }

  calculateCaloreiesRelated() {
    const p: number = parseFloat(this.Protien) || 0;
    const f: number = parseFloat(this.Fiber) || 0;
    var c = parseFloat(this.Calories) || 1;
    const carbs = parseFloat(this.Carbs) || 0;
    console.log("Protein= ", p, " :: Calories= ", c);
    this.Protein_percentage = (((p * 4) / c) * 100).toFixed(1).toString();
    this.net_carbs = (carbs - f).toFixed(1).toString();
  }

  calculateSmartCalories() {
    const cal = parseFloat(this.Calories);
    const dbyScore: number = parseFloat(this.Diabetes_score);
    // Base calculation
    const baseCalories = cal / 35;

    // Factor based on DiabetesScore
    let factor: number;
    switch (dbyScore) {
      case 3:
        factor = 1.2;
        break;
      case 6:
        factor = 1.1;
        break;
      case 9:
        factor = 0.9;
        break;
      case 1:
        factor = 1.3;
        break;
      case -1:
        factor = 1.5;
        break;
      default:
        throw new Error(
          "Invalid DiabetesScore. Supported values: 3, 6, 9, 1, -1."
        );
    }

    const calculatedValue = baseCalories * factor;
    console.log("Calaulaed SmartCalories:: ", calculatedValue);
    // Calculate and return SmartCalories
    this.SmartCalories = parseFloat(calculatedValue.toFixed(1));
  }
}
