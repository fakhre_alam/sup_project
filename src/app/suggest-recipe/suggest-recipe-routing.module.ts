import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { NwDietPlanPage } from "./suggest-recipe.page";

const routes: Routes = [
  {
    path: "",
    component: NwDietPlanPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NwDietPlanPageRoutingModule {}
