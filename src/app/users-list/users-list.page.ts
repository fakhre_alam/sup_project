import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AppService } from '../services';
@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.page.html',
  styleUrls: ['./users-list.page.scss'],
})
export class UsersListPage implements OnInit {

  userForm: FormGroup;
  submitted = true;
  constructor(private formBuilder: FormBuilder, private appService: AppService) {
    this.userForm = this.formBuilder.group(
      {
        username: [null, [Validators.required]],
        firstName: [null, [Validators.required]],
        lastName: [null, [Validators.required]],
        email: [null, [Validators.required, Validators.email]],
        password: [null, [Validators.required]],
        phone: [null, [Validators.required]]
      }
    );
  }
  
  get f() { return this.userForm.controls; }
  update() {
    // stop here if form is invalid
    if (this.userForm.invalid) {
      this.submitted = true;
      return;
    }

    this.userForm.value.userStatus = 0;
    this.userForm.value.id = 0;

    console.log("this.userForm.value", this.userForm.value);
    this.appService.userUpdate(this.userForm.value.username, this.userForm.value).subscribe(success => {
      console.log("success", success);
      this.submitted = false;
      this.onReset();
    }, error => {
      console.log("error", error);
    });
   
  }
  onReset() {
    this.submitted = false;
    this.userForm.reset();
  }
  delete() {
    if (this.userForm.value.username) {
      this.appService.userDelete(this.userForm.value.username).subscribe(success => {
        console.log("success", success);
        this.submitted = false;
        this.onReset();
      }, error => {
        console.log("error", error);
      });
    }
  }
  ngOnInit() {
  }

}
