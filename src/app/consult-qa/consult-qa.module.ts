import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ConsultQaPageRoutingModule } from "./consult-qa-routing.module";

import { ConsultQaPage } from "./consult-qa.page";

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, ConsultQaPageRoutingModule],
  declarations: [
    ConsultQaPage,
   
  ],
})
export class ConsultQaPageModule {}
