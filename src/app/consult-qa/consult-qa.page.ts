import { Component, OnInit } from "@angular/core";
import consultQA from "../../app/shared/constants/consultQA.json";
import { AppService } from "../app.service";
import { AppService as appS } from "../services/app/app.service";
import { UTILITIES } from "../core/utility/utilities";
import { TemplateService } from "../services";
@Component({
  selector: "app-consult-qa",
  templateUrl: "./consult-qa.page.html",
  styleUrls: ["./consult-qa.page.scss"],
})
export class ConsultQaPage implements OnInit {
  consultQA = [];
  lifeStyle: any;
  userId = localStorage.getItem("email");
  qaId = "";


  constructor(
    private appServices: AppService,
    private appServe: appS,
    private utilities: UTILITIES,
    private templateService: TemplateService
  ) {
    this.templateService.getQaByUser(this.userId).subscribe(
      (response) => {
        console.log("!!!!!!!!!!--QA-Result >> ", response);
        if (response[0]) {
          console.log("$$$$$$$$$$$$$$$$$$ ", response[0]);
          this.qaId = response[0]?._id;
        }
      },
      (error) => {
        console.error("------------>> Error Getting instrutions", error.error);
      }
    );
    if (this.consultQA.length === 0) {
      this.consultQA = [...consultQA.consultQA];
      this.qaId = "";
    }
  }

  ngOnInit() {
   
  }
  ionViewWillEnter(){
    this.getQA();
  }
  getQA(){
    this.templateService.getQaByUser(localStorage.getItem("email")).subscribe(
      (response) => {
        console.log("!!!!!!!!!!--QA-Result >> ", response);
        if (response[0]) {
          console.log("$$$$$$$$$$$$$$$$$$ ", response[0]);
          this.consultQA = response[0].content;
          for (let index = 0; index < this.consultQA.length; index++) {
          this.lifeStyle?.consultQA.filter((item) => {
            if (this.consultQA[index].id === item.id) {
              this.consultQA[index].answer = item.answer;
            }
          });
          }
        }
      },
      (error) => {
        console.error("------------>> Error Getting instrutions", error.error);
      }
    );
    
  }
  bindData() {
    this.appServices.getProfile().then(
      (res: any) => {
        this.lifeStyle = res.lifeStyle;
        for (let index = 0; index < this.consultQA.length; index++) {
          if (this.lifeStyle?.consultQA !== undefined) {
            this.lifeStyle?.consultQA.filter((item) => {
              if (this.consultQA[index].id === item.id) {
                this.consultQA[index].answer = item.answer;
              }
            });
          }
        }
      },
      (err) => {
        console.log("profile error", err);
      }
    );
  }
  submitConsultQA() {
    console.log("Consult QA>>> ", this.consultQA);
    if (this.qaId != "") {
      this.templateService
        .updateQaByUser(this.qaId, this.userId, "createrId", this.consultQA)
        .subscribe(
          (response) => {
            // dietitianId, actionId, userId,  actionName,  companyId
            this.utilities.presentAlert("Consult QA Updated Successfully.");
            if(localStorage.getItem("dietitianId")!=null){
            this.templateService
              .handleDietitianAction(
                localStorage.getItem("dietitianId"),
                "668d71e11073b512dd2b9389",
                localStorage.getItem("email"),
                "Update Consult QA",
                localStorage.getItem("companyId")
              )
              .subscribe((dRedf) => {
                console.log("Saved Insructions>> ", response);
              });
            }
          },
          (error) => {
            alert("ewrror");
            console.error("Error Adding instrutions", error.error);
          }
        );
    } else {
      this.templateService
        .createQa(this.userId, "createrId", this.consultQA)
        .subscribe(
          (response) => {
            console.log("Saved Insructions>> ", response);
            if (response) {
              this.qaId = response["_id"];
              console.log("Id>> ", this.qaId);
            }
            this.utilities.presentAlert("Consult QA saved Successfully.");
          },
          (error) => {
            alert("ewrror");
            console.error("Error Adding instrutions", error.error);
          }
        );
    }
    // this.appServe.getUserToken(localStorage.getItem("email")).subscribe(
    //   (response) => {
    //     console.log(response);
    //   },
    //   (error) => {
    //     debugger;
    //     this.lifeStyle.consultQA=[];
    // for (let index = 0; index < this.consultQA.length; index++) {
    //   if(this.consultQA[index].answer.trim()!==''){
    //     this.lifeStyle.consultQA.push(
    //     {id:this.consultQA[index].id,question:this.consultQA[index].question,answer:this.consultQA[index].answer}
    //     );
    //   }
    // }
    //  this.appServices.postLifeStyle(this.lifeStyle,error.error.text).then(res=>{
    //   console.log("instructions response:- ",res);
    //   this.utilities.presentAlert("Consult QA saved successfully.");
    //   },err=>{
    //   console.log("instructions error:-",err);

    //   });
    // });
  }
  back() {
    window.history.back();
  }
}
