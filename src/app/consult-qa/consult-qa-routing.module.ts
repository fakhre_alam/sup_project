import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsultQaPage } from './consult-qa.page';

const routes: Routes = [
  {
    path: '',
    component: ConsultQaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsultQaPageRoutingModule {}
