import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchLeadPage } from './search-lead.page';

const routes: Routes = [
  {
    path: '',
    component: SearchLeadPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchLeadPageRoutingModule {}
