import { Component, OnInit } from "@angular/core";
import { ModalController } from "@ionic/angular";
import { SummaryUpdatePage } from "../modal/summary-update/summary-update.page";
import { Router, ActivatedRoute } from "@angular/router";
import { StorageService, AppService, LoaderService } from "../services";
import moment from "moment";

@Component({
  selector: "app-search-lead",
  templateUrl: "./search-lead.page.html",
  styleUrls: ["./search-lead.page.scss"],
})
export class SearchLeadPage implements OnInit {
  isMoreLeads: boolean = false;
  leadsLimit: number = 4;
  searchLeadData;
  fillterdSearchLeadData;
  searchFilterredList: any = [];
  subOptions: any = [];
  searchQueryType;
  searchQueryCode;

  constructor(
    public modalController: ModalController,
    private router: Router,
    private route: ActivatedRoute,
    private appService: AppService,
    private storageService: StorageService,
    private loaderService: LoaderService
  ) {
    this.route.queryParams.subscribe((res) => {
      this.searchQueryType = res.searchQueryType;
      this.searchQueryCode = res.searchQueryCode;
      this.dataOnInIt(res.searchQueryType, res.searchQueryCode);
    });
  }

  dateFormat(dt) {
    return moment(dt).format("DD-MM-YYYY");
  }

  subOptionName(id) {
    let subOptionName = this.subOptions.filter((ele) => {
      return ele.code == id;
    });
    if(subOptionName.length) return subOptionName[0]["name"];
    else return "";

  }

  dataOnInIt(type, code) {
    this.loaderService.presentLoader("Please Wait").then((_) => {
    let search = type + "=" + code;
      this.appService.searchLead(search).subscribe((response) => {
        if (response) {
        this.loaderService.hideLoader();
          this.appService.refs().subscribe(
            (val) => {
              console.log("test-refs",val);
              if (val) {
                this.subOptions = val.subOptionTypes;
                this.searchLeadData = response.content;
                this.searchLeadData= this.searchLeadData.sort((a,b)=>{
                let date1 ={
                  year: b.leadInfo.createdAt.split(' ')[0].split('-')[2],
                  month: b.leadInfo.createdAt.split(' ')[0].split('-')[1],
                  day: b.leadInfo.createdAt.split(' ')[0].split('-')[0]
              }
              let date2 ={
                year: a.leadInfo.createdAt.split(' ')[0].split('-')[2],
                month: a.leadInfo.createdAt.split(' ')[0].split('-')[1],
                day: a.leadInfo.createdAt.split(' ')[0].split('-')[0]
            }
                  return   new Date(date2.year,Number(date2.month)-1,date2.day) <  new Date(date1.year,Number(date1.month)-1,date1.day)?1:-1;
              });
                this.leadSearch(null);
              }
            },
            (error) => {
              // this.loaderService.hideLoader();
              console.log("Category Error", error);
            }
          );
          // this.storageService.get("refs").then((val) => {
          //   this.subOptions = val.subOptionTypes;
          //   this.searchLeadData = response.content;
          //   console.log("Response for search ", this.searchLeadData);
          //   this.leadSearch(null);
          // });
        }
        (error) => {
          this.loaderService.hideLoader();
        }
      });
    });
  }

  setFalseForAllList() {
    this.searchFilterredList.filter((ele) => {
      ele.isMoreLeads = false;
      return ele;
    });
  }

  ngOnInit() {}

  leadSearch(searchValue) {
    let val =
      searchValue && searchValue.target && searchValue.target.value
        ? searchValue.target.value
        : null;
    if (!val) {
      this.searchFilterredList = [...this.searchLeadData];
    } else {
      this.searchFilterredList = this.searchLeadData.filter((lead) => {
        if (lead.customerInfo == null) {
          return lead.leadInfo.id.includes(val);
        } else {
          return (
            lead.customerInfo.profile.name.includes(val) ||
            lead.customerInfo._id.includes(val) ||
            lead.leadInfo.status.includes(val)
          );
        }
      });
    }
    this.setFalseForAllList();
  }

  showLeadsMore(i) {
    this.searchFilterredList[i]["isMoreLeads"] =
      !this.searchFilterredList[i]["isMoreLeads"];
    // this.isMoreLeads = !this.isMoreLeads;
    this.leadsLimit = this.isMoreLeads ? 4 : 8;
  }

  selectedLeadItem;
  updateSummary(leadItem, i) {
    this.selectedLeadItem = leadItem;
    this.openSummaryUpdate();
  }

  toNormalLabel(text){
    if(text){
      const result = text.replace(/([A-Z])/g, " $1");
      return result.charAt(0).toUpperCase() + result.slice(1);
    }else return "";
    
  }

  async openSummaryUpdate() {    
    console.log("Lead item ", this.selectedLeadItem);
    const modal = await this.modalController.create({
      component: SummaryUpdatePage,
      cssClass: "open-summary-update-modal",
      backdropDismiss: true,
      componentProps: {
        leadData: this.selectedLeadItem,
        searchQueryType: this.searchQueryType,
        searchQueryCode: this.searchQueryCode
      },
    });
    modal.onDidDismiss().then((data: any) => {
      if (data && data.data && data.data.searchQueryCode) {
         this.dataOnInIt(data.data.searchQueryType, data.data.searchQueryCode);
      }
     

    });
    return await modal.present();
  }
}
