import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SearchLeadPageRoutingModule } from './search-lead-routing.module';

import { SearchLeadPage } from './search-lead.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SearchLeadPageRoutingModule
  ],
  declarations: [SearchLeadPage]
})
export class SearchLeadPageModule {}
