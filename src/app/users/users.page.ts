import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { AppService } from '../services';
@Component({
  selector: 'app-users',
  templateUrl: './users.page.html',
  styleUrls: ['./users.page.scss'],
})
export class UsersPage implements OnInit {

  userForm:FormGroup;
  submitted = false;
  constructor(private formBuilder: FormBuilder,private appService: AppService) { 
   this.userForm = this.formBuilder.group(
    {
      username:[null,[Validators.required]],
      firstName:[null,[Validators.required]],
      lastName:[null,[Validators.required]],
      email:[null,[Validators.required,Validators.email]],
      password:[null,[Validators.required]],
      phone:[null,[Validators.required]]
    }
    );
  }
  get f() { return this.userForm.controls; }
  onSubmit() {
   
    // stop here if form is invalid
    if (this.userForm.invalid) {
      this.submitted = true;
        return;
    }

    this.userForm.value.userStatus=0;
    this.userForm.value.id=0;
   
    console.log("this.userForm.value",this.userForm.value);
     this.appService.userAdd(this.userForm.value).subscribe(success=>{
       console.log("success",success);
       this.submitted = false;
     },error=>{
      console.log("error",error);
     });
    // display form values on success
  
   
}
  onReset() {
    this.submitted = false;
    this.userForm.reset();
}
  ngOnInit() {
  }

}
