import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [{
      path: 'consume',
      loadChildren: () => import('../consume-v/consume-v.module').then( m => m.ConsumeVPageModule)
    },
      {
        path: 'summary-lead',
        loadChildren: () => import('../summary-lead/summary-lead.module').then( m => m.SummaryLeadPageModule)
      },
      {
        path: 'home',
        loadChildren: () =>
          import('../home/home.module').then((m) => m.HomePageModule),
      },
      {
        path: 'customers',
        loadChildren: () =>
          import('../customers/customers.module').then(
            (m) => m.CustomersPageModule
          ),
      },
      {
        path: 'admin',
        loadChildren: () => import('../admin/admin.module').then( m => m.AdminPageModule)
      },
      {
        path: '',
        redirectTo: '/login',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
})
export class TabsPageRoutingModule {}
