import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-tabs",
  templateUrl: "tabs.page.html",
  styleUrls: ["tabs.page.scss"],
})
export class TabsPage {
  username="";
  constructor(private router: Router,public alertController: AlertController,) {
    this.username = localStorage.getItem("loginEmail");
  }
ionViewWillEnter(){
  localStorage.clear();
  this.router.navigate(["./login"]);
}
  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: "my-custom-class",
      message:
        "<b>Are you sure you want to Logout your account? </b>",
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          cssClass: "my-custom-class",
          handler: () => {
            console.log("cancel");
          },
        },
        {
          text: "Confirm",
          handler: () => {
            this.logout();
            console.log("Account Logout");
          },
        },
      ],
    });
    await alert.present();
  }
  logout() {
    localStorage.clear();
    this.router.navigate(["./login"]);
  }
}
