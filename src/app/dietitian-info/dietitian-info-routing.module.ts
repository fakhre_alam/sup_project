import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { DietitianInfoPage } from "./dietitian-info.page";

const routes: Routes = [
  {
    path: "",
    component: DietitianInfoPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DietitianInfoPageRoutingModule {}
