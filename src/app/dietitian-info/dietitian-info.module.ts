import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { DietitianInfoPageRoutingModule } from "./dietitian-info-routing.module";

import { DietitianInfoPage } from "./dietitian-info.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DietitianInfoPageRoutingModule,
  ],
  declarations: [DietitianInfoPage],
})
export class DietitianInfoPageModule {}
