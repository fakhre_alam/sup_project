import { Component, OnInit } from "@angular/core";
import { LoaderService, TemplateService } from "../services";
import { UTILITIES } from "../core/utility/utilities";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-dietitian-info",
  templateUrl: "./dietitian-info.page.html",
  styleUrls: ["./dietitian-info.page.scss"],
})
export class DietitianInfoPage implements OnInit {
  dietitianId: string;
  maxFreeRecord: string = null;
  linkedinId: string = null;
  mobileNumber: string = null;
  referredBy: string = null;
  verified: boolean = false;
  isDisabled: boolean = false;
  _id: string;
  name: string;
  emailId: string;
  companyId: string;
  calendlyId: string;
  whatsappNum: string = "";
  role: string;
  speciality: string = "";
  about: string;
  gender: string;
  whatsappVisible: boolean = true;
  calendlyVisible: boolean = false;
  createdOn: string;
  lastLogin: string;
  noOfUsersCanBeAdded: number = 0;
  planExpiryDate: string;
  image: string;
  imageType: string;
  theme: string;
  URLClientId: string;
  companyKey: string;
  urlClientId: string;
  avatar: string;

  constructor(
    private loaderService: LoaderService,
    private utilities: UTILITIES,
    private templateService: TemplateService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.route.queryParamMap.subscribe((params) => {
      this.dietitianId = params.get("id");

      if (this.dietitianId) {
        // alert(this.id);
        this.loaderService.presentLoader("Fetching details...").then((_) => {
          this.templateService
            .getDietitianRecord(`id=${this.dietitianId}`)
            .subscribe(
              (response) => {
                this.loaderService.hideLoader();
                const data = response;
                // Assuming `response` contains the API response object
                Object.assign(this, data);

                const file = data["image"];
                if (file) {
                  this.avatar = `https://nodeapi.smartdietplanner.com/api/getResFromPath/${file}`;
                }

                console.log(":: Get Dietitian Record ", data);
              },
              (err) => {
                console.log("Error Response>> ", err);
                this.loaderService.hideLoader();
              }
            );
        });
      }
    });
  }

  backHandler() {
    this.router.navigate(["food-search"]);
  }
}
