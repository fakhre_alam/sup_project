import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateDetailSlotPage } from './update-detail-slot.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateDetailSlotPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateDetailSlotPageRoutingModule {}
