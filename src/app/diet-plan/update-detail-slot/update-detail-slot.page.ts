import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CONSTANTS } from 'src/app/core/constants/constants';
import { UTILITIES } from 'src/app/core/utility/utilities';
import { AppService } from 'src/app/services';

@Component({
  selector: 'app-update-detail-slot',
  templateUrl: './update-detail-slot.page.html',
  styleUrls: ['./update-detail-slot.page.scss'],
})
export class UpdateDetailSlotPage implements OnInit {
  updateSlot:any=[{name:'sabji', count:14},{name:'Curry', count:14},{name:'Raita', count:14},{name:'Bread', count:14}];
  slot=1;
  slotName='';
  cat='';
  constructor(private readonly utilities:UTILITIES,private route:ActivatedRoute,private router:Router,private appService:AppService) { 
    this.route.queryParams.subscribe(res=>{
      console.log("response:-",res);
      this.slot = res.slot;
      this.cat= res.cat;
      this.slotName=res.name;
      
    })
  }


  ngOnInit() {
    this.fetchFoodDetails();
  }
  isVisibal(isA,isB){
    if(!isA && !isB){
      return false;
    }
    return true;
  }
  isShow=false;
  bkpData:any=[];
  fetchFoodDetails(){
    const payload={
      email:localStorage.getItem('email'),
      slot:this.slot,
      dietPlan:this.slotName,
      cat:this.cat
    }
    this.appService.fetchFoodDetails(payload).subscribe(res=>{
      console.log("res",res);
      this.updateSlot = this.utilities.customSort(res);
      this.bkpData = JSON.stringify(this.updateSlot);
    },err=>{
      console.log("error",err);
    });
  }
  gotoUpdate(){
    this.router.navigate(['./confirm-diet']);
  }

  gotoDetail(item){
   // this.router.navigate(['update-detail-slot'],{queryParams:{param:item.name}})
  }

  updatePAyload(data){
  //  const data=JSON.parse(this.bkpData);
  const tempData=[];
  for (let index = 0; index < data.length; index++) {
    tempData.push({diet:data[index].diet,option:data[index].option,_id:data[index]._id});
    
  }
  return tempData;
  }
  reset(){
   const tempDa = this.updatePAyload(JSON.parse(this.bkpData));
   // this.updateSlot = JSON.parse(this.bkpData);
    console.log("reset",this.updateSlot);
    
    const reqPayload={
      email:localStorage.getItem('email'),
      slot:this.slot,
      cat:this.cat,
      data:tempDa
    }

    this.appService.updateFoodItems(reqPayload).subscribe(res=>{
      console.log("update slot",res);
      },err=>{
    console.log("error",err);
  });
 }
  updateFoodItems(){
    const tempDa = this.updatePAyload(this.updateSlot);
    const reqPayload={
      email:localStorage.getItem('email'),
      slot:this.slot,
      cat:this.cat,
      data:tempDa
    }
    console.log("update slot payload:",tempDa);
    this.appService.updateFoodItems(reqPayload).subscribe(res=>{
      console.log("update slot",res);
      },err=>{
    console.log("error",err);
  });
  }
}
