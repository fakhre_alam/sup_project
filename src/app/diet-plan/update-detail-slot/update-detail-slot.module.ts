import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateDetailSlotPageRoutingModule } from './update-detail-slot-routing.module';

import { UpdateDetailSlotPage } from './update-detail-slot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateDetailSlotPageRoutingModule
  ],
  declarations: [UpdateDetailSlotPage]
})
export class UpdateDetailSlotPageModule {}
