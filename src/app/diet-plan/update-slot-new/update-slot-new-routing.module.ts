import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateSlotNewPage } from './update-slot-new.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateSlotNewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateSlotNewPageRoutingModule {}
