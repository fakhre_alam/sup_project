import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { UTILITIES } from 'src/app/core/utility/utilities';
import { AppService as mainService } from 'src/app/app.service';
import { AppService } from 'src/app/services';
import { CONSTANTS } from 'src/app/core/constants/constants';
@Component({
  selector: 'app-update-slot-new',
  templateUrl: './update-slot-new.page.html',
  styleUrls: ['./update-slot-new.page.scss'],
})
export class UpdateSlotNewPage implements OnInit {
  updateSlot:any=[{name:'sabji', count:14},{name:'Curry', count:14},{name:'Raita', count:14},{name:'Bread', count:14}];
  slot=1;
  slotName='';
  bkpData:any=[];
  category="";
  constructor(private readonly utilities:UTILITIES,private route:ActivatedRoute,private router:Router,private appService:AppService,
    private mainService:mainService) { 
    this.route.queryParams.subscribe(res=>{
      console.log("response:-",res);
      this.slot = res.slot;
      this.slotName = res.name;
      this.category = res.category;
    })
  }

  ngOnInit() {
    this.fetchSlotsDetails();
  }
  
  isShow=false;
  fetchSlotsDetails(){
    console.log("Const",CONSTANTS.dietDate);
    
    const payload={
      email:localStorage.getItem("email"),
      slot:this.slot,
      dietPlan: this.slotName
    }
    this.mainService.getOptionsManagementNew(this.slot,'fakhre.alam101@gmail.com',this.category,CONSTANTS.dietDate,'keto').then(res=>{
      
      this.updateSlot = this.utilities.customSort(res);
      console.log("resmain", this.updateSlot);
      this.bkpData = JSON.stringify(this.updateSlot);
      for (let index = 0; index < this.updateSlot.length; index++) {
        this.updateSlot[index].order=0;
      }
    },err=>{
      console.log("error",err);
    });
  }

  isVisibal(isA,isB){
    if(!isA && !isB){
      return false;
    }
    return true;
  }
  gotoUpdate(){
    this.router.navigate(['./confirm-diet']);
  }


  gotoDetail(item){
    console.log("sdiet",item);
    
    this.router.navigate(['update-detail-slot'],{queryParams:{name:this.slotName,cat:item.category,slot:this.slot}})
  }
  reset(){
    const tempDa = this.updatePAyload(JSON.parse(this.bkpData));
    console.log("reset",tempDa);
    
    this.mainService.updateOptionsManagementNew(this.slot,'fakhre.alam101@gmail.com',this.category,CONSTANTS.dietDate,'keto',tempDa).then(res=>{
      console.log("update slot",res);
      },err=>{
    console.log("error",err);
  });
 }
 updatePAyload(data){
  const tempData=[];
  console.log("this.updateSlot",this.updateSlot);
  
  for (let index = 0; index < data.length; index++) {
    if(data[index].option){
    tempData.push({"_id":data[index]._id});
    }
  
  }
  console.log("tempData",tempData);
  
  return tempData;
  }
  updateDiet(){
    const tempDa = this.updatePAyload(this.updateSlot);
    const reqPayload={
      email:localStorage.getItem('email'),
      slot:this.slot,
      data:tempDa
    }
    console.log("update slot payload:",tempDa);
    
    this.mainService.updateOptionsManagementNew(this.slot,'fakhre.alam101@gmail.com','D','30092022','keto',tempDa).then(res=>{
      console.log("update slot",res);
      },err=>{
    console.log("error",err);
  });
  }
}
