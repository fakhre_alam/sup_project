import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateSlotNewPageRoutingModule } from './update-slot-new-routing.module';

import { UpdateSlotNewPage } from './update-slot-new.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateSlotNewPageRoutingModule
  ],
  declarations: [UpdateSlotNewPage]
})
export class UpdateSlotNewPageModule {}
