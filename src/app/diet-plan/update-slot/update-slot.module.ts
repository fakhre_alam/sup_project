import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateSlotPageRoutingModule } from './update-slot-routing.module';

import { UpdateSlotPage } from './update-slot.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateSlotPageRoutingModule
  ],
  declarations: [UpdateSlotPage]
})
export class UpdateSlotPageModule {}
