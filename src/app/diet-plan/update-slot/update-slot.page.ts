import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CONSTANTS } from 'src/app/core/constants/constants';
import { UTILITIES } from 'src/app/core/utility/utilities';
import { AppService } from 'src/app/services';

@Component({
  selector: 'app-update-slot',
  templateUrl: './update-slot.page.html',
  styleUrls: ['./update-slot.page.scss'],
})
export class UpdateSlotPage implements OnInit {
  updateSlot:any=[{name:'sabji', count:14},{name:'Curry', count:14},{name:'Raita', count:14},{name:'Bread', count:14}];
  slot=1;
  slotName='';
  bkpData:any=[];
  constructor(private readonly utilities:UTILITIES,private route:ActivatedRoute,private router:Router,private appService:AppService) { 
    this.route.queryParams.subscribe(res=>{
      console.log("response:-",res);
      this.slot = res.slot;
      this.slotName = res.name;
      
    })
  }

  ngOnInit() {
    this.fetchSlotsDetails();
  }
  
  isShow=false;
  fetchSlotsDetails(){
    const payload={
      email:localStorage.getItem("email"),
      slot:this.slot,
      dietPlan: this.slotName
    }
    this.appService.fetchAllCategories(payload).subscribe(res=>{
      console.log("res",res);
      this.updateSlot = this.utilities.customSort(res);
      this.bkpData = JSON.stringify(this.updateSlot);
      for (let index = 0; index < this.updateSlot.length; index++) {
        this.updateSlot[index].order=0;
        
      }
    },err=>{
      console.log("error",err);
    });
  }

  isVisibal(isA,isB){
    if(!isA && !isB){
      return false;
    }
    return true;
  }
  gotoUpdate(){
    this.router.navigate(['./confirm-diet']);
  }


  gotoDetail(item){
    console.log("sdiet",item);
    
    this.router.navigate(['update-detail-slot'],{queryParams:{name:this.slotName,cat:item.category,slot:this.slot}})
  }
  reset(){
    const tempDa = this.updatePAyload(JSON.parse(this.bkpData));
    console.log("reset",tempDa);
    
    const reqPayload={
      email:localStorage.getItem('email'),
      slot:this.slot,
      data:tempDa
    }

    this.appService.updateAllCategories(reqPayload).subscribe(res=>{
      console.log("update slot",res);
      
      },err=>{
    console.log("error",err);
  });
 }
 updatePAyload(data){
  //  const data=JSON.parse(this.bkpData);
  const tempData=[];
  for (let index = 0; index < data.length; index++) {
    tempData.push({categories:data[index].category,diet:data[index].diet,option:data[index].option,order:data[index].order});
    
  }
  return tempData;
  }
  updateDiet(){
    const tempDa = this.updatePAyload(this.updateSlot);
    const reqPayload={
      email:localStorage.getItem('email'),
      slot:this.slot,
      data:tempDa
    }
    console.log("update slot payload:",tempDa);
    
    this.appService.updateAllCategories(reqPayload).subscribe(res=>{
      console.log("update slot",res);
      },err=>{
    console.log("error",err);
  });
  }
}
