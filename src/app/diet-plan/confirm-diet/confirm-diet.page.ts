import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CONSTANTS } from 'src/app/core/constants/constants';
import { AppService } from 'src/app/services';

@Component({
  selector: 'app-confirm-diet',
  templateUrl: './confirm-diet.page.html',
  styleUrls: ['./confirm-diet.page.scss'],
})
export class ConfirmDietPage implements OnInit {
  confirmDiet:any=[
    {diet:'When you Wake up (Slot 0)',name:['Drinks']}
  ,{diet:'Before Breakfast (slot 1)',name:['Nuts']},
  {diet:'Breakfast (slot 2)',name:['Snacks / Milk based snacks/ High protein snacks','Drink/Milk based drinks']},
  {diet:'Mid Day Meal (slot 3)',name:['Fruits','Drink']}
];
isSelected=new Array(20);
selectedName:any="";
  isActive:any=-1;
  constructor(private appService:AppService, private router:Router,private route:ActivatedRoute) { 
    this.route.queryParams.subscribe(res=>{
      this.selectedName = res.name;
      
    })
  }

  ngOnInit() {
    this.fetchSlotsDetails();
  }

  fetchSlotsDetails(){
    this.appService.fetchSlotsDetails(localStorage.getItem("email"),this.selectedName).subscribe(res=>{
      console.log("res",res);
      this.confirmDiet = res;
    },err=>{
      console.log("error",err);
    });
  }
  gotoUpdate(){
    console.log("this.confirmDiet.metaDataDietPlan",this.confirmDiet.metaDataDietPlan);
    
    this.appService.updateSlotDetail(this.confirmDiet.metaDataDietPlan,localStorage.getItem("email"),this.selectedName).subscribe(res=>{
      console.log("res",res);
      
    },err=>{
      console.log("error",err);
    });
  }
  gotoUpdateCat(index){
    this.router.navigate(['update-slot'],{queryParams:{name:this.selectedName,slot:index}})
  }
  confirmedDiet(item,index){
     console.log("item",item);
     
    this.confirmDiet.metaDataDietPlan[index].selected= item.selected;
    console.log("detail",this.confirmDiet.metaDataDietPlan);
    
    this.isActive = index;
  }

}
