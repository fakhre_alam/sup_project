import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConfirmDietPageRoutingModule } from './confirm-diet-routing.module';

import { ConfirmDietPage } from './confirm-diet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConfirmDietPageRoutingModule
  ],
  declarations: [ConfirmDietPage]
})
export class ConfirmDietPageModule {}
