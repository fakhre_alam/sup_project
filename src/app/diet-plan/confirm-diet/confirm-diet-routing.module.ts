import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConfirmDietPage } from './confirm-diet.page';

const routes: Routes = [
  {
    path: '',
    component: ConfirmDietPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConfirmDietPageRoutingModule {}
