import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/services';

@Component({
  selector: 'app-select-diet',
  templateUrl: './select-diet.page.html',
  styleUrls: ['./select-diet.page.scss'],
})
export class SelectDietPage implements OnInit {

  selectDiet:any=[{diet:'Weight Loss'},{diet:'Diabetes'},{diet:'Muscle build'}];
  isActive:any=-1;
  dietName:any = "" ;
  constructor(private appService:AppService, private router:Router) { }

  ngOnInit() {
    this.fetchDietPlanNames();
  }

  fetchDietPlanNames(){
    this.appService.fetchDietPlanNames().subscribe(res=>{
      console.log("res",res);
      this.selectDiet = res;
    },err=>{
      console.log("error",err);
    });
  }

  gotoConfirm(){
    this.router.navigate(['./confirm-diet'],{queryParams:{name:this.dietName}});
  }

  selectedDiet(item,index){
    this.dietName = item.dietPlan;
    this.isActive = index;
  }
}
