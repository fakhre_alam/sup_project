import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectDietPage } from './select-diet.page';

const routes: Routes = [
  {
    path: '',
    component: SelectDietPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectDietPageRoutingModule {}
