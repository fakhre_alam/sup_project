import { Component, OnInit } from "@angular/core";
import { AppService } from "src/app/app.service";
import { AppService as apS, LoaderService, TemplateService } from "../services";
import { DomSanitizer } from "@angular/platform-browser";
import { UTILITIES } from "../core/utility/utilities";
import { ActivatedRoute, Router } from "@angular/router";
@Component({
  selector: "app-add-recipe",
  templateUrl: "./add-recipe.page.html",
  styleUrls: ["./add-recipe.page.scss"],
})
export class AddRecipePage implements OnInit {
  searchfit = false;
  searchingData = false;
  copyData;
  totalDataSearchedData: any = [];
  flagX = true;
  newFood = "";
  isShow = false;
  foodItemArr = [];
  foodItemArrTotal = [];
  foodItemArrFinal = [];
  keys = [];
  valuesItems = [];
  values = [];

  constructor(
    private utilities: UTILITIES,
    private aps: apS,
    private loaderService: LoaderService,
    private appService: AppService,
    private route: ActivatedRoute,
    private templateService: TemplateService
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.route.queryParamMap.subscribe((params) => {
      this._id = params.get("id") == undefined ? "" : params.get("id");
      if (this._id) {
        // alert(this.id);
        this.loaderService.presentLoader("Fetching details...").then((_) => {
          this.templateService
            .getIngredientsRecipeRecords("?id=" + this._id)
            .subscribe(
              (res: any) => {
                this.loaderService.hideLoader();
                // alert(JSON.stringify(res));

                if (res.records.length > 0) {
                  this.recipeName = res.records[0].Food;
                  this.portion = res.records[0].portion;
                  this.portion_unit= res.records[0].portion_unit;
                  this.keys = res.records[0].keys;
                  this.addFoodFinal = [...res.records[0].countFood];
                  const itemwithouttotal = [...this.addFoodFinal];
                  const item = itemwithouttotal.pop();
                  this.addFood = [...itemwithouttotal];
                  this.foodItemArrFinal = [...res.records[0].food];
                  const rightSideItem = [...res.records[0].food];
                  this.foodItemArrTotal = rightSideItem.pop();
                  this.foodItemArr = [...rightSideItem];
                  this.tempfoodItemArr = [...rightSideItem];
                  //   this.valuesItems = items;
                } else {
                  this.utilities.presentAlert(
                    "No record found with Recipe Name " + this.recipeName
                  );
                }
              },
              (err) => {
                console.log("Error Response>> ", err);
                this.loaderService.hideLoader();
              }
            );
        });
      }
    });
  }

  onClear() {
    this.flagX = false;
    this.newFood = "";
  }
  servingUnit = "Select Serving Unit";
  quantity = 1;
  tempValue = [];
  addFood = [];
  addFoodFinal = [];
  addTotal: any = {};

  addFoodLength = 0;
  addItem() {
    if (this.newFood !== "") {
      this.foodItemArrTotal = [];
      this.addFood.push({
        name: this.newFood,
        sUnit: this.servingUnit,
        qnty: this.quantity,
        isEdit: true,
      });

      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Energy (kcal)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Carb (gm)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Protein (gm)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Fat (gm)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Fibre (gm)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Calcium (mg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Phosphorus (mg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Magnesium (mg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Sodium (mg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Potassium (mg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Iron (mg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Selenium (mcg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Zinc (mg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Vit A (mcg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Vit E (mg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Vit D2 (mcg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Vit D3 (mcg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Folate (mcg)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.valuesItems["Vit C (mg)"])).toFixed(2)
      );
      this.values = this.tempValue;
      this.foodItemArr.push(this.values);
      this.foodItemArrTotal.push(this.getColumnSums(this.foodItemArr));

      this.foodItemArrFinal = this.foodItemArr.concat(this.foodItemArrTotal);
      console.log("foodItemArrFinal", {
        design_recipe: [
          {
            countFood: this.addFood,
            keys: [
              "Energy (kcal)",
              "Carb (gm)",
              "Protein (gm)",
              "Fat (gm)",
              "Fibre (gm)",
              "Calcium (mg)",
              "Phosphorus (mg)",
              "Magnesium (mg)",
              "Sodium (mg)",
              "Potassium (mg)",
              "Iron (mg)",
              "Selenium (mcg)",
              "Zinc (mg)",
              "Vit A (mcg)",
              "Vit E (mcg)",
              "Vit D2 (mcg)",
              "Vit D3 (mcg)",
              "Folate (mcg)",
              "Vit C (mg)",
            ],
            food: this.foodItemArr,
            total: this.convertArrayInFloat(this.foodItemArrTotal),
          },
        ],
      });
      this.addFoodLength += 1;
      this.showAdd = true;

      this.addLeftfield();
      this.newFood = "";
    }
  }
  isRightCheck(item) {
    if (item.includes("Total")) {
      return true;
    }
    return false;
  }

  updateQuantity(qnty, ind) {
    this.quantity = qnty;
    this.foodItemArrTotal = [];
    this.tempValue = [];
    console.log("this.foodItemArr", this.foodItemArr);
    debugger;
    if (this._id !== "") {
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][0])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][1])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][2])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][3])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][4])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][5])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][6])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][7])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][8])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][9])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][10])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][11])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][12])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][13])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][14])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][15])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][16])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][17])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.tempfoodItemArr[ind][18])).toFixed(2)
      );
    } else {
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Energy (kcal)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.storeBaseItem[ind]["Carb (gm)"])).toFixed(2)
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Protein (gm)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.storeBaseItem[ind]["Fat (gm)"])).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.storeBaseItem[ind]["Fibre (gm)"])).toFixed(
          2
        )
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Calcium (mg)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Phosphorus (mg)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Magnesium (mg)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Sodium (mg)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Potassium (mg)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.storeBaseItem[ind]["Iron (mg)"])).toFixed(
          2
        )
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Selenium (mcg)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.storeBaseItem[ind]["Zinc (mg)"])).toFixed(
          2
        )
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Vit A (mcg)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.storeBaseItem[ind]["Vit E (mg)"])).toFixed(
          2
        )
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Vit D2 (mcg)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Vit D3 (mcg)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (
          this.quantity * Number(this.storeBaseItem[ind]["Folate (mcg)"])
        ).toFixed(2)
      );
      this.tempValue.push(
        (this.quantity * Number(this.storeBaseItem[ind]["Vit C (mg)"])).toFixed(
          2
        )
      );
    }
    this.foodItemArr[ind] = this.tempValue;
    this.foodItemArrTotal.push(this.getColumnSums(this.foodItemArr));
    this.foodItemArrFinal = this.foodItemArr.concat(this.foodItemArrTotal);
    this.editQnty[ind] = false;
    this.addLeftfield();
  }
  removeItem(index) {
    this.foodItemArrTotal = [];
    this.foodItemArr.splice(index, 1);
    this.addFood.splice(index, 1);
    this.addFoodFinal.splice(index, 1);
    this.storeBaseItem.splice(index, 1);
    this.tempfoodItemArr.splice(index, 1);
    if (this.foodItemArr.length == 0) {
      this.foodItemArrTotal = [];
      this.foodItemArrFinal = [];
      this.addFood = [];
      this.storeBaseItem = [];
      this.tempfoodItemArr = [];
    } else {
      this.foodItemArrTotal.push(this.getColumnSums(this.foodItemArr));
      this.foodItemArrFinal = this.foodItemArr.concat(this.foodItemArrTotal);
    }
    this.addLeftfield();
  }
  showAdd1 = false;
  changeUnit($event) {
    console.log($event.detail.value);
    this.showAdd1 = true;
  }
  editQnty = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  addLeftfield() {
    const temppush = [];
    this.addFoodFinal = [];
    temppush.push({
      name: `Total weight: <b>${this.addFood.reduce(
        (acc, val) => acc + Number(val.qnty === "" ? 0 : val.qnty),
        0
      )}</b> Grams`,
      qnty: "Total",
      sUnit: "",
      isEdit: false,
    });
    this.addFoodFinal = this.addFood.concat(temppush);
  }
  getColumnSums(arr) {
    // Initialize an array to hold the column-wise sums
    const columnSums = Array(arr[0].length).fill(0);

    // Iterate through each row and sum the values column-wise
    arr.forEach((row) => {
      row.forEach((value, colIndex) => {
        columnSums[colIndex] = Number(columnSums[colIndex]) + Number(value);
      });
    });

    return columnSums;
  }
  portion_unit="Bowl- 200 ml";
  portion=0;
  portionUnitOptions: string[] = [
    "Bowl- 200 ml",
    // "pc- (mention the quantity of 1 pc in grams)",
    "pc",
    "Katori- 150 ml",
    "Glass- 250ml",
    "quarter plate",
    "tsp- 5ml",
    "Cup- 200 ml",
    "tbsp- 15ml",
    "serving",
    "Portion",
  ];
  showAdd = false;
  fillFood(items) {
    console.log("fill food:-", items);
    
    this.servingUnit = "g(1.0)";
    this.quantity = 1;
    this.showAdd = true;
    this.newFood = "";
    this.flagX = true;
    this.isShow = true;
    this.newFood = items.food_name;
    this.totalDataSearchedData = [];
    this.keys = Object.keys(items);
    let tempkey = [];
    this.tempValue = [];
    tempkey.push("Energy (kcal)");
    tempkey.push("Carb (gm)");
    tempkey.push("Protein (gm)");
    tempkey.push("Fat (gm)");
    tempkey.push("Fibre (gm)");
    tempkey.push("Calcium (mg)");
    tempkey.push("Phosphorus (mg)");
    tempkey.push("Magnesium (mg)");
    tempkey.push("Sodium (mg)");
    tempkey.push("Potassium (mg)");
    tempkey.push("Iron (mg)");
    tempkey.push("Selenium (mcg)");
    tempkey.push("Zinc (mg)");
    tempkey.push("Vit A (mcg)");
    tempkey.push("Vit E (mg)");
    tempkey.push("Vit D2 (mcg)");
    tempkey.push("Vit D3 (mcg)");
    tempkey.push("Folate (mcg)");
    tempkey.push("Vit C (mg)");
    this.storeBaseItem.push(items);
    this.valuesItems = items;
    this.keys = tempkey;
  }
  storeBaseItem = [];
  getAllForSearch(searchTerm) {
    this.newFood = searchTerm;
    this.flagX = false;
    if (searchTerm === "") {
      return;
    }
    this.appService.searchFoodGradientsByName(searchTerm).then((resp: any) => {
      this.totalDataSearchedData = resp.records;
      console.log(resp);
    });
  }
  deleteRecipe() {
    this.appService.deleteRecipe(this._id).then(
      (res: any) => {
        console.log("res", res);
        this.foodItemArrTotal = [];
        this.foodItemArrFinal = [];
        this.addFood = [];
        this.storeBaseItem = [];
        this.utilities.presentAlert(res.message);
      },
      (err) => {
        console.log("err:-", err);
      }
    );
  }
  fixDecimal(itemNumber){
    return Number(itemNumber)?.toFixed(2);
  }
  updateRecipe() {
    const payload = {
      food_name: this.recipeName,
      portion: this.portion,
      portion_unit: this.portion_unit,
      countFood: this.addFoodFinal,
      keys: [
        "Energy (kcal)",
        "Carb (gm)",
        "Protein (gm)",
        "Fat (gm)",
        "Fibre (gm)",
        "Calcium (mg)",
        "Phosphorus (mg)",
        "Magnesium (mg)",
        "Sodium (mg)",
        "Potassium (mg)",
        "Iron (mg)",
        "Selenium (mcg)",
        "Zinc (mg)",
        "Vit A (mcg)",
        "Vit E (mcg)",
        "Vit D2 (mcg)",
        "Vit D3 (mcg)",
        "Folate (mcg)",
        "Vit C (mg)",
      ],
      food: this.foodItemArrFinal,
      total: this.convertArrayInFloat(this.foodItemArrTotal)
    };

    console.log("foodItemArrFinal", payload);

    this.appService.updateRecipe(payload, this._id).then(
      (res: any) => {
        console.log("res", res);
        this.utilities.presentAlert("Record updated Successfully");
      },
      (err) => {
        console.log("err:-", err);
      }
    );
  }
  _id = "";
  tempfoodItemArr = [];
  searchRecipe() {
    this.appService.getRecipe(this.recipeName).then(
      (res: any) => {
        console.log("res", res);
        if (res.records.length > 0) {
          debugger;
          this._id = res.records[0]._id;
          this.keys = res.records[0].keys;
          this.addFoodFinal = [...res.records[0].countFood];
          const itemwithouttotal = [...this.addFoodFinal];
          const item = itemwithouttotal.pop();
          this.addFood = [...itemwithouttotal];
          this.foodItemArrFinal = [...res.records[0].food];
          const rightSideItem = [...res.records[0].food];
          this.foodItemArrTotal = rightSideItem.pop();
          this.foodItemArr = [...rightSideItem];
          this.tempfoodItemArr = [...rightSideItem];
          //   this.valuesItems = items;
        } else {
          this.utilities.presentAlert(
            "No record found with Recipe Name " + this.recipeName
          );
        }
      },
      (err) => {
        console.log("err:-", err);
      }
    );
  }
  recipeName = "";
  saveRecipe() {
    if (this._id === "") {
      if (this.recipeName.trim() != "" && this.foodItemArrTotal?.length > 0) {
        this.saveRecipeMethod();
      } else {
        this.utilities.presentAlert("Recipe name can not be empty!");
      }
    } else {
      if (this.recipeName.trim() != "" && this.foodItemArrTotal?.length > 0) {
        this.updateRecipe();
      } else {
        this.utilities.presentAlert("Recipe name can not be empty!");
      }
    }
  }

  convertArrayInFloat(items){
    const arrayNumber=[];
    for (let i = 0; i < items[0].length; i++){
      arrayNumber.push(parseFloat(Number(items[0][i]).toFixed(2)));
    }
    return arrayNumber;
  }
  saveRecipeMethod() {
    const payload = {
      companyName: localStorage.getItem("companyId"),
      createdOn: new Date(),
      dietitianId: localStorage.getItem("dietitianId"),
      dietitianName: localStorage.getItem("dietitianName"),
      Food: this.recipeName,
      portion: this.portion,
      portion_unit: this.portion_unit,
      countFood: this.addFoodFinal,
      keys: [
        "Energy (kcal)",
        "Carb (gm)",
        "Protein (gm)",
        "Fat (gm)",
        "Fibre (gm)",
        "Calcium (mg)",
        "Phosphorus (mg)",
        "Magnesium (mg)",
        "Sodium (mg)",
        "Potassium (mg)",
        "Iron (mg)",
        "Selenium (mcg)",
        "Zinc (mg)",
        "Vit A (mcg)",
        "Vit E (mcg)",
        "Vit D2 (mcg)",
        "Vit D3 (mcg)",
        "Folate (mcg)",
        "Vit C (mg)",
      ],
      food: this.foodItemArrFinal,
      total: this.convertArrayInFloat(this.foodItemArrTotal)
    };

    console.log("foodItemArrFinal", payload);

    this.appService.saveRecipe(payload).then(
      (res: any) => {
        console.log("res", res);
        this.utilities.presentAlert("Record saved Successfully!");
      },
      (err) => {
        console.log("err:-", err);
      }
    );
  }
  close() {
    this.isShow = false;
  }
}
