import { Component, OnInit } from "@angular/core";
import { FormGroup } from "@angular/forms";
import { LoaderService, TemplateService } from "../services";
import { Router } from "@angular/router";

@Component({
  selector: "app-first-page",
  templateUrl: "./first-page.page.html",
  styleUrls: ["./first-page.page.scss"],
})
export class FirstPagePage implements OnInit {
  public loginForm: FormGroup;
  private username: string = "";
  private whatsapp: string = "";
  private mobileNumber: string = "";
  private calendlyId: string = "";
  rolesOptions: any = [
    "Dietitian",
    "Nutritionist",
    "Fitness Coach",
    "Doctor",
    // "Student",
  ];
  rolesSelected: string = "";
  wpVisible: boolean = false;
  gender: string = "Male";
  calendlyVisible: boolean = false;
  searchAllowed: boolean = true;
  about: string =
    "I am a dietitian with a strong background in nutrition and personalized health management. I specialize in creating tailored diet plans to help individuals reach their health goals.\nWith expertise in weight loss and fitness, I am dedicated to promoting long-term wellness through well-researched, science-based nutrition strategies";
  speciality: string = "Weight Loss, Fitness, Diabetes, PCOD";
  isIndividual = localStorage.getItem("isIndividual");
  URLClientIdVal: string = "alyve";
  selectedFile: File | null = null;
  selectedImage: string | undefined;
  themeSelected: string = "individual3";
  profileExists: boolean = false;
  dietitianId: string = "";
  linkedinId: string = "";
  referredBy: string = "";
  disabled: boolean = false;

  constructor(
    private templateService: TemplateService,
    private router: Router,
    private loaderService: LoaderService
  ) {}

  ionViewWillEnter() {
    localStorage.setItem("isDietitian", "true");
    const query = `mail=${localStorage.getItem("loginEmail")}`;

    this.loaderService.presentLoader("Fetching details...").then((_) => {
      this.templateService.getDietitianRecord(query).subscribe(
        (response) => {
          this.loaderService.hideLoader();
          console.log(":: Fetch Dietician Record ", response);
          if (response != null) {
            this.username = response["name"];
            this.about = response["about"];
            this.dietitianId = response["_id"];
            localStorage.setItem("loginEmail", response["emailId"]);
            localStorage.setItem("dietitianId", response["_id"]);
            localStorage.setItem("dietitianName", response["name"]);
            localStorage.setItem("companyId", response["companyId"]);
            localStorage.setItem("companyKey", response["companyKey"]);
            if (this.isIndividual && this.isIndividual === "true") {
              localStorage.setItem("urlClientId", response["URLClientId"]);
              localStorage.setItem("searchAllowed", response["searchAllowed"]);
            } else {
              localStorage.setItem("searchAllowed", "true");
              localStorage.setItem("urlClientId", response["urlClientId"]);
            }
            localStorage.setItem(
              "menu",
              `{
                "title":"Users Summary",
                "description":[{"name":"This section displays a summary of the latest 20 contacts","desc":[]},{"name":"It has actionable buttons for WhatsApp, scheduling calls, or viewing diet plan details.","desc":[]}],
                "menu":"Home",
                "subMenu":"",
                "activeIndex":0   
              }`
            );
            this.profileExists = true;
            if (response["isDisabled"]) {
              this.disabled = true;
            } else {
              this.router.navigate(["dashboard"]);
            }
          }
        },
        (err) => {
          console.log("Error Response>> ", err);
          this.loaderService.hideLoader();
        }
      );
    });
  }

  ngOnInit() {}

  submitBtnHandler() {
    this.loaderService.presentLoader("Saving details...").then((_) => {
      const payload = {
        name: this.username,
        emailId: localStorage.getItem("loginEmail"),
        calendlyId: this.calendlyId,
        whatsappNum: this.whatsapp,
        role: this.rolesSelected,
        speciality: this.speciality,
        about: this.about,
        whatsappVisible: this.wpVisible,
        calendlyVisible: this.calendlyVisible,
        gender: this.gender,
      };

      if (this.isIndividual && this.isIndividual === "true") {
        payload["searchAllowed"] = this.searchAllowed;
        payload["URLClientId"] = this.themeSelected
          ? this.themeSelected
          : this.URLClientIdVal;
        payload["theme"] = this.themeSelected;
        localStorage.setItem(
          "searchAllowed",
          this.searchAllowed ? "true" : "false"
        );
      } else {
        localStorage.setItem("searchAllowed", "true");
      }

      console.log("Payload>> ", payload);

      const formData = new FormData();
      for (const key in payload) {
        if (payload.hasOwnProperty(key)) {
          formData.append(key, payload[key]);
        }
      }

      if (this.selectedFile) {
        formData.append("image", this.selectedFile, this.selectedFile.name);
      }

      if (this.profileExists) {
        formData.append("_id", this.dietitianId);
        this.templateService.updateDietitian(formData).subscribe(
          (response) => {
            this.loaderService.hideLoader();
            console.log(":: Fetch Dietician Record ", response);
            if (response != null) {
              localStorage.setItem("dietitianId", response["_id"]);
              localStorage.setItem("dietitianName", response["name"]);
              localStorage.setItem("companyId", response["companyId"]);
              localStorage.setItem("companyKey", response["companyKey"]);
              localStorage.setItem("urlClientId", response["urlClientId"]);
              localStorage.setItem(
                "menu",
                `{
              "title":"Users Summary",
              "description":[{"name":"This section displays a summary of the latest 20 contacts","desc":[]},{"name":"It has actionable buttons for WhatsApp, scheduling calls, or viewing diet plan details.","desc":[]}],
              "menu":"Home",
              "subMenu":"",
              "activeIndex":0   
          }`
              );
              this.router.navigate(["dashboard"]);
            }
          },
          (error) => {
            console.log("User token ", error.error.text);
          }
        );
      } else {
        this.templateService.createDietitian(formData).subscribe(
          (response) => {
            this.loaderService.hideLoader();
            console.log(":: Fetch Dietician Record ", response);
            if (response != null) {
              localStorage.setItem("dietitianId", response["_id"]);
              localStorage.setItem("dietitianName", response["name"]);
              localStorage.setItem("companyId", response["companyId"]);
              localStorage.setItem("companyKey", response["companyKey"]);
              localStorage.setItem("urlClientId", response["urlClientId"]);
              localStorage.setItem(
                "menu",
                `{
              "title":"Users Summary",
              "description":[{"name":"This section displays a summary of the latest 20 contacts","desc":[]},{"name":"It has actionable buttons for WhatsApp, scheduling calls, or viewing diet plan details.","desc":[]}],
              "menu":"Home",
              "subMenu":"",
              "activeIndex":0   
          }`
              );
              this.router.navigate(["dashboard"]);
            }
          },
          (err) => {
            console.log("Error Response>> ", err);
            this.loaderService.hideLoader();
          }
        );
      }
    });
  }

  submitBtnHandler2() {
    this.loaderService.presentLoader("Saving details...").then((_) => {
      const payload = {
        name: this.username,
        emailId: localStorage.getItem("loginEmail"),
        calendlyId: this.calendlyId,
        whatsappNum: this.whatsapp,
        mobileNumber: this.mobileNumber,
        role: this.rolesSelected,
        speciality: this.speciality,
        linkedinId: this.linkedinId,
        about: this.about,
        whatsappVisible: this.wpVisible,
        calendlyVisible: this.calendlyVisible,
        gender: this.gender,
      };

      payload["URLClientId"] = this.themeSelected
        ? this.themeSelected
        : this.URLClientIdVal;
      payload["theme"] = this.themeSelected;
      localStorage.setItem(
        "searchAllowed",
        this.searchAllowed ? "true" : "false"
      );

      console.log("Payload>> ", payload);

      const formData = new FormData();
      for (const key in payload) {
        if (payload.hasOwnProperty(key)) {
          formData.append(key, payload[key]);
        }
      }

      if (this.selectedFile) {
        formData.append("image", this.selectedFile, this.selectedFile.name);
      }

      this.templateService.createIndividualDietitian(formData).subscribe(
        (response) => {
          this.loaderService.hideLoader();
          console.log(":: Fetch Dietician Record ", response);
          if (response != null) {
            localStorage.setItem("dietitianId", response["_id"]);
            localStorage.setItem("dietitianName", response["name"]);
            localStorage.setItem("companyId", response["companyId"]);
            localStorage.setItem("companyKey", response["companyKey"]);
            localStorage.setItem("urlClientId", response["URLClientId"]);
            // alert(response["URLClientId"]);
            localStorage.setItem(
              "menu",
              `{
              "title":"Users Summary",
              "description":[{"name":"This section displays a summary of the latest 20 contacts","desc":[]},{"name":"It has actionable buttons for WhatsApp, scheduling calls, or viewing diet plan details.","desc":[]}],
              "menu":"Home",
              "subMenu":"",
              "activeIndex":0   
          }`
            );
            this.router.navigate(["dashboard"]);
          }
        },
        (err) => {
          console.log("Error Response>> ", err);
          this.loaderService.hideLoader();
        }
      );
    });
  }

  // Function to handle the file selection
  onFileSelected(event: any) {
    const file: File = event.target.files[0];

    if (file) {
      this.selectedFile = file;

      // Optional: Display the selected image by creating a local URL
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.selectedImage = e.target.result;
      };
      reader.readAsDataURL(file);
    }
  }

  logout() {
    localStorage.clear();
    localStorage.setItem("email", "");
    localStorage.setItem("femail", "No");
    // this.search = "";
    this.router.navigate(["login"]);
    // this.searchData = "";
    this.username = "";
  }
}
