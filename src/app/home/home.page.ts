import { AfterViewInit, Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { StorageService } from "../services/storage/storage.service";
import { AppService } from "../services/app/app.service";
import { AppService as appSer } from "../../app/app.service";
// import { NativeStorage } from '@awesome-cordova-plugins/native-storage/ngx';

@Component({
  selector: "app-home",
  templateUrl: "./home.page.html",
  styleUrls: ["./home.page.scss"],
})
export class HomePage implements OnInit, AfterViewInit {
  constructor(
    private router: Router,
    private storageService: StorageService,
    private appService: AppService,
    private appS: appSer
  ) {}

  ngAfterViewInit(): void {
    this.defaultDetail();
  }
  defaultDetail() {
    this.appService.defaultData().subscribe(
      (res) => {
        console.log("defaultData::", res);
        localStorage.setItem("defaultData", JSON.stringify(res));
      },
      (err) => {
        console.log("eee", err);
      }
    );
  }
  ngOnInit() {
    if (
      localStorage.getItem("isDietitian") &&
      !localStorage.getItem("dietitianName")
    ) {
      localStorage.clear();
      this.router.navigate(["./login"]);
    }
    if (!localStorage.getItem("acess_token")) {
      this.router.navigate(["./login"]);
    } else {
      this.getRefs();
    }
  }
  gotoCustomer() {
    this.router.navigate(["/tabs/admin"]);
  }

  getRefs() {
    this.appService.refs().subscribe(
      (response) => {
        console.log("test-refs", response);
        if (response) {
          this.storageService.set("refs", response);
        }
      },
      (error) => {
        // this.loaderService.hideLoader();
        console.log("Category Error", error);
      }
    );
  }
  toSummaryLeads() {
    this.router.navigate(["/tabs/summary-lead"]);
  }

  toAdmin() {
    this.router.navigate(["/tabs/admin"]);
  }

  toSearch() {
    this.router.navigate(["deitician-search"]);
  }
}
