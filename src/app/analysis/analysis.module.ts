import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AnalysisPageRoutingModule } from './analysis-routing.module';

import { AnalysisPage } from './analysis.page';
import { OptionsComponent } from './componets/options.component';
import { NgCircleProgressModule } from 'ng-circle-progress';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgCircleProgressModule.forRoot({
      animation:false,
      radius:49,
      innerStrokeWidth:6,
      outerStrokeWidth:6,
      space:-6,
      responsive: false,
      showTitle: true,
      titleFontSize:"15",
      subtitleFontSize:"15",
      unitsFontSize:"15",
      renderOnClick:false
    }),
    AnalysisPageRoutingModule
  ],
  declarations: [AnalysisPage,OptionsComponent],
  entryComponents:[OptionsComponent]
})
export class AnalysisPageModule {}
