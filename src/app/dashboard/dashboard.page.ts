import { Component, OnInit } from "@angular/core";
import { LoaderService, TemplateService } from "../services";
import { AppService } from "../app.service";
import { ActivatedRoute, Router } from "@angular/router";
import { AppService as AppServ } from "../services/app/app.service";
import { UTILITIES } from "../core/utility/utilities";
import moment from "moment";
import { DatePipe } from "@angular/common";
import * as XLSX from "xlsx";
@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.page.html",
  styleUrls: ["./dashboard.page.scss"],
})
export class DashboardPage implements OnInit {
  prevActions: any = [];
  openDititianBox: boolean = false;
  openDateBox: boolean = false;
  dietitianOptions: any = [];
  dietitianChoosen: any;
  dateValue = "";
  current_year = moment().subtract(0, "months").format("YYYY-MM-DD");
  dietitianName = localStorage.getItem("dietitianName") || "Not Assigned";
  isEmpty: boolean = false;
  isBeato: boolean = localStorage.getItem("loginEmail")
    ? localStorage.getItem("loginEmail").toLowerCase().includes("beatoapp.com")
    : false;
  isIndividual = localStorage.getItem("isIndividual");
  search: string;
  diseasesOptions = [
    { code: "A", value: "ACIDITY" },
    { code: "AN", value: "ANEMIA" },
    { code: "AI", value: "ANTI-INFLAMMATORY" },
    { code: "B", value: "BLOOD PRESSURE" },
    { code: "C", value: "CHOLESTEROL" },
    { code: "CR", value: "CALCIUM-RICH" },
    { code: "D", value: "DIABETES" },
    { code: "PD", value: "PRE-DIABETES" },
    { code: "GD", value: "GESTATIONAL DIABETES" },
    // { code: "E", value: "EGESTION/ CONSTIPATION" },
    { code: "FL", value: "FATTY LIVER" },
    { code: "HP", value: "HIGH PROTEIN" },
    { code: "IR", value: "IRON-RICH" },
    { code: "LP", value: "LOW PROTEIN" },
    { code: "P", value: "PCOS" },
    { code: "T", value: "HYPOTHYROID" },
    { code: "S", value: "SLEEP DISORDER" },
    { code: "UA", value: "URIC ACID" },
    { code: "VB", value: "VITAMIN B12" },
    { code: "VD", value: "VITAMIN D" },
    { code: "W", value: "WIND/ FLATULENCE/ BLOATING" },
  ];

  constructor(
    private templateService: TemplateService,
    private loaderService: LoaderService,
    private appS: AppService,
    private router: Router,
    private route: ActivatedRoute,
    private appServe: AppServ,
    private utilities: UTILITIES
  ) {}

  ngOnInit() {}

  exportAsExcelFile(json: any[], excelFileName: string): void {
    // Convert JSON to worksheet
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);

    // Create a workbook
    const workbook: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, "Sheet1");

    // Save as Excel file
    XLSX.writeFile(workbook, `${excelFileName}.xlsx`);
  }

  ConvertToCSV(objArray, headerList) {
    let array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;
    let str = "";
    let row = "S.No,";
    for (let index in headerList) {
      row += headerList[index] + ",";
    }
    row = row.slice(0, -1);
    str += row + "\r\n";

    for (let i = 0; i < array.length; i++) {
      let line = i + 1 + "";
      //  for (let index in headerList) {
      //   let head = headerList[index];
      //   console.log("column",array[i][head]);
      if (array[i]["assignedDietitian"]) {
        //  for (const [key, value] of Object.entries(array[i]['assignedDietitian'])) {
        if (array[i]["assignedDietitian"]["dietitianName"]) {
          line += "," + array[i]["assignedDietitian"]["dietitianName"];
        } else {
          line += ",-";
        }
        if (array[i]["assignedDietitian"]["expiryDate"]) {
          line += "," + array[i]["assignedDietitian"]["expiryDate"];
        } else {
          line += ",-";
        }
        if (array[i]["assignedDietitian"]["lastUpdated"]) {
          line += "," + array[i]["assignedDietitian"]["lastUpdated"];
        } else {
          line += ",-";
        }

        //    }
      } else {
        line += ",-,-,-";
      }

      //  if(array[i][head]==='data'){
      line += "," + array[i]["data"]._id;
      if (array[i]["data"]?.profile) {
        //  for (const [key, value] of Object.entries(array[i]['data'].profile)) {
        if (array[i]["data"]["profile"]["email"]) {
          line += "," + array[i]["data"]["profile"]["email"];
        } else {
          line += ",-";
        }
        if (array[i]["data"]["profile"]["name"]) {
          line += "," + array[i]["data"]["profile"]["name"];
        } else {
          line += ",-";
        }
        if (array[i]["data"]["profile"]["createdDate"]) {
          line +=
            "," +
            moment(array[i]["data"]["profile"]["createdDate"]).format(
              "dd-MMM-yyyy"
            );
        } else {
          line += ",-";
        }
        //  }
      } else {
        line += ",-,-,-";
      }
      if (array[i]["data"]?.demographic) {
        //  for (const [key, value] of Object.entries(array[i]['data']?.demographic)) {
        if (array[i]["data"]["demographic"]["age"]) {
          //    for (const [key1, value1] of Object.entries(array[i]['data']['demographic'][key])) {
          if (array[i]["data"]["demographic"]["age"]["avg_age"]) {
            line += "," + array[i]["data"]["demographic"]["age"]["avg_age"];
          } else {
            line += ",-";
          }
        } else {
          line += ",-";
        }
        if (array[i]["data"]["demographic"]["gender"]) {
          if (array[i]["data"]["demographic"]["gender"]["gender"]) {
            line += "," + array[i]["data"]["demographic"]["gender"]["gender"];
          } else {
            line += ",-";
          }
        } else {
          line += ",-";
        }
        if (array[i]["data"]["demographic"]["height"]) {
          //    for (const [key1, value1] of Object.entries(array[i]['data']['demographic'][key])) {
          if (array[i]["data"]["demographic"]["height"]["value"]) {
            line +=
              "," +
              array[i]["data"]["demographic"]["height"]["value"] +
              " " +
              array[i]["data"]["demographic"]["height"]["unit"];
          } else {
            line += ",-";
          }
        } else {
          line += ",-";
        }
        if (array[i]["data"]["demographic"]["weight"]) {
          //    for (const [key1, value1] of Object.entries(array[i]['data']['demographic'][key])) {
          if (array[i]["data"]["demographic"]["weight"]["value"]) {
            line +=
              "," +
              array[i]["data"]["demographic"]["weight"]["value"] +
              " " +
              array[i]["data"]["demographic"]["weight"]["unit"];
          } else {
            line += ",-";
          }
        } else {
          line += ",-";
        }

        if (array[i]["data"]["demographic"]["bmi"]) {
          if (array[i]["data"]["demographic"]["bmi"]) {
            line += "," + array[i]["data"]["demographic"]["bmi"];
          } else {
            line += ",-";
          }
        } else {
          line += ",-";
        }
      } else {
        line += ",-,-,-,-,-";
      }
      if (array[i]["data"]?.lifeStyle) {
        //    for (const [key, value] of Object.entries(array[i]['data']?.lifeStyle)) {
        //      for (const [key1, value1] of Object.entries(array[i]['data']['lifeStyle'][key])) {
        if (array[i]["data"]["lifeStyle"]["activities"]["code"]) {
          line +=
            "," +
            this.bindActivities(
              this.activities,
              array[i]["data"]["lifeStyle"]["activities"]["code"]
            );
        } else {
          line += ",-";
        }
        if (array[i]["data"]["lifeStyle"]["diseases"]) {
          line += "," + array[i]["data"]["lifeStyle"]["diseases"].join("|"); //.replace(/,/g, "|");
        } else {
          line += ",-";
        }
        if (array[i]["data"]["lifeStyle"]["communities"]) {
          line +=
            "," +
            this.bindCommnities(
              this.communities,
              array[i]["data"]["lifeStyle"]["communities"]
            );
        } else {
          line += ",-";
        }
        if (array[i]["data"]["lifeStyle"]["foodType"]) {
          line +=
            "," +
            this.bindFoodType(
              this.foodType,
              array[i]["data"]["lifeStyle"]["foodType"]
            );
        } else {
          line += ",-";
        }
        if (array[i]["data"]["lifeStyle"]["dietPlanName"]) {
          line += "," + array[i]["data"]["lifeStyle"]["dietPlanName"];
        } else {
          line += ",-";
        }
        if (array[i]["data"]["lifeStyle"]["carb"]) {
          line += "," + array[i]["data"]["lifeStyle"]["carb"];
        } else {
          line += ",-";
        }
        if (array[i]["data"]["lifeStyle"]["protien"]) {
          line += "," + array[i]["data"]["lifeStyle"]["protien"];
        } else {
          line += ",-";
        }
        if (array[i]["data"]["lifeStyle"]["fat"]) {
          line += "," + array[i]["data"]["lifeStyle"]["fat"];
        } else {
          line += ",-";
        }
        if (array[i]["data"]["lifeStyle"]["fiber"]) {
          line += "," + array[i]["data"]["lifeStyle"]["fiber"];
        } else {
          line += ",-";
        }
        if (array[i]["data"]["lifeStyle"]["calories"]) {
          line += "," + array[i]["data"]["lifeStyle"]["calories"];
        } else {
          line += ",-";
        }
      } else {
        line += ",-,-,-,-,-,-,-,-,-,-";
      }
      //  }
      str += line + "\r\n";
    }
    return str;
  }
  bindActivities(activities, codes) {
    const tempActivities = [];

    activities.filter((item) => {
      if (item.code === codes) {
        tempActivities.push(item.value);
      }
    });
    return tempActivities.join().replace(/,/g, "-");
  }
  bindCommnities(communities, codes) {
    const tempCommunities = [];
    communities.filter((item) => {
      for (let index = 0; index < codes.length; index++) {
        if (item.code === codes[index]) {
          tempCommunities.push(item.value);
        }
      }
    });
    return tempCommunities.join("|");
  }
  bindFoodType(foodType, codes) {
    const tempFoodType = [];

    foodType.filter((item) => {
      if (item.code === codes) {
        tempFoodType.push(item.value);
      }
    });
    return tempFoodType;
  }
  downloadFile(data, filename = "user_detail") {
    console.log("data", data);
    //  this.exportAsExcelFile(data,'customer_data')
    let csvData = this.ConvertToCSV(data, [
      "dietitianName",
      "expiryDate",
      "lastUpdated",
      "_id",
      "profile_id",
      "name",
      "createdDate",
      "age",
      "gender",
      "height",
      "weight",
      "bmi",
      "activities",
      "deseases",
      "communities",
      "foodType",
      "dietPlanName",
      "carb",
      "protien",
      "fat",
      "fiber",
      "calories",
    ]);
    console.log(csvData);
    let blob = new Blob(["\ufeff" + csvData], {
      type: "text/csv;charset=utf-8;",
    });
    let dwldLink = document.createElement("a");
    let url = URL.createObjectURL(blob);
    let isSafariBrowser =
      navigator.userAgent.indexOf("Safari") != -1 &&
      navigator.userAgent.indexOf("Chrome") == -1;
    if (isSafariBrowser) {
      //if Safari open in new window to save file with random filename.
      dwldLink.setAttribute("target", "_blank");
    }
    dwldLink.setAttribute("href", url);
    dwldLink.setAttribute("download", filename + ".csv");
    dwldLink.style.visibility = "hidden";
    document.body.appendChild(dwldLink);
    dwldLink.click();
    document.body.removeChild(dwldLink);
  }
  formatDate(value: string) {
    if (!value) return null;
    return moment(value).format("DD-MMM-YYYY");
  }

  activities = [];
  foodType = [];
  communities = [];
  ionViewWillEnter() {
    this.route.queryParamMap.subscribe((params) => {
      this.search = params.get("id");

      // alert(this.search);

      if (this.search) {
        // alert(this.id);
        // alert("Exists")
        // this.prevActions = [];
        // this.loaderService.presentLoader("Fetching details..." + this.search).then((_) => {
        //         this.loaderService.hideLoader();
        //         this.prevActions = [];
        //         this.isEmpty = true;

        //         // this.dietRecords = response;
        //         // console.log(":: Get Diet Plan Record ", data);
        // });

        let query = `userId=${this.search}&testVersion=${true}`;

        if (this.isIndividual === "true") {
          const dietitianId = localStorage.getItem("dietitianId");
          query += `&dietitianId=${dietitianId}`;
        } else {
          const companyId = localStorage.getItem("companyId");
          if (companyId === "smartdietplanner") {
            query += `&companyId=*`;
          } else {
            query += `&companyId=${companyId}`;
          }
        }

        this.templateService.searchUser(query).subscribe(
          (profileData: any) => {
            console.log("Data::>> ", profileData);
            // this.appS.searchProfile(this.search).then((profileData: any) => {
            // alert(profileData.length);

            // if (!profileData || profileData.length===0 ) {
            //   this.utilities.presentAlert(
            //     "User does not exist. Please try again with correct Id."
            //   );
            //   return;
            // }  else if (profileData.length>1 ) {
            // this.isSearch = true;
            // CONSTANTS.email = this.search;
            // const id = this.search;
            // localStorage.setItem("email", this.search);
            // this.router.navigate(["dashboard"], {
            //   queryParams: { id },
            // });

            //   return;
            // } else {
            //   this.isSearch = true;
            //   CONSTANTS.email = this.search;
            //   localStorage.setItem("email", this.search);
            // }
            // this.searchData = profileData["_id"];
            console.log("profileData:-", profileData);
            // this.lifeStyle = profileData?.lifeStyle;
            // this.additionalPref(profileData["additionalPref"]);
            // const data = ;
            const response = profileData;
            // if (data) {
            if (response != null) {
              // this.searchDetails = JSON.parse(JSON.stringify(profileData));
              localStorage.setItem("femail", "Yes");
              console.log("this.searchDetails", profileData);

              this.prevActions = [];
              Object.keys(response).forEach((key) => {
                const data = response[key];

                // this.appS.searchProfile(item.userId)
                //   .then(async (data: any) => {
                if (!Array.isArray(this.prevActions)) {
                  this.prevActions = [];
                }

                const Diseases = data?.data?.lifeStyle?.diseases || [];
                // console.log("Disesase Array OOO>: ", Diseases);

                if (Diseases.length !== 0) {
                  // alert("Empty")

                  // console.log(
                  //   "Default Dieseases --- ? ",
                  //   res["otherMaster"]?.diseases
                  // );
                  // }
                  const diseases: string[] = [];
                  // const defaultData = res;
                  const referenceArray = this.diseasesOptions;
                  const resultArray = Diseases.map((code) => {
                    const match = referenceArray.find(
                      (item) => item.code === code
                    );
                    return match ? match.value : null;
                  }).filter((value) => value !== null);

                  console.log("<><><> Result Array <><>: ", resultArray);

                  console.log("Diseases >>> ", diseases);
                  // return resultArray;

                  data.data.lifeStyle.diseases = resultArray;
                }
                // this.prevActions.push({ ...item, user: data });
                this.prevActions.push(data);
                // console.log(">> ", item);
                console.log("181 ::: Data Got>> ", data);
                // });
              });
              // }
              console.log(
                "|| Finala 185 ---> PrevActionss >>> ",
                this.prevActions
              );

              // const planC = this.dietPlan.filter((item) => {
              //   return item.id === this.searchDetails?.additionalPref?.planChosen;
              // });
              // this.planChoosen = planC[0]?.name;
              // this.updateTargetCal = this.searchDetails?.lifeStyle?.calories;
              // this.recommendedTargetCal = this.searchDetails?.lifeStyle?.calories;
              const dd = JSON.parse(JSON.stringify(profileData));
              // this.profileInititals = dd?.profile.name;
              // dd?.profile.name?.replace("  "," ")?.split(' ')[0][0].toUpperCase() +
              // (dd?.profile?.name?.replace("  "," ")?.split(' ')?.length>=2?dd?.profile?.name?.replace("  "," ")?.split(' ')[1][0].toUpperCase():dd?.profile?.name?.replace("  "," ")?.split(' ')[0][1].toUpperCase());

              // this.isHindi =  profileData["profile"]?.languagePref == undefined ? false : true;
              // this.search ='';
              // this.appServe.getUserToken(this.search).subscribe(
              //   (response) => {
              //     console.log(response);
              //   },
              //   (error) => {
              //     console.log("User token ", error.error.text);
              //     localStorage.setItem("personal_token", error.error.text);
              //     console.log("Category Error", error);
              //   }
              // );
              // this.getProfile();
              // this.getToken1();
            } else {
            }
            //   localStorage.setItem("email", this.search);
            //   localStorage.setItem("femail", "Yes");
            //   localStorage.setItem("activeNum", "1");
            //   localStorage.setItem(
            //     "menu",
            //     `{
            //     "title":"View/Edit Profile",
            //     "description":[{"name":"This page allows you to view and update user information such as:",
            //     "desc":["Name",
            //     "Age",
            //     "Weight",
            //     "Height",
            //     "Activity level",
            //     "Food choices",
            //     "Disorders",
            //     "Allergies",
            //     "Diet Plan"]}],
            //     "menu":"Profile",
            //     "subMenu":"",
            //     "activeIndex":1
            // }`
            //   );
            // this.router.navigate(["/profile-detail"]).then(() => {
            //   window.location.reload();
            // });

            // this.appS.getBeatoData(this.search).then(
            //   (res) => {
            //     console.log("response", res);
            //   },
            //   (err) => {
            //     console.log("error", err);
            //   }
            // );
          },
          (err) => {
            console.log("Error Response>> ", err);
            // if (err.error.error === "No actions found for this dietitian") {
            this.isEmpty = true;
            // }
          }
        );
      } else {
        this.search = null;
        // ---------------------
        this.appServe.getUserToken(localStorage.getItem("email")).subscribe(
          (response) => {
            console.log(response);
          },
          (error) => {
            console.log("User token ", error.error.text);
            this.appS.getDefaultData(error.error.text).then((res) => {
              this.activities = res["otherMaster"]["activities"];
              this.foodType = res["otherMaster"]["foodPref"];
              this.communities = res["otherMaster"]["community"];
              this.templateService
                .getAllDietitianActionSummary(
                  localStorage.getItem("dietitianId")
                )
                .subscribe(
                  (response) => {
                    this.loaderService.hideLoader();
                    console.log(":: Fetch Dietician Actions ", response);
                    if (response != null) {
                      this.prevActions = [];
                      Object.keys(response).forEach((key) => {
                        const data = response[key];

                        // this.appS.searchProfile(item.userId)
                        //   .then(async (data: any) => {
                        if (!Array.isArray(this.prevActions)) {
                          this.prevActions = [];
                        }

                        const Diseases = data?.data?.lifeStyle?.diseases || [];
                        // console.log("Disesase Array OOO>: ", Diseases);

                        if (Diseases.length !== 0) {
                          // alert("Empty")

                          console.log(
                            "Default Dieseases --- ? ",
                            res["otherMaster"]?.diseases
                          );
                          // }
                          const diseases: string[] = [];
                          // const defaultData = res;
                          const referenceArray = res["otherMaster"]?.diseases;

                          const resultArray = Diseases.map((code) => {
                            const match = referenceArray.find(
                              (item) => item.code === code
                            );
                            return match ? match.value : null;
                          }).filter((value) => value !== null);

                          console.log(
                            "<><><> Result Array <><>: ",
                            resultArray
                          );

                          console.log("Diseases >>> ", diseases);
                          // return resultArray;

                          data.data.lifeStyle.diseases = resultArray;
                        }
                        // this.prevActions.push({ ...item, user: data });
                        this.prevActions.push(data);
                        // console.log(">> ", item);
                        console.log("Data Got>> ", data);
                        // });
                      });
                    }
                    console.log("PrevActionss >>> ", this.prevActions);
                  },
                  (err) => {
                    console.log("Error Response>> ", err);
                    this.loaderService.hideLoader();
                    // alert(JSON.stringify(err.error));
                    if (
                      err.error.error === "No actions found for this dietitian"
                    ) {
                      this.isEmpty = true;
                    }
                  }
                );
            });
          }
        );
      }
    });
  }

  handleClick(id: string) {
    // alert(id);
    localStorage.setItem("email", id);
    localStorage.setItem("femail", "Yes");
    localStorage.setItem("activeNum", "1");
    localStorage.setItem(
      "menu",
      `{
        "title":"View/Edit Profile",
        "description":[{"name":"This page allows you to view and update user information such as:",
        "desc":["Name",
        "Age",
        "Weight",
        "Height",
        "Activity level",
        "Food choices",
        "Disorders",
        "Allergies",
        "Diet Plan"]}],
        "menu":"Profile",
        "subMenu":"",
        "activeIndex":1          
    }`
    );
    this.router
      .navigate(["profile-detail"], {
        queryParams: { param: "loged in" },
      })
      .then(() => {
        window.location.reload();
      });
  }

  handleDietPlan(id: string) {
    // alert(id);
    localStorage.setItem("email", id);
    localStorage.setItem("femail", "Yes");
    localStorage.setItem("activeNum", "2");
    localStorage.setItem(
      "menu",
      `{
        "title":"7 Days Plan",
        "description":[{"name":"Create a 7-day diet plan summary based on the chosen diet plan, food choices, and user preferences.","desc":[]},
        {"name":"The dietitian can update and save it as a template or for the user.","desc":[]}
    ],
        "menu":"Diet Plan",
        "subMenu":"",
        "activeIndex":2      
    }`
    );
    this.router.navigate(["manage-diet"]).then(() => {
      window.location.reload();
    });
  }

  copyProfileLink(search: string) {
    // alert(search);
    let body: object;
    if (this.isIndividual === "true") {
      body = {
        URLClientID: localStorage.getItem("urlClientId"),
        companyKey: localStorage.getItem("companyKey"),
        userId: search,
      };
    } else {
      body = {
        companyKey: localStorage.getItem("companyKey"),
        userId: search,
      };
    }
    this.templateService.getProfileLink(body).subscribe(
      async (response) => {
        await navigator.clipboard.writeText(response["url"]);
        this.utilities.presentAlert("Copied Link");
        console.log("Response>> ", response);
      },
      (error) => {
        console.log("Generate Profile Link", error.error.text);
      }
    );
  }

  editProfile(id: string) {
    localStorage.setItem("email", id);
    localStorage.setItem("femail", "Yes");
    localStorage.setItem("activeNum", "1");
    localStorage.setItem(
      "menu",
      `{
        "title":"View/Edit Profile",
        "description":[{"name":"This page allows you to view and update user details.","desc":[]}],
        "menu":"Profile",
        "subMenu":"",
        "activeIndex":1          
    }`
    );
    this.router.navigate(["edit-profile"]).then(() => {
      window.location.reload();
    });
  }

  formatDateString(dateString: string): string {
    const datePipe = new DatePipe("en-US"); // Specify locale, e.g., 'en-US'
    const parsedDate = new Date(dateString); // Parse the string to a Date object

    // Check if the parsed date is valid
    if (isNaN(parsedDate.getTime())) {
      console.error("Invalid date format");
      return "";
    }

    return datePipe.transform(parsedDate, "d/MM/y") || ""; // Format date to 'd/MM/y'
  }

  getResponseStr(obj: Object) {
    /* {
    "_id": "7726922405",
    "profile": {
        "email": "7726922405",
        "family_name": null,
        "given_name": null,
        "country": null,
        "name": null,
        "login_type": null,
        "region": null,
        "source": null,
        "device": null,
        "os_version": null,
        "createdDate": "08-Jun-2024",
        "isReferral": false,
        "referralCode": "b2w0g",
        "createdDateTime": "08-Jun-2024 14:38:23",
        "updatedDateTime": "08-Jun-2024 14:45:05",
        "dietPlanType": "weightLoss",
        "companyId": "bluebein"
    },
    "lifeStyle": {
        "dietPlanName": "weightLoss",
        "country": "IND",
        "carb": null,
        "protien": null,
        "fat": null,
        "fiber": null,
        "calories": null,
        "calculatedCalories": null,
        "bmr": null,
        "activityCalories": null,
        "updatedDate": "08-Jun-2024",
        "updatedDateTime": "08-Jun-2024 14:45:05",
        "communities": null,
        "diseases": []
    }
}
*/
    return JSON.stringify(obj);
  }

  userId: string;
  assignedDietitian;
  editDititician(id: string, assignedDietitianObj: any) {
    console.log("EditDietitiacian Clicked!!! ", this.isIndividual);
    if (this.isIndividual === "true") {
      return;
    }
    this.userId = id;
    if (assignedDietitianObj !== null) {
      this.assignedDietitian = assignedDietitianObj;
    } else {
      this.assignedDietitian = {
        dietitianName: localStorage.getItem("dietitianName"),
        dietitianId: localStorage.getItem("dietitianId"),
        userId: this.userId,
        updatedBy: localStorage.getItem("dietitianId"),
        expiryDate: null,
        aiBasedPlanContinues: true,
        editPermitted: true,
        whatsappContinues: true,
        callSchedulingContinues: true,
      };
      console.log("Alert");
    }

    // this.loaderService.presentLoader("Loading Dietitians Record...").then((_) => {
    this.templateService
      .getAllDietitians(localStorage.getItem("companyId"))
      .subscribe(
        (res) => {
          console.log("All Dietitans Records Result >> ", res);
          this.dietitianOptions = res;
          this.openDititianBox = true;
          // this.loaderService.hideLoader();
        },
        (error) => {
          // this.loaderService.hideLoader();
          this.utilities.presentAlert(
            "Something went wrong in getting AllDietitians Record."
          );
          console.log("Get all Dieitians Record", error.error.text);
        }
      );
    // });
  }

  updateDietitian() {
    // alert(JSON.stringify(this.dietitianChoosen));
    console.log("Assigned Obj ::> >", this.assignedDietitian);
    const payload = {
      dietitianName: this.dietitianChoosen?.name || this.assignedDietitian.name,
      dietitianId:
        this.dietitianChoosen?._id || this.assignedDietitian?.dietitianId,
      userId: this.userId,
      updatedBy: localStorage.getItem("dietitianId"),
      expiryDate: this.assignedDietitian.expiryDate,
      aiBasedPlanContinues: this.assignedDietitian.aiBasedPlanContinues,
      editPermitted: this.assignedDietitian.editPermitted,
      whatsappContinues: this.assignedDietitian.whatsappContinues,
      callSchedulingContinues: this.assignedDietitian.callSchedulingContinues,
    };

    console.log("Payload ::>> ", payload);
    if (this.assignedDietitian._id === undefined) {
      this.templateService.createAssignedDietitian(payload).subscribe(
        (res) => {
          console.log("Create Dietitian Records Result >> ", res);
          this.openDititianBox = false;
          this.openDateBox = false;
          // this.dietitianOptions = res;
          window.location.reload();
          // this.loaderService.hideLoader();
        },
        (error) => {
          // this.loaderService.hideLoader();
          this.utilities.presentAlert(
            "Something went wrong in creating Dietitian Record."
          );
          console.log("Create Assigned Dieitians Record", error.error.text);
        }
      );
    } else {
      this.templateService
        .updateAssignedDietitian(this.assignedDietitian._id, payload)
        .subscribe(
          (res) => {
            console.log("Update Dietitian Records Result >> ", res);
            this.openDititianBox = false;
            this.openDateBox = false;
            // this.dietitianOptions = res;
            window.location.reload();
            // this.loaderService.hideLoader();
          },
          (error) => {
            // this.loaderService.hideLoader();
            this.utilities.presentAlert(
              "Something went wrong in Updating Assigned Dietitian Record."
            );
            console.log("Get all Dieitians Record", error.error.text);
          }
        );
    }
  }

  editDate(id: string, assignedDietitianObj: any) {
    this.openDateBox = true;
    // alert(id);
    this.userId = id;
    if (assignedDietitianObj !== null) {
      this.assignedDietitian = assignedDietitianObj;
      if (assignedDietitianObj.expiryDate !== null) {
        this.assignedDietitian.expiryDate = this.formatDate(
          assignedDietitianObj.expiryDate
        );
      }
    } else {
      this.assignedDietitian = {
        dietitianName: null,
        dietitianId: null,
        userId: this.userId,
        updatedBy: localStorage.getItem("dietitianId"),
        expiryDate: null,
        aiBasedPlanContinues: true,
        editPermitted: true,
        whatsappContinues: true,
        callSchedulingContinues: true,
      };
      console.log("Alert");
    }
    if (!this.assignedDietitian.expiryDate) {
      this.assignedDietitian.expiryDate = "";
    }
  }

  closePopup() {
    this.openDititianBox = false;
    this.openDateBox = false;
  }

  hoveredStatusId: string | null = null;

  onHover(statusId: string | null) {
    this.hoveredStatusId = statusId;
  }
}
