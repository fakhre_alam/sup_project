import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TemplatePopupPage } from './template-popup.page';

const routes: Routes = [
  {
    path: '',
    component: TemplatePopupPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TemplatePopupPageRoutingModule {}

