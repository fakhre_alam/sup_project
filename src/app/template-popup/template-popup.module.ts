import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TemplatePopupPageRoutingModule } from './template-popup-routing.module';

import { TemplatePopupPage } from './template-popup.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TemplatePopupPageRoutingModule
  ],
  declarations: [TemplatePopupPage]
})
export class TemplatePopupPageModule {}
