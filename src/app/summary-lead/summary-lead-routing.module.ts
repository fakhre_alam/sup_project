import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SummaryLeadPage } from './summary-lead.page';

const routes: Routes = [
  {
    path: '',
    component: SummaryLeadPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SummaryLeadPageRoutingModule {}
