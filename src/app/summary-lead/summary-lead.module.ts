import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SummaryLeadPageRoutingModule } from './summary-lead-routing.module';

import { SummaryLeadPage } from './summary-lead.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SummaryLeadPageRoutingModule
  ],
  declarations: [SummaryLeadPage]
})
export class SummaryLeadPageModule {}
