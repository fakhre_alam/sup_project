import { Component, OnInit, ViewChild } from "@angular/core";
import { StorageService, AppService } from "../services";
import { Router } from "@angular/router";
import { IonSlides } from "@ionic/angular";
@Component({
  selector: "app-summary-lead",
  templateUrl: "./summary-lead.page.html",
  styleUrls: ["./summary-lead.page.scss"],
})
export class SummaryLeadPage implements OnInit {
  refsData;
  segment = '0';
  // summaryData = {
  //   subOptionTypes:[],
  //   statusTypes:[],
  //   optionTypes:[]
  // }
  optionTypes: Object[] = [];
  totalOptions = 0;

  subOptionTypes: Object[] = [];
  totalSubOptions = 0;

  statusTypes: Object[] = [];
  totalStatus = 0;

  leadTypes: Object[] = [];
  totalLead = 0;

  totalMainOptions = [];

  myLeads;
  hotLeads;
  constructor(
    private router: Router,
    private storageService: StorageService,
    private appService: AppService
  ) {}

  ngOnInit() {}
 async ionViewWillEnter() {
    console.log("Summary page");
  //  this.storageService.get("refs").then((val) => {
    await this.appService.refs().subscribe(
      (response) => {
        console.log("test-refs",response);
        if (response) {
          this.refsData = response;
        }
      },
      (error) => {
        // this.loaderService.hideLoader();
        console.log("Category Error", error);
      }
    );

    await this.getSummaryLeads();
     
  //  });
  }
  segmentChanged() {
    // this.slider.slideTo(this.segment);
  }



  rersetValues() {
    this.optionTypes = [];
    this.totalOptions = 0;

    this.subOptionTypes = [];
    this.totalSubOptions = 0;

    this.statusTypes = [];
    this.totalStatus = 0;

    this.leadTypes = [];
    this.totalLead = 0;

    this.totalMainOptions = [];
  }

  getSummaryLeads() {
    this.appService.leadSummary().subscribe(
      (response) => {
        if (response) {
          console.log("ref ", this.refsData);
          console.log("leadSummary ", response);
          this.rersetValues();
          this.myLeads =
            response &&
            response.myLeads &&
            response.myLeads[0] &&
            response.myLeads[0].myLeadsCount
              ? response.myLeads[0].myLeadsCount
              : 0;
          this.hotLeads =
            response &&
            response.paidConversions &&
            response.paidConversions[0] &&
            response.paidConversions[0].paidConversionCount
              ? response.paidConversions[0].paidConversionCount
              : 0;

          // this.totalOptions = 0;
          // this.totalSubOptions = 0;
          // this.totalStatus = 0;
          // this.totalLead = 0;
          response.option.forEach((ele) => {
            this.refsData.optionTypes.forEach((ele1) => {
              if (ele1.code == ele._id) {
                let obj = {};
                obj["name"] = ele1["name"];
                obj["code"] = ele1["code"];
                obj["count"] = ele["optionCount"];
                this.totalOptions = this.totalOptions + ele["optionCount"];
                this.optionTypes.push(obj);
              }
            });
          });

          response.subOption.forEach((ele) => {
            this.refsData.subOptionTypes.forEach((ele1) => {
              if (ele1.code == ele._id) {
                let obj = {};
                obj["name"] = ele1["name"];
                obj["code"] = ele1["code"];
                obj["count"] = ele["subOptionCount"];
                this.totalSubOptions =
                  this.totalSubOptions + ele["subOptionCount"];
                this.subOptionTypes.push(obj);
              }
            });
          });
          this.optionTypes.forEach((ele, index) => {
            let obj = {
              title: ele["name"],
              count: ele["count"],
              subOptions: [],
            };
            this.subOptionTypes.forEach((ele1, index1) => {
              let subCode = ele1["code"].split(".")[0];
              if (subCode == ele["code"]) {
                obj["subOptions"].push(ele1);
              }
            });
            this.totalMainOptions.push(obj);
          });

          response.status.forEach((ele) => {
            this.refsData.statusTypes.forEach((ele1) => {
              if (ele1.code == ele._id) {
                let obj = {};
                obj["name"] = ele1["name"];
                obj["code"] = ele1["code"];
                obj["count"] = ele["statusCount"];
                this.totalStatus = this.totalStatus + ele["statusCount"];
                this.statusTypes.push(obj);
              }
            });
          });

          response.leadType.forEach((ele) => {
            this.refsData.leadTypes.forEach((ele1) => {
              if (ele1.code == ele._id) {
                let obj = {};
                obj["name"] = ele1["name"];
                obj["code"] = ele1["code"];
                obj["count"] = ele["leadTypeCount"];
                this.totalLead = this.totalLead + ele["leadTypeCount"];
                this.leadTypes.push(obj);
              }
            });
          });
        }
      },
      (error) => {
        // this.loaderService.hideLoader();
        console.log("Category Error", error);
      }
    );
  }

  goToSearchPage(type, value, code, subOption?) {
    let obj = {
      searchQueryType: type,
      searchQueryName: value,
      searchQueryCode: code,
    };
    this.router.navigate(["/search-lead"], {
      queryParams: obj,
    });
  }
}
