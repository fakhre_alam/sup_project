import { Component, OnInit } from "@angular/core";
import { LoaderService, TemplateService } from "../services";
import { Router } from "@angular/router";
import { UTILITIES } from "../core/utility/utilities";
import { AppService } from "../app.service";

@Component({
  selector: "app-template",
  templateUrl: "./template.page.html",
  styleUrls: ["./template.page.scss"],
})
export class TemplatePage implements OnInit {
  data: any;
  loading = true;
  error: string | null = null;
  filterActive: "all" | "true" | "false" = "true";
  filteredData: any = [];
  sortByName = "false";
  isPersonal = "false";
  userSearched = "";
  isActive = false;
  isIndividual = localStorage.getItem("isIndividual");
  constructor(
    private templateService: TemplateService,
    private router: Router,
    private loaderService: LoaderService,
    private utilities: UTILITIES,
    private appService: AppService
  ) {
    this.userSearched = localStorage.getItem("email");
  }

  ngOnInit() {
    var companyId = localStorage.getItem("companyId") || "smartdietplanner";
    setTimeout(() => {
      this.getTemplate(companyId);
    }, 2000);

    // this.filterActiveFxn();
  }
getTemplate(companyId){
  this.templateService
  .getTemplate(localStorage.getItem("loginEmail"), companyId)
  .subscribe(
    (response) => {
      this.isActive=true;
      this.data = response;
      this.filteredData = this.data.filter((item) => item.isActive);
      console.log("Data Recieved::>> ", this.data);
      this.loading = false;
    },
    (error) => {
      this.isActive=true;
      this.error = "Error fetching data";
      console.error("Error fetching data", error);
      this.loading = false;
    }
  );
}

createTemplate(){
  this.router.navigate(['manage-diet'],{queryParams:{newTemplate:'true'}});
}

  toggleUpdateTemplate(templateId: string, createdBy: string) {
    if (createdBy !== localStorage.getItem("loginEmail")) {
      this.utilities.presentAlert(
        "You cannot change Template created by " + createdBy
      );
      return;
    }
    console.log("Id is :: ", templateId);
    this.templateService
      .toggleTemplate(templateId, localStorage.getItem("loginEmail"))
      .subscribe(
        (response) => {
          console.log("Response After Update:: ", response);
          this.replaceJsonItem("_id", templateId, response);
          this.filterActiveFxn();
        },
        (error) => {
          this.error = "Error Disabling Template";
          console.error("Error Disabling Template", error);
          this.loading = false;
        }
      );
  }

  replaceJsonItem(field: string, fieldValue: string, newJson: any): void {
    const index = this.data.findIndex((item: any) => item._id === fieldValue);
    if (index !== -1) {
      this.data[index] = newJson;
    } else {
      console.error("Item not found");
    }
  }

  // Function to remove item with id
  removeItemById(array: any[], id: string) {
    console.log("Array >>> ", array);
    return array.filter((item) => item._id !== id);
  }

  filterActiveFxn(): void {
    debugger;
    let tempData = [...this.data];

    if (this.filterActive === "true") {
      tempData = tempData.filter((item) => item.isActive);
    } else if (this.filterActive === "false") {
      tempData = tempData.filter((item) => !item.isActive);
    }

    if (this.sortByName==="true") {
      tempData = tempData.sort((a, b) =>
        a.templateName.localeCompare(b.templateName)
      );
    }

    this.filteredData = tempData;
  }

  gotoCopy(id: any) {
    this.router
      .navigate(["manage-diet"], { queryParams: { params: id, edit: false } })
      .then(() => {
        window.location.reload();
      });
  }
  gotoUpdate(id: any) {
    this.router
      .navigate(["manage-diet"], { queryParams: { params: id, edit: true } })
      .then(() => {
        window.location.reload();
      });
  }

  handleFilterChange() {
    if (this.isPersonal==="true") {
      this.filteredData = this.data.filter(
        (item) => item.createdby === localStorage.getItem("loginEmail")
      );
      console.log(
        this.isPersonal,
        " || this.filteredData >> ",
        this.filteredData
      );
    } else {
      this.filteredData = this.data;
      console.log(
        this.isPersonal,
        " || this.filteredData >> ",
        this.filteredData
      );
    }
    this.filterActiveFxn();
  }

  editFxn(id: any) {
    console.log("Edit >>", id);
    this.router
      .navigate(["manage-diet"], { queryParams: { params: id, edit: true } })
      .then(() => {
        window.location.reload();
      });
  }

  deleteFxn(id: any) {
    console.log("Delete >>", id);
    this.loaderService.presentLoader("Deleting...").then((_) => {
      this.templateService
        .deleteTemplate(id, localStorage.getItem("loginEmail"))
        .subscribe(
          (response) => {
            console.log("Response After Update:: ", response);
            this.loaderService.hideLoader();
            this.filteredData = this.removeItemById(this.filteredData, id);
            this.data = this.removeItemById(this.data, id);
            this.utilities.presentAlert("Template Deleted successfully!");
          },
          (error) => {
            this.error = "Error Deleting Template";
            console.error("Error Deleting Template", error);
            this.loaderService.hideLoader();
          }
        );
    });
  }

 

  onToggleChange(event: any, item: any) {
    if (item.createdby !== localStorage.getItem("loginEmail")) {
      this.utilities.presentAlert(
        "You cannot change Template created by " + item["createdby"]
      );
      // Reset the toggle back to its original state
      setTimeout(() => {
        item.isGlobal = !item.isGlobal; // Revert the change
      }, 0);
      return;
    }

    this.appService.updateTemplate(item, item["_id"]).then(
      (res) => {
        console.log("Updated Templates >> ", res);
        if (localStorage.getItem("dietitianId") != null) {
          this.templateService
            .handleDietitianAction(
              localStorage.getItem("dietitianId"),
              "668d74101073b512dd2b978c",
              localStorage.getItem("email"),
              "Edit Template",
              localStorage.getItem("companyId")
            )
            .subscribe((dRespondata) => {
              console.log("success", res);
            });
        }
      },
      (err) => {
        console.log("Error", err);
        //    this.utilities.presentAlert("Error: " + err);
      }
    );
  }
}
