import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FoodItemChoicePage } from './food-item-choice.page';

const routes: Routes = [
  {
    path: '',
    component: FoodItemChoicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FoodItemChoicePageRoutingModule {}
