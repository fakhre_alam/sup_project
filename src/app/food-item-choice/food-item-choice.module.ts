import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { FoodItemChoicePageRoutingModule } from "./food-item-choice-routing.module";
import { NgCircleProgressModule } from "ng-circle-progress";
import { FoodItemChoicePage } from "./food-item-choice.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgCircleProgressModule.forRoot({
      animation: false,
      radius: 49,
      innerStrokeWidth: 6,
      outerStrokeWidth: 6,
      space: -6,
      responsive: false,
      showTitle: true,
      titleFontSize: "15",
      subtitleFontSize: "15",
      unitsFontSize: "15",
      renderOnClick: false,
    }),
    FoodItemChoicePageRoutingModule,
  ],
  declarations: [
    FoodItemChoicePage,
   
  ],
})
export class FoodItemChoicePageModule {}
