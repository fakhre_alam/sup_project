import { ChangeDetectorRef, Component, OnChanges, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { AppService as apS, LoaderService } from "../services";
import { UTILITIES } from '../core/utility/utilities';
import { Router } from '@angular/router';

@Component({
  selector: 'app-food-item-choice',
  templateUrl: './food-item-choice.page.html',
  styleUrls: ['./food-item-choice.page.scss'],
})
export class FoodItemChoicePage implements OnInit,OnChanges {
  itemOp:any=[];
  itemOptions:any= [];
  totalLength=new Array(9).fill(0);
  avgSlotTotalCal = new Array(9).fill(0);
  protien = new Array(9).fill(0);
  sumProtien=0;
  fat = new Array(9).fill(0);
  sumFat=0;
  carbs = new Array(9).fill(0);
  sumCarbs=0;
  calTotal = 0;
  foodItemsArray:any={
    slots:[],
  "total_macros": {
    "calories": 0, // Sum of Breakfast (1350) and Lunch (400)
    "fat": 0,         // Sum of Breakfast (50) and Lunch (14)
    "protein": 0,     // Sum of Breakfast (60) and Lunch (11)
    "carbs": 0,      // Sum of Breakfast (180) and Lunch (58)
    "fiber": 0        // Sum of Breakfast (25) and Lunch (8)
  }
};
slotNames =['When You Wake up','Before Breakfast','Breakfast','Mid Day Meal','Lunch','Post Lunch','Evening Snack','Dinner','Before Sleep'];
  foodItemSlots=[];
  searchItems:any;
  constructor(private router:Router ,private utilities:UTILITIES,private cdr:ChangeDetectorRef,private aps:apS,private appService:AppService) { 
  }

  defaultStructureOfJson(){
    for (let index = 0; index < 9; index++) {

      this.foodItemsArray.slots.push(
        {
          slotIndex: index,
          name:this.slotNames[index],
          items:[],
          avg_macros:{
            calories:0,
            protein:0,
            fat:0,
            carbs:0
          }
        }
      );
    }
    this.foodItemSlots = this.foodItemsArray.slots;
    
  }
  averageSlotTotalCal(){
    let tempAvgCal=0;
    let protien=0;
    let fat=0;
    let carbs=0;
    let diviser=0;
    for (let index = 0; index < this.foodItemSlots.length; index++) {
       tempAvgCal=0;
       protien=0;
       fat=0;
       carbs=0;
       diviser=0;
      for (let j = 0; j < this.foodItemSlots[index].items.length; j++) {
        diviser = this.foodItemSlots[index].items[j].options.length>0?this.foodItemSlots[index].items[j].options.length:1;
         tempAvgCal += (this.foodItemSlots[index].items[j].itemsCalories/diviser);
         protien += (this.foodItemSlots[index].items[j].itemsProtien/diviser);
         fat += (this.foodItemSlots[index].items[j].itemsFat/diviser);
         carbs += (this.foodItemSlots[index].items[j].itemsCarbs/diviser);
      }
      this.avgSlotTotalCal[index]=tempAvgCal;
      this.protien[index] = protien;
      this.fat[index] = fat;
      this.carbs[index] = carbs;

    }
    this.calTotal = this.avgSlotTotalCal.reduce((cummulat,a)=>cummulat+a,0);  
console.log(this.protien,this.fat,this.carbs);

    
  }
  sumTotalCalprotienfatcarbs(item){
    return item.reduce((partialSum, a) => partialSum + a, 0);
  }
  ngOnInit() {
      const userId = localStorage.getItem('email');
    this.appService.getDietRecall(userId).then(res=>{
      console.log("res",res);      
      if(res==null){
        this.defaultStructureOfJson();
        
        console.log("this.foodItemSlots", this.foodItemsArray ,this.foodItemSlots);
      }
      else{
    this.foodItemsArray = res;
    this.foodItemSlots = this.foodItemsArray.slots;
    this.bindData(); // we will use this method when API will return response
    this.averageSlotTotalCal();
      }
    },err=>{
      console.log("err",err);
      
    });
    
    
  }
  bindData(){
    let tcount=0;
    for (let index = 0; index < this.foodItemSlots.length; index++) {
      tcount=0;
      for (let j = 0; j < this.foodItemSlots[index].items.length; j++) {
        tcount = tcount+ this.foodItemSlots[index].items[j].options.length;
      
      }
      this.totalLength[index]= tcount;
    }
   console.log("totalLength", this.totalLength);
  }
  isOpen=false;
  isOpenSlotIndex=-1;
  isOpenItemIndex=-1;
  openPopup(slotIndex,itemIndex){
  this.isOpen=true;
  this.isOpenSlotIndex=slotIndex;
  this.isOpenItemIndex = itemIndex;
  }
  closePopup(){
    this.isOpen=false;
    this.isOpenSlotIndex=-1;
    this.isOpenItemIndex = -1;
    
  }

  ngOnChanges(){
    this.cdr.detectChanges();
  }

  removeItems(index,itemIndex,name){
   // debugger;
   const item = this.foodItemSlots[index].items.filter(item=>{
      return item.name=== name;
    });
    this.foodItemSlots[index].items = this.foodItemSlots[index].items.filter(item=>{
      return item.name!== name;
    });
    this.averageSlotTotalCal();
  }
  updateCaloriesOndemand(event, itemName,unit){
   console.log(event,itemName);
  // debugger;
   if(event.data!=null && !isNaN(parseInt(event.data))){
    let optionFood='';

    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray = 
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.filter(item=>{
      return itemName!==item;
    });
    let temp = itemName.split('~(')[1].split(')')[0];
    let tempName =itemName.split('[')[0];
    //debugger;
   // itemName.split('[')[0]+`[${parseInt(event.data)}~${unit})];
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.push(tempName+`[${event.data}~(${temp})]`);
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArrayJoin = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.join(',');
    
    for (let index = 0; index < this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options.length; index++) {
      optionFood = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options[index].Food;
      if(itemName.split('[')[0]==optionFood){
        this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options[index].portion = parseInt(event.data);
        break;
      }
    }
    this.setCaloriesByPortionupdate();
   }
   // 
  }
  removeMoreItems(food){  
   // debugger;
    const searchItem = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options.filter(item=>{
      return item?.name+`[${item?.portion}~(${item?.portionUnit})]` === food || item?.Food+`[${item?.portion}~(${item?.portionUnit})]` === food;
    });
    console.log("searchItemsearchItem:", searchItem);
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray =
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.filter(item=>{
      return item !== food;
    });
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArrayJoin = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.join(',');
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options =
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options.filter(item=>{
      return item.Food+`[${item?.portion}~(${item?.portionUnit})]` !== food;
    });
    if(searchItem.length>0 && searchItem[0].macros!==undefined){
    this.foodItemSlots[this.isOpenSlotIndex].avg_macros.calories = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.calories - (searchItem[0].macros.calories*searchItem[0].portion);
    this.foodItemSlots[this.isOpenSlotIndex].avg_macros.protein = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.protein - (searchItem[0].macros.protein*searchItem[0].portion);
    this.foodItemSlots[this.isOpenSlotIndex].avg_macros.fat = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.fat - (searchItem[0].macros.fat*searchItem[0].portion);
    this.foodItemSlots[this.isOpenSlotIndex].avg_macros.carbs = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.carbs - (searchItem[0].macros.carbs*searchItem[0].portion);
  //  this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portion = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portion - (searchItem[0].portion);
    this.foodItemsArray.total_macros.calories =this.foodItemsArray.total_macros.calories - (searchItem[0].macros.calories*searchItem[0].portion);
    this.foodItemsArray.total_macros.protein =this.foodItemsArray.total_macros.protein - (searchItem[0].macros.protein*searchItem[0].portion);
    this.foodItemsArray.total_macros.fat = this.foodItemsArray.total_macros.fat - (searchItem[0].macros.fat*searchItem[0].portion);
    this.foodItemsArray.total_macros.carbs =this.foodItemsArray.total_macros.carbs - (searchItem[0].macros.carbs*searchItem[0].portion);
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCalories = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCalories - (searchItem[0].macros.calories *searchItem[0].portion);
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsProtien =  this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsProtien - (searchItem[0].macros.protein*searchItem[0].portion);
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsFat = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsFat- (searchItem[0].macros.fat*searchItem[0].portion);
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCarbs = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCarbs- (searchItem[0].macros.carbs*searchItem[0].portion);
    }
    else if(searchItem.length>0 && searchItem[0].macros===undefined){
      this.foodItemSlots[this.isOpenSlotIndex].avg_macros.calories = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.calories - (searchItem[0].Calories*searchItem[0].portion);
      this.foodItemSlots[this.isOpenSlotIndex].avg_macros.protein = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.protein - (searchItem[0].Protein*searchItem[0].portion);
      this.foodItemSlots[this.isOpenSlotIndex].avg_macros.fat = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.fat - (searchItem[0].Fat*searchItem[0].portion);
      this.foodItemSlots[this.isOpenSlotIndex].avg_macros.carbs = this.foodItemSlots[this.isOpenSlotIndex].avg_macros.carbs - (searchItem[0].Carbs*searchItem[0].portion);
    //  this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portion = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portion - (searchItem[0].portion);
    
      this.foodItemsArray.total_macros.calories =this.foodItemsArray.total_macros.calories - (searchItem[0].Calories*searchItem[0].portion);
      this.foodItemsArray.total_macros.protein =this.foodItemsArray.total_macros.protein - (searchItem[0].Protein*searchItem[0].portion);
      this.foodItemsArray.total_macros.fat = this.foodItemsArray.total_macros.fat - (searchItem[0].Fat*searchItem[0].portion);
      this.foodItemsArray.total_macros.carbs =this.foodItemsArray.total_macros.carbs - (searchItem[0].Carbs*searchItem[0].portion);

      this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCalories = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCalories - (searchItem[0].Calories*searchItem[0].portion);
      this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsProtien =  this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsProtien - (searchItem[0].Protein*searchItem[0].portion);
      this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsFat = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsFat- (searchItem[0].Fat*searchItem[0].portion);
      this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCarbs = this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCarbs- (searchItem[0].Carbs*searchItem[0].portion);
    }
    this.bindData();
    setTimeout(()=>{
      this.averageSlotTotalCal();
    },100);
    
  }
  addItem:any=new Array(9);
  addNewItem(event,index){
    if(event.target.value.trim()===""){
      return this.utilities.presentAlert("Field should not be blank!");
    }
    const newItem ={
      "id": index,
      "name": event.target.value,
      "itemsArray":[],
      "itemsArrayJoin":'',
      "itemsCalories":0,
      "itemsProtien":0,
      "itemsFat":0,
      "itemsCarbs":0,
      "portion":0,
      "portionUinit":"",
      "options": []
    }
    this.foodItemSlots[index].items.push(newItem);
    this.addItem[index]="";
  }
  clearSearch() {
    console.log("Clear search called");
  }
  timeout: any = null;
  searchAllApiData(event){
  
    if(event.target.value.trim()===""){
      return this.utilities.presentAlert("Field should not be blank!");
    }
    if(this.timeout){ 
    clearTimeout(this.timeout);
    }
    //var $this = this;
    this.timeout = setTimeout(()=> {
      if (event.keyCode != 13) {
      console.log(event.target.value);
      
      this.aps.getUserToken(localStorage.getItem("email")).subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log("User token ", error.error.text);
          this.appService.searchFoodItemByName(event.target.value, error.error.text).then((resp)=>{
            this.searchItems =  resp;
            this.searchVisible=true;
            this.foodItemAdd="";
          })
          console.log("Category Error", error);
        }
      );
      }
  }, 1500);
   
   
    
  }
  searchVisible=false;
  foodItemAdd:any = "";
  selectedItem(searchItem){
    this.searchVisible=false;
    console.log("searchItem",searchItem);
    searchItem.macros={
        "calories": searchItem.Calories,
        "fat": searchItem.Fat,
        "protein": (searchItem.Protien==undefined?searchItem.Protein:searchItem.Protien),
        "carbs": searchItem.Carbs,
        "fiber": searchItem.Fiber
    }
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.push(searchItem.Food+`[${searchItem.portion}~(${searchItem.portion_unit})]`);
   // this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portion += searchItem.portion;
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].portionUnit = searchItem.portion_unit;
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArrayJoin = 
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsArray.join(',');
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].options.push(searchItem);
    if(this.totalLength[this.isOpenSlotIndex]>=0){
    this.totalLength[this.isOpenSlotIndex]=this.totalLength[this.isOpenSlotIndex]+1;
    }
    else{
      this.totalLength[this.isOpenSlotIndex]=0;
      this.totalLength[this.isOpenSlotIndex]=this.totalLength[this.isOpenSlotIndex]+1;
    }
    console.log("this.totalLength", this.totalLength);
    this.foodItemSlots[this.isOpenSlotIndex].avg_macros.calories+=(searchItem.Calories*searchItem.portion);
    this.foodItemSlots[this.isOpenSlotIndex].avg_macros.protein+= ((searchItem.Protien==undefined?searchItem.Protein:searchItem.Protien)*searchItem.portion);
    this.foodItemSlots[this.isOpenSlotIndex].avg_macros.fat+=(searchItem.Fat*searchItem.portion);
    this.foodItemSlots[this.isOpenSlotIndex].avg_macros.carbs+=(searchItem.Carbs*searchItem.portion);

    this.foodItemsArray.total_macros.calories+=(searchItem.Calories * searchItem.portion) ;
    this.foodItemsArray.total_macros.protein+=((searchItem.Protien==undefined?searchItem.Protein:searchItem.Protien)*searchItem.portion);
    this.foodItemsArray.total_macros.fat+=(searchItem.Fat*searchItem.portion);
    this.foodItemsArray.total_macros.carbs+=(searchItem.Carbs*searchItem.portion);
//debugger;
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCalories +=  (searchItem.Calories * searchItem.portion);
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsProtien += ((searchItem.Protien==undefined?searchItem.Protein:searchItem.Protien)*searchItem.portion);
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsFat +=  (searchItem.Fat*searchItem.portion);
    this.foodItemSlots[this.isOpenSlotIndex].items[this.isOpenItemIndex].itemsCarbs +=  (searchItem.Carbs*searchItem.portion);
    this.averageSlotTotalCal();
    this.cdr.detectChanges();
  }

  setCaloriesByPortionupdate(){
    let calories=0;
    let protein=0;
    let fat=0;
    let carbs=0;
    let slotCalory=0;
    let slotProtein=0;
    let slotFat=0;
    let slotCarbs=0;
    for (let index = 0; index < this.foodItemSlots.length; index++) {
      for (let j = 0; j < this.foodItemSlots[index].items.length; j++) {
        calories=0;
        protein=0;
        fat=0;
        carbs=0;
        for (let k = 0; k < this.foodItemSlots[index].items[j].options.length; k++) {
          calories += (this.foodItemSlots[index].items[j].options[k].macros.calories*this.foodItemSlots[index].items[j].options[k].portion);
          protein += (this.foodItemSlots[index].items[j].options[k].macros.protein*this.foodItemSlots[index].items[j].options[k].portion);
          fat += (this.foodItemSlots[index].items[j].options[k].macros.fat*this.foodItemSlots[index].items[j].options[k].portion);
          carbs += (this.foodItemSlots[index].items[j].options[k].macros.carbs*this.foodItemSlots[index].items[j].options[k].portion);
        }
        slotCalory=calories;
        slotProtein=protein;
        slotFat=fat;
        slotCarbs=carbs;
        this.foodItemSlots[index].items[j].itemsCalories=slotCalory;
        this.foodItemSlots[index].items[j].itemsProtien=slotProtein;
        this.foodItemSlots[index].items[j].itemsFat=slotFat;
        this.foodItemSlots[index].items[j].itemsCarbs=slotCarbs;
        slotCalory=0;
        slotProtein=0;
        slotFat=0;
        slotCarbs=0;
      }
      this.foodItemSlots[index].avg_macros.calories=calories;
    this.foodItemSlots[index].avg_macros.protein= protein;
    this.foodItemSlots[index].avg_macros.fat=fat;
    this.foodItemSlots[index].avg_macros.carbs=carbs;
      
    }
    this.foodItemsArray.total_macros.calories=calories;
      this.foodItemsArray.total_macros.protein=protein;
      this.foodItemsArray.total_macros.fat=fat;
      this.foodItemsArray.total_macros.carbs=carbs;
      this.averageSlotTotalCal();
  }

  goback(){
    this.router.navigate(['deitician-search']);
  }
  saveRecall(){
    const userId = localStorage.getItem('email');
      this.appService.saveDietRecall(userId,this.foodItemsArray).then(res=>{
        console.log("test", res);
        this.utilities.presentAlert("Record saved successfully!");
      },err=>{
        console.log("err", err);
        
      })
  }
}
