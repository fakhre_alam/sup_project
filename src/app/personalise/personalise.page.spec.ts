import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PersonalisePage } from './personalise.page';

describe('PersonalisePage', () => {
  let component: PersonalisePage;
  let fixture: ComponentFixture<PersonalisePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonalisePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PersonalisePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
