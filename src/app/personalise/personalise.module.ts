import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { PersonalisePageRoutingModule } from "./personalise-routing.module";

import { PersonalisePage } from "./personalise.page";
// import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from "../components/components.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PersonalisePageRoutingModule,
    // TranslateModule,
    ComponentsModule,
  ],
  declarations: [
    PersonalisePage,

  ],
})
export class PersonalisePageModule {}
