import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { UTILITIES } from "../core/utility/utilities";
import { Storage } from "@ionic/storage";
import { AppService } from "../app.service";
import { CONSTANTS } from "../core/constants/constants";
import moment from "moment";
import { TemplateService } from "../services";

@Component({
  selector: "app-personalise",
  templateUrl: "./personalise.page.html",
  styleUrls: ["./personalise.page.scss"],
})
export class PersonalisePage implements OnInit {
  currentPersonaliseIndex: number = 0;
  currentPersonaliseKey: string;
  currentPersonalise: any;
  totalpersonaliseItem: number;
  personaliseKeys: Array<string> = [];
  personalise: any;
  colorCode = ["#E4F3EC", "#FBEEFC", "#F2E5D8", "#E2F0F6", "#FBEEFC"];
  in: number;
  length: number;

  progress;

  drinks = [];
  image_URL = "";
  validation = { min: 0, msg: "" };
  name = "";
  tempCount = 0;
  isEdit = "";
  progressData = {
    in: 1,
    length: 0,
  };
  toConsume: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private storage: Storage,
    private utilities: UTILITIES,
    private appService: AppService,
    private templateService: TemplateService
  ) {
    this.route.queryParams.subscribe((res) => {
      if (res["prop"] != undefined) {
        // localStorage.setItem("edit", res["prop"]);
        this.isEdit = res["prop"];
      }
      if (res["toConsume"] != undefined) {
        // localStorage.setItem("edit", res["prop"]);
        this.toConsume = true;
      }
      // else {
      //   localStorage.setItem("edit", "");
      // }
    });
  }

  setCurrentPersonalise(index) {
    this.currentPersonaliseIndex = index;
    this.currentPersonaliseKey = this.personaliseKeys[index];
    this.currentPersonalise = this.personalise[this.currentPersonaliseKey];

    // this.progress = Math.round((this.currentPersonaliseIndex+1)*100/this.personaliseKeys.length);
    // this.PendingProgress=100-this.progress;
  }

  ngOnInit() {}

  async ionViewWillEnter() {
    this.utilities.presentLoading();
    this.getData();
  }

  personaliseName = [];
  indexName = 0;
  tempData = [];
  getData() {
    let self = this;
    this.personaliseName = [];
    let reqBody = {
      userId: localStorage.getItem("email"), //CONSTANTS.email
    };

    this.appService.getDietPreference(reqBody).then(
      (res) => {
        this.utilities.hideLoader();
        this.personalise = JSON.parse(JSON.stringify(res)).personalise.sort(
          function (a, b) {
            return a.slots - b.slots;
          }
        );
        this.tempData = JSON.parse(JSON.stringify(this.personalise));
        this.tempData.sort(function (a, b) {
          return a.slots - b.slots;
        });
        this.personaliseKeys = Object.keys(this.personalise);
        this.progressData.in = 1;
        this.progressData["length"] = this.personaliseKeys.length;
        this.setCurrentPersonalise(this.currentPersonaliseIndex);
        for (let index = 0; index < this.personalise.length; index++) {
          this.personaliseName.push(this.personalise[index].name);
        }
      },
      (err) => {
        this.utilities.hideLoader();
        this.utilities.presentAlert("Something went wrong. Please try again.");
      }
    );
  }

  clearAll(ind) {
    this.personalise[ind]?.foodItems.forEach((element) => {
      element.isSelected = false;
    });
    this.tempData[ind]?.foodItems.forEach((element) => {
      element.isSelected = false;
    });
  }

  clearSearchAndReset() {
    this.filterFunction("", this.tempData.length - 1);
  }
  filterData(event) {
    console.log(event.target.value);
    this.filterFunction(event.target.value, this.tempData.length - 1);
    // }
  }
  filterFunction(value, n) {
    if (n >= 0) {
      this.personalise[n].foodItems = this.tempData[n]?.foodItems.filter(
        (item) => {
          return item.name?.toLowerCase().includes(value?.toLowerCase());
        }
      );

      let nm = n - 1;
      this.filterFunction(value, nm);
    }
  }
  toggleSelect(item, id) {
    let selectedItem = this.personalise[item].foodItems.find(
      (item) => item["_id"] == id
    );
    selectedItem.isSelected = !selectedItem.isSelected;
  }

  goto() {
    //     if (this.currentPersonaliseIndex + 1 < this.personaliseKeys.length) {
    //       this.setCurrentPersonalise(this.currentPersonaliseIndex + 1);
    //       this.indexName = this.currentPersonaliseIndex;
    //       return true;
    //     }
    // console.log("is submitted");
    this.utilities.presentAlertConfirmBox(
      "Update will delete the current date and future date Diet plans. Please confirm to continue.",
      () => {
        //     this.progressData.in = ++this.currentPersonaliseIndex;
        this.submitData();
      }
    );
  }

  back() {
    this.router.navigate(["deitician-search"]);
  }
  itemData: any = [];
  submitData() {
    this.itemData = [];
    let selectedItem = [];
    this.personaliseKeys.forEach((key: any) => {
      selectedItem = [
        ...selectedItem,
        ...this.personalise[key].foodItems.filter(
          (item) => item.isSelected == true
        ),
      ];
      this.itemData.push({
        slot:
          key === "0"
            ? 2
            : key === "1"
            ? 4
            : key === "2"
            ? 4
            : key === "3"
            ? 6
            : 7,
        foodCodeList: this.personalise[key].foodItems.filter(
          (item) => item.isSelected == true
        ),
      });
    });
    for (let index = 0; index < this.itemData.length; index++) {
      this.itemData[index].foodCodeList = this.itemData[index].foodCodeList.map(
        (item) => {
          if (item.isSelected) return { code: item._id };
        }
      );
    }
    this.utilities.presentLoading();
    this.appService
      .updateDietPref(localStorage.getItem("email"), this.itemData)
      .then(
        (res) => {
          // dietitianId, actionId, userId,  actionName,  companyId
          this.utilities.hideLoader();
          setTimeout(() => {
            this.storage.set("dietData", null).then(() => {
              this.storage.set("optionsData", null).then(() => {
                CONSTANTS.dietDate = moment().format("DDMMYYYY");
                this.utilities.presentAlert("diet choices updated");
                localStorage.setItem("slideToFirst", "true");
                //    this.router.navigate(["deitician-search"]);
              });
            });
          }, 500);
          if (localStorage.getItem("dietitianId") != null) {
            this.templateService
              .handleDietitianAction(
                localStorage.getItem("dietitianId"),
                "668d72791073b512dd2b947f",
                localStorage.getItem("email"),
                "Save Diet Plan",
                localStorage.getItem("companyId")
              )
              .subscribe((response) => {
                console.log("Response>> ", response);
              });
          }
        },
        (err) => {
          this.utilities.hideLoader();
          this.utilities.presentAlert(
            "Something went wrong. Please try again."
          );
        }
      );
  }
}
