import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonalisePage } from './personalise.page';

const routes: Routes = [
  {
    path: '',
    component: PersonalisePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonalisePageRoutingModule {}
