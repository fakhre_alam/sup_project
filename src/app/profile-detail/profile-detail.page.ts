import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";
import { AppService as appS } from "../app.service";
import { AppService as appSer } from "../services/app/app.service";
import { TemplateService } from "../services/node/template.service";
import consultQA from "../../app/shared/constants/consultQA.json";
import moment from "moment";
import { ActivatedRoute, Router } from "@angular/router";
import { UTILITIES } from "../core/utility/utilities";
import { LeftSideBarComponent } from "../left-side-bar/left-side-bar.component";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { RouteCheckService } from "../services/route-check/route-check.service";
@Component({
  selector: "app-profile-detail",
  templateUrl: "./profile-detail.page.html",
  styleUrls: ["./profile-detail.page.scss"],
})
export class ProfileDetailPage implements OnInit {
  name: string = "";
  age: any;
  gender: any;
  height: any;
  heightInCms: boolean = false;
  weight: any;
  desiredWeight: number = 0;
  searchData: any = "";
  lifeStyle: any;
  profileData: any = { _id: 0 };
  activityChoosen: any;
  selectedOptions: any = [];
  diseases = [];
  diseases1 = [];
  alergies = ["SF,SO", "ML", "F", "E", "N", "G"];
  additionPreferences: any;
  dietPlanChoosen: any;
  mealPref;
  defaultData: any;
  community = [];
  diseasesChoosen = [];
  allergyChoosen: any = [];
  searchDetails: any;
  normalDay;
  cheatDay;
  detoxDay;
  instructionId;
  qaId;
  openFirst = true;
  open2 = true;
  open3 = true;
  open4 = true;
  open5 = true;
  loginId = localStorage.getItem("loginEmail");
  consultQA: any = [];
  diseasesOptions = [
    { code: "A", value: "ACIDITY" },
    { code: "AN", value: "ANEMIA" },
    { code: "AI", value: "ANTI-INFLAMMATORY" },
    { code: "B", value: "BLOOD PRESSURE" },
    { code: "C", value: "CHOLESTEROL" },
    { code: "CR", value: "CALCIUM-RICH" },
    { code: "D", value: "DIABETES" },
    { code: "PD", value: "PRE-DIABETES" },
    { code: "GD", value: "GESTATIONAL DIABETES" },
    // { code: "E", value: "EGESTION/ CONSTIPATION" },
    { code: "FL", value: "FATTY LIVER" },
    { code: "HP", value: "HIGH PROTEIN" },
    { code: "IR", value: "IRON-RICH" },
    { code: "LP", value: "LOW PROTEIN" },
    { code: "P", value: "PCOS" },
    { code: "T", value: "HYPOTHYROID" },
    { code: "S", value: "SLEEP DISORDER" },
    { code: "UA", value: "URIC ACID" },
    { code: "VB", value: "VITAMIN B12" },
    { code: "VD", value: "VITAMIN D" },
    { code: "W", value: "WIND/ FLATULENCE/ BLOATING" },
  ];
  communityOptions = [
    { code: "U", label: "All regions (Universal)" },
    { code: "P", label: "North India" },
    { code: "S", label: "South India" },
    { code: "M", label: "Maharashtra" },
    { code: "G", label: "Gujarat" },
    { code: "B", label: "Bengali" },
  ];
  dietPrefOptions = [
    {
      code: "V",
      value: "Vegetarian",
      isSelected: false,
    },
    {
      code: "NV",
      value: "Non-Vegetarian",
      isSelected: false,
    },
    {
      code: "E",
      value: "Vegetarian + Egg",
      isSelected: false,
    },
    {
      code: "Ve",
      value: "Vegan",
      isSelected: false,
    },
  ];
  activitiesOptions = [
    {
      code: "AC1",
      value: "Sedentary (No Exercise)",
      data: 1.2,
      steps: 7500,
      calories: 200,
      isSelected: false,
    },
    {
      code: "AC2",
      value: "Lightly Active (Brisk Walking, Yoga, functional exercise)",
      data: 1.375,
      steps: 7500,
      calories: 300,
      isSelected: false,
    },
    {
      code: "AC3",
      value: "Moderately Active (Workout 5 days a week)",
      data: 1.55,
      steps: 10000,
      calories: 400,
      isSelected: false,
    },
    {
      code: "AC4",
      value: "Super Active (Rigrous workout, 5 days a week)",
      data: 1.7,
      steps: 10000,
      calories: 600,
      isSelected: false,
    },
    {
      code: "AC5",
      value: "Extremly Active (Rigrous workout, 2 times a day)",
      data: 1.9,
      steps: 10000,
      calories: 600,
      isSelected: false,
    },
  ];
  allergiesOptions = [
    { code: "N", label: "NUTS" },
    { code: "E", label: "EGGS" },
    { code: "F", label: "FISH" },
    { code: "ML", label: "MILK/ LACTOSE" },
    { code: "SO", label: "SOYA" },
    { code: "SF", label: "SEAFOOD" },
    { code: "G", label: "GLUTEN" },
  ];
  dietPlan = [
    { id: "fiteloWaterRetention", name: "Water retention" },
    { id: "fiteloWeightLoss", name: "High Protein fiber" },
    { id: "weightLoss", name: "Weight Loss" },
    { id: "muscleGain_morning", name: "Muscle Gain Morning" },
    { id: "muscleGain_evening", name: "Muscle Gain Evening" },
    { id: "fatShredding_morning", name: "Fat Shredding Morning" },
    { id: "fatShredding_evening", name: "Fat Shredding Evening" },
    { id: "diabetes", name: "Diabetes" },
    { id: "pcos", name: "PCOS" },
    { id: "cholesterol", name: "Cholesterol" },
    { id: "hypertension", name: "Hypertension" },
  ];
  goalName = "";
  constructor(
    private appServ: appSer,
    private router: Router,
    private appServe: AppService,
    private appservice: appS,
    private templateService: TemplateService,
    private utilities: UTILITIES,
    private iab: InAppBrowser,
    private queryParams: ActivatedRoute,
    private routeCheckService: RouteCheckService,
    private activeRouter: ActivatedRoute
  ) {
    const leftnav = new LeftSideBarComponent(
      router,
      appServ,
      appservice,
      iab,
      queryParams,
      routeCheckService,
      activeRouter
    );
  }
  showSkel = true;
  isBeato = false;
  ngOnInit() {
    this.isBeato = localStorage.getItem("loginEmail")
      ? localStorage
          .getItem("loginEmail")
          .toLowerCase()
          .includes("beatoapp.com")
      : false;
    if (
      localStorage.getItem("email") === "" ||
      localStorage.getItem("email") === undefined ||
      localStorage.getItem("email") === null
    ) {
      this.router.navigate(["dashboard"]);
      return;
    }
    this.appServ.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {
        console.log(response);
      },
      (error) => {
        this.appServe.getDefaultData(error.error.text).then((res) => {
          this.defaultData = res;
        });
      }
    );
    setTimeout(() => {
      this.getProfile();
      this.getInstructions();
      this.getQA();
      this.getHealthParams();
      this.getActionLog();
      this.getDietitian();
    }, 3000);
  }
  getInstructions() {
    this.templateService
      .getInstructions(localStorage.getItem("email"))
      .subscribe(
        (response) => {
          console.log("Insrtutions-Result >> ", response);
          if (response[0]) {
            console.log("$$$$$$$$$$$$$$$$$$ ", response[0]);
            this.normalDay = response[0]?.normalDay;
            this.cheatDay = response[0]?.cheatDay;
            this.detoxDay = response[0]?.detoxDay;
            this.instructionId = response[0]?._id;
            console.log("Id:: ", this.instructionId);
          }
        },
        (error) => {
          console.error(
            "------------>> Error Getting instrutions",
            error.error
          );
        }
      );
  }

  dietitianName = "";
  getDietitian() {
    const query = `mail=${localStorage.getItem("loginEmail")}`;

    this.templateService.getDietitianRecord(query).subscribe(
      (response) => {
        console.log(":: Fetch Dietician Record ", response);
        if (response != null) {
          this.dietitianName = response["name"];
        }
      },
      (err) => {
        console.log("Error Response>> ", err);
      }
    );
  }
  activityBind = [];
  bindActivity(activity) {
    const dt = activity?.split(",");
    for (let index = 0; index < dt.length; index++) {
      const data = this.activitiesOptions.filter((item) => {
        return item.code == dt[index];
      });
      if (data.length > 0) {
        for (let index = 0; index < data.length; index++) {
          this.activityBind.push(data[index].value);
        }
      }
    }
    if (this.activityBind.length == 0) {
      this.activityBind.push("NA");
    }
  }
  foodPrefernceBind = [];
  bindFoodPreference(foodType) {
    const dt = foodType?.split(",");
    for (let index = 0; index < dt.length; index++) {
      const data = this.dietPrefOptions.filter((item) => {
        return item.code == dt[index];
      });
      if (data.length > 0) {
        for (let index = 0; index < data.length; index++) {
          this.foodPrefernceBind.push(data[index].value);
        }
      }
    }
    if (this.foodPrefernceBind.length == 0) {
      this.foodPrefernceBind.push("NA");
    }
  }
  communityBind = [];
  communityBinding(community) {
    const dt = community;
    for (let index = 0; index < dt.length; index++) {
      const data = this.communityOptions.filter((item) => {
        return item.code == dt[index];
      });
      if (data.length > 0) {
        for (let index = 0; index < data.length; index++) {
          this.communityBind.push(data[index].label);
        }
      }
    }
    if (this.communityBind.length == 0) {
      this.communityBind.push("NA");
    }
  }
  disorderBind = [];
  disorderBinding(disorder) {
    const dt = disorder;
    for (let index = 0; index < dt.length; index++) {
      const data = this.diseasesOptions.filter((item) => {
        return item.code == dt[index];
      });
      if (data.length > 0) {
        for (let index = 0; index < data.length; index++) {
          this.disorderBind.push(data[index].value);
        }
      }
    }
    if (this.disorderBind.length == 0) {
      this.disorderBind.push("NA");
    }
  }
  allergiesBind = [];
  // allergiesBinding(allergies){
  //   const dt = allergies;
  //   for (let index = 0; index < dt?.length; index++) {
  //     const data = this.allergiesOptions.filter(item=>{
  //       return item.code == dt[index];
  //     });
  //     if(data.length>0){
  //       for (let index = 0; index < data.length; index++) {
  //         this.allergiesBind.push(data[index].label);
  //       }
  //     }

  //   }
  //   if(this.allergiesBind.length==0){
  //     this.allergiesBind.push('NA');
  //   }
  // }
  getProfile() {
    this.profileData = { _id: 0 };
    this.appservice.getProfile().then((res) => {
      if (res["profile"] === undefined) {
        this.router.navigate(["dashboard"]);
        return;
      }

      //   debugger;
      this.profileData = res;
      this.name = res["profile"]["name"];
      this.age = res["demographic"]["age"];
      this.gender = res["demographic"]["gender"];

      var hVar = res["demographic"]["height"];
      if (hVar.unit === "in") {
        this.saveHeight(hVar);
      } else {
        this.height = { ...hVar, feet: hVar.value, inches: 0 };
      }
      this.weight = res["demographic"]["weight"];
      this.desiredWeight = Math.round(res["demographic"]["suggestedWeight"]);

      if (this.profileData?.profile?.subCategory === "weightloss") {
        this.profileData.profile.subCategory = "Weight Loss";
      }
      if (this.profileData?.profile?.subCategory === "weightmaintenance") {
        this.profileData.profile.subCategory = "Weight Maintenance";
      }
      if (this.profileData?.profile?.subCategory === "musclebuilding") {
        this.profileData.profile.subCategory = "Muscle Building";
      }
      if (this.profileData?.profile?.subCategory === "leanbody") {
        this.profileData.profile.subCategory = "Lean Body";
      }
      console.log("profileData::>> ", this.profileData);
      this.calories = this.profileData?.lifeStyle?.calories;
      this.bindActivity(this.profileData?.lifeStyle?.activities?.code);
      this.bindFoodPreference(this.profileData?.lifeStyle?.foodType);
      this.communityBinding(this.profileData?.lifeStyle?.communities);
      this.disorderBinding(this.profileData?.lifeStyle?.diseases);

      this.bindallergies(this.profileData?.lifeStyle?.diseases);

      const dietPlanCode = this.profileData?.lifeStyle?.dietPlanName;
      const exists = this.dietPlan.some(
        (plan) => plan.id === dietPlanCode || plan.name === dietPlanCode
      );

      // Add the item if it doesn't exist
      if (!exists) {
        this.dietPlan.push({
          id: dietPlanCode,
          name: dietPlanCode,
        });
      }
      const dietPlanVar = this.dietPlan.find(
        (item) => item.id === dietPlanCode
      );
      // alert(JSON.stringify(this.profileData));
      this.goalName = dietPlanVar?.name;
    });
  }
  refAllergies = ["SF", "SO", "ML", "F", "E", "N", "G"];
  bindallergies(desease) {
    desease.forEach((diseaseCode) => {
      this.defaultData?.otherMaster?.diseases.forEach((disease) => {
        if (disease.code === diseaseCode) {
          if (this.refAllergies.includes(disease.code)) {
            this.allergiesBind.push(disease.value);
            // alert("added");
          } else {
            this.diseases.push(disease.value);
          }
        }
      });
    });
  }

  actionLog: any = [];
  getActionLog() {
    this.templateService.getActionLog(localStorage.getItem("email")).subscribe(
      (response: any) => {
        this.actionLog = response;
      },
      (error) => {
        console.error("------------>> Error Getting action log", error.error);
      }
    );
    this.showSkel = false;
  }
  formatDate(dateData) {
    return moment(dateData).format("DD-MMM-yyyy");
  }
  formatTime(dateData) {
    return moment(dateData).format("hh:mm:ss");
  }
  fetchedData: any = [];
  getHealthParams() {
    this.templateService
      .getHealthParams(localStorage.getItem("email"))
      .subscribe(
        (res) => {
          this.fetchedData = res;
          this.getActive();
          console.log("Fetched Data-->> ", res);
        },
        (error) => {
          console.log("Category Error", error);
        }
      );
  }
  healthRecords: any = [];
  getActive() {
    this.healthRecords = this.fetchedData.filter((record) => record.active);
    console.log("HealthRecords::>> ", this.healthRecords);
  }

  getQA() {
    this.templateService.getQaByUser(localStorage.getItem("email")).subscribe(
      (response) => {
        console.log("!!!!!!!!!!--QA-Result >> ", response);
        if (response[0]) {
          console.log("$$$$$$$$$$$$$$$$$$ ", response[0]);
          this.consultQA = response[0].content;
          this.qaId = response[0]?._id;
        }
      },
      (error) => {
        console.error("------------>> Error Getting instrutions", error.error);
      }
    );
  }
  saveHeight(hObj: any) {
    const inches: number = hObj.value;
    const feet = Math.floor(inches / 12);
    const remainingInches = parseFloat((inches % 12).toFixed(2));

    console.log("Feet: ", feet, " || Inches: ", remainingInches);
    this.height = { ...hObj, feet: feet, inches: remainingInches };
    console.log("Heighttt:: >> ", this.height);
    // return `${feet}' ${remainingInches}"`;
  }

  editProfile() {
    const menu = {
      title: "View/Edit Profile",
      description: [
        {
          name: "This page allows you to view and update user information such as:",
          desc: [
            "Name",
            "Age",
            "Weight",
            "Height",
            "Activity level",
            "Food choices",
            "Disorders",
            "Allergies",
            "Diet Plan",
          ],
        },
      ],
      menu: "Profile",
      subMenu: "",
      activeIndex: 1,
    };
    localStorage.setItem("menu", JSON.stringify(menu));
    localStorage.setItem("activeNum", "1");
    this.router.navigate(["edit-profile"]).then(() => {
      window.location.reload();
    });
  }
  calories: any;
  updateCal(cal) {
    if (!localStorage.getItem("email")) {
      // alert("Please Enter Customer ID");
      return;
    } else {
      this.utilities.presentAlertConfirmBox(
        "Update will delete the current date and future date Diet plans. Please confirm to continue.",
        () => {
          this.appServ.getUserToken(localStorage.getItem("email")).subscribe(
            (response) => {
              console.log(response);
            },
            (error) => {
              console.log("User token ", error.error.text);
              this.appservice.updateTargetCal(
                cal,
                localStorage.getItem("email"),
                error.error.text
              );
              this.appServe.getDefaultData(error.error.text).then((res) => {
                this.defaultData = res;
              });
              // debugger;
              // this.utilities.presentAlert("Calories Updated");
              this.isUpdateCal = false;
              // this.profileData.lifeStyle.calories = Number(this.calories);
              window.location.reload();
              localStorage.setItem("tknupdatetarget", error.error.text);
              console.log("Category Error", error);
            }
          );
        }
      );
    }
  }

  editQA() {
    const menu = {
      title: "QA",
      description: [
        {
          name: "This section logs all the questions and answers exchanged with the user.",
          desc: [],
        },
      ],
      menu: "Profile",
      subMenu: "",
      activeIndex: 1,
    };
    localStorage.setItem("menu", JSON.stringify(menu));
    localStorage.setItem("activeNum", "1");
    this.router.navigate(["consult-qa"]).then(() => {
      window.location.reload();
    });
  }
  isUpdateCal = false;
  openPopupForUpdateCalory() {
    this.isUpdateCal = true;
  }
  closeUpdateCal() {
    this.isUpdateCal = false;
  }
  instructions() {
    const menu = {
      title: "Instructions",
      description: [
        {
          name: "Saved instructions are displayed to users and can be categorized for different days:",
          desc: ["Normal days", "Detox days", "Cheat days"],
        },
        {
          name: "Instructions will appear based on the user's specific diet plan.",
          desc: [],
        },
      ],
      menu: "Instructions",
      subMenu: "",
      activeIndex: 5,
    };
    localStorage.setItem("menu", JSON.stringify(menu));
    localStorage.setItem("activeNum", "5");

    this.router.navigate(["add-instructions"]).then(() => {
      window.location.reload();
    });
  }
  health() {
    const menu = {
      title: "View/Edit Profile",
      description: [
        {
          name: "This page allows you to view and update user information such as:",
          desc: [
            "Name",
            "Age",
            "Weight",
            "Height",
            "Activity level",
            "Food choices",
            "Disorders",
            "Allergies",
            "Diet Plan",
          ],
        },
      ],
      menu: "Profile",
      subMenu: "",
      activeIndex: 1,
    };
    localStorage.setItem("menu", JSON.stringify(menu));
    localStorage.setItem("activeNum", "1");
    this.router.navigate(["health-parameters"]).then(() => {
      window.location.reload();
    });
  }
  action() {
    this.router.navigate(["dashboard"]);
  }
}
