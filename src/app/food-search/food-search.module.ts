import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { DietPlanListPageRoutingModule } from "./food-search-routing.module";

import { DietPlanListPage } from "./food-search.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DietPlanListPageRoutingModule,
  ],
  declarations: [DietPlanListPage],
})
export class DietPlanListPageModule {}
