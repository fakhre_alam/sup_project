import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { DietPlanListPage } from "./food-search.page";

const routes: Routes = [
  {
    path: "",
    component: DietPlanListPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DietPlanListPageRoutingModule {}
