import { Component, OnInit } from "@angular/core";
import { LoaderService, TemplateService } from "../services";
import { Router } from "@angular/router";
import { UTILITIES } from "../core/utility/utilities";

@Component({
  selector: "app-food-search",
  templateUrl: "./food-search.page.html",
  styleUrls: ["./food-search.page.scss"],
})
export class DietPlanListPage implements OnInit {
  dietRecords: Object[];
  isManisha: Boolean =
    localStorage.getItem("loginEmail") === "manisha@smartdietplanner.com";
  isIndividual = localStorage.getItem("isIndividual") === "true";
  companyName: string = localStorage.getItem("companyId");
  searchTerm: string = "";
  filteredFoodItems: Object[] = [];
  dietitianId = localStorage.getItem("dietitianId");
  planId: string;

  constructor(
    private loaderService: LoaderService,
    private templateService: TemplateService,
    private utilities: UTILITIES,
    private router: Router
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    // const query = this.isIndividual ? `?dietitianId=${this.dietitianId}` : this.isManisha  ? "" : `?company=${this.companyName}`;

    const query = this.isManisha
      ? `?dietitianId=*`
      : `?dietitianId=${this.dietitianId}`;

    this.loaderService.presentLoader("Fetching details...").then((_) => {
      this.templateService.getFoodRecipeRecords(query).subscribe(
        (response: Object[]) => {
          // this.templateService.getFoodIngredientsRecords(query).subscribe((res) => {
          this.loaderService.hideLoader();
          this.dietRecords = response["records"];
          this.filteredFoodItems = response["records"];
          console.log(":: Get Diet Plan Record ", response);
          // this.filteredFoodItems = [...res["records"], ...response];
          // console.log(">>>>>>>>>> ", res["records"]);
          console.log("::::::: ", this.filteredFoodItems);
          // })
        },
        (err) => {
          console.log("Error Response>> ", err);
          this.loaderService.hideLoader();
        }
      );
    });
  }

  handleClick(id: String, imported: Object) {
    // alert(JSON.stringify(imported["foodType"]));

    if (this.isRecipeWithIngredients(imported["foodType"])) {
      this.router.navigate(["add-recipe"], {
        queryParams: { id },
      });
    } else {
      this.router.navigate(["suggest-recipe"], {
        queryParams: { id },
      });
    }
  }

  isRecipeWithIngredients(foodType) {
    return foodType === undefined ? true : false;
  }

  handleReviewBtn(id, imported) {
    // alert(id);
    this.router.navigate(["suggest-recipe"], {
      queryParams: { id },
    });
  }

  handleReviewBtn2(id, imported) {
    // alert(id);
    this.router.navigate(["suggest-recipe"], {
      queryParams: { id, designed: true },
    });
  }

  hoveredStatusId: string | null = null;

  onHover(statusId: string | null) {
    this.hoveredStatusId = statusId;
  }

  getFoodType(type: string): string {
    // console.log("Type:: ", type)
    const foodTypeOptions = [
      { code: "V", value: "Vegetarian" },
      { code: "NV", value: "Non-Vegetarian" },
      { code: "E", value: "Vegetarian + Egg" },
      { code: "Ve", value: "Vegan" },
    ];

    const option = foodTypeOptions.find((plan) => plan.code === type);
    return option ? option.value : "Unknown Food Type"; // Fallback if no match found
  }

  status: string; // default or initial value
  foodSearchTerm: string;
  dietitianNameSearchTerm: string;
  infoTablePopup: boolean = false;

  onSearchInput(event: Event) {
    const searchTerm = (event.target as HTMLInputElement).value
      .toLowerCase()
      .trim();
    this.foodSearchTerm = searchTerm;

    if (
      this.status &&
      this.status !== "all" &&
      this.dietitianNameSearchTerm &&
      this.dietitianNameSearchTerm !== ""
    ) {
      this.filteredFoodItems = this.dietRecords.filter(
        (item) =>
          item["dietitianName"]
            .toLowerCase()
            .includes(this.dietitianNameSearchTerm) &&
          item["Food"].toLowerCase().includes(searchTerm) &&
          item["status"] === this.status
      );
    } else if (
      this.dietitianNameSearchTerm &&
      this.dietitianNameSearchTerm !== ""
    ) {
      this.filteredFoodItems = this.dietRecords.filter(
        (item) =>
          item["dietitianName"]
            .toLowerCase()
            .includes(this.dietitianNameSearchTerm) &&
          item["Food"].toLowerCase().includes(searchTerm)
      );
    } else if (this.status && this.status !== "all") {
      this.filteredFoodItems = this.dietRecords.filter(
        (item) =>
          item["Food"].toLowerCase().includes(searchTerm) &&
          item["status"] === this.status
      );
    } else {
      this.filteredFoodItems = this.dietRecords.filter((item) =>
        item["Food"].toLowerCase().includes(searchTerm)
      );
    }
  }

  onDietitianSearchInput(event: Event) {
    const searchTerm = (event.target as HTMLInputElement).value
      .toLowerCase()
      .trim();
    this.dietitianNameSearchTerm = searchTerm;

    if (
      this.status &&
      this.status !== "all" &&
      this.foodSearchTerm &&
      this.foodSearchTerm !== ""
    ) {
      this.filteredFoodItems = this.dietRecords.filter(
        (item) =>
          item["dietitianName"].toLowerCase().includes(searchTerm) &&
          item["Food"].toLowerCase().includes(this.foodSearchTerm) &&
          item["status"] === this.status
      );
    } else if (this.foodSearchTerm && this.foodSearchTerm !== "") {
      this.filteredFoodItems = this.dietRecords.filter(
        (item) =>
          item["dietitianName"].toLowerCase().includes(searchTerm) &&
          item["Food"].toLowerCase().includes(this.foodSearchTerm)
      );
    } else if (this.status && this.status !== "all") {
      this.filteredFoodItems = this.dietRecords.filter(
        (item) =>
          item["dietitianName"].toLowerCase().includes(searchTerm) &&
          item["status"] === this.status
      );
    } else {
      this.filteredFoodItems = this.dietRecords.filter((item) =>
        item["dietitianName"].toLowerCase().includes(searchTerm)
      );
    }
  }

  statusDropDownHandler(event: any) {
    const selectedStatus = event.detail.value;
    this.status = selectedStatus;
    console.log("Selected Status:", selectedStatus);
    // Add further logic based on the selected status

    if (
      this.status &&
      this.status !== "all" &&
      this.foodSearchTerm &&
      this.foodSearchTerm !== "" &&
      this.dietitianNameSearchTerm &&
      this.dietitianNameSearchTerm !== ""
    ) {
      this.filteredFoodItems = this.dietRecords.filter(
        (item) =>
          item["dietitianName"]
            .toLowerCase()
            .includes(this.dietitianNameSearchTerm) &&
          item["Food"].toLowerCase().includes(this.foodSearchTerm) &&
          item["status"] === selectedStatus
      );
    } else if (
      this.status &&
      this.status !== "all" &&
      this.foodSearchTerm &&
      this.foodSearchTerm !== ""
    ) {
      this.filteredFoodItems = this.dietRecords.filter(
        (item) =>
          item["Food"].toLowerCase().includes(this.foodSearchTerm) &&
          item["status"] === selectedStatus
      );
    } else if (
      this.status &&
      this.status !== "all" &&
      this.dietitianNameSearchTerm &&
      this.dietitianNameSearchTerm !== ""
    ) {
      this.filteredFoodItems = this.dietRecords.filter(
        (item) =>
          item["dietitianName"]
            .toLowerCase()
            .includes(this.dietitianNameSearchTerm) &&
          item["status"] === selectedStatus
      );
    } else if (selectedStatus == "all") {
      if (
        this.foodSearchTerm &&
        this.foodSearchTerm !== "" &&
        this.dietitianNameSearchTerm &&
        this.dietitianNameSearchTerm !== ""
      ) {
        this.filteredFoodItems = this.dietRecords.filter(
          (item) =>
            item["dietitianName"]
              .toLowerCase()
              .includes(this.dietitianNameSearchTerm) &&
            item["Food"].toLowerCase().includes(this.foodSearchTerm)
        );
      } else if (this.foodSearchTerm && this.foodSearchTerm !== "") {
        this.filteredFoodItems = this.dietRecords.filter((item) =>
          item["Food"].toLowerCase().includes(this.foodSearchTerm)
        );
      } else if (
        this.dietitianNameSearchTerm &&
        this.dietitianNameSearchTerm !== ""
      ) {
        this.filteredFoodItems = this.dietRecords.filter((item) =>
          item["dietitianName"]
            .toLowerCase()
            .includes(this.dietitianNameSearchTerm)
        );
      } else {
        this.filteredFoodItems = this.dietRecords;
      }
    } else {
      this.filteredFoodItems = this.dietRecords.filter(
        (item) => item["status"] === selectedStatus
      );
    }
  }

  proposeBtnHandler() {
    this.router.navigate(["suggest-recipe"]);
  }

  addIngredientsBtnHandler() {
    this.router.navigate(["add-recipe"]);
  }

  getLabelFromValue(value: string): string | undefined {
    const typeOptions = [
      { label: "Dry Vegetables", value: "A" },
      { label: "Bread/ Roti/ Rice", value: "B" },
      { label: "Curry", value: "C" },
      { label: "Non-dairy drinks/ Plant milk drinks", value: "D" },
      { label: "Milk based drinks", value: "DM" },
      { label: "Smoothies", value: "WS" },
      { label: "Drinks with zero calories", value: "DZ" },
      { label: "Fruits", value: "F" },
      { label: "Meals", value: "M" },
      { label: "Nuts", value: "N" },
      { label: "Nut Seeds", value: "NS" },
      { label: "Post Workout Snacks", value: "PW" },
      { label: "Raita", value: "R" },
      { label: "Side Salad", value: "S" },
      { label: "Soup", value: "SO" },
      { label: "Snacks", value: "W" },
      { label: "Desserts", value: "WD" },
      { label: "Parantha", value: "WP" },
      { label: "Yogurt", value: "Y" },
    ];
    // console.log("Value::>> ", value);

    const foundOption = typeOptions.find((option) => option.value === value);
    return foundOption ? foundOption.label : value ? value : "None";
  }

  objCliked: Object;
  handleInfoClick(item: Object) {
    // alert(id);
    this.infoTablePopup = true;
    this.objCliked = item;
  }

  closePopup() {
    this.infoTablePopup = false;
  }

  dietitianInfoHandler(id: string) {
    // alert(id)
    this.router.navigate(["dietitian-info"], {
      queryParams: { id },
    });
  }

  showUploadView: boolean = false;
  importDataHandler() {
    this.showUploadView = true;
  }

  closePopup2() {
    this.showUploadView = false;
  }

  handleImportBtnClick() {
    this.loaderService.presentLoader("Fetching details...").then((_) => {
      this.templateService.getRecordFromOrigin(this.planId).subscribe(
        (response) => {
          this.loaderService.hideLoader();
          if (!response) {
            this.utilities.presentAlert("Incorrect Id");
            this.planId = null;
            return;
          }
          response["status"] = "submitted";
          response["imported"] = true;
          console.log("Get getRecordFromOrigin API Response:: ", response);
          console.log("|||| Status:: ", response["status"]);
          this.planId = null;
          const id = response["_id"];
          this.showUploadView = false;
          this.router.navigate(["suggest-recipe"], {
            queryParams: { id, type: "origin" },
          });
        },
        (error) => {
          this.loaderService.hideLoader();
          console.log("getRecordFromOrigin Error: ", error);
          this.utilities.presentAlert("Something went wrong");
          this.showUploadView = false;
        }
      );
    });
  }
}
