import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'fulldayname'
})
export class FulldaynamePipe implements PipeTransform {

  transform(value: string) {
    switch(value){
      case 'SUN':{
       return 'Sunday';
        break;
      }
      case 'MON':{
        return 'Monday';
        break;
      }
      case 'TUE':{
        return 'Tuesday';
        break;
      }
      case 'WED':{
        return 'Wednesday';
        break;
      }
      case 'THU':{
        return 'Thursday';
        break;
      }
      case 'FRI':{
        return'Friday';
        break;
      }
      case 'SAT':{
        return'Saturday';
        break;
      }
      default:
        ''
        break;
    }
  }

}
