import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DietitianPlanPageRoutingModule } from './dietitian-plan-routing.module';

import { DietitianPlanPage } from './dietitian-plan.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DietitianPlanPageRoutingModule
  ],
  declarations: [DietitianPlanPage]
})
export class DietitianPlanPageModule {}
