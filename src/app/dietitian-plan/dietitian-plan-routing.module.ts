import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DietitianPlanPage } from './dietitian-plan.page';

const routes: Routes = [
  {
    path: '',
    component: DietitianPlanPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DietitianPlanPageRoutingModule {}
