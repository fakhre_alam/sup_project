import { Component, OnInit } from "@angular/core";
import { AppService } from "src/app/app.service";
import { HttpService, TemplateService } from "../services";
import { HttpClient } from "@angular/common/http";
import { UTILITIES } from "../core/utility/utilities";
declare const RazorpayCheckout: any;
@Component({
  selector: "app-dietitian-plan",
  templateUrl: "./dietitian-plan.page.html",
  styleUrls: ["./dietitian-plan.page.scss"],
})
export class DietitianPlanPage implements OnInit {
  showBuyPopup: boolean = false;
  dietitianPlans = [];
  constructor(
    private appService: AppService,
    private templateService: TemplateService,
    private http: HttpClient,
    private utilities: UTILITIES,
    private httpService: HttpService
  ) {}

  ngOnInit() {
    this.getDietitianPlan();
    const script = document.createElement("script");
    script.src = "https://checkout.razorpay.com/v1/checkout.js";
    document.body.appendChild(script);
  }

  getDietitianPlan() {
    this.httpService.getPlans().subscribe(
      (response) => {
        console.log("Get Plans API Response:: ", response["couponList"]);
        this.dietitianPlans = response["couponList"].sort(
          (a, b) => a.perid - b.perid
        );
      },
      (error) => {
        console.log("Initiate Payment Error: ", error);
      }
    );
  }

  roundValue(value: number): number {
    return Math.round(value);
  }

  gotoPayment(item: any) {
    console.log("Payment ::>> ", item);
    // this.payNow(4);
    // this.showBuyPopup = true;
    this.payNow2(item);
    // this.createRzpayOrder();
    // this.templateService.initiatePayment('').subscribe(
    //   (response) => {
    //     console.log("API Response:: ",response );

    //   },
    //   (error) => {
    //     console.log("Initiate Payment Error: ", error);
    //   }
    // );
  }

  async payNow2(amount: any) {
    // Create order by calling the server endpoint
    const order = await this.http
      .post<any>("https://app.smartdietplanner.com:8444/dietitian/payment", {
        amount: amount["discountedAmount"],
        couponCode: amount["couponCode"], //"CU2600",
        referralCode: null,
        emailId: localStorage.getItem("loginEmail"),
      })
      .toPromise();

    console.log("Order::>> ", order);
    // alert(JSON.stringify(order));

    // Open Razorpay Checkout
    const options: any = {
      key: "rzp_live_kKh0sHoMajNYC0",
      amount: order.amount,
      currency: order.currency,
      name: "Fitrofy",
      // description: "Test Transaction",
      order_id: order.orderId, // This is the order_id created in the backend
      // prefill: {
      //   name: "Your Name",
      //   email: "your.email@example.com",
      //   contact: "9999999999",
      // },
      theme: {
        color: "#052CEB",
      },
      handler: (response: any) => {
        this.verifyPayment2(response, order);
      },
      modal: {
        ondismiss: () => {
          this.utilities.presentAlert("Payment process has been cancelled"); // Handle modal dismissal
        },
      },
    };

    const rzp = new (window as any).Razorpay(options);
    rzp.open();
  }

  async verifyPayment2(paymentResponse: any, order: any) {
    // {_id: '670cbb1f1c596c66ff446f78', orderId: 'order_P8ouewkJFKIBRk', email: 'test@gmail.com', txnId: 'a3f6e5d0-db1b-488c-b27a-517b31f8a5d2', couponCode: 'new3months', …}

    // alert(JSON.stringify(order));

    try {
      console.log("PaymentResponse:: -> ", paymentResponse);

      const verification = await this.http
        .post<any>(
          "https://app.smartdietplanner.com:8444/dietitian/payment/confirm",
          {
            razorpayOrderId: order.orderId,
            razorpayPaymentId: paymentResponse.razorpay_payment_id,
            razorpaySignature: paymentResponse.razorpay_signature,
            txnId: order.txnId,
            email: order.email,
          }
        )
        .toPromise();

      if (verification) {
        this.utilities.presentAlert("Payment Successful");

        setTimeout(() => {
          // Reload the page
          window.location.reload();

          // After the page reloads, redirect to the home screen
          setTimeout(() => {
            window.location.href = "/dashboard"; // Change '/home' to your home screen route
          }, 100); // Small delay after reload
        }, 30);
      } else {
        this.utilities.presentAlert("Payment verification failed");
      }
    } catch (error) {
      console.error("Error:", error);
      this.utilities.presentAlert("Payment verification failed");
    }
  }

  async createRzpayOrder() {
    // console.log('this.couponCode', this.couponCode);
    const success = await this.http
      .post<any>("https://app.smartdietplanner.com:8444/dietitian/payment", {
        amount: 3999,
        couponCode: "diet2", // "new3months",
        referralCode: null,
        emailId: "test@gmail.com",
      })
      .toPromise();

    // alert("razorPayClicked :: true");
    this.payWithRazor(JSON.parse(JSON.stringify(success)));
  }

  payWithRazorpay(val) {
    // this.utilities.showLoading();
    const options: any = {
      key: val.keyId,
      amount: val.amount, // amount should be in paise format to display Rs 1255 without decimal point
      currency: "INR",
      name: "", // company name or product name
      description: "", // product description
      // image: './assets/icons/logoround.png', // company logo or product image
      order_id: val.orderId, // order_id created by you in backend
      modal: {
        // We should prevent closing of the form when esc key is pressed.
        escape: false,
      },
      notes: {
        // include notes if any
      },
      theme: {
        color: "#E74C3C",
      },
    };

    RazorpayCheckout.on("payment.success", async (response) => {
      const payconfirm = await this.http
        .post<any>(
          "https://app.smartdietplanner.com:8444/dietitian/payment/confirm",
          {
            razorpay_order_id: response.razorpay_order_id,
            razorpay_signature: response.razorpay_signature,
            razorpay_payment_id: response.razorpay_payment_id,
            txnId: val.txnId,
          }
        )
        .toPromise();
      console.log("Payment CConfirm:: ", payconfirm);
      alert(JSON.stringify(payconfirm));
    });
    RazorpayCheckout.on("payment.cancel", (error) => {
      alert("Payment Cancelled");
    });
    RazorpayCheckout.open(options);
  }

  payWithRazor(val) {
    console.log("val", val);
    // this.utilities.showLoading();
    const options: any = {
      key: val.keyId,
      amount: val.amount, // amount should be in paise format to display Rs 1255 without decimal point
      currency: "INR",
      name: "", // company name or product name
      description: "", // product description
      // image: './assets/icons/logoround.png', // company logo or product image
      order_id: val.orderId, // order_id created by you in backend
      modal: {
        // We should prevent closing of the form when esc key is pressed.
        escape: false,
      },
      notes: {
        // include notes if any
      },
      theme: {
        color: "#E74C3C",
      },
    };
    options.handler = (response, error) => {
      options.response = response;
      console.log("payment response", response);
      this.appService
        .paymentConfirm({
          razorpay_order_id: response.razorpay_order_id,
          razorpay_signature: response.razorpay_signature,
          razorpay_payment_id: response.razorpay_payment_id,
          txnId: val.txnId,
        })
        .then(
          (payconfirm) => {
            alert("235: Payment CConfirm");
          },
          (err) => {
            alert("Payment_05Failed");
            console.log("err", err);
          }
        );
      console.log(options);
    };
    options.modal.ondismiss = () => {
      alert("Tranasation Cancelled");
      console.log("Transaction cancelled.");
    };
    const target = "_blank";

    const rzp = new (window as any).Razorpay(options);
    rzp.open();
  }

  closePopup() {
    this.showBuyPopup = false;
  }

  goToWhatsapp() {
    {
      const phoneNumber = "+919999118595"; // Replace with the actual WhatsApp number
      const email = localStorage.getItem("loginEmail");
      const name = localStorage.getItem("dietitianName");
      // alert("email: "+email + " :: Name: " + name)
      const message = encodeURIComponent(
        name
          ? `I am ${name} with email id ${email} and I need support`
          : `I am ${email} and I need support`
      );
      const whatsappUrl = `https://wa.me/${phoneNumber}?text=${message}`;
      // const whatsappUrl = `https://wa.me/message/KGHV4SZOZ424J1`;
      window.open(whatsappUrl, "_blank");
    }
  }

  goToEmail() {
    const url =
      "mailto:dietitian@smartdietplanner.com?subject=New%20User&body=Hello%20Fitrofy";
    window.open(url, "_blank");
  }

  async payNow(amount) {
    // Create order by calling the server endpoint
    const order = await this.http
      .post<any>(
        "https://nodeapi.smartdietplanner.com/api/payment/createOrder",
        {
          // 'http://localhost:8080/api/payment/createOrder', {
          // const order = await this.http.post<any>('http://localhost:3000/create-order', {
          amount: amount,
          currency: "INR",
          receipt: "receipt#1",
          notes: {},
        }
      )
      .toPromise();

    // Open Razorpay Checkout
    const options: any = {
      // key: 'rzp_test_Y2wy8t1wD1AFaA', // test
      key: "rzp_live_kKh0sHoMajNYC0",
      amount: order.amount,
      currency: order.currency,
      name: "Fitrofy",
      description: "Test Transaction",
      order_id: order.id, // This is the order_id created in the backend
      prefill: {
        name: "Your Name",
        email: "your.email@example.com",
        contact: "9999999999",
      },
      theme: {
        color: "#F37254",
      },
      handler: (response: any) => {
        this.verifyPayment(response);
      },
    };

    const rzp = new (window as any).Razorpay(options);
    rzp.open();
  }

  async verifyPayment(paymentResponse: any) {
    try {
      const verification = await this.http
        .post<any>(
          "https://nodeapi.smartdietplanner.com/api/payment/verifyPayment",
          {
            // 'http://localhost:8080/api/payment/verifyPayment', {
            // const verification = await this.http.post<any>('http://localhost:3000/verify-payment', {
            razorpay_order_id: paymentResponse.razorpay_order_id,
            razorpay_payment_id: paymentResponse.razorpay_payment_id,
            razorpay_signature: paymentResponse.razorpay_signature,
          }
        )
        .toPromise();

      if (verification.status === "ok") {
        alert("Payment Successful222");
        // Redirect to a success page or show a success message
      } else {
        alert("Payment verification failed");
      }
    } catch (error) {
      console.error("Error:", error);
      alert("Error verifying payment");
    }
  }
}
