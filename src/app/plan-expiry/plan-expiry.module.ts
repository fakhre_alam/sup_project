import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { PlanExpiryPageRoutingModule } from "./plan-expiry-routing.module";

import { PlanExpiryPage } from "./plan-expiry.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlanExpiryPageRoutingModule,
    ReactiveFormsModule,
  ],
  declarations: [
    PlanExpiryPage,
 
  ],
})
export class PlanExpiryPageModule {}
