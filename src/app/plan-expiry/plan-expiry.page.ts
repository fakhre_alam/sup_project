import { Component, OnInit, ViewChild } from "@angular/core";
import { AppService, LoaderService, TemplateService } from "../services";
import { InAppBrowser } from "@ionic-native/in-app-browser/ngx";
import { APIS } from "../shared/constants/constants";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { IonDatetime } from "@ionic/angular";
import moment from "moment";
import { CONSTANTS } from "../core/constants/constants";
import { Router } from "@angular/router";

@Component({
  selector: "app-plan-expiry",
  templateUrl: "./plan-expiry.page.html",
  styleUrls: ["./plan-expiry.page.scss"],
})
export class PlanExpiryPage implements OnInit {
  customerId = "";
  accountIdRef = "";
  customerAmount = "";
  customerExpiryDate = "";
  refferalCode = "";
  current_year = moment().subtract(0, "months").format("YYYY-MM-DD");
  max_year: any = moment().add(3, "years").format("YYYY-MM-DD");
  public navigateEmailForm: FormGroup;
  public refferalEmailForm: FormGroup;
  public updateExpiryForm: FormGroup;
  public customerEmailForm: FormGroup;
  username = "";
  couponSelected = "";
  couponOptions: { label: string; value: string }[] = [
    { label: "Smart Plan", value: "sm" },
    { label: "Ultra Plan", value: "ultra" },
    { label: "Premium Plan", value: "Premium" },
  ];

  @ViewChild(IonDatetime, { static: true }) datetime: IonDatetime;

  refferalEmail = "";
  validation_messages = {
    email: [
      { type: "required", message: "Email Address is mandatory." },
      {
        type: "pattern",
        message: "Please enter valid Email Address",
      },
    ],
    customerId: [{ type: "required", message: "Enter customer id." }],
    accountIdRef: [{ type: "required", message: "Enter Account id." }],
    customerAmount: [{ type: "required", message: "Enter customer amount." }],
    customerDate: [{ type: "required", message: "Select expiry date." }],
    couponSelected: [{ type: "required", message: "Select Plan type." }],
  };
  dateValue = "";
  dateValue2 = "";
  navigateEmail = "";
  customerEmail = "";
  customerDietplanEmail = "";
  constructor(
    private appService: AppService,
    private iab: InAppBrowser,
    private loaderService: LoaderService,
    private formBuilder: FormBuilder,
    private router: Router,
    private templateService: TemplateService
  ) {
    this.username = this.getUserName();
    this.navigateEmailForm = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required])],
    });
    this.customerEmailForm = this.formBuilder.group({
      email: [null, Validators.compose([Validators.required])],
    });
    this.refferalEmailForm = this.formBuilder.group({
      accountIdRef: [""],
      email: [null],
      refferalCode: [""],
    });
    this.updateExpiryForm = this.formBuilder.group({
      customerId: ["", Validators.required],
      customerAmount: ["", Validators.required],
      customerDate: ["", Validators.required],
      couponSelected: ["", Validators.required],
    });
  }

  getUserName() {
    const email = localStorage.getItem("loginEmail");
    return email.toLowerCase().includes("beatoapp.com".toLowerCase())
      ? "beato"
      : email;
  }

  addupdateCust() {
    this.router.navigate(["addupdate"]);
  }
  CreateCustomDietPlan() {
    console.log(this.customerDietplanEmail);

    localStorage.setItem("email", this.customerDietplanEmail);
    this.router.navigate(["select-diet"]);
  }

  ngOnInit() {}
  formatDate(value: string) {
    return moment(value).format("DD-MMM-YYYY");
  }

  updateUser() {
    console.log("Customer Id:: ", this.customerId);
    console.log("Coupon Code", this.couponSelected);
    // alert(this.couponSelected);
    localStorage.setItem("get_user_details", "true");
    let obj = {
      amount: this.customerAmount,
      customerEmail: this.customerId,
      expiryDate: this.dateValue2,
      couponCode: this.couponSelected,
    };
    console.log("Object ::>> ", obj);
    this.loaderService.presentLoader("Please Wait").then((_) => {
      this.appService.updateExpDate(obj).subscribe(
        (response) => {
          this.loaderService.hideLoader();
          // alert("User Updated");
          this.updateExpiryForm.reset();
          if (localStorage.getItem("dietitianId") != null) {
            // dietitianId, actionId, userId,  actionName,  companyId
            this.templateService
              .handleDietitianAction(
                localStorage.getItem("dietitianId"),
                "668d73781073b512dd2b9658",
                localStorage.getItem("email"),
                "Update User Expiry Date",
                localStorage.getItem("companyId")
              )
              .subscribe((dRespondata) => {
                console.log(response);
              });
          }
        },
        (error) => {
          this.loaderService.hideLoader();
          console.log("Category Error", error);
        }
      );
    });
  }
  updateRefferalEmail() {
    if (!this.refferalEmail) {
      alert("Please Enter Customer ID");
    } else {
      let obj = {
        email: this.refferalEmail.includes("@")
          ? this.refferalEmail
          : "91-" + this.refferalEmail,
        accountId: this.accountIdRef,
        referralCode: this.refferalCode,
      };
      console.log("obj", obj);

      this.loaderService.presentLoader("Please Wait").then((_) => {
        this.appService.updatRefferal(obj).subscribe(
          (response) => {
            this.loaderService.hideLoader();
            console.log(response);
            alert("User Updated with Referral Code: " + response.referralCode);
            this.refferalEmail = "";
          },
          (error) => {
            this.loaderService.hideLoader();
            console.log("Category Error", error);
          }
        );
      });
    }
  }

  goToClientApp(token) {
    let link = APIS.mainUrl + "?token=" + token;
    console.log("link", link);

    // let link = "http://localhost:8101/token="+token;
    this.iab.create(link, "_system", "location=yes");
  }

  updateCustomer() {
    if (!this.customerEmail) {
      alert("Please Enter Customer ID");
    } else {
      CONSTANTS.email = this.customerEmail;
      localStorage.setItem("email", this.customerEmail);
      localStorage.setItem("userid", this.customerEmail);
      if (this.username !== "beato") {
        this.router.navigate(["/tabs/consume"]);
      } else {
        this.router.navigate(["/consume"]);
      }
    }
  }
  getToken() {
    if (!this.navigateEmail) {
      alert("Please Enter Customer ID");
    } else {
      this.appService.getUserToken(this.navigateEmail).subscribe(
        (response) => {
          console.log(response);
          // if (response) {
          //   this.storageService.set('refs',response);
          // }
        },
        (error) => {
          // this.loaderService.hideLoader();
          console.log("User token ", error.error.text);
          this.goToClientApp(error.error.text);
          console.log("Category Error", error);
        }
      );
    }
  }
}
