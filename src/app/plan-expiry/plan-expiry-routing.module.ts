import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { PlanExpiryPage } from "./plan-expiry.page";

const routes: Routes = [
  {
    path: "",
    component: PlanExpiryPage,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlanExpiryPageRoutingModule {}
