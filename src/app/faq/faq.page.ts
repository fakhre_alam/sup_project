import { Component, OnInit } from '@angular/core';
import faq from "../shared/constants/faq.json";
@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {
  faqs = [];
  constructor() { 
    debugger
    this.faqs=faq;
  }

  ngOnInit() {
  }
  toggleFAQ(index: number) {
    this.faqs[index].isOpen = !this.faqs[index].isOpen;
  }
}
