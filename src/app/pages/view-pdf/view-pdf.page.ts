import { Component, OnDestroy, OnInit } from '@angular/core';
import { AppService } from "src/app/app.service";
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import moment from "moment";
import { AppService as apS, TemplateService } from "../../services";
import { ModalController, PopoverController } from "@ionic/angular";
import { ActivatedRoute, Router } from "@angular/router";
import { UTILITIES } from "src/app/core/utility/utilities";
@Component({
  selector: 'app-view-pdf',
  templateUrl: './view-pdf.page.html',
  styleUrls: ['./view-pdf.page.scss'],
})
export class ViewPdfPage implements OnInit, OnDestroy {
  sanitizedPdfUrl: SafeResourceUrl | null = null;
  pdfLog=[];
  constructor(  
    private appService: AppService,
    private appServe: AppService,
    private utilities: UTILITIES,
    private sanitizer: DomSanitizer) { }

  ngOnInit() {
    this.getListPdf();
  }
  pdfUrl="";
  // blobData;
  getPdf(){
    const dietitianId="9739849rnvj9ei094"; //localStorage.getItem("dietitianId");
    debugger;
    this.appServe.getPdf(dietitianId).then((blob) => {
      // this.blobData = blob;
      const url = URL.createObjectURL(blob); // Convert Blob to object URL
      console.log("url", url);
      this.pdfUrl = url;
      this.sanitizedPdfUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
    },err=>{
      console.log("PDF error:--",err.error.text);
  });
  }
  openPDF(url){
    this.sanitizedPdfUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
   
  }
  onFileSelected(event: any) {
    this.selectedFile = event.target.files[0];
  }
  selectedFile: File | null = null;
  uploadPdf(){
    const formData = new FormData();
    formData.append('userId', localStorage.getItem("email"));

    if (this.selectedFile) {
      formData.append('pdfFile',this.selectedFile);
    }
    this.appServe.uploadPdf(formData).then((res:any)=>{
      console.log("PDF:--",res);   
      this.utilities.presentAlert("File Uploaded successfully."); 
      this.getListPdf();

  },err=>{
    console.log("PDF error:--",err);
  });
  }
  customChange(name){
     return name.replace(' ()','');
  }
  deletePDF(_id){
    this.appServe.deletePdf(_id).then((res:any)=>{
      this.utilities.presentAlert("File Deleted successfully."); 
      this.getListPdf();

  },err=>{
    console.log("PDF error:--",err);
  });
  }
    formatDate(dateData) {
      return moment(dateData).format("DD-MMM-yyyy");
    }
  getListPdf(){
    const userId = localStorage.getItem("email");
    console.log("userId", userId);
    
    this.appServe.getListPdf(userId).then((res:any)=>{
      console.log("PDF:--",res);   
      this.pdfLog = res["records"];    
  },err=>{
    console.log("PDF error:--",err);
  });
  }
  ngOnDestroy() {
    // Clean up object URL to prevent memory leaks
    if (this.pdfUrl) {
      URL.revokeObjectURL(this.pdfUrl);
    }
  }
}
