import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { AppService } from "src/app/app.service";
import moment from "moment";
import { AppService as apS, LoaderService } from "../../services";
import { PopoverController } from "@ionic/angular";
import { UTILITIES } from "src/app/core/utility/utilities";

@Component({
  selector: "app-manage-diet-choice",
  templateUrl: "./manage-diet-choice.page.html",
  styleUrls: ["./manage-diet-choice.page.scss"],
})
export class ManageDietChoicePage implements OnInit {
  dietPlan: any=[];
  dietPlanPayload: any;
  calories: any = [];
  constructor(
    private appService: AppService,
    private aps: apS,
    private cdr: ChangeDetectorRef,
    private popoverController: PopoverController,
    private utilities:UTILITIES
  ) {}

  ngOnInit() {
    this.getDietPlan();
  }

  getDayWithSuffix(date: Date) {
    const day = date.getDate();
    if (day === 1 || day === 21 || day === 31) {
      return day + "st";
    } else if (day === 2 || day === 22) {
      return day + "nd";
    } else if (day === 3 || day === 23) {
      return day + "rd";
    } else {
      return day + "th";
    }
  }
  defaultStructureOfJson() {
    for (let index = 0; index < 7; index++) {
      const minusDate =7-index;
      this.dietPlan.push({
         "date": moment(new Date(new Date().setDate(new Date().getDate() - minusDate))).format("DDMMYYYY"),
          "totalCal": 0,
          "recomended": 0,
          "tolalCalories": 0,
          "totalCalories": 0,
          "totalCaloriesPer": 0,
          "totalCarbs": 0,
          "totalCarbsPer": 0,
          "totalFat": 0,
          "totalFatPer": 0,
          "totalProtien": 0,
          "totalProtienPer": 0,
          "totalFiber": 0,
          "totalFiberPer": 0,
          "calDistribution": 0,
          "calDistributionPer": 0,
          "diets": []
        });
      for (let j = 0; j < 1; j++) {
        this.dietPlan[index].diets.push(
          {
            "time": "06:30",
            "message": "When You Wake up",
            "slot": 0,
            "data": [{}],
            "totalCalories": 0,
            "totalCarbs": 0,
            "totalFat": 0,
            "totalProtien": 0,
            "totalFiber": 0
        },
        {
            "time": "7:30",
            "message": "Before Breakfast",
            "slot": 1,
            "data": [{}],
            "totalCalories": 0,
            "totalCarbs": 0,
            "totalFat": 0,
            "totalProtien": 0,
            "totalFiber": 0
        },
        {
            "time": "10:00",
            "message": "Breakfast",
            "slot": 2,
            "data": [{}],
            "totalCalories": 0,
            "totalCarbs": 0,
            "totalFat": 0,
            "totalProtien": 0,
            "totalFiber": 0
        },
        {
            "time": "11:30",
            "message": "Mid Day Meal",
            "slot": 3,
            "data": [{}],
            "totalCalories": 0,
            "totalCarbs": 0,
            "totalFat": 0,
            "totalProtien": 0,
            "totalFiber": 0
        },
        {
            "time": "13:30",
            "message": "Lunch",
            "slot": 4,
            "data": [{}],
            "totalCalories": 0,
            "totalCarbs": 0,
            "totalFat": 0,
            "totalProtien": 0,
            "totalFiber": 0
        },
        {
            "time": "15:00",
            "message": "Post Lunch",
            "slot": 5,
            "data": [{}],
            "totalCalories": 0,
            "totalCarbs": 0,
            "totalFat": 0,
            "totalProtien": 0,
            "totalFiber": 0
        },
        {
            "time": "17:00",
            "message": "Evening Snack",
            "slot": 6,
            "data": [
              {
               
              }
          ],
          "totalCalories": 0,
          "totalCarbs": 0,
          "totalFat": 0,
          "totalProtien": 0,
          "totalFiber": 0
        },
        {
            "time": "19:30",
            "message": "Dinner",
            "slot": 7,
            "data": [
              {
               
              }
          ],
          "totalCalories": 0,
          "totalCarbs": 0,
          "totalFat": 0,
          "totalProtien": 0,
          "totalFiber": 0
        },
        {
            "time": "20:30",
            "message": "Before Sleep",
            "slot": 8,
            "data": [
                {
                 
                }
            ],
            "totalCalories": 0,
            "totalCarbs": 0,
            "totalFat": 0,
            "totalProtien": 0,
            "totalFiber": 0
        }   
        );
       
      }
    }
  }
  suffuleArray(){
    this.dietPlan.filter((item:any,index:number)=>{
       const day = moment(new Date()).format("dddd");
        if(item.date1.toLowerCase() === day.toLowerCase()){
          if(index!==0){
          const element = this.dietPlan.splice(0, index);
          this.dietPlan.push(...element);
          console.log("element",element,this.dietPlan);
          }
        }
    });
  }

  returnDay(date){
return moment(date).format("ddd");
  }
  getDietPlan() {

    const userId = localStorage.getItem("email");
    this.appService.getDietRecall(userId).then(
      (res:any) => {
        console.log("res", res);
        if (res=== null || res.data === null) {
          this.defaultStructureOfJson();
          this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
        } else {
          this.dietPlan = res.data;
          this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
          console.log(this.dietPlan);
          }
          this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
          console.log(this.dietPlan);
          for (let index = 0; index < this.dietPlan.length; index++) {
             const formatedDate = new Date(
              this.dietPlan[index].date.slice(4),
              this.dietPlan[index].date.slice(2, 4)-1,
              this.dietPlan[index].date.slice(0, 2)
            );

            this.dietPlan[index].date1 = this.getDayWithSuffix(formatedDate) + "-" + moment(formatedDate).format("MMM-YYYY") ;
            this.dietPlan[index].date2=formatedDate;
            this.prepareData(this.dietPlan[index].diets);
            
          }
          const maxLength0 = Math.max(
            ...this.preparedData0.map((obj) => obj.length)
          );
          this.preparedData0.forEach((obj) => {
            while (obj.length < maxLength0) {
              obj.push({}); // Add empty objects until it reaches the maximum length
            }
          });
          const maxLength1 = Math.max(
            ...this.preparedData1.map((obj) => obj.length)
          );
          this.preparedData1.forEach((obj) => {
            while (obj.length < maxLength1) {
              obj.push({}); // Add empty objects until it reaches the maximum length
            }
          });
          const maxLength2 = Math.max(
            ...this.preparedData2.map((obj) => obj.length)
          );
          this.preparedData2.forEach((obj) => {
            while (obj.length < maxLength2) {
              obj.push({}); // Add empty objects until it reaches the maximum length
            }
          });
          const maxLength3 = Math.max(
            ...this.preparedData3.map((obj) => obj.length)
          );
          this.preparedData3.forEach((obj) => {
            while (obj.length < maxLength3) {
              obj.push({}); // Add empty objects until it reaches the maximum length
            }
          });
          const maxLength4 = Math.max(
            ...this.preparedData4.map((obj) => obj.length)
          );
          this.preparedData4.forEach((obj) => {
            while (obj.length < maxLength4) {
              obj.push({}); // Add empty objects until it reaches the maximum length
            }
          });
          const maxLength5 = Math.max(
            ...this.preparedData5.map((obj) => obj.length)
          );
          this.preparedData5.forEach((obj) => {
            while (obj.length < maxLength5) {
              obj.push({}); // Add empty objects until it reaches the maximum length
            }
          });
          const maxLength6 = Math.max(
            ...this.preparedData6.map((obj) => obj.length)
          );
          this.preparedData6.forEach((obj) => {
            while (obj.length < maxLength6) {
              obj.push({}); // Add empty objects until it reaches the maximum length
            }
          });
          const maxLength7 = Math.max(
            ...this.preparedData7.map((obj) => obj.length)
          );
          this.preparedData7.forEach((obj) => {
            while (obj.length < maxLength7) {
              obj.push({}); // Add empty objects until it reaches the maximum length
            }
          });
          const maxLength8 = Math.max(
            ...this.preparedData8.map((obj) => obj.length)
          );
          this.preparedData8.forEach((obj) => {
            while (obj.length < maxLength8) {
              obj.push({}); // Add empty objects until it reaches the maximum length
            }
          });
          
          for (
            let index = 0;
            index < this.dietPlan[0].diets.length;
            index++
          ) {
            this.slotItems.push({
              slotName: this.dietPlan[0].diets[index].message,
              data:
                index == 0
                  ? this.preparedData0
                  : index == 1
                  ? this.preparedData1
                  : index == 2
                  ? this.preparedData2
                  : index == 3
                  ? this.preparedData3
                  : index == 4
                  ? this.preparedData4
                  : index == 5
                  ? this.preparedData5
                  : index == 6
                  ? this.preparedData6
                  : index == 7
                  ? this.preparedData7
                  : index == 8
                  ? this.preparedData8
                  : [],
            });
          }
        },
        (error) => {
          console.log("Error:-", error);
        }
      );
  }

  saveItem() {
    this.calculateCal(this.dietPlan);
    this.popoverController.dismiss();
  }
  calculateCal(sevenDaysDiet: any) {
    let totalCal = 0;
    let totalCarbs = 0;
    let totalFat = 0;
    let totalProtein = 0;
    let totalFiber = 0;
    for (let index = 0; index < sevenDaysDiet.length; index++) {
      for (let j = 0; j < sevenDaysDiet[index].diets.length; j++) {
        for (let k = 0; k < sevenDaysDiet[index].diets[j].data.length; k++) {
          console.log(
            "sevenDaysDiet[index].diets[j].data[k]",
            sevenDaysDiet[index].diets[j].data[k]
          );
          totalCal =
            totalCal +
            Number(
              sevenDaysDiet[index].diets[j].data[k]?.Calories === undefined
                ? 0
                : sevenDaysDiet[index].diets[j].data[k]?.Calories
            ) *
              (sevenDaysDiet[index].diets[j].data[k]?.portion === undefined
                ? 0
                : sevenDaysDiet[index].diets[j].data[k]?.portion);
          totalCarbs =
            totalCarbs +
            Number(
              sevenDaysDiet[index].diets[j].data[k]?.Carbs === undefined
                ? 0
                : sevenDaysDiet[index].diets[j].data[k]?.Carbs
            ) *
              (sevenDaysDiet[index].diets[j].data[k]?.portion === undefined
                ? 0
                : sevenDaysDiet[index].diets[j].data[k]?.portion);
          totalFat =
            totalFat +
            Number(
              sevenDaysDiet[index].diets[j].data[k]?.Fat === undefined
                ? 0
                : sevenDaysDiet[index].diets[j].data[k]?.Fat
            ) *
              (sevenDaysDiet[index].diets[j].data[k]?.portion === undefined
                ? 0
                : sevenDaysDiet[index].diets[j].data[k]?.portion);
          totalProtein =
            totalProtein +
            Number(
              sevenDaysDiet[index].diets[j].data[k]?.Protien === undefined
                ? 0
                : sevenDaysDiet[index].diets[j].data[k]?.Protien
            ) *
              (sevenDaysDiet[index].diets[j].data[k]?.portion === undefined
                ? 0
                : sevenDaysDiet[index].diets[j].data[k]?.portion);
          totalFiber =
            totalFiber +
            Number(
              sevenDaysDiet[index].diets[j].data[k]?.Fiber === undefined
                ? 0
                : sevenDaysDiet[index].diets[j].data[k]?.Fiber
            ) *
              (sevenDaysDiet[index].diets[j].data[k]?.portion === undefined
                ? 0
                : sevenDaysDiet[index].diets[j].data[k]?.portion);
        }
      }
      this.dietPlan[index].tolalCalories = totalCal.toFixed(1);
      this.dietPlan[index].totalCarbs = totalCarbs.toFixed(1);
      this.dietPlan[index].totalFat = totalFat.toFixed(1);
      this.dietPlan[index].totalProtien = totalProtein.toFixed(1);
      this.dietPlan[index].tolalFiber = totalFiber.toFixed(1);

      console.log(totalCal);
      console.log(totalCarbs);
      console.log(totalFat);
      console.log(totalProtein);
      console.log(totalFiber);
      totalCal = 0;
      totalCarbs = 0;
      totalFat = 0;
      totalProtein = 0;
      totalFiber = 0;
    }
    console.log("", this.dietPlan);
  }
  preparedData0: any = [];
  preparedData1: any = [];
  preparedData2: any = [];
  preparedData3: any = [];
  preparedData4: any = [];
  preparedData5: any = [];
  preparedData6: any = [];
  preparedData7: any = [];
  preparedData8: any = [];
  slotItems: any = [];
  prepareData(diet: any) {
    for (let index = 0; index < diet.length; index++) {
      if (diet[index].slot === 0) {
        this.preparedData0.push(diet[index].data);
      } else if (diet[index].slot === 1) {
        this.preparedData1.push(diet[index].data);
      } else if (diet[index].slot === 2) {
        this.preparedData2.push(diet[index].data);
      } else if (diet[index].slot === 3) {
        this.preparedData3.push(diet[index].data);
      } else if (diet[index].slot === 4) {
        this.preparedData4.push(diet[index].data);
      } else if (diet[index].slot === 5) {
        this.preparedData5.push(diet[index].data);
      } else if (diet[index].slot === 6) {
        this.preparedData6.push(diet[index].data);
      } else if (diet[index].slot === 7) {
        this.preparedData7.push(diet[index].data);
      } else if (diet[index].slot === 8) {
        this.preparedData8.push(diet[index].data);
      }
    }
  }
  searchfit = false;
  searchingData = false;
  copyData;
  totalDataSearchedData: any = [];
  getAllForSearch(searchTerm, ind, indp, indParent) {
    if (searchTerm === "") {
      return;
    }
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        this.isShow[ind] = true;
        this.isShowParent[indp] = true;
        this.isShowSuperParent[indParent] = true;
        this.appService
          .searchFoodItemByName(searchTerm, error.error.text)
          .then((resp) => {
            this.totalDataSearchedData = resp["homeBased"];
          });
      }
    );
  }
  isShow = new Array(10000).fill(false);
  isShowParent = new Array(9).fill(false);
  isShowSuperParent = new Array(7).fill(false);
  fillFood(items, ind, searchItem, indp, indParent) {
    items[ind] = searchItem;
    this.isShow[ind] = false;
    this.isShowParent[indp] = false;
    this.isShowSuperParent[indParent] = false;
    this.calculateCal(this.dietPlan);
    this.popoverController.dismiss();
  }

  addItem(slotItem) {
    for (let index = 0; index < slotItem.data.length; index++) {
      slotItem.data[index].push({});
    }

    this.cdr.detectChanges();
  }
  removeItem(slotData, indp) {
    for (let index = 0; index < slotData.length; index++) {
      slotData[index].splice(indp, 1);
    }
    this.calculateCal(this.dietPlan);
  }
  removeAllFromSlot(slotItem) {
    // debugger;
    for (let index = 0; index < slotItem.data.length; index++) {
      slotItem.data[index] = [];
    }
    this.calculateCal(this.dietPlan);
  }
  searchQuery: string = "";
  isSearchCleared: boolean = false;
  onClear(event: any) {
    if (this.isSearchCleared) {
      event.target.value = this.searchQuery; // Restore the previous search query
      this.isSearchCleared = false; // Reset the flag
    } else {
      this.searchQuery = event.target.value; // Update the search query
    }
  }
  copyItems(slot, indp, ind, itemIndex) {
    for (let index = 0; index < slot.data.length; index++) {
      if (index !== indp) {
        const data = slot.data[index].filter((item) => {
          return item._id === slot.data[indp][ind]._id;
        });
        if (data.length === 0) {
          slot.data[index][ind] = slot.data[indp][ind];
        }
      }
    }
    this.calculateCal(this.dietPlan);
  }
  removeItemFromPopup(items, mainIndex, indp, ind) {
    items.splice(ind, 1);
    items.push({});
    this.calculateCal(this.dietPlan);
    this.popoverController.dismiss();
  }
  payload: any = { meal_plans: [] };
  cancel(){
    
  }
  save() {
    const userId = localStorage.getItem('email');
      this.appService.saveDietRecall(userId,{data:this.dietPlan}).then(res=>{
        console.log("test", res);
        this.utilities.presentAlert("Record saved successfully!");
      },err=>{
        console.log("err", err);     
      })
    console.log("payload", this.payload);
  }
}
