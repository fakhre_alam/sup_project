import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { ManageDietChoicePageRoutingModule } from "./manage-diet-choice-routing.module";

import { ManageDietChoicePage } from "./manage-diet-choice.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManageDietChoicePageRoutingModule,
  ],
  declarations: [
    ManageDietChoicePage,
 
  ],
})
export class ManageDietChoicePageModule {}
