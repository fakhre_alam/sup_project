import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { ManageDietPageRoutingModule } from './manage-diet-routing.module';
import { ManageDietPage } from './manage-diet.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ManageDietPageRoutingModule
  ],
  declarations: [ManageDietPage,


  ]
})
export class ManageDietPageModule {}
