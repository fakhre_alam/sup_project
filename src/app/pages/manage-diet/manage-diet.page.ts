import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnInit,
  Output,
} from "@angular/core";
import { AppService } from "src/app/app.service";

import moment from "moment";
import { AppService as apS, TemplateService } from "../../services";
import { AlertController, ModalController, PopoverController } from "@ionic/angular";
import { ActivatedRoute, Router } from "@angular/router";
import { UTILITIES } from "src/app/core/utility/utilities";
import { TemplatePopupPage } from "../../template-popup/template-popup.page";

@Component({
  selector: "app-manage-diet",
  templateUrl: "./manage-diet.page.html",
  styleUrls: ["./manage-diet.page.scss"],
})
export class ManageDietPage implements OnInit {
  dietPlan: any = [];
  dietPlanPayload: any;
  calories: any = [];
  templateId: any = "";
  isRepeat = "false";
  samedietmessage = "Repeat the 7-day plan for future weeks.";
  isEdit = "false";
  recall = "false";
  newTemplate: any = "false";
  inSights = "false";
  isGlobal: boolean = false;
  isIndividual = localStorage.getItem("isIndividual");
  @Output() searchEvent = new EventEmitter<string>();
  isCaloryExceed = new Array(7).fill(false);
  userSearched = "";
  emplyDataSlot = {
    totalCal: 0,
    recomended: 0,
    tolalCalories: 0,
    totalCalories: 0,
    totalCaloriesPer: 0,
    totalCarbs: 0,
    totalCarbsPer: 0,
    totalFat: 0,
    totalFatPer: 0,
    totalProtien: 0,
    totalProtienPer: 0,
    totalFiber: 0,
    totalFiberPer: 0,
    calDistribution: 0,
    calDistributionPer: 0,
    date: "",
    diets: [
      { slot: 0, message: "When You Wake up", data: [{}] },
      { slot: 1, message: "Before Breakfast", data: [{}] },
      { slot: 2, message: "Breakfast", data: [{}] },
      { slot: 3, message: "Mid Day Meal", data: [{}] },
      { slot: 4, message: "Lunch", data: [{}] },
      { slot: 5, message: "Post Lunch", data: [{}] },
      { slot: 6, message: "Evening Snack", data: [{}] },
      { slot: 7, message: "Dinner", data: [{}] },
      { slot: 8, message: "Before Sleep", data: [{}] },
    ],
  };
  startDate: any;
  endDate: any;
  constructor(
    private appService: AppService,
    private aps: apS,
    private cdr: ChangeDetectorRef,
    private popoverController: PopoverController,
    private queryParams: ActivatedRoute,
    private router: Router,
    private utilities: UTILITIES,
    private templateService: TemplateService,
    public modalController: ModalController,
    private alertController: AlertController
  ) {
    this.dietPlan = [];
    this.startDate = new Date().setDate(new Date().getDate());
    this.queryParams.queryParams.subscribe((res) => {
      console.log("xyz:-", res["recall"], res["newTemplate"]);

      this.recall = res["recall"] !== undefined ? res["recall"] : "false";
      this.newTemplate =
        res["newTemplate"] !== undefined ? res["newTemplate"] : "false";
      this.templateId = res["params"];
      this.isEdit = res["edit"];
      if (res["insights"]) {
        this.inSights = res["insights"];
      }
      console.log("this.templateId", this.templateId, this.isEdit, res);
    });
    this.userSearched = localStorage.getItem("email");
  }
  getObjectSizeInBytes(obj) {
    // Serialize the object to a JSON string
    const jsonString = JSON.stringify(obj);
    // Calculate the byte size of the string
    return new Blob([jsonString]).size;
}
  deepEqual(obj1, obj2) {
    const size1 = this.getObjectSizeInBytes(obj1);
    const size2 = this.getObjectSizeInBytes(obj2);
    return size1 === size2;
  }
  profileData: any;
  getProfile() {
    this.profileData = [];
    this.appService.getProfile().then(
      (res) => {
        this.profileData = res;
      },
      (err) => {
        console.log("profile Error:-", err);
      }
    );
  }
  async copyFromTemplate() {
    const modal = await this.modalController.create({
      component: TemplatePopupPage,
      cssClass: "template-popup-css", // Optional: custom CSS class for the modal
    });

    await modal.present();

    // Handle data returned from the modal
    const { data } = await modal.onWillDismiss();
    console.log("Modal Data:", data);
  }

  current_year = new Date().getFullYear();
  copyPreviousWeek() {
    const dateValue = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate() - 7)
    );
    const dateValue2 = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate())
    );
    this.appService
      .copyDietPlan(
        this.userSearched,
        moment(dateValue).format("DDMMYYYY"),
        moment(dateValue2).format("DDMMYYYY")
      )
      .then(
        (res) => {
          console.log("res", res);
          this.utilities.presentAlert("Diet Plan Copied successfully!");
          this.getDietPlanBasedOnDate();
        },
        (err) => {
          console.log("error", err);
        }
      );
  }

  formatDate(value: string) {
    return moment(value).format("DD-MMM-YYYY");
  }
  getTemplateForCopy() {
    this.appService.getTemplateById(this.templateId).then(
      (res: any) => {
        console.log("success teplate:-", res);
        this.template.templateDesc = res.templateDescription;
        this.template.templateName = res.templateName;
        this.dietPlan = JSON.parse(
          JSON.stringify(res.dietPlanData?.meal_plans)
        );
        this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
        this.suffuleArrayforCopy();
        this.bindTemplateDataCopy();
        this.calculateCal(this.dietPlan);
        this.isShowTemplate = false;
      },
      (err) => {
        console.log("Error", err);
        this.isShowTemplate = false;
      }
    );
  }
  handleSearchEvent(event) {
    this.router.navigate([this.router.url]).then(() => {
      window.location.reload();
    });
  }
  ngOnInit() {
    this.getProfile();
    this.bindData();
    this.getRepeatFutureFromService();
  }

  bindData() {
    if (this.recall === "true") {
      this.getRecallPlan();
    } else if (this.newTemplate === "true") {
      this.getnewTemplat();
    } else if (this.inSights === "true") {
      this.bindInSites();
    } else {
      if (
        this.templateId !== "" &&
        this.templateId !== undefined &&
        this.isEdit === "true"
      ) {
        this.getTemplateById();
      } else if (
        this.templateId !== "" &&
        this.templateId !== undefined &&
        this.isEdit === "false"
      ) {
        this.getTemplateForCopy();
      } else {
        this.getDietPlan();
      }
    }
  }
  backCounter = 1;
  backCounterForward = 0;
  dateRangeDis = "";
  dateRangeSevenDays = "";
  backCounterSeven = 1;
  backSeven1() {
    const defaultDate = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate() - 1)
    );
    this.startDate = new Date(defaultDate);
    if (this.startDate < new Date()) {
      this.isPastDate = true;
    } else {
      this.isPastDate = false;
    }
    const defaultDate1 = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate() + 6)
    );
    this.dateRangeSevenDays =
      moment(
        new Date(this.startDate).setDate(new Date(this.startDate).getDate())
      ).format("MMM DD") +
      "-" +
      moment(defaultDate1).format("DD");
    this.getDietPlanSeven(1);
  }
  backSeven() {
    const defaultDate = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate() - 7)
    );
    this.startDate = new Date(defaultDate);
    if (this.startDate < new Date()) {
      this.isPastDate = true;
    } else {
      this.isPastDate = false;
    }
    const defaultDate1 = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate() + 6)
    );
    this.dateRangeSevenDays =
      moment(
        new Date(this.startDate).setDate(new Date(this.startDate).getDate())
      ).format("MMM DD") +
      "-" +
      moment(defaultDate1).format("DD");
    this.getDietPlanSeven();
  }
  back() {
    this.backCounter = this.backCounter + 1;
    this.dietPlan = [];
    this.bindInSites();
    const defaultDate = new Date(
      new Date().setDate(new Date().getDate() - this.backCounter * 7)
    );
    const defaultDate1 = new Date(defaultDate).setDate(
      new Date(defaultDate).getDate() + 6
    );
    console.log("defaultDate1", defaultDate1);
    this.dateRangeDis =
      moment(defaultDate).format("MMM DD") +
      "-" +
      moment(defaultDate1).format("DD");
  }
  tempcountSeven = 0;
  forwardSeven1() {
    const defaultDate = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate() + 1)
    );
    this.startDate = new Date(defaultDate);
    if (this.startDate < new Date()) {
      this.isPastDate = true;
    } else {
      this.isPastDate = false;
    }
    const defaultDate1 = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate() + 6)
    );
    this.dateRangeSevenDays =
      moment(
        new Date(this.startDate).setDate(new Date(this.startDate).getDate())
      ).format("MMM DD") +
      "-" +
      moment(defaultDate1).format("DD");
    this.getDietPlanForward(1);
  }
  forwardSeven() {
    const defaultDate = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate() + 7)
    );
    this.startDate = new Date(defaultDate);
    if (this.startDate < new Date()) {
      this.isPastDate = true;
    } else {
      this.isPastDate = false;
    }
    const defaultDate1 = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate() + 6)
    );
    this.dateRangeSevenDays =
      moment(
        new Date(this.startDate).setDate(new Date(this.startDate).getDate())
      ).format("MMM DD") +
      "-" +
      moment(defaultDate1).format("DD");
    this.getDietPlanForward();
  }
  tempcount = 0;
  forward() {
    this.backCounter = this.backCounter - 1;

    if (this.backCounter === 0) {
      this.backCounter = 1;
      return;
    }
    this.dietPlan = [];
    const defaultDate3 = new Date(
      new Date().setDate(new Date().getDate() - this.backCounter * 7)
    );
    const defaultDate2 = new Date(defaultDate3).setDate(
      new Date(defaultDate3).getDate() + 6
    );
    if (defaultDate3.getTime() > new Date().getTime()) {
      if (this.tempcount === 0) {
        this.backCounter = this.backCounter + 1;
        this.tempcount = this.tempcount + 1;
      }
      return;
    }
    console.log("defaultDate2", defaultDate2);
    this.dateRangeDis =
      moment(defaultDate3).format("MMM DD") +
      "-" +
      moment(defaultDate2).format("DD");
    this.bindInSitesForward();
  }
  bindInSitesForward() {
    const defaultDate = new Date(
      new Date().setDate(new Date().getDate() - this.backCounter * 7)
    );
    const date = moment(defaultDate).format("DDMMYYYY");
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        const token = error.error.text;
        this.appService.getDiet7daysPlan(date, token).then(
          (success: any) => {
            const filteredData = success.filter((item) => {
              return item?.code === undefined;
            });
            this.dietPlan = filteredData;
            this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
            const nextDate = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 0)
            ).format("DDMMYYYY");
            const nextDate1 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 1)
            ).format("DDMMYYYY");
            const nextDate2 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 2)
            ).format("DDMMYYYY");
            const nextDate3 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 3)
            ).format("DDMMYYYY");
            const nextDate4 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 4)
            ).format("DDMMYYYY");
            const nextDate5 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 5)
            ).format("DDMMYYYY");
            const nextDate6 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 6)
            ).format("DDMMYYYY");

            if (this.dietPlan[0]?.date !== nextDate) {
              this.dietPlan.splice(
                0,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[0].date = nextDate;
              this.dietPlan[0].date1 = nextDate;
            }
            if (this.dietPlan[1]?.date !== nextDate1) {
              this.dietPlan.splice(
                1,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[1].date = nextDate1;
              this.dietPlan[1].date1 = nextDate1;
            }
            if (this.dietPlan[2]?.date !== nextDate2) {
              this.dietPlan.splice(
                2,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[2].date = nextDate2;
              this.dietPlan[2].date1 = nextDate2;
            }
            if (this.dietPlan[3]?.date !== nextDate3) {
              this.dietPlan.splice(
                3,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[3].date = nextDate3;
              this.dietPlan[3].date1 = nextDate3;
            }
            if (this.dietPlan[4]?.date !== nextDate4) {
              this.dietPlan.splice(
                4,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[4].date = nextDate4;
              this.dietPlan[4].date1 = nextDate4;
            }
            if (this.dietPlan[5]?.date !== nextDate5) {
              this.dietPlan.splice(
                5,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[5].date = nextDate5;
              this.dietPlan[5].date1 = nextDate5;
            }
            if (this.dietPlan[6]?.date !== nextDate6) {
              this.dietPlan.splice(
                6,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[6].date = nextDate6;
              this.dietPlan[6].date1 = nextDate6;
            }
            // bindmethod

            this.bindDietPlanModificationInsights();
            this.calculateCal(this.dietPlan);
            this.disablePastDateDiet();
          },
          (error) => {
            console.log("Error:-", error);
          }
        );
      }
    );
  }
  bindInSites() {
    const defaultDate2 = new Date(
      new Date().setDate(new Date().getDate() - this.backCounter)
    );
    const defaultDate3 = new Date(
      new Date().setDate(new Date().getDate() - this.backCounter * 7)
    );
    this.dateRangeDis =
      moment(defaultDate3).format("MMMDD") +
      " - " +
      moment(defaultDate2).format("DD");
    const defaultDate = new Date(
      new Date().setDate(new Date().getDate() - this.backCounter * 7)
    );
    const date = moment(defaultDate).format("DDMMYYYY");
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        const token = error.error.text;
        this.appService.getDiet7daysPlan(date, token).then(
          (success: any) => {
            const filteredData = success.filter((item) => {
              return item?.code === undefined;
            });
            this.dietPlan = filteredData;
            const nextDate = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 0)
            ).format("DDMMYYYY");
            const nextDate1 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 1)
            ).format("DDMMYYYY");
            const nextDate2 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 2)
            ).format("DDMMYYYY");
            const nextDate3 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 3)
            ).format("DDMMYYYY");
            const nextDate4 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 4)
            ).format("DDMMYYYY");
            const nextDate5 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 5)
            ).format("DDMMYYYY");
            const nextDate6 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 6)
            ).format("DDMMYYYY");

            if (this.dietPlan[0]?.date !== nextDate) {
              this.dietPlan.splice(
                0,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[0].date = nextDate;
              this.dietPlan[0].date1 = nextDate;
            }
            if (this.dietPlan[1]?.date !== nextDate1) {
              this.dietPlan.splice(
                1,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[1].date = nextDate1;
              this.dietPlan[1].date1 = nextDate1;
            }
            if (this.dietPlan[2]?.date !== nextDate2) {
              this.dietPlan.splice(
                2,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[2].date = nextDate2;
              this.dietPlan[2].date1 = nextDate2;
            }
            if (this.dietPlan[3]?.date !== nextDate3) {
              this.dietPlan.splice(
                3,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[3].date = nextDate3;
              this.dietPlan[3].date1 = nextDate3;
            }
            if (this.dietPlan[4]?.date !== nextDate4) {
              this.dietPlan.splice(
                4,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[4].date = nextDate4;
              this.dietPlan[4].date1 = nextDate4;
            }
            if (this.dietPlan[5]?.date !== nextDate5) {
              this.dietPlan.splice(
                5,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[5].date = nextDate5;
              this.dietPlan[5].date1 = nextDate5;
            }
            if (this.dietPlan[6]?.date !== nextDate6) {
              this.dietPlan.splice(
                6,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[6].date = nextDate6;
              this.dietPlan[6].date1 = nextDate6;
            }
            this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
            // bindmethod
            this.bindDietPlanModificationInsights();
            this.calculateCal(this.dietPlan);
            this.disablePastDateDiet();
          },
          (error) => {
            console.log("Error:-", error);
          }
        );
      }
    );
  }
  getTemplateById() {
    this.appService.getTemplateById(this.templateId).then(
      (res: any) => {
        // console.log("success teplate:-", res);
        this.template.templateDesc = res.templateDescription;
        this.template.templateName = res.templateName;
        this.dietPlan = JSON.parse(
          JSON.stringify(res.dietPlanData?.meal_plans)
        );
        this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
        this.bindTemplateData();
        this.suffuleArrayforTemplate();
        this.calculateCal(this.dietPlan);
        this.isShowTemplate = false;
      },
      (err) => {
        console.log("Error", err);
        this.isShowTemplate = false;
      }
    );
  }
  getDayWithSuffix(date: Date) {
    const day = date.getDate();
    if (day === 1 || day === 21 || day === 31) {
      return day + "st";
    } else if (day === 2 || day === 22) {
      return day + "nd";
    } else if (day === 3 || day === 23) {
      return day + "rd";
    } else {
      return day + "th";
    }
  }
  suffuleArrayforCopy() {
    this.dietPlan.filter((item: any, index: number) => {
      const day = moment(new Date()).format("ddd");
      if (
        moment(item.date1).format("ddd").toLowerCase() === day.toLowerCase()
      ) {
        if (index !== 0) {
          const element = this.dietPlan.splice(0, index);
          //  item.date1 = moment(new Date()).format("dd-MMM-yyyy")
          this.dietPlan.push(...element);
          // console.log("element",element,this.dietPlan);
        }
      }
    });
  }
  suffuleArrayforTemplate() {
    this.dietPlan.filter((item: any, index: number) => {
      const day = moment(new Date()).format("ddd");
      if (
        moment(item.date1).format("ddd").toLowerCase() === day.toLowerCase()
      ) {
        if (index !== 0) {
          const element = this.dietPlan.splice(0, index);
          this.dietPlan.push(...element);
          console.log("element", element, this.dietPlan);
        }
      }
    });
  }
  bindTemplateDataCopy() {
    const startDate = new Date();
    this.startDate = new Date(startDate).setDate(
      new Date(startDate).getDate()
    );
    const startPoint = new Date(this.startDate).setDate(
      new Date(this.startDate).getDate()
    );
    const defaultDate1 = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate() + 7)
    );
    this.dateRangeSevenDays =
      moment(startPoint).format("MMM DD") +
      " - " +
      moment(defaultDate1).format("DD");
    for (let index = 0; index < this.dietPlan.length; index++) {
      // this.dietPlan[index].date2 = moment(this.dietPlan[index]?.date1).format(
      //   "ddd"
      // );
      this.dietPlan[index].date1 = moment(
        new Date(this.startDate).setDate(
          new Date(this.startDate).getDate() + index
        )
      ).format("DD-MMM-YYYY");

      this.dietPlan[index].date =
        "<div class='custom-date-format'>" +
        moment(this.dietPlan[index].date1).format("ddd") +
        "</div><div class='custom-date-format2'>" +
        moment(this.dietPlan[index].date1).format("DD-MMM-YYYY") +
        "</div>";

      this.prepareData(this.dietPlan[index].diets);
    }
    const maxLength0 = Math.max(...this.preparedData0.map((obj) => obj.length));
    this.preparedData0.forEach((obj) => {
      while (obj.length < maxLength0) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength1 = Math.max(...this.preparedData1.map((obj) => obj.length));
    this.preparedData1.forEach((obj) => {
      while (obj.length < maxLength1) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength2 = Math.max(...this.preparedData2.map((obj) => obj.length));
    this.preparedData2.forEach((obj) => {
      while (obj.length < maxLength2) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength3 = Math.max(...this.preparedData3.map((obj) => obj.length));
    this.preparedData3.forEach((obj) => {
      while (obj.length < maxLength3) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength4 = Math.max(...this.preparedData4.map((obj) => obj.length));
    this.preparedData4.forEach((obj) => {
      while (obj.length < maxLength4) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength5 = Math.max(...this.preparedData5.map((obj) => obj.length));
    this.preparedData5.forEach((obj) => {
      while (obj.length < maxLength5) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength6 = Math.max(...this.preparedData6.map((obj) => obj.length));
    this.preparedData6.forEach((obj) => {
      while (obj.length < maxLength6) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength7 = Math.max(...this.preparedData7.map((obj) => obj.length));
    this.preparedData7.forEach((obj) => {
      while (obj.length < maxLength7) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength8 = Math.max(...this.preparedData8.map((obj) => obj.length));
    this.preparedData8.forEach((obj) => {
      while (obj.length < maxLength8) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });

    for (let index = 0; index < this.dietPlan[0].diets.length; index++) {
      this.slotItems.push({
        slotName: this.dietPlan[0].diets[index].message,
        time: this.dietPlan[0].diets[index].time,
        data:
          index == 0
            ? this.preparedData0
            : index == 1
            ? this.preparedData1
            : index == 2
            ? this.preparedData2
            : index == 3
            ? this.preparedData3
            : index == 4
            ? this.preparedData4
            : index == 5
            ? this.preparedData5
            : index == 6
            ? this.preparedData6
            : index == 7
            ? this.preparedData7
            : index == 8
            ? this.preparedData8
            : [],
      });
    }
  }
  bindTemplateData() {
    for (let index = 0; index < this.dietPlan.length; index++) {
      if (!this.dietPlan[index].date.includes("<div")) {
        this.dietPlan[index].date2 = moment(this.dietPlan[index]?.date1).format(
          "ddd"
        );
        this.dietPlan[index].date1 = moment(this.dietPlan[index]?.date1).format(
          "DD-MMM-YYYY"
        );
      } else {
        this.dietPlan[index].date2 = moment(this.dietPlan[index]?.date1).format(
          "ddd"
        );
      }
      this.prepareData(this.dietPlan[index].diets);
    }
    const maxLength0 = Math.max(...this.preparedData0.map((obj) => obj.length));
    this.preparedData0.forEach((obj) => {
      while (obj.length < maxLength0) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength1 = Math.max(...this.preparedData1.map((obj) => obj.length));
    this.preparedData1.forEach((obj) => {
      while (obj.length < maxLength1) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength2 = Math.max(...this.preparedData2.map((obj) => obj.length));
    this.preparedData2.forEach((obj) => {
      while (obj.length < maxLength2) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength3 = Math.max(...this.preparedData3.map((obj) => obj.length));
    this.preparedData3.forEach((obj) => {
      while (obj.length < maxLength3) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength4 = Math.max(...this.preparedData4.map((obj) => obj.length));
    this.preparedData4.forEach((obj) => {
      while (obj.length < maxLength4) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength5 = Math.max(...this.preparedData5.map((obj) => obj.length));
    this.preparedData5.forEach((obj) => {
      while (obj.length < maxLength5) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength6 = Math.max(...this.preparedData6.map((obj) => obj.length));
    this.preparedData6.forEach((obj) => {
      while (obj.length < maxLength6) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength7 = Math.max(...this.preparedData7.map((obj) => obj.length));
    this.preparedData7.forEach((obj) => {
      while (obj.length < maxLength7) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength8 = Math.max(...this.preparedData8.map((obj) => obj.length));
    this.preparedData8.forEach((obj) => {
      while (obj.length < maxLength8) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });

    for (let index = 0; index < this.dietPlan[0].diets.length; index++) {
      this.slotItems.push({
        slotName: this.dietPlan[0].diets[index].message,
        time: this.dietPlan[0].diets[index].time,
        data:
          index == 0
            ? this.preparedData0
            : index == 1
            ? this.preparedData1
            : index == 2
            ? this.preparedData2
            : index == 3
            ? this.preparedData3
            : index == 4
            ? this.preparedData4
            : index == 5
            ? this.preparedData5
            : index == 6
            ? this.preparedData6
            : index == 7
            ? this.preparedData7
            : index == 8
            ? this.preparedData8
            : [],
      });
    }
  }
  bindDietPlanModificationInsights() {
    this.preparedData0 = [];
    this.preparedData1 = [];
    this.preparedData2 = [];
    this.preparedData3 = [];
    this.preparedData4 = [];
    this.preparedData5 = [];
    this.preparedData6 = [];
    this.preparedData7 = [];
    this.preparedData8 = [];
    this.slotItems = [];
    for (let index = 0; index < this.dietPlan.length; index++) {
      if (!this.dietPlan[index].date.includes("<div")) {
        const splitNumbers = [
          this.dietPlan[index]?.date.slice(0, 2),
          this.dietPlan[index]?.date.slice(2, 4),
          this.dietPlan[index]?.date.slice(4),
        ];
        this.dietPlan[index].date1 =
          this.dietPlanPayload[index]?.date.slice(4) +
          "-" +
          this.dietPlanPayload[index]?.date.slice(2, 4) +
          "-" +
          this.dietPlanPayload[index]?.date.slice(0, 2);
        //  this.dietPlan[index].date2 = moment(this.dietPlanPayload[index]?.date).format("dddd");
        // console.log(splitNumbers.join("-"));
        const formatedDate = new Date(
          this.dietPlan[index]?.date.slice(4),
          this.dietPlan[index]?.date.slice(2, 4) - 1,
          this.dietPlan[index]?.date.slice(0, 2)
        );
        //  const dayformat = this.getDayWithSuffix(formatedDate);
        this.dietPlan[index].date =
          "<div class='custom-date-format'>" +
          moment(formatedDate).format("ddd") +
          "</div><div class='custom-date-format2'>" +
          moment(formatedDate).format("DD-MMM-YYYY") +
          "</div>";
      }

      this.prepareDataInSights(this.dietPlan[index].diets);
    }
    const maxLength0 = Math.max(...this.preparedData0.map((obj) => obj.length));
    this.preparedData0.forEach((obj) => {
      while (obj.length < maxLength0) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength1 = Math.max(...this.preparedData1.map((obj) => obj.length));
    this.preparedData1.forEach((obj) => {
      while (obj.length < maxLength1) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength2 = Math.max(...this.preparedData2.map((obj) => obj.length));
    this.preparedData2.forEach((obj) => {
      while (obj.length < maxLength2) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength3 = Math.max(...this.preparedData3.map((obj) => obj.length));
    this.preparedData3.forEach((obj) => {
      while (obj.length < maxLength3) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength4 = Math.max(...this.preparedData4.map((obj) => obj.length));
    this.preparedData4.forEach((obj) => {
      while (obj.length < maxLength4) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength5 = Math.max(...this.preparedData5.map((obj) => obj.length));
    this.preparedData5.forEach((obj) => {
      while (obj.length < maxLength5) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength6 = Math.max(...this.preparedData6.map((obj) => obj.length));
    this.preparedData6.forEach((obj) => {
      while (obj.length < maxLength6) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength7 = Math.max(...this.preparedData7.map((obj) => obj.length));
    this.preparedData7.forEach((obj) => {
      while (obj.length < maxLength7) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength8 = Math.max(...this.preparedData8.map((obj) => obj.length));
    this.preparedData8.forEach((obj) => {
      while (obj.length < maxLength8) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });

    for (let index = 0; index < this.dietPlan[0].diets.length; index++) {
      this.slotItems.push({
        slotName: this.dietPlan[0].diets[index].message,
        time: this.dietPlan[0].diets[index].time,
        data:
          index == 0
            ? this.preparedData0
            : index == 1
            ? this.preparedData1
            : index == 2
            ? this.preparedData2
            : index == 3
            ? this.preparedData3
            : index == 4
            ? this.preparedData4
            : index == 5
            ? this.preparedData5
            : index == 6
            ? this.preparedData6
            : index == 7
            ? this.preparedData7
            : index == 8
            ? this.preparedData8
            : [],
      });
    }
    console.log("this.slotItems:-", this.slotItems);
  }
  bindDietPlanModification() {
    this.slotItems = [];
    this.preparedData0 = [];
    this.preparedData1 = [];
    this.preparedData2 = [];
    this.preparedData3 = [];
    this.preparedData4 = [];
    this.preparedData5 = [];
    this.preparedData6 = [];
    this.preparedData7 = [];
    this.preparedData8 = [];
    for (let index = 0; index < this.dietPlan.length; index++) {
      if (!this.dietPlan[index].date.includes("<div")) {
        const splitNumbers = [
          this.dietPlan[index]?.date.slice(0, 2),
          this.dietPlan[index]?.date.slice(2, 4),
          this.dietPlan[index]?.date.slice(4),
        ];
        this.dietPlan[index].date1 =
          this.dietPlanPayload[index]?.date.slice(4) +
          "-" +
          this.dietPlanPayload[index]?.date.slice(2, 4) +
          "-" +
          this.dietPlanPayload[index]?.date.slice(0, 2);
        const formatedDate = new Date(
          this.dietPlan[index]?.date.slice(4),
          this.dietPlan[index]?.date.slice(2, 4) - 1,
          this.dietPlan[index]?.date.slice(0, 2)
        );

        this.dietPlan[index].date =
          "<div class='custom-date-format'>" +
          moment(formatedDate).format("ddd") +
          "</div><div class='custom-date-format2'>" +
          moment(formatedDate).format("DD-MMM-YYYY") +
          "</div>";
      }
      if (this.dietPlan[index].diets === undefined) {
        this.dietPlan[index].diets = this.emplyDataSlot.diets;
      }
      this.prepareData(this.dietPlan[index].diets);
    }

    const maxLength0 = Math.max(...this.preparedData0.map((obj) => obj.length));
    this.preparedData0.forEach((obj) => {
      while (obj.length < maxLength0) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength1 = Math.max(...this.preparedData1.map((obj) => obj.length));
    this.preparedData1.forEach((obj) => {
      while (obj.length < maxLength1) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength2 = Math.max(...this.preparedData2.map((obj) => obj.length));
    this.preparedData2.forEach((obj) => {
      while (obj.length < maxLength2) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength3 = Math.max(...this.preparedData3.map((obj) => obj.length));
    this.preparedData3.forEach((obj) => {
      while (obj.length < maxLength3) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength4 = Math.max(...this.preparedData4.map((obj) => obj.length));
    this.preparedData4.forEach((obj) => {
      while (obj.length < maxLength4) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength5 = Math.max(...this.preparedData5.map((obj) => obj.length));
    this.preparedData5.forEach((obj) => {
      while (obj.length < maxLength5) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength6 = Math.max(...this.preparedData6.map((obj) => obj.length));
    this.preparedData6.forEach((obj) => {
      while (obj.length < maxLength6) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength7 = Math.max(...this.preparedData7.map((obj) => obj.length));
    this.preparedData7.forEach((obj) => {
      while (obj.length < maxLength7) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength8 = Math.max(...this.preparedData8.map((obj) => obj.length));
    this.preparedData8.forEach((obj) => {
      while (obj.length < maxLength8) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });

    for (let index = 0; index < this.dietPlan[0].diets?.length; index++) {
      this.slotItems.push({
        slotName: this.dietPlan[0].diets[index].message,
        time: this.dietPlan[0].diets[index].time,
        data:
          index == 0
            ? this.preparedData0
            : index == 1
            ? this.preparedData1
            : index == 2
            ? this.preparedData2
            : index == 3
            ? this.preparedData3
            : index == 4
            ? this.preparedData4
            : index == 5
            ? this.preparedData5
            : index == 6
            ? this.preparedData6
            : index == 7
            ? this.preparedData7
            : index == 8
            ? this.preparedData8
            : [],
      });
    }
    console.log("this.slotItems:-", this.slotItems);
  }
  getDietPlanForward1() {
    const defaultDate = new Date(
      new Date().setDate(new Date().getDate() + this.backCounterForward)
    );
    const date = moment(defaultDate).format("DDMMYYYY");
    this.dietPlan = [];
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        const token = error.error.text;
        this.appService.getDiet7daysPlan(date, token).then(
          (success: any) => {
            this.dietPlan = success;
            this.dietPlanPayload = [];
            for (let index = 0; index < this.dietPlan.length; index++) {
              this.dietPlan[index].date = moment(
                new Date(defaultDate).setDate(defaultDate.getDate() + index)
              ).format("DDMMYYYY");
            }
            this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));

            // console.log(this.dietPlan);
            const nextDate = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 0)
            ).format("DDMMYYYY");
            const nextDate1 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 1)
            ).format("DDMMYYYY");
            const nextDate2 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 2)
            ).format("DDMMYYYY");
            const nextDate3 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 3)
            ).format("DDMMYYYY");
            const nextDate4 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 4)
            ).format("DDMMYYYY");
            const nextDate5 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 5)
            ).format("DDMMYYYY");
            const nextDate6 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 6)
            ).format("DDMMYYYY");

            if (this.dietPlan[0].date !== nextDate) {
              this.dietPlan.splice(
                0,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[0].date = nextDate;
            }
            if (this.dietPlan[1]?.date !== nextDate1) {
              this.dietPlan.splice(
                1,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[1].date = nextDate1;
            }
            if (this.dietPlan[2]?.date !== nextDate2) {
              this.dietPlan.splice(
                2,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[2].date = nextDate2;
            }
            if (this.dietPlan[3]?.date !== nextDate3) {
              this.dietPlan.splice(
                3,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[3].date = nextDate3;
            }
            if (this.dietPlan[4]?.date !== nextDate4) {
              this.dietPlan.splice(
                4,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[4].date = nextDate4;
            }
            if (this.dietPlan[5]?.date !== nextDate5) {
              this.dietPlan.splice(
                5,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[5].date = nextDate5;
            }
            if (this.dietPlan[6]?.date !== nextDate6) {
              this.dietPlan.splice(
                6,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[6].date = nextDate6;
            }

            // bindmethod
            debugger;
            this.bindDietPlanModification();
            this.calculateCal(this.dietPlan);
            this.disablePastDateDiet();
          },
          (error) => {
            console.log("Error:-", error);
          }
        );
      }
    );
  }
  getDietPlanForward(isOne = null) {
    let defaultDate;
    defaultDate = this.startDate;
    const date = moment(defaultDate).format("DDMMYYYY");
    this.dietPlan = [];
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        const token = error.error.text;
        this.appService.getDiet7daysPlan(date, token).then(
          (success: any) => {
            // const filteredData = success.filter((item) => {
            //   return item?.code === undefined;
            // });

            this.dietPlan = success;
            this.dietPlanPayload = [];
            for (let index = 0; index < this.dietPlan.length; index++) {
              this.dietPlan[index].date = moment(
                new Date(defaultDate).setDate(defaultDate.getDate() + index)
              ).format("DDMMYYYY");
            }
            this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));

            // console.log(this.dietPlan);
            const nextDate = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 0)
            ).format("DDMMYYYY");
            const nextDate1 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 1)
            ).format("DDMMYYYY");
            const nextDate2 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 2)
            ).format("DDMMYYYY");
            const nextDate3 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 3)
            ).format("DDMMYYYY");
            const nextDate4 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 4)
            ).format("DDMMYYYY");
            const nextDate5 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 5)
            ).format("DDMMYYYY");
            const nextDate6 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 6)
            ).format("DDMMYYYY");

            if (this.dietPlan[0]?.date !== nextDate) {
              this.dietPlan.splice(
                0,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[0].date = nextDate;
            }
            if (this.dietPlan[1]?.date !== nextDate1) {
              this.dietPlan.splice(
                1,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[1].date = nextDate1;
            }
            if (this.dietPlan[2]?.date !== nextDate2) {
              this.dietPlan.splice(
                2,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[2].date = nextDate2;
            }
            if (this.dietPlan[3]?.date !== nextDate3) {
              this.dietPlan.splice(
                3,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[3].date = nextDate3;
            }
            if (this.dietPlan[4]?.date !== nextDate4) {
              this.dietPlan.splice(
                4,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[4].date = nextDate4;
            }
            if (this.dietPlan[5]?.date !== nextDate5) {
              this.dietPlan.splice(
                5,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[5].date = nextDate5;
            }
            if (this.dietPlan[6]?.date !== nextDate6) {
              this.dietPlan.splice(
                6,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[6].date = nextDate6;
            }
            debugger;
            // bindmethod
            this.bindDietPlanModification();
            this.calculateCal(this.dietPlan);
            this.disablePastDateDiet();
          },
          (error) => {
            console.log("Error:-", error);
          }
        );
      }
    );
  }
  getDietPlan1() {
    // const defaultDate = new Date()
    // const defaultDate1 = new Date().setDate(new Date().getDate() + (this.backCounterForward * 7));
    const defaultDate = new Date(
      new Date().setDate(new Date().getDate() + 1 + this.backCounterForward * 1)
    );
    const defaultDate1 = new Date(defaultDate).setDate(
      new Date(defaultDate).getDate() + 1
    );
    this.dateRangeSevenDays =
      moment(defaultDate).format("MMM DD") +
      " - " +
      moment(defaultDate1).format("DD");
    const date = moment(defaultDate).format("DDMMYYYY");
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        const token = error.error.text;
        this.appService.getDiet7daysPlan(date, token).then(
          (success: any) => {
            this.dietPlan = success;
            //  this.dietPlanPayload=[];
            for (let index = 0; index < this.dietPlan.length; index++) {
              if (this.dietPlan[index].code === "0001") {
                this.dietPlan[index] = this.emplyDataSlot;
              }
              this.dietPlan[index].date = moment(
                new Date(defaultDate).setDate(defaultDate.getDate() + index)
              ).format("DDMMYYYY");
            }
            this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));

            // console.log(this.dietPlan);
            const nextDate = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 0)
            ).format("DDMMYYYY");
            const nextDate1 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 1)
            ).format("DDMMYYYY");
            const nextDate2 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 2)
            ).format("DDMMYYYY");
            const nextDate3 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 3)
            ).format("DDMMYYYY");
            const nextDate4 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 4)
            ).format("DDMMYYYY");
            const nextDate5 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 5)
            ).format("DDMMYYYY");
            const nextDate6 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 6)
            ).format("DDMMYYYY");

            if (this.dietPlan[0].date !== nextDate) {
              this.dietPlan.splice(
                0,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[0].date = nextDate;
            }
            if (this.dietPlan[1]?.date !== nextDate1) {
              this.dietPlan.splice(
                1,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[1].date = nextDate1;
            }
            if (this.dietPlan[2]?.date !== nextDate2) {
              this.dietPlan.splice(
                2,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[2].date = nextDate2;
            }
            if (this.dietPlan[3]?.date !== nextDate3) {
              this.dietPlan.splice(
                3,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[3].date = nextDate3;
            }
            if (this.dietPlan[4]?.date !== nextDate4) {
              this.dietPlan.splice(
                4,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[4].date = nextDate4;
            }
            if (this.dietPlan[5]?.date !== nextDate5) {
              this.dietPlan.splice(
                5,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[5].date = nextDate5;
            }
            if (this.dietPlan[6]?.date !== nextDate6) {
              this.dietPlan.splice(
                6,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[6].date = nextDate6;
            }

            // bindmethod
            debugger;
            this.bindDietPlanModification();
            this.calculateCal(this.dietPlan);
            this.disablePastDateDiet();
          },
          (error) => {
            console.log("Error:-", error);
          }
        );
      }
    );
  }
  getDietPlanSeven(isOne = null) {
    let defaultDate;
    if (isOne === null) {
      defaultDate = this.startDate;
    } else {
      defaultDate = this.startDate;
    }
    const date = moment(defaultDate).format("DDMMYYYY");
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        const token = error.error.text;
        this.appService.getDiet7daysPlan(date, token).then(
          (success: any) => {
            this.dietPlan = success;
            for (let index = 0; index < this.dietPlan.length; index++) {
              this.dietPlan[index].date = moment(
                new Date(defaultDate).setDate(defaultDate.getDate() + index)
              ).format("DDMMYYYY");
            }
            const nextDate = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 0)
            ).format("DDMMYYYY");
            const nextDate1 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 1)
            ).format("DDMMYYYY");
            const nextDate2 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 2)
            ).format("DDMMYYYY");
            const nextDate3 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 3)
            ).format("DDMMYYYY");
            const nextDate4 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 4)
            ).format("DDMMYYYY");
            const nextDate5 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 5)
            ).format("DDMMYYYY");
            const nextDate6 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 6)
            ).format("DDMMYYYY");

            if (this.dietPlan[0]?.date !== nextDate) {
              this.dietPlan.splice(
                0,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[0].date = nextDate;
            }
            if (this.dietPlan[1]?.date !== nextDate1) {
              this.dietPlan.splice(
                1,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[1].date = nextDate1;
            }
            if (this.dietPlan[2]?.date !== nextDate2) {
              this.dietPlan.splice(
                2,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[2].date = nextDate2;
            }
            if (this.dietPlan[3]?.date !== nextDate3) {
              this.dietPlan.splice(
                3,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[3].date = nextDate3;
            }
            if (this.dietPlan[4]?.date !== nextDate4) {
              this.dietPlan.splice(
                4,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[4].date = nextDate4;
            }
            if (this.dietPlan[5]?.date !== nextDate5) {
              this.dietPlan.splice(
                5,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[5].date = nextDate5;
            }
            if (this.dietPlan[6]?.date !== nextDate6) {
              this.dietPlan.splice(
                6,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[6].date = nextDate6;
            }

            // bindmethod

            this.bindDietPlanModification();
            this.calculateCal(this.dietPlan);
            this.disablePastDateDiet();
          },
          (error) => {
            console.log("Error:-", error);
          }
        );
      }
    );
  }
  getDietPlanBasedOnDate() {
    this.startDate = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate())
    );
    const defaultDate1 = new Date(this.startDate).setDate(
      new Date(this.startDate).getDate() + 6
    );
    const defaultDate = new Date(this.startDate);
    this.dateRangeSevenDays =
      moment(this.startDate).format("MMM DD") +
      " - " +
      moment(defaultDate1).format("DD");
    const date = moment(defaultDate).format("DDMMYYYY");
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        const token = error.error.text;
        this.appService.getDiet7daysPlan(date, token).then(
          (success: any) => {
            this.dietPlan = success;
            //  this.dietPlanPayload=[];
            for (let index = 0; index < this.dietPlan.length; index++) {
              if (this.dietPlan[index].code === "0001") {
                this.dietPlan[index] = this.emplyDataSlot;
              }
              this.dietPlan[index].date = moment(
                new Date(defaultDate).setDate(defaultDate.getDate() + index)
              ).format("DDMMYYYY");
            }
            this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));

            // console.log(this.dietPlan);
            const nextDate = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 0)
            ).format("DDMMYYYY");
            const nextDate1 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 1)
            ).format("DDMMYYYY");
            const nextDate2 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 2)
            ).format("DDMMYYYY");
            const nextDate3 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 3)
            ).format("DDMMYYYY");
            const nextDate4 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 4)
            ).format("DDMMYYYY");
            const nextDate5 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 5)
            ).format("DDMMYYYY");
            const nextDate6 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 6)
            ).format("DDMMYYYY");

            if (this.dietPlan[0].date !== nextDate) {
              this.dietPlan.splice(
                0,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[0].date = nextDate;
            }
            if (this.dietPlan[1]?.date !== nextDate1) {
              this.dietPlan.splice(
                1,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[1].date = nextDate1;
            }
            if (this.dietPlan[2]?.date !== nextDate2) {
              this.dietPlan.splice(
                2,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[2].date = nextDate2;
            }
            if (this.dietPlan[3]?.date !== nextDate3) {
              this.dietPlan.splice(
                3,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[3].date = nextDate3;
            }
            if (this.dietPlan[4]?.date !== nextDate4) {
              this.dietPlan.splice(
                4,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[4].date = nextDate4;
            }
            if (this.dietPlan[5]?.date !== nextDate5) {
              this.dietPlan.splice(
                5,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[5].date = nextDate5;
            }
            if (this.dietPlan[6]?.date !== nextDate6) {
              this.dietPlan.splice(
                6,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[6].date = nextDate6;
            }

            // bindmethod
            this.bindDietPlanModification();
            this.calculateCal(this.dietPlan);
            this.disablePastDateDiet();
          },
          (error) => {
            console.log("Error:-", error);
          }
        );
      }
    );
  }
  isPastDate = false;
  compareDietPlanObject:any=undefined;
  getDietPlan() {
    this.startDate = new Date(
      new Date(this.startDate).setDate(new Date(this.startDate).getDate())
    );
    const defaultDate1 = new Date(this.startDate).setDate(
      new Date().getDate() + 7
    );
    const defaultDate = new Date(this.startDate);
    if (defaultDate < new Date()) {
      this.isPastDate = true;
    } else {
      this.isPastDate = false;
    }

    this.dateRangeSevenDays =
      moment(this.startDate).format("MMM DD") +
      " - " +
      moment(defaultDate1).format("DD");
    const date = moment(defaultDate).format("DDMMYYYY");
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        const token = error.error.text;
        this.appService.getDiet7daysPlan(date, token).then(
          (success: any) => {
            this.dietPlan = success;
           
            console.log("dietPlan:---", this.dietPlan);
            //  this.dietPlanPayload=[];
            for (let index = 0; index < this.dietPlan.length; index++) {
              if (this.dietPlan[index].code === "0001") {
                this.dietPlan[index] = this.emplyDataSlot;
              }
              this.dietPlan[index].date = moment(
                new Date(defaultDate).setDate(defaultDate.getDate() + index)
              ).format("DDMMYYYY");
            }
            this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));

            // console.log(this.dietPlan);
            const nextDate = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 0)
            ).format("DDMMYYYY");
            const nextDate1 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 1)
            ).format("DDMMYYYY");
            const nextDate2 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 2)
            ).format("DDMMYYYY");
            const nextDate3 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 3)
            ).format("DDMMYYYY");
            const nextDate4 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 4)
            ).format("DDMMYYYY");
            const nextDate5 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 5)
            ).format("DDMMYYYY");
            const nextDate6 = moment(
              new Date(defaultDate).setDate(defaultDate.getDate() + 6)
            ).format("DDMMYYYY");

            if (this.dietPlan[0].date !== nextDate) {
              this.dietPlan.splice(
                0,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[0].date = nextDate;
            }
            if (this.dietPlan[1]?.date !== nextDate1) {
              this.dietPlan.splice(
                1,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[1].date = nextDate1;
            }
            if (this.dietPlan[2]?.date !== nextDate2) {
              this.dietPlan.splice(
                2,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[2].date = nextDate2;
            }
            if (this.dietPlan[3]?.date !== nextDate3) {
              this.dietPlan.splice(
                3,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[3].date = nextDate3;
            }
            if (this.dietPlan[4]?.date !== nextDate4) {
              this.dietPlan.splice(
                4,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[4].date = nextDate4;
            }
            if (this.dietPlan[5]?.date !== nextDate5) {
              this.dietPlan.splice(
                5,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[5].date = nextDate5;
            }
            if (this.dietPlan[6]?.date !== nextDate6) {
              this.dietPlan.splice(
                6,
                0,
                JSON.parse(JSON.stringify(this.emplyDataSlot))
              );
              this.dietPlan[6].date = nextDate6;
            }

            // bindmethod
            this.bindDietPlanModification();
            this.calculateCal(this.dietPlan);
            this.disablePastDateDiet();
          },
          (error) => {
            console.log("Error:-", error);
          }
        );
      }
    );
  }
  defaultStructureOfJson() {
    for (let index = 0; index < 7; index++) {
      const minusDate = 7 - index;
      this.dietPlan.push({
        date: moment(
          new Date(new Date().setDate(new Date().getDate() - minusDate))
        ).format("DDMMYYYY"),
        totalCal: 0,
        recomended: 0,
        tolalCalories: 0,
        totalCalories: 0,
        totalCaloriesPer: 0,
        totalCarbs: 0,
        totalCarbsPer: 0,
        totalFat: 0,
        totalFatPer: 0,
        totalProtien: 0,
        totalProtienPer: 0,
        totalFiber: 0,
        totalFiberPer: 0,
        calDistribution: 0,
        calDistributionPer: 0,
        diets: [],
      });
      for (let j = 0; j < 1; j++) {
        this.dietPlan[index].diets.push(
          {
            time: "06:30",
            message: "When You Wake up",
            slot: 0,
            data: [{}],
            totalCalories: 0,
            totalCarbs: 0,
            totalFat: 0,
            totalProtien: 0,
            totalFiber: 0,
          },
          {
            time: "7:30",
            message: "Before Breakfast",
            slot: 1,
            data: [{}],
            totalCalories: 0,
            totalCarbs: 0,
            totalFat: 0,
            totalProtien: 0,
            totalFiber: 0,
          },
          {
            time: "10:00",
            message: "Breakfast",
            slot: 2,
            data: [{}],
            totalCalories: 0,
            totalCarbs: 0,
            totalFat: 0,
            totalProtien: 0,
            totalFiber: 0,
          },
          {
            time: "11:30",
            message: "Mid Day Meal",
            slot: 3,
            data: [{}],
            totalCalories: 0,
            totalCarbs: 0,
            totalFat: 0,
            totalProtien: 0,
            totalFiber: 0,
          },
          {
            time: "13:30",
            message: "Lunch",
            slot: 4,
            data: [{}],
            totalCalories: 0,
            totalCarbs: 0,
            totalFat: 0,
            totalProtien: 0,
            totalFiber: 0,
          },
          {
            time: "15:00",
            message: "Post Lunch",
            slot: 5,
            data: [{}],
            totalCalories: 0,
            totalCarbs: 0,
            totalFat: 0,
            totalProtien: 0,
            totalFiber: 0,
          },
          {
            time: "17:00",
            message: "Evening Snack",
            slot: 6,
            data: [{}],
            totalCalories: 0,
            totalCarbs: 0,
            totalFat: 0,
            totalProtien: 0,
            totalFiber: 0,
          },
          {
            time: "19:30",
            message: "Dinner",
            slot: 7,
            data: [{}],
            totalCalories: 0,
            totalCarbs: 0,
            totalFat: 0,
            totalProtien: 0,
            totalFiber: 0,
          },
          {
            time: "20:30",
            message: "Before Sleep",
            slot: 8,
            data: [{}],
            totalCalories: 0,
            totalCarbs: 0,
            totalFat: 0,
            totalProtien: 0,
            totalFiber: 0,
          }
        );
      }
    }
  }
  getnewTemplat() {
    // const userId = localStorage.getItem("email");
    // this.appService.getDietRecall(userId).then(
    //   (res: any) => {
    //     console.log("res", res);
    const res = null;
    if (res === undefined || res === null || res.data === null) {
      this.defaultStructureOfJson();
      this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
    } else {
      this.dietPlan = res.data;
      this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
      console.log(this.dietPlan);
    }
    this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
    console.log(this.dietPlan);
    for (let index = 0; index < this.dietPlan.length; index++) {
      const formatedDate = new Date(
        this.dietPlan[index].date.slice(4),
        this.dietPlan[index].date.slice(2, 4) - 1,
        this.dietPlan[index].date.slice(0, 2)
      );

      this.dietPlan[index].date1 = formatedDate;
      this.dietPlan[index].date2 = JSON.parse(JSON.stringify(this.dietPlan))[
        index
      ].date;
      this.dietPlan[index].date =
        "<div class='custom-date-format'>" +
        moment(formatedDate).format("ddd") +
        "</div>";
      this.prepareData(this.dietPlan[index].diets);
      this.calculateCal(this.dietPlan);
    }
    const maxLength0 = Math.max(...this.preparedData0.map((obj) => obj.length));
    this.preparedData0.forEach((obj) => {
      while (obj.length < maxLength0) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength1 = Math.max(...this.preparedData1.map((obj) => obj.length));
    this.preparedData1.forEach((obj) => {
      while (obj.length < maxLength1) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength2 = Math.max(...this.preparedData2.map((obj) => obj.length));
    this.preparedData2.forEach((obj) => {
      while (obj.length < maxLength2) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength3 = Math.max(...this.preparedData3.map((obj) => obj.length));
    this.preparedData3.forEach((obj) => {
      while (obj.length < maxLength3) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength4 = Math.max(...this.preparedData4.map((obj) => obj.length));
    this.preparedData4.forEach((obj) => {
      while (obj.length < maxLength4) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength5 = Math.max(...this.preparedData5.map((obj) => obj.length));
    this.preparedData5.forEach((obj) => {
      while (obj.length < maxLength5) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength6 = Math.max(...this.preparedData6.map((obj) => obj.length));
    this.preparedData6.forEach((obj) => {
      while (obj.length < maxLength6) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength7 = Math.max(...this.preparedData7.map((obj) => obj.length));
    this.preparedData7.forEach((obj) => {
      while (obj.length < maxLength7) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });
    const maxLength8 = Math.max(...this.preparedData8.map((obj) => obj.length));
    this.preparedData8.forEach((obj) => {
      while (obj.length < maxLength8) {
        obj.push({}); // Add empty objects until it reaches the maximum length
      }
    });

    for (let index = 0; index < this.dietPlan[0].diets.length; index++) {
      this.slotItems.push({
        slotName: this.dietPlan[0].diets[index].message,
        time: this.dietPlan[0].diets[index].time,
        data:
          index == 0
            ? this.preparedData0
            : index == 1
            ? this.preparedData1
            : index == 2
            ? this.preparedData2
            : index == 3
            ? this.preparedData3
            : index == 4
            ? this.preparedData4
            : index == 5
            ? this.preparedData5
            : index == 6
            ? this.preparedData6
            : index == 7
            ? this.preparedData7
            : index == 8
            ? this.preparedData8
            : [],
      });
    }
    this.compareDietPlanObject = JSON.parse(JSON.stringify(this.dietPlan));
   
  }
  getRecallPlan() {
    const userId = localStorage.getItem("email");
    this.appService.getDietRecall(userId).then(
      (res: any) => {
        console.log("res", res);
        if (res === undefined || res === null || res.data === null) {
          this.defaultStructureOfJson();
          this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
        } else {
          this.dietPlan = res.data;
          this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
          console.log(this.dietPlan);
        }
        this.dietPlanPayload = JSON.parse(JSON.stringify(this.dietPlan));
        console.log(this.dietPlan);
        for (let index = 0; index < this.dietPlan.length; index++) {
          const formatedDate = new Date(
            this.dietPlan[index].date.slice(4),
            this.dietPlan[index].date.slice(2, 4) - 1,
            this.dietPlan[index].date.slice(0, 2)
          );

          this.dietPlan[index].date1 =
            this.getDayWithSuffix(formatedDate) +
            "-" +
            moment(formatedDate).format("MMM-YYYY");
          this.dietPlan[index].date2 = JSON.parse(
            JSON.stringify(this.dietPlan)
          )[index].date;
          this.dietPlan[index].date =
            "<div class='custom-date-format'>" +
            moment(formatedDate).format("ddd") +
            "</div><div class='custom-date-format2'>" +
            moment(formatedDate).format("DD-MMM-YYYY") +
            "</div>";
          this.prepareData(this.dietPlan[index].diets);
          this.calculateCal(this.dietPlan);
        }
        const maxLength0 = Math.max(
          ...this.preparedData0.map((obj) => obj.length)
        );
        this.preparedData0.forEach((obj) => {
          while (obj.length < maxLength0) {
            obj.push({}); // Add empty objects until it reaches the maximum length
          }
        });
        const maxLength1 = Math.max(
          ...this.preparedData1.map((obj) => obj.length)
        );
        this.preparedData1.forEach((obj) => {
          while (obj.length < maxLength1) {
            obj.push({}); // Add empty objects until it reaches the maximum length
          }
        });
        const maxLength2 = Math.max(
          ...this.preparedData2.map((obj) => obj.length)
        );
        this.preparedData2.forEach((obj) => {
          while (obj.length < maxLength2) {
            obj.push({}); // Add empty objects until it reaches the maximum length
          }
        });
        const maxLength3 = Math.max(
          ...this.preparedData3.map((obj) => obj.length)
        );
        this.preparedData3.forEach((obj) => {
          while (obj.length < maxLength3) {
            obj.push({}); // Add empty objects until it reaches the maximum length
          }
        });
        const maxLength4 = Math.max(
          ...this.preparedData4.map((obj) => obj.length)
        );
        this.preparedData4.forEach((obj) => {
          while (obj.length < maxLength4) {
            obj.push({}); // Add empty objects until it reaches the maximum length
          }
        });
        const maxLength5 = Math.max(
          ...this.preparedData5.map((obj) => obj.length)
        );
        this.preparedData5.forEach((obj) => {
          while (obj.length < maxLength5) {
            obj.push({}); // Add empty objects until it reaches the maximum length
          }
        });
        const maxLength6 = Math.max(
          ...this.preparedData6.map((obj) => obj.length)
        );
        this.preparedData6.forEach((obj) => {
          while (obj.length < maxLength6) {
            obj.push({}); // Add empty objects until it reaches the maximum length
          }
        });
        const maxLength7 = Math.max(
          ...this.preparedData7.map((obj) => obj.length)
        );
        this.preparedData7.forEach((obj) => {
          while (obj.length < maxLength7) {
            obj.push({}); // Add empty objects until it reaches the maximum length
          }
        });
        const maxLength8 = Math.max(
          ...this.preparedData8.map((obj) => obj.length)
        );
        this.preparedData8.forEach((obj) => {
          while (obj.length < maxLength8) {
            obj.push({}); // Add empty objects until it reaches the maximum length
          }
        });

        for (let index = 0; index < this.dietPlan[0].diets.length; index++) {
          this.slotItems.push({
            slotName: this.dietPlan[0].diets[index].message,
            time: this.dietPlan[0].diets[index].time,
            data:
              index == 0
                ? this.preparedData0
                : index == 1
                ? this.preparedData1
                : index == 2
                ? this.preparedData2
                : index == 3
                ? this.preparedData3
                : index == 4
                ? this.preparedData4
                : index == 5
                ? this.preparedData5
                : index == 6
                ? this.preparedData6
                : index == 7
                ? this.preparedData7
                : index == 8
                ? this.preparedData8
                : [],
          });
        }
        this.compareDietPlanObject = JSON.parse(JSON.stringify(this.dietPlan));
      },
      (error) => {
        console.log("Error:-", error);
      }
    );
  }
  isDisable = new Array(63).fill(false);
  disablePastDateDiet() {
    const dt = new Date(
      new Date().getFullYear(),
      new Date().getMonth(),
      new Date().getDate()
    );
    console.log("dt", dt);
    const currentdatetime = dt.getTime();
    for (let index = 0; index < this.dietPlan?.length; index++) {
      for (let j = 0; j < this.dietPlan[index]?.diets?.length; j++) {
        //
        if (new Date(this.dietPlan[index].date1).getTime() < currentdatetime) {
          this.isDisable[j] = true;
        }
      }
    }
    console.log("this.isDisable", this.isDisable);
  //  if(this.inSights==='false' && (this.isEdit==='false' ||this.isEdit===undefined)){
     this.compareDietPlanObject = JSON.parse(JSON.stringify(this.dietPlan));
  //  }
   
  }
  calculateCal(sevenDaysDiet: any) {
    let svndaysplan = sevenDaysDiet;
    let totalCal = 0;
    let totalCarbs = 0;
    let totalFat = 0;
    let totalProtein = 0;
    let totalFiber = 0;

    let totalCalEaten = 0;
    let totalCarbsEaten = 0;
    let totalFatEaten = 0;
    let totalProteinEaten = 0;
    let totalFiberEaten = 0;
    const currentdatetime = new Date().getTime();
    for (let index = 0; index < svndaysplan.length; index++) {
      totalCal = 0;
      totalCarbs = 0;
      totalFat = 0;
      totalProtein = 0;
      totalFiber = 0;

      totalCalEaten = 0;
      totalCarbsEaten = 0;
      totalFatEaten = 0;
      totalProteinEaten = 0;
      totalFiberEaten = 0;
      for (let j = 0; j < svndaysplan[index].diets?.length; j++) {
        for (let k = 0; k < svndaysplan[index].diets[j].data?.length; k++) {
          svndaysplan[index].diets[j].data[k].defaultPortion =
            this.newPortion !== 0
              ? this.newPortion
              : JSON.parse(JSON.stringify(svndaysplan[index])).diets[j].data[k]
                  .portion;
          totalCal =
            totalCal +
            Number(
              svndaysplan[index].diets[j].data[k]?.Calories === undefined
                ? 0
                : svndaysplan[index].diets[j].data[k]?.Calories
            );
          totalCarbs =
            totalCarbs +
            Number(
              svndaysplan[index].diets[j].data[k]?.Carbs === undefined
                ? 0
                : svndaysplan[index].diets[j].data[k]?.Carbs
            );
          totalFat =
            totalFat +
            Number(
              svndaysplan[index].diets[j].data[k]?.Fat === undefined
                ? 0
                : svndaysplan[index].diets[j].data[k]?.Fat
            );
          totalProtein =
            totalProtein +
            Number(
              svndaysplan[index].diets[j].data[k]?.Protien === undefined
                ? 0
                : svndaysplan[index].diets[j].data[k]?.Protien
            );

          totalFiber =
            totalFiber +
            Number(
              svndaysplan[index].diets[j].data[k]?.Fiber === undefined
                ? 0
                : svndaysplan[index].diets[j].data[k]?.Fiber
            );


            
            if(svndaysplan[index].diets[j].data[k].eaten>0){
            totalCalEaten =
              totalCalEaten +
              Number(
                svndaysplan[index].diets[j].data[k]?.Calories === undefined
                  ? 0
                  : svndaysplan[index].diets[j].data[k]?.Calories
              );
            totalCarbsEaten =
              totalCarbsEaten +
              Number(
                svndaysplan[index].diets[j].data[k]?.Carbs === undefined
                  ? 0
                  : svndaysplan[index].diets[j].data[k]?.Carbs
              );
            totalFatEaten =
              totalFatEaten +
              Number(
                svndaysplan[index].diets[j].data[k]?.Fat === undefined
                  ? 0
                  : svndaysplan[index].diets[j].data[k]?.Fat
              );
            totalProteinEaten =
              totalProteinEaten +
              Number(
                svndaysplan[index].diets[j].data[k]?.Protien === undefined
                  ? 0
                  : svndaysplan[index].diets[j].data[k]?.Protien
              );

            totalFiberEaten =
              totalFiberEaten +
              Number(
                svndaysplan[index].diets[j].data[k]?.Fiber === undefined
                  ? 0
                  : svndaysplan[index].diets[j].data[k]?.Fiber
              );
          }
        }
      }
      this.dietPlan[index].tolalCalories = totalCal.toFixed(0);
      this.dietPlan[index].totalCarbs = totalCarbs.toFixed(0);
      this.dietPlan[index].totalFat = totalFat.toFixed(0);
      this.dietPlan[index].totalProtien = totalProtein.toFixed(0);
      this.dietPlan[index].totalFiber = totalFiber.toFixed(0);

      this.dietPlan[index].tolalCaloriesEaten = totalCalEaten.toFixed(0);
      this.dietPlan[index].totalCarbsEaten = totalCarbsEaten.toFixed(0);
      this.dietPlan[index].totalFatEaten = totalFatEaten.toFixed(0);
      this.dietPlan[index].totalProtienEaten = totalProteinEaten.toFixed(0);
      this.dietPlan[index].totalFiberEaten = totalFiberEaten.toFixed(0);

      console.log("totalCal", totalCal);
      console.log("totalCarbs", totalCarbs);
      console.log("totalFat", totalFat);
      console.log("totalProtein", totalProtein);
      console.log("totalFiber", totalFiber);
      totalCal = 0;
      totalCarbs = 0;
      totalFat = 0;
      totalProtein = 0;
      totalFiber = 0;

      console.log("totalCalEaten", totalCalEaten);
      console.log("totalCarbsEaten", totalCarbsEaten);
      console.log("totalFatEaten", totalFatEaten);
      console.log("totalProteinEaten", totalProteinEaten);
      console.log("totalFiberEaten", totalFiberEaten);
      totalCalEaten = 0;
      totalCarbsEaten = 0;
      totalFatEaten = 0;
      totalProteinEaten = 0;
      totalFiberEaten = 0;

      if (
        Number(this.profileData?.lifeStyle?.calories) <
        Number(this.dietPlan[index].tolalCalories)
      ) {
        this.isCaloryExceed[index] = true;
      } else {
        this.isCaloryExceed[index] = false;
      }
    }
    // console.log("",this.dietPlan);
  }
  preparedData0: any = [];
  preparedData1: any = [];
  preparedData2: any = [];
  preparedData3: any = [];
  preparedData4: any = [];
  preparedData5: any = [];
  preparedData6: any = [];
  preparedData7: any = [];
  preparedData8: any = [];
  slotItems: any = [];
  prepareData(diet: any) {
    if (diet?.length > 0) {
      for (let index = 0; index < diet.length; index++) {
        if (diet[index].slot === 0) {
          this.preparedData0.push(diet[index].data);
        } else if (diet[index].slot === 1) {
          this.preparedData1.push(diet[index].data);
        } else if (diet[index].slot === 2) {
          this.preparedData2.push(diet[index].data);
        } else if (diet[index].slot === 3) {
          this.preparedData3.push(diet[index].data);
        } else if (diet[index].slot === 4) {
          this.preparedData4.push(diet[index].data);
        } else if (diet[index].slot === 5) {
          this.preparedData5.push(diet[index].data);
        } else if (diet[index].slot === 6) {
          this.preparedData6.push(diet[index].data);
        } else if (diet[index].slot === 7) {
          this.preparedData7.push(diet[index].data);
        } else if (diet[index].slot === 8) {
          this.preparedData8.push(diet[index].data);
        }
      }
    } else {
      this.preparedData0.push([{}]);
      this.preparedData1.push([{}]);
      this.preparedData2.push([{}]);
      this.preparedData3.push([{}]);
      this.preparedData4.push([{}]);
      this.preparedData5.push([{}]);
      this.preparedData6.push([{}]);
      this.preparedData7.push([{}]);
      this.preparedData8.push([{}]);
    }
  }
  prepareDataInSights(diet: any) {
    if (diet?.length > 0) {
      for (let index = 0; index < diet.length; index++) {
        if (diet[index].slot === 0) {
          this.preparedData0.push(diet[index].data);
        } else if (diet[index].slot === 1) {
          this.preparedData1.push(diet[index].data);
        } else if (diet[index].slot === 2) {
          this.preparedData2.push(diet[index].data);
        } else if (diet[index].slot === 3) {
          this.preparedData3.push(diet[index].data);
        } else if (diet[index].slot === 4) {
          this.preparedData4.push(diet[index].data);
        } else if (diet[index].slot === 5) {
          this.preparedData5.push(diet[index].data);
        } else if (diet[index].slot === 6) {
          this.preparedData6.push(diet[index].data);
        } else if (diet[index].slot === 7) {
          this.preparedData7.push(diet[index].data);
        } else if (diet[index].slot === 8) {
          this.preparedData8.push(diet[index].data);
        }
      }
    } else {
      this.preparedData0.push([{}]);
      this.preparedData1.push([{}]);
      this.preparedData2.push([{}]);
      this.preparedData3.push([{}]);
      this.preparedData4.push([{}]);
      this.preparedData5.push([{}]);
      this.preparedData6.push([{}]);
      this.preparedData7.push([{}]);
      this.preparedData8.push([{}]);
    }
  }
  searchfit = false;
  searchingData = false;
  copyData;
  totalDataSearchedData: any = [];
  flagX=true;
  isFormDirty: boolean = false;
  getAllForSearch(searchTerm, ind, indp, indParent) {
//this.isFormDirty=true;
    this.newFood=searchTerm;
    this.flagX = false;
    if (searchTerm === "") {
      return;
    }
    
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        this.isShow[ind] = true;
        this.isShowParent[indp] = true;
        this.isShowSuperParent[indParent] = true;
        this.appService
          .searchFoodItemByName(searchTerm, error.error.text)
          .then((resp) => {
            this.isFillFood=true
            if (this.recall==='true') {
              this.totalDataSearchedData = resp["homeBased"]
                .concat(resp["packaged"])
                .concat(resp["restaurant"]);
              
            } else {
              this.totalDataSearchedData = resp["homeBased"];
            }
          });

      }
    );
  }

  async ionViewWillLeave() {
    //console.log("compare:--",this.compareDietPlanObject, this.dietPlan);
    if(!this.deepEqual(this.compareDietPlanObject,this.dietPlan)&& this.compareDietPlanObject!==undefined){
      this.isFormDirty=true;
    }
    else{
      this.isFormDirty=false;
    }
    if (this.isFormDirty) {
      const alert = await this.alertController.create({
        header: 'Diet plan modifications were not saved.',
        message: 'Would you like to go back and save your changes or continue without saving?',
        buttons: [
          {
            text: 'Go Back & Save',
            role: 'cancel',
            handler: () => {
              debugger;
              if(this.recall==="true"){
              this.router.navigate(["manage-diet"],{queryParams:{recall:'true'}});
              }
              else if(this.newTemplate==='true'){
                this.router.navigate(["manage-diet"],{queryParams:{newTemplate:'true'}});
              }
              else{
                this.router.navigate(["manage-diet"]);
              }
            },
          },
          {
            text: 'Continue Without Saving',
            handler: () => {
              debugger;
              this.compareDietPlanObject = JSON.parse(JSON.stringify(this.dietPlan));
              this.isFormDirty=false;
              // Allow navigation
            },
          },
        ],
      });
      await alert.present();
    }
  }
  isShow = new Array(10000).fill(false);
  isShowParent = new Array(9).fill(false);
  isShowSuperParent = new Array(7).fill(false);
  isFillFood = false;
  newFood = "";
  storeFoodWithIndex: any;
  newPortion = 0;
  newPortionUnit = "";
  defaultPortion = 0;
  newCarbs = 0;
  newFat = 0;
  newFiber = 0;
  newProtien = 0;
  newCalories = 0;
  checkFoodFilled=false;
  fillFood(items, ind, searchItem, indp, indParent) {
    this.newFood='';
    this.flagX=true;
    this.totalDataSearchedData=[];
    this.isFillFood = true;
    this.isShow[ind] = false;
    this.isShowParent[indp] = false;
    this.isShowSuperParent[indParent] = false;
    this.calculateCal(this.dietPlan);
    console.log(ind, indp, indParent);
    console.log(this.dietPlan[indp].diets[indParent].data[ind]);
    this.newFood = searchItem.Food;
    this.storeFoodWithIndex = searchItem;

    this.newPortion = searchItem.portion;
    this.defaultPortion = searchItem.portion;
    this.newCarbs = searchItem.Carbs;
    this.newFat = searchItem.Fat;
    this.newCalories = searchItem.Calories;
    this.newFiber = searchItem.Fiber;
    this.newProtien = searchItem.Protien;
    this.newPortionUnit = searchItem.portion_unit;
  }

  addItem(slotItem) {
 //   this.isFormDirty=true;
    if (this.inSights === "true") {
      return;
    }
    for (let index = 0; index < slotItem.data.length; index++) {
      slotItem.data[index].push({});
    }
    this.cdr.detectChanges();
  }
  removeItem(slotData, indp) {
  //  this.isFormDirty=true;
    if (this.inSights === "true") {
      return;
    }
    for (let index = 0; index < slotData.length; index++) {
      slotData[index].splice(indp, 1);
    }
    this.calculateCal(this.dietPlan);
  }
  removeAllFromSlot(slotItem, slotIndex) {
  //  this.isFormDirty=true;
    if (this.inSights === "true") {
      return;
    }
    for (let index = 0; index < slotItem.data.length; index++) {
      slotItem.data[index] = [];
    }
    for (let index = 0; index < this.dietPlan.length; index++) {
      this.dietPlan[index].diets[slotIndex].data = [];
    }
    this.calculateCal(this.dietPlan);
  }
  searchQuery: string = "";
  isSearchCleared: boolean = false;
  onClear(event: any,items,mainIndex,indp,ind,food) {
    
    this.flagX = false;
    this.newFood='';
    this.newPortion=0;
    this.newPortionUnit='';
  //  this.popoverController.dismiss();
    if (this.isSearchCleared) {
      event.target.value = this.searchQuery; // Restore the previous search query
      this.isSearchCleared = false; // Reset the flag
    } else {
      this.searchQuery = event.target.value; // Update the search query
    }
  }
  async saveItem(slot, indp, ind, itemIndex) {
    console.log(this.flagX);
    if (this.newPortion !== 0) {
      this.storeFoodWithIndex.defaultPortion = 1;
      this.storeFoodWithIndex.portion = this.newPortion;
      this.storeFoodWithIndex.portion_unit = this.newPortionUnit;
      slot.data[indp][ind] = this.storeFoodWithIndex;
      this.cdr.detectChanges();
    }
    if ((this.flagX && this.isFillFood === true && slot.data[indp][ind].portion!==0) || (this.newFood==='' && this.flagX && slot.data[indp][ind].portion!==0)) {
      slot.data[indp][ind].Calories =
        (Number(slot.data[indp][ind].Calories) /
          slot.data[indp][ind].defaultPortion) *
        slot.data[indp][ind].portion;
      slot.data[indp][ind].Carbs =
        (Number(slot.data[indp][ind].Carbs) /
          slot.data[indp][ind].defaultPortion) *
        slot.data[indp][ind].portion;
      slot.data[indp][ind].Fat =
        (Number(slot.data[indp][ind].Fat) /
          slot.data[indp][ind].defaultPortion) *
        slot.data[indp][ind].portion;
      slot.data[indp][ind].Protien =
        (Number(slot.data[indp][ind].Protien) /
          slot.data[indp][ind].defaultPortion) *
        slot.data[indp][ind].portion;

      slot.data[indp][ind].Fiber =
        (Number(slot.data[indp][ind].Fiber) /
          slot.data[indp][ind].defaultPortion) *
        slot.data[indp][ind].portion;

      // for (let index = 0; index < this.dietPlan.length; index++) {

      this.dietPlan[indp].diets[itemIndex].data = slot.data[indp];
      this.cdr.detectChanges();
      this.newFood = "";
      this.isFillFood = false;
      this.storeFoodWithIndex = "";
      this.newPortion = 0;
      this.newPortionUnit = "";
      this.popoverController.dismiss();
    } else {
      this.utilities.presentAlert("Please Select Food Item.");
    }
    this.cdr.detectChanges();
    this.calculateCal(this.dietPlan);
  }

  async copyItems(slot: any, indp, ind, itemIndex, event) {
 //   this.isFormDirty=true;
    console.log(event.detail.value, event);
    if (event.detail.checked) {
      if (this.newPortion !== 0) {
        await this.saveItem(slot, indp, ind, itemIndex);
      }
      if (this.isFillFood === false) {
        for (let index = 0; index < slot.data.length; index++) {
          if (index !== indp) {
            const data = slot.data[index].filter((item) => {
              return item._id === slot.data[indp][ind]._id;
            });
            if (data.length === 0) {
              console.log(
                "JSON.parse(JSON.stringify(slot)).data[indp][ind]",
                JSON.parse(JSON.stringify(slot)).data[indp][ind]
              );

              if (
                JSON.stringify(
                  JSON.parse(JSON.stringify(slot)).data[indp][ind]
                ) !== "{}"
              ) {
                slot.data[index][ind] = JSON.parse(JSON.stringify(slot)).data[
                  indp
                ][ind];
                console.log(index, indp, ind, itemIndex);
                slot.data[index][ind].eaten = -1;
              } else {
                event.detail.checked=false;
                this.utilities.presentAlert("Items Not exist there.");
                break;
                
              }
            }
          }
        }
        this.isFillFood = false;
      } else {
        this.utilities.presentAlert(
          "First save the item, then you can use copy all option."
        );
      }
    }
    this.popoverController.dismiss();
    this.calculateCal(this.dietPlan);
  }
  removeItemFromPopup(items, mainIndex, indp, ind) {
 //   this.isFormDirty=true;
    items.splice(ind, 1);
    items.push({});
    this.calculateCal(this.dietPlan);
    this.isFillFood = false;
    this.isShow[ind] = false;
    this.isShowParent[indp] = false;
    this.isShowSuperParent[mainIndex] = false;
    this.newFood='';
    this.newPortion=0;
    this.newPortionUnit='';
    this.popoverController.dismiss();
  }
  closePopover(slot, indp, ind, itemIndex) {
    this.newFood='';
    if (this.isFillFood === false ) {
      this.popoverController.dismiss();
    } else {
      this.utilities.presentAlert(
        "You have modified the item please save item first."
      );
    }
  }
  payload: any = { meal_plans: [] };
  template: any = { templateName: "", templateDesc: "" };
  isShowTemplate = false;
  saveTemplate() {
    this.isFormDirty=false;
    if (
      this.template.templateName === "" ||
      this.template.templateDesc === ""
    ) {
      this.utilities.presentAlert("All fields are required");
      return;
    }
    const data = {
      templateName: this.template.templateName,
      templateDescription: this.template.templateDesc,
      dietPlanData: { meal_plans: this.dietPlan },
      createdby: localStorage.getItem("loginEmail"),
      createdOn: moment(new Date()).format("dd-MMM-yyyy"),
      companyID: localStorage.getItem("companyId") || "smartdietplanner",
      isActive: true,
      isGlobal: this.isGlobal,
    };
    if (this.isEdit == "true") {
      this.appService.updateTemplate(data, this.templateId).then(
        (res) => {
          if (localStorage.getItem("dietitianId") != null) {
            // dietitianId, actionId, userId,  actionName,  companyId
            this.templateService
              .handleDietitianAction(
                localStorage.getItem("dietitianId"),
                "668d72d51073b512dd2b9528",
                localStorage.getItem("email"),
                "Create Template",
                localStorage.getItem("companyId")
              )
              .subscribe((dRespondata) => {
                this.utilities.presentAlert("Template saved successfully!");
                console.log("success", res);
                this.isShowTemplate = false;
              });
          } else {
            this.utilities.presentAlert("Template saved successfully!");
          }
        },
        (err) => {
          console.log("Error", err);
          this.isShowTemplate = false;
          this.utilities.presentAlert("Error: " + err);
        }
      );
    } else {
      // console.log("data",data);
      this.appService.addTemplate(data).then(
        (res) => {
          this.utilities.presentAlert("Template saved successfully!");
          console.log("success", res);
          this.isShowTemplate = false;
        },
        (err) => {
          this.utilities.presentAlert("Error: " + err);
          console.log("Error", err);
          this.isShowTemplate = false;
        }
      );
    }
  }
  saveRecall() {
    this.compareDietPlanObject = JSON.parse(JSON.stringify(this.dietPlan));
    const userId = localStorage.getItem("email");
    const dataDiet: any = JSON.parse(JSON.stringify(this.dietPlan));
    for (let index = 0; index < dataDiet.length; index++) {
      dataDiet[index].date = dataDiet[index].date2; //JSON.parse(JSON.stringify(this.dietPlan))[index].date2;
      //   dataDiet[index].date2 = moment(new Date().setDate(new Date().getDate()+index)).format("DDMMYYYY");
    }
    this.appService.saveDietRecall(userId, { data: dataDiet }).then(
      (res) => {
        if (localStorage.getItem("dietitianId") != null) {
          // dietitianId, actionId, userId,  actionName,  companyId
          this.templateService
            .handleDietitianAction(
              localStorage.getItem("dietitianId"),
              "668d70591073b512dd2b9155",
              localStorage.getItem("email"),
              "Save Recall",
              localStorage.getItem("companyId")
            )
            .subscribe((response) => {
              console.log("test", res);
              this.utilities.presentAlert("Record saved successfully!");
            });
        } else {
          this.utilities.presentAlert("Record saved successfully!");
        }
      },
      (err) => {
        console.log("err", err);
      }
    );
  }

  public alertButtons = ["OK"];
  public alertInputs = [
    {
      label: "Yes",
      type: "radio",
      value: true,
    },
    {
      label: "No",
      type: "radio",
      value: false,
    },
  ];

  isRepeatShow = false;

  getRepeatFutureFromService() {
    this.appService
      .isDietPlanRepeated(localStorage.getItem("email"))
      .then((res: any) => {
        this.isRepeat = res.repeatForFutureDays + "";
        if (res.repeatForFutureDays === false) {
          this.samedietmessage = "Repeat the 7-day plan for future weeks.";
        } else {
          this.samedietmessage =
            "our 7-day plan is repeating. Continue repeating.";
        }
      });
  }
  username = localStorage
  .getItem("loginEmail")
  .toLowerCase()
  .includes("beatoapp.com".toLowerCase())
  ? "beato"
  : localStorage.getItem("loginEmail");
  openRepeat() {
    if(this.username==='beato'){
      this.save()
    }
    else{
    this.isRepeatShow = true;
    }
  }
  save() {
    this.compareDietPlanObject = JSON.parse(JSON.stringify(this.dietPlan));
    this.isFormDirty=false;
    this.isRepeatShow = false;
    this.payload = { meal_plans: [], repeatForFutureDays: this.isRepeat };
    for (let index = 0; index < this.dietPlan.length; index++) {
      this.payload.meal_plans.push({
        date: moment(this.dietPlan[index].date1).format("YYYY-MM-DD"),
        slots: [],
      });
      for (let j = 0; j < this.dietPlan[index].diets.length; j++) {
        this.payload.meal_plans[index].slots.push({
          slot_id: this.dietPlan[index].diets[j].slot,
          time: this.slotItems[j].time,
          message: this.slotItems[j].slotName,
          foods: [],
        });
        for (let k = 0; k < this.dietPlan[index].diets[j].data.length; k++) {
          if (this.dietPlan[index].diets[j].data[k]?._id !== undefined) {
            this.payload.meal_plans[index].slots[j].foods.push({
              item_code: this.dietPlan[index].diets[j].data[k]._id,
              quantity: this.dietPlan[index].diets[j].data[k].portion,
            });
          }
        }
      }
    }
    console.log("payload", this.payload);
    this.appService.postManagePlan(this.payload).subscribe(
      (res) => {
        // dietitianId, actionId, userId,  actionName,  companyId
        if (localStorage.getItem("dietitianId") != null) {
          this.templateService
            .handleDietitianAction(
              localStorage.getItem("dietitianId"),
              "668d72791073b512dd2b947f",
              localStorage.getItem("email"),
              "Save Diet Plan",
              localStorage.getItem("companyId")
            )
            .subscribe((dRespondata) => {
              this.utilities.presentAlert("Record saved successfully!");
              console.log("res", res);
            });
        } else {
          this.utilities.presentAlert("Record saved successfully!");
        }
      },
      (err) => {
        console.log("err", err);
      }
    );
    console.log("payload", this.payload);
  }
  async presentPopover(event: Event) {
    const popover = await this.popoverController.create({
      component: "",
      event: event,
      translucent: true,
    });
    return await popover.present();
  }
}
