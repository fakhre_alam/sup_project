import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ManageDietPage } from './manage-diet.page';

const routes: Routes = [
  {
    path: '',
    component: ManageDietPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ManageDietPageRoutingModule {}
