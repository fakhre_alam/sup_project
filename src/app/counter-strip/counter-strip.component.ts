import { Component, Input, OnInit } from '@angular/core';
import { TemplateService } from '../services';

@Component({
  selector: 'app-counter-strip',
  templateUrl: './counter-strip.component.html',
  styleUrls: ['./counter-strip.component.scss'],
})
export class CounterStripComponent implements OnInit {
  @Input() count: number;
  @Input() menuName: any;
  isOpen = false;
  hasExpired: boolean = true;
  dietitianEmail: string = localStorage.getItem("loginEmail");
  planExpiry: string;

  constructor(
    private templateService: TemplateService,
  ) { }
  ngAfterViewInit() {
    if (localStorage.getItem("menu").length > 10) {
      this.menuName = JSON.parse(localStorage.getItem("menu"));
    } else {
      localStorage.setItem(
        "menu",
        `{
        "title":"Users Summary",
        "description":[{"name":"This section displays a summary of the latest 20 contacts","desc":[]},{"name":"It has actionable buttons for WhatsApp, scheduling calls, or viewing diet plan details.","desc":[]}],
        "menu":"Home",
        "subMenu":"",
        "activeIndex":0   
    }`
      );
      this.menuName = JSON.parse(localStorage.getItem("menu"));
    }
  }
  openPopup() {
    this.isOpen = !this.isOpen;
  }
  ngOnInit() {
    console.log("DieticianName >> ", localStorage.getItem("dietitianName"));
    console.log("Runningf")
    const query = `mail=${this.dietitianEmail}`;

    this.templateService.getDietitianRecord(query).subscribe(
      (res) => {
        console.log("Can AddResult ::--> ", res);
        this.count = 5 - res["noOfUsersCanBeAdded"];
        // planExpiryDate
        console.log("CCount :: ", this.count)
        if (res["planExpiryDate"]) {
          const expiryDate = new Date(res["planExpiryDate"]);

          console.log(":::  -----  line 32 -----::: ", expiryDate);
          // Get today's date (without time component)
          const today = new Date();
          today.setHours(0, 0, 0, 0); // Set the time to 00:00 to only compare the date

          console.log("Expiry date:: ", expiryDate);

          // Compare expiryDate with today
          this.hasExpired = expiryDate < today;
          this.planExpiry = expiryDate.toISOString().split("T")[0];
        } else {
          this.hasExpired = true;
        }

        console.log("Has Expired ::?? ", this.hasExpired);
      },
      (err) => {
        console.log("error", err);
      }
    );
  }


}
