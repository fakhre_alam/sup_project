import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { AppService } from "../app.service";
import { LoaderService, TemplateService } from "../services";

@Component({
  selector: "app-authenticate",
  templateUrl: "./authenticate.page.html",
  styleUrls: ["./authenticate.page.scss"],
})
export class AuthenticatePage implements OnInit {
  constructor(
    private router: Router,
    private appService: AppService,
    private route: ActivatedRoute,
        private templateService: TemplateService,
    
    private loaderService: LoaderService
  ) {}
  userId: string = "";
  dietitianId: string = "";
  key: string = "";
  isDisabled: boolean = false;

  ngOnInit(): void { }

  ionViewWillEnter() {
    this.route.queryParamMap.subscribe((params)=> {
      this.userId = params.get("id");
      this.dietitianId = params.get("dId");
      this.key = params.get("key");

      const payload = 
      {
        dietitianId: this.dietitianId,
        key: this.key
      }

      this.loaderService.presentLoader("Validating...").then((_) => {
        this.templateService.getAuthenticate(payload).subscribe((response: Object)=>{
          // alert(JSON.stringify(response));
          this.loaderService.hideLoader();

          if(!response["verified"]){
            alert(response["data"]);
            return;
          }
          localStorage.setItem("isIndividual", response["isIndividual"]);

          if(response["isIndividual"] && response["isDisabled"]){
            this.isDisabled = response["isDisabled"];
            this.loaderService.hideLoader();
            return;
          }

          localStorage.setItem("acess_token", response["token"]["access_token"]);
          localStorage.setItem("loginEmail", response["data"]["emailId"]);

          localStorage.setItem("email", this.userId);

          localStorage.setItem("dietitianId", response["data"]["_id"]);
          localStorage.setItem("dietitianName", response["data"]["name"]);
          localStorage.setItem("companyId", response["companyId"]);
          localStorage.setItem("companyKey", response["companyKey"]);
          localStorage.setItem("urlClientId", response["URLClientId"]);
          localStorage.setItem(
            "menu",
            `{
              "title":"Users Summary",
              "description":[{"name":"This section displays a summary of the latest 20 contacts","desc":[]},{"name":"It has actionable buttons for WhatsApp, scheduling calls, or viewing diet plan details.","desc":[]}],
              "menu":"Home",
              "subMenu":"",
              "activeIndex":0   
            }`
          );
          
          this.router.navigate(["dashboard"]);

        },
        (err) => {
          console.log("Error Response>> ", err);
          this.loaderService.hideLoader();
        });
      });
    });
  }

  /*
  ngOnInit() {
    this.loaderService.presentLoader("Validating data...").then((_) => {
      this.loaderService.hideLoader();
      this.route.queryParams.subscribe(
        (params) => {
          // const companyId = params["companyId"];
          const token = params["token"];
          // const companyKey = params["companyKey"];
          const mail = params["mail"];

          if (mail && token) {
            this.appService
              .externalRegistration("SDP2021", {
                profile: {
                  email: mail,
                },
              })
              .subscribe(
                (res) => {
                  console.log("Response logs 21212 ::>> ", res);
                  localStorage.setItem("acess_token", res.acess_token);

                  this.loaderService.hideLoader();
                  // Call your authentication service
                  // alert(companyId + " | " + mail + " | " + token + " | " + companyKey);
                  // localStorage.setItem("companyId", companyId);
                  localStorage.setItem("loginEmail", mail);
                  // localStorage.setItem("companyKey", companyKey);
                  // localStorage.setItem("dietitianId", dieticianId);
                  // localStorage.setItem("dietitianName", response["name"]);
                  // localStorage.setItem("companyId", response["companyId"]);
                  // this.authenticateUser(companyId, dieticianId, token);
                  this.router.navigate(["first-page"]);
                },
                (error) => {
                  console.log("Error::>> ", error);
                  this.loaderService.hideLoader();
                  this.router.navigate(["login"]);
                }
              );
          } else {
            // Handle missing parameters
            console.error("Missing query parameters");
            this.router.navigate(["login"]);
          }
        },
        (error) => {
          console.log("Error::>> ", error);
          this.loaderService.hideLoader();
          this.router.navigate(["login"]);
        }
      );
    });
  }
  

  authenticateUser(
    companyId: string,
    dieticianId: string,
    token: string
  ): void {
    console.log(
      "\n ------------------- \n   --------------------\n CompanyID: ",
      companyId,
      " || dieticianId: ",
      dieticianId,
      " || token: ",
      token
    );
    // Call your authentication service and handle the response
    // For example:
    // this.authService.authenticate(companyId, dieticianId, token).subscribe(
    //   (response) => {
    //     // Handle successful authentication
    //     console.log("User authenticated successfully", response);
    //     // Redirect to the appropriate page or feature the user
    //     this.router.navigate(["/home"]);
    //   },
    //   (error) => {
    //     // Handle authentication error
    //     console.error("Authentication failed", error);
    //   }
    // );
  }
  */
}
