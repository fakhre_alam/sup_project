import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { AppService } from "src/app/app.service";
import { AppService as apS, TemplateService } from "../services";
@Component({
  selector: 'app-new-food-detail',
  templateUrl: './new-food-detail.page.html',
  styleUrls: ['./new-food-detail.page.scss'],
})
export class NewFoodDetailPage implements OnInit {
  videoUrl: any;
  streamVideo = false;
  @Input() disabled: Boolean = false;
  @Output() isdisplayFooter=new EventEmitter<boolean>();
  searchfit = false;
  searchingData = false;
  copyData;
  totalDataSearchedData: any = [];
  flagX=true;
  newFood = "";
  isShow=false;
  popup={steps:'',recipe:'',Calories:0,portion:0};
  constructor(private _sanitizer: DomSanitizer, private aps: apS, private appService: AppService) { }

  ngOnInit() {
  }
  onClear() {
   this.flagX = false;
    this.newFood='';
  }
  fillFood(items) {
    this.newFood='';
    this.flagX=true;
    this.isShow=true;
    this.totalDataSearchedData=[];
   
   
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        const reqBody ={
          country: "",
          foodId: items._id
        }
    this.appService.fetchFoodById(reqBody,error.error.text).then((res:any)=>{
      console.log(res);
      debugger;
      
      this.popup = res.dietItem;
      this.gotoView(this.popup);
      console.log("selected Items:-", this.popup);
    });
      });
    
  }
  getAllForSearch(searchTerm) {
    this.newFood=searchTerm;
    this.flagX = false;
    if (searchTerm === "") {
      return;
    }
    
    this.aps.getUserToken(localStorage.getItem("email")).subscribe(
      (response) => {},
      (error) => {
        console.log("User token ", error.error.text);
        this.appService
          .searchFoodItemByName(searchTerm, error.error.text)
          .then((resp) => {
         //   this.isShow=true;
             this.totalDataSearchedData = resp["homeBased"]
                .concat(resp["packaged"])
                .concat(resp["restaurant"]);
            // } else {
            //   this.totalDataSearchedData = resp["homeBased"];
            // }
          });
      }
    );
  }
  videoClick(data) {
    this.videoUrl = this._sanitizer.bypassSecurityTrustResourceUrl(data);
    this.streamVideo = true;
    this.isShow = false;
    
  }
  closeVideo(){
     this.isShow = true;
    this.streamVideo = false;
   
  }
  senitizedData(videoUrl) {
    this.videoUrl = this._sanitizer.bypassSecurityTrustResourceUrl(videoUrl);
   
  }

  instructionsArray=[];
  gredientArray=[];
  wrapFirstWordWithBold(text) {
    // Using regular expression to match the first word followed by ":"
    let replacedText = text.includes(':')?text.replace(/^(.*?):\s*(.*)$/, "<span class='custom-style1'>$1</span> <span class='custom-style2'>$2</span>"):"<span class='custom-style1'>"+text+"</span><br>";
    return replacedText;
}
  async gotoView(d) {
    if (this.disabled) {
      return;
    }
    console.log("d",d);
    this.isdisplayFooter.emit(false);
    if (!d.recipe && d?.video?.includes("http")) {
      d.recipe = "As per video";
    }
    if (!d.recipe && !d?.video?.includes("http")) {
      d.recipe = "--";
    }

    if (!d.steps && d?.video?.includes("http")) {
      d.steps = "As per video";
    }
    if (!d.steps && !d?.video?.includes("http")) {
      d.steps = "--";
    }
   this.popup = d; 
   this.senitizedData(d.video);
   this.gredientArray = this.popup?.recipe.replace(/\:+/g, ":<br>").split("\n").filter(item => item.trim() !== '');

   for (let index = 0; index < this.gredientArray.length; index++) {
    this.gredientArray[index] = this.wrapFirstWordWithBold(this.gredientArray[index]);
    
   }
   console.log("this.gredientArray",this.gredientArray);

   this.instructionsArray = this.popup?.steps.replace("\n\n","\n")
   .replace("Step 1","1.")
   .replace("Step 2","2.")
   .replace("Step 3","3.")
   .replace("Step 4","4.")
   .replace("Step 5","5.")
   .replace("Step 6","6.")
   .replace("Step 7","7.")
   .replace("Step 8","8.")
   .replace("Step 9","9.")
   .replace("Step 10","10.")
   .replace("Step 11","11.")
   .replace("Step 12","12.")
   .replace("Step 13","13.")
   .replace("Step 14","14.")
   .replace("Step 15","15.")
   .replace("Step 16","16.").split(/\d+\./).filter(item => item.trim() !== '');
    this.isShow=true;
    
  }

  close(){
    this.isShow=false;
  }
  addRemove(type) {
    this.isdisplayFooter.emit(true);
    let calCount = this.popup?.Calories / this.popup.portion;
    if (type === "add") {
      this.popup.portion = Number(this.popup?.portion || 0) + 0.5;
    } else {
      if (Number(this.popup?.portion) !== 0.5) {
        this.popup.portion = Number(this.popup?.portion || 0) - 0.5;
        // this.data.portion = 0;
      }
    }
    this.popup.Calories = calCount * this.popup.portion;
  }
}
