import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewFoodDetailPage } from './new-food-detail.page';

const routes: Routes = [
  {
    path: '',
    component: NewFoodDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewFoodDetailPageRoutingModule {}
