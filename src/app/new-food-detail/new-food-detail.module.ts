import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewFoodDetailPageRoutingModule } from './new-food-detail-routing.module';

import { NewFoodDetailPage } from './new-food-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewFoodDetailPageRoutingModule
  ],
  declarations: [NewFoodDetailPage]
})
export class NewFoodDetailPageModule {}
