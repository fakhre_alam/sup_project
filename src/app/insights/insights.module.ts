import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InsightsPageRoutingModule } from './insights-routing.module';

import { InsightsPage } from './insights.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InsightsPageRoutingModule
  ],
  declarations: [InsightsPage],
  schemas:[NO_ERRORS_SCHEMA]
})
export class InsightsPageModule {}
