import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-demo-video",
  templateUrl: "./demo-video.page.html",
  styleUrls: ["./demo-video.page.scss"],
})
export class DemoVideoPage implements OnInit {
  contentArr: any = [
    {
      title: "Series 1: Introduction!",
      link: "https://youtu.be/bjA-68yfMgw?si=KH09QWOZp_93rvJ6",
      subHeading:
        "Master the dietitian module's core features and learn to create and manage your profile effortlessly.",
    },
    {
      title: "Series 2: Managing Customers",
      link: "https://youtu.be/X4ZWdDxczLQ?si=n4_9PUtwOUSn5GMZ",

      subHeading:
        "Seamlessly add clients, capture diet recalls, record Q&A sessions, and log health parameters with ease.",
    },
    {
      title: "Series 3: Creating Diet Plans",
      link: "https://youtu.be/5p3yRseQxwM?si=GJg8lqPGiCbzBfki",
      subHeading:
        "Explore AI-powered diet planning, customize existing plans, create new ones, and manage diet templates effectively.",
    },
    {
      title: "Series 4: Customer Delight",
      link: "https://youtu.be/R_T6Vsgvhb0?si=VEUcpDNF1oXDY-W7",
      subHeading:
        "Delight clients with personalized diet plans, recipes, and macro tracking. Share plans and communicate effortlessly.",
    },
    {
      title: "Series 5: Customer Insights",
      link: "https://youtu.be/w3VfjxuDvcQ?si=CfD9hIcqlpvWt5nH",
      subHeading:
        "Track clients' food intake, monitor portion sizes, and provide tailored health guidance for optimal results",
    },
  ];

  constructor() {}

  ngOnInit() {}

  clickHandler(link: string){
    window.open(link, "_blank");
  }
}
