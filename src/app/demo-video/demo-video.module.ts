import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DemoVideoPageRoutingModule } from './demo-video-routing.module';

import { DemoVideoPage } from './demo-video.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DemoVideoPageRoutingModule
  ],
  declarations: [DemoVideoPage]
})
export class DemoVideoPageModule {}
