import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoVideoPage } from './demo-video.page';

const routes: Routes = [
  {
    path: '',
    component: DemoVideoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DemoVideoPageRoutingModule {}
