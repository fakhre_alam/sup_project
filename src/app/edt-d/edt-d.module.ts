import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EdtDPageRoutingModule } from './edt-d-routing.module';

import { EdtDPage } from './edt-d.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EdtDPageRoutingModule
  ],
  declarations: [EdtDPage]
})
export class EdtDPageModule {}
