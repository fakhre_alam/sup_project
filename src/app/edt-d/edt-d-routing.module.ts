import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EdtDPage } from './edt-d.page';

const routes: Routes = [
  {
    path: '',
    component: EdtDPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EdtDPageRoutingModule {}
