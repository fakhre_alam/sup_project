import { Component, OnInit } from "@angular/core";
import { AppService } from "../app.service";
import { UTILITIES } from "../core/utility/utilities";
import { AlertController } from "@ionic/angular";
import { Router } from "@angular/router";
import { LoaderService, TemplateService } from "../services";
import { JsonPipe } from "@angular/common";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { APIS } from "../shared/constants";

@Component({
  selector: "app-edt-d",
  templateUrl: "./edt-d.page.html",
  styleUrls: ["./edt-d.page.scss"],
})
export class EdtDPage implements OnInit {
  name: string = "";
  email: string = localStorage.getItem("loginEmail");
  rolesOptions: any = ["Dietitian", "Nutritionist", "Fitness Coach", "Doctor"];
  rolesSelected: string = null;
  mobileNumber: string = "";
  linkedinId: string = "";
  wpNumber: string = "";
  wpVisible: boolean = false;
  isIndividual = localStorage.getItem("isIndividual");
  calendlyLink: string = "";
  calendlyVisible: boolean = false;
  about: string =
    "I am a dietitian with a strong background in nutrition and personalized health management. I specialize in creating tailored diet plans to help individuals reach their health goals.\nWith expertise in weight loss and fitness, I am dedicated to promoting long-term wellness through well-researched, science-based nutrition strategies";
  speciality: string = "Weight Loss, Fitness, Diabetes, PCOD";
  responseResult: any;
  gender: string = "";
  selectedFile: File | null = null;
  selectedImage: string | undefined;
  themeSelected: string = "individual3";
  // imageBase64: string = null;
  base64String: string = null;
  URLClientIdVal: string = "alyve";
  profileExists: boolean = false;
  dietitianId: string = localStorage.getItem("dietitianId");
  disabled: boolean = false;

  isNewUser: boolean = false;
  currentUrl = this.router.url;

  constructor(
    private appS: AppService,
    private appServe: AppService,
    private appservice: AppService,
    private utilities: UTILITIES,
    private alertController: AlertController,
    private router: Router,
    private templateService: TemplateService,
    private loaderService: LoaderService,
    private httpClient: HttpClient
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    const query = `mail=${this.email}`;
    this.loaderService.presentLoader("Please Wait").then((_) => {
      this.templateService.getDietitianRecord(query).subscribe(
        (response) => {
          // about
          this.responseResult = response;
          if (response != null) {
            // alert(this.dietitianId);
            if (this.currentUrl==='/first-page') {
              this.dietitianId = response["_id"];
              localStorage.setItem("loginEmail", response["emailId"]);
              localStorage.setItem("dietitianId", response["_id"]);
              localStorage.setItem("dietitianName", response["name"]);
              localStorage.setItem("companyId", response["companyId"]);
              localStorage.setItem("companyKey", response["companyKey"]);
              if (this.isIndividual && this.isIndividual === "true") {
                localStorage.setItem("urlClientId", response["URLClientId"]);
                localStorage.setItem(
                  "searchAllowed",
                  response["searchAllowed"]
                );
              } else {
                localStorage.setItem("searchAllowed", "true");
                localStorage.setItem("urlClientId", response["urlClientId"]);
              }
              localStorage.setItem(
                "menu",
                `{
                  "title":"Users Summary",
                  "description":[{"name":"This section displays a summary of the latest 20 contacts","desc":[]},{"name":"It has actionable buttons for WhatsApp, scheduling calls, or viewing diet plan details.","desc":[]}],
                  "menu":"Home",
                  "subMenu":"",
                  "activeIndex":0   
                }`
              );
              this.profileExists = true;
              if (response["isDisabled"]) {
                this.disabled = true;
              } else {
                this.router.navigate(["dashboard"]);
              }
            }
            
            this.calendlyVisible = response["calendlyVisible"] || false;
            this.calendlyLink =
              response["calendlyId"] === "null" ? "" : response["calendlyId"];
            this.name = response["name"];
            this.mobileNumber = response["mobileNumber"];
            this.linkedinId = response["linkedinId"];
            this.gender = response["gender"] || "";
            this.rolesSelected =
              response["role"] === "null" ? "" : response["role"];
            this.speciality =
              response["speciality"] === "null" ? "" : response["speciality"];
            this.wpNumber =
              response["whatsappNum"] === "null" ? "" : response["whatsappNum"];
            this.wpVisible = response["whatsappVisible"] || false;
            this.about = response["about"] === "null" ? "" : response["about"];

            if (response["theme"]) {
              this.themeSelected = response["theme"];
            }

            if (response["image"]) {
              const file = response["image"];

              console.log(" ---- File Image ---+++>> ", file);
              // this.imageBase64 = `data:${data.imageType};base64,${data.image}`;
              // this.selectedImage = response["Image"]; // Convert blob to base64 image URL

              if (file) {
                // https://nodeapi.smartdietplanner.com/api/getResFromPath/uploads/1725988948823-675168050.png
                this.base64String = `https://nodeapi.smartdietplanner.com/api/getResFromPath/${file}`;

                // this.base64String = `http://localhost:8080/api/getResFromPath/${file}`
              }
              // Convert the array to a Base64 string
              // this.base64String = btoa(String.fromCharCode(...uint8Array));
            }

            console.log("Response>> ", response);

          } else {
            this.isNewUser = true;
          }

          this.loaderService.hideLoader();
        },
        (err) => {
          console.log("Error Response>> ", err);
          const errMsg = err?.error?.error;

          if (errMsg === "Dietitian Record Not Found") {
            this.isNewUser = true;
          }
          this.loaderService.hideLoader();
        }
      );
    });
  }

  alertConfirmClicked(): void {
    if (this.name === "") {
      this.utilities.presentAlert("All Fields are required");
    } else {
      // Destructure the Activites properties

      this.responseResult["calendlyVisible"] = this.calendlyVisible;
      this.responseResult["calendlyId"] = this.calendlyLink;
      this.responseResult["name"] = this.name;
      this.responseResult["role"] = this.rolesSelected;
      this.responseResult["gender"] = this.gender;
      this.responseResult["speciality"] = this.speciality;
      this.responseResult["whatsappNum"] = this.wpNumber;
      this.responseResult["whatsappVisible"] = this.wpVisible;
      this.responseResult["mobileNumber"] = this.mobileNumber;
      this.responseResult["linkedinId"] = this.linkedinId;
      this.responseResult["about"] = this.about;
      // Theme
      if (this.themeSelected !== null && this.themeSelected !== "") {
        // body += `--${boundary}\r\n`;
        // body += 'Content-Disposition: form-data; name="theme"\r\n\r\n';
        // body += `${this.themeSelected}\r\n`;
        console.log("Theme Selected:: .", this.themeSelected);
        this.responseResult["theme"] = this.themeSelected;
        this.responseResult["URLClientId"] = this.themeSelected;
        localStorage.setItem("urlClientId", this.themeSelected);
      }

      const formData = new FormData();

      // Append all key-value pairs from the userProfile object
      for (const key in this.responseResult) {
        if (this.responseResult.hasOwnProperty(key)) {
          formData.append(key, this.responseResult[key]);
        }
      }

      console.log("Payload ::-+->> ", this.responseResult);

      console.log("Selected Filed:::>> >", this.selectedFile);

      if (this.selectedFile) {
        formData.append("image", this.selectedFile, this.selectedFile.name);
      }

      this.httpClient
        .post(APIS.nodeBaseUrl + `dietitian/updateDietitianRecord`, formData)
        .subscribe(
          (res) => {
            localStorage.setItem("dietitianName", this.name);
            console.log("Result >> ", res);

            // if (localStorage.getItem("dietitianId") != null) {
            //   this.templateService
            //     .handleDietitianAction(
            //       localStorage.getItem("dietitianId"),
            //       "66a34e015322b972c017fa2a",
            //       localStorage.getItem("email"),
            //       "Edit Profile",
            //       localStorage.getItem("companyId")
            //     )
            //     .subscribe((dRespondata) => {
            //       console.log("success", res);
            this.utilities.presentAlert("Profile Updated Successfully!");
            window.location.reload();
          },
          (error) => {
            console.log("User token ", error.error.text);
          }
        );
    }
  }

  submitBtnHandler() {
    this.loaderService.presentLoader("Saving details...").then((_) => {
      const payload = {
        name: this.name,
        emailId: localStorage.getItem("loginEmail"),
        calendlyId: this.calendlyLink,
        whatsappNum: this.wpNumber,
        role: this.rolesSelected,
        speciality: this.speciality,
        about: this.about,
        whatsappVisible: this.wpVisible,
        calendlyVisible: this.calendlyVisible,
        gender: this.gender,
      };

      if (this.isIndividual && this.isIndividual === "true") {
        // payload["searchAllowed"] = this.searchAllowed;
        payload["URLClientId"] = this.themeSelected
          ? this.themeSelected
          : this.URLClientIdVal;
        payload["theme"] = this.themeSelected;
        // localStorage.setItem(
        //   "searchAllowed",
        //   this.searchAllowed ? "true" : "false"
        // );
      } else {
        localStorage.setItem("searchAllowed", "true");
      }

      console.log("Payload>> ", payload);

      const formData = new FormData();
      for (const key in payload) {
        if (payload.hasOwnProperty(key)) {
          formData.append(key, payload[key]);
        }
      }

      if (this.selectedFile) {
        formData.append("image", this.selectedFile, this.selectedFile.name);
      }

      if (this.profileExists) {
        formData.append("_id", this.dietitianId);
        this.templateService.updateDietitian(formData).subscribe(
          (response) => {
            this.loaderService.hideLoader();
            console.log(":: Fetch Dietician Record ", response);
            if (response != null) {
              localStorage.setItem("dietitianId", response["_id"]);
              localStorage.setItem("dietitianName", response["name"]);
              localStorage.setItem("companyId", response["companyId"]);
              localStorage.setItem("companyKey", response["companyKey"]);
              localStorage.setItem("urlClientId", response["urlClientId"]);
              localStorage.setItem(
                "menu",
                `{
              "title":"Users Summary",
              "description":[{"name":"This section displays a summary of the latest 20 contacts","desc":[]},{"name":"It has actionable buttons for WhatsApp, scheduling calls, or viewing diet plan details.","desc":[]}],
              "menu":"Home",
              "subMenu":"",
              "activeIndex":0   
          }`
              );
              this.router.navigate(["dashboard"]);
            }
          },
          (error) => {
            console.log("User token ", error.error.text);
          }
        );
      } else {
        this.templateService.createDietitian(formData).subscribe(
          (response) => {
            this.loaderService.hideLoader();
            console.log(":: Fetch Dietician Record ", response);
            if (response != null) {
              localStorage.setItem("dietitianId", response["_id"]);
              localStorage.setItem("dietitianName", response["name"]);
              localStorage.setItem("companyId", response["companyId"]);
              localStorage.setItem("companyKey", response["companyKey"]);
              localStorage.setItem("urlClientId", response["urlClientId"]);
              localStorage.setItem(
                "menu",
                `{
              "title":"Users Summary",
              "description":[{"name":"This section displays a summary of the latest 20 contacts","desc":[]},{"name":"It has actionable buttons for WhatsApp, scheduling calls, or viewing diet plan details.","desc":[]}],
              "menu":"Home",
              "subMenu":"",
              "activeIndex":0   
          }`
              );
              this.router.navigate(["dashboard"]);
            }
          },
          (err) => {
            console.log("Error Response>> ", err);
            this.loaderService.hideLoader();
          }
        );
      }
    });
  }

  submitBtnHandler2() {
    this.loaderService.presentLoader("Saving details...").then((_) => {
      const payload = {
        name: this.name,
        emailId: localStorage.getItem("loginEmail"),
        calendlyId: this.calendlyLink,
        whatsappNum: this.wpNumber,
        mobileNumber: this.mobileNumber,
        role: this.rolesSelected,
        speciality: this.speciality,
        linkedinId: this.linkedinId,
        about: this.about,
        whatsappVisible: this.wpVisible,
        calendlyVisible: this.calendlyVisible,
        gender: this.gender,
      };

      payload["URLClientId"] = this.themeSelected
        ? this.themeSelected
        : this.URLClientIdVal;
      payload["theme"] = this.themeSelected;
      // localStorage.setItem(
      //   "searchAllowed",
      //   this.searchAllowed ? "true" : "false"
      // );

      console.log("Payload>> ", payload);

      const formData = new FormData();
      for (const key in payload) {
        if (payload.hasOwnProperty(key)) {
          formData.append(key, payload[key]);
        }
      }

      if (this.selectedFile) {
        formData.append("image", this.selectedFile, this.selectedFile.name);
      }

      this.templateService.createIndividualDietitian(formData).subscribe(
        (response) => {
          this.loaderService.hideLoader();
          console.log(":: Fetch Dietician Record ", response);
          if (response != null) {
            localStorage.setItem("dietitianId", response["_id"]);
            localStorage.setItem("dietitianName", response["name"]);
            localStorage.setItem("companyId", response["companyId"]);
            localStorage.setItem("companyKey", response["companyKey"]);
            localStorage.setItem("urlClientId", response["URLClientId"]);
            // alert(response["URLClientId"]);
            localStorage.setItem(
              "menu",
              `{
              "title":"Users Summary",
              "description":[{"name":"This section displays a summary of the latest 20 contacts","desc":[]},{"name":"It has actionable buttons for WhatsApp, scheduling calls, or viewing diet plan details.","desc":[]}],
              "menu":"Home",
              "subMenu":"",
              "activeIndex":0   
          }`
            );
            this.router.navigate(["dashboard"]);
          }
        },
        (err) => {
          console.log("Error Response>> ", err);
          this.loaderService.hideLoader();
        }
      );
    });
  }

  updateProfile() {
    // this.presentAlertConfirm();
    // this.utilities.presentAlertConfirmBox(
    //   "Update will delete the current date and future date Diet plans. Please confirm to continue.",
    //   () => this.alertConfirmClicked()
    // );
    this.alertConfirmClicked();
    // this.utilities.presentAlertConfirm("Update will delete the current date and future date Diet plans. Please conform to continue.", () => this.updateProfile());
    // alert("Clicked");
    console.log("Diet palna>>>");
  }

  onChangeHandler(num) {
    if (this.calendlyLink === "") this.calendlyVisible = false;
    if (this.wpNumber === "") this.wpVisible = false;
    // alert("hanges")
  }

  // Function to handle the file selection
  onFileSelected(event: any) {
    const file: File = event.target.files[0];

    if (file) {
      this.selectedFile = file;

      // Optional: Display the selected image by creating a local URL
      const reader = new FileReader();
      reader.onload = (e: any) => {
        this.selectedImage = e.target.result;
      };
      reader.readAsDataURL(file);
    }
  }

  printSelectedTheme(event) {
    console.log("Event  >> ", event);
  }

  logout() {
    localStorage.clear();
    localStorage.setItem("email", "");
    localStorage.setItem("femail", "No");
    // this.search = "";
    this.router.navigate(["login"]);
    // this.searchData = "";
    this.name = "";
  }
}
