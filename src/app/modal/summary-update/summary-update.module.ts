import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SummaryUpdatePageRoutingModule } from './summary-update-routing.module';

import { SummaryUpdatePage } from './summary-update.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SummaryUpdatePageRoutingModule
  ],
  declarations: [SummaryUpdatePage]
})
export class SummaryUpdatePageModule {}
