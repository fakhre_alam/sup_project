import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SummaryUpdatePage } from './summary-update.page';

const routes: Routes = [
  {
    path: '',
    component: SummaryUpdatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SummaryUpdatePageRoutingModule {}
