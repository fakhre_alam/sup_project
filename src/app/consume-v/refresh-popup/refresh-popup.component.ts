import { Component, OnInit, ViewChild } from '@angular/core';
import { IonDatetime, ModalController } from '@ionic/angular';
import moment from 'moment';
import { AppService } from 'src/app/app.service';
import { CONSTANTS } from 'src/app/core/constants/constants';

@Component({
  selector: 'app-refresh-popup',
  templateUrl: './refresh-popup.component.html',
  styleUrls: ['./refresh-popup.component.scss'],
})
export class RefreshPopupComponent implements OnInit {

  fromDietPlanDate = new Date();
  toDietPlanDate = new Date();
  current_year = moment().subtract(0, 'months').format('YYYY-MM-DD');
  max_year: any =moment().add(1, 'months').format('YYYY-MM-DD');
  @ViewChild(IonDatetime, { static: true }) datetime: IonDatetime;
  @ViewChild(IonDatetime, { static: true }) datetimeTo: IonDatetime;
  ranges=[];
  selectedRange=0;
  constructor(public modalController: ModalController,private readonly appService:AppService) {
    for (let index = 1; index <= 30; index++) {
      this.ranges[index-1]=index;
    }
   }

  close(){
  this.modalController.dismiss();
  }

  ngOnInit() {}
  refreshDietPlan(){
    const date = moment(CONSTANTS.dietDate).format('DDMMYYYY');
    console.log("selectedRange",CONSTANTS.dietDate,date);
    this.appService.getRefreshDietPlan(date).then(res=>{
      console.log("res",res);
      this.modalController.dismiss();
      
    },err=>{
      console.log("err",err);
      
    });
  }
  formatDate(value: string) {
    return moment(value).format("DD-MMM-YYYY");
  }
}
