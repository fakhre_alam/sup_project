import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConsumeVPage } from './consume-v.page';

describe('ConsumeVPage', () => {
  let component: ConsumeVPage;
  let fixture: ComponentFixture<ConsumeVPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsumeVPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ConsumeVPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
