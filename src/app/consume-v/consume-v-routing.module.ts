import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConsumeVPage } from './consume-v.page';

const routes: Routes = [
  {
    path: '',
    component: ConsumeVPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsumeVPageRoutingModule {}
