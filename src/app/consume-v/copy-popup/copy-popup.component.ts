import { Component, OnInit, ViewChild } from '@angular/core';
import { IonDatetime, ModalController } from '@ionic/angular';
import moment from 'moment';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-copy-popup',
  templateUrl: './copy-popup.component.html',
  styleUrls: ['./copy-popup.component.scss'],
})
export class CopyPopupComponent implements OnInit {

  fromDietPlanDate = new Date();
  toDietPlanDate = new Date();
  current_year = moment().subtract(0, 'months').format('YYYY-MM-DD');
  max_year: any =moment().add(1, 'months').format('YYYY-MM-DD');
  @ViewChild(IonDatetime, { static: true }) datetime: IonDatetime;
  @ViewChild(IonDatetime, { static: true }) datetimeTo: IonDatetime;
  ranges=[];
  selectedRange=0;
  constructor(public modalController: ModalController,private readonly appService:AppService) {
    for (let index = 1; index <= 30; index++) {
      this.ranges[index-1]=index;
    }
   }

  close(){
  this.modalController.dismiss();
  }

  ngOnInit() {}
  copyDietPlan(){
    const fromdate = moment(this.fromDietPlanDate).format('DDMMYYYY');
    const toDate = moment(this.fromDietPlanDate).add(this.selectedRange,'days').format('DDMMYYYY');
    console.log("selectedRange",this.selectedRange,fromdate,toDate);
    this.appService.postCopyDietPlan("",fromdate,toDate,this.selectedRange).then(res=>{
      console.log("res",res);
      this.modalController.dismiss();
      
    },err=>{
      console.log("err",err);
      
    });
  }
  formatDate(value: string) {
    return moment(value).format("DD-MMM-YYYY");
  }
}
