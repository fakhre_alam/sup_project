import { ComponentsModule } from "./../components/components.module";
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { IonicModule } from "@ionic/angular";
import { ConsumeVPageRoutingModule } from "./consume-v-routing.module";
import { ConsumeVPage } from "./consume-v.page";
import { NgCircleProgressModule } from "ng-circle-progress";
import { CopyPopupComponent } from "./copy-popup/copy-popup.component";
import { RefreshPopupComponent } from "./refresh-popup/refresh-popup.component";
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConsumeVPageRoutingModule,
    ComponentsModule,
    NgCircleProgressModule.forRoot({
      animation: false,
      radius: 49,
      innerStrokeWidth: 6,
      outerStrokeWidth: 6,
      space: -6,
      responsive: false,
      showTitle: true,
      titleFontSize: "15",
      subtitleFontSize: "15",
      unitsFontSize: "15",
      renderOnClick: false,
    }),
  ],
  declarations: [
    ConsumeVPage,
    CopyPopupComponent,
    RefreshPopupComponent,
   
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ConsumeVPageModule {}
