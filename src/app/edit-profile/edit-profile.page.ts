import { Component, OnInit } from "@angular/core";
import { AppService as appS } from "../app.service";
import { AppService as appSer } from "../../app/app.service";
import { AppService } from "../services/app/app.service";
import { UTILITIES } from "../core/utility/utilities";
import { AlertController } from "@ionic/angular";
import { Router } from "@angular/router";
import { LoaderService, TemplateService } from "../services";

@Component({
  selector: "app-edit-profile",
  templateUrl: "./edit-profile.page.html",
  styleUrls: ["./edit-profile.page.scss"],
})
export class EditProfilePage implements OnInit {
  name: string = "";
  age: any;
  gender: any;
  height: any;
  heightInCms: boolean = true;
  selectedHeight = "";
  weight: any;
  desiredWeight: number;
  searchData: any = "";
  lifeStyle: any;
  profileData: any = {};
  activityChoosen: any;
  selectedOptions: any = [];
  diseases = [];
  diseases1 = [];
  alergies = ["SF,SO", "ML", "F", "E", "N", "G"];
  additionPreferences: any;
  dietPlanChoosen: any;
  mealPref;
  defaultData: any;
  community = [];
  diseasesChoosen = [];
  allergyChoosen: any = [];
  searchDetails: any;
  dietPlanOptions: any = [
    { id: "fiteloWaterRetention", name: "Water retention" },
    { id: "ys_gutelimination", name: "Gut Elimination" },
    // { id: "fiteloWeightLoss", name: "High Protein fiber" },
    { id: "weightLoss", name: "Weight Loss" },
    { id: "muscleGain_morning", name: "Muscle Gain Morning" },
    { id: "muscleGain_evening", name: "Muscle Gain Evening" },
    { id: "fatShredding_morning", name: "Fat Shredding Morning" },
    { id: "fatShredding_evening", name: "Fat Shredding Evening" },
    { id: "diabetes", name: "Diabetes" },
    { id: "pcos", name: "PCOS" },
    { id: "cholesterol", name: "Cholesterol" },
    { id: "hypertension", name: "Hypertension" },
  ];
  genderOptions = [
    {
      code: "G1",
      gender: "Male",
      isSelected: false,
    },
    {
      code: "G2",
      gender: "Female",
      isSelected: false,
    },
    // {
    //   code: "G3",
    //   value: "Other",
    //   isSelected: false,
    // },
  ];

  allergiesOptions = [
    { code: "N", label: "NUTS" },
    { code: "E", label: "EGGS" },
    { code: "F", label: "FISH" },
    { code: "ML", label: "MILK/ LACTOSE" },
    { code: "SO", label: "SOYA" },
    { code: "SF", label: "SEAFOOD" },
    { code: "G", label: "GLUTEN" },
  ];
  countryOptions = [
    { code: "IND", label: "India" },
    { code: "USA", label: "USA" },
    { code: "UAE", label: "Dubai" },
    { code: "EGY", label: "Egypt" },
    { code: "CAN", label: "Canada" },
    { code: "AUS", label: "Australia" },
    { code: "MAL", label: "Malaysia" },
  ];

  diseasesOptions = [
    { code: "A", value: "ACIDITY" },
    { code: "AN", value: "ANEMIA" },
    { code: "AI", value: "ANTI-INFLAMMATORY" },
    { code: "B", value: "BLOOD PRESSURE" },
    { code: "C", value: "CHOLESTEROL" },
    { code: "CR", value: "CALCIUM-RICH" },
    { code: "D", value: "DIABETES" },
    { code: "PD", value: "PRE-DIABETES" },
    { code: "GD", value: "GESTATIONAL DIABETES" },
    // { code: "E", value: "EGESTION/ CONSTIPATION" },
    { code: "FL", value: "FATTY LIVER" },
    { code: "HP", value: "HIGH PROTEIN" },
    { code: "IR", value: "IRON-RICH" },
    { code: "LP", value: "LOW PROTEIN" },
    { code: "P", value: "PCOS" },
    { code: "T", value: "HYPOTHYROID" },
    { code: "S", value: "SLEEP DISORDER" },
    { code: "UA", value: "URIC ACID" },
    { code: "VB", value: "VITAMIN B12" },
    { code: "VD", value: "VITAMIN D" },
    { code: "W", value: "WIND/ FLATULENCE/ BLOATING" },
  ];
  companyId: string = "";

  diseasesOptionsMale = [
    { code: "A", value: "ACIDITY" },
    { code: "AN", value: "ANEMIA" },
    { code: "AI", value: "ANTI-INFLAMMATORY" },
    { code: "B", value: "BLOOD PRESSURE" },
    { code: "C", value: "CHOLESTEROL" },
    { code: "CR", value: "CALCIUM-RICH" },
    { code: "D", value: "DIABETES" },
    { code: "PD", value: "PRE-DIABETES" },
   // { code: "GD", value: "GESTATIONAL DIABETES" },
    // { code: "E", value: "EGESTION/ CONSTIPATION" },
    { code: "FL", value: "FATTY LIVER" },
    { code: "HP", value: "HIGH PROTEIN" },
    { code: "IR", value: "IRON-RICH" },
    { code: "LP", value: "LOW PROTEIN" },
   // { code: "P", value: "PCOS" },
    { code: "T", value: "HYPOTHYROID" },
    { code: "S", value: "SLEEP DISORDER" },
    { code: "UA", value: "URIC ACID" },
    { code: "VB", value: "VITAMIN B12" },
    { code: "VD", value: "VITAMIN D" },
    { code: "W", value: "WIND/ FLATULENCE/ BLOATING" },
  ];
  communityOptions = [
    { code: "U", label: "Other" },
    { code: "P", label: "North India" },
    { code: "S", label: "South India" },
    { code: "M", label: "Maharashtra" },
    { code: "G", label: "Gujarat" },
    { code: "B", label: "Bengali" },
  ];
  dietPrefOptions = [
    {
      code: "V",
      value: "Vegetarian",
      isSelected: false,
    },
    {
      code: "NV",
      value: "Non-Vegetarian",
      isSelected: false,
    },
    {
      code: "E",
      value: "Vegetarian + Egg",
      isSelected: false,
    },
    {
      code: "Ve",
      value: "Vegan",
      isSelected: false,
    },
  ];

  activitiesOptions = [
    {
      code: "AC1",
      value: "Sedentary (No Exercise)",
      data: 1.2,
      steps: 7500,
      calories: 200,
      isSelected: false,
    },
    {
      code: "AC2",
      value: "Lightly Active (Brisk Walking, Yoga, functional exercise)",
      data: 1.375,
      steps: 7500,
      calories: 300,
      isSelected: false,
    },
    {
      code: "AC3",
      value: "Moderately Active (Workout 5 days a week)",
      data: 1.55,
      steps: 10000,
      calories: 400,
      isSelected: false,
    },
    {
      code: "AC4",
      value: "Super Active (Rigrous workout, 5 days a week)",
      data: 1.7,
      steps: 10000,
      calories: 600,
      isSelected: false,
    },
    {
      code: "AC5",
      value: "Extremly Active (Rigrous workout, 2 times a day)",
      data: 1.9,
      steps: 10000,
      calories: 600,
      isSelected: false,
    },
  ];

  otherData = {
    workoutTime: [
      {
        code: "WO1",
        name: "morning",
        value: "Morning",
        isSelected: false,
      },
      {
        code: "WO2",
        name: "evening",
        value: "Evening",
        isSelected: false,
      },
    ],
    sleepType: [
      {
        code: "ST1",
        value: "Sound sleep",
        isSelected: false,
      },
      {
        code: "ST2",
        value: "Disturbed most of time",
        isSelected: false,
      },
      {
        code: "ST3",
        value: "Partially disturbed",
        isSelected: false,
      },
    ],
    leaveForOffice: [
      {
        code: "LFO1",
        value: "7:00 - 8:00 AM",
        isSelected: false,
      },
      {
        code: "LFO2",
        value: "8:00 - 9:00 AM",
        isSelected: false,
      },
      {
        code: "LFO3",
        value: "9:00 - 10:00 AM",
        isSelected: false,
      },
      {
        code: "LFO4",
        value: "10:00 - 11:00 AM",
        isSelected: false,
      },
      {
        code: "LFO5",
        value: "11:00 - 12:00 PM",
        isSelected: false,
      },
      {
        code: "LFO6",
        value: "Other",
        otherValue: "Other",
        isSelected: false,
      },
    ],
    comeBack: [
      {
        code: "CB1",
        value: "6:00 - 7:00 PM",
        isSelected: false,
      },
      {
        code: "CB2",
        value: "7:00 - 8:00 PM",
        isSelected: false,
      },
      {
        code: "CB3",
        value: "8:00 - 9:00 PM",
        isSelected: false,
      },
      {
        code: "CB4",
        value: "9:00 - 10:00 PM",
        isSelected: false,
      },
      {
        code: "CB5",
        value: "10:00 - 11:00 PM",
        isSelected: false,
      },
      {
        code: "CB6",
        value: "11:00 - 12:00 PM",
        isSelected: false,
      },
      {
        code: "CB7",
        value: "Other",
        otherValue: "Other",
        isSelected: false,
      },
    ],
    alcohol: [
      {
        code: "AL1",
        value: "None",
        isSelected: false,
      },
      {
        code: "AL2",
        value: "Once in a month",
        isSelected: false,
      },
      {
        code: "AL3",
        value: "Once in a week",
        isSelected: false,
      },
      {
        code: "AL4",
        value: "Two to 3 times a day",
        isSelected: false,
      },
      {
        code: "AL5",
        value: "Daily",
        isSelected: false,
      },
    ],
    waterDrink: [
      {
        code: "WD1",
        value: "3 to 4 glass",
        isSelected: false,
      },
      {
        code: "WD2",
        value: "5 to 7",
        isSelected: false,
      },
      {
        code: "WD3",
        value: "8 to 10",
        isSelected: false,
      },
      {
        code: "WD4",
        value: "10 to 12",
        isSelected: false,
      },
      {
        code: "WD5",
        value: "More than 12",
        isSelected: false,
      },
    ],
    ageMaster: {
      data: [
        {
          code: "A1",
          value: "15 to 18 years",
          isSelected: false,
          avg_age: "16.5",
        },
        {
          code: "A2",
          value: "19 to 22 years",
          isSelected: false,
          avg_age: "20.5",
        },
        {
          code: "A3",
          value: "23 to 28 years",
          isSelected: false,
          avg_age: "25.5",
        },
        {
          code: "A4",
          value: "29 to 35 years",
          isSelected: false,
          avg_age: "32",
        },
        {
          code: "A5",
          value: "36 to 40 years",
          isSelected: false,
          avg_age: "38",
        },
        {
          code: "A6",
          value: "41 to 45 years",
          isSelected: false,
          avg_age: "43",
        },
        {
          code: "A7",
          value: "46 to 50 years",
          isSelected: false,
          avg_age: "48",
        },
        {
          code: "A8",
          value: "51 to 55 years",
          isSelected: false,
          avg_age: "53",
        },
        {
          code: "A9",
          value: "56 to 60 years",
          isSelected: false,
          avg_age: "58",
        },
        {
          code: "A10",
          value: "61 to 65 years",
          isSelected: false,
          avg_age: "63",
        },
        {
          code: "A11",
          value: "66 to 70 years",
          isSelected: false,
          avg_age: "68",
        },
      ],
      validation: {
        required: true,
        msg: "Please select one",
      },
    },
  };
  updateTargetCal: any;
  recommendedTargetCal: any;
  profileInititals = "";
  search: string = localStorage.getItem("email");
  dietPrefChoosen;
  countryChoosen;
  communityChoosen: any;
  isLoading: boolean = true;

  constructor(
    private appS: appSer,
    private appServe: AppService,
    private appservice: appS,
    private utilities: UTILITIES,
    private alertController: AlertController,
    private router: Router,
    private templateService: TemplateService,
    private loaderService: LoaderService,
  ) {}

  ngOnInit() {}

  ionViewWillEnter() {
    this.fetchProfile();
    // console.log("DietPlans >> ", this.dietPlanOptions);
    console.log(
      "alam101>> ",
      localStorage.getItem("dietitianId") +
        " | " +
        localStorage.getItem("dietitianName") +
        " | " +
        localStorage.getItem("companyId") +
        " | " +
        localStorage.getItem("companyKey") +
        " | " +
        localStorage.getItem("urlClientId")
    );
    // if (localStorage.getItem("companyId") === "yellowsquash") {
      this.templateService.getSingleCompanyRecordtemp(localStorage.getItem("companyId")).subscribe(
        (response) => {
          console.log(
            "Get SingleCompany Records:: ",
            response["specialDietPlans"]
          );
          if(response["specialDietPlans"]){
          const specialDietPlans = response["specialDietPlans"];

          specialDietPlans.map((res) => {
            // console.log(">>> Res ----> ", res);
            const targetId = res.id;
            const targetName = res.name;
            // Check if the item exists in the array
            const exists = this.dietPlanOptions.some(
              (plan) => plan.id === targetId || plan.name === targetName
            );

            // Add the item if it doesn't exist
            if (!exists) {
              this.dietPlanOptions.push({ id: targetId, name: targetName });
            }
          });
          }
          // this.dietPlanOptions = [...this.dietPlanOptions, ...specialDietPlans];
        },
        (error) => {
          console.log("Get Single Company Error: ", error);
        }
      );
    // }
  }

  fetchProfile() {
    // const search = "91-7726922405";
    this.loaderService.presentLoader("Loading profile...").then((_) => {
      this.appS
        .searchProfile(this.search)
        .then((profileData: any) => {
          this.searchData = profileData["_id"];
          console.log("profileData:-", profileData);

          if (profileData["lifeStyle"]) {
            console.log("LifeStyle Exixts");
            var cCode = profileData["lifeStyle"]["country"];
            var communityCode = profileData["lifeStyle"]["communities"];
            var diseasesCode = profileData["lifeStyle"]["diseases"];
            var dietPrefCode = profileData["lifeStyle"]["foodType"];
            var dietPlanCode = profileData["lifeStyle"]["dietPlanName"];
            var activitiesCode = profileData["lifeStyle"]["activities"];
            this.countryChoosen = this.countryOptions.find(
              (country) => country.code === cCode
            );
            if (communityCode && communityCode.length > 0) {
              this.communityChoosen = this.communityOptions.find(
                (item) => item.code === communityCode[0]
              );
            }
           if(profileData["demographic"]["gender"]["code"]==="G1"){
            this.diseasesChoosen = this.diseasesOptionsMale.filter((item) =>
              diseasesCode.includes(item.code)
            );
          }
          else{
            this.diseasesChoosen = this.diseasesOptions.filter((item) =>
              diseasesCode.includes(item.code)
            );
          }
            this.allergyChoosen = this.allergiesOptions.filter((item) =>
              diseasesCode.includes(item.code)
            );
            this.dietPrefChoosen = this.dietPrefOptions.find(
              (item) => item.code === dietPrefCode
            );
            if (dietPlanCode) {
              const exists = this.dietPlanOptions.some(
                (plan) => plan.id === dietPlanCode || plan.name === dietPlanCode
              );

              // Add the item if it doesn't exist
              if (!exists) {
                this.dietPlanOptions.push({
                  id: dietPlanCode,
                  name: dietPlanCode,
                });
              }
            }
            this.dietPlanChoosen = this.dietPlanOptions.find(
              (item) => item.id === dietPlanCode
            );
            this.activityChoosen = this.activitiesOptions.find(
              (item) => item.code === activitiesCode.code
            );
          }

          // console.log(
          //   "Allergy DieseaseCode:: ",
          //   diseasesCode,
          //   " || Allergy Choosenfd >> ",
          //   this.allergyChoosen
          // );
          // console.log(
          //   "Code:: ",
          //   cCode,
          //   " || Countryy Choosenfd >> ",
          //   this.countryChoosen
          // );
          // console.log(
          //   "CommunityCode:: ",
          //   communityCode,
          //   " || community Choosenfd >> ",
          //   this.communityChoosen
          // );
          // console.log(
          //   "DieseasesCode:: ",
          //   diseasesCode,
          //   " || diseases Choosenfd >> ",
          //   this.diseasesChoosen
          // );
          // console.log(
          //   "activitiesCode:: ",
          //   activitiesCode,
          //   " || activities Choosenfd >> ",
          //   this.activityChoosen
          // );

          this.lifeStyle = profileData?.lifeStyle;

          if (this.lifeStyle) {
            this.lifeStyle.firstConsult = false;
          }
          this.additionalPref = profileData["additionalPref"];
          const data = profileData["profile"];
          // console.log("Data ::: ", data);
          if (data) {
            this.companyId = data["companyId"];
            this.searchDetails = JSON.parse(JSON.stringify(profileData));
            console.log("this.searchDetails", this.searchDetails);

            // this.dietPlanChoosen = planC[0];
            this.updateTargetCal = this.searchDetails?.lifeStyle?.calories;
            this.recommendedTargetCal = this.searchDetails?.lifeStyle?.calories;
            const dd = JSON.parse(JSON.stringify(profileData));
            this.profileInititals = dd?.profile.name;

            this.getProfile();
            // this.getToken1();
            this.isLoading = false;
            this.loaderService.hideLoader();
          } else {
            this.searchDetails = undefined;
            this.handleNoData();
          }
        })
        .catch((err) => {
          this.handleNoData();
        });
    });
  }

  handleNoData() {
    this.age = {
      code: "A4",
      label: "29 to 35 years",
      avg_age: "",
    };
    this.gender = {
      code: "G1",
      gender: "",
    };
    this.height = {
      unit: "in",
      value: "",
      feet: "",
      inches: "",
      cms: "",
      ischecked: true,
    };
    this.weight = {
      unit: "kg",
      value: "",
      ischecked: true,
    };
    this.isLoading = false;
    this.loaderService.hideLoader();
  }

  additionalPref(additionalPref) {
    this.additionPreferences = additionalPref;
  }

  getProfile() {
    this.profileData = [];
    this.appservice.getProfile().then((res) => {
      // console.log("Result :>> ", res);
      this.profileData = res;
      this.name = res["profile"]["name"];

      if (res["demographic"]["age"]) {
        this.age = res["demographic"]["age"];
      } else {
        this.age = {
          code: "A4",
          label: "29 to 35 years",
          avg_age: "",
        };
      }
      if (res["demographic"]["gender"]) {
        this.gender = res["demographic"]["gender"];
      } else {
        // alert('Gender Not');
        this.gender = {
          code: "G1",
          gender: "",
        };
      }

      var hVar = res["demographic"]["height"];

      if (!hVar) {
        // alert("Height Doesn't exixsts");
        this.heightInCms = true;
        this.height = {
          unit: "in",
          value: 0,
          feet: "",
          inches: "",
          cms: 0,
          ischecked: true,
        };
        this.selectedHeight = "1";
      } else if (hVar.unit === "in") {
        // this.saveHeight(hVar);
        this.heightInCms = true;
        // console.log("COnvert feets-Inhes to cm and store to value");
        // const value = this.convertFeetInchesToCm(0, hVar.value);
        this.height = {
          ...hVar,
          feet: "",
          inches: "",
          cms: Math.round(res["demographic"]["heightInCM"]),
        };
        this.selectedHeight = "1";
      } else {
        this.heightInCms = false;
        this.selectedHeight = "1";
        this.height = { ...hVar, feet: hVar.value, inches: 0, cms: hVar.value };
      }

      if (res["demographic"]["weight"]) {
        this.weight = res["demographic"]["weight"];
      } else {
        // alert("Weight not there");
        this.weight = {
          unit: "kg",
          value: "",
          ischecked: true,
        };
      }

      if (res["demographic"]["suggestedWeight"]) {
        this.desiredWeight = Math.round(res["demographic"]["suggestedWeight"]);
      }

      if (this.profileData?.profile?.subCategory === "weightloss") {
        this.profileData.profile.subCategory = "Weight Loss";
      }
      if (this.profileData?.profile?.subCategory === "weightmaintenance") {
        this.profileData.profile.subCategory = "Weight Maintenance";
      }
      if (this.profileData?.profile?.subCategory === "musclebuilding") {
        this.profileData.profile.subCategory = "Muscle Building";
      }
      if (this.profileData?.profile?.subCategory === "leanbody") {
        this.profileData.profile.subCategory = "Lean Body";
      }
      console.log("profileData::>> ", this.profileData);
    });
  }

  onDieseasesOptionSelected(event: any) {
    this.selectedOptions = event.detail.value;
    this.reorderDieseasesOptions();
  }

  reorderDieseasesOptions() {
    this.diseasesOptions.sort((a, b) => {
      const aSelected = this.selectedOptions.includes(a.value);
      const bSelected = this.selectedOptions.includes(b.value);
      return bSelected - aSelected;
    });
  }

  onAllergyOptionSelected(event: any) {
    this.selectedOptions = event.detail.value;
    this.reorderAllergyOptions();
  }

  reorderAllergyOptions() {
    this.allergiesOptions.sort((a, b) => {
      const aSelected = this.selectedOptions.includes(a.label);
      const bSelected = this.selectedOptions.includes(b.label);
      return bSelected - aSelected;
    });
  }

  // This function will be triggered when the Height unit toggle changes
  onToggleChange(event: any) {
    // Changed from feet to cm
    if (this.selectedHeight === "1") {
      this.height.cms = this.convertFeetInchesToCm(
        this.height.feet,
        this.height.inches
      );
    } // Changed from cm to feet inches
    else {
      // console.log("Convert value to feet-Inches");
      const { feet, inches } = this.convertCmToFeetInches(this.height.cms);
      this.height.feet = feet;
      this.height.inches = inches;
    }
  }

  alertConfirmClicked(): void {
    // Destructure the Activites properties
    const { code, data } = this.activityChoosen;

    var communityArr = [this.communityChoosen.code];

    if (this.countryChoosen.code === "IND" && !communityArr.includes("U")) {
      communityArr.push("U");
    }

    var heightVar = this.height;
    var cmsVar = this.height.cms;
    console.log("Height:: ", this.height);

    // Toggle is in cms
    if (this.selectedHeight==='1') {
      if(heightVar.unit === "in") { // unit is inches & toggle is cms
        heightVar = this.cmToInches(heightVar);
        console.log(">> Cms In Height:: >> ", cmsVar);
        this.heightInCms = false;
      } else {  // unit is cms & toogle is also cms
        heightVar.value = cmsVar;
      }
    } else if (heightVar.unit === "in") {  // Toggle is in Inches & unit also inches
      heightVar = this.feetToInches(heightVar);
    } else { // Toggle is in Inches & unit in cms
      const { value, feet, inches, unit, ischecked } = this.height;
      const cms = this.convertFeetInchesToCm(feet, inches)
      heightVar = { value: cms, unit, ischecked };
    }

    const genderChoosen = this.genderOptions.find(
      (item) => item.gender === this.gender.gender
    );

    genderChoosen.isSelected = true;

    // console.log("Allergy >> ", this.allergyChoosen);
    // console.log("Dieses >> ", this.diseasesChoosen);
    // console.log("Height: >> ", this.height);

    const diseaseaArr = [
      ...this.diseasesChoosen.map((disease) => disease.code),
      ...this.allergyChoosen.map((disease) => disease.code),
    ];

    var heightObj = {
      value: heightVar.value,
      unit: heightVar.unit ,
      ischecked: heightVar.ischecked
    }
    console.log("Height::: ", heightVar);
    console.log("OBJ Height:: ", heightObj);
    

    // return;

    const payload = {
      profile: {
        email: this.search,
        name: this.name,
        // companyId: "yellowsquash"//this.companyId
      },
      demographic: {
        age: this.age,
        gender: genderChoosen,
        height: heightObj,
        weight: this.weight,
        suggestedWeight: this.desiredWeight
      },
      lifeStyle: {
        diseases: diseaseaArr,
        activities: { code, data }, // { code: "AC2", data: 1.375,  },
        communities: communityArr, //["P", "U"],
        foodType: this.dietPrefChoosen.code,
        dietPlanName: this.dietPlanChoosen.id,
        country: this.countryChoosen.code,
      },
    };

    console.log("Payload ::-+->> ", payload);

    this.loaderService.presentLoader("Updating profile...").then((_) => {
      this.templateService.getSingleCompanyRecordtemp(this.companyId).subscribe((companyResponse) => {
        // console.log("Company Response :: ", companyResponse);
        this.appS.externalRegistration(companyResponse["companyKey"], payload).subscribe((res) => {
              console.log("Result >> ", res);

              if (localStorage.getItem("dietitianId") != null) {
                this.templateService
                  .handleDietitianAction(
                    localStorage.getItem("dietitianId"),
                    "66a34e015322b972c017fa2a",
                    localStorage.getItem("email"),
                    "Edit Profile",
                    localStorage.getItem("companyId")
                  )
                  .subscribe((dRespondata) => {
                    console.log("success", res);
                    this.utilities
                      .presentAlert("Profile Updated Successfully!")
                      .then((res) => {
                        // this.router
                        // .navigate(["edit-profile"])
                        // .then(() => {
                        window.location.reload();
                        // });
                      });
                  });
              } else {
                window.location.reload();
              }
              this.loaderService.hideLoader();
            },
            (error) => {
              this.utilities.presentAlert("Something Went Wrong...");
              console.log("User token ", error.error.text);
              this.loaderService.hideLoader();
            }
        );
      },
      (error) => {
        console.log("Get Single Company Error: ", error);
      });
    });
  }

  updateProfile() {
    if (
      this.height.value === "" &&
      this.height.feet === "" &&
      this.height.inches === ""
    ) {
      this.utilities.presentAlert("All Fields are required");
    } else if (
      this.activityChoosen === undefined ||
      this.countryChoosen === undefined ||
      this.communityChoosen === undefined ||
      this.dietPlanChoosen === undefined ||
      this.dietPrefChoosen === undefined ||
      this.weight.value === "" ||
      this.gender.gender === "" ||
      this.age.avg_age === "" ||
      this.age.avg_age === null ||
      this.name === ""
    ) {
      console.log("Not Given Fields");
      this.utilities.presentAlert("All Fields are required");
    } else {
      // this.presentAlertConfirm();
      this.utilities.presentAlertConfirmBox(
        "Update will delete the current date and future date Diet plans. Please confirm to continue.",
        () => this.alertConfirmClicked()
      );
    }
    // this.utilities.presentAlertConfirm("Update will delete the current date and future date Diet plans. Please conform to continue.", () => this.updateProfile());
    // alert("Clicked");
    console.log("Diet palna>>>", this.dietPlanChoosen);
  }

  // Inches to feet-inches
  saveHeight(hObj: any) {
    const inches: number = hObj.value;
    const feet = Math.floor(inches / 12);
    const remainingInches = parseFloat((inches % 12).toFixed(2));

    console.log("Feet: ", feet, " || Inches: ", remainingInches);
    this.height = { ...hObj, feet: feet, inches: remainingInches };
    console.log("Heighttt:: >> ", this.height);
    // return `${feet}' ${remainingInches}"`;
  }

  // feet-Inches to Inches
  feetToInches(heightVar: any) {
    const { feet, inches } = heightVar;

    heightVar.value = feet * 12 + inches;

    console.log(">>> Height Calulate", heightVar);

    const { value, unit, ischecked } = heightVar;

    return { value, unit, ischecked };
  }

  // Cms to Inches
  cmToInches(heightVar: any) {
    const Inches = heightVar.cms / 2.54;

    heightVar.value = parseFloat(Inches.toFixed(0));

    console.log(">>> Height Calulate", heightVar);

    const { value, unit, ischecked } = heightVar;

    return { value, unit, ischecked };
  }

  // Feet-Inches to Cm
  convertFeetInchesToCm(feet: number, inches: number): number {
    const feetToCm = feet * 30.48;
    const inchesToCm = inches * 2.54;
    const totalCm = feetToCm + inchesToCm;
    return Math.round(Math.round(totalCm * 100) / 100);
  }

  //  cm to feet-inches
  convertCmToFeetInches(cm: number): { feet: number; inches: number } {
    const totalInches = cm / 2.54;
    const feet = Math.floor(totalInches / 12);
    const inches = parseFloat((totalInches % 12).toFixed(0));

    const feetWithDecimal = parseFloat(
      (feet + Math.floor(inches / 12)).toFixed(2)
    );

    return { feet: feetWithDecimal, inches: inches % 12 };
  }
}
