import {
  Component,
  ElementRef,
  OnInit,
  QueryList,
  ViewChildren,
} from "@angular/core";
import {
  StorageService,
  AppService,
  LoaderService,
  TemplateService,
} from "../services";
import { Router } from "@angular/router";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { UTILITIES } from "../core/utility/utilities";

@Component({
  selector: "app-login",
  templateUrl: "./login.page.html",
  styleUrls: ["./login.page.scss"],
})
export class LoginPage implements OnInit {
  public loginForm: FormGroup;
  error: string;
  name = "fitrofy";
  hide: boolean;
  otp: string;
  isDisabled:boolean = false;
  // isIndividual:boolean = false;
  otpSent: boolean = false;
  signWithEmail: boolean = true;
  countdown: number;
  interval: any;
  validation_messages = {
    password: [
      { type: "required", message: "Password is mandatory." },
      {
        type: "pattern",
        message:
          "Use 8 or more characters with a mix of letters, numbers & symbols.",
      },
    ],
    email: [
      { type: "required", message: "Username is mandatory." },
      {
        type: "pattern",
        message: "Please enter valid Username",
      },
    ],
  };
  constructor(
    private _FB: FormBuilder,
    private appService: AppService,
    private storageService: StorageService,
    private router: Router,
    private loaderService: LoaderService,
    private utilities: UTILITIES,
    private templateService: TemplateService
  ) {
    this.loginForm = this._FB.group({
      email: [""],
      name: [""],
      password: [""],
    });
    localStorage.setItem("loginEmail", "");
    localStorage.setItem("acess_token", "");
  }

  ngOnInit() {}
  ionViewWillEnter() {
    if (
      localStorage.getItem("loginEmail") === "orthocure" ||
      localStorage.getItem("loginEmail") === "fitelo" ||
      localStorage.getItem("loginEmail") === "traya" ||
      localStorage.getItem("loginEmail") === "kapiva"
    ) {
      this.router.navigate(["deitician-search"]);
    } else if (localStorage.getItem("acess_token")) {
      this.router.navigate(["./tabs/home"]);
    }
  }

  logIn() {
    let email: any = this.loginForm.controls["email"].value;
    let name: any = this.loginForm.controls["name"].value;
    let password: any = this.loginForm.controls["password"].value;

    console.log(">>> Email::  ", email, " ||---|| Name: ", name);

    if (email === "" && name === "") {
      this.utilities.presentAlert("Provide either Email or Name");
    } else if (email !== "") {
      localStorage.setItem("loginEmail", email);
      this.loaderService.presentLoader("Please Wait").then((_) => {
        this.templateService.loginByEmail(email).subscribe(
          (response) => {
            console.log("\n-->>>> ", response);
            if (response["valid"]) {
              localStorage.setItem("isIndividual", response["isIndividual"]);
              if(response["isIndividual"] && response["isDisabled"]){
                this.isDisabled = response["isDisabled"];
                this.loaderService.hideLoader();
                return;
              }
              // this.isIndividual = response["isIndividual"];
              // localStorage.setItem("companyId", response["companyId"]);
              // localStorage.setItem("companyKey", response["companyKey"]);
              this.templateService.generateOtp(email).subscribe((result) => {
                this.startCountdown(5); // Start a 5-minute countdown
                console.log("Otp Generateed --)) ", result);
                this.otpSent = response["valid"];
                this.loaderService.hideLoader();
              });
            } else {
              this.utilities.presentAlert("Entered Email is not valid");
              this.loaderService.hideLoader();
            }
          },
          (err) => {
            // alert("Error")
            this.loaderService.hideLoader();
            this.utilities.presentAlert("Something went wrong!")
            console.log("Error Response>> ", err);
          }
        );
      });
    } else if (name !== "" && password === "") {
      this.utilities.presentAlert("Password is required");
    } else {
      localStorage.setItem("loginEmail", name);
      let obj = {
        username: name,
        password: password,
      };
      this.loaderService.presentLoader("Please Wait").then((_) => {
        this.appService.signIn(obj).subscribe(
          (response) => {
            if (response) {
              localStorage.setItem("email", "");
              console.log("response", response.access_token.toString());
              localStorage.setItem("acess_token", response.access_token);
              this.loaderService.hideLoader();
              if (
                localStorage.getItem("loginEmail") === "orthocure" ||
                localStorage.getItem("loginEmail") === "fitelo" ||
                localStorage.getItem("loginEmail") === "traya" ||
                localStorage.getItem("loginEmail") === "kapiva" ||
                localStorage.getItem("loginEmail") === "beato"
              ) {
                localStorage.setItem("email", "");
                // this.router.navigate(["deitician-search"],{queryParams:{param:'loged in'}});
                this.router.navigate(["preference-diet"], {
                  queryParams: { param: "loged in" },
                });
              } else {
                this.router.navigate(["/tabs/home"]);
              }
            }
            (error) => {
              this.loaderService.hideLoader();
            };
          },
          (err) => {
            this.loaderService.hideLoader();
          }
        );
      });
    }
  }
  showPassword() {
    this.hide = !this.hide;
  }

  verifyOtp() {
    console.log("OTPPP :: ", this.otp);
    let email: any = this.loginForm.controls["email"].value;
    this.loaderService.presentLoader("Please Wait").then((_) => {
      this.templateService.verifyOtp(email, this.otp).subscribe(
        (response) => {
          console.log("\n-->>>> VerifyOtp+ ", response);
          clearInterval(this.interval); // Clear the interval when the component is destroyed
          // this.utilities.presentAlert("Login Success");
          localStorage.setItem("acess_token", response["access_token"]);
          localStorage.setItem("loginEmail", email);

          localStorage.setItem("email", "");
          this.loaderService.hideLoader();
          this.otpSent = false;
          this.router.navigate(["first-page"]);
        },
        (err) => {
          console.log("Error Response>> ", err);
          this.utilities.presentAlert("Incorrect Otp");
          this.loaderService.hideLoader();
        }
      );
    });
  }

  startCountdown(minutes: number) {
    this.countdown = minutes * 60; // Convert minutes to seconds

    this.interval = setInterval(() => {
      const minutes = Math.floor(this.countdown / 60);
      const seconds = this.countdown % 60;
      const formattedMinutes = minutes < 10 ? "0" + minutes : minutes;
      const formattedSeconds = seconds < 10 ? "0" + seconds : seconds;

      const countdownElement = document.getElementById("countdown-timer");
      if (countdownElement) {
        countdownElement.textContent = `${formattedMinutes}:${formattedSeconds}`;
      }

      if (this.countdown > 0) {
        this.countdown--;
      } else {
        clearInterval(this.interval);
        // You can add additional logic here when the timer reaches zero
      }
    }, 1000);
  }

  backHandler() {
    clearInterval(this.interval);
    this.otp = "";
    this.otpSent = false;
  }

  toggleSignUp() {
    this.signWithEmail = !this.signWithEmail;
    // alert(this.signWithEmail)
  }
}
