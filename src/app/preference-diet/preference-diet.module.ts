import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

import { IonicModule } from "@ionic/angular";

import { PreferenceDietPageRoutingModule } from "./preference-diet-routing.module";

import { PreferenceDietPage } from "./preference-diet.page";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreferenceDietPageRoutingModule,
  ],
  declarations: [
    PreferenceDietPage
  ],
})
export class PreferenceDietPageModule {}
