import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreferenceDietPage } from './preference-diet.page';

const routes: Routes = [
  {
    path: '',
    component: PreferenceDietPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreferenceDietPageRoutingModule {}
