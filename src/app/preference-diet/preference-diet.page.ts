import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { UTILITIES } from "../core/utility/utilities";
import { AppService } from "../services/app/app.service";
import * as hulnuts from "../shared/constants/nuts.json";
import { CONSTANTS } from "../core/constants/constants";
import { AppService as appS } from "../app.service";
import { AppService as appSer } from "../../app/app.service";
import { TemplateService } from "../services";
@Component({
  selector: "app-preference-diet",
  templateUrl: "./preference-diet.page.html",
  styleUrls: ["./preference-diet.page.scss"],
})
export class PreferenceDietPage implements OnInit {
  username = localStorage
    .getItem("loginEmail")
    .toLowerCase()
    .includes("beatoapp.com".toLowerCase())
    ? "beato"
    : localStorage.getItem("loginEmail");
  dietitianPrefItems = [];
  nonVegWeek = new Array(7);
  fruitsWeek = new Array(7);
  desertWeek = new Array(7);
  nonVegDays = new Array(7);
  nonVegDinnerDays = new Array(7);
  daysOfWeek = new Array("SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT");
  nonEggDays = new Array(7);
  detoxDays = new Array(7).fill(false);
  dinnerPref = new Array(3).fill(false);
  cheatDays = new Array(7);
  cheatDinnerDays = new Array(7);
  cheatTime = new Array(7).fill("Lunch");
  cheatDinnerTime = new Array(7).fill("Dinner");
  desertDays = new Array(7);
  desertTime = new Array(7);
  fruitsDays = new Array(7);
  fruitsTime = new Array(7);
  waterProtein = "";
  cookingExpeties = "";
  routine = "";
  fruitesCon = "";
  milkTea = "3";
  desert = true;
  hideInFiveMint=true;
  midDayMeal = "";
  eatOutside = "";
  beatoDietPlan = [
    { id: "weightLoss", name: "Weight Loss" },
    { id: "diabetes", name: "Diabetes" },
  ];
  dietPlan = [
    { id: "fiteloWaterRetention", name: "Water retention" },
    { id: "fiteloWeightLoss", name: "High Protein fiber" },
    { id: "weightLoss", name: "Weight Loss" },
    { id: "muscleGain_morning", name: "Muscle Gain Morning" },
    { id: "muscleGain_evening", name: "Muscle Gain Evening" },
    { id: "fatShredding_morning", name: "Fat Shredding Morning" },
    { id: "fatShredding_evening", name: "Fat Shredding Evening" },
    { id: "diabetes", name: "Diabetes" },
    { id: "pcos", name: "PCOS" },
    { id: "cholesterol", name: "Cholesterol" },
    { id: "hypertension", name: "Hypertension" },
  ];

  week = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];

  dietData = [
    { value: "0", name: "When You Wake up" },
    { value: "1", name: "Before Breakfast" },
    { value: "2", name: "Breakfast" },
    { value: "3", name: "Mid Day Meal" },
    { value: "4", name: "Lunch" },
    { value: "5", name: "Post Lunch" },
    { value: "6", name: "Evening Snack" },
    { value: "7", name: "Dinner" },
    { value: "8", name: "Before Sleep" },
  ];
  dietData1 = [
    { value: "0", name: "When You Wake up" },
    { value: "1", name: "Before Breakfast" },
    { value: "2", name: "Breakfast" },
    { value: "3", name: "Mid Day Meal" },
    { value: "4", name: "Lunch" },
    { value: "5", name: "Post Lunch" },
    { value: "6", name: "Evening Snack" },
    { value: "7", name: "Dinner" },
    { value: "8", name: "Before Sleep" },
  ];
  dietData2 = [{ value: "4", name: "Lunch" }];
  dietData7 = [{ value: "2", name: "Breakfast" }];
  dietData8 = [{ value: "7", name: "Dinner" }];
  dietData9 = [
    { value: "0", name: "When You Wake up" },
    { value: "1", name: "Before Breakfast" },
    { value: "2", name: "Breakfast" },
    { value: "3", name: "Mid Day Meal" },
    { value: "4", name: "Lunch" },
    { value: "5", name: "Post Lunch" },
    { value: "6", name: "Evening Snack" },
    { value: "7", name: "Dinner" },
    { value: "8", name: "Before Sleep" },
  ];
  nonvegupto = [];
  nonveguptoDinner = [];
  additionalPref: [
    {
      planChosen: "waterRetention";
      notNonVegDay: ["TUE", "SAT"];
      notNonEggDay: ["TUE", "SAT"];
      NonVegLunch: ["WED"];
      NonVegDinner: ["SAT"];
      // dessertLiking: true;
      teaLiking: "";
      coffeeLiking: "";
      fruitsLiking: "";
      cookingProficiency: 9;
      cookingTimeAvailability: 3;
      cheatDaysLunch: ["Wed"];
      cheatDaysDinner: ["Sun"];
      detoxDay: ["Tue"];
      caloriesDeficit: 500;
      midDayMeals: ["3", "6"];
    }
  ];
  breakfastValue = [
    { name: "R", value: "1786" },
    { name: "Low Fat Milk(1% Fat)", value: "1077" },
    { name: "Buttermilk", value: "2086" },
  ];
  dinnerValue = [];
  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private utils: UTILITIES,
    private appService: AppService,
    private appservice: appS,
    private appS: appSer,
    private appServe: AppService,
    private utilities: UTILITIES,
    private templateService: TemplateService
  ) {
    setTimeout(()=>{
      this.hideInFiveMint=false;
    },4000);
    if (this.username === "beato") {
      this.dietPlan = [
        { id: "weightLoss", name: "Weight Loss" },
        { id: "diabetes", name: "Diabetes" },
      ];
    } else {
      this.dietPlan = [
        { id: "fiteloWaterRetention", name: "Water retention" },
        { id: "ys_gutelimination", name: "Ys Gut Elimination" },
        // { id: "fiteloWeightLoss", name: "High Protein fiber" },
        { id: "weightLoss", name: "Weight Loss" },
        { id: "muscleGain_morning", name: "Muscle Gain Morning" },
        { id: "muscleGain_evening", name: "Muscle Gain Evening" },
        { id: "fatShredding_morning", name: "Fat Shredding Morning" },
        { id: "fatShredding_evening", name: "Fat Shredding Evening" },
        { id: "diabetes", name: "Diabetes" },
        { id: "pcos", name: "PCOS" },
        { id: "cholesterol", name: "Cholesterol" },
        { id: "hypertension", name: "Hypertension" },
      ];
    }
  }
  defaultslot1 = [];
  defaultslot2 = [];
  defaultslot3 = [];
  defaultslot4 = [];
  defaultslot5 = [];
  defaultslot55 = [];
  defaultslot56 = [];
  defaultslot6 = [];
  defaultslot7 = [];
  defaultslot8 = [];
  defaultslot9 = [];
  defaultslot21 = [];
  defaultslot22 = [];
  ngOnInit() {
    this.profileData = [];
    this.getNutsValue();
    this.getPreferences();
    this.fetchProfile();

    setTimeout(() => {
      if (this.isShow?.length == 0) {
        this.isShow = [...this.isT1];
      }
      if (this.isShowDinner?.length == 0) {
        this.isShowDinner = [...this.isT5Dinner];
      }
      if (this.isShow2?.length == 0) {
        this.isShow2 = [...this.isT2];
      }
      if (this.isShow3?.length == 0) {
        this.isShow3 = [...this.isT3];
      }
      if (this.isShow1?.length == 0) {
        this.isShow1 = [...this.isT4];
      }
      if (this.isShowCheatDinner1?.length == 0) {
        this.isShowCheatDinner1 = [...this.isT4Dinner];
      }

      // console.log(this.detoxDays, this.checkedDetox);

      this.checkedDetox = this.detoxDays.filter((item) => {
        return item === true;
      }).length;
    }, 500);

  //  if (localStorage.getItem("companyId") === "yellowsquash") {
      this.templateService.getSingleCompanyRecordtemp(localStorage.getItem("companyId")).subscribe(
        (response) => {
          console.log(
            "Get SingleCompany Records:: ",
            response["specialDietPlans"]
          );
          if(response["specialDietPlans"]){
          const specialDietPlans = response["specialDietPlans"];

          specialDietPlans.map((res) => {
            // console.log(">>> Res ----> ", res);
            const targetId = res.id;
            const targetName = res.name;
            // Check if the item exists in the array
            const exists = this.dietPlan.some(
              (plan) => plan.id === targetId || plan.name === targetName
            );

            // Add the item if it doesn't exist
            if (!exists) {
              this.dietPlan.push({ id: targetId, name: targetName });
            }
          });
        }
          // this.dietPlan = [...this.dietPlan, ...specialDietPlans];
        },
        (error) => {
          console.log("Get Single Company Error: ", error);
        }
      );
   // }
  }
  actualLength = 7;
  actualLengthDinner = 7;
  isShow = [];
  preferNonVegTime = new Array(7).fill("Lunch");
  preferNonVegDinnerTime = new Array(7).fill("Dinner");
  preferNonVegDays = new Array(7);
  preferNonVegDinnerDays = new Array(7);
  isShowItem(index, event) {
    this.preferNonVegDays.push(index);
    if (this.nonvegupto.length < this.actualLength && event.detail.checked) {
      this.nonvegupto.push(index);
      this.isShow.push(index);
    } else if (event.detail.checked == false) {
      this.preferNonVegTime[index - 1] = "";
      if (this.nonvegupto.includes(index)) {
        this.nonvegupto = this.nonvegupto.filter((item) => {
          return item != index;
        });
      }
      this.isShow = this.isShow.filter((item) => {
        return item != index;
      });
    }
  }
  isShowDinner = [];
  isShowItemDinner(index, event) {
    if (
      this.nonveguptoDinner.length < this.actualLengthDinner &&
      event.detail.checked
    ) {
      this.nonveguptoDinner.push(index);
      this.isShowDinner.push(index);
    } else if (event.detail.checked == false) {
      if (this.nonveguptoDinner.includes(index)) {
        this.nonveguptoDinner = this.nonveguptoDinner.filter((item) => {
          return item != index;
        });
      }
      this.isShowDinner = this.isShowDinner.filter((item) => {
        return item != index;
      });
    }
  }

  checkedDetox = 0;
  onCheckedDetox(event) {
    if (event.detail.checked === true) {
      this.checkedDetox = this.checkedDetox + 1;
    } else {
      this.checkedDetox = this.checkedDetox - 1;
    }
  }
  detoxLimit = 2;
  cheatLength = 2;
  cheatDinnerLength = 2;
  isShow1 = [];
  isShowCheatDinner1 = [];
  isShow2 = [];
  isShow3 = [];
  nonvegupto1 = [];
  cheatUptoDinner1 = [];
  nonvegupto2 = [];
  nonvegupto3 = [];

  isT1 = [];
  isT2 = [];
  isT3 = [];
  isT4 = [];
  isT4Dinner = [];
  isT5Dinner = [];

  isShowDesertItem(index, event) {
    if (this.nonvegupto2.length < this.cheatLength && event.detail.checked) {
      this.nonvegupto2.push(index);
      this.isShow2.push(index);
    } else if (event.detail.checked == false) {
      this.desertTime[index - 1] = "";
      if (this.nonvegupto2.includes(index)) {
        this.nonvegupto2 = this.nonvegupto2.filter((item) => {
          return item != index;
        });
      }
      this.isShow2 = this.isShow2.filter((item) => {
        return item != index;
      });
    }
  }

  isShowFruitsItem(index, event) {
    if (event.detail.checked) {
      this.nonvegupto3.push(index);
      this.isShow3.push(index);
    } else if (event.detail.checked == false) {
      if (this.nonvegupto3.includes(index)) {
        this.nonvegupto3 = this.nonvegupto3.filter((item) => {
          return item != index;
        });
      }
      this.isShow3 = this.isShow3.filter((item) => {
        return item != index;
      });
    }
  }
  //alam://
  isShowCheatDinnerItem(index, event) {
    if (
      this.cheatUptoDinner1.length < this.cheatDinnerLength &&
      event.detail.checked
    ) {
      this.cheatUptoDinner1.push(index);
      this.isShowCheatDinner1.push(index);
    } else if (event.detail.checked == false) {
      this.cheatDinnerTime[index - 1] = "";
      if (this.cheatUptoDinner1.includes(index)) {
        this.cheatUptoDinner1 = this.cheatUptoDinner1.filter((item) => {
          return item != index;
        });
      }
      this.isShowCheatDinner1 = this.isShowCheatDinner1.filter((item) => {
        return item != index;
      });
    }
  }
  isShowCheatItem(index, event) {
    if (this.nonvegupto1.length < this.cheatLength && event.detail.checked) {
      this.nonvegupto1.push(index);
      this.isShow1.push(index);
    } else if (event.detail.checked == false) {
      this.cheatTime[index - 1] = "";
      if (this.nonvegupto1.includes(index)) {
        this.nonvegupto1 = this.nonvegupto1.filter((item) => {
          return item != index;
        });
      }
      this.isShow1 = this.isShow1.filter((item) => {
        return item != index;
      });
    }
  }
  removedSlot = [];
  defaultIncomingPlan;
  additionalPref1: any;
  foodType = "NV";
  getPreferences() {
    this.dinnerOptions = new Array(3).fill("");
    const payload = {
      email: localStorage.getItem("email"),
    };
    this.appService.getPreferences(payload).subscribe((res: any) => {
      // console.log("res", res);
      if (res?.lifeStyle != undefined) {
        const targetId = res?.lifeStyle.dietPlanName;
        // Check if the item exists in the array
        const exists = this.dietPlan.some(
          (plan) => plan.id === targetId || plan.name === targetId
        );

        // Add the item if it doesn't exist
        if (!exists) {
          this.dietPlan.push({ id: targetId, name: targetId });
        }

        this.waterProtein = res?.lifeStyle.dietPlanName;
        this.foodType = res?.lifeStyle?.foodType;
        this.defaultIncomingPlan = res?.lifeStyle.dietPlanName;
      }
      if (
        res?.additionalPref != undefined &&
        res?.additionalPref?.dietitianPrefItems?.length > 0
      ) {
        res?.additionalPref?.dietitianPrefItems.filter((item, index) => {
          if (item.category === "N") {
            const data1 = {
              slot: item.slot,
              category: "N",
              foodId: item?.foodId,
            };
            this.dietitianPrefItems.push(data1);
            this.defaultslot1 = item?.foodId;
            this.slot1 = item?.slot.toString();
          } else if (item.category === "NS") {
            const data2 = {
              slot: item.slot,
              category: "NS",
              foodId: item?.foodId,
            };
            this.dietitianPrefItems.push(data2);
            this.defaultslot2 = item?.foodId;
            this.slot2 = item?.slot.toString();
          } else if (item.category === "F") {
            const data5 = {
              slot: item.slot,
              category: "F",
              foodId: item?.foodId,
            };
            this.dietitianPrefItems.push(data5);
            this.defaultslot5 = item?.foodId;
            this.slot5 = item?.slot.toString();
          } else if (item.category === "PW") {
            const data55 = {
              slot: item.slot,
              category: "PW",
              foodId: item?.foodId,
            };
            this.dietitianPrefItems.push(data55);
            this.defaultslot55 = item?.foodId;
            this.slot55 = item?.slot.toString();
          } else if (item.category === "WS") {
            const data56 = {
              slot: item.slot,
              category: "WS",
              foodId: item?.foodId,
            };
            this.dietitianPrefItems.push(data56);
            this.defaultslot56 = item?.foodId;
            this.slot56 = item?.slot.toString();
          } else if (item.category === "S") {
            const data7 = {
              slot: item.slot,
              category: "S",
              foodId: item?.foodId,
            };
            this.dietitianPrefItems.push(data7);
            this.defaultslot7 = item?.foodId;
            this.slot7 = item?.slot.toString();
          } else if (item.category === "S_1") {
            const data21 = {
              slot: item.slot,
              category: "S_1",
              foodId: item?.foodId,
            };
            this.dietitianPrefItems.push(data21);
            this.defaultslot21 = item?.foodId;
            this.slot21 = item?.slot.toString();
          } else if (item.category === "R") {
            const data22 = {
              slot: item.slot,
              category: "R",
              foodId: item?.foodId,
            };
            this.dietitianPrefItems.push(data22);
            this.defaultslot22 = item?.foodId;
            this.slot22 = item?.slot.toString();
          } else if (
            item.category === "DZ" &&
            (item.slot === 0 || item.slot === "0")
          ) {
            const data3 = { slot: 0, category: "DZ", foodId: item?.foodId };
            this.dietitianPrefItems.push(data3);
            this.defaultslot3 = item?.foodId;
            this.slot3 = "0";
          } else if (
            item.category === "DZ_1" &&
            (item?.slot === 8 || item?.slot === "8")
          ) {
            const data6 = { slot: 8, category: "DZ_1", foodId: item?.foodId };
            this.dietitianPrefItems.push(data6);
            this.defaultslot6 = item?.foodId;
            this.slot6 = "8";
          } else if (item.category === "BTODBW") {
            const data9 = {
              slot: item?.slot.toString(),
              category: "BTODBW",
              foodId: item?.foodId,
            };
            this.dietitianPrefItems.push(data9);
            this.defaultslot9 = item?.foodId;
            this.slot9 = item?.slot.toString();
          }
        });
        const tempDMItem = res?.additionalPref?.dietitianPrefItems.filter(
          (item) => {
            return item.category === "DM";
          }
        );
        let slot4Data = [];
        for (let index = 0; index < tempDMItem.length; index++) {
          slot4Data.push(tempDMItem[index].slot + "");
        }
        const data4 = {
          slot: slot4Data,
          category: "DM",
          foodId: tempDMItem[0]?.foodId,
        };
        this.slot4 = slot4Data;
        this.defaultslot4 = tempDMItem[0]?.foodId;
        this.dietitianPrefItems.push(data4);
        // console.log("this.dietitianPrefItems", this.dietitianPrefItems);
      }
      if (res?.additionalPref?.dinnerOptions?.length > 0) {
        res?.additionalPref?.dinnerOptions?.filter((itm, index) => {
          this.dinnerOptions[index] = itm;
        });
        this.dinnerOptions.filter((item) => {
          if (item === "A") {
            this.dinnerPref[0] = true;
          } else if (item === "C") {
            this.dinnerPref[1] = true;
          } else if (item === "W") {
            this.dinnerPref[2] = true;
          }
        });
      }

      if (res?.additionalPref != undefined) {
        this.additionalPref1 = res?.additionalPref;
        this.removedSlot = res?.additionalPref?.removeSlot
          .toString()
          .split(",");
        this.removeSlot = this.removedSlot;

        this.week.filter((item, ind) => {
          for (
            let index = 0;
            index < this.additionalPref1?.notNonVegDay?.length;
            index++
          ) {
            if (item === this.additionalPref1?.notNonVegDay[index]) {
              this.nonVegWeek[ind] = true;
            }
          }
        });
        for (
          let index = 0;
          index < this.additionalPref1?.notNonEggDay?.length;
          index++
        ) {
          this.week.filter((item, ind) => {
            if (item === this.additionalPref1?.notNonEggDay[index]) {
              this.nonEggDays[ind] = true;
            }
          });
        }

        if (this.additionalPref1?.nonVegLunch?.length > 0) {
          for (
            let index = 0;
            index < this.additionalPref1?.nonVegLunch?.length;
            index++
          ) {
            this.week.filter((item, ind) => {
              if (item === this.additionalPref1?.nonVegLunch[index]) {
                this.nonVegDays[ind] = true;
                this.isT1.push(ind + 1);
                this.preferNonVegTime[ind] = "Lunch";
              }
            });
          }
        }
        if (this.additionalPref1?.nonVegDinner?.length > 0) {
          for (
            let index = 0;
            index < this.additionalPref1?.nonVegDinner?.length;
            index++
          ) {
            this.week.filter((item, ind) => {
              if (item === this.additionalPref1?.nonVegDinner[index]) {
                this.nonVegDinnerDays[ind] = true;
                this.isT5Dinner.push(ind + 1);
                this.preferNonVegDinnerTime[ind] = "Dinner";
              }
            });
          }
        }
        if (this.additionalPref1?.cheatDaysDinner?.length > 0) {
          for (
            let index = 0;
            index < this.additionalPref1?.cheatDaysDinner?.length;
            index++
          ) {
            this.week.filter((item, ind) => {
              if (item === this.additionalPref1?.cheatDaysDinner[index]) {
                this.cheatDinnerDays[ind] = true;
                this.isT4Dinner.push(ind + 1);
                this.cheatDinnerTime[ind] = "Dinner";
              }
            });
          }
        }
        if (this.additionalPref1?.cheatDaysLunch?.length > 0) {
          for (
            let index = 0;
            index < this.additionalPref1?.cheatDaysLunch?.length;
            index++
          ) {
            this.week.filter((item, ind) => {
              if (item === this.additionalPref1?.cheatDaysLunch[index]) {
                this.cheatDays[ind] = true;
                this.isT4.push(ind + 1);
                this.cheatTime[ind] = "Lunch";
              }
            });
          }
        }
        if (this.additionalPref1?.fruitsDaysEveMeal?.length > 0) {
          for (
            let index = 0;
            index < this.additionalPref1?.fruitsDaysEveMeal?.length;
            index++
          ) {
            this.week.filter((item, ind) => {
              if (item === this.additionalPref1.fruitsDaysEveMeal[index]) {
                this.fruitsDays[ind] = true;
                this.isT3.push(ind + 1);
                this.fruitsTime[ind] = "Dinner";
              }
            });
          }
        }
        if (this.additionalPref1.fruitsDaysMidDay.length > 0) {
          for (
            let index = 0;
            index < this.additionalPref1.fruitsDaysMidDay.length;
            index++
          ) {
            this.week.filter((item, ind) => {
              if (item === this.additionalPref1.fruitsDaysMidDay[index]) {
                this.fruitsDays[ind] = true;
                this.isT3.push(ind + 1);
                this.fruitsTime[ind] = "Lunch";
              }
            });
          }
        }
        if (this.additionalPref1.dessertDaysEveMeal.length > 0) {
          for (
            let index = 0;
            index < this.additionalPref1.dessertDaysEveMeal.length;
            index++
          ) {
            this.week.filter((item, ind) => {
              if (item === this.additionalPref1.dessertDaysEveMeal[index]) {
                this.desertDays[ind] = true;
                this.isT2.push(ind + 1);
                this.desertTime[ind] = "Dinner";
              }
            });
          }
        }
        if (this.additionalPref1.dessertDaysMidDay.length > 0) {
          for (
            let index = 0;
            index < this.additionalPref1.dessertDaysMidDay.length;
            index++
          ) {
            this.week.filter((item, ind) => {
              if (item === this.additionalPref1.dessertDaysMidDay[index]) {
                this.desertDays[ind] = true;
                this.isT2.push(ind + 1);
                this.desertTime[ind] = "Lunch";
              }
            });
          }
        }

        this.nonvegupto = [...this.isShow];
        this.nonvegupto1 = [...this.isShow1];
        this.nonveguptoDinner = [...this.isShowDinner];
        this.cheatUptoDinner1 = [...this.isShowCheatDinner1];
        this.nonvegupto2 = [...this.isShow2];
        this.nonvegupto3 = [...this.isShow3];

        if (this.additionalPref1?.normalDetoxDay?.length > 0) {
          for (
            let index = 0;
            index < this.additionalPref1.normalDetoxDay.length;
            index++
          ) {
            this.week.filter((item, ind) => {
              if (item === this.additionalPref1.normalDetoxDay[index]) {
                this.detoxDays[ind] = true;
                this.checkedDetox = this.checkedDetox + 1;
              }
            });
          }
        }

        this.cookingExpeties = this.additionalPref1.cookingProficiency + "";
        this.routine = this.additionalPref1.cookingTimeAvailability + "";
        this.fruitesCon = this.additionalPref1.fruitsLiking + "";
        this.milkTea =
          this.additionalPref1.teaLiking == false &&
          this.additionalPref1.coffeeLiking == false
            ? "3"
            : this.additionalPref1.teaLiking == true
            ? "2"
            : "1";
        this.midDayMeal = this.additionalPref1.midDayMeals + "";
      }
    });
  }
  goback() {
    this.router.navigate(["deitician-search"]).then(() => {
      window.location.reload();
    });
  }
  dinnerOptions = new Array(3).fill("");
  updatePreferences() {
    const fruitsWeekDays = [];
    const desertsWeekDays = [];
    const notNonVegDay = [];
    const notNonEggDay = [];
    const nonVegLunch = [];
    let nonVegDinner = [];
    const cheatLunch = [];
    let cheatDinner = [];
    const desertLunch = [];
    const desertDinner = [];
    const fruitsLunch = [];
    const fruitsDinner = [];
    const detoxDaysdetail = [];

    this.utilities.presentAlertConfirmBox(
      "Update will delete the current date and future date Diet plans. Please confirm to continue.",
      () => {
        for (let index = 0; index < this.fruitsWeek.length; index++) {
          if (this.fruitsWeek[index]) {
            fruitsWeekDays.push(this.week[index]);
          }
        }
        for (let index = 0; index < this.desertWeek.length; index++) {
          if (this.desertWeek[index]) {
            desertsWeekDays.push(this.week[index]);
          }
        }
        for (let index = 0; index < this.nonVegWeek.length; index++) {
          if (this.nonVegWeek[index]) {
            notNonVegDay.push(this.week[index]);
          }
        }
        for (let index = 0; index < this.nonEggDays.length; index++) {
          if (this.nonEggDays[index]) {
            notNonEggDay.push(this.week[index]);
          }
        }
        for (let index = 0; index < this.nonVegDays.length; index++) {
          if (this.nonVegDays[index]) {
            if (this.preferNonVegTime[index] === "Lunch") {
              nonVegLunch.push(this.week[index]);
            }
          }
        }
        for (let index = 0; index < this.nonVegDinnerDays.length; index++) {
          if (this.nonVegDinnerDays[index]) {
            if (this.preferNonVegDinnerTime[index] === "Dinner") {
              nonVegDinner.push(this.week[index]);
            }
          }
        }

        for (let index = 0; index < this.cheatDays.length; index++) {
          if (this.cheatDays[index]) {
            if (this.cheatTime[index] === "Lunch") {
              cheatLunch.push(this.week[index]);
            }
          }
        }

        for (let index = 0; index < this.cheatDinnerDays.length; index++) {
          if (this.cheatDinnerDays[index]) {
            if (this.cheatDinnerTime[index] === "Dinner") {
              cheatDinner.push(this.week[index]);
            }
          }
        }

        for (let index = 0; index < this.desertDays.length; index++) {
          if (this.desertDays[index]) {
            if (this.desertTime[index] === "Lunch") {
              desertLunch.push(this.week[index]);
            }
            if (this.desertTime[index] === "Dinner") {
              desertDinner.push(this.week[index]);
            }
          }
        }

        for (let index = 0; index < this.fruitsDays.length; index++) {
          if (this.fruitsDays[index]) {
            if (this.fruitsTime[index] === "Lunch") {
              fruitsLunch.push(this.week[index]);
            }
            if (this.fruitsTime[index] === "Dinner") {
              fruitsDinner.push(this.week[index]);
            }
          }
        }

        for (let index = 0; index < this.detoxDays.length; index++) {
          if (this.detoxDays[index]) {
            detoxDaysdetail.push(this.week[index]);
          }
        }

        this.dietitianPrefItems = this.dietitianPrefItems.filter((item) => {
          return item.category !== "DM" && item.category !== "B";
        });

        if (this.slot7 != undefined && this.defaultslot7?.length > 0) {
          for (let index = 0; index < this.slot7.length; index++) {
            this.dietitianPrefItems.push({
              slot: this.slot7[index],
              category: "S",
              foodId: this.defaultslot7,
            });
          }
        }
        if (this.slot21 != undefined && this.defaultslot21?.length > 0) {
          for (let index = 0; index < this.slot21.length; index++) {
            this.dietitianPrefItems.push({
              slot: this.slot21[index],
              category: "S_1",
              foodId: this.defaultslot21,
            });
          }
        }
        if (this.slot22 != undefined && this.defaultslot22?.length > 0) {
          for (let index = 0; index < this.slot22.length; index++) {
            this.dietitianPrefItems.push({
              slot: this.slot22[index],
              category: "R",
              foodId: this.defaultslot22,
            });
          }
        }
        if (this.slot8 != undefined && this.defaultslot8?.length > 0) {
          for (let index = 0; index < this.slot8.length; index++) {
            this.dietitianPrefItems.push({
              slot: this.slot8[index],
              category: "B",
              foodId: this.defaultslot8,
            });
          }
        }
        if (this.slot5 != undefined && this.defaultslot5?.length > 0) {
          for (let index = 0; index < this.slot5.length; index++) {
            this.dietitianPrefItems.push({
              slot: this.slot5[index],
              category: "F",
              foodId: this.defaultslot5,
            });
          }
        }
        if (this.slot55 != undefined && this.defaultslot55?.length > 0) {
          for (let index = 0; index < this.slot55.length; index++) {
            this.dietitianPrefItems.push({
              slot: this.slot55[index],
              category: "PW",
              foodId: this.defaultslot55,
            });
          }
        }
        if (this.slot56 != undefined && this.defaultslot56?.length > 0) {
          for (let index = 0; index < this.slot56.length; index++) {
            this.dietitianPrefItems.push({
              slot: this.slot56[index],
              category: "WP",
              foodId: this.defaultslot56,
            });
          }
        }

        const itemCode = this.uniqueByKeepLast(
          this.dietitianPrefItems,
          (it) => it.category
        );
        // console.log("itemCode", itemCode);
        this.dietitianPrefItems = itemCode;
        if (this.slot4 != undefined && this.defaultslot4?.length > 0) {
          for (let index = 0; index < this.slot4.length; index++) {
            this.dietitianPrefItems.push({
              slot: this.slot4[index],
              category: "DM",
              foodId: this.defaultslot4,
            });
          }
        }
        // nonVegDinner=[];
        // this.nonveguptoDinner.forEach((item)=>{
        //     nonVegDinner.push(this.daysOfWeek[item-1]);
        // });
        // cheatDinner=[];
        // this.cheatUptoDinner1.forEach((item)=>{
        //     cheatDinner.push(this.daysOfWeek[item-1]);
        // });

        const payload = {
          _id: localStorage.getItem("email"),

          additionalPref: {
            planChosen: this.waterProtein,
            notNonVegDay: notNonVegDay,
            notNonEggDay: notNonEggDay,
            nonVegLunch: nonVegLunch,
            nonVegDinner: nonVegDinner,
            teaLiking:
              this.milkTea === "3" ? false : this.milkTea == "1" ? false : true,
            coffeeLiking:
              this.milkTea === "3" ? false : this.milkTea == "2" ? false : true,
            fruitsLiking: this.fruitesCon !== "" ? true : false,
            cookingProficiency:
              this.cookingExpeties != "undefined" ? this.cookingExpeties : "",
            cookingTimeAvailability:
              this.routine != "undefined" ? this.routine : "",
            cheatDaysLunch: cheatLunch,
            cheatDaysDinner: cheatDinner,
            normalDetoxDay: detoxDaysdetail,
            caloriesDeficit: 500,
            midDayMeals:
              this.midDayMeal === "" ? [] : this.midDayMeal?.split(","),
            fruitsDays: fruitsWeekDays,
            dessertDaysMidDay: desertLunch,
            dessertDaysEveMeal: desertDinner,
            fruitsDaysMidDay: fruitsLunch,
            fruitsDaysEveMeal: fruitsDinner,
            removeSlot:
              this.removeSlot.length > 0 &&
              this.removeSlot[0] != "" &&
              this.removeSlot != null
                ? this.removeSlot
                : [],
            dietitianPrefItems: this.dietitianPrefItems,
            dinnerOptions: [...new Set(this.dinnerOptions)].filter((item) => {
              return item !== "";
            }),
          },
        };

        // console.log("payload", payload);

        this.appService.updateDietPref(payload).subscribe((res) => {
          // console.log("res", res);
          this.utils.presentAlert("Record Saved successfully!");
          if (localStorage.getItem("dietitianId") != null) {
            // dietitianId, actionId, userId,  actionName,  companyId
            this.templateService
              .handleDietitianAction(
                localStorage.getItem("dietitianId"),
                "668d70ea1073b512dd2b9253",
                localStorage.getItem("email"),
                "Save Preferences",
                localStorage.getItem("companyId")
              )
              .subscribe((response) => {
                //  this.router.navigate(["deitician-search"]);
                //  this.router.navigate(['deitician-search'])
                //  .then(() => {
                //    window.location.reload();
                //  });
                console.log("Response >> ", response);
              });
          }
        });
      }
    );
  }

  uniqueByKeepLast(data, key) {
    return [...new Map(data.map((item) => [key(item), item])).values()];
  }
  onCheckedDinnerPref2(e, item: any) {
    if (e.detail.checked === true) {
      this.dinnerPref[1] = true;
      this.dinnerOptions.push("C");
    } else {
      this.dinnerPref[1] = false;
      this.dinnerOptions.filter((m, index) => {
        if (m === "C") {
          this.dinnerOptions[index] = "";
        }
      });
    }
  }
  onCheckedDinnerPref1(e, item: any) {
    if (e.detail.checked === true) {
      this.dinnerPref[0] = true;
      this.dinnerOptions.push("A");
    } else {
      this.dinnerPref[0] = false;
      this.dinnerOptions.filter((m, index) => {
        if (m === "A") {
          this.dinnerOptions[index] = "";
        }
      });
    }
  }
  onCheckedDinnerPref(e, item: any) {
    if (e.detail.checked === true) {
      this.dinnerPref[2] = true;
      this.dinnerOptions.push("W");
    } else {
      this.dinnerPref[2] = false;
      this.dinnerOptions.filter((m, index) => {
        if (m === "W") {
          this.dinnerOptions[index] = "";
        }
      });
    }
  }

  removeSlot = [];
  selectedSlotForRemove(e) {
    this.removeSlot = e.detail.value.toString().split(",");
    console.log("ionChange fired with value: " + this.removeSlot);
  }

  allnuts = [];
  getNutsValue() {
    this.allnuts = JSON.parse(JSON.stringify(hulnuts))?.nuts;
    console.log("nuts", this.allnuts);
  }

  slot1;
  selecteSlot1(e) {
    this.slot1 = e.detail.value;
    this.defaultslot1 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "N") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }

  slot2;
  selecteSlot2(e) {
    this.slot2 = e.detail.value;
    this.defaultslot2 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "NS") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }
  slot3 = "0";
  selecteSlot3(e) {
    this.slot3 = e.detail.value;
    this.defaultslot3 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "DZ") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }
  slot4;
  selecteSlot4(e) {
    this.slot4 = e.detail.value;
    this.defaultslot4 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "DM") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }

  slot5;
  selecteSlot5(e) {
    this.slot5 = e.detail.value;
    this.defaultslot5 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "F") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }
  slot55;
  selecteSlot55(e) {
    this.slot55 = e.detail.value;
    this.defaultslot55 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "PW") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }
  slot56;
  selecteSlot56(e) {
    this.slot56 = e.detail.value;
    this.defaultslot56 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "WS") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }
  slot6 = "8";
  selecteSlot6(e) {
    this.slot6 = e.detail.value;
    this.defaultslot6 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "DZ_1") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }
  slot7 = "7";
  selecteSlot7(e) {
    this.slot7 = e.detail.value;
    this.defaultslot7 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "S") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }

  slot21 = "4";
  selecteSlot21(e) {
    this.slot21 = e.detail.value;
    this.defaultslot21 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "S_1") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }
  slot22 = "4";
  selecteSlot22(e) {
    this.slot22 = e.detail.value;
    this.defaultslot22 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "R") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }

  slot8 = "7";
  selecteSlot8(e) {
    this.slot8 = e.detail.value;
    this.defaultslot8 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "B") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }

  slot9;
  selecteSlot9(e) {
    this.slot9 = e.detail.value;
    this.defaultslot9 = [];
    this.dietitianPrefItems.find((item, index) => {
      if (item?.category === "BTODBW") {
        this.dietitianPrefItems.splice(index, 1);
      }
    });
  }

  selectedNutsinSlot1(e) {
    setTimeout(() => {
      if (e.detail.value != "" && e.detail.value != undefined) {
        this.dietitianPrefItems.find((item, index) => {
          if (item.category === "N") {
            this.dietitianPrefItems[index].slot = this.slot1;
            this.dietitianPrefItems[index].category = "N";
            this.dietitianPrefItems[index].foodId = e.detail.value
              .toString()
              .split(",");
            return;
          }
        });
        if (
          this.dietitianPrefItems.filter((item) => item.category === "N")
            .length === 0
        ) {
          this.dietitianPrefItems.push({
            slot: this.slot1,
            category: "N",
            foodId: e.detail.value.toString().split(","),
          });
        }
        if (this.dietitianPrefItems.length == 0) {
          this.dietitianPrefItems.push({
            slot: this.slot1,
            category: "N",
            foodId: e.detail.value.toString().split(","),
          });
        }
      } else {
        this.dietitianPrefItems.filter((item, index) => {
          if (item.category === "N") {
            this.dietitianPrefItems.splice(index, 1);
          }
        });
      }
    }, 1000);
  }
  selectedNutsinSlot2(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (item.category === "NS") {
          this.dietitianPrefItems[index].slot = this.slot2;
          this.dietitianPrefItems[index].category = "NS";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter((item) => item.category === "NS")
          .length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: this.slot2,
          category: "NS",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: this.slot2,
          category: "NS",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.filter((item, index) => {
        if (item.category === "NS") {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }
  selectedNutsinSlot3(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (item.category === "DZ" && item.slot === 0) {
          this.dietitianPrefItems[index].slot = 0;
          this.dietitianPrefItems[index].category = "DZ";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter(
          (item) => item.category === "DZ" && item.slot === 0
        ).length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: 0,
          category: "DZ",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: 0,
          category: "DZ",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "DZ" &&
          this.dietitianPrefItems[index].slot === 0
        ) {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }
  selectedNutsinSlot4(e) {
    this.dietitianPrefItems = this.dietitianPrefItems.filter((item) => {
      return item.category !== "DM" && item.category !== "B";
    });
  }

  selectedNutsinSlot5(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (item.category === "F") {
          this.dietitianPrefItems[index].slot = this.slot5;
          this.dietitianPrefItems[index].category = "F";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter((item) => item.category === "F")
          .length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: this.slot5,
          category: "F",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: this.slot5,
          category: "F",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.find((item, index) => {
        if (item.category === "F") {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }

  selectedNutsinSlot55(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (item.category === "PW") {
          this.dietitianPrefItems[index].slot = this.slot55;
          this.dietitianPrefItems[index].category = "PW";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter((item) => item.category === "PW")
          .length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: this.slot55,
          category: "PW",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: this.slot55,
          category: "PW",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.find((item, index) => {
        if (item.category === "PW") {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }

  selectedNutsinSlot56(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (item.category === "WS") {
          this.dietitianPrefItems[index].slot = this.slot56;
          this.dietitianPrefItems[index].category = "WS";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter((item) => item.category === "WS")
          .length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: this.slot56,
          category: "WS",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: this.slot56,
          category: "WS",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.find((item, index) => {
        if (item.category === "WS") {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }
  selectedNutsinSlot6(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "DZ_1" &&
          (this.dietitianPrefItems[index].slot === 8 ||
            this.dietitianPrefItems[index].slot === "8")
        ) {
          this.dietitianPrefItems[index].slot = 8;
          this.dietitianPrefItems[index].category = "DZ_1";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter(
          (item) => item.category === "DZ_1" && item.slot === 8
        ).length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: 8,
          category: "DZ_1",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: 8,
          category: "DZ_1",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "DZ_1" &&
          this.dietitianPrefItems[index].slot === 8
        ) {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }
  selectedNutsinSlot7(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "S" &&
          Number(this.dietitianPrefItems[index].slot) === 7
        ) {
          this.dietitianPrefItems[index].slot = 7;
          this.dietitianPrefItems[index].category = "S";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter(
          (item) => item.category === "S" && item.slot === 7
        ).length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: 7,
          category: "S",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: 7,
          category: "S",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "S" &&
          this.dietitianPrefItems[index].slot === 7
        ) {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }
  selectedNutsinSlot21(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "S_1" &&
          Number(this.dietitianPrefItems[index].slot) === 4
        ) {
          this.dietitianPrefItems[index].slot = 4;
          this.dietitianPrefItems[index].category = "S_1";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter(
          (item) => item.category === "S_1" && item.slot === 4
        ).length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: 4,
          category: "S_1",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: 4,
          category: "S_1",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "S_1" &&
          this.dietitianPrefItems[index].slot === 4
        ) {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }
  selectedNutsinSlot22(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "R" &&
          Number(this.dietitianPrefItems[index].slot) === 4
        ) {
          this.dietitianPrefItems[index].slot = 4;
          this.dietitianPrefItems[index].category = "R";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter(
          (item) => item.category === "R" && item.slot === 4
        ).length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: 4,
          category: "R",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: 4,
          category: "R",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "R" &&
          this.dietitianPrefItems[index].slot === 4
        ) {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }
  selectedNutsinSlot8(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "B" &&
          Number(this.dietitianPrefItems[index].slot) === 7
        ) {
          this.dietitianPrefItems[index].slot = 7;
          this.dietitianPrefItems[index].category = "B";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter(
          (item) => item.category === "B" && item.slot === 7
        ).length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: 7,
          category: "B",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: 7,
          category: "B",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.find((item, index) => {
        if (
          item.category === "B" &&
          this.dietitianPrefItems[index].slot === 7
        ) {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }
  selectedNutsinSlot9(e) {
    if (e.detail.value != "" && e.detail.value != undefined) {
      this.dietitianPrefItems.find((item, index) => {
        if (item.category === "BTODBW") {
          this.dietitianPrefItems[index].slot = this.slot9;
          this.dietitianPrefItems[index].category = "BTODBW";
          this.dietitianPrefItems[index].foodId = e.detail.value
            .toString()
            .split(",");
          return;
        }
      });
      if (
        this.dietitianPrefItems.filter((item) => item.category === "BTODBW")
          .length === 0
      ) {
        this.dietitianPrefItems.push({
          slot: this.slot9,
          category: "BTODBW",
          foodId: e.detail.value.toString().split(","),
        });
      }
      if (this.dietitianPrefItems.length == 0) {
        this.dietitianPrefItems.push({
          slot: this.slot9,
          category: "BTODBW",
          foodId: e.detail.value.toString().split(","),
        });
      }
    } else {
      this.dietitianPrefItems.find((item, index) => {
        if (item.category === "BTODBW") {
          this.dietitianPrefItems.splice(index, 1);
        }
      });
    }
  }
  save() {}

  search: any = localStorage.getItem("email");
  isSearch = false;
  searchData: any = "";
  lifeStyle: any;
  searchDetails: any;
  planChoosen: any;
  profileInititals = "";
  additionPreferences: any;
  updateTargetCal: any;
  recommendedTargetCal: any;
  isHindi: boolean = true;
  defaultData: any;
  activityName;
  diseases = [];
  diseases1 = [];
  alergies = ["SF,SO", "ML", "F", "E", "N", "G"];
  mealPref;
  community = [];
  profileData: any = {};

  handleSearchEvent($event) {
    this.search = $event;
    console.log("Inside Parent Page:: ", this.search);
    this.fetchProfile();
  }

  fetchProfile() {
    if (!this.search?.trim()) {
      return;
    }
    CONSTANTS.email = this.search;
    localStorage.setItem("email", this.search);
    this.isSearch = true;
    this.appS.searchProfile(this.search).then((profileData: any) => {
      // this.searchData = profileData["_id"];
      console.log("profileData:-", profileData);
      this.lifeStyle = profileData?.lifeStyle;
      this.additionPreferences = profileData["additionalPref"];
      const data = profileData["profile"];
      if (data) {
        this.searchDetails = JSON.parse(JSON.stringify(profileData));
        console.log("this.searchDetails", this.searchDetails);

        const planC = this.dietPlan.filter((item) => {
          return item.id === this.searchDetails?.additionalPref?.planChosen;
        });
        this.planChoosen = planC[0]?.name;
        this.updateTargetCal = this.searchDetails?.lifeStyle?.calories;
        this.recommendedTargetCal = this.searchDetails?.lifeStyle?.calories;
        const dd = JSON.parse(JSON.stringify(profileData));
        this.profileInititals = dd?.profile.name;
        // dd?.profile.name?.replace("  "," ")?.split(' ')[0][0].toUpperCase() +
        // (dd?.profile?.name?.replace("  "," ")?.split(' ')?.length>=2?dd?.profile?.name?.replace("  "," ")?.split(' ')[1][0].toUpperCase():dd?.profile?.name?.replace("  "," ")?.split(' ')[0][1].toUpperCase());

        // this.isHindi =
        //   profileData["profile"]?.languagePref == undefined ? false : true;
        this.search = "";
        this.appServe.getUserToken(this.search).subscribe(
          (response) => {
            console.log(response);
          },
          (error) => {
            console.log("User token ", error.error.text);
            localStorage.setItem("personal_token", error.error.text);
            console.log("Category Error", error);
          }
        );
        this.getProfile();
        this.getToken1();
      } else {
        // this.searchDetails = undefined;
        return;
      }
    });
  }

  getToken1() {
    if (!localStorage.getItem("email")) {
      this.utilities.presentAlert("Please enter email");
    } else {
      this.appServe.getUserToken(localStorage.getItem("email")).subscribe(
        (response) => {
          console.log(response);
        },
        (error) => {
          console.log("User token ", error.error.text);
          this.appS.getDefaultData(error.error.text).then((res) => {
            this.diseases = [];
            this.diseases1 = [];
            this.alergies = ["SF,SO", "ML", "F", "E", "N", "G"];
            this.mealPref;
            this.community = [];
            this.defaultData = res;
            this.activityName = this.defaultData.otherMaster.activities.filter(
              (item) => {
                return (
                  item.code ===
                  this.searchDetails?.lifeStyle?.activities["code"]
                );
              }
            );

            // console.log("Activity Response:: ", this.defaultData);

            for (
              let index = 0;
              index < this.searchDetails?.lifeStyle?.diseases.length;
              index++
            ) {
              for (
                let j = 0;
                j < this.defaultData?.otherMaster?.diseases.length;
                j++
              ) {
                if (
                  this.defaultData?.otherMaster?.diseases[j].code ===
                  this.searchDetails?.lifeStyle?.diseases[index]
                ) {
                  if (
                    this.alergies.includes(
                      this.defaultData?.otherMaster?.diseases[j].code
                    )
                  ) {
                    this.diseases1.push(
                      this.defaultData?.otherMaster?.diseases[j].value
                    );
                  } else {
                    this.diseases.push(
                      this.defaultData?.otherMaster?.diseases[j].value
                    );
                  }
                }
              }
            }
            this.mealPref = this.defaultData?.otherMaster?.foodPref?.filter(
              (item) => {
                return item.code === this.searchDetails?.lifeStyle?.foodType;
              }
            );
            for (
              let index = 0;
              index < this.searchDetails?.lifeStyle?.communities.length;
              index++
            ) {
              for (
                let j = 0;
                j < this.defaultData?.otherMaster?.community.length;
                j++
              ) {
                if (
                  this.defaultData?.otherMaster?.community[j].code ===
                  this.searchDetails?.lifeStyle?.communities[index]
                ) {
                  this.community.push(
                    this.defaultData?.otherMaster?.community[j].value
                  );
                }
              }
            }
          });

          this.appS.getOnePlan1(error.error.text).then((res) => {
            console.log("getOnePlan", res);
          });
          console.log("Category Error", error);
        }
      );
    }
  }

  getProfile() {
    this.profileData = [];
    this.appservice.getProfile().then((res) => {
      this.profileData = res;

      // if (this.profileData?.profile?.subCategory === "weightloss") {
      //   this.profileData.profile.subCategory = "Weight Loss";
      // }
      // if (this.profileData?.profile?.subCategory === "weightmaintenance") {
      //   this.profileData.profile.subCategory = "Weight Maintenance";
      // }
      // if (this.profileData?.profile?.subCategory === "musclebuilding") {
      //   this.profileData.profile.subCategory = "Muscle Building";
      // }
      // if (this.profileData?.profile?.subCategory === "leanbody") {
      //   this.profileData.profile.subCategory = "Lean Body";
      // }

      // let h: any =
      //   this.profileData?.demographic?.height?.unit === "in"
      //     ? this.profileData?.demographic?.height?.value / 12
      //     : this.profileData?.demographic?.height?.value;
      // if (this.profileData?.demographic?.height?.unit === "in") {
      //   console.log(h);
      //   h = h.toString().split(".");
      //   console.log(h);
      //   const h1: any = (h[1] / 0.0833333).toString().split("0")[0];
      //   console.log(h1);
      //   this.profileData.demographic.height.value = `${h[0]}' ${h1}"`;
      // } else {
      //   this.profileData.demographic.height.value =
      //     this.profileData.demographic.height.value + " cm";
      // }
      // console.log(this.profileData);
    });
  }
}
